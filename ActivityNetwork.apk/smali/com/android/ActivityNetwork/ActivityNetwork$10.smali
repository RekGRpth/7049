.class Lcom/android/ActivityNetwork/ActivityNetwork$10;
.super Landroid/os/Handler;
.source "ActivityNetwork.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ActivityNetwork/ActivityNetwork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1    # Landroid/os/Message;

    iget v2, p1, Landroid/os/Message;->what:I

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "received message:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x1

    if-ne v2, v7, :cond_2

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v7

    if-eqz v7, :cond_1

    :try_start_0
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v7

    iget-object v8, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->clear_sdcard:Ljava/lang/String;
    invoke-static {v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1800(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v5

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "thread mNlConnection.sendCmd(clear_sdcard)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v7

    const/16 v8, 0xc

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "clear log :Connect FAIL "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v7, 0x2

    if-ne v2, v7, :cond_5

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "clear log mNlConnection == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    new-instance v8, Lcom/android/ActivityNetwork/NlConnection;

    const-string v9, "netdiag"

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/android/ActivityNetwork/NlConnection;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7, v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1402(Lcom/android/ActivityNetwork/ActivityNetwork;Lcom/android/ActivityNetwork/NlConnection;)Lcom/android/ActivityNetwork/NlConnection;

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/ActivityNetwork/NlConnection;->connect()Z

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "Connection failed"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v7

    if-eqz v7, :cond_4

    :try_start_1
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->run_command:Ljava/lang/String;
    invoke-static {v9}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1900(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;
    invoke-static {v9}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v5

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "thread mNlConnection.sendCmd(run_command)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const-string v8, "There is some error to create connection with deamon!"

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_5
    const/16 v7, 0x8

    if-ne v2, v7, :cond_8

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "stop command "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v7

    if-nez v7, :cond_6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "clear log mNlConnection == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    new-instance v8, Lcom/android/ActivityNetwork/NlConnection;

    const-string v9, "netdiag"

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/android/ActivityNetwork/NlConnection;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7, v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1402(Lcom/android/ActivityNetwork/ActivityNetwork;Lcom/android/ActivityNetwork/NlConnection;)Lcom/android/ActivityNetwork/NlConnection;

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/ActivityNetwork/NlConnection;->connect()Z

    move-result v7

    if-nez v7, :cond_6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "Connection failed"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v7

    if-eqz v7, :cond_7

    :try_start_2
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v7

    iget-object v8, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->stop_command:Ljava/lang/String;
    invoke-static {v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v5

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "thread mNlConnection.sendCmd(stop_command)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const-string v8, "There is some error to create connection with deamon!"

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_8
    const/16 v7, 0x20

    if-le v2, v7, :cond_16

    and-int/lit8 v7, v2, 0x1

    if-lez v7, :cond_b

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->Clear_log_title:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2100(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    and-int/lit16 v7, v2, 0x80

    if-lez v7, :cond_e

    const-string v0, "OK!"

    :goto_2
    const-string v7, "OK!"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    new-instance v7, Landroid/app/AlertDialog$Builder;

    iget-object v8, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {v7, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v8, "Command Running Status"

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const-string v8, "Ok"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_9
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->Clear_log_title:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2100(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const v8, 0x7f05000c

    invoke-virtual {v7, v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    if-eqz v3, :cond_a

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_a
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v7

    const/16 v8, 0xc

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_b
    and-int/lit8 v7, v2, 0x2

    if-lez v7, :cond_c

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->Run_command_title:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2200(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_c
    and-int/lit8 v7, v2, 0x10

    if-lez v7, :cond_d

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->Stop_command_title:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2300(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_d
    const-string v1, "other"

    goto :goto_1

    :cond_e
    and-int/lit16 v7, v2, 0x100

    if-lez v7, :cond_f

    const-string v0, "some error has happened!"

    goto :goto_2

    :cond_f
    and-int/lit16 v7, v2, 0x200

    if-lez v7, :cond_10

    const-string v0, "there is no deamon pid running!"

    goto/16 :goto_2

    :cond_10
    const-string v0, "some error has happened!"

    goto/16 :goto_2

    :cond_11
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->Stop_command_title:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2300(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_14

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const v8, 0x7f050002

    invoke-virtual {v7, v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    if-eqz v3, :cond_12

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_12
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const v8, 0x7f050003

    invoke-virtual {v7, v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    if-eqz v3, :cond_13

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_13
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceRunShellTag:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2400(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceDisabled:Ljava/lang/String;
    invoke-static {v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2500(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStopShellTag:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2600(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceDisabled:Ljava/lang/String;
    invoke-static {v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2500(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    goto/16 :goto_0

    :cond_14
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->Run_command_title:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2200(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const v8, 0x7f050002

    invoke-virtual {v7, v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    if-eqz v3, :cond_15

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_15
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceRunShellTag:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2400(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceDisabled:Ljava/lang/String;
    invoke-static {v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2500(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStopShellTag:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2600(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceDisabled:Ljava/lang/String;
    invoke-static {v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$2500(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    goto/16 :goto_0

    :cond_16
    const/16 v7, 0xc

    if-ne v2, v7, :cond_0

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v7

    const/16 v8, 0xc

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1000(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/app/ProgressDialog;

    move-result-object v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-virtual {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->isFinishing()Z

    move-result v7

    if-nez v7, :cond_17

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "remove sendEmptyMessageDelayed timer"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1000(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/app/ProgressDialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    :try_start_3
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1000(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/app/ProgressDialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const/4 v8, 0x0

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v7, v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1002(Lcom/android/ActivityNetwork/ActivityNetwork;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "waitingStopDialog = null"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v4

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "waitingStopDialog.dismiss "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_17
    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-virtual {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->isFinishing()Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork$10;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "ActivityNetwork.this.isFinishing()==true"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
