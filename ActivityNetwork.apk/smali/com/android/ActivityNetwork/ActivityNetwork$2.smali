.class Lcom/android/ActivityNetwork/ActivityNetwork$2;
.super Landroid/content/BroadcastReceiver;
.source "ActivityNetwork.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ActivityNetwork/ActivityNetwork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0xc

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BroadcastReceiver onReceive: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.mediatek.syslogger.action"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "From"

    const-string v4, "ActivityNetworkLog"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "To"

    const-string v4, "CommonUI"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.android.ActivityNetwork.APLogger_START"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "updateUItoStart(true)  "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "Return"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "Command"

    const-string v4, "Start"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "Normal"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork;->updateUItoStart(Z)V
    invoke-static {v3, v8}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork;Z)V

    :goto_0
    const-string v3, "Return"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-virtual {v3, v2}, Lcom/android/ActivityNetwork/ActivityNetwork;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$100(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    const-string v3, "com.android.ActivityNetwork.APLogger_STOP"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "updateUItoStart(false)  "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "Command"

    const-string v4, "Stop"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "Return"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "Normal"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork;->updateUItoStart(Z)V
    invoke-static {v3, v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork;Z)V

    :goto_2
    const-string v3, "Return"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-virtual {v3, v2}, Lcom/android/ActivityNetwork/ActivityNetwork;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$100(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_2

    :cond_4
    const-string v3, "com.android.ActivityNetwork.APLogger_START_FAIL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "receive Utils.ACTION_ACTIVITY_START_FAIL "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_5
    const-string v3, "com.android.ActivityNetwork.APLogger_STOP_FAIL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "receive Utils.ACTION_ACTIVITY_STOP_FAIL "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
