.class Lcom/android/ActivityNetwork/ActivityNetwork$4;
.super Ljava/lang/Object;
.source "ActivityNetwork.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/ActivityNetwork/ActivityNetwork;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTextChanged new log size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "count"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "start "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "before "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTextChanged new log size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceLogSizeTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    return-void
.end method
