.class Lcom/android/ActivityNetwork/ActivityNetwork$5;
.super Ljava/lang/Object;
.source "ActivityNetwork.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ActivityNetwork/ActivityNetwork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 19
    .param p1    # Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v10

    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting_config_path:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v7, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting_config_path:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting_config_name:Ljava/lang/String;
    invoke-static {v14}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$900(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v6, v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, Ljava/util/Properties;

    invoke-direct {v12}, Ljava/util/Properties;-><init>()V

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "click the button,enter...."

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1000(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/app/ProgressDialog;

    move-result-object v13

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const/high16 v16, 0x7f040000

    invoke-virtual/range {v15 .. v16}, Lcom/android/ActivityNetwork/ActivityNetwork;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    move-object/from16 v16, v0

    const v17, 0x7f040001

    invoke-virtual/range {v16 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x1

    const/16 v18, 0x0

    invoke-static/range {v14 .. v18}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v14

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v13, v14}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1002(Lcom/android/ActivityNetwork/ActivityNetwork;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const-string v14, "storage"

    invoke-virtual {v13, v14}, Lcom/android/ActivityNetwork/ActivityNetwork;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/storage/StorageManager;

    const-string v13, "persist.radio.log2sd.path"

    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "SDpath is  "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_1

    const-string v13, ""

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    :cond_1
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_7

    const-string v2, "/storage/sdcard0"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathTag:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1100(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathSdCard:Ljava/lang/String;
    invoke-static {v14}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1200(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v13, v14}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "dir:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting_config_path:Ljava/lang/String;
    invoke-static {v15}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "does not exist"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "SDpath is null, so changed to "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v13, "/mnt/sdcard"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    const-string v13, "/storage/sdcard0"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    const-string v13, "/sdcard"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const-string v14, "internal_sd_path"

    invoke-static {v14}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v13, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v13, v13, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v13, v13, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const-string v14, "/storage/sdcard0"

    iput-object v14, v13, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v13, v13, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v4, v13}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "log should be saved in internal sdcard. At this time,internal sdcard is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v15, v15, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "Mount status:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v13

    invoke-virtual {v13, v10}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    const-string v13, "mounted"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_e

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v13

    if-eqz v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v13

    const/16 v14, 0xc

    invoke-virtual {v13, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_6
    new-instance v13, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {v13, v14}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v14, "ERROR"

    invoke-virtual {v13, v14}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v13

    const-string v14, "No SD Card"

    invoke-virtual {v13, v14}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v13

    const-string v14, "Ok"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_2
    return-void

    :cond_7
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_8

    const-string v2, "/storage/sdcard0"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathTag:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1100(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathSdCard:Ljava/lang/String;
    invoke-static {v14}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1200(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v13, v14}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "file:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting_config_name:Ljava/lang/String;
    invoke-static {v15}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$900(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "does not exist"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v12, v9}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v8, v9

    :goto_3
    const-string v13, "persist.radio.log2sd.path"

    const-string v14, "/storage/sdcard0"

    invoke-virtual {v12, v13, v14}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object v2, v11

    const-string v13, "/data"

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathTag:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1100(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathPhone:Ljava/lang/String;
    invoke-static {v14}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1300(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v13, v14}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mtklog-config.prop log size:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_0
    move-exception v3

    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "mtklog-config.prop loadconfig"

    invoke-static {v13, v14, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathTag:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1100(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_a
    const-string v13, "/data"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    const-string v1, "mounted"

    goto/16 :goto_1

    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const-string v14, "external_sd_path"

    invoke-static {v14}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v13, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v13, v13, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    if-eqz v13, :cond_c

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v13, v13, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_d

    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const-string v14, "/storage/sdcard1"

    iput-object v14, v13, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v13, v13, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v4, v13}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "log should be saved in external sdcard. At this time,external sdcard is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v15, v15, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "Mount status:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1000(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/app/ProgressDialog;

    move-result-object v13

    if-eqz v13, :cond_f

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v13

    if-eqz v13, :cond_f

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v13

    const/16 v14, 0xc

    const-wide/16 v15, 0x2710

    invoke-virtual/range {v13 .. v16}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_f
    new-instance v5, Landroid/content/Intent;

    const-string v13, "com.android.ActivityNetwork.ActivityNetwork_Service"

    invoke-direct {v5, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v13, "com.android.ActivityNetwork.DEVICEBOOTUP"

    const-string v14, "true"

    invoke-virtual {v5, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v13, "com.android.ActivityNetwork.Update_UI"

    const-string v14, "true"

    invoke-virtual {v5, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "start service"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-virtual {v13}, Lcom/android/ActivityNetwork/ActivityNetwork;->getBaseContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13, v5}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_2

    :catch_1
    move-exception v3

    move-object v8, v9

    goto/16 :goto_5
.end method
