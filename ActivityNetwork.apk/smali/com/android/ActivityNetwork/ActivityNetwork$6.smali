.class Lcom/android/ActivityNetwork/ActivityNetwork$6;
.super Ljava/lang/Object;
.source "ActivityNetwork.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ActivityNetwork/ActivityNetwork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ActivityNetwork stop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$100(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1000(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const v4, 0x7f040002

    invoke-virtual {v3, v4}, Lcom/android/ActivityNetwork/ActivityNetwork;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const v5, 0x7f040003

    invoke-virtual {v4, v5}, Lcom/android/ActivityNetwork/ActivityNetwork;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v2, v3, v4, v5, v6}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v2

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1002(Lcom/android/ActivityNetwork/ActivityNetwork;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.ActivityNetwork.ActivityNetwork_Service"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.ActivityNetwork.DEVICEBOOTUP"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.android.ActivityNetwork.Update_UI"

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-virtual {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xc

    const-wide/32 v3, 0x15f90

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "sendEmptyMessageDelayed 90s"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork$6;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "ERROR"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "No tcpdump starting,\npress start button first"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "Ok"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method
