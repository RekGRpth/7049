.class Lcom/android/ActivityNetwork/ActivityNetwork$7;
.super Ljava/lang/Object;
.source "ActivityNetwork.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ActivityNetwork/ActivityNetwork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const v4, 0x7f050008

    invoke-virtual {v3, v4}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v4, "ERROR"

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const-string v4, "You must stop first, then clear log is permitted!"

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const-string v4, "Ok"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1000(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/app/ProgressDialog;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "clear log mNlConnection == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    new-instance v4, Lcom/android/ActivityNetwork/NlConnection;

    const-string v5, "netdiag"

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/android/ActivityNetwork/NlConnection;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v3, v4}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1402(Lcom/android/ActivityNetwork/ActivityNetwork;Lcom/android/ActivityNetwork/NlConnection;)Lcom/android/ActivityNetwork/NlConnection;

    :goto_1
    if-eqz v2, :cond_2

    :try_start_0
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/ActivityNetwork/NlConnection;->connect()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_4

    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const v6, 0x7f040004

    invoke-virtual {v5, v6}, Lcom/android/ActivityNetwork/ActivityNetwork;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const v7, 0x7f040005

    invoke-virtual {v6, v7}, Lcom/android/ActivityNetwork/ActivityNetwork;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6, v9, v8}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v4

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v3, v4}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1002(Lcom/android/ActivityNetwork/ActivityNetwork;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const v4, 0x7f05000c

    invoke-virtual {v3, v4}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_3

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_3
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "clear log start ...."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0xc

    const-wide/16 v5, 0x2710

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_4
    const-wide/16 v3, 0x1388

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Connection retry"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :catch_0
    move-exception v1

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$7;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "thread "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method
