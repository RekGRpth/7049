.class Lcom/android/ActivityNetwork/ActivityNetwork$9;
.super Ljava/lang/Object;
.source "ActivityNetwork.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ActivityNetwork/ActivityNetwork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$9;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$9;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const v2, 0x7f050003

    invoke-virtual {v1, v2}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$9;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->isEnabled()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$9;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork$9;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1602(Lcom/android/ActivityNetwork/ActivityNetwork;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$9;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$9;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$9;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork;->updateUI_Runshell(ZLjava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1700(Lcom/android/ActivityNetwork/ActivityNetwork;ZLjava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork$9;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop command start ....: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork$9;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method
