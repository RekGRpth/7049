.class public Lcom/android/ActivityNetwork/ActivityNetwork;
.super Landroid/app/Activity;
.source "ActivityNetwork.java"


# static fields
.field private static Aservice:Lcom/android/ActivityNetwork/ActivityNetwork_Service; = null

.field public static final COMMAND_CLEARLOG:Ljava/lang/String; = "ClearLog"

.field private static final MSG_ALIVE:I = 0x9

.field private static final MSG_CLEARLOG:I = 0x1

.field private static final MSG_CONN_FAIL:I = 0x5

.field private static final MSG_DIE:I = 0x3

.field private static final MSG_INIT:I = 0x4

.field private static final MSG_LOW:I = 0xb

.field private static final MSG_REJECT:I = 0xa

.field private static final MSG_RUNCOMMAND:I = 0x2

.field private static final MSG_SD:I = 0x7

.field private static final MSG_STOP:I = 0x6

.field private static final MSG_STOPCOMMAND:I = 0x8

.field private static final MSG_STOPDIAG:I = 0xc

.field private static final MSG_STOP_TCPDUMP_COMMAND:I = 0x10

.field private static MSG_TYPE:I


# instance fields
.field private Clear_log_title:Ljava/lang/String;

.field private IPaddress:Ljava/lang/String;

.field private Run_command_title:Ljava/lang/String;

.field SDpath_SWAP:Ljava/lang/String;

.field private Stop_command_title:Ljava/lang/String;

.field private Stop_tcpdump_title:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private Tcpdump_title:Ljava/lang/String;

.field private clear_sdcard:Ljava/lang/String;

.field private command:Ljava/lang/String;

.field private flag:Z

.field private isBootup:Ljava/lang/String;

.field private mBindListener:Landroid/view/View$OnClickListener;

.field private mButtonCancleCommand:Landroid/widget/Button;

.field private mButtonClearLog:Landroid/widget/Button;

.field private mButtonClearLoglistener:Landroid/view/View$OnClickListener;

.field private mButtonRunCommand:Landroid/widget/Button;

.field private mButtonRunCommandlistener:Landroid/view/View$OnClickListener;

.field private mButtonStart:Landroid/widget/Button;

.field private mButtonStop:Landroid/widget/Button;

.field private mButtonStopCommandlistener:Landroid/view/View$OnClickListener;

.field private mIntentReceiverService:Landroid/content/BroadcastReceiver;

.field private mLogPathPhone:Ljava/lang/String;

.field private mLogPathSdCard:Ljava/lang/String;

.field private mLogPathTag:Ljava/lang/String;

.field private mLogPathTypePhone:Ljava/lang/String;

.field private mLogPathTypeSdCard:Ljava/lang/String;

.field private mLogPathTypeTag:Ljava/lang/String;

.field private mLogSize:Ljava/lang/String;

.field private mLogSizeTag:Ljava/lang/String;

.field private mMessageHandler:Landroid/os/Handler;

.field private mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

.field private mNetworkSetting_config_name:Ljava/lang/String;

.field private mNetworkSetting_config_path:Ljava/lang/String;

.field private mNetworkSetting_path:Ljava/lang/String;

.field private mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

.field private mServiceBootOptionTag:Ljava/lang/String;

.field private mServiceCommandTag:Ljava/lang/String;

.field private mServiceDisabled:Ljava/lang/String;

.field private mServiceEnabled:Ljava/lang/String;

.field private mServiceLogSizeTag:Ljava/lang/String;

.field private mServiceOnBootDisabled:Ljava/lang/String;

.field private mServiceOnBootEnabled:Ljava/lang/String;

.field private mServiceReceiver:Landroid/content/BroadcastReceiver;

.field private mServiceRunShellTag:Ljava/lang/String;

.field private mServiceRunning:Ljava/lang/String;

.field private mServiceStatusTag:Ljava/lang/String;

.field private mServiceStopShellTag:Ljava/lang/String;

.field private mServiceStopped:Ljava/lang/String;

.field private mServiceSync:Ljava/lang/String;

.field private mServiceSyncNo:Ljava/lang/String;

.field private mServiceSyncYes:Ljava/lang/String;

.field private mTextbox:Landroid/widget/EditText;

.field private mTextbox_logsize:Landroid/widget/EditText;

.field private mUnbindListener:Landroid/view/View$OnClickListener;

.field final network_service:Ljava/lang/String;

.field final on_start:I

.field private run_command:Ljava/lang/String;

.field private shouldcpy:Ljava/lang/Boolean;

.field private stop_command:Ljava/lang/String;

.field private waitingStopDialog:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/ActivityNetwork/ActivityNetwork;->MSG_TYPE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->shouldcpy:Ljava/lang/Boolean;

    const-string v0, "false"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->isBootup:Ljava/lang/String;

    const-string v0, "/data/data/com.android.ActivityNetwork/files"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting_path:Ljava/lang/String;

    const-string v0, "service_status"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStatusTag:Ljava/lang/String;

    const-string v0, "service_running"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceRunning:Ljava/lang/String;

    const-string v0, "service_stopped"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStopped:Ljava/lang/String;

    const-string v0, "runshell_status"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceRunShellTag:Ljava/lang/String;

    const-string v0, "stop_runshell_status"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStopShellTag:Ljava/lang/String;

    const-string v0, "true"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceEnabled:Ljava/lang/String;

    const-string v0, "false"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceDisabled:Ljava/lang/String;

    const-string v0, "input_shell_command"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceCommandTag:Ljava/lang/String;

    const-string v0, "input_log_size"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceLogSizeTag:Ljava/lang/String;

    const-string v0, "service_boot_option"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceBootOptionTag:Ljava/lang/String;

    const-string v0, "true"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceOnBootEnabled:Ljava/lang/String;

    const-string v0, "false"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceOnBootDisabled:Ljava/lang/String;

    const-string v0, "UI_prop_sync"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceSync:Ljava/lang/String;

    const-string v0, "yes"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceSyncYes:Ljava/lang/String;

    const-string v0, "no"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceSyncNo:Ljava/lang/String;

    const-string v0, "log_size"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogSizeTag:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogSize:Ljava/lang/String;

    const-string v0, "log_path"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathTag:Ljava/lang/String;

    const-string v0, "/data/mtklog/network"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathPhone:Ljava/lang/String;

    const-string v0, "/sdcard/mtklog/network"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathSdCard:Ljava/lang/String;

    const-string v0, "/system/etc"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting_config_path:Ljava/lang/String;

    const-string v0, "mtklog-config.prop"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting_config_name:Ljava/lang/String;

    const-string v0, "log_path_type"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathTypeTag:Ljava/lang/String;

    const-string v0, "phone"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathTypePhone:Ljava/lang/String;

    const-string v0, "sdcard"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathTypeSdCard:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->flag:Z

    const-string v0, "ActivityNetwork"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v0, "netdiag"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->network_service:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->IPaddress:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->on_start:I

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    const-string v0, "clear_sdcard_start"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->clear_sdcard:Ljava/lang/String;

    const-string v0, "runshell_command_start_"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->run_command:Ljava/lang/String;

    const-string v0, "runshell_command_stop_"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->stop_command:Ljava/lang/String;

    const-string v0, "Clear log:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->Clear_log_title:Ljava/lang/String;

    const-string v0, "Run command:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->Run_command_title:Ljava/lang/String;

    const-string v0, "Stop command:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->Stop_command_title:Ljava/lang/String;

    const-string v0, "Tcpdump:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->Tcpdump_title:Ljava/lang/String;

    const-string v0, "Stop tcpdump:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->Stop_tcpdump_title:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->SDpath_SWAP:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork$1;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork$1;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mIntentReceiverService:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork$2;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork$2;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork$5;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork$5;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mBindListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork$6;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork$6;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mUnbindListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork$7;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork$7;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonClearLoglistener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork$8;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork$8;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonRunCommandlistener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork$9;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork$9;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStopCommandlistener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork$10;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork$10;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/ActivityNetwork/ActivityNetwork;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathSdCard:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogPathPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NlConnection;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/android/ActivityNetwork/ActivityNetwork;Lcom/android/ActivityNetwork/NlConnection;)Lcom/android/ActivityNetwork/NlConnection;
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;
    .param p1    # Lcom/android/ActivityNetwork/NlConnection;

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/android/ActivityNetwork/ActivityNetwork;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/android/ActivityNetwork/ActivityNetwork;ZLjava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/android/ActivityNetwork/ActivityNetwork;->updateUI_Runshell(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->clear_sdcard:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->run_command:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->stop_command:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->Clear_log_title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->Run_command_title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->Stop_command_title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceRunShellTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceDisabled:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStopShellTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/ActivityNetwork/ActivityNetwork;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mMessageHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/ActivityNetwork/ActivityNetwork;Z)V
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/ActivityNetwork/ActivityNetwork;->updateUItoStart(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/ActivityNetwork/ActivityNetwork;)Lcom/android/ActivityNetwork/NetworkSetting;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceCommandTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceLogSizeTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting_config_path:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/ActivityNetwork/ActivityNetwork;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting_config_name:Ljava/lang/String;

    return-object v0
.end method

.method private getAvailableExternalMemorySize()J
    .locals 9

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v5

    const-string v6, "mounted"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v4, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v5

    int-to-long v2, v5

    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    int-to-long v0, v5

    mul-long v5, v0, v2

    const-wide/32 v7, 0x100000

    div-long/2addr v5, v7

    :goto_0
    return-wide v5

    :cond_0
    const-wide/16 v5, 0x0

    goto :goto_0
.end method

.method private getAvailableInternalMemorySize()J
    .locals 10

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v4

    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v2, v6

    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v0, v6

    mul-long v6, v0, v2

    const-wide/32 v8, 0x100000

    div-long/2addr v6, v8

    return-wide v6
.end method

.method private updateUI_RunCommand()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v3}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceRunShellTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceEnabled:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStopShellTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceEnabled:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceCommandTag:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateUI_RunCommand() ....: onResume "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceEnabled:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceDisabled:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonRunCommand:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonCancleCommand:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateUI_RunCommand() ....: running "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v3, v0}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceDisabled:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceEnabled:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonRunCommand:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonCancleCommand:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateUI_RunCommand() ....: stopping "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->command:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v3, v0}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    goto :goto_0
.end method

.method private updateUI_Runshell(ZLjava/lang/String;)V
    .locals 4
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonRunCommand:Landroid/widget/Button;

    if-nez p1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonCancleCommand:Landroid/widget/Button;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v1}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v0

    if-ne p1, v2, :cond_1

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceRunShellTag:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceEnabled:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStopShellTag:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceDisabled:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceCommandTag:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v1, v0}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceRunShellTag:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceDisabled:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStopShellTag:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceEnabled:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private updateUItoStart(Z)V
    .locals 5
    .param p1    # Z

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;

    if-nez p1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v1}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v0

    if-ne p1, v2, :cond_2

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceBootOptionTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceOnBootEnabled:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStatusTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceRunning:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v1, v0}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    move-result v1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v4, "mNetworkSetting.saveConfig exception"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;

    if-nez p1, :cond_3

    :goto_2
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceBootOptionTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceOnBootDisabled:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStatusTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v4, "onCreate"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f020001

    invoke-virtual {p0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->setContentView(I)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v3

    if-ne v3, v6, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v4, "onCreate waitingStopDialog = null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v3, Lcom/android/ActivityNetwork/NetworkSetting;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting_path:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/android/ActivityNetwork/NetworkSetting;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    const v3, 0x7f050008

    invoke-virtual {p0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;

    const v3, 0x7f05000a

    invoke-virtual {p0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;

    const v3, 0x7f050003

    invoke-virtual {p0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonCancleCommand:Landroid/widget/Button;

    const v3, 0x7f050002

    invoke-virtual {p0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonRunCommand:Landroid/widget/Button;

    const v3, 0x7f050001

    invoke-virtual {p0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox:Landroid/widget/EditText;

    const v3, 0x7f050006

    invoke-virtual {p0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox_logsize:Landroid/widget/EditText;

    const v3, 0x7f05000c

    invoke-virtual {p0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonClearLog:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonClearLog:Landroid/widget/Button;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonRunCommand:Landroid/widget/Button;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonCancleCommand:Landroid/widget/Button;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox:Landroid/widget/EditText;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v4, "setOnClickListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "ro.build.type"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "user"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v4, "build type---user"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox:Landroid/widget/EditText;

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonCancleCommand:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonRunCommand:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_1
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mBindListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mUnbindListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonCancleCommand:Landroid/widget/Button;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStopCommandlistener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonRunCommand:Landroid/widget/Button;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonRunCommandlistener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonClearLog:Landroid/widget/Button;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonClearLoglistener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox:Landroid/widget/EditText;

    new-instance v4, Lcom/android/ActivityNetwork/ActivityNetwork$3;

    invoke-direct {v4, p0}, Lcom/android/ActivityNetwork/ActivityNetwork$3;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v3}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox_logsize:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceLogSizeTag:Ljava/lang/String;

    const-string v5, "200"

    invoke-virtual {v2, v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceLogSizeTag:Ljava/lang/String;

    const-string v4, "200"

    invoke-virtual {v2, v3, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "200"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v6, :cond_1

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceLogSizeTag:Ljava/lang/String;

    const-string v4, "200"

    invoke-virtual {v2, v3, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v3, v2}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    :cond_1
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mServiceLogSizeTag: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceLogSizeTag:Ljava/lang/String;

    const-string v6, "200"

    invoke-virtual {v2, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox_logsize:Landroid/widget/EditText;

    new-instance v4, Lcom/android/ActivityNetwork/ActivityNetwork$4;

    invoke-direct {v4, p0}, Lcom/android/ActivityNetwork/ActivityNetwork$4;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/ActivityNetwork/ActivityNetwork;->updateUi()V

    invoke-direct {p0}, Lcom/android/ActivityNetwork/ActivityNetwork;->updateUI_RunCommand()V

    return-void

    :catch_0
    move-exception v0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "waitingStopDialog.dismiss "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v4, "build type---eng"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox:Landroid/widget/EditText;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox_logsize:Landroid/widget/EditText;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonCancleCommand:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonRunCommand:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v2, "ActivityNetwork: onDestroy"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/ActivityNetwork/ActivityNetwork;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v2, "onDestroy remove sendEmptyMessageDelayed timer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-ne v1, v3, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->waitingStopDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v2, "onDestroy waitingStopDialog = null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "waitingStopDialog.dismiss "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/ActivityNetwork/ActivityNetwork;->isFinishing()Z

    move-result v1

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v2, "onDestroy ActivityNetwork.this.isFinishing()==true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onPause()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    :try_start_0
    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v2, "onPause unregisterReceiver(mServiceReceiver)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mIntentReceiverService:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v2, "onPause unregisterReceiver(mIntentReceiverService)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregisterReceiver(mServiceReceiver) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregisterReceiver(mIntentReceiverService) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onResume()V
    .locals 4

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.android.ActivityNetwork.SDCARD_DAMAGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "com.android.ActivityNetwork.LOW_STORAGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mIntentReceiverService:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/android/ActivityNetwork/ActivityNetwork;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.android.ActivityNetwork.APLogger_START"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "com.android.ActivityNetwork.APLogger_STOP"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "com.android.ActivityNetwork.APLogger_START_FAIL"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "com.android.ActivityNetwork.APLogger_STOP_FAIL"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/android/ActivityNetwork/ActivityNetwork;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v3, "onResume"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Lcom/android/ActivityNetwork/ActivityNetwork;->updateUi()V

    invoke-direct {p0}, Lcom/android/ActivityNetwork/ActivityNetwork;->updateUI_RunCommand()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v1, "ActivityNetwork: onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public updateUi()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v4}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v0

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStatusTag:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceRunning:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceBootOptionTag:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceOnBootDisabled:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceSync:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceSyncYes:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceSyncNo:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    const-string v5, "UI doesnot sync with prop"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v4, v0}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    move-result v4

    if-ne v4, v6, :cond_1

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_1
    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceLogSizeTag:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogSize:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateUI ....: onResume "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogSize:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mTextbox_logsize:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mLogSize:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_2
    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceRunning:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceOnBootEnabled:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mServiceOnBootDisabled:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStart:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork;->mButtonStop:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
