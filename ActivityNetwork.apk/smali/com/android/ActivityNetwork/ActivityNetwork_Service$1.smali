.class Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;
.super Landroid/content/BroadcastReceiver;
.source "ActivityNetwork_Service.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ActivityNetwork/ActivityNetwork_Service;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v9, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    const-string v6, "ActivityNetwork_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "receive intent: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSDPATH()Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v1

    const-string v6, "storage_volume"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/storage/StorageVolume;

    if-nez v5, :cond_1

    const-string v6, "ActivityNetwork_Service"

    const-string v7, "StorageVolume is null!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v6, ""

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_2
    const-string v6, "ActivityNetwork_Service"

    const-string v7, "Can not get sdcard path!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    if-nez v6, :cond_4

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v8, "storage"

    invoke-virtual {v6, v8}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/storage/StorageManager;

    iput-object v6, v7, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    :cond_4
    const-string v6, "/mnt/sdcard"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    const-string v6, "/storage/sdcard0"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    const-string v6, "/sdcard"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    :cond_5
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "internal_sd_path"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    :cond_6
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "/storage/sdcard0"

    iput-object v7, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    :cond_7
    const-string v6, "ActivityNetwork_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "this intent comes from"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ". log should be saved in internal sdcard,At this time,internal sdcard is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v8, v8, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    const-string v6, "ActivityNetwork_Service"

    const-string v7, "Right sdcard sends right intent "

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    const-string v6, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    const-string v6, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    :cond_8
    const-string v6, "ActivityNetwork_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "runtime receive "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v6

    if-eqz v6, :cond_f

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v6

    const/16 v7, 0xd

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    sput-boolean v9, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mount_sd:Z

    new-instance v3, Landroid/content/Intent;

    const-string v6, "com.android.ActivityNetwork.APLogger_START"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "Return"

    const-string v7, "NoSDCard"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    invoke-virtual {v6, v3}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const v7, 0x1080073

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showNotification(IZ)V
    invoke-static {v6, v7, v9}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$900(Lcom/android/ActivityNetwork/ActivityNetwork_Service;IZ)V

    const-string v6, "ActivityNetwork_Service"

    const-string v7, "sendBroadcast(itCommonUI) Utils.RETURN_NOSD "

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    :goto_2
    const-string v6, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "ActivityNetwork_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "runtime receive "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    sput-boolean v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mount_sd:Z

    const-string v6, "ActivityNetwork_Service"

    const-string v7, "SDCARD has readyyyyyyyyyyy"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v6

    if-nez v6, :cond_10

    const-string v6, "ActivityNetwork_Service"

    const-string v7, "handler == null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_a
    const-string v6, "/data"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    const-string v6, "ActivityNetwork_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Ignore intent: The intent is sent by "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_b
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "external_sd_path"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    if-eqz v6, :cond_c

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    :cond_c
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "/storage/sdcard1"

    iput-object v7, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    :cond_d
    const-string v6, "ActivityNetwork_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "this intent comes from"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".log should be saved in external sdcard, At this time,external sdcard is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v8, v8, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_e
    const-string v6, "ActivityNetwork_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Ignore intent: The intent is sent by "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_f
    const-string v6, "ActivityNetwork_Service"

    const-string v7, "handler == null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_10
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    const-string v6, "ActivityNetwork_Service"

    const-string v7, "Because SD is ready, so continue starting tcpdump"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "true"

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1002(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_11
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "ActivityNetwork_Service"

    const-string v7, "Because SD is ready, so continue stopping tcpdump"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x6

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "true"

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1002(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0
.end method
