.class Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;
.super Landroid/content/BroadcastReceiver;
.source "ActivityNetwork_Service.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ActivityNetwork/ActivityNetwork_Service;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/16 v11, 0xd

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v6, "ActivityNetwork_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "runtime receive intent: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "ActivityNetwork_Service"

    const-string v7, "Low Storage!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "ActivityNetwork_Service"

    const-string v7, "Currnet write to sdcard!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v6, "ActivityNetwork_Service"

    const-string v7, "handler == null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v6, "com.mediatek.filemanager.ACTION_DELETE"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "ActivityNetwork_Service"

    const-string v7, "File Deleted"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v6, "ActivityNetwork_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File Deleted: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSDPATH()Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "/mnt/sdcard"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "/storage/sdcard0"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "/sdcard"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    :cond_3
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "internal_sd_path"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    :cond_4
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "/storage/sdcard0"

    iput-object v7, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v7, v7, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/mtklog/netlog/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x20

    invoke-virtual {v1, v9, v6, v9, v7}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v6

    if-ne v6, v10, :cond_0

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v6

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    new-instance v4, Landroid/content/Intent;

    const-string v6, "com.android.ActivityNetwork.APLogger_STOP"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "Return"

    const-string v7, "Normal"

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    invoke-virtual {v6, v4}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    move-result v6

    if-ne v6, v10, :cond_6

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "Device"

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showStorageFull(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)V

    :cond_6
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const v7, 0x1080073

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showNotification(IZ)V
    invoke-static {v6, v7, v9}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$900(Lcom/android/ActivityNetwork/ActivityNetwork_Service;IZ)V

    goto/16 :goto_0

    :cond_7
    const-string v6, "/data"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/mtklog/netlog/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "external_sd_path"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    :cond_9
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "/storage/sdcard1"

    iput-object v7, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    :cond_a
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v7, v7, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/mtklog/netlog/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_b
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "ActivityNetwork_Service"

    const-string v7, "File Deleted has been processed, stop button is pressed"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
