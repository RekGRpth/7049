.class Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;
.super Landroid/content/BroadcastReceiver;
.source "ActivityNetwork_Service.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ActivityNetwork/ActivityNetwork_Service;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v5, "ActivityNetwork_Service"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mIntentReceiverSWAP receive intent: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "com.mediatek.SD_SWAP"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "ActivityNetwork_Service"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "runtime receive "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v5, v5, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    if-nez v5, :cond_0

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v7, "storage"

    invoke-virtual {v5, v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/storage/StorageManager;

    iput-object v5, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    :cond_0
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSDPATH()Ljava/lang/String;
    invoke-static {v5}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "/mnt/sdcard"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "/storage/sdcard0"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "/sdcard"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_1
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v6, "internal_sd_path"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v5, v5, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v5, v5, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v6, "/storage/sdcard0"

    iput-object v6, v5, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    :cond_3
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v5, v5, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "ActivityNetwork_Service"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mIntentReceiverSWAP log should be saved in internal sdcard. At this time,internal sdcard is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v7, v7, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Mount status:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v5, "mounted"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "ActivityNetwork_Service"

    const-string v6, "SDCARD has readyyyyyyyyyyy"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v5

    if-nez v5, :cond_9

    const-string v5, "ActivityNetwork_Service"

    const-string v6, "handler == null"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_1
    return-void

    :cond_5
    const-string v5, "/data"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v0, "mounted"

    goto :goto_0

    :cond_6
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v6, "external_sd_path"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v5, v5, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v5, v5, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_7
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v6, "/storage/sdcard1"

    iput-object v6, v5, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    :cond_8
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v5, v5, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v6, v6, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "ActivityNetwork_Service"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mIntentReceiverSWAP log should be saved in external sdcard. At this time,external sdcard is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v7, v7, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Mount status:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_9
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v5}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    const-string v5, "ro.build.type"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    const-string v5, "ActivityNetwork_Service"

    const-string v6, "Because SD is ready, so continue starting tcpdump"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v6, "true"

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1002(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    :cond_a
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "ActivityNetwork_Service"

    const-string v6, "Because SD is ready, so continue stopping tcpdump"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v6, "true"

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1002(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1
.end method
