.class Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;
.super Ljava/lang/Thread;
.source "ActivityNetwork_Service.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/ActivityNetwork/ActivityNetwork_Service;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    const/4 v14, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v10}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v9

    const/4 v0, 0x0

    :cond_0
    :goto_0
    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-boolean v10, v10, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->stopflag:Z

    if-nez v10, :cond_6

    const-wide/16 v10, 0x7d0

    :try_start_0
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string v10, "persist.radio.writtingpath"

    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v10, ""

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    if-eqz v8, :cond_0

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v10, "/mtklog/netlog/.*"

    const-string v11, ""

    invoke-virtual {v8, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v10, "/sdcard"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "/mnt/sdcard"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, ""

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    if-nez v2, :cond_2

    :cond_1
    const-string v2, "/storage/sdcard0"

    :cond_2
    const-string v10, "ActivityNetwork_Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "netlog writting folder\'s path: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->IftrueVolume(Ljava/lang/String;)Z
    invoke-static {v10, v2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v10, v10, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v10, v2}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v10, "mounted"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const-string v10, "ActivityNetwork_Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "netlog writting folder\'s path: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "has mounted"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_3

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-ne v10, v14, :cond_4

    :cond_3
    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getAvailableExternalMemorySize()J
    invoke-static {v10}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)J

    move-result-wide v10

    const-wide/16 v12, 0xa

    cmp-long v10, v10, v12

    if-gtz v10, :cond_0

    :cond_4
    const-string v10, "ActivityNetwork_Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "dir:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " has not exist,maybe has been deleted"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static {v10}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v9

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static {v10}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static {v11}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;
    invoke-static {v11}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static {v10}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v11}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v11}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v10

    const/16 v11, 0xd

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    new-instance v7, Landroid/content/Intent;

    const-string v10, "com.android.ActivityNetwork.APLogger_START"

    invoke-direct {v7, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v10, "Return"

    const-string v11, "Unknown"

    invoke-virtual {v7, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    invoke-virtual {v10, v7}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const v11, 0x1080073

    const/4 v12, 0x0

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showNotification(IZ)V
    invoke-static {v10, v11, v12}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$900(Lcom/android/ActivityNetwork/ActivityNetwork_Service;IZ)V

    move-object v4, v8

    goto/16 :goto_0

    :catch_0
    move-exception v6

    const-string v10, "ActivityNetwork_Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "thread "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_5
    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->StoPressed:Z
    invoke-static {v10}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Z

    move-result v10

    if-ne v10, v14, :cond_0

    const-string v10, "ActivityNetwork_Service"

    const-string v11, "File Deleted has been processed, stop button is pressed"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    return-void
.end method
