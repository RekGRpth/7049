.class Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;
.super Landroid/os/Handler;
.source "ActivityNetwork_Service.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ActivityNetwork/ActivityNetwork_Service;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;


# direct methods
.method constructor <init>(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 22
    .param p1    # Landroid/os/Message;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v15

    new-instance v12, Landroid/content/Intent;

    const-string v17, "com.mediatek.syslogger.action"

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v17, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Aservice:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    if-nez v17, :cond_1

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "Unhandle message %d\n"

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "receive message type is "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    const/16 v18, 0x4

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1b

    const-string v17, "From"

    const-string v18, "ActivityNetworkLog"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v17, "To"

    const-string v18, "CommonUI"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v17, "Command"

    const-string v18, "Start"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "send intent to commonUI: readConfig() sucess"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v16, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    move-object/from16 v17, v0

    if-nez v17, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    new-instance v18, Lcom/android/ActivityNetwork/NlConnection;

    const-string v19, "netdiag"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v20, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v20

    invoke-direct/range {v18 .. v20}, Lcom/android/ActivityNetwork/NlConnection;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1502(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Lcom/android/ActivityNetwork/NlConnection;)Lcom/android/ActivityNetwork/NlConnection;

    :goto_1
    if-eqz v16, :cond_3

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/android/ActivityNetwork/NlConnection;->connect()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v17

    if-eqz v17, :cond_5

    :cond_3
    :goto_2
    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mNlConnection "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v19, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v19 .. v19}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v16, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    if-nez v17, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "Device"

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showStorageFull(Ljava/lang/String;)V
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)V

    :cond_4
    const-string v17, "ActivityNetwork_Service"

    const-string v18, "Unable connect to activitynetwork, exit"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v17, "Return"

    const-string v18, "DaemonUnable"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    new-instance v3, Landroid/content/Intent;

    const-string v17, "com.android.ActivityNetwork.APLogger_START_FAIL"

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "send intent to commonUI: Connect to Daemon error!"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    const-wide/16 v17, 0x7d0

    :try_start_1
    invoke-static/range {v17 .. v18}, Ljava/lang/Thread;->sleep(J)V

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "Connection retry"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v16, v16, -0x1

    goto/16 :goto_1

    :catch_0
    move-exception v10

    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "thread "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    move-object/from16 v17, v0

    if-nez v17, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v19, "storage"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/os/storage/StorageManager;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSDPATH()Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v8

    const-string v17, "/mnt/sdcard"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_8

    const-string v17, "/storage/sdcard0"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_8

    const-string v17, "/sdcard"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "internal_sd_path"

    invoke-static/range {v18 .. v18}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v17, v0

    if-eqz v17, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "/storage/sdcard0"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "MSG_INIT log should be saved in internal sdcard. At this time,internal sdcard is "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "Mount status:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    const-string v17, "mounted"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_f

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "No SD Card"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v17, "Return"

    const-string v18, "NoSDCard"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "send intent to commonUI: NoSDCard"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v17, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->sendEmptyMessage(I)Z

    new-instance v3, Landroid/content/Intent;

    const-string v17, "com.android.ActivityNetwork.APLogger_START"

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v17, "Return"

    const-string v18, "NoSDCard"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_b
    const-string v17, "/data"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    const-string v7, "mounted"

    goto :goto_3

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "external_sd_path"

    invoke-static/range {v18 .. v18}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v17, v0

    if-eqz v17, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "/storage/sdcard1"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "MSG_INIT log should be saved in external sdcard. At this time,external sdcard is "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "Mount status:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceLogSizeTag:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    const-string v19, "200"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1602(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    const-string v18, "0"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_11

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "200"

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1602(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Ljava/lang/String;

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->i:I

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->i:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->i:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Character;->isDigit(C)Z

    move-result v17

    if-nez v17, :cond_15

    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "text has not only digital numbers but other "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v19, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->i:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->charAt(I)C

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->i:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "200"

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1602(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-wide/16 v18, 0xc8

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->log_size_lng:J
    invoke-static/range {v17 .. v19}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1802(Lcom/android/ActivityNetwork/ActivityNetwork_Service;J)J

    :goto_5
    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Log size "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v19, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "parse long:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v19, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->log_size_lng:J
    invoke-static/range {v19 .. v19}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getAvailableExternalMemorySize()J
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)J

    move-result-wide v17

    const-wide/16 v19, 0xa

    cmp-long v17, v17, v19

    if-lez v17, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getAvailableExternalMemorySize()J
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)J

    move-result-wide v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v19, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->log_size_lng:J
    invoke-static/range {v19 .. v19}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)J

    move-result-wide v19

    cmp-long v17, v17, v19

    if-gtz v17, :cond_17

    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "Device"

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showStorageFull(Ljava/lang/String;)V
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)V

    :cond_14
    const-string v17, "ActivityNetwork_Service"

    const-string v18, "No Enough Space on SD card"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v17, "Return"

    const-string v18, "SDCardFull"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "send intent to commonUI: No Enough Space on SD card"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "SDcard"

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showStorageFull(Ljava/lang/String;)V
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)V

    const/16 v17, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->sendEmptyMessage(I)Z

    new-instance v3, Landroid/content/Intent;

    const-string v17, "com.android.ActivityNetwork.APLogger_START_FAIL"

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v17, "Return"

    const-string v18, "SDCardFull"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->i:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->i:I

    goto/16 :goto_4

    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->log_size_lng:J
    invoke-static/range {v17 .. v19}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1802(Lcom/android/ActivityNetwork/ActivityNetwork_Service;J)J

    goto/16 :goto_5

    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    const-string v18, "true"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "Boot up!"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v17, "mounted"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1a

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "sdcard is already mounted,send tcpdump_sdcard_start"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    if-eqz v17, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    move-object/from16 v17, v0

    if-eqz v17, :cond_18

    :try_start_2
    const-string v17, "/data"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "tcpdump_data_start_"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v19, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_18
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "false"

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1002(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_19
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "tcpdump_sdcard_start_"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v19, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_6

    :catch_1
    move-exception v10

    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "thread mNlConnection.sendCmd(command9)"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :cond_1a
    const-string v17, "ActivityNetwork_Service"

    const-string v18, "sdcard is not ready, waiting..."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v17, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->sendEmptyMessage(I)Z

    new-instance v3, Landroid/content/Intent;

    const-string v17, "com.android.ActivityNetwork.APLogger_START"

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v17, "Return"

    const-string v18, "SDCardFull"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_6

    :cond_1b
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/android/ActivityNetwork/NlConnection;->stop()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    :goto_7
    const-string v17, "ActivityNetwork_Service"

    const-string v18, "die mNlConnection == null"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_2
    move-exception v9

    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mNlConnection stop failed: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :cond_1c
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    const/16 v18, 0x5

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->updateServiceStatus(Z)I

    new-instance v4, Landroid/content/Intent;

    const-string v17, "com.android.ActivityNetwork.APLogger_START_FAIL"

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_1d
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    const/16 v18, 0x9

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->updateServiceStatus(Z)I

    goto/16 :goto_0

    :cond_1e
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    const/16 v18, 0xd

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_20

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "check tcpdump running!"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    if-eqz v17, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1f

    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    const-string v18, "tcpdump_sdcard_check"

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v10

    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "thread mNlConnection.sendCmd(command11)"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1f
    const-string v17, "ActivityNetwork_Service"

    const-string v18, "check mNlConnection == null"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v17, 0x88

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_20
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    const/16 v18, 0x6

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_26

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "stop!"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSDPATH()Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v8

    const/16 v16, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    if-eqz v17, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    move-object/from16 v17, v0

    if-nez v17, :cond_22

    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    new-instance v18, Lcom/android/ActivityNetwork/NlConnection;

    const-string v19, "netdiag"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v20, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;

    move-result-object v20

    invoke-direct/range {v18 .. v20}, Lcom/android/ActivityNetwork/NlConnection;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1502(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Lcom/android/ActivityNetwork/NlConnection;)Lcom/android/ActivityNetwork/NlConnection;

    :goto_8
    if-eqz v16, :cond_22

    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/android/ActivityNetwork/NlConnection;->connect()Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    move-result v17

    if-eqz v17, :cond_23

    :cond_22
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    if-eqz v17, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    move-object/from16 v17, v0

    if-eqz v17, :cond_25

    :try_start_7
    const-string v17, "/data"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    const-string v18, "tcpdump_data_stop"

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v10

    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "thread mNlConnection.sendCmd(command10)"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_23
    const-wide/16 v17, 0x7d0

    :try_start_8
    invoke-static/range {v17 .. v18}, Ljava/lang/Thread;->sleep(J)V

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "Connection retry"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    add-int/lit8 v16, v16, -0x1

    goto :goto_8

    :catch_5
    move-exception v10

    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "thread "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9

    :cond_24
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    const-string v18, "tcpdump_sdcard_stop"

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_0

    :cond_25
    const-string v17, "ActivityNetwork_Service"

    const-string v18, "stop mNlConnection == null"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v17, 0x88

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_26
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    const/16 v18, 0xb

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v15

    new-instance v11, Landroid/content/Intent;

    const-string v17, "com.android.ActivityNetwork.LOW_STORAGE"

    move-object/from16 v0, v17

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v17, "msg"

    const-string v18, "Low Storage, activitynetwork service stop"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "lwo storage,send intent"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "Device"

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showStorageFull(Ljava/lang/String;)V
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)V

    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSDPATH()Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    :try_start_a
    const-string v17, "/data"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    const-string v18, "tcpdump_data_stop"

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    :goto_a
    new-instance v3, Landroid/content/Intent;

    const-string v17, "com.android.ActivityNetwork.APLogger_START_FAIL"

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_28
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    const-string v18, "tcpdump_sdcard_stop"

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    goto :goto_a

    :catch_6
    move-exception v10

    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "thread mNlConnection.sendCmd(command10)"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    :cond_29
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    const/16 v18, 0xe

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;

    move-result-object v17

    const-string v18, "tcpdump_sdcard_tag"

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_7

    goto/16 :goto_0

    :catch_7
    move-exception v10

    const-string v17, "ActivityNetwork_Service"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "thread mNlConnection.sendCmd(command10)"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_2a
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    const/16 v18, 0x20

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_3a

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x4

    if-lez v17, :cond_2b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Tcpdump_title:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1900(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    :goto_b
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0x80

    move/from16 v17, v0

    if-lez v17, :cond_2f

    const-string v5, "OK!"

    :goto_c
    const-string v17, "OK!"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_32

    new-instance v4, Landroid/content/Intent;

    const-string v17, "com.android.ActivityNetwork.APLogger_START_FAIL"

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_2b
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x8

    if-lez v17, :cond_2c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Stop_tcpdump_title:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    goto :goto_b

    :cond_2c
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x20

    if-lez v17, :cond_2d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Tag_title:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    goto :goto_b

    :cond_2d
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x40

    if-lez v17, :cond_2e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Check_stop_tcpdump_title:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v6

    goto :goto_b

    :cond_2e
    const-string v6, "other"

    goto :goto_b

    :cond_2f
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0x100

    move/from16 v17, v0

    if-lez v17, :cond_30

    const-string v5, "some error has happened!"

    goto :goto_c

    :cond_30
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0x200

    move/from16 v17, v0

    if-lez v17, :cond_31

    const-string v5, "there is no deamon pid running!"

    goto/16 :goto_c

    :cond_31
    const-string v5, "some error has happened!"

    goto/16 :goto_c

    :cond_32
    const-string v17, "From"

    const-string v18, "ActivityNetworkLog"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v17, "To"

    const-string v18, "CommonUI"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "layout_inflater"

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/view/LayoutInflater;

    const v17, 0x7f020001

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v14, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_33

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "R.layout.main"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    const v17, 0x7f050008

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/Button;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStart:Landroid/widget/Button;
    invoke-static {v0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2302(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Landroid/widget/Button;)Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    const v17, 0x7f05000a

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/Button;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStop:Landroid/widget/Button;
    invoke-static {v0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2402(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Landroid/widget/Button;)Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Tcpdump_title:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1900(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->updateUItoStart(Z)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_34

    new-instance v2, Landroid/content/Intent;

    const-string v17, "com.android.ActivityNetwork.APLogger_STOP"

    move-object/from16 v0, v17

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v17, "Return"

    const-string v18, "NandFull"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v17, "Command"

    const-string v18, "Stop"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v17, "Return"

    const-string v18, "NandFull"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "sendBroadcast(itCommonUI)  STOP"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "send intent to ActivityNetwork: ACTION_ACTIVITY_STOP"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->updateServiceStatus(Z)I

    goto/16 :goto_0

    :cond_34
    new-instance v2, Landroid/content/Intent;

    const-string v17, "com.android.ActivityNetwork.APLogger_START"

    move-object/from16 v0, v17

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v17, "Return"

    const-string v18, "Normal"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v17, "Command"

    const-string v18, "Start"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v17, "Return"

    const-string v18, "Normal"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "sendBroadcast(itCommonUI)  "

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "send intent to ActivityNetwork: ACTION_ACTIVITY_START"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->updateServiceStatus(Z)I

    goto/16 :goto_0

    :cond_35
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Stop_tcpdump_title:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_37

    new-instance v2, Landroid/content/Intent;

    const-string v17, "com.android.ActivityNetwork.APLogger_STOP"

    move-object/from16 v0, v17

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->updateUItoStart(Z)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_36

    const-string v17, "Command"

    const-string v18, "SDCardFull"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v17, "Return"

    const-string v18, "SDCardFull"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "send intent to ActivityNetwork: ACTION_ACTIVITY_STOP"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->updateServiceStatus(Z)I

    const-string v17, "Return"

    const-string v18, "Stop"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "sendBroadcast(itCommonUI)  "

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_36
    const-string v17, "Command"

    const-string v18, "Normal"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v17, "Return"

    const-string v18, "Normal"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_d

    :cond_37
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Check_stop_tcpdump_title:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStart:Landroid/widget/Button;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/widget/Button;

    move-result-object v17

    if-eqz v17, :cond_39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStop:Landroid/widget/Button;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/widget/Button;

    move-result-object v17

    if-eqz v17, :cond_39

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "Check_stop_tcpdump_title OK  "

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStart:Landroid/widget/Button;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/widget/Button;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/Button;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStop:Landroid/widget/Button;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/widget/Button;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/Button;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    # setter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->StoPressed:Z
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1402(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Z)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceSync:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v18, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceSyncNo:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$2600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    # getter for: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;
    invoke-static/range {v17 .. v17}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_38

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const-string v18, "Device"

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showStorageFull(Ljava/lang/String;)V
    invoke-static/range {v17 .. v18}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$1100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)V

    :cond_38
    const-string v17, "ActivityNetwork_Service"

    const-string v18, "Check_stop_tcpdump_title mButtonStart.setEnabled(true)  "

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_39
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;->this$0:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    move-object/from16 v17, v0

    const v18, 0x1080073

    const/16 v19, 0x0

    # invokes: Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showNotification(IZ)V
    invoke-static/range {v17 .. v19}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->access$900(Lcom/android/ActivityNetwork/ActivityNetwork_Service;IZ)V

    const-string v17, "ActivityNetwork_Service"

    const-string v18, "MSG_CHECK return successfully "

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3a
    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto/16 :goto_0
.end method
