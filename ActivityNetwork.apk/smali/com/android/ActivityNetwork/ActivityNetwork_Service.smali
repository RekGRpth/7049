.class public Lcom/android/ActivityNetwork/ActivityNetwork_Service;
.super Landroid/app/Service;
.source "ActivityNetwork_Service.java"


# static fields
.field protected static Aservice:Lcom/android/ActivityNetwork/ActivityNetwork_Service; = null

.field public static final BROADCAST_KEY_FOLDERPATH:Ljava/lang/String; = ""

.field private static final DBG:Z = true

.field public static final EXTRA_COMMON_UI:Ljava/lang/String; = "com.meidatek.syslogger.action"

.field public static final EXTRA_Device_Bootup:Ljava/lang/String; = "com.android.ActivityNetwork.DEVICEBOOTUP"

.field public static final EXTRA_FILESIZE:Ljava/lang/String; = "com.android.ActivityNetwork.LOGSIZE"

.field public static final EXTRA_LOGPATH:Ljava/lang/String; = "com.android.ActivityNetwork.LOGPATH"

.field public static final EXTRA_LOGPATHTYPE:Ljava/lang/String; = "com.android.ActivityNetwork.LOGPATHTYPE"

.field public static final EXTRA_Update_UI:Ljava/lang/String; = "com.android.ActivityNetwork.Update_UI"

.field public static final FILE_DELETE:Ljava/lang/String; = "com.mediatek.filemanager.ACTION_DELETE"

.field public static final FILE_TAG:Ljava/lang/String; = "com.mediatek.log2server.EXCEPTION_HAPPEND"

.field public static final INTENT_SD_SWAP:Ljava/lang/String; = "com.mediatek.SD_SWAP"

.field public static final LOW_STORAGE:Ljava/lang/String; = "com.android.ActivityNetwork.LOW_STORAGE"

.field public static final MAX_LOG_SIZE:I = 0x400

.field private static final MSG_ALIVE:I = 0x9

.field private static final MSG_CHECK:I = 0xd

.field private static final MSG_CLEARLOG:I = 0x1

.field private static final MSG_CONN_FAIL:I = 0x5

.field private static final MSG_DIE:I = 0x3

.field private static final MSG_INIT:I = 0x4

.field private static final MSG_LOW:I = 0xb

.field private static final MSG_REJECT:I = 0xa

.field private static final MSG_RUNCOMMAND:I = 0x2

.field private static final MSG_SD:I = 0x7

.field private static final MSG_STOP:I = 0x6

.field private static final MSG_STOPCOMMAND:I = 0x8

.field private static final MSG_STOPCOMMAND_SUCCESS:I = 0x88

.field private static final MSG_STOPDIAG:I = 0xc

.field private static final MSG_STOP_TCPDUMP_COMMAND:I = 0x10

.field private static final MSG_TAG:I = 0xe

.field private static MSG_TYPE:I = 0x0

.field public static final SDCARD_DAMAGE:Ljava/lang/String; = "com.android.ActivityNetwork.SDCARD_DAMAGE"

.field public static final SD_EXIST:Ljava/lang/String; = "SD_EXIST"

.field static final SERVICE_START_ACTION:Ljava/lang/String; = "com.android.ActivityNetwork.ActivityNetwork_Service"

.field private static final TAG:Ljava/lang/String; = "ActivityNetwork_Service"

.field static mount_sd:Z


# instance fields
.field private Check_stop_tcpdump_title:Ljava/lang/String;

.field private Clear_log_title:Ljava/lang/String;

.field private Run_command_title:Ljava/lang/String;

.field SDpath_SWAP:Ljava/lang/String;

.field private StoPressed:Z

.field private Stop_command_title:Ljava/lang/String;

.field private Stop_tcpdump_title:Ljava/lang/String;

.field private Tag_title:Ljava/lang/String;

.field private Tcpdump_title:Ljava/lang/String;

.field final command10:Ljava/lang/String;

.field final command11:Ljava/lang/String;

.field final command14:Ljava/lang/String;

.field private command15:Ljava/lang/String;

.field private command16:Ljava/lang/String;

.field private command17:Ljava/lang/String;

.field final command7:Ljava/lang/String;

.field final command8:Ljava/lang/String;

.field final command9:Ljava/lang/String;

.field private curWriteToPhone:Z

.field extStorageManager:Landroid/os/storage/StorageManager;

.field i:I

.field private isBootup:Ljava/lang/String;

.field private isButtonEnable:Ljava/lang/String;

.field private log_size:Ljava/lang/String;

.field private log_size_lng:J

.field private mButtonStart:Landroid/widget/Button;

.field private mButtonStop:Landroid/widget/Button;

.field mFile:Ljava/io/File;

.field private final mHandler:Landroid/os/Handler;

.field private mIntentFilterSDCard:Landroid/content/IntentFilter;

.field private mIntentFilterSWAP:Landroid/content/IntentFilter;

.field private mIntentFilterStorage:Landroid/content/IntentFilter;

.field private mIntentReceiverSDCard:Landroid/content/BroadcastReceiver;

.field private mIntentReceiverSWAP:Landroid/content/BroadcastReceiver;

.field private mIntentReceiverStorage:Landroid/content/BroadcastReceiver;

.field private mLogPathPhone:Ljava/lang/String;

.field private mLogPathSdCard:Ljava/lang/String;

.field private mLogPathTag:Ljava/lang/String;

.field private mLogSize:Ljava/lang/String;

.field private mNM:Landroid/app/NotificationManager;

.field private mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

.field private mNetworkSetting_config_name:Ljava/lang/String;

.field private mNetworkSetting_config_path:Ljava/lang/String;

.field private mNetworkSetting_path:Ljava/lang/String;

.field private mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

.field private mServiceBootOptionTag:Ljava/lang/String;

.field private mServiceLogSizeTag:Ljava/lang/String;

.field private mServiceOnBootDisabled:Ljava/lang/String;

.field private mServiceOnBootEnabled:Ljava/lang/String;

.field private mServiceRunning:Ljava/lang/String;

.field private mServiceStatusTag:Ljava/lang/String;

.field private mServiceStopped:Ljava/lang/String;

.field private mServiceSync:Ljava/lang/String;

.field private mServiceSyncNo:Ljava/lang/String;

.field private mServiceSyncYes:Ljava/lang/String;

.field private mStatusOn:Z

.field mdir:Ljava/io/File;

.field mfileInputStream:Ljava/io/FileInputStream;

.field private mfilemonitorThread:Ljava/lang/Thread;

.field final network_service:Ljava/lang/String;

.field private notify_id:I

.field prop_logpath:Ljava/lang/String;

.field properties_config:Ljava/util/Properties;

.field stopflag:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->MSG_TYPE:I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mount_sd:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mfilemonitorThread:Ljava/lang/Thread;

    iput-boolean v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mStatusOn:Z

    const-string v0, "netdiag"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->network_service:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->log_size_lng:J

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;

    const-string v0, "/data/data/com.android.ActivityNetwork/files"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_path:Ljava/lang/String;

    const-string v0, "service_boot_option"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;

    const-string v0, "true"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;

    const-string v0, "false"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;

    const-string v0, "log_path"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogPathTag:Ljava/lang/String;

    const-string v0, "/data"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogPathPhone:Ljava/lang/String;

    const-string v0, "/sdcard"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogPathSdCard:Ljava/lang/String;

    const-string v0, "service_status"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;

    const-string v0, "service_running"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;

    const-string v0, "service_stopped"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;

    const-string v0, "UI_prop_sync"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceSync:Ljava/lang/String;

    const-string v0, "yes"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceSyncYes:Ljava/lang/String;

    const-string v0, "no"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceSyncNo:Ljava/lang/String;

    const-string v0, "input_log_size"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceLogSizeTag:Ljava/lang/String;

    const-string v0, "0"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->log_size:Ljava/lang/String;

    const-string v0, "false"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;

    const-string v0, "false"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isButtonEnable:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->StoPressed:Z

    const v0, 0xd431

    iput v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->notify_id:I

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    iput-boolean v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->curWriteToPhone:Z

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    const-string v0, "tcpdump_data_start"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->command7:Ljava/lang/String;

    const-string v0, "tcpdump_data_stop"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->command8:Ljava/lang/String;

    const-string v0, "tcpdump_sdcard_start"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->command9:Ljava/lang/String;

    const-string v0, "tcpdump_sdcard_stop"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->command10:Ljava/lang/String;

    const-string v0, "tcpdump_sdcard_check"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->command11:Ljava/lang/String;

    const-string v0, "tcpdump_sdcard_tag"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->command14:Ljava/lang/String;

    const-string v0, "clear_data_start"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->command15:Ljava/lang/String;

    const-string v0, "clear_sdcard_start"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->command16:Ljava/lang/String;

    const-string v0, "kill_data_start"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->command17:Ljava/lang/String;

    const-string v0, "Clear log:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Clear_log_title:Ljava/lang/String;

    const-string v0, "Run command:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Run_command_title:Ljava/lang/String;

    const-string v0, "Stop command:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Stop_command_title:Ljava/lang/String;

    const-string v0, "Stop tcpdump_check:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Check_stop_tcpdump_title:Ljava/lang/String;

    const-string v0, "Tcpdump:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Tcpdump_title:Ljava/lang/String;

    const-string v0, "Stop tcpdump:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Stop_tcpdump_title:Ljava/lang/String;

    const-string v0, "Tag log:"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Tag_title:Ljava/lang/String;

    const-string v0, "/system/etc"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_config_path:Ljava/lang/String;

    const-string v0, "mtklog-config.prop"

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_config_name:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_config_path:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mdir:Ljava/io/File;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_config_path:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_config_name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mFile:Ljava/io/File;

    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->properties_config:Ljava/util/Properties;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mfileInputStream:Ljava/io/FileInputStream;

    iput-boolean v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->stopflag:Z

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service$1;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentReceiverSDCard:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service$2;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentReceiverStorage:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service$3;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentReceiverSWAP:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;

    invoke-direct {v0, p0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service$5;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private IftrueVolume(Ljava/lang/String;)Z
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v6}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    array-length v0, v4

    new-array v3, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_0

    aget-object v6, v4, v2

    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v2

    aget-object v6, v3, v2

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v6, "ActivityNetwork_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NetworkSetting;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    invoke-direct {p0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSDPATH()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showStorageFull(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->IftrueVolume(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)J
    .locals 2
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    invoke-direct {p0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getAvailableExternalMemorySize()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Z
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-boolean v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->StoPressed:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Z)Z
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->StoPressed:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Lcom/android/ActivityNetwork/NlConnection;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Lcom/android/ActivityNetwork/NlConnection;)Lcom/android/ActivityNetwork/NlConnection;
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;
    .param p1    # Lcom/android/ActivityNetwork/NlConnection;

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogSize:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceLogSizeTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)J
    .locals 2
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-wide v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->log_size_lng:J

    return-wide v0
.end method

.method static synthetic access$1802(Lcom/android/ActivityNetwork/ActivityNetwork_Service;J)J
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->log_size_lng:J

    return-wide p1
.end method

.method static synthetic access$1900(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Tcpdump_title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Stop_tcpdump_title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Tag_title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Check_stop_tcpdump_title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStart:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStop:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/android/ActivityNetwork/ActivityNetwork_Service;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStop:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceSync:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceSyncNo:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    iget-object v0, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/ActivityNetwork/ActivityNetwork_Service;IZ)V
    .locals 0
    .param p0    # Lcom/android/ActivityNetwork/ActivityNetwork_Service;
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showNotification(IZ)V

    return-void
.end method

.method private getAvailableExternalMemorySize()J
    .locals 17

    const/4 v1, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v11}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    if-nez v11, :cond_0

    const-string v11, "storage"

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/storage/StorageManager;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSDPATH()Ljava/lang/String;

    move-result-object v2

    const-string v11, "/sdcard"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    const-string v11, "/mnt/sdcard"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    const-string v11, "/storage/sdcard0"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    const-string v11, "/sdcard"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    :cond_1
    const-string v11, "internal_sd_path"

    invoke-static {v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    const-string v11, ""

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    :cond_2
    const-string v7, "/storage/sdcard0"

    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v11, v7}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v11, "ActivityNetwork_Service"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAvailableExternalMemorySize log should be saved in internal sdcard. At this time,internal sdcard is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Mount status:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    if-nez v1, :cond_4

    const-string v1, "mounted"

    :cond_4
    const-string v11, "mounted"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    new-instance v10, Landroid/os/StatFs;

    invoke-direct {v10, v7}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Landroid/os/StatFs;->getBlockSize()I

    move-result v11

    int-to-long v5, v11

    invoke-virtual {v10}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v11

    int-to-long v3, v11

    const-string v11, "ActivityNetwork_Service"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "sdcard blocks "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    mul-long v13, v3, v5

    const-wide/32 v15, 0x100000

    div-long/2addr v13, v15

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "M"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    mul-long v11, v3, v5

    const-wide/32 v13, 0x100000

    div-long/2addr v11, v13

    :goto_1
    return-wide v11

    :cond_5
    const-string v11, "/data"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v8

    new-instance v10, Landroid/os/StatFs;

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Landroid/os/StatFs;->getBlockSize()I

    move-result v11

    int-to-long v5, v11

    invoke-virtual {v10}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v11

    int-to-long v3, v11

    const-string v11, "ActivityNetwork_Service"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAvailableExternalMemorySize /data log is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    mul-long v13, v3, v5

    const-wide/32 v15, 0x100000

    div-long/2addr v13, v15

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    mul-long v11, v3, v5

    const-wide/32 v13, 0x100000

    div-long/2addr v11, v13

    goto :goto_1

    :cond_6
    const-string v11, "external_sd_path"

    invoke-static {v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_7

    const-string v11, ""

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    :cond_7
    const-string v7, "/storage/sdcard1"

    :cond_8
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v11, v7}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v11, "ActivityNetwork_Service"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAvailableExternalMemorySize log should be saved in external sdcard. At this time,external sdcard is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Mount status:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_9
    const-wide/16 v11, 0x0

    goto/16 :goto_1
.end method

.method private getSDPATH()Ljava/lang/String;
    .locals 13

    new-instance v3, Ljava/io/File;

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_config_path:Ljava/lang/String;

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_config_path:Ljava/lang/String;

    iget-object v11, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_config_name:Ljava/lang/String;

    invoke-direct {v2, v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, Ljava/util/Properties;

    invoke-direct {v9}, Ljava/util/Properties;-><init>()V

    const/4 v4, 0x0

    const/4 v6, 0x0

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v10}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v7

    const-string v10, "persist.radio.log2sd.path"

    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v10, ""

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_2

    const-string v0, "/storage/sdcard0"

    const-string v10, "ActivityNetwork_Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "dir:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_config_path:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "does not exist"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    const-string v10, "/data"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogPathTag:Ljava/lang/String;

    iget-object v11, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogPathPhone:Ljava/lang/String;

    invoke-virtual {v7, v10, v11}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v10, v7}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    return-object v0

    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_3

    const-string v0, "/storage/sdcard0"

    const-string v10, "ActivityNetwork_Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "file:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_config_name:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "does not exist"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    new-instance v9, Ljava/util/Properties;

    invoke-direct {v9}, Ljava/util/Properties;-><init>()V

    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v9, v5}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v4, v5

    :goto_2
    const-string v10, "persist.radio.log2sd.path"

    const-string v11, "/storage/sdcard0"

    invoke-virtual {v9, v10, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v10, "ActivityNetwork_Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mtklog-config.prop log path:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v8

    goto :goto_0

    :catch_0
    move-exception v1

    :goto_3
    const-string v10, "ActivityNetwork_Service"

    const-string v11, "mtklog-config.prop loadconfig"

    invoke-static {v10, v11, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_4
    iget-object v10, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mLogPathTag:Ljava/lang/String;

    invoke-virtual {v7, v10, v0}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :catch_1
    move-exception v1

    move-object v4, v5

    goto :goto_3
.end method

.method private showNotification(IZ)V
    .locals 8
    .param p1    # I
    .param p2    # Z

    const v6, 0x7f040019

    const/4 v7, 0x0

    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    iput-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    if-eqz p2, :cond_2

    invoke-virtual {p0, v6}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v2, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v2, p1, v3, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iget v4, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v4, v4, 0x20

    iput v4, v2, Landroid/app/Notification;->flags:I

    const/4 v1, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v4, Landroid/content/ComponentName;

    const-string v5, "com.mediatek.engineermode"

    const-string v6, "com.mediatek.engineermode.syslogger.SysLogger"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v0, v7}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {p0, v7, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    :goto_0
    const v4, 0x7f040018

    invoke-virtual {p0, v4}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, p0, v4, v3, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v4, v7, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/android/ActivityNetwork/ActivityNetwork;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v7, v4, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    goto :goto_0

    :cond_2
    const-string v4, "ActivityNetwork_Service"

    const-string v5, "cancel(R.string.activitynetworklog_service_start)"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v4, v7}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v4, v6}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    const v5, 0x7f04001a

    invoke-virtual {v4, v5}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v4}, Landroid/app/NotificationManager;->cancelAll()V

    goto :goto_1
.end method

.method private showStorageFull(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;

    const/16 v4, 0xd

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.android.ActivityNetwork.APLogger_STOP"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "SDcard"

    if-ne p1, v3, :cond_6

    const-string v3, "ActivityNetwork_Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Storage:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " has no space!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "Return"

    const-string v4, "SDCardFull"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v3, "ActivityNetwork_Service"

    const-string v4, "cancel(R.string.activitynetworklog_service_start)"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStart:Landroid/widget/Button;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStop:Landroid/widget/Button;

    if-nez v3, :cond_3

    :cond_1
    const-string v3, "layout_inflater"

    invoke-virtual {p0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    const v3, 0x7f020001

    invoke-virtual {v2, v3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "ActivityNetwork_Service"

    const-string v4, "showStorageFull R.layout.main"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const v3, 0x7f050008

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStart:Landroid/widget/Button;

    const v3, 0x7f05000a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStop:Landroid/widget/Button;

    :cond_3
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStart:Landroid/widget/Button;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStop:Landroid/widget/Button;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStart:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStop:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    iput-boolean v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->StoPressed:Z

    :cond_4
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v3, v6}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    const v4, 0x7f040019

    invoke-virtual {v3, v4}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    const v4, 0x7f04001a

    invoke-virtual {v3, v4}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v3}, Landroid/app/NotificationManager;->cancelAll()V

    :cond_5
    return-void

    :cond_6
    const-string v3, "ActivityNetwork_Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Storage:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " has no space!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "Return"

    const-string v4, "NandFull"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    const-string v1, "ActivityNetwork_Service"

    const-string v2, "ActivityNetwork_Service onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sput-object p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Aservice:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    new-instance v1, Lcom/android/ActivityNetwork/NetworkSetting;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_path:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/android/ActivityNetwork/NetworkSetting;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v2, "com.mediatek.SD_SWAP"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v2, "file"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentReceiverSDCard:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    invoke-virtual {p0, v1, v2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterStorage:Landroid/content/IntentFilter;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterStorage:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterStorage:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterStorage:Landroid/content/IntentFilter;

    const-string v2, "com.mediatek.filemanager.ACTION_DELETE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterStorage:Landroid/content/IntentFilter;

    const-string v2, "file"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentReceiverStorage:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterStorage:Landroid/content/IntentFilter;

    invoke-virtual {p0, v1, v2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSWAP:Landroid/content/IntentFilter;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSWAP:Landroid/content/IntentFilter;

    const-string v2, "com.mediatek.SD_SWAP"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentReceiverSWAP:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentFilterSWAP:Landroid/content/IntentFilter;

    invoke-virtual {p0, v1, v2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v1, "ro.build.type"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;

    invoke-direct {v1, p0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service$4;-><init>(Lcom/android/ActivityNetwork/ActivityNetwork_Service;)V

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mfilemonitorThread:Ljava/lang/Thread;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mfilemonitorThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_0
    const-string v1, "ActivityNetwork_Service"

    const-string v2, "Start mfilemonitorThread over"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDestroy()V
    .locals 4

    const-string v1, "ActivityNetwork_Service"

    const-string v2, "ActivityNetwork_Service onDestroy"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentReceiverSDCard:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentReceiverStorage:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mIntentReceiverSWAP:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->stopflag:Z

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    iget-object v1, v1, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    const-string v2, "tcpdump_sdcard_check"

    invoke-virtual {v1, v2}, Lcom/android/ActivityNetwork/NlConnection;->sendCmd(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-wide/16 v1, 0x64

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const-string v1, "ActivityNetwork_Service"

    const-string v2, "cancel(R.string.activitynetworklog_service_start)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    const v2, 0x7f040019

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v1}, Landroid/app/NotificationManager;->cancelAll()V

    const/4 v1, 0x0

    sput-object v1, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->Aservice:Lcom/android/ActivityNetwork/ActivityNetwork_Service;

    return-void

    :catch_0
    move-exception v0

    const-string v1, "ActivityNetwork_Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " mNlConnection.sendCmd(command10)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "ActivityNetwork_Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "thread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_0
    const-string v1, "ActivityNetwork_Service"

    const-string v2, "stop mNlConnection == null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v8, 0x1

    const-string v5, "ActivityNetwork_Service"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onStartCommand "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " flags "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_4

    const-string v5, "false"

    iput-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;

    const-string v5, "false"

    iput-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isButtonEnable:Ljava/lang/String;

    :goto_0
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    iget-object v5, v5, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    if-nez v5, :cond_1

    :cond_0
    new-instance v5, Lcom/android/ActivityNetwork/NlConnection;

    const-string v6, "netdiag"

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;

    invoke-direct {v5, v6, v7}, Lcom/android/ActivityNetwork/NlConnection;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    iput-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    :goto_1
    if-eqz v4, :cond_1

    :try_start_0
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    invoke-virtual {v5}, Lcom/android/ActivityNetwork/NlConnection;->connect()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_8

    :cond_1
    :goto_2
    const-string v5, "ActivityNetwork_Service"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mNlConnection "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    iget-object v7, v7, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isButtonEnable:Ljava/lang/String;

    const-string v6, "true"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    new-instance v2, Landroid/content/Intent;

    const-string v5, "com.mediatek.syslogger.action"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v5, "From"

    const-string v6, "ActivityNetworkLog"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "To"

    const-string v6, "CommonUI"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "Command"

    const-string v6, "Start"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v5}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v3

    if-nez v4, :cond_9

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNlConnection:Lcom/android/ActivityNetwork/NlConnection;

    if-nez v5, :cond_9

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v5, v3}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    move-result v5

    if-ne v5, v8, :cond_2

    const-string v5, "Device"

    invoke-direct {p0, v5}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showStorageFull(Ljava/lang/String;)V

    :cond_2
    const-string v5, "ActivityNetwork_Service"

    const-string v6, "Unable connect to activitynetwork, exit"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "Return"

    const-string v6, "DaemonUnable"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    new-instance v0, Landroid/content/Intent;

    const-string v5, "com.android.ActivityNetwork.APLogger_START_FAIL"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v5, "ActivityNetwork_Service"

    const-string v6, "send intent to commonUI: Connect to Daemon error!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_3
    return v8

    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    if-nez v5, :cond_5

    const-string v5, "false"

    iput-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;

    const-string v5, "false"

    iput-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isButtonEnable:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "com.android.ActivityNetwork.DEVICEBOOTUP"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "com.android.ActivityNetwork.Update_UI"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isButtonEnable:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;

    if-nez v5, :cond_6

    const-string v5, "false"

    iput-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;

    :goto_4
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isButtonEnable:Ljava/lang/String;

    if-nez v5, :cond_7

    const-string v5, "false"

    iput-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isButtonEnable:Ljava/lang/String;

    goto/16 :goto_0

    :cond_6
    const-string v5, "ActivityNetwork_Service"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isBootup is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_7
    const-string v5, "ActivityNetwork_Service"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isButtonEnable is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isButtonEnable:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    const-wide/16 v5, 0x7d0

    :try_start_1
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V

    const-string v5, "ActivityNetwork_Service"

    const-string v6, "Connection retry"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v4, v4, -0x1

    goto/16 :goto_1

    :catch_0
    move-exception v1

    const-string v5, "ActivityNetwork_Service"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "thread "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_9
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;

    const-string v6, "true"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->readConfig()Z

    move-result v5

    if-nez v5, :cond_a

    const-string v5, "ActivityNetwork_Service"

    const-string v6, "readConfig() fails"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "Return"

    const-string v6, "ConfigFile"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    const-string v5, "ActivityNetwork_Service"

    const-string v6, "send intent to commonUI: readConfig() fails"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;

    const/16 v6, 0xd

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    new-instance v0, Landroid/content/Intent;

    const-string v5, "com.android.ActivityNetwork.APLogger_START"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v5, "Return"

    const-string v6, "ConfigFile"

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_3

    :cond_a
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v5, "ActivityNetwork_Service"

    const-string v6, "MSG_INIT "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_b
    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->isBootup:Ljava/lang/String;

    const-string v6, "true"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v5, "ActivityNetwork_Service"

    const-string v6, "MSG_STOP "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3
.end method

.method pad(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    const/16 v1, 0xa

    if-lt p1, v1, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public readConfig()Z
    .locals 6

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v3}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v2

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    if-nez v3, :cond_0

    const-string v3, "storage"

    invoke-virtual {p0, v3}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/storage/StorageManager;

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    :cond_0
    invoke-direct {p0}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->getSDPATH()Ljava/lang/String;

    move-result-object v1

    const-string v3, "/mnt/sdcard"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "/storage/sdcard0"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "/sdcard"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_1
    const-string v3, "internal_sd_path"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    const-string v3, "/storage/sdcard0"

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    :cond_3
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "ActivityNetwork_Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "readConfig log should be saved in internal sdcard. At this time,internal sdcard is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Mount status:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v3, "mounted"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    :goto_1
    return v3

    :cond_4
    const-string v3, "/data"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "ActivityNetwork_Service"

    const-string v4, "readConfig log should be saved in /data"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "mounted"

    goto :goto_0

    :cond_5
    const-string v3, "external_sd_path"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_6
    const-string v3, "/storage/sdcard1"

    iput-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    :cond_7
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->extStorageManager:Landroid/os/storage/StorageManager;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "ActivityNetwork_Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "log should be saved in external sdcard. At this time,external sdcard is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->SDpath_SWAP:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Mount status:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_8
    const-string v3, "ActivityNetwork_Service"

    const-string v4, "sd card not ready."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "ActivityNetwork_Service"

    const-string v4, "read config file fails, exit"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    goto :goto_1
.end method

.method public updateServiceStatus(Z)I
    .locals 5
    .param p1    # Z

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v3}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v3, 0x1080072

    invoke-direct {p0, v3, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showNotification(IZ)V

    :goto_0
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v3, v0}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    move-result v3

    if-ne v3, v1, :cond_1

    const-string v2, "Device"

    invoke-direct {p0, v2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showStorageFull(Ljava/lang/String;)V

    :goto_1
    return v1

    :cond_0
    iget-object v3, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v3, 0x1080073

    invoke-direct {p0, v3, v2}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showNotification(IZ)V

    goto :goto_0

    :cond_1
    const-string v1, "ActivityNetwork_Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "leave updateServiceStatus:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    goto :goto_1
.end method

.method public updateUItoStart(Z)I
    .locals 5
    .param p1    # Z

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStart:Landroid/widget/Button;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStop:Landroid/widget/Button;

    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStart:Landroid/widget/Button;

    if-nez p1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mButtonStop:Landroid/widget/Button;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setEnabled(Z)V

    if-nez p1, :cond_2

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->StoPressed:Z

    :cond_0
    new-instance v1, Lcom/android/ActivityNetwork/NetworkSetting;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting_path:Ljava/lang/String;

    invoke-direct {v1, v4}, Lcom/android/ActivityNetwork/NetworkSetting;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v1}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v0

    if-ne p1, v2, :cond_3

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootEnabled:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceRunning:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceSync:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceSyncYes:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mNetworkSetting:Lcom/android/ActivityNetwork/NetworkSetting;

    invoke-virtual {v1, v0}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    move-result v1

    if-ne v1, v2, :cond_4

    const-string v1, "Device"

    invoke-direct {p0, v1}, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->showStorageFull(Ljava/lang/String;)V

    :goto_3
    return v2

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceBootOptionTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceOnBootDisabled:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStatusTag:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ActivityNetwork/ActivityNetwork_Service;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_3
.end method
