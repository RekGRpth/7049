.class public Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NetworkBroadcastReceiver.java"


# static fields
.field static final BOOT_ACTION:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field static final EXTRA_COMMON_UI:Ljava/lang/String; = "com.mediatek.syslogger.action"

.field static final IPO_BOOT_ACTION:Ljava/lang/String; = "android.intent.action.ACTION_BOOT_IPO"

.field static final IPO_DOWN_ACTION:Ljava/lang/String; = "android.intent.action.ACTION_SHUTDOWN_IPO"

.field private static final TAG:Ljava/lang/String; = "NetworkBroadcastReceiver"


# instance fields
.field private mLogPathTypePhone:Ljava/lang/String;

.field private mLogPathTypeSdCard:Ljava/lang/String;

.field private mLogPathTypeTag:Ljava/lang/String;

.field private mNetworkSetting_config:Lcom/android/ActivityNetwork/NetworkSetting;

.field private mNetworkSetting_config_name:Ljava/lang/String;

.field private mNetworkSetting_config_path:Ljava/lang/String;

.field mNetworkSetting_path:Ljava/lang/String;

.field private mServiceBootOptionTag:Ljava/lang/String;

.field private mServiceLogSizeTag:Ljava/lang/String;

.field private mServiceOnBootDisabled:Ljava/lang/String;

.field private mServiceOnBootEnabled:Ljava/lang/String;

.field private mServiceRunning:Ljava/lang/String;

.field private mServiceStatusTag:Ljava/lang/String;

.field private mServiceStopped:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const-string v0, "/data/data/com.android.ActivityNetwork/files"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mNetworkSetting_path:Ljava/lang/String;

    const-string v0, "/system/etc"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mNetworkSetting_config_path:Ljava/lang/String;

    const-string v0, "mtklog-config.prop"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mNetworkSetting_config_name:Ljava/lang/String;

    const-string v0, "service_boot_option"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceBootOptionTag:Ljava/lang/String;

    const-string v0, "true"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceOnBootEnabled:Ljava/lang/String;

    const-string v0, "false"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceOnBootDisabled:Ljava/lang/String;

    const-string v0, "service_status"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceStatusTag:Ljava/lang/String;

    const-string v0, "service_running"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceRunning:Ljava/lang/String;

    const-string v0, "service_stopped"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceStopped:Ljava/lang/String;

    const-string v0, "log_path_type"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mLogPathTypeTag:Ljava/lang/String;

    const-string v0, "phone"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mLogPathTypePhone:Ljava/lang/String;

    const-string v0, "sdcard"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mLogPathTypeSdCard:Ljava/lang/String;

    const-string v0, "input_log_size"

    iput-object v0, p0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceLogSizeTag:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 17
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v14, "NetworkBroadcastReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "NetworkBroadcastReceiver onReceive "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "action:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "android.intent.action.ACTION_BOOT_IPO"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    :cond_0
    const-string v14, "NetworkBroadcastReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "recv intent "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v6, Lcom/android/ActivityNetwork/NetworkSetting;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mNetworkSetting_path:Ljava/lang/String;

    invoke-direct {v6, v14}, Lcom/android/ActivityNetwork/NetworkSetting;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/android/ActivityNetwork/NetworkSetting;->loadConfig()Ljava/util/Properties;

    move-result-object v9

    new-instance v4, Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.ActivityNetwork_Service"

    invoke-direct {v4, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceBootOptionTag:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceOnBootDisabled:Ljava/lang/String;

    invoke-virtual {v9, v14, v15}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceOnBootEnabled:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "android.intent.action.ACTION_BOOT_IPO"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    const-string v14, "com.android.ActivityNetwork.DEVICEBOOTUP"

    const-string v15, "false"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.Update_UI"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "mServiceOnBootEnabled"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    :goto_2
    return-void

    :cond_2
    const-string v14, "com.android.ActivityNetwork.DEVICEBOOTUP"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceStatusTag:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v9, v14, v15}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceRunning:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    const-string v14, "com.android.ActivityNetwork.Update_UI"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_3
    const-string v14, "com.android.ActivityNetwork.Update_UI"

    const-string v15, "false"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_4
    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "mServiceOnBootDisabled "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceStatusTag:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceRunning:Ljava/lang/String;

    invoke-virtual {v9, v14, v15}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    const-string v14, "com.android.ActivityNetwork.DEVICEBOOTUP"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.Update_UI"

    const-string v15, "false"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "mServiceStopped "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mNetworkSetting_config_path:Ljava/lang/String;

    invoke-direct {v7, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_6

    const-string v14, "com.android.ActivityNetwork.DEVICEBOOTUP"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.Update_UI"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceStatusTag:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v9, v14, v15}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceBootOptionTag:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceOnBootEnabled:Ljava/lang/String;

    invoke-virtual {v9, v14, v15}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v6, v9}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    const-string v14, "NetworkBroadcastReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "dir:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mNetworkSetting_config_path:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "does not exist"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "mService Start "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_6
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mNetworkSetting_config_path:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mNetworkSetting_config_name:Ljava/lang/String;

    invoke-direct {v5, v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_7

    const-string v14, "com.android.ActivityNetwork.DEVICEBOOTUP"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.Update_UI"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceStatusTag:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v9, v14, v15}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceBootOptionTag:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceOnBootEnabled:Ljava/lang/String;

    invoke-virtual {v9, v14, v15}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v6, v9}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    const-string v14, "NetworkBroadcastReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "file:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mNetworkSetting_config_name:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "does not exist"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_7
    new-instance v12, Ljava/util/Properties;

    invoke-direct {v12}, Ljava/util/Properties;-><init>()V

    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v12, v8}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    const-string v14, "com.mediatek.log.net.enabled"

    const-string v15, "true"

    invoke-virtual {v12, v14, v15}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v14, "com.mediatek.log.net.maxsize"

    const-string v15, "200"

    invoke-virtual {v12, v14, v15}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v14, "0"

    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    const-string v11, "200"

    const-string v14, "NetworkBroadcastReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "mtklog-config.prop loadconfig log default size: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceLogSizeTag:Ljava/lang/String;

    invoke-virtual {v9, v14, v11}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v14, "NetworkBroadcastReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "mtklog-config.prop log size:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v14, "true"

    invoke-virtual {v10, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_9

    const-string v14, "com.android.ActivityNetwork.DEVICEBOOTUP"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.Update_UI"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceStatusTag:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceRunning:Ljava/lang/String;

    invoke-virtual {v9, v14, v15}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceBootOptionTag:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ActivityNetwork/NetworkBroadcastReceiver;->mServiceOnBootEnabled:Ljava/lang/String;

    invoke-virtual {v9, v14, v15}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "first boot default start"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    invoke-virtual {v6, v9}, Lcom/android/ActivityNetwork/NetworkSetting;->saveConfig(Ljava/util/Properties;)I

    goto/16 :goto_3

    :catch_0
    move-exception v2

    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "mtklog-config.prop loadconfig"

    invoke-static {v14, v15, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_4

    :cond_9
    const-string v14, "com.android.ActivityNetwork.DEVICEBOOTUP"

    const-string v15, "false"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.Update_UI"

    const-string v15, "false"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_5

    :cond_a
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    new-instance v4, Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.ActivityNetwork_Service"

    invoke-direct {v4, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto/16 :goto_2

    :cond_b
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.mediatek.syslogger.action"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_10

    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "Start from Common UI"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v14, "From"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_c

    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "Utils.EXTRA_FLAG_COMMON_UI_FROM is null"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_c
    const-string v14, "To"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-nez v13, :cond_d

    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "Utils.EXTRA_FLAG_COMMON_UI_TO is null"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_d
    const-string v14, "Command"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_e

    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "Utils.EXTRA_FLAG_COMMON_UI_COMMAND is null"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_e
    const-string v14, "NetworkBroadcastReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "receive EXTRA "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v13}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v14, "CommonUI"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f

    const-string v14, "ActivityNetworkLog"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f

    const-string v14, "Start"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f

    new-instance v4, Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.ActivityNetwork_Service"

    invoke-direct {v4, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v14, "com.android.ActivityNetwork.DEVICEBOOTUP"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.Update_UI"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "receive from Common UI and start"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_2

    :cond_f
    const-string v14, "CommonUI"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    const-string v14, "ActivityNetworkLog"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    const-string v14, "Stop"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    new-instance v4, Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.ActivityNetwork_Service"

    invoke-direct {v4, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v14, "com.android.ActivityNetwork.DEVICEBOOTUP"

    const-string v15, "true"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "com.android.ActivityNetwork.Update_UI"

    const-string v15, "false"

    invoke-virtual {v4, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "receive from Common UI and stop"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_2

    :cond_10
    const-string v14, "NetworkBroadcastReceiver"

    const-string v15, "no action is right "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method
