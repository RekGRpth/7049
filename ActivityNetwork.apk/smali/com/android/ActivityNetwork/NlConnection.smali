.class public Lcom/android/ActivityNetwork/NlConnection;
.super Ljava/lang/Object;
.source "NlConnection.java"


# static fields
.field private static final MSG_ALIVE:I = 0x9

.field private static final MSG_CLEARLOG:I = 0x1

.field private static final MSG_CONN_FAIL:I = 0x5

.field private static final MSG_DIE:I = 0x3

.field private static final MSG_INIT:I = 0x4

.field private static final MSG_LOW:I = 0xb

.field private static final MSG_REJECT:I = 0xa

.field private static final MSG_RUNCOMMAND:I = 0x2

.field private static final MSG_SD:I = 0x7

.field private static final MSG_STOP:I = 0x6

.field private static final MSG_STOPCOMMAND:I = 0x8

.field private static final MSG_STOPDIAG:I = 0xc

.field private static final MSG_STOP_TCPDUMP_COMMAND:I = 0x10

.field private static MSG_TYPE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ActivityNetwork_Thread"


# instance fields
.field private final BUFFER_SIZE:I

.field address:Landroid/net/LocalSocketAddress;

.field command_ret:I

.field private event:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field private mInputStream:Ljava/io/InputStream;

.field private mMsg:Landroid/os/Message;

.field private mOutputStream:Ljava/io/OutputStream;

.field private mlistenThread:Ljava/lang/Thread;

.field socket:Landroid/net/LocalSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/Handler;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Handler;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/ActivityNetwork/NlConnection;->mlistenThread:Ljava/lang/Thread;

    iput-object v0, p0, Lcom/android/ActivityNetwork/NlConnection;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x1000

    iput v0, p0, Lcom/android/ActivityNetwork/NlConnection;->BUFFER_SIZE:I

    iput-object p2, p0, Lcom/android/ActivityNetwork/NlConnection;->mHandler:Landroid/os/Handler;

    new-instance v0, Landroid/net/LocalSocket;

    invoke-direct {v0}, Landroid/net/LocalSocket;-><init>()V

    iput-object v0, p0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    new-instance v0, Landroid/net/LocalSocketAddress;

    sget-object v1, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct {v0, p1, v1}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    iput-object v0, p0, Lcom/android/ActivityNetwork/NlConnection;->address:Landroid/net/LocalSocketAddress;

    return-void
.end method


# virtual methods
.method public connect()Z
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    iget-object v2, p0, Lcom/android/ActivityNetwork/NlConnection;->address:Landroid/net/LocalSocketAddress;

    invoke-virtual {v1, v2}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    iget-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->mOutputStream:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->mInputStream:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v1, Lcom/android/ActivityNetwork/NlConnection$1;

    invoke-direct {v1, p0}, Lcom/android/ActivityNetwork/NlConnection$1;-><init>(Lcom/android/ActivityNetwork/NlConnection;)V

    iput-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->mlistenThread:Ljava/lang/Thread;

    iget-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->mlistenThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    const-string v1, "ActivityNetwork_Thread"

    const-string v2, "Start mAndroidLogThread over"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "ActivityNetwork_Thread"

    const-string v2, "Communications error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public listen()V
    .locals 7

    const/16 v4, 0x1000

    new-array v0, v4, [B

    const-string v4, "ActivityNetwork_Thread"

    const-string v5, "read thread running"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/android/ActivityNetwork/NlConnection;->mInputStream:Ljava/io/InputStream;

    const/4 v5, 0x0

    const/16 v6, 0x1000

    invoke-virtual {v4, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-gez v1, :cond_0

    :goto_1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/ActivityNetwork/NlConnection;->stop()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    return-void

    :cond_0
    :try_start_2
    const-string v4, "ActivityNetwork_Thread"

    const-string v5, "read success"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    sput v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    const/4 v4, 0x0

    aget-byte v4, v0, v4

    const/16 v5, 0x63

    if-ne v4, v5, :cond_3

    sget v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    :cond_1
    :goto_3
    const/4 v4, 0x1

    aget-byte v4, v0, v4

    const/16 v5, 0x6f

    if-ne v4, v5, :cond_9

    sget v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    add-int/lit16 v4, v4, 0x80

    sput v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    :cond_2
    :goto_4
    iget-object v4, p0, Lcom/android/ActivityNetwork/NlConnection;->mHandler:Landroid/os/Handler;

    sget v5, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    iput-object v4, p0, Lcom/android/ActivityNetwork/NlConnection;->mMsg:Landroid/os/Message;

    iget-object v4, p0, Lcom/android/ActivityNetwork/NlConnection;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/ActivityNetwork/NlConnection;->mMsg:Landroid/os/Message;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-string v4, "ActivityNetwork_Thread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MSG_TYPE:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v4, "ActivityNetwork_Thread"

    const-string v5, "read failed"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    :try_start_3
    aget-byte v4, v0, v4

    const/16 v5, 0x72

    if-ne v4, v5, :cond_4

    sget v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    add-int/lit8 v4, v4, 0x2

    sput v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    aget-byte v4, v0, v4

    const/16 v5, 0x74

    if-ne v4, v5, :cond_5

    sget v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    add-int/lit8 v4, v4, 0x4

    sput v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    goto :goto_3

    :cond_5
    const/4 v4, 0x0

    aget-byte v4, v0, v4

    const/16 v5, 0x70

    if-ne v4, v5, :cond_6

    sget v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    add-int/lit8 v4, v4, 0x8

    sput v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    aget-byte v4, v0, v4

    const/16 v5, 0x73

    if-ne v4, v5, :cond_7

    sget v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    add-int/lit8 v4, v4, 0x10

    sput v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    aget-byte v4, v0, v4

    const/16 v5, 0x67

    if-ne v4, v5, :cond_8

    sget v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    add-int/lit8 v4, v4, 0x20

    sput v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    goto/16 :goto_3

    :cond_8
    const/4 v4, 0x0

    aget-byte v4, v0, v4

    const/16 v5, 0x6b

    if-ne v4, v5, :cond_1

    sget v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    add-int/lit8 v4, v4, 0x40

    sput v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    goto/16 :goto_3

    :cond_9
    const/4 v4, 0x1

    aget-byte v4, v0, v4

    const/16 v5, 0x77

    if-ne v4, v5, :cond_a

    sget v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    add-int/lit16 v4, v4, 0x100

    sput v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    goto/16 :goto_4

    :cond_a
    const/4 v4, 0x1

    aget-byte v4, v0, v4

    const/16 v5, 0x6e

    if-ne v4, v5, :cond_2

    sget v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I

    add-int/lit16 v4, v4, 0x200

    sput v4, Lcom/android/ActivityNetwork/NlConnection;->MSG_TYPE:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_4

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method public sendCmd(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const-string v2, "ActivityNetwork_Thread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "send cmd: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/android/ActivityNetwork/NlConnection;->mOutputStream:Ljava/io/OutputStream;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    if-nez v2, :cond_1

    :cond_0
    const-string v2, "ActivityNetwork_Thread"

    const-string v3, "No connection to daemon"

    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v2, v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "ActivityNetwork_Thread"

    const-string v3, "builder success"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v2, p0, Lcom/android/ActivityNetwork/NlConnection;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    iget-object v2, p0, Lcom/android/ActivityNetwork/NlConnection;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "ActivityNetwork_Thread"

    const-string v3, "IOException in do_command"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/android/ActivityNetwork/NlConnection;->stop()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public stop()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v1, "ActivityNetwork_Thread"

    const-string v2, "thread stop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->mlistenThread:Ljava/lang/Thread;

    if-nez v1, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->mlistenThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    iget-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    if-nez v1, :cond_1

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->shutdownInput()V

    iget-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->shutdownOutput()V

    iget-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->mlistenThread:Ljava/lang/Thread;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->socket:Landroid/net/LocalSocket;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->mOutputStream:Ljava/io/OutputStream;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/ActivityNetwork/NlConnection;->mInputStream:Ljava/io/InputStream;

    const-string v1, "ActivityNetwork_Thread"

    const-string v2, "mlistenThread socket = null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "ActivityNetwork_Thread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "socket close: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
