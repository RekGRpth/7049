.class public Lcom/android/ActivityNetwork/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static final ACTION_ACTIVITY_START:Ljava/lang/String; = "com.android.ActivityNetwork.APLogger_START"

.field public static final ACTION_ACTIVITY_START_FAIL:Ljava/lang/String; = "com.android.ActivityNetwork.APLogger_START_FAIL"

.field public static final ACTION_ACTIVITY_STOP:Ljava/lang/String; = "com.android.ActivityNetwork.APLogger_STOP"

.field public static final ACTION_ACTIVITY_STOP_FAIL:Ljava/lang/String; = "com.android.ActivityNetwork.APLogger_STOP_FAIL"

.field public static final ACTION_EM_SYSTEM_LOGGER:Ljava/lang/String; = "com.mediatek.syslogger.action"

.field public static final ACTION_START_SERVICE:Ljava/lang/String; = "com.android.ActivityNetwork.ActivityNetwork_Service"

.field public static final BROADCAST_KEY_FOLDERPATH:Ljava/lang/String; = ""

.field public static final BROADCAST_KEY_SRC_FROM:Ljava/lang/String; = "From"

.field public static final BROADCAST_KEY_SRC_TO:Ljava/lang/String; = "To"

.field public static final COMMAND_START:Ljava/lang/String; = "Start"

.field public static final COMMAND_STOP:Ljava/lang/String; = "Stop"

.field public static final COMMAND_UNKNOWN:Ljava/lang/String; = "Unknown"

.field public static final EXTRA_FLAG_COMMON_UI_COMMAND:Ljava/lang/String; = "Command"

.field public static final EXTRA_FLAG_COMMON_UI_FROM:Ljava/lang/String; = "From"

.field public static final EXTRA_FLAG_COMMON_UI_RETURN:Ljava/lang/String; = "Return"

.field public static final EXTRA_FLAG_COMMON_UI_TO:Ljava/lang/String; = "To"

.field public static final FROM_ACTIVITY:I = 0x2

.field public static final FROM_COMMONUI:I = 0x3

.field public static final FROM_MDLOGGER:I = 0x1

.field public static final LOG_PATH_DATA:Ljava/lang/String; = "/data"

.field public static final LOG_PATH_SDCARD:Ljava/lang/String; = "/mnt/sdcard"

.field public static final LOG_PATH_SDCARD_EXT:Ljava/lang/String; = "/mnt/sdcard2"

.field public static final LOG_PATH_SDCARD_EXT_JB:Ljava/lang/String; = "/storage/sdcard1"

.field public static final LOG_PATH_SDCARD_JB:Ljava/lang/String; = "/storage/sdcard0"

.field public static final RETURN_CONFIG:Ljava/lang/String; = "ConfigFile"

.field public static final RETURN_DAEMON:Ljava/lang/String; = "DaemonUnable"

.field public static final RETURN_NANDFULL:Ljava/lang/String; = "NandFull"

.field public static final RETURN_NORMAL:Ljava/lang/String; = "Normal"

.field public static final RETURN_NOSD:Ljava/lang/String; = "NoSDCard"

.field public static final RETURN_SDFULL:Ljava/lang/String; = "SDCardFull"

.field public static final RETURN_UNKNOWN:Ljava/lang/String; = "Unknown"

.field public static final SOURCE_APLOGGER:Ljava/lang/String; = "ActivityNetworkLog"

.field public static final SOURCE_COMMONUI:Ljava/lang/String; = "CommonUI"

.field public static final SOURCE_UNKNOWN:Ljava/lang/String; = "Unknown"

.field public static final TAG_ACTIVITY:Ljava/lang/String; = "com.mediatek.syslogger.taglog"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
