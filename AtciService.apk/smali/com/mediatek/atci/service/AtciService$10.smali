.class Lcom/mediatek/atci/service/AtciService$10;
.super Landroid/bluetooth/AtCommandHandler;
.source "AtciService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/atci/service/AtciService;->initializeAtMp3Play()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/atci/service/AtciService;


# direct methods
.method constructor <init>(Lcom/mediatek/atci/service/AtciService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService$10;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Landroid/bluetooth/AtCommandHandler;-><init>()V

    return-void
.end method

.method private queryTestMode()Landroid/bluetooth/AtCommandResult;
    .locals 5

    const/16 v4, 0x30

    invoke-static {}, Lcom/mediatek/atci/service/Mp3Player;->getInstanceOfMp3Player()Lcom/mediatek/atci/service/Mp3Player;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$10;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$2100(Lcom/mediatek/atci/service/AtciService;)C

    move-result v2

    if-le v2, v4, :cond_0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$10;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$2100(Lcom/mediatek/atci/service/AtciService;)C

    move-result v2

    const/16 v3, 0x35

    if-gt v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/atci/service/Mp3Player;->isIdle()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$10;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2, v4}, Lcom/mediatek/atci/service/AtciService;->access$2102(Lcom/mediatek/atci/service/AtciService;C)C

    :cond_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Get MPT mMp3PlayerMode:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$10;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v4}, Lcom/mediatek/atci/service/AtciService;->access$2100(Lcom/mediatek/atci/service/AtciService;)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$10;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v3}, Lcom/mediatek/atci/service/AtciService;->access$2100(Lcom/mediatek/atci/service/AtciService;)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v2, v1}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    return-object v2
.end method


# virtual methods
.method public handleActionCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$10;->queryTestMode()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleReadCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$10;->queryTestMode()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleSetCommand([Ljava/lang/Object;)Landroid/bluetooth/AtCommandResult;
    .locals 9
    .param p1    # [Ljava/lang/Object;

    const/16 v8, 0x30

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string v3, "ATCIJ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "args:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "ATCIJ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "args string:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/atci/service/Mp3Player;->getInstanceOfMp3Player()Lcom/mediatek/atci/service/Mp3Player;

    move-result-object v1

    array-length v3, p1

    if-ne v3, v7, :cond_3

    const/4 v0, 0x0

    const/4 v2, 0x0

    aget-object v3, p1, v6

    instance-of v3, v3, Ljava/lang/Integer;

    if-eqz v3, :cond_0

    aget-object v3, p1, v6

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    :cond_0
    const-string v3, "ATCIJ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "c is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-lt v0, v8, :cond_1

    const/16 v3, 0x35

    if-gt v0, v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$10;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v3, v0}, Lcom/mediatek/atci/service/AtciService;->access$2102(Lcom/mediatek/atci/service/AtciService;C)C

    :cond_1
    if-ne v0, v8, :cond_2

    invoke-virtual {v1}, Lcom/mediatek/atci/service/Mp3Player;->stopPlayer()V

    :goto_0
    packed-switch v0, :pswitch_data_0

    const-string v2, "[5]MP3 ERROR"

    :goto_1
    new-instance v3, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v3, v2}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    :goto_2
    return-object v3

    :cond_2
    invoke-virtual {v1, v0}, Lcom/mediatek/atci/service/Mp3Player;->startPlayer(C)V

    goto :goto_0

    :pswitch_0
    const-string v2, "[0]MP3 OFF"

    goto :goto_1

    :pswitch_1
    const-string v2, "[1]NO SIGNAL"

    goto :goto_1

    :pswitch_2
    const-string v2, "[2]LR"

    goto :goto_1

    :pswitch_3
    const-string v2, "[3]L"

    goto :goto_1

    :pswitch_4
    const-string v2, "[4]R"

    goto :goto_1

    :pswitch_5
    const-string v2, "[5]MULTI LR"

    goto :goto_1

    :cond_3
    new-instance v3, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v3, v7}, Landroid/bluetooth/AtCommandResult;-><init>(I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public handleTestCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$10;->queryTestMode()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method
