.class Lcom/mediatek/atci/service/AtciService$11;
.super Landroid/bluetooth/AtCommandHandler;
.source "AtciService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/atci/service/AtciService;->initializeAtNoSleep()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/atci/service/AtciService;


# direct methods
.method constructor <init>(Lcom/mediatek/atci/service/AtciService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService$11;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Landroid/bluetooth/AtCommandHandler;-><init>()V

    return-void
.end method

.method private checkFullWakelcok()Landroid/bluetooth/AtCommandResult;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService$11;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v0}, Lcom/mediatek/atci/service/AtciService;->access$2200(Lcom/mediatek/atci/service/AtciService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/bluetooth/AtCommandResult;

    const-string v1, "1"

    invoke-direct {v0, v1}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/bluetooth/AtCommandResult;

    const-string v1, "0"

    invoke-direct {v0, v1}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public handleActionCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$11;->checkFullWakelcok()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleReadCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$11;->checkFullWakelcok()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleSetCommand([Ljava/lang/Object;)Landroid/bluetooth/AtCommandResult;
    .locals 7
    .param p1    # [Ljava/lang/Object;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "args:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "args string:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    array-length v2, p1

    if-ne v2, v6, :cond_1

    const/4 v0, 0x0

    const/4 v1, 0x0

    aget-object v2, p1, v5

    instance-of v2, v2, Ljava/lang/Integer;

    if-eqz v2, :cond_0

    aget-object v2, p1, v5

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    :cond_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "c is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    const-string v1, "NOSLEEP ERROR"

    :goto_0
    new-instance v2, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v2, v1}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    :goto_1
    return-object v2

    :pswitch_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$11;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$2200(Lcom/mediatek/atci/service/AtciService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    const-string v1, "[0]Sleep Mode"

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$11;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$2200(Lcom/mediatek/atci/service/AtciService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-string v1, "[1]No Sleep Mode"

    goto :goto_0

    :cond_1
    new-instance v2, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v2, v6}, Landroid/bluetooth/AtCommandResult;-><init>(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public handleTestCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$11;->checkFullWakelcok()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method
