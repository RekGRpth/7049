.class Lcom/mediatek/atci/service/AtciService$17;
.super Landroid/bluetooth/AtCommandHandler;
.source "AtciService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/atci/service/AtciService;->initializeAtSystemCall()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/atci/service/AtciService;

.field final synthetic val$mStorageListener:Landroid/os/storage/StorageEventListener;


# direct methods
.method constructor <init>(Lcom/mediatek/atci/service/AtciService;Landroid/os/storage/StorageEventListener;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService$17;->this$0:Lcom/mediatek/atci/service/AtciService;

    iput-object p2, p0, Lcom/mediatek/atci/service/AtciService$17;->val$mStorageListener:Landroid/os/storage/StorageEventListener;

    invoke-direct {p0}, Landroid/bluetooth/AtCommandHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleActionCommand()Landroid/bluetooth/AtCommandResult;
    .locals 8

    const-string v2, "OK\r\n"

    new-instance v1, Landroid/content/Intent;

    const-string v6, "com.android.internal.os.storage.FORMAT_ONLY"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/android/internal/os/storage/ExternalStorageFormatter;->COMPONENT_NAME:Landroid/content/ComponentName;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/atci/service/AtciService$17;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v7, "storage"

    invoke-virtual {v6, v7}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/storage/StorageManager;

    iget-object v6, p0, Lcom/mediatek/atci/service/AtciService$17;->val$mStorageListener:Landroid/os/storage/StorageEventListener;

    invoke-virtual {v3, v6}, Landroid/os/storage/StorageManager;->registerListener(Landroid/os/storage/StorageEventListener;)V

    invoke-virtual {v3}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v5

    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v6, v5

    if-ge v0, v6, :cond_0

    aget-object v6, v5, v0

    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "/mnt/sdcard"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    aget-object v4, v5, v0

    :cond_0
    const-string v6, "storage_volume"

    invoke-virtual {v1, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/atci/service/AtciService$17;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-virtual {v6, v1}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    new-instance v6, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v6, v2}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    return-object v6

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
