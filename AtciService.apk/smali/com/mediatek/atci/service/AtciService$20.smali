.class Lcom/mediatek/atci/service/AtciService$20;
.super Landroid/bluetooth/AtCommandHandler;
.source "AtciService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/atci/service/AtciService;->initializeAtComCamera()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/atci/service/AtciService;


# direct methods
.method constructor <init>(Lcom/mediatek/atci/service/AtciService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Landroid/bluetooth/AtCommandHandler;-><init>()V

    return-void
.end method

.method private cameraOk()Landroid/bluetooth/AtCommandResult;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v2, "AVR OK"

    invoke-static {v1, v2}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v1, v0}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method private isCameraOn()Landroid/bluetooth/AtCommandResult;
    .locals 4

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v3}, Lcom/mediatek/atci/service/AtciService;->access$2400(Lcom/mediatek/atci/service/AtciService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v1, v0}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method private notSupport()Landroid/bluetooth/AtCommandResult;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v2, "NOT SUPPORT"

    invoke-static {v1, v2}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v1, v0}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method private startCameraCaptureVideo()Z
    .locals 3

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.media.action.VIDEO_CAMERA"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-virtual {v2, v1}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public handleActionCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$20;->isCameraOn()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleReadCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$20;->isCameraOn()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleSetCommand([Ljava/lang/Object;)Landroid/bluetooth/AtCommandResult;
    .locals 9
    .param p1    # [Ljava/lang/Object;

    const/4 v8, 0x7

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, -0x1

    if-eqz p1, :cond_0

    aget-object v3, p1, v6

    instance-of v3, v3, Ljava/lang/Integer;

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "AtciService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleSetCommand:Invalid args "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$20;->notSupport()Landroid/bluetooth/AtCommandResult;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_1
    aget-object v3, p1, v6

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "AtciService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleSetCommand:get setNumber="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-ltz v2, :cond_2

    if-le v2, v8, :cond_3

    :cond_2
    const-string v3, "AtciService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error set number "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Landroid/bluetooth/AtCommandResult;

    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v5, "AVR ERROR"

    invoke-static {v4, v5}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v3, 0x6

    if-lt v2, v3, :cond_4

    if-gt v2, v8, :cond_4

    const-string v3, "AtciService"

    const-string v4, "not supported function tested!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$20;->notSupport()Landroid/bluetooth/AtCommandResult;

    move-result-object v3

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v3, v6}, Lcom/mediatek/atci/service/AtciService;->access$2302(Lcom/mediatek/atci/service/AtciService;I)I

    if-ne v7, v2, :cond_6

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v3, v7}, Lcom/mediatek/atci/service/AtciService;->access$2402(Lcom/mediatek/atci/service/AtciService;I)I

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$20;->startCameraCaptureVideo()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$20;->cameraOk()Landroid/bluetooth/AtCommandResult;

    move-result-object v3

    goto :goto_0

    :cond_5
    new-instance v3, Landroid/bluetooth/AtCommandResult;

    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v5, "CAMERA INITIAL FAILED"

    invoke-static {v4, v5}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.mediatek.AtciService.AT_AVR"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "setNumber"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-virtual {v3, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v3, "AtciService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleSetCommand:sent AT_CAM broadcast intent:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$20;->this$0:Lcom/mediatek/atci/service/AtciService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AVR set "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OK"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v3, v1}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public handleTestCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$20;->isCameraOn()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method
