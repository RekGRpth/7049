.class Lcom/mediatek/atci/service/AtciService$23;
.super Ljava/lang/Object;
.source "AtciService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/atci/service/AtciService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/atci/service/AtciService;


# direct methods
.method constructor <init>(Lcom/mediatek/atci/service/AtciService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService$23;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.onServiceConnected"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$23;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {p2}, Lcom/mediatek/atci/service/IFMRadioService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mediatek/atci/service/IFMRadioService;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/atci/service/AtciService;->access$702(Lcom/mediatek/atci/service/AtciService;Lcom/mediatek/atci/service/IFMRadioService;)Lcom/mediatek/atci/service/IFMRadioService;

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$23;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$700(Lcom/mediatek/atci/service/AtciService;)Lcom/mediatek/atci/service/IFMRadioService;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: null interface"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$23;->this$0:Lcom/mediatek/atci/service/AtciService;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/atci/service/AtciService;->access$802(Lcom/mediatek/atci/service/AtciService;Z)Z

    const-string v2, "ATCIJ"

    const-string v3, "<<< FMRadio.onServiceConnected"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$23;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$2800(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "ATCIJ"

    const-string v3, "FMRadio service is not init."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$23;->this$0:Lcom/mediatek/atci/service/AtciService;

    const/16 v3, 0x3e8

    invoke-static {v2, v3}, Lcom/mediatek/atci/service/AtciService;->access$2900(Lcom/mediatek/atci/service/AtciService;I)V

    new-instance v1, Lcom/mediatek/atci/service/AtciService$InitialThread;

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$23;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-direct {v1, v2}, Lcom/mediatek/atci/service/AtciService$InitialThread;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :cond_1
    const-string v2, "ATCIJ"

    const-string v3, "FMRadio service is already init."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$23;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$3000(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$23;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$1300(Lcom/mediatek/atci/service/AtciService;)I

    move-result v0

    const/16 v2, 0x438

    if-gt v0, v2, :cond_2

    const/16 v2, 0x36b

    if-ge v0, v2, :cond_3

    :cond_2
    const-string v2, "ATCIJ"

    const-string v3, "FMRadio Error: invalid frequency in service."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v2, "ATCIJ"

    const-string v3, "The frequency in FM service is same as in database."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v2, "ATCIJ"

    const-string v3, "Error: FMRadio device is not open"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "ATCIJ"

    const-string v1, ">>> FMRadioEMActivity.onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService$23;->this$0:Lcom/mediatek/atci/service/AtciService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/atci/service/AtciService;->access$702(Lcom/mediatek/atci/service/AtciService;Lcom/mediatek/atci/service/IFMRadioService;)Lcom/mediatek/atci/service/IFMRadioService;

    const-string v0, "ATCIJ"

    const-string v1, "<<< FMRadioEMActivity.onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
