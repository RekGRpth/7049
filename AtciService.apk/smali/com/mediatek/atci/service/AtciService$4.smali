.class Lcom/mediatek/atci/service/AtciService$4;
.super Landroid/bluetooth/AtCommandHandler;
.source "AtciService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/atci/service/AtciService;->initializeAtTelephony()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/atci/service/AtciService;


# direct methods
.method constructor <init>(Lcom/mediatek/atci/service/AtciService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService$4;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Landroid/bluetooth/AtCommandHandler;-><init>()V

    return-void
.end method

.method private isAirplaneModeOn()Landroid/bluetooth/AtCommandResult;
    .locals 5

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$4;->this$0:Lcom/mediatek/atci/service/AtciService;

    iget-object v2, v2, Lcom/mediatek/atci/service/AtciService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Get airplane mode:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$4;->this$0:Lcom/mediatek/atci/service/AtciService;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v2, v1}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    return-object v2
.end method


# virtual methods
.method public handleActionCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$4;->isAirplaneModeOn()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleReadCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$4;->isAirplaneModeOn()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleSetCommand([Ljava/lang/Object;)Landroid/bluetooth/AtCommandResult;
    .locals 10
    .param p1    # [Ljava/lang/Object;

    const/16 v9, 0x30

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v4, "ATCIJ"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "args:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, p1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "ATCIJ"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "args string:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, p1, v5

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    array-length v4, p1

    if-ne v4, v6, :cond_5

    const/4 v0, 0x0

    const/4 v3, 0x0

    aget-object v4, p1, v5

    instance-of v4, v4, Ljava/lang/Integer;

    if-eqz v4, :cond_0

    aget-object v4, p1, v5

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    :cond_0
    const-string v4, "ATCIJ"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "c is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq v0, v9, :cond_1

    const/16 v4, 0x31

    if-ne v0, v4, :cond_5

    :cond_1
    const-string v4, "ATCIJ"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Set airplane mode:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v0, v9, :cond_2

    move v1, v5

    :goto_0
    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$4;->this$0:Lcom/mediatek/atci/service/AtciService;

    iget-object v4, v4, Lcom/mediatek/atci/service/AtciService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v7, "airplane_mode_on"

    if-eqz v1, :cond_3

    :goto_1
    invoke-static {v4, v7, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "state"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$4;->this$0:Lcom/mediatek/atci/service/AtciService;

    iget-object v4, v4, Lcom/mediatek/atci/service/AtciService;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    if-ne v0, v9, :cond_4

    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$4;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v5, "[0] FLIGHT Mode OFF"

    invoke-static {v4, v5}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    new-instance v4, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v4, v3}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    :goto_3
    return-object v4

    :cond_2
    move v1, v6

    goto :goto_0

    :cond_3
    move v6, v5

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$4;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v5, "[1] FLIGHT Mode ON"

    invoke-static {v4, v5}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_5
    new-instance v4, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v4, v6}, Landroid/bluetooth/AtCommandResult;-><init>(I)V

    goto :goto_3
.end method

.method public handleTestCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$4;->isAirplaneModeOn()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method
