.class Lcom/mediatek/atci/service/AtciService$5;
.super Landroid/bluetooth/AtCommandHandler;
.source "AtciService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/atci/service/AtciService;->initializeAtTelephony()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/atci/service/AtciService;


# direct methods
.method constructor <init>(Lcom/mediatek/atci/service/AtciService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService$5;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Landroid/bluetooth/AtCommandHandler;-><init>()V

    return-void
.end method

.method private isEmgencyCallOn()Landroid/bluetooth/AtCommandResult;
    .locals 4

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService$5;->this$0:Lcom/mediatek/atci/service/AtciService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$5;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v3}, Lcom/mediatek/atci/service/AtciService;->access$100(Lcom/mediatek/atci/service/AtciService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v1, v0}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public handleActionCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$5;->isEmgencyCallOn()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleReadCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$5;->isEmgencyCallOn()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleSetCommand([Ljava/lang/Object;)Landroid/bluetooth/AtCommandResult;
    .locals 8
    .param p1    # [Ljava/lang/Object;

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    array-length v3, p1

    if-ne v3, v6, :cond_4

    const/4 v0, -0x1

    const/4 v2, 0x1

    aget-object v3, p1, v4

    instance-of v3, v3, Ljava/lang/Integer;

    if-eqz v3, :cond_0

    aget-object v3, p1, v4

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :cond_0
    if-ltz v0, :cond_4

    if-gt v0, v7, :cond_4

    const-string v3, "ATCIJ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Make ECC call with "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "ATCIJ"

    const-string v4, "FeatureOption.MTK_GEMINI_SUPPORT "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_4

    if-nez v0, :cond_2

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$5;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v4, "[0]ECALL OFF"

    invoke-static {v3, v4}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    :goto_0
    new-instance v3, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v3, v1}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    :goto_1
    return-object v3

    :cond_2
    if-ne v0, v6, :cond_3

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$5;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v4, "[1]ECALL ON"

    invoke-static {v3, v4}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    if-ne v0, v7, :cond_1

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$5;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v4, "[2]ECALL ON"

    invoke-static {v3, v4}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService$5;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v4, "ECALL ERROR"

    invoke-static {v3, v4}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v3, v1}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public handleTestCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$5;->isEmgencyCallOn()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method
