.class Lcom/mediatek/atci/service/AtciService$6;
.super Landroid/bluetooth/AtCommandHandler;
.source "AtciService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/atci/service/AtciService;->initializeAtTelephony()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/atci/service/AtciService;


# direct methods
.method constructor <init>(Lcom/mediatek/atci/service/AtciService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService$6;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Landroid/bluetooth/AtCommandHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleActionCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService$6;->handleReadCommand()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleReadCommand()Landroid/bluetooth/AtCommandResult;
    .locals 3

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$6;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$200(Lcom/mediatek/atci/service/AtciService;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "BLUETOOTH ADDRESS READ FAIL"

    new-instance v1, Landroid/bluetooth/AtCommandResult;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/bluetooth/AtCommandResult;-><init>(I)V

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$6;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2, v0}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/bluetooth/AtCommandResult;->addResponse(Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Landroid/bluetooth/AtCommandResult;

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$6;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2, v0}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleSetCommand([Ljava/lang/Object;)Landroid/bluetooth/AtCommandResult;
    .locals 11
    .param p1    # [Ljava/lang/Object;

    const/16 v10, 0xc

    const/4 v6, 0x1

    const/4 v5, 0x0

    array-length v8, p1

    if-ne v8, v10, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object v1, p1

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v1, v2

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const-string v8, "ATCIJ"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Addr="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/mediatek/atci/service/AtciService$6;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v8, v7}, Lcom/mediatek/atci/service/AtciService;->access$300(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v6, -0x1

    :cond_1
    :goto_1
    new-instance v5, Landroid/bluetooth/AtCommandResult;

    const/4 v8, 0x2

    invoke-direct {v5, v8}, Landroid/bluetooth/AtCommandResult;-><init>(I)V

    if-gez v6, :cond_3

    iget-object v8, p0, Lcom/mediatek/atci/service/AtciService$6;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v9, "BLUETOOTH ADDRESS WRITE FAIL"

    invoke-static {v8, v9}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/bluetooth/AtCommandResult;->addResponse(Ljava/lang/String;)V

    :goto_2
    return-object v5

    :cond_2
    const/4 v6, -0x1

    goto :goto_1

    :cond_3
    iget-object v8, p0, Lcom/mediatek/atci/service/AtciService$6;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v9, "BLUETOOTH ADDRESS WRITE OK"

    invoke-static {v8, v9}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/bluetooth/AtCommandResult;->addResponse(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public handleTestCommand()Landroid/bluetooth/AtCommandResult;
    .locals 3

    new-instance v0, Landroid/bluetooth/AtCommandResult;

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService$6;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v2, "AT%BTAD=[BD ADDR : 12 HEX nibble => 6 Bytes]"

    invoke-static {v1, v2}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
