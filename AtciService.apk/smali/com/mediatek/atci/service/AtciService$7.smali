.class Lcom/mediatek/atci/service/AtciService$7;
.super Landroid/bluetooth/AtCommandHandler;
.source "AtciService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/atci/service/AtciService;->initializeAtTelephony()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/atci/service/AtciService;


# direct methods
.method constructor <init>(Lcom/mediatek/atci/service/AtciService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService$7;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Landroid/bluetooth/AtCommandHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleActionCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService$7;->handleReadCommand()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleReadCommand()Landroid/bluetooth/AtCommandResult;
    .locals 3

    new-instance v1, Landroid/bluetooth/AtCommandResult;

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$7;->this$0:Lcom/mediatek/atci/service/AtciService;

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService$7;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v0}, Lcom/mediatek/atci/service/AtciService;->access$400(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-static {v2, v0}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_0
    const-string v0, "2"

    goto :goto_0
.end method

.method public handleSetCommand([Ljava/lang/Object;)Landroid/bluetooth/AtCommandResult;
    .locals 12
    .param p1    # [Ljava/lang/Object;

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    array-length v6, p1

    if-ne v6, v10, :cond_4

    aget-object v6, p1, v9

    instance-of v6, v6, Ljava/lang/Integer;

    if-eqz v6, :cond_4

    aget-object v6, p1, v9

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const-string v6, "ATCIJ"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AT%BTTM="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, p1, v9

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x3

    if-ne v2, v6, :cond_1

    iget-object v6, p0, Lcom/mediatek/atci/service/AtciService$7;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v6}, Lcom/mediatek/atci/service/AtciService;->access$500(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v5

    iget-object v6, p0, Lcom/mediatek/atci/service/AtciService$7;->this$0:Lcom/mediatek/atci/service/AtciService;

    iget-object v6, v6, Lcom/mediatek/atci/service/AtciService;->mContext:Landroid/content/Context;

    const-string v7, "audio"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const-string v6, "SET_LOOPBACK_TYPE=0"

    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    :cond_0
    :goto_0
    new-instance v4, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v4, v11}, Landroid/bluetooth/AtCommandResult;-><init>(I)V

    if-eqz v5, :cond_5

    iget-object v6, p0, Lcom/mediatek/atci/service/AtciService$7;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v7, "BTTM ERROR"

    invoke-static {v6, v7}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/bluetooth/AtCommandResult;->addResponse(Ljava/lang/String;)V

    :goto_1
    return-object v4

    :cond_1
    const/4 v3, -0x1

    if-ne v2, v11, :cond_3

    const/4 v3, 0x0

    :cond_2
    :goto_2
    if-ltz v3, :cond_0

    const-string v6, "0005c90000%02d"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v6, p0, Lcom/mediatek/atci/service/AtciService$7;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v6, v1}, Lcom/mediatek/atci/service/AtciService;->access$600(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v6, p0, Lcom/mediatek/atci/service/AtciService$7;->this$0:Lcom/mediatek/atci/service/AtciService;

    iget-object v6, v6, Lcom/mediatek/atci/service/AtciService;->mContext:Landroid/content/Context;

    const-string v7, "audio"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const-string v6, "SET_LOOPBACK_TYPE=31"

    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/16 v6, 0xb

    if-lt v2, v6, :cond_2

    const/16 v6, 0x2a

    if-gt v2, v6, :cond_2

    add-int/lit8 v3, v2, -0xa

    goto :goto_2

    :cond_4
    const-string v6, "ATCIJ"

    const-string v7, "arg length is not 1 or arg[0] is not integer"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    iget-object v6, p0, Lcom/mediatek/atci/service/AtciService$7;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v7, "BTTM OK"

    invoke-static {v6, v7}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/bluetooth/AtCommandResult;->addResponse(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public handleTestCommand()Landroid/bluetooth/AtCommandResult;
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService$7;->handleReadCommand()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method
