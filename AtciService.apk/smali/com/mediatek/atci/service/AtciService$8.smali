.class Lcom/mediatek/atci/service/AtciService$8;
.super Landroid/bluetooth/AtCommandHandler;
.source "AtciService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/atci/service/AtciService;->initializeAtFMRadio()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/atci/service/AtciService;


# direct methods
.method constructor <init>(Lcom/mediatek/atci/service/AtciService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Landroid/bluetooth/AtCommandHandler;-><init>()V

    return-void
.end method

.method private getFMRadioState()I
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$700(Lcom/mediatek/atci/service/AtciService;)Lcom/mediatek/atci/service/IFMRadioService;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$800(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const-string v2, "ATCIJ"

    const-string v3, "FMRadio Service is null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return v1

    :cond_2
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$900(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "ATCIJ"

    const-string v3, "FMRadio is not power up"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$1000(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$1100(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_4
    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v1}, Lcom/mediatek/atci/service/AtciService;->access$1200(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x3

    goto :goto_0

    :cond_5
    const/4 v1, 0x4

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v2}, Lcom/mediatek/atci/service/AtciService;->access$1300(Lcom/mediatek/atci/service/AtciService;)I

    move-result v0

    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FMRadio frequency="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v2, 0x385

    if-ne v2, v0, :cond_7

    const/4 v1, 0x1

    goto :goto_0

    :cond_7
    const/16 v2, 0x427

    if-ne v2, v0, :cond_1

    const/4 v1, 0x2

    goto :goto_0
.end method

.method private getState()Landroid/bluetooth/AtCommandResult;
    .locals 4

    const-string v1, "Green"

    const-string v2, "getFMRadioState"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$8;->getFMRadioState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v1, v0}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public handleActionCommand()Landroid/bluetooth/AtCommandResult;
    .locals 2

    const-string v0, "Green"

    const-string v1, "handleActionCommand"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$8;->getState()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleReadCommand()Landroid/bluetooth/AtCommandResult;
    .locals 2

    const-string v0, "Green"

    const-string v1, "handleReadCommand"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$8;->getState()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method

.method public handleSetCommand([Ljava/lang/Object;)Landroid/bluetooth/AtCommandResult;
    .locals 14
    .param p1    # [Ljava/lang/Object;

    const v13, 0x42d4999a

    const v12, 0x42b43333

    const/high16 v11, 0x41200000

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    array-length v5, p1

    if-ne v5, v10, :cond_9

    const/4 v1, -0x1

    const/4 v4, 0x1

    aget-object v5, p1, v9

    instance-of v5, v5, Ljava/lang/Integer;

    if-eqz v5, :cond_0

    aget-object v5, p1, v9

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    :cond_0
    if-ltz v1, :cond_9

    const/4 v5, 0x4

    if-gt v1, v5, :cond_9

    const-string v5, "ATCIJ"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Make ECC call with "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    new-instance v6, Landroid/content/Intent;

    iget-object v7, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-class v8, Lcom/mediatek/atci/service/FMRadioService;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v6}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v9}, Lcom/mediatek/atci/service/AtciService;->access$1402(Lcom/mediatek/atci/service/AtciService;Z)Z

    const-string v5, "ATCIJ"

    const-string v6, "FMRadio: can\'t start FMRadio service"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    if-eqz v4, :cond_9

    packed-switch v1, :pswitch_data_0

    :goto_1
    new-instance v5, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v5, v3}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    :goto_2
    return-object v5

    :cond_1
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    new-instance v6, Landroid/content/Intent;

    iget-object v7, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-class v8, Lcom/mediatek/atci/service/FMRadioService;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v7, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v7}, Lcom/mediatek/atci/service/AtciService;->access$1500(Lcom/mediatek/atci/service/AtciService;)Landroid/content/ServiceConnection;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v10}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0

    :pswitch_0
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v9}, Lcom/mediatek/atci/service/AtciService;->access$1002(Lcom/mediatek/atci/service/AtciService;Z)Z

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$900(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$1600(Lcom/mediatek/atci/service/AtciService;)Z

    :goto_3
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v6, "[0]FMR OFF"

    invoke-static {v5, v6}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    const-string v5, "ATCIJ"

    const-string v6, "FMRadio has power down"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :pswitch_1
    const-string v5, "ATCIJ"

    const-string v6, "FMRadio want to tune 90.1"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v9}, Lcom/mediatek/atci/service/AtciService;->access$1002(Lcom/mediatek/atci/service/AtciService;Z)Z

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$900(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v12}, Lcom/mediatek/atci/service/AtciService;->access$1700(Lcom/mediatek/atci/service/AtciService;F)Z

    :goto_4
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v6, "[1]FMR 90.1"

    invoke-static {v5, v6}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$1300(Lcom/mediatek/atci/service/AtciService;)I

    move-result v2

    const-string v5, "ATCIJ"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FMRadio frequency="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v5, 0x385

    if-eq v5, v2, :cond_4

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v12}, Lcom/mediatek/atci/service/AtciService;->access$1800(Lcom/mediatek/atci/service/AtciService;F)Z

    goto :goto_4

    :cond_4
    const-string v5, "ATCIJ"

    const-string v6, "FMRadio has tune to 90.1"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :pswitch_2
    const-string v5, "ATCIJ"

    const-string v6, "FMRadio want to tune 106.3"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v9}, Lcom/mediatek/atci/service/AtciService;->access$1002(Lcom/mediatek/atci/service/AtciService;Z)Z

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$900(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v13}, Lcom/mediatek/atci/service/AtciService;->access$1700(Lcom/mediatek/atci/service/AtciService;F)Z

    :goto_5
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v6, "[2]FMR 106.3"

    invoke-static {v5, v6}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    :cond_5
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$1300(Lcom/mediatek/atci/service/AtciService;)I

    move-result v2

    const-string v5, "ATCIJ"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FMRadio frequency="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v5, 0x427

    if-eq v5, v2, :cond_6

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v13}, Lcom/mediatek/atci/service/AtciService;->access$1800(Lcom/mediatek/atci/service/AtciService;F)Z

    goto :goto_5

    :cond_6
    const-string v5, "ATCIJ"

    const-string v6, "FMRadio has tune to 106.3"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :pswitch_3
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v10}, Lcom/mediatek/atci/service/AtciService;->access$1202(Lcom/mediatek/atci/service/AtciService;Z)Z

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$900(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$1300(Lcom/mediatek/atci/service/AtciService;)I

    move-result v2

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    int-to-float v6, v2

    div-float/2addr v6, v11

    invoke-static {v5, v6}, Lcom/mediatek/atci/service/AtciService;->access$1700(Lcom/mediatek/atci/service/AtciService;F)Z

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v9, v2, v10}, Lcom/mediatek/atci/service/AtciService;->access$1900(Lcom/mediatek/atci/service/AtciService;ZIZ)V

    :goto_6
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v6, "[3]FMR SEARCH UP"

    invoke-static {v5, v6}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    :cond_7
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$1300(Lcom/mediatek/atci/service/AtciService;)I

    move-result v2

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v9, v2, v10}, Lcom/mediatek/atci/service/AtciService;->access$1900(Lcom/mediatek/atci/service/AtciService;ZIZ)V

    goto :goto_6

    :pswitch_4
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v9}, Lcom/mediatek/atci/service/AtciService;->access$1202(Lcom/mediatek/atci/service/AtciService;Z)Z

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$900(Lcom/mediatek/atci/service/AtciService;)Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$1300(Lcom/mediatek/atci/service/AtciService;)I

    move-result v2

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    int-to-float v6, v2

    div-float/2addr v6, v11

    invoke-static {v5, v6}, Lcom/mediatek/atci/service/AtciService;->access$1700(Lcom/mediatek/atci/service/AtciService;F)Z

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v9, v2, v9}, Lcom/mediatek/atci/service/AtciService;->access$1900(Lcom/mediatek/atci/service/AtciService;ZIZ)V

    :goto_7
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v6, "[4]FMR SEARCH DOWN"

    invoke-static {v5, v6}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    :cond_8
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5}, Lcom/mediatek/atci/service/AtciService;->access$1300(Lcom/mediatek/atci/service/AtciService;)I

    move-result v2

    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-static {v5, v9, v2, v9}, Lcom/mediatek/atci/service/AtciService;->access$1900(Lcom/mediatek/atci/service/AtciService;ZIZ)V

    goto :goto_7

    :cond_9
    iget-object v5, p0, Lcom/mediatek/atci/service/AtciService$8;->this$0:Lcom/mediatek/atci/service/AtciService;

    const-string v6, "FMRX ERROR"

    invoke-static {v5, v6}, Lcom/mediatek/atci/service/AtciService;->access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Landroid/bluetooth/AtCommandResult;

    invoke-direct {v5, v3}, Landroid/bluetooth/AtCommandResult;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public handleTestCommand()Landroid/bluetooth/AtCommandResult;
    .locals 2

    const-string v0, "Green"

    const-string v1, "handleTestCommand"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService$8;->getState()Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    return-object v0
.end method
