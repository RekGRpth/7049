.class Lcom/mediatek/atci/service/AtciService$SeekThread;
.super Ljava/lang/Thread;
.source "AtciService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/atci/service/AtciService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SeekThread"
.end annotation


# instance fields
.field public mCrntStation:I

.field public mSeekDirection:Z

.field final synthetic this$0:Lcom/mediatek/atci/service/AtciService;


# direct methods
.method public constructor <init>(Lcom/mediatek/atci/service/AtciService;IZ)V
    .locals 1
    .param p2    # I
    .param p3    # Z

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->this$0:Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput v0, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->mCrntStation:I

    iput-boolean v0, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->mSeekDirection:Z

    iput p2, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->mCrntStation:I

    iput-boolean p3, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->mSeekDirection:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const/16 v9, 0x438

    const/16 v8, 0x36b

    const/high16 v7, 0x41200000

    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->this$0:Lcom/mediatek/atci/service/AtciService;

    iget v5, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->mCrntStation:I

    int-to-float v5, v5

    div-float/2addr v5, v7

    iget-boolean v6, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->mSeekDirection:Z

    invoke-static {v4, v5, v6}, Lcom/mediatek/atci/service/AtciService;->access$2700(Lcom/mediatek/atci/service/AtciService;FZ)F

    move-result v1

    mul-float v4, v1, v7

    float-to-int v2, v4

    if-ge v2, v9, :cond_0

    if-gt v2, v8, :cond_1

    :cond_0
    iget-boolean v4, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->mSeekDirection:Z

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->this$0:Lcom/mediatek/atci/service/AtciService;

    const/high16 v5, 0x42d80000

    iget-boolean v6, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->mSeekDirection:Z

    invoke-static {v4, v5, v6}, Lcom/mediatek/atci/service/AtciService;->access$2700(Lcom/mediatek/atci/service/AtciService;FZ)F

    move-result v1

    mul-float v4, v1, v7

    float-to-int v2, v4

    :cond_1
    :goto_0
    if-ge v2, v9, :cond_2

    if-gt v2, v8, :cond_4

    :cond_2
    const-string v4, "ATCIJ"

    const-string v5, "Error: Can not search previous station."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->this$0:Lcom/mediatek/atci/service/AtciService;

    iget-object v4, v4, Lcom/mediatek/atci/service/AtciService;->mFMRadioHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/os/Message;->setTarget(Landroid/os/Handler;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "MSGID"

    const/4 v5, 0x7

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    :goto_1
    return-void

    :cond_3
    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->this$0:Lcom/mediatek/atci/service/AtciService;

    const/high16 v5, 0x42af0000

    iget-boolean v6, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->mSeekDirection:Z

    invoke-static {v4, v5, v6}, Lcom/mediatek/atci/service/AtciService;->access$2700(Lcom/mediatek/atci/service/AtciService;FZ)F

    move-result v1

    mul-float v4, v1, v7

    float-to-int v2, v4

    goto :goto_0

    :cond_4
    const-string v4, "ATCIJ"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Send message to tune to recently seeked station: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    iget-object v4, p0, Lcom/mediatek/atci/service/AtciService$SeekThread;->this$0:Lcom/mediatek/atci/service/AtciService;

    iget-object v4, v4, Lcom/mediatek/atci/service/AtciService;->mFMRadioHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/os/Message;->setTarget(Landroid/os/Handler;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "MSGID"

    const/4 v5, 0x6

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "SEEK_STATION"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    const-string v4, "ATCIJ"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Send message to tune to recently seeked station: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
