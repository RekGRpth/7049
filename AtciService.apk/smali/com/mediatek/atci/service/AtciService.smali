.class public Lcom/mediatek/atci/service/AtciService;
.super Landroid/app/Service;
.source "AtciService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/atci/service/AtciService$SeekThread;,
        Lcom/mediatek/atci/service/AtciService$InitialThread;,
        Lcom/mediatek/atci/service/AtciService$AtciReceiver;
    }
.end annotation


# static fields
.field static final ATCI_MAX_BUFFER_BYTES:I = 0x400

.field private static final DBG:Z = true

.field public static final DEFAULT_FREQUENCY:I = 0x3e8

.field static final ECALL_NUMBER:Ljava/lang/String; = "112"

.field private static final EMERGENCY_CALL_ACTION:Ljava/lang/String; = "android.location.agps.EMERGENCY_CALL"

.field public static final FIRST_FREQUENCY:I = 0x385

.field private static final FMRADIO_FIRST_STATION:I = 0x1

.field private static final FMRADIO_POWER_OFF:I = 0x0

.field private static final FMRADIO_SECOND_STATION:I = 0x2

.field private static final FMRADIO_SEEK_DOWN:I = 0x4

.field private static final FMRADIO_SEEK_UP:I = 0x3

.field private static final FREQUENCY_CONVERT_RATE:I = 0xa

.field private static final HEX_EX_STRING:[C

.field public static final HIGHEST_STATION:I = 0x438

.field static final LOG_TAG:Ljava/lang/String; = "ATCIJ"

.field public static final LOWEST_STATION:I = 0x36b

.field public static final MSGID_SEEK_FAIL:I = 0x7

.field public static final MSGID_SEEK_FINISH:I = 0x6

.field static final NUM_ELEVEN:I = 0xb

.field static final NUM_FIVE:I = 0x5

.field static final NUM_FORTY_TWO:I = 0x2a

.field static final NUM_FOUR:I = 0x4

.field static final NUM_ONE:I = 0x1

.field static final NUM_SEVEN:I = 0x7

.field static final NUM_SIX:I = 0x6

.field static final NUM_SIXTEEN:I = 0x10

.field static final NUM_TEN:I = 0xa

.field static final NUM_THREE:I = 0x3

.field static final NUM_TWELVE:I = 0xc

.field static final NUM_TWO:I = 0x2

.field static final NUM_ZERO:I = 0x0

.field public static final SECOND_FREQUENCY:I = 0x427

.field static final SOCKET_NAME_ATCI:Ljava/lang/String; = "atci-serv-fw"

.field static final SOCKET_OPEN_RETRY_MILLIS:I = 0xfa0

.field static final THREAD_SLEEP_ONE:I = 0x7d0

.field static final THREAD_SLEEP_TWO:I = 0xfa0

.field public static final TYPE_MSGID:Ljava/lang/String; = "MSGID"

.field public static final TYPE_SEEK_STATION:Ljava/lang/String; = "SEEK_STATION"

.field private static final WAIT_TIME:I = 0x3e8


# instance fields
.field mAtParser:Landroid/bluetooth/AtParser;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCameraAVROn:I

.field private mCameraOn:I

.field private mConnection:Landroid/content/ServiceConnection;

.field mContext:Landroid/content/Context;

.field private mCurrentStation:I

.field private mEcallState:I

.field private mEccStateReceiver:Landroid/content/BroadcastReceiver;

.field private mFMRadioAtCommandResult:Landroid/bluetooth/AtCommandResult;

.field mFMRadioHandler:Landroid/os/Handler;

.field private mFMRadioSeekThread:Ljava/lang/Thread;

.field private mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

.field private mFullWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mIsFMRadioDoSeek:Z

.field private mIsFMRadioPlaying:Z

.field private mIsFMRadioSeekUp:Z

.field private mIsFMRadioSeeking:Z

.field private mIsFMRadioServiceBinded:Z

.field private mIsFMRadioServiceStarted:Z

.field private mMp3PlayerMode:C

.field mReceiver:Lcom/mediatek/atci/service/AtciService$AtciReceiver;

.field mReceiverThread:Ljava/lang/Thread;

.field mSocket:Landroid/net/LocalSocket;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/atci/service/AtciService;->HEX_EX_STRING:[C

    const-string v0, "atciserv_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void

    :array_0
    .array-data 2
        0x2s
        0x3s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput v1, p0, Lcom/mediatek/atci/service/AtciService;->mEcallState:I

    const/16 v0, 0x30

    iput-char v0, p0, Lcom/mediatek/atci/service/AtciService;->mMp3PlayerMode:C

    iput-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    iput-boolean v1, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioPlaying:Z

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/mediatek/atci/service/AtciService;->mCurrentStation:I

    iput-boolean v1, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioServiceStarted:Z

    iput-boolean v1, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioServiceBinded:Z

    iput-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mAudioManager:Landroid/media/AudioManager;

    iput-boolean v1, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioSeeking:Z

    iput-boolean v1, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioDoSeek:Z

    iput-boolean v1, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioSeekUp:Z

    iput-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioAtCommandResult:Landroid/bluetooth/AtCommandResult;

    iput-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioSeekThread:Ljava/lang/Thread;

    iput v1, p0, Lcom/mediatek/atci/service/AtciService;->mCameraOn:I

    iput v1, p0, Lcom/mediatek/atci/service/AtciService;->mCameraAVROn:I

    new-instance v0, Lcom/mediatek/atci/service/AtciService$21;

    invoke-direct {v0, p0}, Lcom/mediatek/atci/service/AtciService$21;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    iput-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mEccStateReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/atci/service/AtciService$22;

    invoke-direct {v0, p0}, Lcom/mediatek/atci/service/AtciService$22;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    iput-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/atci/service/AtciService$23;

    invoke-direct {v0, p0}, Lcom/mediatek/atci/service/AtciService$23;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    iput-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/atci/service/AtciService;->formatResponse(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/atci/service/AtciService;)I
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    iget v0, p0, Lcom/mediatek/atci/service/AtciService;->mEcallState:I

    return v0
.end method

.method static synthetic access$1000(Lcom/mediatek/atci/service/AtciService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    iget-boolean v0, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioDoSeek:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/mediatek/atci/service/AtciService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioDoSeek:Z

    return p1
.end method

.method static synthetic access$102(Lcom/mediatek/atci/service/AtciService;I)I
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/atci/service/AtciService;->mEcallState:I

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/atci/service/AtciService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    iget-boolean v0, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioSeeking:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/mediatek/atci/service/AtciService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    iget-boolean v0, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioSeekUp:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/mediatek/atci/service/AtciService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioSeekUp:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/mediatek/atci/service/AtciService;)I
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->getFrequency()I

    move-result v0

    return v0
.end method

.method static synthetic access$1402(Lcom/mediatek/atci/service/AtciService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioPlaying:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/mediatek/atci/service/AtciService;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/mediatek/atci/service/AtciService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->powerDown()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/mediatek/atci/service/AtciService;F)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # F

    invoke-direct {p0, p1}, Lcom/mediatek/atci/service/AtciService;->powerUp(F)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/mediatek/atci/service/AtciService;F)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # F

    invoke-direct {p0, p1}, Lcom/mediatek/atci/service/AtciService;->tune(F)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/mediatek/atci/service/AtciService;ZIZ)V
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # Z
    .param p2    # I
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/atci/service/AtciService;->seekStation(ZIZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/atci/service/AtciService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->readBTAddressNative()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/atci/service/AtciService;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # Ljava/util/Locale;

    invoke-direct {p0, p1}, Lcom/mediatek/atci/service/AtciService;->getLangDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(Lcom/mediatek/atci/service/AtciService;)C
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    iget-char v0, p0, Lcom/mediatek/atci/service/AtciService;->mMp3PlayerMode:C

    return v0
.end method

.method static synthetic access$2102(Lcom/mediatek/atci/service/AtciService;C)C
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # C

    iput-char p1, p0, Lcom/mediatek/atci/service/AtciService;->mMp3PlayerMode:C

    return p1
.end method

.method static synthetic access$2200(Lcom/mediatek/atci/service/AtciService;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mFullWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/mediatek/atci/service/AtciService;)I
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    iget v0, p0, Lcom/mediatek/atci/service/AtciService;->mCameraOn:I

    return v0
.end method

.method static synthetic access$2302(Lcom/mediatek/atci/service/AtciService;I)I
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/atci/service/AtciService;->mCameraOn:I

    return p1
.end method

.method static synthetic access$2400(Lcom/mediatek/atci/service/AtciService;)I
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    iget v0, p0, Lcom/mediatek/atci/service/AtciService;->mCameraAVROn:I

    return v0
.end method

.method static synthetic access$2402(Lcom/mediatek/atci/service/AtciService;I)I
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/atci/service/AtciService;->mCameraAVROn:I

    return p1
.end method

.method static synthetic access$2500(Lcom/mediatek/atci/service/AtciService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->openDevice()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Lcom/mediatek/atci/service/AtciService;)V
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->playFM()V

    return-void
.end method

.method static synthetic access$2700(Lcom/mediatek/atci/service/AtciService;FZ)F
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # F
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/atci/service/AtciService;->seek(FZ)F

    move-result v0

    return v0
.end method

.method static synthetic access$2800(Lcom/mediatek/atci/service/AtciService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->isServiceInit()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Lcom/mediatek/atci/service/AtciService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/atci/service/AtciService;->initService(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/atci/service/AtciService;->writeBTAddressNative(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3000(Lcom/mediatek/atci/service/AtciService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->isDeviceOpen()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/atci/service/AtciService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->queryBTTestModeNative()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/atci/service/AtciService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->leaveBTTestModeNative()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/mediatek/atci/service/AtciService;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/atci/service/AtciService;->enterBTTestModeNative(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/atci/service/AtciService;)Lcom/mediatek/atci/service/IFMRadioService;
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    return-object v0
.end method

.method static synthetic access$702(Lcom/mediatek/atci/service/AtciService;Lcom/mediatek/atci/service/IFMRadioService;)Lcom/mediatek/atci/service/IFMRadioService;
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # Lcom/mediatek/atci/service/IFMRadioService;

    iput-object p1, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    return-object p1
.end method

.method static synthetic access$800(Lcom/mediatek/atci/service/AtciService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    iget-boolean v0, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioServiceBinded:Z

    return v0
.end method

.method static synthetic access$802(Lcom/mediatek/atci/service/AtciService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/AtciService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioServiceBinded:Z

    return p1
.end method

.method static synthetic access$900(Lcom/mediatek/atci/service/AtciService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/AtciService;

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->isPowerUp()Z

    move-result v0

    return v0
.end method

.method private declared-synchronized acquireWakeLock()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private native enterBTTestModeNative(Ljava/lang/String;)Z
.end method

.method private formatResponse(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "%c%s%c"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lcom/mediatek/atci/service/AtciService;->HEX_EX_STRING:[C

    aget-char v2, v2, v3

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    const/4 v2, 0x2

    sget-object v3, Lcom/mediatek/atci/service/AtciService;->HEX_EX_STRING:[C

    aget-char v3, v3, v4

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getFrequency()I
    .locals 5

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.getFrequency"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: No service interface."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadio.getFrequency: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v2}, Lcom/mediatek/atci/service/IFMRadioService;->getFrequency()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "ATCIJ"

    const-string v3, "Exception: Cannot call service function."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getLangDisplayName(Ljava/util/Locale;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/util/Locale;

    const/4 v3, 0x1

    invoke-virtual {p1, p1}, Ljava/util/Locale;->getDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private initService(I)V
    .locals 4
    .param p1    # I

    const-string v1, "ATCIJ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>> FMRadio.initService: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v1, :cond_0

    const-string v1, "ATCIJ"

    const-string v2, "Error: No service interface."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v1, "ATCIJ"

    const-string v2, "<<< FMRadio.initService"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v1, p1}, Lcom/mediatek/atci/service/IFMRadioService;->initService(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "ATCIJ"

    const-string v2, "Exception: Cannot call service function."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isAntennaAvailable()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mContext:Landroid/content/Context;

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    return v0
.end method

.method private isDeviceOpen()Z
    .locals 5

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.isDeviceOpen"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: No service interface."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadio.isDeviceOpen: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v2}, Lcom/mediatek/atci/service/IFMRadioService;->isDeviceOpen()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "ATCIJ"

    const-string v3, "Exception: Cannot call service function."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isPowerUp()Z
    .locals 5

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.isPowerUp"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: No service interface."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadio.isPowerUp: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v2}, Lcom/mediatek/atci/service/IFMRadioService;->isPowerUp()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "ATCIJ"

    const-string v3, "Exception: Cannot call service function."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isServiceInit()Z
    .locals 5

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.isServiceInit"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: No service interface."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadio.isServiceInit: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v2}, Lcom/mediatek/atci/service/IFMRadioService;->isServiceInit()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "ATCIJ"

    const-string v3, "Exception: Cannot call service function."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private native leaveBTTestModeNative()Z
.end method

.method private openDevice()Z
    .locals 5

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.openDevice"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: No service interface."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadio.openDevice: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v2}, Lcom/mediatek/atci/service/IFMRadioService;->openDevice()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "ATCIJ"

    const-string v3, "Exception: Cannot call service function."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private playFM()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v1, "ATCIJ"

    const-string v2, ">>> PlayFM"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v3}, Lcom/mediatek/atci/service/AtciService;->setMute(Z)I

    iget v1, p0, Lcom/mediatek/atci/service/AtciService;->mCurrentStation:I

    int-to-float v1, v1

    const/high16 v2, 0x41200000

    div-float/2addr v1, v2

    invoke-direct {p0, v1}, Lcom/mediatek/atci/service/AtciService;->powerUp(F)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v3, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioPlaying:Z

    invoke-direct {p0, v4}, Lcom/mediatek/atci/service/AtciService;->setMute(Z)I

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->isAntennaAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v3}, Lcom/mediatek/atci/service/AtciService;->switchAntenna(I)I

    :cond_0
    :goto_0
    const-string v1, "ATCIJ"

    const-string v2, "<<< PlayFM"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    invoke-direct {p0, v3}, Lcom/mediatek/atci/service/AtciService;->setMute(Z)I

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->powerDown()Z

    iput-boolean v4, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioPlaying:Z

    const-string v1, "ATCIJ"

    const-string v2, "Error: Can not power up."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private powerDown()Z
    .locals 5

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.powerDown"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: No service interface."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadio.powerDown: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v2}, Lcom/mediatek/atci/service/IFMRadioService;->powerDown()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "ATCIJ"

    const-string v3, "Exception: Cannot call service function."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private powerUp(F)Z
    .locals 5
    .param p1    # F

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.powerUp"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: No service interface."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadio.powerUp: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v2, p1}, Lcom/mediatek/atci/service/IFMRadioService;->powerUp(F)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "ATCIJ"

    const-string v3, "Exception: Cannot call service function."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private native queryBTTestModeNative()Z
.end method

.method private native readBTAddressNative()Ljava/lang/String;
.end method

.method private declared-synchronized releaseWakeLock()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private seek(FZ)F
    .locals 5
    .param p1    # F
    .param p2    # Z

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.seek"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: No service interface."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadio.seek: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v2, p1, p2}, Lcom/mediatek/atci/service/IFMRadioService;->seek(FZ)F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "ATCIJ"

    const-string v3, "Exception: Cannot call service function."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private seekStation(ZIZ)V
    .locals 2
    .param p1    # Z
    .param p2    # I
    .param p3    # Z

    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioSeeking:Z

    if-eqz v0, :cond_0

    const-string v0, "ATCIJ"

    const-string v1, "Warning: already seeking"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioSeeking:Z

    new-instance v0, Lcom/mediatek/atci/service/AtciService$SeekThread;

    invoke-direct {v0, p0, p2, p3}, Lcom/mediatek/atci/service/AtciService$SeekThread;-><init>(Lcom/mediatek/atci/service/AtciService;IZ)V

    iput-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioSeekThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioSeekThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioSeekThread:Ljava/lang/Thread;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/atci/service/AtciService;->mIsFMRadioSeeking:Z

    goto :goto_0
.end method

.method private setMute(Z)I
    .locals 5
    .param p1    # Z

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.setMute"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: No service interface."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadio.setMute: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v2, p1}, Lcom/mediatek/atci/service/IFMRadioService;->setMute(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "ATCIJ"

    const-string v3, "Exception: Cannot call service function."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private tune(F)Z
    .locals 5
    .param p1    # F

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.tune"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: No service interface."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadio.tune: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v2, p1}, Lcom/mediatek/atci/service/IFMRadioService;->tune(F)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "ATCIJ"

    const-string v3, "Exception: Cannot call service function."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private native writeBTAddressNative(Ljava/lang/String;)Z
.end method


# virtual methods
.method public getAtParser()Landroid/bluetooth/AtParser;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    return-object v0
.end method

.method protected handleInput(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->acquireWakeLock()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    invoke-virtual {v3, p1}, Landroid/bluetooth/AtParser;->process(Ljava/lang/String;)Landroid/bluetooth/AtCommandResult;

    move-result-object v0

    const-string v3, "ATCIJ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Processing "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " took "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/bluetooth/AtCommandResult;->getResultCode()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const-string v3, "ATCIJ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error processing <"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "> with result <"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/bluetooth/AtCommandResult;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/bluetooth/AtCommandResult;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/atci/service/AtciService;->sendURC(Ljava/lang/String;)Z

    invoke-direct {p0}, Lcom/mediatek/atci/service/AtciService;->releaseWakeLock()V

    return-void
.end method

.method protected initializeAtComCamera()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%CAM"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$19;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$19;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%AVR"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$20;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$20;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    return-void
.end method

.method protected initializeAtDB()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%INITDB"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$1;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$1;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%DBCHK"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$2;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$2;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    return-void
.end method

.method protected initializeAtFMRadio()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%FMR"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$8;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$8;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    return-void
.end method

.method protected initializeAtLanguage()V
    .locals 3

    const-string v0, "ATCIJ"

    const-string v1, "initializeAtLanguage"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%LANG"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$9;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$9;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    return-void
.end method

.method protected initializeAtMp3Play()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%MPT"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$10;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$10;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    return-void
.end method

.method protected initializeAtNoSleep()V
    .locals 3

    const-string v0, "ATCIJ"

    const-string v1, "initializeAtSleep"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%NOSLEEP"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$11;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$11;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    return-void
.end method

.method protected initializeAtOSVer()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%OSVER"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$3;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$3;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    return-void
.end method

.method protected initializeAtParser()V
    .locals 2

    const-string v0, "ATCIJ"

    const-string v1, "initializeAtParser"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService;->initializeAtTelephony()V

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService;->initializeAtFMRadio()V

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService;->initializeAtMp3Play()V

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService;->initializeAtDB()V

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService;->initializeAtOSVer()V

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService;->initializeAtNoSleep()V

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService;->initializeAtLanguage()V

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService;->initializeAtComCamera()V

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService;->initializeAtSystemCall()V

    return-void
.end method

.method protected initializeAtSystemCall()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v2, "+SN"

    new-instance v3, Lcom/mediatek/atci/service/AtciService$12;

    invoke-direct {v3, p0}, Lcom/mediatek/atci/service/AtciService$12;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v1, v2, v3}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v2, "+SHUTDOWN"

    new-instance v3, Lcom/mediatek/atci/service/AtciService$13;

    invoke-direct {v3, p0}, Lcom/mediatek/atci/service/AtciService$13;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v1, v2, v3}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v2, "+MODEL"

    new-instance v3, Lcom/mediatek/atci/service/AtciService$14;

    invoke-direct {v3, p0}, Lcom/mediatek/atci/service/AtciService$14;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v1, v2, v3}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v2, "+POWERKEY"

    new-instance v3, Lcom/mediatek/atci/service/AtciService$15;

    invoke-direct {v3, p0}, Lcom/mediatek/atci/service/AtciService$15;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v1, v2, v3}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    new-instance v0, Lcom/mediatek/atci/service/AtciService$16;

    invoke-direct {v0, p0}, Lcom/mediatek/atci/service/AtciService$16;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v2, "+FACTORYRESET"

    new-instance v3, Lcom/mediatek/atci/service/AtciService$17;

    invoke-direct {v3, p0, v0}, Lcom/mediatek/atci/service/AtciService$17;-><init>(Lcom/mediatek/atci/service/AtciService;Landroid/os/storage/StorageEventListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    iget-object v1, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v2, "+WITOF"

    new-instance v3, Lcom/mediatek/atci/service/AtciService$18;

    invoke-direct {v3, p0}, Lcom/mediatek/atci/service/AtciService$18;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v1, v2, v3}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    return-void
.end method

.method protected initializeAtTelephony()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%FLIGHT"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$4;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$4;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%ECALL"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$5;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$5;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%BTAD"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$6;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$6;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    iget-object v0, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    const-string v1, "%BTTM"

    new-instance v2, Lcom/mediatek/atci/service/AtciService$7;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$7;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/AtParser;->register(Ljava/lang/String;Landroid/bluetooth/AtCommandHandler;)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v2, Lcom/mediatek/atci/service/AtciService$AtciReceiver;

    invoke-direct {v2, p0}, Lcom/mediatek/atci/service/AtciService$AtciReceiver;-><init>(Lcom/mediatek/atci/service/AtciService;)V

    iput-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mReceiver:Lcom/mediatek/atci/service/AtciService$AtciReceiver;

    new-instance v2, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService;->mReceiver:Lcom/mediatek/atci/service/AtciService$AtciReceiver;

    const-string v4, "AtciReceiver"

    invoke-direct {v2, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mReceiverThread:Ljava/lang/Thread;

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mContext:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    const/4 v2, 0x1

    const-string v3, "AtciService"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    const v2, 0x1000001a

    const-string v3, "AtciService"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFullWakeLock:Landroid/os/PowerManager$WakeLock;

    new-instance v2, Landroid/bluetooth/AtParser;

    invoke-direct {v2}, Landroid/bluetooth/AtParser;-><init>()V

    iput-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mAtParser:Landroid/bluetooth/AtParser;

    invoke-virtual {p0}, Lcom/mediatek/atci/service/AtciService;->initializeAtParser()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.location.agps.EMERGENCY_CALL"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService;->mEccStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 5

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v3}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_0
    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void

    :catch_0
    move-exception v0

    const-string v3, "ATCIJ"

    const-string v4, " IOException"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v3, "ATCIJ"

    const-string v4, " NullPointerException"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public declared-synchronized sendURC(Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, "ATCIJ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "URC Processing:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    :try_start_1
    iget-object v3, p0, Lcom/mediatek/atci/service/AtciService;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v3}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    move v3, v2

    :goto_1
    monitor-exit p0

    return v3

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public switchAntenna(I)I
    .locals 5
    .param p1    # I

    const-string v2, "ATCIJ"

    const-string v3, ">>> FMRadio.switchAntenna"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    if-nez v2, :cond_0

    const-string v2, "ATCIJ"

    const-string v3, "Error: No service interface."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "ATCIJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadio.switchAntenna: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/AtciService;->mFMRadioService:Lcom/mediatek/atci/service/IFMRadioService;

    invoke-interface {v2, p1}, Lcom/mediatek/atci/service/IFMRadioService;->switchAntenna(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "ATCIJ"

    const-string v3, "FMRadio Exception: Cannot call service function."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
