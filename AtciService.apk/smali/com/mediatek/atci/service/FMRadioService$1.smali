.class Lcom/mediatek/atci/service/FMRadioService$1;
.super Lcom/mediatek/atci/service/IFMRadioService$Stub;
.source "FMRadioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/atci/service/FMRadioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/atci/service/FMRadioService;


# direct methods
.method constructor <init>(Lcom/mediatek/atci/service/FMRadioService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-direct {p0}, Lcom/mediatek/atci/service/IFMRadioService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public closeDevice()Z
    .locals 4

    const-string v1, "FMRadioService"

    const-string v2, ">>> FMRadioService.closeDevice"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v1}, Lcom/mediatek/atci/service/FMRadioService;->access$000(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/mediatek/atci/service/AtciFMRadioNative;->closedev()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/mediatek/atci/service/FMRadioService;->access$002(Lcom/mediatek/atci/service/FMRadioService;Z)Z

    :goto_0
    const-string v1, "FMRadioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<<< FMRadioService.closeDevice: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    const-string v1, "FMRadioService"

    const-string v2, "Error: closedev failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v1, "FMRadioService"

    const-string v2, "Error: device is already closed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getFrequency()I
    .locals 3

    const-string v0, "FMRadioService"

    const-string v1, ">>> FMRadioService.getFrequency"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "FMRadioService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<<< FMRadioService.getFrequency: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v2}, Lcom/mediatek/atci/service/FMRadioService;->access$100(Lcom/mediatek/atci/service/FMRadioService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v0}, Lcom/mediatek/atci/service/FMRadioService;->access$100(Lcom/mediatek/atci/service/FMRadioService;)I

    move-result v0

    return v0
.end method

.method public initService(I)V
    .locals 3
    .param p1    # I

    const-string v0, "FMRadioService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>> FMRadioService.initService: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/atci/service/FMRadioService;->access$602(Lcom/mediatek/atci/service/FMRadioService;Z)Z

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v0, p1}, Lcom/mediatek/atci/service/FMRadioService;->access$102(Lcom/mediatek/atci/service/FMRadioService;I)I

    const-string v0, "FMRadioService"

    const-string v1, "<<< FMRadioService.initService"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public isDeviceOpen()Z
    .locals 3

    const-string v0, "FMRadioService"

    const-string v1, ">>> FMRadioService.isDeviceOpen"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "FMRadioService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<<< FMRadioService.isDeviceOpen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v2}, Lcom/mediatek/atci/service/FMRadioService;->access$000(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v0}, Lcom/mediatek/atci/service/FMRadioService;->access$000(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v0

    return v0
.end method

.method public isEarphoneUsed()Z
    .locals 3

    const-string v0, "FMRadioService"

    const-string v1, ">>> FMRadioService.isEarphoneUsed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "FMRadioService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<<< FMRadioService.isEarphoneUsed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v2}, Lcom/mediatek/atci/service/FMRadioService;->access$500(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v0}, Lcom/mediatek/atci/service/FMRadioService;->access$500(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v0

    return v0
.end method

.method public isPowerUp()Z
    .locals 3

    const-string v0, "FMRadioService"

    const-string v1, ">>> FMRadioService.isPowerUp"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "FMRadioService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<<< FMRadioService.isPowerUp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v2}, Lcom/mediatek/atci/service/FMRadioService;->access$200(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v0}, Lcom/mediatek/atci/service/FMRadioService;->access$200(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v0

    return v0
.end method

.method public isServiceInit()Z
    .locals 3

    const-string v0, "FMRadioService"

    const-string v1, ">>> FMRadioService.isServiceInit"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "FMRadioService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<<< FMRadioService.isServiceInit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v2}, Lcom/mediatek/atci/service/FMRadioService;->access$600(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v0}, Lcom/mediatek/atci/service/FMRadioService;->access$600(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v0

    return v0
.end method

.method public openDevice()Z
    .locals 4

    const-string v1, "FMRadioService"

    const-string v2, ">>> FMRadioService.openDevice"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v1}, Lcom/mediatek/atci/service/FMRadioService;->access$000(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "FMRadioService"

    const-string v2, "Error: device is already open."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    const-string v1, "FMRadioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<<< FMRadioService.openDevice: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    invoke-static {}, Lcom/mediatek/atci/service/AtciFMRadioNative;->opendev()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/mediatek/atci/service/FMRadioService;->access$002(Lcom/mediatek/atci/service/FMRadioService;Z)Z

    goto :goto_0

    :cond_1
    const-string v1, "FMRadioService"

    const-string v2, "Error: opendev failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public powerDown()Z
    .locals 5

    const/4 v4, 0x0

    const-string v2, "FMRadioService"

    const-string v3, ">>> FMRadioService.powerDown"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v2}, Lcom/mediatek/atci/service/FMRadioService;->access$200(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v2, v4}, Lcom/mediatek/atci/service/FMRadioService;->access$300(Lcom/mediatek/atci/service/FMRadioService;Z)V

    invoke-static {v4}, Lcom/mediatek/atci/service/AtciFMRadioNative;->powerdown(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v2, v4}, Lcom/mediatek/atci/service/FMRadioService;->access$202(Lcom/mediatek/atci/service/FMRadioService;Z)Z

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mediatek.FMRadio.FMRadioService.ACTION_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "EXTRA_FMRADIO_ISPOWERUP"

    iget-object v3, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v3}, Lcom/mediatek/atci/service/FMRadioService;->access$200(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-virtual {v2, v1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :goto_0
    const-string v2, "FMRadioService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< FMRadioService.powerDown: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    const-string v2, "FMRadioService"

    const-string v3, "Error: powerdown failed."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v2, "FMRadioService"

    const-string v3, "Error: device is already power down."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public powerUp(F)Z
    .locals 8
    .param p1    # F

    const/4 v7, 0x1

    const-string v4, "FMRadioService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ">>> FMRadioService.powerUp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    const/high16 v5, 0x41200000

    mul-float/2addr v5, p1

    float-to-int v5, v5

    invoke-static {v4, v5}, Lcom/mediatek/atci/service/FMRadioService;->access$102(Lcom/mediatek/atci/service/FMRadioService;I)I

    iget-object v4, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v4}, Lcom/mediatek/atci/service/FMRadioService;->access$200(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "FMRadioService"

    const-string v5, "Error: device is already power up."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    const-string v4, "FMRadioService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<<< FMRadioService.powerUp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    const-wide/16 v4, 0xa

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-static {p1}, Lcom/mediatek/atci/service/AtciFMRadioNative;->powerup(F)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v4, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v4, v7}, Lcom/mediatek/atci/service/FMRadioService;->access$300(Lcom/mediatek/atci/service/FMRadioService;Z)V

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.music.musicservicecommand.pause"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-virtual {v4, v3}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v4, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v4, v7}, Lcom/mediatek/atci/service/FMRadioService;->access$202(Lcom/mediatek/atci/service/FMRadioService;Z)Z

    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.mediatek.FMRadio.FMRadioService.ACTION_STATE_CHANGED"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "EXTRA_FMRADIO_ISPOWERUP"

    iget-object v5, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v5}, Lcom/mediatek/atci/service/FMRadioService;->access$200(Lcom/mediatek/atci/service/FMRadioService;)Z

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-virtual {v4, v2}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    const-string v4, "FMRadioService"

    const-string v5, "Exception: Thread sleep."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    const-string v4, "FMRadioService"

    const-string v5, "Error: powerup failed."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resumeFMAudio()V
    .locals 2

    const-string v0, "FMRadioService"

    const-string v1, ">>> FMRadioService.resumeFMAudio"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/atci/service/FMRadioService;->access$300(Lcom/mediatek/atci/service/FMRadioService;Z)V

    const-string v0, "FMRadioService"

    const-string v1, "<<< FMRadioService.resumeFMAudio"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public seek(FZ)F
    .locals 4
    .param p1    # F
    .param p2    # Z

    const-string v1, "FMRadioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>> FMRadioService.seek: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, p2}, Lcom/mediatek/atci/service/AtciFMRadioNative;->seek(FZ)F

    move-result v0

    const-string v1, "FMRadioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<<< FMRadioService.seek: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public setMute(Z)I
    .locals 4
    .param p1    # Z

    const-string v1, "FMRadioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>> FMRadioService.setMute: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/mediatek/atci/service/AtciFMRadioNative;->setmute(Z)I

    move-result v0

    const-string v1, "FMRadioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<<< FMRadioService.setMute: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public switchAntenna(I)I
    .locals 4
    .param p1    # I

    const-string v1, "FMRadioService"

    const-string v2, ">>> FMRadioService.switchAntenna"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/mediatek/atci/service/AtciFMRadioNative;->switchAntenna(I)I

    move-result v0

    const-string v1, "FMRadioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<<< FMRadioService.switchAntenna: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public tune(F)Z
    .locals 4
    .param p1    # F

    const-string v1, "FMRadioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>> FMRadioService.tune: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/mediatek/atci/service/AtciFMRadioNative;->tune(F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    const/high16 v2, 0x41200000

    mul-float/2addr v2, p1

    float-to-int v2, v2

    invoke-static {v1, v2}, Lcom/mediatek/atci/service/FMRadioService;->access$102(Lcom/mediatek/atci/service/FMRadioService;I)I

    :cond_0
    const-string v1, "FMRadioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<<< FMRadioService.tune: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public useEarphone(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "FMRadioService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>> FMRadioService.useEarphone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v0}, Lcom/mediatek/atci/service/FMRadioService;->access$400(Lcom/mediatek/atci/service/FMRadioService;)Landroid/media/AudioManager;

    move-result-object v0

    const-string v1, "AudioSetForceToSpeaker=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/atci/service/FMRadioService;->access$502(Lcom/mediatek/atci/service/FMRadioService;Z)Z

    :goto_0
    const-string v0, "FMRadioService"

    const-string v1, "<<< FMRadioService.useEarphone"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    invoke-static {v0}, Lcom/mediatek/atci/service/FMRadioService;->access$400(Lcom/mediatek/atci/service/FMRadioService;)Landroid/media/AudioManager;

    move-result-object v0

    const-string v1, "AudioSetForceToSpeaker=1"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService$1;->this$0:Lcom/mediatek/atci/service/FMRadioService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/atci/service/FMRadioService;->access$502(Lcom/mediatek/atci/service/FMRadioService;Z)Z

    goto :goto_0
.end method
