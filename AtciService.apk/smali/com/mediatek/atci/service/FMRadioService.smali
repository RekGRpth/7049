.class public Lcom/mediatek/atci/service/FMRadioService;
.super Landroid/app/Service;
.source "FMRadioService.java"


# static fields
.field public static final ACTION_STATE_CHANGED:Ljava/lang/String; = "com.mediatek.FMRadio.FMRadioService.ACTION_STATE_CHANGED"

.field public static final ACTION_TOFMSERVICE_POWERDOWN:Ljava/lang/String; = "com.mediatek.FMRadio.FMRadioService.ACTION_TOFMSERVICE_POWERDOWN"

.field private static final AUDIO_PATH_EARPHONE:Ljava/lang/String; = "AudioSetForceToSpeaker=0"

.field private static final AUDIO_PATH_LOUDSPEAKER:Ljava/lang/String; = "AudioSetForceToSpeaker=1"

.field public static final EXTRA_FMRADIO_ISPOWERUP:Ljava/lang/String; = "EXTRA_FMRADIO_ISPOWERUP"

.field private static final NUM_TEN:I = 0xa

.field public static final TAG:Ljava/lang/String; = "FMRadioService"

.field private static sFMService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/mediatek/atci/service/FMRadioService;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mBinder:Lcom/mediatek/atci/service/IFMRadioService$Stub;

.field private mCurrentStation:I

.field private mFMPlayer:Landroid/media/MediaPlayer;

.field private mIsDeviceOpen:Z

.field private mIsEarphoneUsed:Z

.field private mIsPowerUp:Z

.field private mIsRecording:Z

.field private mIsServiceInit:Z

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/atci/service/FMRadioService;->sFMService:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsEarphoneUsed:Z

    iput-boolean v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsDeviceOpen:Z

    iput-boolean v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsPowerUp:Z

    const/16 v0, 0x385

    iput v0, p0, Lcom/mediatek/atci/service/FMRadioService;->mCurrentStation:I

    iput-boolean v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsServiceInit:Z

    iput-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mAudioManager:Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mFMPlayer:Landroid/media/MediaPlayer;

    iput-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iput-boolean v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsRecording:Z

    new-instance v0, Lcom/mediatek/atci/service/FMRadioService$1;

    invoke-direct {v0, p0}, Lcom/mediatek/atci/service/FMRadioService$1;-><init>(Lcom/mediatek/atci/service/FMRadioService;)V

    iput-object v0, p0, Lcom/mediatek/atci/service/FMRadioService;->mBinder:Lcom/mediatek/atci/service/IFMRadioService$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/atci/service/FMRadioService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;

    iget-boolean v0, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsDeviceOpen:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/atci/service/FMRadioService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsDeviceOpen:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/atci/service/FMRadioService;)I
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;

    iget v0, p0, Lcom/mediatek/atci/service/FMRadioService;->mCurrentStation:I

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/atci/service/FMRadioService;I)I
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/atci/service/FMRadioService;->mCurrentStation:I

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/atci/service/FMRadioService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;

    iget-boolean v0, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsPowerUp:Z

    return v0
.end method

.method static synthetic access$202(Lcom/mediatek/atci/service/FMRadioService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsPowerUp:Z

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/atci/service/FMRadioService;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/atci/service/FMRadioService;->enableFMAudio(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/atci/service/FMRadioService;)Landroid/media/AudioManager;
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/atci/service/FMRadioService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;

    iget-boolean v0, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsEarphoneUsed:Z

    return v0
.end method

.method static synthetic access$502(Lcom/mediatek/atci/service/FMRadioService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsEarphoneUsed:Z

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/atci/service/FMRadioService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;

    iget-boolean v0, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsServiceInit:Z

    return v0
.end method

.method static synthetic access$602(Lcom/mediatek/atci/service/FMRadioService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/atci/service/FMRadioService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsServiceInit:Z

    return p1
.end method

.method private enableFMAudio(Z)V
    .locals 5
    .param p1    # Z

    const-string v2, "FMRadioService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ">>> FMRadioService.enableFMAudio: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mFMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "FMRadioService"

    const-string v3, "Error: FM audio is already enabled."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v2, "FMRadioService"

    const-string v3, "<<< FMRadioService.enableFMAudio"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mFMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mFMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "FMRadioService"

    const-string v3, "Exception: Cannot call MediaPlayer prepare."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v2, "FMRadioService"

    const-string v3, "Exception: Cannot call MediaPlayer prepare."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mFMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mFMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->stop()V

    goto :goto_0

    :cond_2
    const-string v2, "FMRadioService"

    const-string v3, "Error: FM audio is already disabled."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static onStateChanged(I)V
    .locals 5
    .param p0    # I

    const/4 v4, 0x0

    const-string v2, "FMRadioService"

    const-string v3, ">>> onStateChanged"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/mediatek/atci/service/FMRadioService;->sFMService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/atci/service/FMRadioService;

    if-nez v0, :cond_0

    const-string v2, "FMRadioService"

    const-string v3, "onStateChanged: service ref is null!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-nez p0, :cond_1

    const-string v2, "FMRadioService"

    const-string v3, "onStateChanged: FM has been powered down"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, v0, Lcom/mediatek/atci/service/FMRadioService;->mIsPowerUp:Z

    if-eqz v2, :cond_1

    invoke-direct {v0, v4}, Lcom/mediatek/atci/service/FMRadioService;->enableFMAudio(Z)V

    iput-boolean v4, v0, Lcom/mediatek/atci/service/FMRadioService;->mIsPowerUp:Z

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mediatek.FMRadio.FMRadioService.ACTION_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "EXTRA_FMRADIO_ISPOWERUP"

    iget-boolean v3, v0, Lcom/mediatek/atci/service/FMRadioService;->mIsPowerUp:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :cond_1
    const-string v2, "FMRadioService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<< onStateChanged: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1    # Landroid/content/Intent;

    const-string v0, "FMRadioService"

    const-string v1, ">>> FMRadioService.onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "FMRadioService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<<< FMRadioService.onBind: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mBinder:Lcom/mediatek/atci/service/IFMRadioService$Stub;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/FMRadioService;->mBinder:Lcom/mediatek/atci/service/IFMRadioService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    const-string v2, "FMRadioService"

    const-string v3, ">>> FMRadioService.onCreate"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v2, Lcom/mediatek/atci/service/FMRadioService;->sFMService:Ljava/lang/ref/WeakReference;

    const-string v2, "audio"

    invoke-virtual {p0, v2}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mAudioManager:Landroid/media/AudioManager;

    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mFMPlayer:Landroid/media/MediaPlayer;

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mFMPlayer:Landroid/media/MediaPlayer;

    const-string v3, "MEDIATEK://MEDIAPLAYER_PLAYERTYPE_FM"

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    iget-object v2, p0, Lcom/mediatek/atci/service/FMRadioService;->mFMPlayer:Landroid/media/MediaPlayer;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.mediatek.FMRadio.FMRadioService.ACTION_TOFMSERVICE_POWERDOWN"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "FMRadioService"

    const-string v3, "<<< FMRadioService.onCreate"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "FMRadioService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDataSource: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "FMRadioService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDataSource: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v2, "FMRadioService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDataSource: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    const/4 v1, 0x0

    sput-object v1, Lcom/mediatek/atci/service/FMRadioService;->sFMService:Ljava/lang/ref/WeakReference;

    const-string v1, "FMRadioService"

    const-string v2, ">>> FMRadioService.onDestroy"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-boolean v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsEarphoneUsed:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mBinder:Lcom/mediatek/atci/service/IFMRadioService$Stub;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/mediatek/atci/service/IFMRadioService$Stub;->useEarphone(Z)V

    :cond_0
    iget-boolean v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsPowerUp:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mBinder:Lcom/mediatek/atci/service/IFMRadioService$Stub;

    invoke-virtual {v1}, Lcom/mediatek/atci/service/IFMRadioService$Stub;->powerDown()Z

    :cond_1
    iget-boolean v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mIsDeviceOpen:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mBinder:Lcom/mediatek/atci/service/IFMRadioService$Stub;

    invoke-virtual {v1}, Lcom/mediatek/atci/service/IFMRadioService$Stub;->closeDevice()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mFMPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/atci/service/FMRadioService;->mFMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    :cond_3
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v1, "FMRadioService"

    const-string v2, "<<< FMRadioService.onDestroy"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    const-string v1, "FMRadioService"

    const-string v2, "Exception: Cannot call binder function."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v1, "FMRadioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>> FMRadioService.onStartCommand intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " startId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    const-string v1, "FMRadioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<<< FMRadioService.onStartCommand: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method
