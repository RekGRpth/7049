.class public Lcom/mediatek/atci/service/Mp3Player;
.super Ljava/lang/Object;
.source "Mp3Player.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;


# static fields
.field private static final DBG:Z = true

.field private static final LR_FILE_NAME:Ljava/lang/String; = "1kHz_0dB_LR_128k.mp3"

.field private static final L_FILE_NAME:Ljava/lang/String; = "1kHz_0dB_L128k.mp3"

.field private static final MULTI_LR_FILE_NAME:Ljava/lang/String; = "MultiSine_20-20kHz-0dBp_128k.mp3"

.field private static final NO_SIGNAL_FILE_NAME:Ljava/lang/String; = "NoSignal_LR_128k.mp3"

.field private static final R_FILE_NAME:Ljava/lang/String; = "1kHz_0dB_R_128k.mp3"

.field private static final STATE_ERROR:I = 0x2

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_PLAYING:I = 0x1

.field static final TAG:Ljava/lang/String; = "ATCIJ_MP3PLAYER"

.field private static sMp3Player:Lcom/mediatek/atci/service/Mp3Player;


# instance fields
.field private mPlayer:Landroid/media/MediaPlayer;

.field private mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/atci/service/Mp3Player;->sMp3Player:Lcom/mediatek/atci/service/Mp3Player;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/atci/service/Mp3Player;->mState:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/atci/service/Mp3Player;->mPlayer:Landroid/media/MediaPlayer;

    return-void
.end method

.method public static getInstanceOfMp3Player()Lcom/mediatek/atci/service/Mp3Player;
    .locals 1

    sget-object v0, Lcom/mediatek/atci/service/Mp3Player;->sMp3Player:Lcom/mediatek/atci/service/Mp3Player;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/atci/service/Mp3Player;

    invoke-direct {v0}, Lcom/mediatek/atci/service/Mp3Player;-><init>()V

    sput-object v0, Lcom/mediatek/atci/service/Mp3Player;->sMp3Player:Lcom/mediatek/atci/service/Mp3Player;

    :cond_0
    sget-object v0, Lcom/mediatek/atci/service/Mp3Player;->sMp3Player:Lcom/mediatek/atci/service/Mp3Player;

    return-object v0
.end method

.method private getfilePath(C)Ljava/lang/String;
    .locals 5
    .param p1    # C

    const/4 v0, 0x0

    const-string v1, "/system/media/audio/lgeSounds/factorytest/"

    const-string v2, "ATCIJ_MP3PLAYER"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file  directory is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    :goto_0
    const-string v2, "ATCIJ_MP3PLAYER"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file Name is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "ATCIJ_MP3PLAYER"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file Path is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :pswitch_0
    const-string v0, "NoSignal_LR_128k.mp3"

    goto :goto_0

    :pswitch_1
    const-string v0, "1kHz_0dB_LR_128k.mp3"

    goto :goto_0

    :pswitch_2
    const-string v0, "1kHz_0dB_L128k.mp3"

    goto :goto_0

    :pswitch_3
    const-string v0, "1kHz_0dB_R_128k.mp3"

    goto :goto_0

    :pswitch_4
    const-string v0, "MultiSine_20-20kHz-0dBp_128k.mp3"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public isError()Z
    .locals 2

    iget v0, p0, Lcom/mediatek/atci/service/Mp3Player;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIdle()Z
    .locals 1

    iget v0, p0, Lcom/mediatek/atci/service/Mp3Player;->mState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/mediatek/atci/service/Mp3Player;->mState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;

    const-string v0, "ATCIJ_MP3PLAYER"

    const-string v1, "onCompletion"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/atci/service/Mp3Player;->stopPlayer()V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const-string v0, "ATCIJ_MP3PLAYER"

    const-string v1, "onError"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/atci/service/Mp3Player;->stopPlayer()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/atci/service/Mp3Player;->mState:I

    const/4 v0, 0x1

    return v0
.end method

.method public startPlayer(C)V
    .locals 5
    .param p1    # C

    const/4 v4, 0x2

    const-string v1, "ATCIJ_MP3PLAYER"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startPlayer:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/atci/service/Mp3Player;->stopPlayer()V

    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/mediatek/atci/service/Mp3Player;->mPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/mediatek/atci/service/Mp3Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    invoke-direct {p0, p1}, Lcom/mediatek/atci/service/Mp3Player;->getfilePath(C)Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/atci/service/Mp3Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0, p1}, Lcom/mediatek/atci/service/Mp3Player;->getfilePath(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/atci/service/Mp3Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v1, p0, Lcom/mediatek/atci/service/Mp3Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    iget-object v1, p0, Lcom/mediatek/atci/service/Mp3Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v1, 0x1

    iput v1, p0, Lcom/mediatek/atci/service/Mp3Player;->mState:I

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iput v4, p0, Lcom/mediatek/atci/service/Mp3Player;->mState:I

    goto :goto_0

    :catch_1
    move-exception v0

    iput v4, p0, Lcom/mediatek/atci/service/Mp3Player;->mState:I

    goto :goto_0
.end method

.method public stopPlayer()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/atci/service/Mp3Player;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "ATCIJ_MP3PLAYER"

    const-string v1, "stopPlayer"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/atci/service/Mp3Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    iget-object v0, p0, Lcom/mediatek/atci/service/Mp3Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/atci/service/Mp3Player;->mPlayer:Landroid/media/MediaPlayer;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/atci/service/Mp3Player;->mState:I

    goto :goto_0
.end method
