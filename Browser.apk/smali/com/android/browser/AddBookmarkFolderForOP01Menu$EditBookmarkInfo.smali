.class Lcom/android/browser/AddBookmarkFolderForOP01Menu$EditBookmarkInfo;
.super Ljava/lang/Object;
.source "AddBookmarkFolderForOP01Menu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/AddBookmarkFolderForOP01Menu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EditBookmarkInfo"
.end annotation


# instance fields
.field mAccountName:Ljava/lang/String;

.field mAccountType:Ljava/lang/String;

.field mEBITitle:Ljava/lang/String;

.field mId:J

.field mLastUsedAccountName:Ljava/lang/String;

.field mLastUsedAccountType:Ljava/lang/String;

.field mLastUsedId:J

.field mLastUsedTitle:Ljava/lang/String;

.field mParentId:J

.field mParentTitle:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$EditBookmarkInfo;->mId:J

    iput-wide v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$EditBookmarkInfo;->mParentId:J

    iput-wide v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$EditBookmarkInfo;->mLastUsedId:J

    return-void
.end method
