.class Lcom/android/browser/AddBookmarkPage$4;
.super Landroid/text/InputFilter$LengthFilter;
.source "AddBookmarkPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/browser/AddBookmarkPage;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/browser/AddBookmarkPage;

.field final synthetic val$nLimit:I


# direct methods
.method constructor <init>(Lcom/android/browser/AddBookmarkPage;II)V
    .locals 0
    .param p2    # I

    iput-object p1, p0, Lcom/android/browser/AddBookmarkPage$4;->this$0:Lcom/android/browser/AddBookmarkPage;

    iput p3, p0, Lcom/android/browser/AddBookmarkPage$4;->val$nLimit:I

    invoke-direct {p0, p2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/text/Spanned;
    .param p5    # I
    .param p6    # I

    iget v1, p0, Lcom/android/browser/AddBookmarkPage$4;->val$nLimit:I

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v2

    sub-int v3, p6, p5

    sub-int/2addr v2, v3

    sub-int v0, v1, v2

    if-gtz v0, :cond_0

    iget-object v1, p0, Lcom/android/browser/AddBookmarkPage$4;->this$0:Lcom/android/browser/AddBookmarkPage;

    invoke-static {v1}, Lcom/android/browser/AddBookmarkPage;->access$1200(Lcom/android/browser/AddBookmarkPage;)V

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_0
    sub-int v1, p3, p2

    if-lt v0, v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/browser/AddBookmarkPage$4;->this$0:Lcom/android/browser/AddBookmarkPage;

    invoke-static {v1}, Lcom/android/browser/AddBookmarkPage;->access$1200(Lcom/android/browser/AddBookmarkPage;)V

    :cond_2
    add-int v1, p2, v0

    invoke-interface {p1, p2, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0
.end method
