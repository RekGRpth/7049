.class Lcom/android/browser/AutoFillSettingsFragment$PhoneNumberValidator;
.super Ljava/lang/Object;
.source "AutoFillSettingsFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/AutoFillSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhoneNumberValidator"
.end annotation


# static fields
.field private static final PHONE_NUMBER_LENGTH:I = 0x7

.field private static final PHONE_NUMBER_SEPARATORS_REGEX:Ljava/lang/String; = "[\\s\\.\\(\\)-]"


# instance fields
.field final synthetic this$0:Lcom/android/browser/AutoFillSettingsFragment;


# direct methods
.method private constructor <init>(Lcom/android/browser/AutoFillSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/browser/AutoFillSettingsFragment$PhoneNumberValidator;->this$0:Lcom/android/browser/AutoFillSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/browser/AutoFillSettingsFragment;Lcom/android/browser/AutoFillSettingsFragment$1;)V
    .locals 0
    .param p1    # Lcom/android/browser/AutoFillSettingsFragment;
    .param p2    # Lcom/android/browser/AutoFillSettingsFragment$1;

    invoke-direct {p0, p1}, Lcom/android/browser/AutoFillSettingsFragment$PhoneNumberValidator;-><init>(Lcom/android/browser/AutoFillSettingsFragment;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6
    .param p1    # Landroid/text/Editable;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const-string v3, "[\\s\\.\\(\\)-]"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v1, :cond_0

    const/4 v3, 0x7

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/android/browser/AutoFillSettingsFragment$PhoneNumberValidator;->this$0:Lcom/android/browser/AutoFillSettingsFragment;

    invoke-static {v3}, Lcom/android/browser/AutoFillSettingsFragment;->access$000(Lcom/android/browser/AutoFillSettingsFragment;)Landroid/widget/EditText;

    move-result-object v3

    iget-object v4, p0, Lcom/android/browser/AutoFillSettingsFragment$PhoneNumberValidator;->this$0:Lcom/android/browser/AutoFillSettingsFragment;

    invoke-virtual {v4}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00d9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v3, p0, Lcom/android/browser/AutoFillSettingsFragment$PhoneNumberValidator;->this$0:Lcom/android/browser/AutoFillSettingsFragment;

    invoke-static {v3}, Lcom/android/browser/AutoFillSettingsFragment;->access$100(Lcom/android/browser/AutoFillSettingsFragment;)V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/browser/AutoFillSettingsFragment$PhoneNumberValidator;->this$0:Lcom/android/browser/AutoFillSettingsFragment;

    invoke-static {v3}, Lcom/android/browser/AutoFillSettingsFragment;->access$000(Lcom/android/browser/AutoFillSettingsFragment;)Landroid/widget/EditText;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method
