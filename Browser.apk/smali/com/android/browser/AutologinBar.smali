.class public Lcom/android/browser/AutologinBar;
.super Landroid/widget/LinearLayout;
.source "AutologinBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/browser/DeviceAccountLogin$AutoLoginCallback;


# instance fields
.field protected mAccountsAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mAutoLoginAccount:Landroid/widget/Spinner;

.field protected mAutoLoginCancel:Landroid/view/View;

.field protected mAutoLoginError:Landroid/widget/TextView;

.field protected mAutoLoginHandler:Lcom/android/browser/DeviceAccountLogin;

.field protected mAutoLoginLogin:Landroid/widget/Button;

.field protected mAutoLoginProgress:Landroid/widget/ProgressBar;

.field protected mTitleBar:Lcom/android/browser/TitleBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method hideAutoLogin(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v0, p1}, Lcom/android/browser/TitleBar;->hideAutoLogin(Z)V

    return-void
.end method

.method public loginFailed()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginAccount:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginLogin:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginError:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginCancel:Landroid/view/View;

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginHandler:Lcom/android/browser/DeviceAccountLogin;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginHandler:Lcom/android/browser/DeviceAccountLogin;

    invoke-virtual {v0}, Lcom/android/browser/DeviceAccountLogin;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginHandler:Lcom/android/browser/DeviceAccountLogin;

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/browser/AutologinBar;->hideAutoLogin(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginLogin:Landroid/widget/Button;

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginHandler:Lcom/android/browser/DeviceAccountLogin;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginAccount:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginLogin:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginError:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginHandler:Lcom/android/browser/DeviceAccountLogin;

    iget-object v1, p0, Lcom/android/browser/AutologinBar;->mAutoLoginAccount:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Lcom/android/browser/DeviceAccountLogin;->login(ILcom/android/browser/DeviceAccountLogin$AutoLoginCallback;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0d007b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginAccount:Landroid/widget/Spinner;

    const v0, 0x7f0d007e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginLogin:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginLogin:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0d007d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginProgress:Landroid/widget/ProgressBar;

    const v0, 0x7f0d007f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginError:Landroid/widget/TextView;

    const v0, 0x7f0d007c

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginCancel:Landroid/view/View;

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginCancel:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setTitleBar(Lcom/android/browser/TitleBar;)V
    .locals 0
    .param p1    # Lcom/android/browser/TitleBar;

    iput-object p1, p0, Lcom/android/browser/AutologinBar;->mTitleBar:Lcom/android/browser/TitleBar;

    return-void
.end method

.method showAutoLogin(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/browser/AutologinBar;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v0, p1}, Lcom/android/browser/TitleBar;->showAutoLogin(Z)V

    return-void
.end method

.method public updateAutoLogin(Lcom/android/browser/Tab;Z)V
    .locals 8
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Z

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/android/browser/Tab;->getDeviceAccountLogin()Lcom/android/browser/DeviceAccountLogin;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/android/browser/AutologinBar;->mAutoLoginHandler:Lcom/android/browser/DeviceAccountLogin;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    const v3, 0x103006e

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x1090008

    invoke-virtual {v0}, Lcom/android/browser/DeviceAccountLogin;->getAccountNames()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v1, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/android/browser/AutologinBar;->mAccountsAdapter:Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAccountsAdapter:Landroid/widget/ArrayAdapter;

    const v3, 0x1090009

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAutoLoginAccount:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/android/browser/AutologinBar;->mAccountsAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAutoLoginAccount:Landroid/widget/Spinner;

    invoke-virtual {v2, v5}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAutoLoginAccount:Landroid/widget/Spinner;

    invoke-virtual {v2, v6}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAutoLoginLogin:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAutoLoginProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAutoLoginError:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Lcom/android/browser/DeviceAccountLogin;->getState()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    :pswitch_0
    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAutoLoginAccount:Landroid/widget/Spinner;

    invoke-virtual {v2, v5}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAutoLoginLogin:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAutoLoginProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/android/browser/AutologinBar;->showAutoLogin(Z)V

    :goto_1
    return-void

    :pswitch_2
    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAutoLoginProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/browser/AutologinBar;->mAutoLoginError:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/android/browser/AutologinBar;->hideAutoLogin(Z)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
