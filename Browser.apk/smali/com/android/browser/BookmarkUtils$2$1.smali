.class Lcom/android/browser/BookmarkUtils$2$1;
.super Ljava/lang/Object;
.source "BookmarkUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/browser/BookmarkUtils$2;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/browser/BookmarkUtils$2;


# direct methods
.method constructor <init>(Lcom/android/browser/BookmarkUtils$2;)V
    .locals 0

    iput-object p1, p0, Lcom/android/browser/BookmarkUtils$2$1;->this$0:Lcom/android/browser/BookmarkUtils$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private deleteBookmarkById(J)V
    .locals 4
    .param p1    # J

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/browser/BookmarkUtils$2$1;->this$0:Lcom/android/browser/BookmarkUtils$2;

    iget-object v2, v2, Lcom/android/browser/BookmarkUtils$2;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/BrowserContract$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private deleteFoldBookmarks(J)V
    .locals 10
    .param p1    # J

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v2, p0, Lcom/android/browser/BookmarkUtils$2$1;->this$0:Lcom/android/browser/BookmarkUtils$2;

    iget-object v2, v2, Lcom/android/browser/BookmarkUtils$2;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/android/browser/BookmarkUtils$2$1;->this$0:Lcom/android/browser/BookmarkUtils$2;

    iget-object v2, v2, Lcom/android/browser/BookmarkUtils$2;->val$context:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/browser/BookmarkUtils;->getBookmarksUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v8

    const-string v3, "parent = ? AND deleted = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ""

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    const-string v5, "0"

    aput-object v5, v4, v9

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-direct {p0, p1, p2}, Lcom/android/browser/BookmarkUtils$2$1;->deleteBookmarkById(J)V

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v2, v2

    invoke-direct {p0, v2, v3}, Lcom/android/browser/BookmarkUtils$2$1;->deleteFoldBookmarks(J)V

    goto :goto_0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/browser/BookmarkUtils$2$1;->this$0:Lcom/android/browser/BookmarkUtils$2;

    iget-wide v0, v0, Lcom/android/browser/BookmarkUtils$2;->val$id:J

    invoke-direct {p0, v0, v1}, Lcom/android/browser/BookmarkUtils$2$1;->deleteFoldBookmarks(J)V

    return-void
.end method
