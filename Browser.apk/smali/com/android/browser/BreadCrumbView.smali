.class public Lcom/android/browser/BreadCrumbView;
.super Landroid/widget/LinearLayout;
.source "BreadCrumbView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/BreadCrumbView$Crumb;,
        Lcom/android/browser/BreadCrumbView$Controller;
    }
.end annotation


# static fields
.field private static final CRUMB_PADDING:I = 0x8

.field private static final DIVIDER_PADDING:I = 0xc


# instance fields
.field private mBackButton:Landroid/widget/ImageButton;

.field private mContext:Landroid/content/Context;

.field private mController:Lcom/android/browser/BreadCrumbView$Controller;

.field private mCrumbPadding:I

.field private mCrumbs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/browser/BreadCrumbView$Crumb;",
            ">;"
        }
    .end annotation
.end field

.field private mDividerPadding:F

.field private mMaxVisible:I

.field private mSeparatorDrawable:Landroid/graphics/drawable/Drawable;

.field private mUseBackButton:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/browser/BreadCrumbView;->mMaxVisible:I

    invoke-direct {p0, p1}, Lcom/android/browser/BreadCrumbView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/browser/BreadCrumbView;->mMaxVisible:I

    invoke-direct {p0, p1}, Lcom/android/browser/BreadCrumbView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/browser/BreadCrumbView;->mMaxVisible:I

    invoke-direct {p0, p1}, Lcom/android/browser/BreadCrumbView;->init(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/BreadCrumbView;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/browser/BreadCrumbView;

    iget-object v0, p0, Lcom/android/browser/BreadCrumbView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/browser/BreadCrumbView;)I
    .locals 1
    .param p0    # Lcom/android/browser/BreadCrumbView;

    iget v0, p0, Lcom/android/browser/BreadCrumbView;->mCrumbPadding:I

    return v0
.end method

.method private addBackButton()V
    .locals 6

    new-instance v2, Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/android/browser/BreadCrumbView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    const v3, 0x7f02001a

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x101030e

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    return-void
.end method

.method private addSeparator()V
    .locals 2

    invoke-direct {p0}, Lcom/android/browser/BreadCrumbView;->makeDividerView()Landroid/widget/ImageView;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/browser/BreadCrumbView;->makeDividerLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/browser/BreadCrumbView;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Landroid/view/View;->setFocusable(Z)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/browser/BreadCrumbView;->mUseBackButton:Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/internal/R$styleable;->Theme:[I

    invoke-virtual {v2, v3}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/16 v2, 0x9c

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/BreadCrumbView;->mSeparatorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x41400000

    mul-float/2addr v2, v1

    iput v2, p0, Lcom/android/browser/BreadCrumbView;->mDividerPadding:F

    const/high16 v2, 0x41000000

    mul-float/2addr v2, v1

    float-to-int v2, v2

    iput v2, p0, Lcom/android/browser/BreadCrumbView;->mCrumbPadding:I

    invoke-direct {p0}, Lcom/android/browser/BreadCrumbView;->addBackButton()V

    return-void
.end method

.method private makeDividerLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 3

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget v1, p0, Lcom/android/browser/BreadCrumbView;->mDividerPadding:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v1, p0, Lcom/android/browser/BreadCrumbView;->mDividerPadding:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    return-object v0
.end method

.method private makeDividerView()Landroid/widget/ImageView;
    .locals 2

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/browser/BreadCrumbView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/browser/BreadCrumbView;->mSeparatorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-object v0
.end method

.method private pop(Z)V
    .locals 4
    .param p1    # Z

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-direct {p0}, Lcom/android/browser/BreadCrumbView;->removeLastView()V

    iget-boolean v2, p0, Lcom/android/browser/BreadCrumbView;->mUseBackButton:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/browser/BreadCrumbView;->removeLastView()V

    :cond_1
    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    add-int/lit8 v3, v0, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/android/browser/BreadCrumbView;->mUseBackButton:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/android/browser/BreadCrumbView;->getTopCrumb()Lcom/android/browser/BreadCrumbView$Crumb;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-boolean v2, v1, Lcom/android/browser/BreadCrumbView$Crumb;->canGoBack:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/android/browser/BreadCrumbView;->updateVisible()V

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/android/browser/BreadCrumbView;->notifyController()V

    :cond_3
    return-void

    :cond_4
    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private pushCrumb(Lcom/android/browser/BreadCrumbView$Crumb;)V
    .locals 1
    .param p1    # Lcom/android/browser/BreadCrumbView$Crumb;

    iget-object v0, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/android/browser/BreadCrumbView;->addSeparator()V

    :cond_0
    iget-object v0, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, Lcom/android/browser/BreadCrumbView$Crumb;->crumbView:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/android/browser/BreadCrumbView;->updateVisible()V

    iget-object v0, p1, Lcom/android/browser/BreadCrumbView$Crumb;->crumbView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private removeLastView()V
    .locals 2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    :cond_0
    return-void
.end method

.method private updateVisible()V
    .locals 11

    const/16 v8, 0x8

    const/4 v7, 0x0

    const/4 v2, 0x1

    iget v9, p0, Lcom/android/browser/BreadCrumbView;->mMaxVisible:I

    if-ltz v9, :cond_2

    invoke-virtual {p0}, Lcom/android/browser/BreadCrumbView;->size()I

    move-result v9

    iget v10, p0, Lcom/android/browser/BreadCrumbView;->mMaxVisible:I

    sub-int v6, v9, v10

    if-lez v6, :cond_1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_1

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_1
    if-ge v2, v1, :cond_3

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    move v5, v2

    :goto_2
    if-ge v5, v3, :cond_3

    invoke-virtual {p0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_3
    iget-boolean v9, p0, Lcom/android/browser/BreadCrumbView;->mUseBackButton:Z

    if-eqz v9, :cond_6

    invoke-virtual {p0}, Lcom/android/browser/BreadCrumbView;->getTopCrumb()Lcom/android/browser/BreadCrumbView$Crumb;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {p0}, Lcom/android/browser/BreadCrumbView;->getTopCrumb()Lcom/android/browser/BreadCrumbView$Crumb;

    move-result-object v9

    iget-boolean v0, v9, Lcom/android/browser/BreadCrumbView$Crumb;->canGoBack:Z

    :goto_3
    iget-object v9, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_5

    :goto_4
    invoke-virtual {v9, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_5
    return-void

    :cond_4
    move v0, v7

    goto :goto_3

    :cond_5
    move v7, v8

    goto :goto_4

    :cond_6
    iget-object v7, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5
.end method


# virtual methods
.method public clear()V
    .locals 2

    const/4 v1, 0x1

    :goto_0
    iget-object v0, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/browser/BreadCrumbView;->pop(Z)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, v1}, Lcom/android/browser/BreadCrumbView;->pop(Z)V

    return-void
.end method

.method public getBaseline()I
    .locals 2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBaseline()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->getBaseline()I

    move-result v1

    goto :goto_0
.end method

.method public getMaxVisible()I
    .locals 1

    iget v0, p0, Lcom/android/browser/BreadCrumbView;->mMaxVisible:I

    return v0
.end method

.method getTopCrumb()Lcom/android/browser/BreadCrumbView$Crumb;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/browser/BreadCrumbView$Crumb;

    :cond_0
    return-object v0
.end method

.method public getTopData()Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lcom/android/browser/BreadCrumbView;->getTopCrumb()Lcom/android/browser/BreadCrumbView$Crumb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/browser/BreadCrumbView$Crumb;->data:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTopLevel()I
    .locals 1

    iget-object v0, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public notifyController()V
    .locals 3

    iget-object v0, p0, Lcom/android/browser/BreadCrumbView;->mController:Lcom/android/browser/BreadCrumbView$Controller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/browser/BreadCrumbView;->mController:Lcom/android/browser/BreadCrumbView$Controller;

    iget-object v1, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/browser/BreadCrumbView;->getTopCrumb()Lcom/android/browser/BreadCrumbView$Crumb;

    move-result-object v2

    iget-object v2, v2, Lcom/android/browser/BreadCrumbView$Crumb;->data:Ljava/lang/Object;

    invoke-interface {v0, p0, v1, v2}, Lcom/android/browser/BreadCrumbView$Controller;->onTop(Lcom/android/browser/BreadCrumbView;ILjava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/browser/BreadCrumbView;->mController:Lcom/android/browser/BreadCrumbView$Controller;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, Lcom/android/browser/BreadCrumbView$Controller;->onTop(Lcom/android/browser/BreadCrumbView;ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/browser/BreadCrumbView;->mBackButton:Landroid/widget/ImageButton;

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/android/browser/BreadCrumbView;->popView()V

    invoke-virtual {p0}, Lcom/android/browser/BreadCrumbView;->notifyController()V

    :goto_0
    return-void

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/android/browser/BreadCrumbView;->getTopCrumb()Lcom/android/browser/BreadCrumbView$Crumb;

    move-result-object v0

    iget-object v0, v0, Lcom/android/browser/BreadCrumbView$Crumb;->crumbView:Landroid/view/View;

    if-eq p1, v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/browser/BreadCrumbView;->pop(Z)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/android/browser/BreadCrumbView;->notifyController()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    iget-object v2, p0, Lcom/android/browser/BreadCrumbView;->mSeparatorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-ge v2, v0, :cond_1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0, v2, v0}, Landroid/view/View;->setMeasuredDimension(II)V

    :cond_1
    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    if-ge v2, v0, :cond_0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public popView()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/browser/BreadCrumbView;->pop(Z)V

    return-void
.end method

.method public pushView(Ljava/lang/String;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/browser/BreadCrumbView;->pushView(Ljava/lang/String;ZLjava/lang/Object;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public pushView(Ljava/lang/String;ZLjava/lang/Object;)Landroid/view/View;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Ljava/lang/Object;

    new-instance v0, Lcom/android/browser/BreadCrumbView$Crumb;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/browser/BreadCrumbView$Crumb;-><init>(Lcom/android/browser/BreadCrumbView;Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/android/browser/BreadCrumbView;->pushCrumb(Lcom/android/browser/BreadCrumbView$Crumb;)V

    iget-object v1, v0, Lcom/android/browser/BreadCrumbView$Crumb;->crumbView:Landroid/view/View;

    return-object v1
.end method

.method public pushView(Landroid/view/View;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/Object;

    new-instance v0, Lcom/android/browser/BreadCrumbView$Crumb;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/android/browser/BreadCrumbView$Crumb;-><init>(Lcom/android/browser/BreadCrumbView;Landroid/view/View;ZLjava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/android/browser/BreadCrumbView;->pushCrumb(Lcom/android/browser/BreadCrumbView$Crumb;)V

    return-void
.end method

.method public setController(Lcom/android/browser/BreadCrumbView$Controller;)V
    .locals 0
    .param p1    # Lcom/android/browser/BreadCrumbView$Controller;

    iput-object p1, p0, Lcom/android/browser/BreadCrumbView;->mController:Lcom/android/browser/BreadCrumbView$Controller;

    return-void
.end method

.method public setMaxVisible(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/browser/BreadCrumbView;->mMaxVisible:I

    invoke-direct {p0}, Lcom/android/browser/BreadCrumbView;->updateVisible()V

    return-void
.end method

.method public setUseBackButton(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/browser/BreadCrumbView;->mUseBackButton:Z

    invoke-direct {p0}, Lcom/android/browser/BreadCrumbView;->updateVisible()V

    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/android/browser/BreadCrumbView;->mCrumbs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
