.class public Lcom/android/browser/BrowserWebViewFactory;
.super Ljava/lang/Object;
.source "BrowserWebViewFactory.java"

# interfaces
.implements Lcom/android/browser/WebViewFactory;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/browser/BrowserWebViewFactory;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public createSubWebView(Z)Landroid/webkit/WebView;
    .locals 1
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/android/browser/BrowserWebViewFactory;->createWebView(Z)Landroid/webkit/WebView;

    move-result-object v0

    return-object v0
.end method

.method public createWebView(Z)Landroid/webkit/WebView;
    .locals 3
    .param p1    # Z

    const/4 v1, 0x0

    const v2, 0x1010085

    invoke-virtual {p0, v1, v2, p1}, Lcom/android/browser/BrowserWebViewFactory;->instantiateWebView(Landroid/util/AttributeSet;IZ)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/browser/BrowserWebViewFactory;->initWebViewSettings(Landroid/webkit/WebView;)V

    return-object v0
.end method

.method protected initWebViewSettings(Landroid/webkit/WebView;)V
    .locals 6
    .param p1    # Landroid/webkit/WebView;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Landroid/view/View;->setScrollbarFadingEnabled(Z)V

    const/high16 v5, 0x2000000

    invoke-virtual {p1, v5}, Landroid/webkit/WebView;->setScrollBarStyle(I)V

    invoke-virtual {p1, v3}, Landroid/webkit/WebView;->setMapTrackballToArrowKeys(Z)V

    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    const/4 v5, 0x2

    invoke-virtual {p1, v5}, Landroid/webkit/WebView;->setOverScrollMode(I)V

    iget-object v5, p0, Lcom/android/browser/BrowserWebViewFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v5, "android.hardware.touchscreen.multitouch"

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "android.hardware.faketouch.multitouch.distinct"

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v2, v4

    :goto_0
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    if-nez v2, :cond_2

    :goto_1
    invoke-virtual {v5, v4}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    invoke-static {}, Lcom/android/browser/BrowserSettings;->getInstance()Lcom/android/browser/BrowserSettings;

    move-result-object v1

    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/android/browser/BrowserSettings;->startManagingSettings(Landroid/webkit/WebSettings;)V

    return-void

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    move v4, v3

    goto :goto_1
.end method

.method protected instantiateWebView(Landroid/util/AttributeSet;IZ)Landroid/webkit/WebView;
    .locals 2
    .param p1    # Landroid/util/AttributeSet;
    .param p2    # I
    .param p3    # Z

    new-instance v0, Lcom/android/browser/BrowserWebView;

    iget-object v1, p0, Lcom/android/browser/BrowserWebViewFactory;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/android/browser/BrowserWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    return-object v0
.end method
