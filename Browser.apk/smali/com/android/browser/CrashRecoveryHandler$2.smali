.class Lcom/android/browser/CrashRecoveryHandler$2;
.super Ljava/lang/Object;
.source "CrashRecoveryHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/CrashRecoveryHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/browser/CrashRecoveryHandler;


# direct methods
.method constructor <init>(Lcom/android/browser/CrashRecoveryHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/browser/CrashRecoveryHandler$2;->this$0:Lcom/android/browser/CrashRecoveryHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v2, p0, Lcom/android/browser/CrashRecoveryHandler$2;->this$0:Lcom/android/browser/CrashRecoveryHandler;

    invoke-static {v2}, Lcom/android/browser/CrashRecoveryHandler;->access$500(Lcom/android/browser/CrashRecoveryHandler;)Lcom/android/browser/Controller;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/browser/Controller;->createSaveState()Landroid/os/Bundle;

    move-result-object v0

    iget-object v2, p0, Lcom/android/browser/CrashRecoveryHandler$2;->this$0:Lcom/android/browser/CrashRecoveryHandler;

    invoke-static {v2}, Lcom/android/browser/CrashRecoveryHandler;->access$600(Lcom/android/browser/CrashRecoveryHandler;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    iget-object v2, p0, Lcom/android/browser/CrashRecoveryHandler$2;->this$0:Lcom/android/browser/CrashRecoveryHandler;

    invoke-static {v2}, Lcom/android/browser/CrashRecoveryHandler;->access$800(Lcom/android/browser/CrashRecoveryHandler;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/CrashRecoveryHandler$2;->this$0:Lcom/android/browser/CrashRecoveryHandler;

    invoke-static {v3}, Lcom/android/browser/CrashRecoveryHandler;->access$700(Lcom/android/browser/CrashRecoveryHandler;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "BrowserCrashRecovery"

    const-string v3, "Failed to save state"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
