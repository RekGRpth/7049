.class public Lcom/android/browser/GeolocationPermissionsPrompt;
.super Landroid/widget/RelativeLayout;
.source "GeolocationPermissionsPrompt.java"


# instance fields
.field private mCallback:Landroid/webkit/GeolocationPermissions$Callback;

.field private mDontShareButton:Landroid/widget/Button;

.field private mMessage:Landroid/widget/TextView;

.field private mOrigin:Ljava/lang/String;

.field private mRemember:Landroid/widget/CheckBox;

.field private mShareButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/browser/GeolocationPermissionsPrompt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/GeolocationPermissionsPrompt;Z)V
    .locals 0
    .param p0    # Lcom/android/browser/GeolocationPermissionsPrompt;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/browser/GeolocationPermissionsPrompt;->handleButtonClick(Z)V

    return-void
.end method

.method private handleButtonClick(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/browser/GeolocationPermissionsPrompt;->hide()V

    iget-object v2, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mRemember:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz p1, :cond_1

    const v2, 0x7f0c0169

    :goto_0
    const/4 v4, 0x1

    invoke-static {v3, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    const/16 v2, 0x50

    invoke-virtual {v1, v2, v5, v5}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    iget-object v2, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mCallback:Landroid/webkit/GeolocationPermissions$Callback;

    iget-object v3, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mOrigin:Ljava/lang/String;

    invoke-interface {v2, v3, p1, v0}, Landroid/webkit/GeolocationPermissions$Callback;->invoke(Ljava/lang/String;ZZ)V

    return-void

    :cond_1
    const v2, 0x7f0c016a

    goto :goto_0
.end method

.method private init()V
    .locals 2

    const v0, 0x7f0d004b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mMessage:Landroid/widget/TextView;

    const v0, 0x7f0d004e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mShareButton:Landroid/widget/Button;

    const v0, 0x7f0d004d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mDontShareButton:Landroid/widget/Button;

    const v0, 0x7f0d004c

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mRemember:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mShareButton:Landroid/widget/Button;

    new-instance v1, Lcom/android/browser/GeolocationPermissionsPrompt$1;

    invoke-direct {v1, p0}, Lcom/android/browser/GeolocationPermissionsPrompt$1;-><init>(Lcom/android/browser/GeolocationPermissionsPrompt;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mDontShareButton:Landroid/widget/Button;

    new-instance v1, Lcom/android/browser/GeolocationPermissionsPrompt$2;

    invoke-direct {v1, p0}, Lcom/android/browser/GeolocationPermissionsPrompt$2;-><init>(Lcom/android/browser/GeolocationPermissionsPrompt;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setMessage(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mMessage:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0165

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    invoke-direct {p0}, Lcom/android/browser/GeolocationPermissionsPrompt;->init()V

    return-void
.end method

.method public show(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/webkit/GeolocationPermissions$Callback;

    iput-object p1, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mOrigin:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mCallback:Landroid/webkit/GeolocationPermissions$Callback;

    iget-object v1, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mOrigin:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "http"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mOrigin:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {p0, v1}, Lcom/android/browser/GeolocationPermissionsPrompt;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mRemember:Landroid/widget/CheckBox;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/browser/GeolocationPermissionsPrompt;->mOrigin:Ljava/lang/String;

    goto :goto_0
.end method
