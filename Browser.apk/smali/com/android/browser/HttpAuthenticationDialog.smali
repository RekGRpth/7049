.class public Lcom/android/browser/HttpAuthenticationDialog;
.super Ljava/lang/Object;
.source "HttpAuthenticationDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/HttpAuthenticationDialog$CancelListener;,
        Lcom/android/browser/HttpAuthenticationDialog$OkListener;
    }
.end annotation


# instance fields
.field private mCancelListener:Lcom/android/browser/HttpAuthenticationDialog$CancelListener;

.field private final mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private final mHost:Ljava/lang/String;

.field private mOkListener:Lcom/android/browser/HttpAuthenticationDialog$OkListener;

.field private mPasswordView:Landroid/widget/TextView;

.field private final mRealm:Ljava/lang/String;

.field private mUsernameView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/browser/HttpAuthenticationDialog;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/browser/HttpAuthenticationDialog;->mHost:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mRealm:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/browser/HttpAuthenticationDialog;->createDialog()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/HttpAuthenticationDialog;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/android/browser/HttpAuthenticationDialog;

    iget-object v0, p0, Lcom/android/browser/HttpAuthenticationDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/browser/HttpAuthenticationDialog;)Lcom/android/browser/HttpAuthenticationDialog$CancelListener;
    .locals 1
    .param p0    # Lcom/android/browser/HttpAuthenticationDialog;

    iget-object v0, p0, Lcom/android/browser/HttpAuthenticationDialog;->mCancelListener:Lcom/android/browser/HttpAuthenticationDialog$CancelListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/browser/HttpAuthenticationDialog;)Lcom/android/browser/HttpAuthenticationDialog$OkListener;
    .locals 1
    .param p0    # Lcom/android/browser/HttpAuthenticationDialog;

    iget-object v0, p0, Lcom/android/browser/HttpAuthenticationDialog;->mOkListener:Lcom/android/browser/HttpAuthenticationDialog$OkListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/browser/HttpAuthenticationDialog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/browser/HttpAuthenticationDialog;

    iget-object v0, p0, Lcom/android/browser/HttpAuthenticationDialog;->mHost:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/browser/HttpAuthenticationDialog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/browser/HttpAuthenticationDialog;

    iget-object v0, p0, Lcom/android/browser/HttpAuthenticationDialog;->mRealm:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/browser/HttpAuthenticationDialog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/browser/HttpAuthenticationDialog;

    invoke-direct {p0}, Lcom/android/browser/HttpAuthenticationDialog;->getUsername()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/browser/HttpAuthenticationDialog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/browser/HttpAuthenticationDialog;

    invoke-direct {p0}, Lcom/android/browser/HttpAuthenticationDialog;->getPassword()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private createDialog()V
    .locals 6

    iget-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f04001d

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0d0052

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mUsernameView:Landroid/widget/TextView;

    const v3, 0x7f0d0053

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mPasswordView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mPasswordView:Landroid/widget/TextView;

    new-instance v4, Lcom/android/browser/HttpAuthenticationDialog$1;

    invoke-direct {v4, p0}, Lcom/android/browser/HttpAuthenticationDialog$1;-><init>(Lcom/android/browser/HttpAuthenticationDialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f0c0050

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "%s1"

    iget-object v5, p0, Lcom/android/browser/HttpAuthenticationDialog;->mHost:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "%s2"

    iget-object v5, p0, Lcom/android/browser/HttpAuthenticationDialog;->mRealm:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/browser/HttpAuthenticationDialog;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1080027

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0c0053

    new-instance v5, Lcom/android/browser/HttpAuthenticationDialog$4;

    invoke-direct {v5, p0}, Lcom/android/browser/HttpAuthenticationDialog$4;-><init>(Lcom/android/browser/HttpAuthenticationDialog;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0c0055

    new-instance v5, Lcom/android/browser/HttpAuthenticationDialog$3;

    invoke-direct {v5, p0}, Lcom/android/browser/HttpAuthenticationDialog$3;-><init>(Lcom/android/browser/HttpAuthenticationDialog;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/android/browser/HttpAuthenticationDialog$2;

    invoke-direct {v4, p0}, Lcom/android/browser/HttpAuthenticationDialog$2;-><init>(Lcom/android/browser/HttpAuthenticationDialog;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mDialog:Landroid/app/AlertDialog;

    iget-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/Window;->setSoftInputMode(I)V

    return-void
.end method

.method private getPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/HttpAuthenticationDialog;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/HttpAuthenticationDialog;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public reshow()V
    .locals 4

    invoke-direct {p0}, Lcom/android/browser/HttpAuthenticationDialog;->getUsername()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/android/browser/HttpAuthenticationDialog;->getPassword()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    invoke-direct {p0}, Lcom/android/browser/HttpAuthenticationDialog;->createDialog()V

    iget-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/android/browser/HttpAuthenticationDialog;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    goto :goto_0
.end method

.method public setCancelListener(Lcom/android/browser/HttpAuthenticationDialog$CancelListener;)V
    .locals 0
    .param p1    # Lcom/android/browser/HttpAuthenticationDialog$CancelListener;

    iput-object p1, p0, Lcom/android/browser/HttpAuthenticationDialog;->mCancelListener:Lcom/android/browser/HttpAuthenticationDialog$CancelListener;

    return-void
.end method

.method public setOkListener(Lcom/android/browser/HttpAuthenticationDialog$OkListener;)V
    .locals 0
    .param p1    # Lcom/android/browser/HttpAuthenticationDialog$OkListener;

    iput-object p1, p0, Lcom/android/browser/HttpAuthenticationDialog;->mOkListener:Lcom/android/browser/HttpAuthenticationDialog$OkListener;

    return-void
.end method

.method public show()V
    .locals 1

    iget-object v0, p0, Lcom/android/browser/HttpAuthenticationDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iget-object v0, p0, Lcom/android/browser/HttpAuthenticationDialog;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void
.end method
