.class Lcom/android/browser/NavTabScroller$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "NavTabScroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/browser/NavTabScroller;->animateOut(Landroid/view/View;FF)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/browser/NavTabScroller;

.field final synthetic val$pos:I

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/android/browser/NavTabScroller;II)V
    .locals 0

    iput-object p1, p0, Lcom/android/browser/NavTabScroller$2;->this$0:Lcom/android/browser/NavTabScroller;

    iput p2, p0, Lcom/android/browser/NavTabScroller$2;->val$position:I

    iput p3, p0, Lcom/android/browser/NavTabScroller$2;->val$pos:I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/android/browser/NavTabScroller$2;->this$0:Lcom/android/browser/NavTabScroller;

    invoke-static {v0}, Lcom/android/browser/NavTabScroller;->access$000(Lcom/android/browser/NavTabScroller;)Lcom/android/browser/NavTabScroller$OnRemoveListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/NavTabScroller$2;->this$0:Lcom/android/browser/NavTabScroller;

    invoke-static {v0}, Lcom/android/browser/NavTabScroller;->access$000(Lcom/android/browser/NavTabScroller;)Lcom/android/browser/NavTabScroller$OnRemoveListener;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/NavTabScroller$2;->val$position:I

    invoke-interface {v0, v1}, Lcom/android/browser/NavTabScroller$OnRemoveListener;->onRemovePosition(I)V

    iget-object v0, p0, Lcom/android/browser/NavTabScroller$2;->this$0:Lcom/android/browser/NavTabScroller;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/browser/NavTabScroller;->access$102(Lcom/android/browser/NavTabScroller;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/android/browser/NavTabScroller$2;->this$0:Lcom/android/browser/NavTabScroller;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/android/browser/NavTabScroller;->access$202(Lcom/android/browser/NavTabScroller;I)I

    iget-object v0, p0, Lcom/android/browser/NavTabScroller$2;->this$0:Lcom/android/browser/NavTabScroller;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/browser/NavTabScroller;->access$302(Lcom/android/browser/NavTabScroller;I)I

    iget-object v0, p0, Lcom/android/browser/NavTabScroller$2;->this$0:Lcom/android/browser/NavTabScroller;

    iget v1, p0, Lcom/android/browser/NavTabScroller$2;->val$pos:I

    invoke-virtual {v0, v1}, Lcom/android/browser/NavTabScroller;->handleDataChanged(I)V

    :cond_0
    return-void
.end method
