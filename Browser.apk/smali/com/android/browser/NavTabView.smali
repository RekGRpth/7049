.class public Lcom/android/browser/NavTabView;
.super Landroid/widget/LinearLayout;
.source "NavTabView.java"


# instance fields
.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mClose:Landroid/widget/ImageView;

.field private mContent:Landroid/view/ViewGroup;

.field private mHighlighted:Z

.field mImage:Landroid/widget/ImageView;

.field private mTab:Lcom/android/browser/Tab;

.field private mTitle:Landroid/widget/TextView;

.field private mTitleBar:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/browser/NavTabView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/browser/NavTabView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/android/browser/NavTabView;->init()V

    return-void
.end method

.method private init()V
    .locals 2

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040021

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0d0009

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/browser/NavTabView;->mContent:Landroid/view/ViewGroup;

    const v0, 0x7f0d005c

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/browser/NavTabView;->mClose:Landroid/widget/ImageView;

    const v0, 0x7f0d0007

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/browser/NavTabView;->mTitle:Landroid/widget/TextView;

    const v0, 0x7f0d005b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/NavTabView;->mTitleBar:Landroid/view/View;

    const v0, 0x7f0d0003

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/browser/NavTabView;->mImage:Landroid/widget/ImageView;

    return-void
.end method

.method private setTitle()V
    .locals 3

    iget-object v1, p0, Lcom/android/browser/NavTabView;->mTab:Lcom/android/browser/Tab;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/android/browser/NavTabView;->mHighlighted:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/browser/NavTabView;->mTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/browser/NavTabView;->mTab:Lcom/android/browser/Tab;

    invoke-virtual {v2}, Lcom/android/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v1, p0, Lcom/android/browser/NavTabView;->mTab:Lcom/android/browser/Tab;

    invoke-virtual {v1}, Lcom/android/browser/Tab;->isSnapshot()Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f02002d

    invoke-direct {p0, v1}, Lcom/android/browser/NavTabView;->setTitleIcon(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/browser/NavTabView;->mTab:Lcom/android/browser/Tab;

    invoke-virtual {v1}, Lcom/android/browser/Tab;->getTitle()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/android/browser/NavTabView;->mTab:Lcom/android/browser/Tab;

    invoke-virtual {v1}, Lcom/android/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    :cond_2
    iget-object v1, p0, Lcom/android/browser/NavTabView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/android/browser/NavTabView;->mTab:Lcom/android/browser/Tab;

    invoke-virtual {v1}, Lcom/android/browser/Tab;->isPrivateBrowsingEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f020030

    invoke-direct {p0, v1}, Lcom/android/browser/NavTabView;->setTitleIcon(I)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/browser/NavTabView;->setTitleIcon(I)V

    goto :goto_0
.end method

.method private setTitleIcon(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/browser/NavTabView;->mTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/browser/NavTabView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCompoundDrawablePadding()I

    move-result v1

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    :goto_0
    iget-object v0, p0, Lcom/android/browser/NavTabView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/browser/NavTabView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method


# virtual methods
.method protected isClose(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/browser/NavTabView;->mClose:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isHighlighted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/NavTabView;->mHighlighted:Z

    return v0
.end method

.method protected isTitle(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/browser/NavTabView;->mTitleBar:Landroid/view/View;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isWebView(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/browser/NavTabView;->mImage:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/android/browser/NavTabView;->mClickListener:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/android/browser/NavTabView;->mTitleBar:Landroid/view/View;

    iget-object v1, p0, Lcom/android/browser/NavTabView;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/browser/NavTabView;->mClose:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/browser/NavTabView;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/browser/NavTabView;->mImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/NavTabView;->mImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/browser/NavTabView;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method protected setWebView(Lcom/android/browser/Tab;)V
    .locals 3
    .param p1    # Lcom/android/browser/Tab;

    iput-object p1, p0, Lcom/android/browser/NavTabView;->mTab:Lcom/android/browser/Tab;

    invoke-direct {p0}, Lcom/android/browser/NavTabView;->setTitle()V

    invoke-virtual {p1}, Lcom/android/browser/Tab;->getScreenshot()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/browser/NavTabView;->mImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/browser/NavTabView;->mImage:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/android/browser/Tab;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
