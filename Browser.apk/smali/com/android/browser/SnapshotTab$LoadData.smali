.class Lcom/android/browser/SnapshotTab$LoadData;
.super Landroid/os/AsyncTask;
.source "SnapshotTab.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/SnapshotTab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LoadData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field static final PROJECTION:[Ljava/lang/String;

.field static final SNAPSHOT_BACKGROUND:I = 0x5

.field static final SNAPSHOT_DATE_CREATED:I = 0x6

.field static final SNAPSHOT_FAVICON:I = 0x3

.field static final SNAPSHOT_ID:I = 0x0

.field static final SNAPSHOT_TITLE:I = 0x2

.field static final SNAPSHOT_URL:I = 0x1

.field static final SNAPSHOT_VIEWSTATE:I = 0x4

.field static final SNAPSHOT_VIEWSTATE_PATH:I = 0x7


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mTab:Lcom/android/browser/SnapshotTab;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "url"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "favicon"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "view_state"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "background"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "date_created"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "viewstate_path"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/browser/SnapshotTab$LoadData;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/browser/SnapshotTab;Landroid/content/Context;)V
    .locals 1
    .param p1    # Lcom/android/browser/SnapshotTab;
    .param p2    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/SnapshotTab$LoadData;->mContentResolver:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/android/browser/SnapshotTab$LoadData;->mContext:Landroid/content/Context;

    return-void
.end method

.method private getInputStream(Landroid/database/Cursor;)Ljava/io/InputStream;
    .locals 4
    .param p1    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    const/4 v3, 0x7

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/browser/SnapshotTab$LoadData;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/database/Cursor;
    .locals 8
    .param p1    # [Ljava/lang/Void;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    invoke-static {v0}, Lcom/android/browser/SnapshotTab;->access$000(Lcom/android/browser/SnapshotTab;)J

    move-result-wide v6

    sget-object v0, Lcom/android/browser/provider/SnapshotProvider$Snapshots;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/android/browser/SnapshotTab$LoadData;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/android/browser/SnapshotTab$LoadData;->PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/browser/SnapshotTab$LoadData;->doInBackground([Ljava/lang/Void;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/database/Cursor;)V
    .locals 9
    .param p1    # Landroid/database/Cursor;

    const/4 v8, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    iget-object v5, v5, Lcom/android/browser/Tab;->mCurrentState:Lcom/android/browser/Tab$PageState;

    const/4 v6, 0x2

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/browser/Tab$PageState;->mTitle:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    iget-object v5, v5, Lcom/android/browser/Tab;->mCurrentState:Lcom/android/browser/Tab$PageState;

    const/4 v6, 0x1

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/browser/Tab$PageState;->mUrl:Ljava/lang/String;

    const/4 v5, 0x3

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v5, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    iget-object v5, v5, Lcom/android/browser/Tab;->mCurrentState:Lcom/android/browser/Tab$PageState;

    const/4 v6, 0x0

    array-length v7, v1

    invoke-static {v1, v6, v7}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, v5, Lcom/android/browser/Tab$PageState;->mFavicon:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v5, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    invoke-virtual {v5}, Lcom/android/browser/Tab;->getWebViewClassic()Landroid/webkit/WebViewClassic;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-direct {p0, p1}, Lcom/android/browser/SnapshotTab$LoadData;->getInputStream(Landroid/database/Cursor;)Ljava/io/InputStream;

    move-result-object v2

    new-instance v3, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v3, v2}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v4, v3}, Landroid/webkit/WebViewClassic;->loadViewState(Ljava/io/InputStream;)V

    :cond_1
    iget-object v5, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    const/4 v6, 0x5

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v5, v6}, Lcom/android/browser/SnapshotTab;->access$102(Lcom/android/browser/SnapshotTab;I)I

    iget-object v5, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    const/4 v6, 0x6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/android/browser/SnapshotTab;->access$202(Lcom/android/browser/SnapshotTab;J)J

    iget-object v5, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    iget-object v5, v5, Lcom/android/browser/Tab;->mWebViewController:Lcom/android/browser/WebViewController;

    iget-object v6, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    invoke-interface {v5, v6}, Lcom/android/browser/WebViewController;->onPageFinished(Lcom/android/browser/Tab;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_3
    iget-object v5, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    invoke-static {v5, v8}, Lcom/android/browser/SnapshotTab;->access$302(Lcom/android/browser/SnapshotTab;Lcom/android/browser/SnapshotTab$LoadData;)Lcom/android/browser/SnapshotTab$LoadData;

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v5, "SnapshotTab"

    const-string v6, "Failed to load view state, closing tab"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v5, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    iget-object v5, v5, Lcom/android/browser/Tab;->mWebViewController:Lcom/android/browser/WebViewController;

    iget-object v6, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    invoke-interface {v5, v6}, Lcom/android/browser/WebViewController;->closeTab(Lcom/android/browser/Tab;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_4
    iget-object v5, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    invoke-static {v5, v8}, Lcom/android/browser/SnapshotTab;->access$302(Lcom/android/browser/SnapshotTab;Lcom/android/browser/SnapshotTab$LoadData;)Lcom/android/browser/SnapshotTab$LoadData;

    goto :goto_0

    :catchall_0
    move-exception v5

    if-eqz p1, :cond_5

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_5
    iget-object v6, p0, Lcom/android/browser/SnapshotTab$LoadData;->mTab:Lcom/android/browser/SnapshotTab;

    invoke-static {v6, v8}, Lcom/android/browser/SnapshotTab;->access$302(Lcom/android/browser/SnapshotTab;Lcom/android/browser/SnapshotTab$LoadData;)Lcom/android/browser/SnapshotTab$LoadData;

    throw v5
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/android/browser/SnapshotTab$LoadData;->onPostExecute(Landroid/database/Cursor;)V

    return-void
.end method
