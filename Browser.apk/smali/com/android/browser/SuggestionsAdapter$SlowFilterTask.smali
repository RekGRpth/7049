.class Lcom/android/browser/SuggestionsAdapter$SlowFilterTask;
.super Landroid/os/AsyncTask;
.source "SuggestionsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/SuggestionsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SlowFilterTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/android/browser/SuggestionsAdapter$SuggestItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/browser/SuggestionsAdapter;


# direct methods
.method constructor <init>(Lcom/android/browser/SuggestionsAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/browser/SuggestionsAdapter$SlowFilterTask;->this$0:Lcom/android/browser/SuggestionsAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/android/browser/SuggestionsAdapter$SlowFilterTask;->doInBackground([Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 5
    .param p1    # [Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/android/browser/SuggestionsAdapter$SuggestItem;",
            ">;"
        }
    .end annotation

    new-instance v1, Lcom/android/browser/SuggestionsAdapter$SuggestCursor;

    iget-object v4, p0, Lcom/android/browser/SuggestionsAdapter$SlowFilterTask;->this$0:Lcom/android/browser/SuggestionsAdapter;

    invoke-direct {v1, v4}, Lcom/android/browser/SuggestionsAdapter$SuggestCursor;-><init>(Lcom/android/browser/SuggestionsAdapter;)V

    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {v1, v4}, Lcom/android/browser/SuggestionsAdapter$SuggestCursor;->runQuery(Ljava/lang/CharSequence;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Lcom/android/browser/SuggestionsAdapter$CursorSource;->getCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {v1}, Lcom/android/browser/SuggestionsAdapter$SuggestCursor;->getItem()Lcom/android/browser/SuggestionsAdapter$SuggestItem;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/android/browser/SuggestionsAdapter$CursorSource;->moveToNext()Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/android/browser/SuggestionsAdapter$CursorSource;->close()V

    return-object v3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/android/browser/SuggestionsAdapter$SlowFilterTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/browser/SuggestionsAdapter$SuggestItem;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/browser/SuggestionsAdapter$SlowFilterTask;->this$0:Lcom/android/browser/SuggestionsAdapter;

    iput-object p1, v0, Lcom/android/browser/SuggestionsAdapter;->mSuggestResults:Ljava/util/List;

    iget-object v0, p0, Lcom/android/browser/SuggestionsAdapter$SlowFilterTask;->this$0:Lcom/android/browser/SuggestionsAdapter;

    iget-object v1, p0, Lcom/android/browser/SuggestionsAdapter$SlowFilterTask;->this$0:Lcom/android/browser/SuggestionsAdapter;

    invoke-virtual {v1}, Lcom/android/browser/SuggestionsAdapter;->buildSuggestionResults()Lcom/android/browser/SuggestionsAdapter$SuggestionResults;

    move-result-object v1

    iput-object v1, v0, Lcom/android/browser/SuggestionsAdapter;->mMixedResults:Lcom/android/browser/SuggestionsAdapter$SuggestionResults;

    iget-object v0, p0, Lcom/android/browser/SuggestionsAdapter$SlowFilterTask;->this$0:Lcom/android/browser/SuggestionsAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
