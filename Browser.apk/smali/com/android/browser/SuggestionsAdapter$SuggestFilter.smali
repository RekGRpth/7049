.class Lcom/android/browser/SuggestionsAdapter$SuggestFilter;
.super Landroid/widget/Filter;
.source "SuggestionsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/SuggestionsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SuggestFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/browser/SuggestionsAdapter;


# direct methods
.method constructor <init>(Lcom/android/browser/SuggestionsAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method private shouldProcessEmptyQuery()Z
    .locals 2

    iget-object v1, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    iget-object v1, v1, Lcom/android/browser/SuggestionsAdapter;->mSettings:Lcom/android/browser/BrowserSettings;

    invoke-virtual {v1}, Lcom/android/browser/BrowserSettings;->getSearchEngine()Lcom/android/browser/search/SearchEngine;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/browser/search/SearchEngine;->wantsEmptyQuery()Z

    move-result v1

    return v1
.end method


# virtual methods
.method public convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # Ljava/lang/Object;

    if-nez p1, :cond_0

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/android/browser/SuggestionsAdapter$SuggestItem;

    iget-object v1, v0, Lcom/android/browser/SuggestionsAdapter$SuggestItem;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/android/browser/SuggestionsAdapter$SuggestItem;->title:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/android/browser/SuggestionsAdapter$SuggestItem;->url:Ljava/lang/String;

    goto :goto_0
.end method

.method mixResults(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/browser/SuggestionsAdapter$SuggestItem;",
            ">;)V"
        }
    .end annotation

    iget-object v6, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    invoke-static {v6}, Lcom/android/browser/SuggestionsAdapter;->access$000(Lcom/android/browser/SuggestionsAdapter;)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    iget-object v6, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    iget-object v6, v6, Lcom/android/browser/SuggestionsAdapter;->mSources:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_1

    iget-object v6, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    iget-object v6, v6, Lcom/android/browser/SuggestionsAdapter;->mSources:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/browser/SuggestionsAdapter$CursorSource;

    invoke-virtual {v5}, Lcom/android/browser/SuggestionsAdapter$CursorSource;->getCount()I

    move-result v6

    invoke-static {v6, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    sub-int/2addr v2, v4

    const/4 v3, 0x0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_0

    invoke-virtual {v5}, Lcom/android/browser/SuggestionsAdapter$CursorSource;->getItem()Lcom/android/browser/SuggestionsAdapter$SuggestItem;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Lcom/android/browser/SuggestionsAdapter$CursorSource;->moveToNext()Z

    move-result v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 7
    .param p1    # Ljava/lang/CharSequence;

    new-instance v3, Landroid/widget/Filter$FilterResults;

    invoke-direct {v3}, Landroid/widget/Filter$FilterResults;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0}, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->shouldProcessEmptyQuery()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x0

    iput v5, v3, Landroid/widget/Filter$FilterResults;->count:I

    const/4 v5, 0x0

    iput-object v5, v3, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->startSuggestionsAsync(Ljava/lang/CharSequence;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_2

    iget-object v5, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    iget-object v5, v5, Lcom/android/browser/SuggestionsAdapter;->mSources:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/browser/SuggestionsAdapter$CursorSource;

    invoke-virtual {v4, p1}, Lcom/android/browser/SuggestionsAdapter$CursorSource;->runQuery(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->mixResults(Ljava/util/List;)V

    :cond_2
    iget-object v5, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    iget-object v6, v5, Lcom/android/browser/SuggestionsAdapter;->mResultsLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    iput-object v0, v5, Lcom/android/browser/SuggestionsAdapter;->mFilterResults:Ljava/util/List;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v5, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    invoke-virtual {v5}, Lcom/android/browser/SuggestionsAdapter;->buildSuggestionResults()Lcom/android/browser/SuggestionsAdapter$SuggestionResults;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/browser/SuggestionsAdapter$SuggestionResults;->getLineCount()I

    move-result v5

    iput v5, v3, Landroid/widget/Filter$FilterResults;->count:I

    iput-object v2, v3, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/widget/Filter$FilterResults;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/browser/SuggestionsAdapter$SuggestionResults;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Lcom/android/browser/SuggestionsAdapter$SuggestionResults;

    iput-object v0, v1, Lcom/android/browser/SuggestionsAdapter;->mMixedResults:Lcom/android/browser/SuggestionsAdapter$SuggestionResults;

    iget-object v0, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method startSuggestionsAsync(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    iget-boolean v0, v0, Lcom/android/browser/SuggestionsAdapter;->mIncognitoMode:Z

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/browser/SuggestionsAdapter$SlowFilterTask;

    iget-object v1, p0, Lcom/android/browser/SuggestionsAdapter$SuggestFilter;->this$0:Lcom/android/browser/SuggestionsAdapter;

    invoke-direct {v0, v1}, Lcom/android/browser/SuggestionsAdapter$SlowFilterTask;-><init>(Lcom/android/browser/SuggestionsAdapter;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method
