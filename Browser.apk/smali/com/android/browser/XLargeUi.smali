.class public Lcom/android/browser/XLargeUi;
.super Lcom/android/browser/BaseUi;
.source "XLargeUi.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "XLargeUi"


# instance fields
.field private mActionBar:Landroid/app/ActionBar;

.field private mFaviconBackground:Landroid/graphics/drawable/PaintDrawable;

.field private mHandler:Landroid/os/Handler;

.field private mNavBar:Lcom/android/browser/NavigationBarTablet;

.field private mTabBar:Lcom/android/browser/TabBar;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/android/browser/UiController;)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/android/browser/UiController;

    invoke-direct {p0, p1, p2}, Lcom/android/browser/BaseUi;-><init>(Landroid/app/Activity;Lcom/android/browser/UiController;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/browser/XLargeUi;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v0}, Lcom/android/browser/TitleBar;->getNavigationBar()Lcom/android/browser/NavigationBarBase;

    move-result-object v0

    check-cast v0, Lcom/android/browser/NavigationBarTablet;

    iput-object v0, p0, Lcom/android/browser/XLargeUi;->mNavBar:Lcom/android/browser/NavigationBarTablet;

    new-instance v0, Lcom/android/browser/TabBar;

    iget-object v1, p0, Lcom/android/browser/BaseUi;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/browser/BaseUi;->mUiController:Lcom/android/browser/UiController;

    invoke-direct {v0, v1, v2, p0}, Lcom/android/browser/TabBar;-><init>(Landroid/app/Activity;Lcom/android/browser/UiController;Lcom/android/browser/XLargeUi;)V

    iput-object v0, p0, Lcom/android/browser/XLargeUi;->mTabBar:Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/BaseUi;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/XLargeUi;->mActionBar:Landroid/app/ActionBar;

    invoke-direct {p0}, Lcom/android/browser/XLargeUi;->setupActionBar()V

    invoke-static {}, Lcom/android/browser/BrowserSettings;->getInstance()Lcom/android/browser/BrowserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/browser/BrowserSettings;->useQuickControls()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/browser/XLargeUi;->setUseQuickControls(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/XLargeUi;)Landroid/app/ActionBar;
    .locals 1
    .param p0    # Lcom/android/browser/XLargeUi;

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method private checkHideActionBar()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/browser/BaseUi;->mUseQuickControls:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/browser/XLargeUi$1;

    invoke-direct {v1, p0}, Lcom/android/browser/XLargeUi$1;-><init>(Lcom/android/browser/XLargeUi;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private getFaviconBackground()Landroid/graphics/drawable/Drawable;
    .locals 3

    iget-object v1, p0, Lcom/android/browser/XLargeUi;->mFaviconBackground:Landroid/graphics/drawable/PaintDrawable;

    if-nez v1, :cond_0

    new-instance v1, Landroid/graphics/drawable/PaintDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/PaintDrawable;-><init>()V

    iput-object v1, p0, Lcom/android/browser/XLargeUi;->mFaviconBackground:Landroid/graphics/drawable/PaintDrawable;

    iget-object v1, p0, Lcom/android/browser/BaseUi;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/XLargeUi;->mFaviconBackground:Landroid/graphics/drawable/PaintDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    const v2, 0x7f0a0008

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/browser/XLargeUi;->mFaviconBackground:Landroid/graphics/drawable/PaintDrawable;

    const v2, 0x7f0b0021

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/PaintDrawable;->setCornerRadius(F)V

    :cond_0
    iget-object v1, p0, Lcom/android/browser/XLargeUi;->mFaviconBackground:Landroid/graphics/drawable/PaintDrawable;

    return-object v1
.end method

.method private isTypingKey(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setupActionBar()V
    .locals 2

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mActionBar:Landroid/app/ActionBar;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/browser/XLargeUi;->mTabBar:Lcom/android/browser/TabBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public addTab(Lcom/android/browser/Tab;)V
    .locals 1
    .param p1    # Lcom/android/browser/Tab;

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mTabBar:Lcom/android/browser/TabBar;

    invoke-virtual {v0, p1}, Lcom/android/browser/TabBar;->onNewTab(Lcom/android/browser/Tab;)V

    return-void
.end method

.method public dispatchKey(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/browser/BaseUi;->mActiveTab:Lcom/android/browser/Tab;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/browser/BaseUi;->mActiveTab:Lcom/android/browser/Tab;

    invoke-virtual {v4}, Lcom/android/browser/Tab;->getWebView()Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    const/16 v4, 0x1000

    invoke-virtual {p2, v4}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Lcom/android/browser/XLargeUi;->isTypingKey(Landroid/view/KeyEvent;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v4}, Lcom/android/browser/TitleBar;->isEditingUrl()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p0, v2, v3}, Lcom/android/browser/XLargeUi;->editUrl(ZZ)V

    iget-object v2, p0, Lcom/android/browser/BaseUi;->mContentView:Landroid/widget/FrameLayout;

    invoke-virtual {v2, p2}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    :goto_0
    return v2

    :sswitch_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/ViewGroup;->hasFocus()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->hasFocus()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0, v3, v3}, Lcom/android/browser/XLargeUi;->editUrl(ZZ)V

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x15 -> :sswitch_0
        0x3d -> :sswitch_0
    .end sparse-switch
.end method

.method public editUrl(ZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z

    iget-boolean v0, p0, Lcom/android/browser/BaseUi;->mUseQuickControls:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/browser/TitleBar;->setShowProgressOnly(Z)V

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/browser/BaseUi;->editUrl(ZZ)V

    return-void
.end method

.method getContentWidth()I
    .locals 1

    iget-object v0, p0, Lcom/android/browser/BaseUi;->mContentView:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/BaseUi;->mContentView:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFaviconDrawable(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v1, 0x1

    const/4 v2, 0x2

    new-array v6, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/android/browser/XLargeUi;->getFaviconBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    aput-object v4, v6, v3

    if-nez p1, :cond_0

    iget-object v3, p0, Lcom/android/browser/BaseUi;->mGenericFavicon:Landroid/graphics/drawable/Drawable;

    aput-object v3, v6, v1

    :goto_0
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    return-object v0

    :cond_0
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/android/browser/BaseUi;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v3, v6, v1

    goto :goto_0
.end method

.method getTabBar()Lcom/android/browser/TabBar;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mTabBar:Lcom/android/browser/TabBar;

    return-object v0
.end method

.method public onActionModeFinished(Z)V
    .locals 2
    .param p1    # Z

    invoke-direct {p0}, Lcom/android/browser/XLargeUi;->checkHideActionBar()V

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/browser/BaseUi;->mUseQuickControls:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/browser/TitleBar;->setShowProgressOnly(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/browser/BaseUi;->showTitleBar()V

    :cond_1
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 1
    .param p1    # Landroid/view/ActionMode;

    iget-object v0, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v0}, Lcom/android/browser/TitleBar;->isEditingUrl()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/browser/BaseUi;->hideTitleBar()V

    :cond_0
    return-void
.end method

.method protected onAddTabCompleted(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    invoke-direct {p0}, Lcom/android/browser/XLargeUi;->checkHideActionBar()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/browser/BaseUi;->hideTitleBar()V

    return-void
.end method

.method public onHideCustomView()V
    .locals 0

    invoke-super {p0}, Lcom/android/browser/BaseUi;->onHideCustomView()V

    invoke-direct {p0}, Lcom/android/browser/XLargeUi;->checkHideActionBar()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    const v1, 0x7f0d00b2

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method protected onRemoveTabCompleted(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    invoke-direct {p0}, Lcom/android/browser/XLargeUi;->checkHideActionBar()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/browser/BaseUi;->onResume()V

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mNavBar:Lcom/android/browser/NavigationBarTablet;

    invoke-virtual {v0}, Lcom/android/browser/NavigationBarBase;->clearCompletions()V

    invoke-direct {p0}, Lcom/android/browser/XLargeUi;->checkHideActionBar()V

    return-void
.end method

.method public removeTab(Lcom/android/browser/Tab;)V
    .locals 2
    .param p1    # Lcom/android/browser/Tab;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v0, v1}, Lcom/android/browser/TitleBar;->cancelTitleBarAnimation(Z)V

    iget-object v0, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v0, v1}, Lcom/android/browser/TitleBar;->setSkipTitleBarAnimations(Z)V

    invoke-super {p0, p1}, Lcom/android/browser/BaseUi;->removeTab(Lcom/android/browser/Tab;)V

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mTabBar:Lcom/android/browser/TabBar;

    invoke-virtual {v0, p1}, Lcom/android/browser/TabBar;->onRemoveTab(Lcom/android/browser/Tab;)V

    iget-object v0, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/browser/TitleBar;->setSkipTitleBarAnimations(Z)V

    return-void
.end method

.method public setActiveTab(Lcom/android/browser/Tab;)V
    .locals 3
    .param p1    # Lcom/android/browser/Tab;

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v1, v2}, Lcom/android/browser/TitleBar;->cancelTitleBarAnimation(Z)V

    iget-object v1, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v1, v2}, Lcom/android/browser/TitleBar;->setSkipTitleBarAnimations(Z)V

    invoke-super {p0, p1}, Lcom/android/browser/BaseUi;->setActiveTab(Lcom/android/browser/Tab;)V

    invoke-virtual {p1}, Lcom/android/browser/Tab;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    check-cast v0, Lcom/android/browser/BrowserWebView;

    if-nez v0, :cond_0

    const-string v1, "XLargeUi"

    const-string v2, "active tab with no webview detected"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/browser/XLargeUi;->mTabBar:Lcom/android/browser/TabBar;

    invoke-virtual {v1, p1}, Lcom/android/browser/TabBar;->onSetActiveTab(Lcom/android/browser/Tab;)V

    invoke-virtual {p0, p1}, Lcom/android/browser/BaseUi;->updateLockIconToLatest(Lcom/android/browser/Tab;)V

    iget-object v1, p0, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/browser/TitleBar;->setSkipTitleBarAnimations(Z)V

    goto :goto_0
.end method

.method public setFavicon(Lcom/android/browser/Tab;)V
    .locals 2
    .param p1    # Lcom/android/browser/Tab;

    invoke-super {p0, p1}, Lcom/android/browser/BaseUi;->setFavicon(Lcom/android/browser/Tab;)V

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mTabBar:Lcom/android/browser/TabBar;

    invoke-virtual {p1}, Lcom/android/browser/Tab;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/android/browser/TabBar;->onFavicon(Lcom/android/browser/Tab;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public setUrlTitle(Lcom/android/browser/Tab;)V
    .locals 3
    .param p1    # Lcom/android/browser/Tab;

    invoke-super {p0, p1}, Lcom/android/browser/BaseUi;->setUrlTitle(Lcom/android/browser/Tab;)V

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mTabBar:Lcom/android/browser/TabBar;

    invoke-virtual {p1}, Lcom/android/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/browser/Tab;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/browser/TabBar;->onUrlAndTitle(Lcom/android/browser/Tab;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setUseQuickControls(Z)V
    .locals 4
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/browser/BaseUi;->setUseQuickControls(Z)V

    invoke-direct {p0}, Lcom/android/browser/XLargeUi;->checkHideActionBar()V

    if-nez p1, :cond_0

    iget-object v2, p0, Lcom/android/browser/XLargeUi;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->show()V

    :cond_0
    iget-object v2, p0, Lcom/android/browser/XLargeUi;->mTabBar:Lcom/android/browser/TabBar;

    iget-boolean v3, p0, Lcom/android/browser/BaseUi;->mUseQuickControls:Z

    invoke-virtual {v2, v3}, Lcom/android/browser/TabBar;->setUseQuickControls(Z)V

    iget-object v2, p0, Lcom/android/browser/BaseUi;->mTabControl:Lcom/android/browser/TabControl;

    invoke-virtual {v2}, Lcom/android/browser/TabControl;->getTabs()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/browser/Tab;

    invoke-virtual {v1}, Lcom/android/browser/Tab;->updateShouldCaptureThumbnails()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public shouldCaptureThumbnails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/BaseUi;->mUseQuickControls:Z

    return v0
.end method

.method public showComboView(Lcom/android/browser/UI$ComboViews;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Lcom/android/browser/UI$ComboViews;
    .param p2    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2}, Lcom/android/browser/BaseUi;->showComboView(Lcom/android/browser/UI$ComboViews;Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/android/browser/BaseUi;->mUseQuickControls:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    :cond_0
    return-void
.end method

.method stopWebViewScrolling()V
    .locals 2

    iget-object v1, p0, Lcom/android/browser/BaseUi;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v1}, Lcom/android/browser/UiController;->getCurrentWebView()Landroid/webkit/WebView;

    move-result-object v0

    check-cast v0, Lcom/android/browser/BrowserWebView;

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/webkit/WebViewClassic;->fromWebView(Landroid/webkit/WebView;)Landroid/webkit/WebViewClassic;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->stopScroll()V

    :cond_0
    return-void
.end method

.method protected updateNavigationState(Lcom/android/browser/Tab;)V
    .locals 1
    .param p1    # Lcom/android/browser/Tab;

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mNavBar:Lcom/android/browser/NavigationBarTablet;

    invoke-virtual {v0, p1}, Lcom/android/browser/NavigationBarTablet;->updateNavigationState(Lcom/android/browser/Tab;)V

    return-void
.end method

.method public updateTabs(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/browser/Tab;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/browser/XLargeUi;->mTabBar:Lcom/android/browser/TabBar;

    invoke-virtual {v0, p1}, Lcom/android/browser/TabBar;->updateTabs(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/android/browser/XLargeUi;->checkHideActionBar()V

    return-void
.end method
