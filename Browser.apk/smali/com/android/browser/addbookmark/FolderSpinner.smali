.class public Lcom/android/browser/addbookmark/FolderSpinner;
.super Landroid/widget/Spinner;
.source "FolderSpinner.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/addbookmark/FolderSpinner$OnSetSelectionListener;
    }
.end annotation


# instance fields
.field private mFireSetSelection:Z

.field private mOnSetSelectionListener:Lcom/android/browser/addbookmark/FolderSpinner$OnSetSelectionListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-super {p0, p0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/android/browser/addbookmark/FolderSpinner;->mFireSetSelection:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/addbookmark/FolderSpinner;->mOnSetSelectionListener:Lcom/android/browser/addbookmark/FolderSpinner$OnSetSelectionListener;

    invoke-interface {v0, p4, p5}, Lcom/android/browser/addbookmark/FolderSpinner$OnSetSelectionListener;->onSetSelection(J)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/addbookmark/FolderSpinner;->mFireSetSelection:Z

    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 2
    .param p1    # Landroid/widget/AdapterView$OnItemSelectedListener;

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot set an OnItemSelectedListener on a FolderSpinner"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnSetSelectionListener(Lcom/android/browser/addbookmark/FolderSpinner$OnSetSelectionListener;)V
    .locals 0
    .param p1    # Lcom/android/browser/addbookmark/FolderSpinner$OnSetSelectionListener;

    iput-object p1, p0, Lcom/android/browser/addbookmark/FolderSpinner;->mOnSetSelectionListener:Lcom/android/browser/addbookmark/FolderSpinner$OnSetSelectionListener;

    return-void
.end method

.method public setSelection(I)V
    .locals 7
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/browser/addbookmark/FolderSpinner;->mFireSetSelection:Z

    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v6

    invoke-super {p0, p1}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v0, p0, Lcom/android/browser/addbookmark/FolderSpinner;->mOnSetSelectionListener:Lcom/android/browser/addbookmark/FolderSpinner$OnSetSelectionListener;

    if-eqz v0, :cond_0

    if-ne v6, p1, :cond_0

    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/widget/SpinnerAdapter;->getItemId(I)J

    move-result-wide v4

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p0

    move v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/browser/addbookmark/FolderSpinner;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :cond_0
    return-void
.end method

.method public setSelectionIgnoringSelectionChange(I)V
    .locals 0
    .param p1    # I

    invoke-super {p0, p1}, Landroid/widget/AbsSpinner;->setSelection(I)V

    return-void
.end method
