.class public Lcom/android/browser/addbookmark/FolderSpinnerAdapter;
.super Landroid/widget/BaseAdapter;
.source "FolderSpinnerAdapter.java"


# static fields
.field public static final HOME_SCREEN:I = 0x0

.field public static final OTHER_FOLDER:I = 0x2

.field public static final RECENT_FOLDER:I = 0x3

.field public static final ROOT_FOLDER:I = 0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIncludeHomeScreen:Z

.field private mIncludesRecentFolder:Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private mOtherFolderDisplayText:Ljava/lang/String;

.field private mRecentFolderId:J

.field private mRecentFolderName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-boolean p2, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mIncludeHomeScreen:Z

    iput-object p1, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method private bindView(ILandroid/view/View;Z)V
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Z

    const/4 v5, 0x0

    iget-boolean v4, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mIncludeHomeScreen:Z

    if-nez v4, :cond_0

    add-int/lit8 p1, p1, 0x1

    :cond_0
    packed-switch p1, :pswitch_data_0

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    move-object v3, p2

    check-cast v3, Landroid/widget/TextView;

    const/4 v4, 0x3

    if-ne p1, v4, :cond_1

    iget-object v4, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mRecentFolderName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v4, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void

    :pswitch_0
    const v2, 0x7f0c0076

    const v1, 0x7f02002e

    goto :goto_0

    :pswitch_1
    const v2, 0x7f0c0075

    const v1, 0x7f020020

    goto :goto_0

    :pswitch_2
    const v2, 0x7f0c0077

    const v1, 0x7f020027

    goto :goto_0

    :cond_1
    const/4 v4, 0x2

    if-ne p1, v4, :cond_2

    if-nez p3, :cond_2

    iget-object v4, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mOtherFolderDisplayText:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mOtherFolderDisplayText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public addRecentFolder(JLjava/lang/String;)V
    .locals 1
    .param p1    # J
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mIncludesRecentFolder:Z

    iput-wide p1, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mRecentFolderId:J

    iput-object p3, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mRecentFolderName:Ljava/lang/String;

    return-void
.end method

.method public clearRecentFolder()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mIncludesRecentFolder:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mIncludesRecentFolder:Z

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 2

    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mIncludeHomeScreen:Z

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    iget-boolean v1, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mIncludesRecentFolder:Z

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x1090009

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->bindView(ILandroid/view/View;Z)V

    return-object p2
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 4
    .param p1    # I

    int-to-long v0, p1

    iget-boolean v2, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mIncludeHomeScreen:Z

    if-nez v2, :cond_0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    :cond_0
    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x1090008

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2, v2}, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->bindView(ILandroid/view/View;Z)V

    return-object p2
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public recentFolderId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mRecentFolderId:J

    return-wide v0
.end method

.method public setOtherFolderDisplayText(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->mOtherFolderDisplayText:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
