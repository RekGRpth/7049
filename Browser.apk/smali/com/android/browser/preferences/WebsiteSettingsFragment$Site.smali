.class Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;
.super Ljava/lang/Object;
.source "WebsiteSettingsFragment.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/preferences/WebsiteSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Site"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;",
            ">;"
        }
    .end annotation
.end field

.field static final FEATURE_COUNT:I = 0x2

.field static final FEATURE_GEOLOCATION:I = 0x1

.field static final FEATURE_WEB_STORAGE:I


# instance fields
.field private mFeatures:I

.field private mIcon:Landroid/graphics/Bitmap;

.field private mOrigin:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site$1;

    invoke-direct {v0}, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site$1;-><init>()V

    sput-object v0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mOrigin:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mTitle:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mFeatures:I

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mIcon:Landroid/graphics/Bitmap;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/browser/preferences/WebsiteSettingsFragment$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/android/browser/preferences/WebsiteSettingsFragment$1;

    invoke-direct {p0, p1}, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mOrigin:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mIcon:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mFeatures:I

    return-void
.end method

.method private hideHttp(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "http"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x7

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public addFeature(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mFeatures:I

    const/4 v1, 0x1

    shl-int/2addr v1, p1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mFeatures:I

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getFeatureByIndex(I)I
    .locals 3
    .param p1    # I

    const/4 v1, -0x1

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_2

    invoke-virtual {p0, v0}, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->hasFeature(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    add-int/2addr v1, v2

    if-ne v1, p1, :cond_1

    :goto_2
    return v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public getFeatureCount()I
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->hasFeature(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    return v0
.end method

.method public getIcon()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getOrigin()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mOrigin:Ljava/lang/String;

    return-object v0
.end method

.method public getPrettyOrigin()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mTitle:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mOrigin:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->hideHttp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPrettyTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mTitle:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mOrigin:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->hideHttp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method public hasFeature(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mFeatures:I

    shl-int v2, v0, p1

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeFeature(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mFeatures:I

    const/4 v1, 0x1

    shl-int/2addr v1, p1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mFeatures:I

    return-void
.end method

.method public setIcon(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mIcon:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mTitle:Ljava/lang/String;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mOrigin:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mFeatures:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/android/browser/preferences/WebsiteSettingsFragment$Site;->mIcon:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
