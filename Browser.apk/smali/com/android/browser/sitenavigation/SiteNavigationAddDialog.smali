.class public Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;
.super Landroid/app/Activity;
.source "SiteNavigationAddDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/sitenavigation/SiteNavigationAddDialog$SaveSiteNavigationRunnable;
    }
.end annotation


# static fields
.field private static final ACCEPTABLE_WEBSITE_SCHEMES:[Ljava/lang/String;

.field private static final SAVE_SITE_NAVIGATION:I = 0x64

.field private static final XLOGTAG:Ljava/lang/String; = "browser/AddSiteNavigationPage"


# instance fields
.field private mAddress:Landroid/widget/EditText;

.field private mButtonCancel:Landroid/widget/Button;

.field private mButtonOK:Landroid/widget/Button;

.field private mCancelListener:Landroid/view/View$OnClickListener;

.field private mDialogText:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mIsAdding:Z

.field private mItemUrl:Ljava/lang/String;

.field private mMap:Landroid/os/Bundle;

.field private mName:Landroid/widget/EditText;

.field private mOKListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "http:"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https:"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "about:"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "data:"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "javascript:"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "file:"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "content:"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->ACCEPTABLE_WEBSITE_SCHEMES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog$1;

    invoke-direct {v0, p0}, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog$1;-><init>(Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;)V

    iput-object v0, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mOKListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog$2;

    invoke-direct {v0, p0}, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog$2;-><init>(Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;)V

    iput-object v0, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mCancelListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static isSiteNavigationUrl(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v6, 0x0

    :try_start_0
    sget-object v1, Lcom/android/browser/sitenavigation/SiteNavigation;->SITE_NAVIGATION_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "title"

    aput-object v4, v2, v3

    const-string v3, "url = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "browser/AddSiteNavigationPage"

    const-string v2, "isSiteNavigationUrl will return true."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    move v1, v8

    :goto_0
    return v1

    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move v1, v9

    goto :goto_0

    :catch_0
    move-exception v7

    :try_start_1
    const-string v1, "browser/AddSiteNavigationPage"

    const-string v2, "isSiteNavigationUrl"

    invoke-static {v1, v2, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method private static urlHasAcceptableScheme(Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;

    const/4 v1, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    sget-object v2, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->ACCEPTABLE_WEBSITE_SCHEMES:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->ACCEPTABLE_WEBSITE_SCHEMES:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v3, 0x7f04002b

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mMap:Landroid/os/Bundle;

    const-string v3, "browser/AddSiteNavigationPage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCreate mMap is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mMap:Landroid/os/Bundle;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mMap:Landroid/os/Bundle;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mMap:Landroid/os/Bundle;

    const-string v4, "websites"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mMap:Landroid/os/Bundle;

    :cond_0
    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mMap:Landroid/os/Bundle;

    const-string v4, "name"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mMap:Landroid/os/Bundle;

    const-string v4, "url"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mMap:Landroid/os/Bundle;

    const-string v4, "isAdding"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mIsAdding:Z

    :cond_1
    iput-object v2, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mItemUrl:Ljava/lang/String;

    const v3, 0x7f0d0007

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mName:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mName:Landroid/widget/EditText;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0d0032

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mAddress:Landroid/widget/EditText;

    const-string v3, "about:blank"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mAddress:Landroid/widget/EditText;

    const-string v4, "about:blank"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v3, 0x7f0d0069

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mDialogText:Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mIsAdding:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mDialogText:Landroid/widget/TextView;

    const v4, 0x7f0c002c

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    const v3, 0x7f0d0039

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mButtonOK:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mButtonOK:Landroid/widget/Button;

    iget-object v4, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mOKListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f0d0038

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mButtonCancel:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mButtonCancel:Landroid/widget/Button;

    iget-object v4, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mCancelListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->isInTouchMode()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mButtonOK:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    :cond_3
    return-void

    :cond_4
    iget-object v3, p0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mAddress:Landroid/widget/EditText;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method save()Z
    .locals 21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mName:Landroid/widget/EditText;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mAddress:Landroid/widget/EditText;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/browser/UrlUtils;->fixUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v18

    if-nez v18, :cond_3

    const/4 v5, 0x1

    :goto_0
    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    if-nez v18, :cond_4

    const/4 v6, 0x1

    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    if-nez v5, :cond_0

    if-eqz v6, :cond_5

    :cond_0
    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mName:Landroid/widget/EditText;

    move-object/from16 v18, v0

    const v19, 0x7f0c0030

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    :cond_1
    if-eqz v6, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mAddress:Landroid/widget/EditText;

    move-object/from16 v18, v0

    const v19, 0x7f0c0031

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    :cond_2
    const/16 v18, 0x0

    :goto_2
    return v18

    :cond_3
    const/4 v5, 0x0

    goto :goto_0

    :cond_4
    const/4 v6, 0x0

    goto :goto_1

    :cond_5
    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    :try_start_0
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v18

    const-string v19, "javascript:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_8

    new-instance v16, Ljava/net/URI;

    invoke-direct/range {v16 .. v17}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v13

    invoke-static/range {v17 .. v17}, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->urlHasAcceptableScheme(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_9

    if-eqz v13, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mAddress:Landroid/widget/EditText;

    move-object/from16 v18, v0

    const v19, 0x7f0c0032

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v18, 0x0

    goto :goto_2

    :cond_6
    :try_start_1
    new-instance v2, Landroid/net/WebAddress;

    invoke-direct {v2, v15}, Landroid/net/WebAddress;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/net/ParseException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-virtual {v2}, Landroid/net/WebAddress;->getHost()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    if-nez v18, :cond_7

    new-instance v18, Ljava/net/URISyntaxException;

    const-string v19, ""

    const-string v20, ""

    invoke-direct/range {v18 .. v20}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v18
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mAddress:Landroid/widget/EditText;

    move-object/from16 v18, v0

    const v19, 0x7f0c0089

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    const/16 v18, 0x0

    goto :goto_2

    :catch_1
    move-exception v4

    :try_start_3
    new-instance v18, Ljava/net/URISyntaxException;

    const-string v19, ""

    const-string v20, ""

    invoke-direct/range {v18 .. v20}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v18

    :cond_7
    invoke-virtual {v2}, Landroid/net/WebAddress;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/net/URISyntaxException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v17

    :cond_8
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mIsAdding:Z

    move/from16 v18, v0

    if-eqz v18, :cond_b

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->isSiteNavigationUrl(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mAddress:Landroid/widget/EditText;

    move-object/from16 v18, v0

    const v19, 0x7f0c0037

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    const/16 v18, 0x0

    goto/16 :goto_2

    :cond_9
    :try_start_4
    const-string v9, "://"

    const/4 v8, -0x1

    if-eqz v17, :cond_a

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    :cond_a
    if-lez v8, :cond_8

    const-string v18, "/"

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v19

    add-int v19, v19, v8

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v18

    if-gez v18, :cond_8

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    const-string v18, "browser/AddSiteNavigationPage"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "URL="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/net/URISyntaxException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_3

    :cond_b
    const-string v18, "about:blank"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mItemUrl:Ljava/lang/String;

    move-object/from16 v17, v0

    :cond_c
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v18, "title"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "url"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "itemUrl"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mItemUrl:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x64

    invoke-static/range {v18 .. v19}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v10

    invoke-virtual {v10, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    new-instance v14, Ljava/lang/Thread;

    new-instance v18, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog$SaveSiteNavigationRunnable;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v10}, Lcom/android/browser/sitenavigation/SiteNavigationAddDialog$SaveSiteNavigationRunnable;-><init>(Lcom/android/browser/sitenavigation/SiteNavigationAddDialog;Landroid/os/Message;)V

    move-object/from16 v0, v18

    invoke-direct {v14, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v14}, Ljava/lang/Thread;->start()V

    const/16 v18, 0x1

    goto/16 :goto_2
.end method
