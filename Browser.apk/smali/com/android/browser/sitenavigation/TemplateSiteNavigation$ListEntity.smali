.class Lcom/android/browser/sitenavigation/TemplateSiteNavigation$ListEntity;
.super Ljava/lang/Object;
.source "TemplateSiteNavigation.java"

# interfaces
.implements Lcom/android/browser/sitenavigation/TemplateSiteNavigation$Entity;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/sitenavigation/TemplateSiteNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ListEntity"
.end annotation


# instance fields
.field mKey:Ljava/lang/String;

.field mSubTemplate:Lcom/android/browser/sitenavigation/TemplateSiteNavigation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/browser/sitenavigation/TemplateSiteNavigation$ListEntity;->mKey:Ljava/lang/String;

    new-instance v0, Lcom/android/browser/sitenavigation/TemplateSiteNavigation;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p3, v1}, Lcom/android/browser/sitenavigation/TemplateSiteNavigation;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/browser/sitenavigation/TemplateSiteNavigation$1;)V

    iput-object v0, p0, Lcom/android/browser/sitenavigation/TemplateSiteNavigation$ListEntity;->mSubTemplate:Lcom/android/browser/sitenavigation/TemplateSiteNavigation;

    return-void
.end method


# virtual methods
.method public write(Ljava/io/OutputStream;Lcom/android/browser/sitenavigation/TemplateSiteNavigation$EntityData;)V
    .locals 2
    .param p1    # Ljava/io/OutputStream;
    .param p2    # Lcom/android/browser/sitenavigation/TemplateSiteNavigation$EntityData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/browser/sitenavigation/TemplateSiteNavigation$ListEntity;->mKey:Ljava/lang/String;

    invoke-interface {p2, v1}, Lcom/android/browser/sitenavigation/TemplateSiteNavigation$EntityData;->getListIterator(Ljava/lang/String;)Lcom/android/browser/sitenavigation/TemplateSiteNavigation$ListEntityIterator;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/browser/sitenavigation/TemplateSiteNavigation$ListEntityIterator;->reset()V

    :goto_0
    invoke-interface {v0}, Lcom/android/browser/sitenavigation/TemplateSiteNavigation$ListEntityIterator;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/browser/sitenavigation/TemplateSiteNavigation$ListEntity;->mSubTemplate:Lcom/android/browser/sitenavigation/TemplateSiteNavigation;

    invoke-virtual {v1, p1, v0}, Lcom/android/browser/sitenavigation/TemplateSiteNavigation;->write(Ljava/io/OutputStream;Lcom/android/browser/sitenavigation/TemplateSiteNavigation$EntityData;)V

    goto :goto_0

    :cond_0
    return-void
.end method
