.class Lcom/android/browser/util/ThreadedCursorAdapter$1;
.super Landroid/widget/CursorAdapter;
.source "ThreadedCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/browser/util/ThreadedCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/browser/util/ThreadedCursorAdapter;


# direct methods
.method constructor <init>(Lcom/android/browser/util/ThreadedCursorAdapter;Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;
    .param p4    # I

    iput-object p1, p0, Lcom/android/browser/util/ThreadedCursorAdapter$1;->this$0:Lcom/android/browser/util/ThreadedCursorAdapter;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter$1;->this$0:Lcom/android/browser/util/ThreadedCursorAdapter;

    invoke-virtual {p0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/browser/util/ThreadedCursorAdapter;->access$002(Lcom/android/browser/util/ThreadedCursorAdapter;I)I

    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter$1;->this$0:Lcom/android/browser/util/ThreadedCursorAdapter;

    invoke-static {v0}, Lcom/android/browser/util/ThreadedCursorAdapter;->access$108(Lcom/android/browser/util/ThreadedCursorAdapter;)J

    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter$1;->this$0:Lcom/android/browser/util/ThreadedCursorAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public notifyDataSetInvalidated()V
    .locals 2

    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter$1;->this$0:Lcom/android/browser/util/ThreadedCursorAdapter;

    invoke-virtual {p0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/browser/util/ThreadedCursorAdapter;->access$002(Lcom/android/browser/util/ThreadedCursorAdapter;I)I

    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter$1;->this$0:Lcom/android/browser/util/ThreadedCursorAdapter;

    invoke-static {v0}, Lcom/android/browser/util/ThreadedCursorAdapter;->access$108(Lcom/android/browser/util/ThreadedCursorAdapter;)J

    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter$1;->this$0:Lcom/android/browser/util/ThreadedCursorAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    return-void
.end method
