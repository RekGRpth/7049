.class public Lcom/android/browser/view/BookmarkExpandableView;
.super Landroid/widget/ExpandableListView;
.source "BookmarkExpandableView.java"

# interfaces
.implements Lcom/android/browser/BreadCrumbView$Controller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/view/BookmarkExpandableView$BookmarkContextMenuInfo;,
        Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;
    }
.end annotation


# static fields
.field public static final LOCAL_ACCOUNT_NAME:Ljava/lang/String; = "local"


# instance fields
.field private mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

.field private mBreadcrumbController:Lcom/android/browser/BreadCrumbView$Controller;

.field private mChildClickListener:Landroid/view/View$OnClickListener;

.field private mColumnWidth:I

.field private mContext:Landroid/content/Context;

.field private mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field private mGroupOnClickListener:Landroid/view/View$OnClickListener;

.field private mLongClickable:Z

.field private mMaxColumnCount:I

.field private mOnChildClickListener:Landroid/widget/ExpandableListView$OnChildClickListener;

.field private mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    new-instance v0, Lcom/android/browser/view/BookmarkExpandableView$1;

    invoke-direct {v0, p0}, Lcom/android/browser/view/BookmarkExpandableView$1;-><init>(Lcom/android/browser/view/BookmarkExpandableView;)V

    iput-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mChildClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/browser/view/BookmarkExpandableView$2;

    invoke-direct {v0, p0}, Lcom/android/browser/view/BookmarkExpandableView$2;-><init>(Lcom/android/browser/view/BookmarkExpandableView;)V

    iput-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mGroupOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1}, Lcom/android/browser/view/BookmarkExpandableView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    new-instance v0, Lcom/android/browser/view/BookmarkExpandableView$1;

    invoke-direct {v0, p0}, Lcom/android/browser/view/BookmarkExpandableView$1;-><init>(Lcom/android/browser/view/BookmarkExpandableView;)V

    iput-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mChildClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/browser/view/BookmarkExpandableView$2;

    invoke-direct {v0, p0}, Lcom/android/browser/view/BookmarkExpandableView$2;-><init>(Lcom/android/browser/view/BookmarkExpandableView;)V

    iput-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mGroupOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1}, Lcom/android/browser/view/BookmarkExpandableView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    new-instance v0, Lcom/android/browser/view/BookmarkExpandableView$1;

    invoke-direct {v0, p0}, Lcom/android/browser/view/BookmarkExpandableView$1;-><init>(Lcom/android/browser/view/BookmarkExpandableView;)V

    iput-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mChildClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/browser/view/BookmarkExpandableView$2;

    invoke-direct {v0, p0}, Lcom/android/browser/view/BookmarkExpandableView$2;-><init>(Lcom/android/browser/view/BookmarkExpandableView;)V

    iput-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mGroupOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1}, Lcom/android/browser/view/BookmarkExpandableView;->init(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/browser/view/BookmarkExpandableView;)Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;
    .locals 1
    .param p0    # Lcom/android/browser/view/BookmarkExpandableView;

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/browser/view/BookmarkExpandableView;)Landroid/widget/ExpandableListView$OnChildClickListener;
    .locals 1
    .param p0    # Lcom/android/browser/view/BookmarkExpandableView;

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mOnChildClickListener:Landroid/widget/ExpandableListView$OnChildClickListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/browser/view/BookmarkExpandableView;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/browser/view/BookmarkExpandableView;

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/browser/view/BookmarkExpandableView;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0    # Lcom/android/browser/view/BookmarkExpandableView;
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/browser/view/BookmarkExpandableView;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/browser/view/BookmarkExpandableView;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0    # Lcom/android/browser/view/BookmarkExpandableView;

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mChildClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/browser/view/BookmarkExpandableView;)Z
    .locals 1
    .param p0    # Lcom/android/browser/view/BookmarkExpandableView;

    iget-boolean v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mLongClickable:Z

    return v0
.end method

.method static synthetic access$600(Lcom/android/browser/view/BookmarkExpandableView;)I
    .locals 1
    .param p0    # Lcom/android/browser/view/BookmarkExpandableView;

    iget v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mColumnWidth:I

    return v0
.end method

.method static synthetic access$700(Lcom/android/browser/view/BookmarkExpandableView;)I
    .locals 1
    .param p0    # Lcom/android/browser/view/BookmarkExpandableView;

    iget v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mMaxColumnCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/android/browser/view/BookmarkExpandableView;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0    # Lcom/android/browser/view/BookmarkExpandableView;

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mGroupOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public addAccount(Ljava/lang/String;Lcom/android/browser/BrowserBookmarksAdapter;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/browser/BrowserBookmarksAdapter;
    .param p3    # Z

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v2, v2, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_2

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v2, v2, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mChildren:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/browser/BrowserBookmarksAdapter;

    if-eq v0, p2, :cond_0

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v2, v2, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v2}, Landroid/widget/BaseAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v2, v2, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mChildren:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v2, v2, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mChildren:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v2, v2, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mObserver:Landroid/database/DataSetObserver;

    invoke-virtual {p2, v2}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    invoke-virtual {v2}, Landroid/widget/BaseExpandableListAdapter;->notifyDataSetChanged()V

    if-eqz p3, :cond_1

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    invoke-virtual {v2}, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->getGroupCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v2, v2, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v2, v2, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mChildren:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v2, v2, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mObserver:Landroid/database/DataSetObserver;

    invoke-virtual {p2, v2}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public clearAccounts()V
    .locals 1

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    invoke-virtual {v0}, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->clear()V

    return-void
.end method

.method public createContextMenu(Landroid/view/ContextMenu;)V
    .locals 3
    .param p1    # Landroid/view/ContextMenu;

    invoke-virtual {p0}, Lcom/android/browser/view/BookmarkExpandableView;->getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    move-object v1, p1

    check-cast v1, Lcom/android/internal/view/menu/MenuBuilder;

    invoke-virtual {v1, v0}, Lcom/android/internal/view/menu/MenuBuilder;->setCurrentMenuInfo(Landroid/view/ContextMenu$ContextMenuInfo;)V

    invoke-virtual {p0, p1}, Landroid/view/View;->onCreateContextMenu(Landroid/view/ContextMenu;)V

    iget-object v1, p0, Lcom/android/browser/view/BookmarkExpandableView;->mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/browser/view/BookmarkExpandableView;->mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    invoke-interface {v1, p1, p0, v0}, Landroid/view/View$OnCreateContextMenuListener;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    :cond_0
    move-object v1, p1

    check-cast v1, Lcom/android/internal/view/menu/MenuBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/internal/view/menu/MenuBuilder;->setCurrentMenuInfo(Landroid/view/ContextMenu$ContextMenuInfo;)V

    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    invoke-interface {v1, p1}, Landroid/view/ViewParent;->createContextMenu(Landroid/view/ContextMenu;)V

    :cond_1
    return-void
.end method

.method public getBreadCrumbs(I)Lcom/android/browser/BreadCrumbView;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    invoke-virtual {v0, p1}, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->getBreadCrumbView(I)Lcom/android/browser/BreadCrumbView;

    move-result-object v0

    return-object v0
.end method

.method public getChildAdapter(I)Lcom/android/browser/BrowserBookmarksAdapter;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v0, v0, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mChildren:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/browser/BrowserBookmarksAdapter;

    return-object v0
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method init(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/browser/view/BookmarkExpandableView;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mMaxColumnCount:I

    const/high16 v0, 0x2000000

    invoke-virtual {p0, v0}, Landroid/view/View;->setScrollBarStyle(I)V

    new-instance v0, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v1, p0, Lcom/android/browser/view/BookmarkExpandableView;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;-><init>(Lcom/android/browser/view/BookmarkExpandableView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    invoke-super {p0, v0}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-lez v0, :cond_0

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    invoke-virtual {v2, v0}, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->measureChildren(I)V

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget v2, v2, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mRowPadding:I

    iget-object v3, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget v3, v3, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mRowPadding:I

    invoke-virtual {p0, v2, v4, v3, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    if-eq v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->measureChildren(I)V

    :cond_1
    return-void
.end method

.method public onTop(Lcom/android/browser/BreadCrumbView;ILjava/lang/Object;)V
    .locals 1
    .param p1    # Lcom/android/browser/BreadCrumbView;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mBreadcrumbController:Lcom/android/browser/BreadCrumbView$Controller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mBreadcrumbController:Lcom/android/browser/BreadCrumbView$Controller;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/browser/BreadCrumbView$Controller;->onTop(Lcom/android/browser/BreadCrumbView;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public saveGroupState()Lorg/json/JSONObject;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    iget-object v4, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    invoke-virtual {v4}, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->getGroupCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    iget-object v4, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    iget-object v4, v4, Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v0, :cond_1

    :goto_1
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "local"

    goto :goto_1

    :cond_2
    return-object v3
.end method

.method public setAdapter(Landroid/widget/ExpandableListAdapter;)V
    .locals 2
    .param p1    # Landroid/widget/ExpandableListAdapter;

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not supported"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setBreadcrumbController(Lcom/android/browser/BreadCrumbView$Controller;)V
    .locals 0
    .param p1    # Lcom/android/browser/BreadCrumbView$Controller;

    iput-object p1, p0, Lcom/android/browser/view/BookmarkExpandableView;->mBreadcrumbController:Lcom/android/browser/BreadCrumbView$Controller;

    return-void
.end method

.method public setColumnWidthFromLayout(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Landroid/view/View;->measure(II)V

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iput v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mColumnWidth:I

    return-void
.end method

.method public setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V
    .locals 0
    .param p1    # Landroid/widget/ExpandableListView$OnChildClickListener;

    iput-object p1, p0, Lcom/android/browser/view/BookmarkExpandableView;->mOnChildClickListener:Landroid/widget/ExpandableListView$OnChildClickListener;

    return-void
.end method

.method public setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnCreateContextMenuListener;

    iput-object p1, p0, Lcom/android/browser/view/BookmarkExpandableView;->mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    iget-boolean v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mLongClickable:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mLongClickable:Z

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/view/BookmarkExpandableView;->mAdapter:Lcom/android/browser/view/BookmarkExpandableView$BookmarkAccountAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseExpandableListAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .locals 4
    .param p1    # Landroid/view/View;

    const/high16 v2, 0x7f0d0000

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v2, 0x7f0d0001

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v2, Lcom/android/browser/view/BookmarkExpandableView$BookmarkContextMenuInfo;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/android/browser/view/BookmarkExpandableView$BookmarkContextMenuInfo;-><init>(IILcom/android/browser/view/BookmarkExpandableView$1;)V

    iput-object v2, p0, Lcom/android/browser/view/BookmarkExpandableView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, p0}, Landroid/view/ViewParent;->showContextMenuForChild(Landroid/view/View;)Z

    :cond_0
    const/4 v2, 0x1

    return v2
.end method
