.class public Lcom/android/browser/view/SnapshotGridView;
.super Landroid/widget/GridView;
.source "SnapshotGridView.java"


# static fields
.field private static final MAX_COLUMNS:I = 0x5


# instance fields
.field private mColWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-lez v2, :cond_0

    iget v3, p0, Lcom/android/browser/view/SnapshotGridView;->mColWidth:I

    if-lez v3, :cond_0

    iget v3, p0, Lcom/android/browser/view/SnapshotGridView;->mColWidth:I

    div-int v0, v2, v3

    const/4 v3, 0x5

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget v4, p0, Lcom/android/browser/view/SnapshotGridView;->mColWidth:I

    mul-int/2addr v3, v4

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/GridView;->onMeasure(II)V

    return-void
.end method

.method public setColumnWidth(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/browser/view/SnapshotGridView;->mColWidth:I

    invoke-super {p0, p1}, Landroid/widget/GridView;->setColumnWidth(I)V

    return-void
.end method
