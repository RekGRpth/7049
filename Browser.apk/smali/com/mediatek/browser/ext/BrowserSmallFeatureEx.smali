.class public Lcom/mediatek/browser/ext/BrowserSmallFeatureEx;
.super Landroid/content/ContextWrapper;
.source "BrowserSmallFeatureEx.java"

# interfaces
.implements Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;


# static fields
.field private static final TAG:Ljava/lang/String; = "BrowserPluginEx"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public addDefaultBookmarksForCustomer(Lcom/mediatek/browser/ext/IBrowserProvider2Ex;Landroid/database/sqlite/SQLiteDatabase;JI)I
    .locals 2
    .param p1    # Lcom/mediatek/browser/ext/IBrowserProvider2Ex;
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3    # J
    .param p5    # I

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: addDefaultBookmarksForCustomer --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public checkAndTrimUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: checkAndTrimUrl --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p1
.end method

.method public getCustomerHomepage()Ljava/lang/String;
    .locals 2

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: getCustomerHomepage --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getOperatorUA(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: getOperatorUA --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPredefinedWebsites()[Ljava/lang/CharSequence;
    .locals 2

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: getPredefinedWebsites --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSearchEngine(Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: getSearchEngine --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public setIntentSearchEngineExtra(Landroid/content/Intent;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: setIntentSearchEngineExtra --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public setStandardFontFamily(Landroid/webkit/WebSettings;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebSettings;
    .param p2    # Ljava/lang/String;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: setStandardFontFamily --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setTextEncodingChoices(Landroid/preference/ListPreference;)V
    .locals 2
    .param p1    # Landroid/preference/ListPreference;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: setTextEncodingChoices --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public shouldChangeBookmarkMenuManner()Z
    .locals 2

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: shouldChangeBookmarkMenuManner --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public shouldCheckUrlLengthLimit()Z
    .locals 2

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: shouldCheckUrlLengthLimit --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public shouldConfigHistoryPageMenuItem(Landroid/view/Menu;ZZ)Z
    .locals 2
    .param p1    # Landroid/view/Menu;
    .param p2    # Z
    .param p3    # Z

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: configHistoryPageMenuItem --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public shouldCreateBookmarksOptionMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: createBookmarksOptionMenu --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public shouldCreateHistoryPageOptionMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: createHistoryPageOptionMenu --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public shouldDownloadPreference()Z
    .locals 2

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: shouldDownloadPreference --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public shouldLoadCustomerAdvancedXml()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public shouldOnlyLandscape(Landroid/content/SharedPreferences;)Z
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: shouldOnlyLandscape --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public shouldProcessResultForFileManager()Z
    .locals 2

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: shouldProcessResultForFileManager --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public shouldSetNavigationBarTitle()Z
    .locals 2

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: shouldSetNavigationBarTitle --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public shouldTransferToWapBrowser()Z
    .locals 2

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: shouldTransferToWapBrowser --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public updatePreferenceItem(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/String;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: updatePreferenceItem --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public updatePreferenceItemAndSetListener(Landroid/preference/Preference;Ljava/lang/String;Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/preference/Preference$OnPreferenceChangeListener;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: updatePreferenceItemAndSetListener --default implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
