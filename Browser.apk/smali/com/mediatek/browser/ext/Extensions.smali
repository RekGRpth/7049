.class public Lcom/mediatek/browser/ext/Extensions;
.super Ljava/lang/Object;
.source "Extensions.java"


# static fields
.field private static sDownloadPlugin:Lcom/mediatek/browser/ext/IBrowserDownloadEx;

.field private static sProcessNetworkPlugin:Lcom/mediatek/browser/ext/IBrowserProcessNetworkEx;

.field private static sSmallFeaturePlugin:Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

.field private static sSmsHandlerPlugin:Lcom/mediatek/browser/ext/IBrowserSmsHandlerEx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/browser/ext/Extensions;->sSmallFeaturePlugin:Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

    sput-object v0, Lcom/mediatek/browser/ext/Extensions;->sDownloadPlugin:Lcom/mediatek/browser/ext/IBrowserDownloadEx;

    sput-object v0, Lcom/mediatek/browser/ext/Extensions;->sProcessNetworkPlugin:Lcom/mediatek/browser/ext/IBrowserProcessNetworkEx;

    sput-object v0, Lcom/mediatek/browser/ext/Extensions;->sSmsHandlerPlugin:Lcom/mediatek/browser/ext/IBrowserSmsHandlerEx;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDownloadPlugin(Landroid/content/Context;)Lcom/mediatek/browser/ext/IBrowserDownloadEx;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/mediatek/browser/ext/Extensions;->sDownloadPlugin:Lcom/mediatek/browser/ext/IBrowserDownloadEx;

    if-nez v1, :cond_0

    :try_start_0
    const-class v1, Lcom/mediatek/browser/ext/IBrowserDownloadEx;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/pluginmanager/PluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/browser/ext/IBrowserDownloadEx;

    sput-object v1, Lcom/mediatek/browser/ext/Extensions;->sDownloadPlugin:Lcom/mediatek/browser/ext/IBrowserDownloadEx;
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v1, Lcom/mediatek/browser/ext/Extensions;->sDownloadPlugin:Lcom/mediatek/browser/ext/IBrowserDownloadEx;

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mediatek/browser/ext/BrowserDownloadEx;

    invoke-direct {v1}, Lcom/mediatek/browser/ext/BrowserDownloadEx;-><init>()V

    sput-object v1, Lcom/mediatek/browser/ext/Extensions;->sDownloadPlugin:Lcom/mediatek/browser/ext/IBrowserDownloadEx;

    goto :goto_0
.end method

.method public static getProcessNetworkPlugin(Landroid/content/Context;)Lcom/mediatek/browser/ext/IBrowserProcessNetworkEx;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/mediatek/browser/ext/Extensions;->sProcessNetworkPlugin:Lcom/mediatek/browser/ext/IBrowserProcessNetworkEx;

    if-nez v1, :cond_0

    :try_start_0
    const-class v1, Lcom/mediatek/browser/ext/IBrowserProcessNetworkEx;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/pluginmanager/PluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/browser/ext/IBrowserProcessNetworkEx;

    sput-object v1, Lcom/mediatek/browser/ext/Extensions;->sProcessNetworkPlugin:Lcom/mediatek/browser/ext/IBrowserProcessNetworkEx;
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v1, Lcom/mediatek/browser/ext/Extensions;->sProcessNetworkPlugin:Lcom/mediatek/browser/ext/IBrowserProcessNetworkEx;

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mediatek/browser/ext/BrowserProcessNetworkEx;

    invoke-direct {v1}, Lcom/mediatek/browser/ext/BrowserProcessNetworkEx;-><init>()V

    sput-object v1, Lcom/mediatek/browser/ext/Extensions;->sProcessNetworkPlugin:Lcom/mediatek/browser/ext/IBrowserProcessNetworkEx;

    goto :goto_0
.end method

.method public static getSmallFeaturePlugin(Landroid/content/Context;)Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/mediatek/browser/ext/Extensions;->sSmallFeaturePlugin:Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

    if-nez v1, :cond_0

    :try_start_0
    const-class v1, Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/pluginmanager/PluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

    sput-object v1, Lcom/mediatek/browser/ext/Extensions;->sSmallFeaturePlugin:Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v1, Lcom/mediatek/browser/ext/Extensions;->sSmallFeaturePlugin:Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mediatek/browser/ext/BrowserSmallFeatureEx;

    invoke-direct {v1, p0}, Lcom/mediatek/browser/ext/BrowserSmallFeatureEx;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/mediatek/browser/ext/Extensions;->sSmallFeaturePlugin:Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

    goto :goto_0
.end method

.method public static getSmsHandlerPlugin(Landroid/content/Context;)Lcom/mediatek/browser/ext/IBrowserSmsHandlerEx;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/mediatek/browser/ext/Extensions;->sSmsHandlerPlugin:Lcom/mediatek/browser/ext/IBrowserSmsHandlerEx;

    if-nez v1, :cond_0

    :try_start_0
    const-class v1, Lcom/mediatek/browser/ext/IBrowserSmsHandlerEx;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/pluginmanager/PluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/browser/ext/IBrowserSmsHandlerEx;

    sput-object v1, Lcom/mediatek/browser/ext/Extensions;->sSmsHandlerPlugin:Lcom/mediatek/browser/ext/IBrowserSmsHandlerEx;
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v1, Lcom/mediatek/browser/ext/Extensions;->sSmsHandlerPlugin:Lcom/mediatek/browser/ext/IBrowserSmsHandlerEx;

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mediatek/browser/ext/BrowserSmsHandlerEx;

    invoke-direct {v1}, Lcom/mediatek/browser/ext/BrowserSmsHandlerEx;-><init>()V

    sput-object v1, Lcom/mediatek/browser/ext/Extensions;->sSmsHandlerPlugin:Lcom/mediatek/browser/ext/IBrowserSmsHandlerEx;

    goto :goto_0
.end method
