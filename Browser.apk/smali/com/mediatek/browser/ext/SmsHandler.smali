.class public Lcom/mediatek/browser/ext/SmsHandler;
.super Ljava/lang/Object;
.source "SmsHandler.java"


# static fields
.field private static final XLOGTAG:Ljava/lang/String; = "browser/SmsHandler"


# instance fields
.field mActivity:Landroid/app/Activity;

.field private mSmsChangedFilter:Landroid/content/IntentFilter;

.field private mSmsIntentReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/browser/ext/SmsHandler;->mActivity:Landroid/app/Activity;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/browser/ext/SmsHandler;->mSmsChangedFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/browser/ext/SmsHandler;->mSmsChangedFilter:Landroid/content/IntentFilter;

    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/browser/ext/SmsHandler$1;

    invoke-direct {v0, p0}, Lcom/mediatek/browser/ext/SmsHandler$1;-><init>(Lcom/mediatek/browser/ext/SmsHandler;)V

    iput-object v0, p0, Lcom/mediatek/browser/ext/SmsHandler;->mSmsIntentReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/browser/ext/SmsHandler;Landroid/content/Intent;)[Landroid/telephony/gsm/SmsMessage;
    .locals 1
    .param p0    # Lcom/mediatek/browser/ext/SmsHandler;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/mediatek/browser/ext/SmsHandler;->fetchMessageFromIntent(Landroid/content/Intent;)[Landroid/telephony/gsm/SmsMessage;

    move-result-object v0

    return-object v0
.end method

.method private fetchMessageFromIntent(Landroid/content/Intent;)[Landroid/telephony/gsm/SmsMessage;
    .locals 5
    .param p1    # Landroid/content/Intent;

    const-string v4, "pdus"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v1, v4

    check-cast v1, [Ljava/lang/Object;

    if-nez v1, :cond_1

    const/4 v2, 0x0

    :cond_0
    return-object v2

    :cond_1
    array-length v3, v1

    new-array v2, v3, [Landroid/telephony/gsm/SmsMessage;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    check-cast v4, [B

    check-cast v4, [B

    invoke-static {v4}, Landroid/telephony/gsm/SmsMessage;->createFromPdu([B)Landroid/telephony/gsm/SmsMessage;

    move-result-object v4

    aput-object v4, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onPause()V
    .locals 2

    const-string v0, "browser/SmsHandler"

    const-string v1, " (SmsHandler::onPause) unregisterReceiver SMS_RECEIVED_ACTION : android.provider.Telephony.SMS_RECEIVED"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/browser/ext/SmsHandler;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/mediatek/browser/ext/SmsHandler;->mSmsIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 3

    const-string v0, "browser/SmsHandler"

    const-string v1, " (SmsHandler::onResume) registerReceiver SMS_RECEIVED_ACTION : android.provider.Telephony.SMS_RECEIVED"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/browser/ext/SmsHandler;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/mediatek/browser/ext/SmsHandler;->mSmsIntentReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/mediatek/browser/ext/SmsHandler;->mSmsChangedFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method
