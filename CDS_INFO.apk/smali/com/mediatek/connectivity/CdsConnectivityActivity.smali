.class public Lcom/mediatek/connectivity/CdsConnectivityActivity;
.super Landroid/app/Activity;
.source "CdsConnectivityActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final DEFAULT_CONN_LIST:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "CdsConnectivityActivity"


# instance fields
.field private mConnMgr:Landroid/net/ConnectivityManager;

.field mConnSpinner:Landroid/widget/Spinner;

.field private mContext:Landroid/content/Context;

.field private mReportBtnCmd:Landroid/widget/Button;

.field private mReportPercent:Landroid/widget/EditText;

.field private mSelectConnType:I

.field private mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Wi-Fi"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Mobile"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->DEFAULT_CONN_LIST:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mConnSpinner:Landroid/widget/Spinner;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mSelectConnType:I

    iput-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mReportPercent:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mConnMgr:Landroid/net/ConnectivityManager;

    iput-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mReportBtnCmd:Landroid/widget/Button;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/connectivity/CdsConnectivityActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/connectivity/CdsConnectivityActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mSelectConnType:I

    return p1
.end method

.method private reportInetAcction()V
    .locals 6

    iget-object v3, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mReportPercent:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mToast:Landroid/widget/Toast;

    const-string v4, "The percent value is empty. This is not allowed"

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0x64

    if-gt v2, v3, :cond_1

    if-gez v2, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mToast:Landroid/widget/Toast;

    const-string v4, "The range fo report percent should be 1 ~ 100"

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v3, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mToast:Landroid/widget/Toast;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ERROR:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v3, "CdsConnectivityActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Report nw:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mSelectConnType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mConnMgr:Landroid/net/ConnectivityManager;

    iget v4, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mSelectConnType:I

    invoke-virtual {v3, v4, v2}, Landroid/net/ConnectivityManager;->reportInetCondition(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v1, "CdsConnectivityActivity"

    const-string v2, "Error button"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    const-string v1, "CdsConnectivityActivity"

    const-string v2, "Report Inet action"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/connectivity/CdsConnectivityActivity;->reportInetAcction()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f070002
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030001

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mContext:Landroid/content/Context;

    const/high16 v1, 0x7f070000

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mConnSpinner:Landroid/widget/Spinner;

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x1090008

    sget-object v2, Lcom/mediatek/connectivity/CdsConnectivityActivity;->DEFAULT_CONN_LIST:[Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mConnSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mConnSpinner:Landroid/widget/Spinner;

    new-instance v2, Lcom/mediatek/connectivity/CdsConnectivityActivity$1;

    invoke-direct {v2, p0}, Lcom/mediatek/connectivity/CdsConnectivityActivity$1;-><init>(Lcom/mediatek/connectivity/CdsConnectivityActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v1, 0x7f070001

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mReportPercent:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mReportPercent:Landroid/widget/EditText;

    const-string v2, "55"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    iput-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mConnMgr:Landroid/net/ConnectivityManager;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mToast:Landroid/widget/Toast;

    const v1, 0x7f070002

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mReportBtnCmd:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsConnectivityActivity;->mReportBtnCmd:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v1, "CdsConnectivityActivity"

    const-string v2, "CdsConnectivityActivity is started"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method
