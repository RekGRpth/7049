.class public Lcom/mediatek/connectivity/CdsDuHelperActivity;
.super Landroid/app/Activity;
.source "CdsDuHelperActivity.java"


# instance fields
.field private mAdapter:Landroid/widget/SimpleAdapter;

.field private mAppUsageReceiver:Landroid/content/BroadcastReceiver;

.field private mBtnClear:Landroid/widget/Button;

.field private mBtnExit:Landroid/widget/Button;

.field private mBtnSnap:Landroid/widget/Button;

.field mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mListview:Landroid/widget/ListView;

.field private mMyArrayAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mThis:Lcom/mediatek/connectivity/CdsDuHelperActivity;

.field private mToast:Landroid/widget/Toast;

.field private mTv:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mList:Ljava/util/ArrayList;

    new-instance v0, Lcom/mediatek/connectivity/CdsDuHelperActivity$4;

    invoke-direct {v0, p0}, Lcom/mediatek/connectivity/CdsDuHelperActivity$4;-><init>(Lcom/mediatek/connectivity/CdsDuHelperActivity;)V

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mAppUsageReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/connectivity/CdsDuHelperActivity;)Lcom/mediatek/connectivity/CdsDuHelperActivity;
    .locals 1
    .param p0    # Lcom/mediatek/connectivity/CdsDuHelperActivity;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mThis:Lcom/mediatek/connectivity/CdsDuHelperActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/connectivity/CdsDuHelperActivity;)Landroid/widget/Toast;
    .locals 1
    .param p0    # Lcom/mediatek/connectivity/CdsDuHelperActivity;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/connectivity/CdsDuHelperActivity;)Landroid/widget/SimpleAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/connectivity/CdsDuHelperActivity;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mAdapter:Landroid/widget/SimpleAdapter;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x2

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f070006

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mListview:Landroid/widget/ListView;

    const/4 v0, 0x0

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mToast:Landroid/widget/Toast;

    iput-object p0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mThis:Lcom/mediatek/connectivity/CdsDuHelperActivity;

    new-instance v0, Landroid/widget/SimpleAdapter;

    iget-object v2, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mList:Ljava/util/ArrayList;

    const v3, 0x7f03000c

    new-array v4, v6, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v4, v5

    const/4 v1, 0x1

    const-string v5, "data"

    aput-object v5, v4, v1

    new-array v5, v6, [I

    fill-array-data v5, :array_0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mAdapter:Landroid/widget/SimpleAdapter;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mListview:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mAdapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v0, 0x7f070003

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mBtnSnap:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mBtnSnap:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/connectivity/CdsDuHelperActivity$1;

    invoke-direct {v1, p0}, Lcom/mediatek/connectivity/CdsDuHelperActivity$1;-><init>(Lcom/mediatek/connectivity/CdsDuHelperActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f070004

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mBtnExit:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mBtnExit:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/connectivity/CdsDuHelperActivity$2;

    invoke-direct {v1, p0}, Lcom/mediatek/connectivity/CdsDuHelperActivity$2;-><init>(Lcom/mediatek/connectivity/CdsDuHelperActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f070005

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mBtnClear:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mBtnClear:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/connectivity/CdsDuHelperActivity$3;

    invoke-direct {v1, p0}, Lcom/mediatek/connectivity/CdsDuHelperActivity$3;-><init>(Lcom/mediatek/connectivity/CdsDuHelperActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x1020014
        0x1020015
    .end array-data
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mAppUsageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "mediatek.net.datausg.DATA_INFO_DONE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsDuHelperActivity;->mAppUsageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method
