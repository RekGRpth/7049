.class public Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;
.super Landroid/app/Activity;
.source "CdsNatvieNetworkSrvActivity.java"


# static fields
.field private static final ERROR_STRING:Ljava/lang/String; = "Command Error"

.field private static final IF_CONFIG_CMD:Ljava/lang/String; = "ip link"

.field private static final IP_ROUTE_CMD:Ljava/lang/String; = "ip -4 route show"

.field private static final MSG_UPDATE_UI:I = 0x3001

.field private static final NET_CFG_CMD:Ljava/lang/String; = "netcfg"

.field private static final NET_STAT_CMD:Ljava/lang/String; = "netstat"

.field private static final TAG:Ljava/lang/String; = "CdsNatvieNetworkSrvActivity"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mIpRoute:Landroid/widget/TextView;

.field private mNetStat:Landroid/widget/TextView;

.field private mNetworkConfig:Landroid/widget/TextView;

.field private mNetworkList:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity$2;

    invoke-direct {v0, p0}, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity$2;-><init>(Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;)V

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;ILjava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->updateInfo(ILjava/lang/String;)V

    return-void
.end method

.method private executeShellCmd(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity$1;-><init>(Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;Ljava/lang/String;I)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private updateFrameworkSrvInfo()V
    .locals 3

    :try_start_0
    const-string v1, "netcfg"

    const v2, 0x7f070015

    invoke-direct {p0, v1, v2}, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->executeShellCmd(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mNetworkList:Landroid/widget/TextView;

    const v2, 0x7f05003d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const-string v1, "ip link"

    const v2, 0x7f070016

    invoke-direct {p0, v1, v2}, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->executeShellCmd(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mNetworkConfig:Landroid/widget/TextView;

    const v2, 0x7f05003d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const-string v1, "ip -4 route show"

    const v2, 0x7f070017

    invoke-direct {p0, v1, v2}, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->executeShellCmd(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mIpRoute:Landroid/widget/TextView;

    const v2, 0x7f05003d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const-string v1, "netstat"

    const v2, 0x7f070018

    invoke-direct {p0, v1, v2}, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->executeShellCmd(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mNetStat:Landroid/widget/TextView;

    const v2, 0x7f05003d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const-string v1, "CdsNatvieNetworkSrvActivity"

    const-string v2, "updateFrameworkSrvInfo Done"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private updateInfo(ILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mNetworkList:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mNetworkConfig:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mIpRoute:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mNetStat:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f070015
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f070015

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mNetworkList:Landroid/widget/TextView;

    const v0, 0x7f070016

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mNetworkConfig:Landroid/widget/TextView;

    const v0, 0x7f070017

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mIpRoute:Landroid/widget/TextView;

    const v0, 0x7f070018

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->mNetStat:Landroid/widget/TextView;

    const-string v0, "CdsNatvieNetworkSrvActivity"

    const-string v1, "CdsFrameworkSrvActivity is started"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/connectivity/CdsNatvieNetworkSrvActivity;->updateFrameworkSrvInfo()V

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    return-void
.end method
