.class Lcom/mediatek/connectivity/CdsUtilityActivity$1;
.super Ljava/lang/Object;
.source "CdsUtilityActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/connectivity/CdsUtilityActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/connectivity/CdsUtilityActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/connectivity/CdsUtilityActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/connectivity/CdsUtilityActivity$1;->this$0:Lcom/mediatek/connectivity/CdsUtilityActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    add-int/lit16 p3, p3, 0x1001

    const/16 v0, 0x1002

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity$1;->this$0:Lcom/mediatek/connectivity/CdsUtilityActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsUtilityActivity;->access$000(Lcom/mediatek/connectivity/CdsUtilityActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    const-string v1, "ping -c 5 www.google.com"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity$1;->this$0:Lcom/mediatek/connectivity/CdsUtilityActivity;

    invoke-static {v0, p3}, Lcom/mediatek/connectivity/CdsUtilityActivity;->access$102(Lcom/mediatek/connectivity/CdsUtilityActivity;I)I

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity$1;->this$0:Lcom/mediatek/connectivity/CdsUtilityActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsUtilityActivity;->access$000(Lcom/mediatek/connectivity/CdsUtilityActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity$1;->this$0:Lcom/mediatek/connectivity/CdsUtilityActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsUtilityActivity;->access$000(Lcom/mediatek/connectivity/CdsUtilityActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsUtilityActivity$1;->this$0:Lcom/mediatek/connectivity/CdsUtilityActivity;

    invoke-static {v1}, Lcom/mediatek/connectivity/CdsUtilityActivity;->access$000(Lcom/mediatek/connectivity/CdsUtilityActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    return-void

    :cond_0
    const/16 v0, 0x1003

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity$1;->this$0:Lcom/mediatek/connectivity/CdsUtilityActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsUtilityActivity;->access$000(Lcom/mediatek/connectivity/CdsUtilityActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    const-string v1, "ping6 -c 5 www.google.com"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x1005

    if-ne p3, v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity$1;->this$0:Lcom/mediatek/connectivity/CdsUtilityActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsUtilityActivity;->access$000(Lcom/mediatek/connectivity/CdsUtilityActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x1001

    if-ne p3, v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity$1;->this$0:Lcom/mediatek/connectivity/CdsUtilityActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsUtilityActivity;->access$000(Lcom/mediatek/connectivity/CdsUtilityActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity$1;->this$0:Lcom/mediatek/connectivity/CdsUtilityActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsUtilityActivity;->access$000(Lcom/mediatek/connectivity/CdsUtilityActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    const-string v1, "www.google.com"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
