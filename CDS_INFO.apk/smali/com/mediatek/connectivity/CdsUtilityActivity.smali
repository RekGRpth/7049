.class public Lcom/mediatek/connectivity/CdsUtilityActivity;
.super Landroid/app/Activity;
.source "CdsUtilityActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/connectivity/CdsUtilityActivity$DnsTask;,
        Lcom/mediatek/connectivity/CdsUtilityActivity$HttpTask;,
        Lcom/mediatek/connectivity/CdsUtilityActivity$ProgressThread;
    }
.end annotation


# static fields
.field private static final BASE:I = 0x1001

.field private static final CMDTYPESTRING:[Ljava/lang/String;

.field private static final DNS:I = 0x1004

.field private static final HTTPRESPONSNE:I = 0x1005

.field private static final HTTPSTRING:Ljava/lang/String; = "http://"

.field private static final MSG_UPDATE_UI:I = 0x3001

.field private static final PING:I = 0x1002

.field private static final PINGSTRING:Ljava/lang/String; = "ping -c 5 www.google.com"

.field private static final PINGV6:I = 0x1003

.field private static final PINGV6STRING:Ljava/lang/String; = "ping6 -c 5 www.google.com"

.field private static final RUN:I = 0x1001

.field private static final TAG:Ljava/lang/String; = "CdsUtilityActivity"

.field private static final WEBSITES:[Ljava/lang/String;


# instance fields
.field private mAutoCompleteAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCmdLineList:Landroid/widget/AutoCompleteTextView;

.field private mCmdOption:I

.field private mConnMgr:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/ProgressDialog;

.field private final mHandler:Landroid/os/Handler;

.field private mOutputScreen:Landroid/widget/TextView;

.field private mProgressThread:Lcom/mediatek/connectivity/CdsUtilityActivity$ProgressThread;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mtk_netstat"

    aput-object v1, v0, v3

    const-string v1, "mtk_route"

    aput-object v1, v0, v4

    const-string v1, "netstat"

    aput-object v1, v0, v5

    const-string v1, "mtk_ifconfig"

    aput-object v1, v0, v6

    const-string v1, "mtk_route del default"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "mtk_route add default ccmni0"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "iptables -t filter -L -n"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ping -c 1 -s 0 www.google.com"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "http://www.google.com"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "http://www.baidu.cn"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "http://www.sina.cn"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "ps"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "getprop"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "setprop "

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "8.8.8.8"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/connectivity/CdsUtilityActivity;->WEBSITES:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "SHELL"

    aput-object v1, v0, v3

    const-string v1, "PING"

    aput-object v1, v0, v4

    const-string v1, "PING IPV6"

    aput-object v1, v0, v5

    const-string v1, "DNS"

    aput-object v1, v0, v6

    const-string v1, "HTTP RESPONSE"

    aput-object v1, v0, v7

    sput-object v0, Lcom/mediatek/connectivity/CdsUtilityActivity;->CMDTYPESTRING:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mDialog:Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mProgressThread:Lcom/mediatek/connectivity/CdsUtilityActivity$ProgressThread;

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mOutputScreen:Landroid/widget/TextView;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdOption:I

    new-instance v0, Lcom/mediatek/connectivity/CdsUtilityActivity$3;

    invoke-direct {v0, p0}, Lcom/mediatek/connectivity/CdsUtilityActivity$3;-><init>(Lcom/mediatek/connectivity/CdsUtilityActivity;)V

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/connectivity/CdsUtilityActivity;)Landroid/widget/AutoCompleteTextView;
    .locals 1
    .param p0    # Lcom/mediatek/connectivity/CdsUtilityActivity;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdLineList:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/connectivity/CdsUtilityActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/connectivity/CdsUtilityActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdOption:I

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/connectivity/CdsUtilityActivity;)Lcom/mediatek/connectivity/CdsUtilityActivity$ProgressThread;
    .locals 1
    .param p0    # Lcom/mediatek/connectivity/CdsUtilityActivity;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mProgressThread:Lcom/mediatek/connectivity/CdsUtilityActivity$ProgressThread;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/connectivity/CdsUtilityActivity;Lcom/mediatek/connectivity/CdsUtilityActivity$ProgressThread;)Lcom/mediatek/connectivity/CdsUtilityActivity$ProgressThread;
    .locals 0
    .param p0    # Lcom/mediatek/connectivity/CdsUtilityActivity;
    .param p1    # Lcom/mediatek/connectivity/CdsUtilityActivity$ProgressThread;

    iput-object p1, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mProgressThread:Lcom/mediatek/connectivity/CdsUtilityActivity$ProgressThread;

    return-object p1
.end method

.method static synthetic access$300(Lcom/mediatek/connectivity/CdsUtilityActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/connectivity/CdsUtilityActivity;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/connectivity/CdsUtilityActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/connectivity/CdsUtilityActivity;

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mOutputScreen:Landroid/widget/TextView;

    return-object v0
.end method

.method private handleRunCmd()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdLineList:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "CdsUtilityActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdOption:I

    const/16 v4, 0x1002

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdOption:I

    const/16 v4, 0x1001

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdOption:I

    const/16 v4, 0x1003

    if-ne v3, v4, :cond_2

    :cond_0
    const-string v3, "CdsUtilityActivity"

    const-string v4, "Run PING/RUN command"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/mediatek/connectivity/CdsUtilityActivity$2;

    invoke-direct {v4, p0}, Lcom/mediatek/connectivity/CdsUtilityActivity$2;-><init>(Lcom/mediatek/connectivity/CdsUtilityActivity;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdOption:I

    const/16 v4, 0x1005

    if-ne v3, v4, :cond_3

    const-string v3, "CdsUtilityActivity"

    const-string v4, "Run HTTPRESPONSNE command"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/mediatek/connectivity/CdsUtilityActivity$HttpTask;

    invoke-direct {v2, p0, v8}, Lcom/mediatek/connectivity/CdsUtilityActivity$HttpTask;-><init>(Lcom/mediatek/connectivity/CdsUtilityActivity;Lcom/mediatek/connectivity/CdsUtilityActivity$1;)V

    new-array v3, v7, [Ljava/lang/String;

    aput-object v0, v3, v6

    invoke-virtual {v2, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_3
    iget v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdOption:I

    const/16 v4, 0x1004

    if-ne v3, v4, :cond_1

    const-string v3, "CdsUtilityActivity"

    const-string v4, "Run DNS command"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/mediatek/connectivity/CdsUtilityActivity$DnsTask;

    invoke-direct {v1, p0, v8}, Lcom/mediatek/connectivity/CdsUtilityActivity$DnsTask;-><init>(Lcom/mediatek/connectivity/CdsUtilityActivity;Lcom/mediatek/connectivity/CdsUtilityActivity$1;)V

    new-array v3, v7, [Ljava/lang/String;

    aput-object v0, v3, v6

    invoke-virtual {v1, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "CdsUtilityActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "button id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/connectivity/CdsUtilityActivity;->handleRunCmd()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f07001a
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f030007

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mContext:Landroid/content/Context;

    if-nez v3, :cond_0

    const-string v3, "CdsUtilityActivity"

    const-string v4, "Could not get Conext of this activity"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    iput-object v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mConnMgr:Landroid/net/ConnectivityManager;

    iget-object v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mConnMgr:Landroid/net/ConnectivityManager;

    if-nez v3, :cond_1

    const-string v3, "CdsUtilityActivity"

    const-string v4, "Could not get Connectivity Manager"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    new-instance v3, Landroid/widget/ArrayAdapter;

    const v4, 0x109000a

    sget-object v5, Lcom/mediatek/connectivity/CdsUtilityActivity;->WEBSITES:[Ljava/lang/String;

    invoke-direct {v3, p0, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mAutoCompleteAdapter:Landroid/widget/ArrayAdapter;

    const v3, 0x7f070019

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/AutoCompleteTextView;

    iput-object v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdLineList:Landroid/widget/AutoCompleteTextView;

    iget-object v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdLineList:Landroid/widget/AutoCompleteTextView;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    iget-object v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mCmdLineList:Landroid/widget/AutoCompleteTextView;

    iget-object v4, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mAutoCompleteAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v3, 0x7f07001c

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/connectivity/CdsUtilityActivity;->mOutputScreen:Landroid/widget/TextView;

    const v3, 0x7f07001b

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v3, 0x1090008

    sget-object v4, Lcom/mediatek/connectivity/CdsUtilityActivity;->CMDTYPESTRING:[Ljava/lang/String;

    invoke-direct {v0, p0, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v3, 0x1090009

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v3, Lcom/mediatek/connectivity/CdsUtilityActivity$1;

    invoke-direct {v3, p0}, Lcom/mediatek/connectivity/CdsUtilityActivity$1;-><init>(Lcom/mediatek/connectivity/CdsUtilityActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v3, 0x7f07001a

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v3, "CdsUtilityActivity"

    const-string v4, "CdsUtilityActivity is started"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
