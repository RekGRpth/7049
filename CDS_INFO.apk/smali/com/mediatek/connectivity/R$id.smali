.class public final Lcom/mediatek/connectivity/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/connectivity/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final Add:I = 0x7f070021

.field public static final AtComLine:I = 0x7f07002b

.field public static final HostAddress:I = 0x7f070020

.field public static final ListViewCdsInfo:I = 0x7f07000d

.field public static final Report:I = 0x7f070002

.field public static final ReportPercent:I = 0x7f070001

.field public static final Start:I = 0x7f07001e

.field public static final Stop:I = 0x7f07001f

.field public static final Submit:I = 0x7f07002c

.field public static final TableRow02:I = 0x7f070008

.field public static final TextView01:I = 0x7f07000e

.field public static final active_link_properties:I = 0x7f070009

.field public static final active_network_info:I = 0x7f070007

.field public static final active_network_quota_info:I = 0x7f07000a

.field public static final apnTypeSpinnner:I = 0x7f07001d

.field public static final attempts:I = 0x7f07003e

.field public static final bssid:I = 0x7f07004f

.field public static final call:I = 0x7f07003a

.field public static final ccmni0_info:I = 0x7f070028

.field public static final ccmni1_info:I = 0x7f070029

.field public static final ccmni2_info:I = 0x7f07002a

.field public static final clear:I = 0x7f070005

.field public static final cmdLine:I = 0x7f070019

.field public static final cmdSpinnner:I = 0x7f07001b

.field public static final connTypeSpinnner:I = 0x7f070000

.field public static final dbm:I = 0x7f070033

.field public static final disconnects:I = 0x7f070040

.field public static final exit:I = 0x7f070004

.field public static final gprs:I = 0x7f070037

.field public static final gprs_attach:I = 0x7f070036

.field public static final gsm:I = 0x7f070035

.field public static final hidden_ssid:I = 0x7f070051

.field public static final httpClientTest:I = 0x7f070048

.field public static final imei:I = 0x7f07002d

.field public static final imsi:I = 0x7f07002e

.field public static final interfaceName:I = 0x7f07000f

.field public static final interfaceUpChk:I = 0x7f070014

.field public static final ip_route:I = 0x7f070017

.field public static final ipaddr:I = 0x7f070052

.field public static final link_speed:I = 0x7f070055

.field public static final listview:I = 0x7f070006

.field public static final location:I = 0x7f07003b

.field public static final mac_label:I = 0x7f07005b

.field public static final mac_update_btn:I = 0x7f07005d

.field public static final macaddr:I = 0x7f070053

.field public static final macid:I = 0x7f07005c

.field public static final mobileOnlyChk:I = 0x7f070013

.field public static final mtuConfigure:I = 0x7f070012

.field public static final mtuValue:I = 0x7f070011

.field public static final neighboring:I = 0x7f07003c

.field public static final net_stat:I = 0x7f070018

.field public static final network:I = 0x7f070038

.field public static final network_config:I = 0x7f070016

.field public static final network_infos:I = 0x7f07000b

.field public static final network_list:I = 0x7f070015

.field public static final network_policy:I = 0x7f07000c

.field public static final network_state:I = 0x7f07004c

.field public static final networkid:I = 0x7f070054

.field public static final number:I = 0x7f070031

.field public static final operator:I = 0x7f070032

.field public static final outputText:I = 0x7f07001c

.field public static final pingHostname:I = 0x7f070047

.field public static final pingIpAddr:I = 0x7f070046

.field public static final pingRadioBtn1:I = 0x7f070058

.field public static final pingRadioBtn2:I = 0x7f070059

.field public static final pingRadioBtn3:I = 0x7f07005a

.field public static final pingRadioGroup:I = 0x7f070057

.field public static final ping_test:I = 0x7f070045

.field public static final radioState:I = 0x7f07002f

.field public static final received:I = 0x7f070042

.field public static final resets:I = 0x7f07003d

.field public static final roaming:I = 0x7f070034

.field public static final rssi:I = 0x7f07004e

.field public static final runBtn:I = 0x7f07001a

.field public static final rx_info:I = 0x7f070024

.field public static final rx_mobile_info:I = 0x7f070026

.field public static final scan:I = 0x7f07004a

.field public static final scan_list:I = 0x7f070056

.field public static final sent:I = 0x7f070041

.field public static final sentSinceReceived:I = 0x7f070043

.field public static final service_state:I = 0x7f070039

.field public static final simState:I = 0x7f070030

.field public static final snapshot:I = 0x7f070003

.field public static final spinnerInterfaceName:I = 0x7f070010

.field public static final ssid:I = 0x7f070050

.field public static final successes:I = 0x7f07003f

.field public static final supplicant_state:I = 0x7f07004d

.field public static final system_property:I = 0x7f070044

.field public static final tether_info:I = 0x7f070022

.field public static final tx_info:I = 0x7f070023

.field public static final tx_mobile_info:I = 0x7f070025

.field public static final update:I = 0x7f070049

.field public static final wifi_state:I = 0x7f07004b

.field public static final wlan_info:I = 0x7f070027


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
