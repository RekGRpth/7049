.class Lcom/android/calculator2/EventListener;
.super Ljava/lang/Object;
.source "EventListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field mHandler:Lcom/android/calculator2/Logic;

.field mPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    instance-of v2, p1, Landroid/widget/Button;

    if-eqz v2, :cond_1

    check-cast p1, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    iget-object v2, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    invoke-virtual {v2, v1}, Lcom/android/calculator2/Logic;->insert(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/calculator2/EventListener;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/calculator2/EventListener;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/calculator2/EventListener;->mPager:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :cond_1
    :goto_0
    return-void

    :sswitch_0
    iget-object v2, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    invoke-virtual {v2}, Lcom/android/calculator2/Logic;->onDelete()V

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    invoke-virtual {v2}, Lcom/android/calculator2/Logic;->onClear()V

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    invoke-virtual {v2}, Lcom/android/calculator2/Logic;->onEnter()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0d0010 -> :sswitch_1
        0x7f0d0011 -> :sswitch_0
        0x7f0d0022 -> :sswitch_2
    .end sparse-switch
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/16 v5, 0x15

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eq p2, v5, :cond_0

    const/16 v4, 0x16

    if-ne p2, v4, :cond_3

    :cond_0
    iget-object v4, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    if-ne p2, v5, :cond_2

    :goto_0
    invoke-virtual {v4, v2}, Lcom/android/calculator2/Logic;->eatHorizontalMove(Z)Z

    move-result v1

    move v2, v1

    :cond_1
    :goto_1
    return v2

    :cond_2
    move v2, v3

    goto :goto_0

    :cond_3
    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    if-eqz p2, :cond_1

    :cond_4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v4

    const/16 v5, 0x3d

    if-ne v4, v5, :cond_5

    if-ne v0, v2, :cond_1

    iget-object v3, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    invoke-virtual {v3}, Lcom/android/calculator2/Logic;->onEnter()V

    goto :goto_1

    :cond_5
    const/16 v4, 0x17

    if-eq p2, v4, :cond_7

    const/16 v4, 0x13

    if-eq p2, v4, :cond_7

    const/16 v4, 0x14

    if-eq p2, v4, :cond_7

    const/16 v4, 0x42

    if-eq p2, v4, :cond_7

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isPrintingKey()Z

    move-result v4

    if-eqz v4, :cond_6

    if-ne v0, v2, :cond_6

    iget-object v2, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    invoke-virtual {v2}, Lcom/android/calculator2/Logic;->onTextChanged()V

    :cond_6
    move v2, v3

    goto :goto_1

    :cond_7
    if-ne v0, v2, :cond_1

    sparse-switch p2, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    iget-object v3, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    invoke-virtual {v3}, Lcom/android/calculator2/Logic;->onUp()V

    goto :goto_1

    :sswitch_1
    iget-object v3, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    invoke-virtual {v3}, Lcom/android/calculator2/Logic;->onEnter()V

    goto :goto_1

    :sswitch_2
    iget-object v3, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    invoke-virtual {v3}, Lcom/android/calculator2/Logic;->onDown()V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_2
        0x17 -> :sswitch_1
        0x42 -> :sswitch_1
    .end sparse-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0d0011

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    invoke-virtual {v1}, Lcom/android/calculator2/Logic;->onClear()V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method setHandler(Lcom/android/calculator2/Logic;Landroid/support/v4/view/ViewPager;)V
    .locals 0
    .param p1    # Lcom/android/calculator2/Logic;
    .param p2    # Landroid/support/v4/view/ViewPager;

    iput-object p1, p0, Lcom/android/calculator2/EventListener;->mHandler:Lcom/android/calculator2/Logic;

    iput-object p2, p0, Lcom/android/calculator2/EventListener;->mPager:Landroid/support/v4/view/ViewPager;

    return-void
.end method
