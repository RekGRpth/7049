.class Lcom/android/calculator2/PanelSwitcher;
.super Landroid/widget/FrameLayout;
.source "PanelSwitcher.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calculator2/PanelSwitcher$Listener;
    }
.end annotation


# static fields
.field private static final ANIM_DURATION:I = 0x190

.field private static final LEFT:I = 0x1

.field private static final MAJOR_MOVE:I = 0x3c

.field private static final RIGHT:I = 0x2


# instance fields
.field private inLeft:Landroid/view/animation/TranslateAnimation;

.field private inRight:Landroid/view/animation/TranslateAnimation;

.field private mChildren:[Landroid/view/View;

.field private mCurrentView:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mListener:Lcom/android/calculator2/PanelSwitcher$Listener;

.field private mPreviousMove:I

.field private mWidth:I

.field private outLeft:Landroid/view/animation/TranslateAnimation;

.field private outRight:Landroid/view/animation/TranslateAnimation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    iput v1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/android/calculator2/PanelSwitcher$1;

    invoke-direct {v1, p0}, Lcom/android/calculator2/PanelSwitcher$1;-><init>(Lcom/android/calculator2/PanelSwitcher;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mGestureDetector:Landroid/view/GestureDetector;

    return-void
.end method

.method private updateCurrentView()V
    .locals 3

    iget-object v1, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    aget-object v2, v1, v0

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method getCurrentIndex()I
    .locals 1

    iget v0, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    return v0
.end method

.method moveLeft()V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    iget-object v1, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/android/calculator2/PanelSwitcher;->mPreviousMove:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/android/calculator2/PanelSwitcher;->inLeft:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/android/calculator2/PanelSwitcher;->outLeft:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    aget-object v0, v0, v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget v0, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    iput v2, p0, Lcom/android/calculator2/PanelSwitcher;->mPreviousMove:I

    :cond_0
    return-void
.end method

.method moveRight()V
    .locals 3

    const/4 v2, 0x2

    iget v0, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/calculator2/PanelSwitcher;->mPreviousMove:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/android/calculator2/PanelSwitcher;->inRight:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/android/calculator2/PanelSwitcher;->outRight:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    aget-object v0, v0, v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget v0, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    iput v2, p0, Lcom/android/calculator2/PanelSwitcher;->mPreviousMove:I

    :cond_0
    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1    # Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mListener:Lcom/android/calculator2/PanelSwitcher$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mListener:Lcom/android/calculator2/PanelSwitcher$Listener;

    invoke-interface {v0}, Lcom/android/calculator2/PanelSwitcher$Listener;->onChange()V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    new-array v2, v0, [Landroid/view/View;

    iput-object v2, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/android/calculator2/PanelSwitcher;->mChildren:[Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/calculator2/PanelSwitcher;->updateCurrentView()V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onSizeChanged(IIII)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-wide/16 v3, 0x190

    const/4 v2, 0x0

    iput p1, p0, Lcom/android/calculator2/PanelSwitcher;->mWidth:I

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mWidth:I

    int-to-float v1, v1

    invoke-direct {v0, v1, v2, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->inLeft:Landroid/view/animation/TranslateAnimation;

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->inLeft:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mWidth:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v1, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->outLeft:Landroid/view/animation/TranslateAnimation;

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mWidth:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-direct {v0, v1, v2, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->inRight:Landroid/view/animation/TranslateAnimation;

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->inRight:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mWidth:I

    int-to-float v1, v1

    invoke-direct {v0, v2, v1, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->outRight:Landroid/view/animation/TranslateAnimation;

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->inLeft:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->outLeft:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->inRight:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->outRight:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/calculator2/PanelSwitcher;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x1

    return v0
.end method

.method setCurrentIndex(I)V
    .locals 2
    .param p1    # I

    iget v1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    if-eq v1, p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput p1, p0, Lcom/android/calculator2/PanelSwitcher;->mCurrentView:I

    invoke-direct {p0}, Lcom/android/calculator2/PanelSwitcher;->updateCurrentView()V

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/calculator2/PanelSwitcher;->mListener:Lcom/android/calculator2/PanelSwitcher$Listener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calculator2/PanelSwitcher;->mListener:Lcom/android/calculator2/PanelSwitcher$Listener;

    invoke-interface {v1}, Lcom/android/calculator2/PanelSwitcher$Listener;->onChange()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setListener(Lcom/android/calculator2/PanelSwitcher$Listener;)V
    .locals 0
    .param p1    # Lcom/android/calculator2/PanelSwitcher$Listener;

    iput-object p1, p0, Lcom/android/calculator2/PanelSwitcher;->mListener:Lcom/android/calculator2/PanelSwitcher$Listener;

    return-void
.end method
