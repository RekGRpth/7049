.class public Lorg/javia/arity/Complex;
.super Ljava/lang/Object;
.source "Complex.java"


# instance fields
.field public im:D

.field public re:D


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(DD)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    return-void
.end method

.method public constructor <init>(Lorg/javia/arity/Complex;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lorg/javia/arity/Complex;->set(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;

    return-void
.end method

.method private final normalizeInfinity()Lorg/javia/arity/Complex;
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_1

    iput-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_0

    iput-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    goto :goto_0
.end method

.method private final sqrt1z()Lorg/javia/arity/Complex;
    .locals 6

    const-wide/high16 v0, 0x3ff0000000000000L

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide/high16 v2, -0x4000000000000000L

    iget-wide v4, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->sqrt()Lorg/javia/arity/Complex;

    move-result-object v0

    return-object v0
.end method

.method private final swap()Lorg/javia/arity/Complex;
    .locals 4

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final abs()D
    .locals 7

    const-wide/16 v5, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpl-double v4, v0, v5

    if-eqz v4, :cond_0

    cmpl-double v4, v2, v5

    if-nez v4, :cond_1

    :cond_0
    add-double/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_1
    cmpl-double v4, v0, v2

    if-lez v4, :cond_2

    const/4 v4, 0x1

    move v6, v4

    :goto_1
    if-eqz v6, :cond_3

    div-double v4, v2, v0

    :goto_2
    if-eqz v6, :cond_4

    :goto_3
    const-wide/high16 v2, 0x3ff0000000000000L

    mul-double/2addr v4, v4

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    move v6, v4

    goto :goto_1

    :cond_3
    div-double v4, v0, v2

    goto :goto_2

    :cond_4
    move-wide v0, v2

    goto :goto_3
.end method

.method public final abs2()D
    .locals 6

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public final acos()Lorg/javia/arity/Complex;
    .locals 7

    const-wide/16 v4, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v4, v5}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    invoke-direct {p0}, Lorg/javia/arity/Complex;->sqrt1z()Lorg/javia/arity/Complex;

    move-result-object v4

    iget-wide v5, p0, Lorg/javia/arity/Complex;->im:D

    sub-double/2addr v0, v5

    iget-wide v5, p0, Lorg/javia/arity/Complex;->re:D

    add-double/2addr v2, v5

    invoke-virtual {v4, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->log()Lorg/javia/arity/Complex;

    move-result-object v0

    iget-wide v1, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v3, p0, Lorg/javia/arity/Complex;->re:D

    neg-double v3, v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final acosh()Lorg/javia/arity/Complex;
    .locals 12

    const-wide/high16 v10, 0x3ff0000000000000L

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    cmpl-double v0, v0, v10

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->acosh(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v6, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v8, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    sub-double/2addr v4, v10

    const-wide/high16 v6, 0x4000000000000000L

    iget-wide v8, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v6, v8

    invoke-virtual {p0, v4, v5, v6, v7}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v4

    invoke-virtual {v4}, Lorg/javia/arity/Complex;->sqrt()Lorg/javia/arity/Complex;

    move-result-object v4

    iget-wide v5, p0, Lorg/javia/arity/Complex;->re:D

    add-double/2addr v0, v5

    iget-wide v5, p0, Lorg/javia/arity/Complex;->im:D

    add-double/2addr v2, v5

    invoke-virtual {v4, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->log()Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final add(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;
    .locals 6

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->ulp(D)D

    move-result-wide v0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v4, p1, Lorg/javia/arity/Complex;->re:D

    add-double/2addr v2, v4

    iput-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p1, Lorg/javia/arity/Complex;->im:D

    add-double/2addr v2, v4

    iput-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4090000000000000L

    mul-double/2addr v0, v4

    cmpg-double v0, v2, v0

    if-gez v0, :cond_0

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    :cond_0
    return-object p0
.end method

.method public final arg()D
    .locals 4

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public asReal()D
    .locals 4

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x7ff8000000000000L

    goto :goto_0
.end method

.method public final asin()Lorg/javia/arity/Complex;
    .locals 7

    const-wide/16 v4, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v4, v5}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    invoke-direct {p0}, Lorg/javia/arity/Complex;->sqrt1z()Lorg/javia/arity/Complex;

    move-result-object v4

    iget-wide v5, p0, Lorg/javia/arity/Complex;->re:D

    sub-double v2, v5, v2

    iget-wide v5, p0, Lorg/javia/arity/Complex;->im:D

    add-double/2addr v0, v5

    invoke-virtual {v4, v2, v3, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->log()Lorg/javia/arity/Complex;

    move-result-object v0

    iget-wide v1, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v3, p0, Lorg/javia/arity/Complex;->re:D

    neg-double v3, v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final asinh()Lorg/javia/arity/Complex;
    .locals 10

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->asinh(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v6, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v8, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    const-wide/high16 v6, 0x3ff0000000000000L

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L

    iget-wide v8, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v6, v8

    invoke-virtual {p0, v4, v5, v6, v7}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v4

    invoke-virtual {v4}, Lorg/javia/arity/Complex;->sqrt()Lorg/javia/arity/Complex;

    move-result-object v4

    iget-wide v5, p0, Lorg/javia/arity/Complex;->re:D

    add-double/2addr v0, v5

    iget-wide v5, p0, Lorg/javia/arity/Complex;->im:D

    add-double/2addr v2, v5

    invoke-virtual {v4, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->log()Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final atan()Lorg/javia/arity/Complex;
    .locals 12

    const-wide/high16 v10, 0x4000000000000000L

    const-wide/high16 v8, 0x3ff0000000000000L

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v2, v4

    add-double v4, v0, v2

    iget-wide v6, p0, Lorg/javia/arity/Complex;->im:D

    sub-double/2addr v4, v6

    iget-wide v6, p0, Lorg/javia/arity/Complex;->im:D

    sub-double/2addr v4, v6

    add-double/2addr v4, v8

    add-double/2addr v0, v2

    sub-double/2addr v0, v8

    neg-double v0, v0

    div-double/2addr v0, v4

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v6, p0, Lorg/javia/arity/Complex;->re:D

    add-double/2addr v2, v6

    neg-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->log()Lorg/javia/arity/Complex;

    move-result-object v0

    iget-wide v1, p0, Lorg/javia/arity/Complex;->im:D

    neg-double v1, v1

    div-double/2addr v1, v10

    iget-wide v3, p0, Lorg/javia/arity/Complex;->re:D

    div-double/2addr v3, v10

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final atanh()Lorg/javia/arity/Complex;
    .locals 12

    const-wide/high16 v10, 0x4000000000000000L

    const-wide/high16 v8, 0x3ff0000000000000L

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->atanh(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v2, v4

    add-double v4, v0, v8

    iget-wide v6, p0, Lorg/javia/arity/Complex;->re:D

    sub-double/2addr v4, v6

    iget-wide v6, p0, Lorg/javia/arity/Complex;->re:D

    sub-double/2addr v4, v6

    sub-double v0, v8, v0

    sub-double/2addr v0, v2

    div-double/2addr v0, v4

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v6, p0, Lorg/javia/arity/Complex;->im:D

    add-double/2addr v2, v6

    div-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->log()Lorg/javia/arity/Complex;

    move-result-object v0

    iget-wide v1, p0, Lorg/javia/arity/Complex;->re:D

    div-double/2addr v1, v10

    iget-wide v3, p0, Lorg/javia/arity/Complex;->im:D

    div-double/2addr v3, v10

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final combinations(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;
    .locals 14

    const-wide/16 v4, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    iget-wide v0, p1, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p1, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1, v2, v3}, Lorg/javia/arity/MoreMath;->combinations(DD)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v4, v5}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->lgamma()Lorg/javia/arity/Complex;

    iget-wide v4, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v6, p0, Lorg/javia/arity/Complex;->im:D

    invoke-virtual {p0, p1}, Lorg/javia/arity/Complex;->set(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;

    move-result-object v8

    invoke-virtual {v8}, Lorg/javia/arity/Complex;->lgamma()Lorg/javia/arity/Complex;

    iget-wide v8, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v10, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v12, p1, Lorg/javia/arity/Complex;->re:D

    sub-double/2addr v0, v12

    iget-wide v12, p1, Lorg/javia/arity/Complex;->im:D

    sub-double/2addr v2, v12

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->lgamma()Lorg/javia/arity/Complex;

    sub-double v0, v4, v8

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    sub-double/2addr v0, v2

    sub-double v2, v6, v10

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    sub-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->exp()Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final conjugate()Lorg/javia/arity/Complex;
    .locals 4

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    neg-double v2, v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    return-object v0
.end method

.method public final cos()Lorg/javia/arity/Complex;
    .locals 6

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->cos(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->cos(D)D

    move-result-wide v0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v2, v3}, Ljava/lang/Math;->cosh(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v2, v3}, Lorg/javia/arity/MoreMath;->sin(D)D

    move-result-wide v2

    neg-double v2, v2

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v4, v5}, Ljava/lang/Math;->sinh(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final cosh()Lorg/javia/arity/Complex;
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->cosh(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lorg/javia/arity/Complex;->swap()Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->cos()Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->conjugate()Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final div(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;
    .locals 10

    const-wide/16 v6, 0x0

    iget-wide v0, p1, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p1, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v4, v4, v6

    if-nez v4, :cond_0

    cmpl-double v4, v2, v6

    if-nez v4, :cond_0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    div-double v0, v2, v0

    invoke-virtual {p0, v0, v1, v6, v7}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lorg/javia/arity/Complex;->isInfinite()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->isFinite()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, v6, v7, v6, v7}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0

    :cond_1
    cmpl-double v4, v2, v6

    if-nez v4, :cond_3

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    cmpl-double v2, v2, v6

    if-nez v2, :cond_2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    div-double v0, v2, v0

    invoke-virtual {p0, v6, v7, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    div-double/2addr v2, v0

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    div-double v0, v4, v0

    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0

    :cond_3
    cmpl-double v4, v0, v6

    if-nez v4, :cond_4

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    div-double/2addr v0, v2

    iget-wide v4, p0, Lorg/javia/arity/Complex;->re:D

    neg-double v4, v4

    div-double v2, v4, v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    cmpl-double v4, v4, v6

    if-lez v4, :cond_5

    div-double v4, v2, v0

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v6, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v6, v4

    add-double/2addr v2, v6

    div-double/2addr v2, v0

    iget-wide v6, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v8, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v4, v8

    sub-double v4, v6, v4

    div-double v0, v4, v0

    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0

    :cond_5
    div-double v4, v0, v2

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v2, v4

    iget-wide v6, p0, Lorg/javia/arity/Complex;->im:D

    add-double/2addr v2, v6

    div-double/2addr v2, v0

    iget-wide v6, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Lorg/javia/arity/Complex;->re:D

    sub-double/2addr v4, v6

    div-double v0, v4, v0

    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final equals(Lorg/javia/arity/Complex;)Z
    .locals 4

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p1, Lorg/javia/arity/Complex;->re:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_2

    iget-wide v0, p1, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p1, Lorg/javia/arity/Complex;->re:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_2

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v2, p1, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_2

    iget-wide v0, p1, Lorg/javia/arity/Complex;->im:D

    iget-wide v2, p1, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final exp()Lorg/javia/arity/Complex;
    .locals 6

    const-wide/16 v4, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    invoke-virtual {p0, v0, v1, v4, v5}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v2, v3}, Lorg/javia/arity/MoreMath;->cos(D)D

    move-result-wide v2

    mul-double/2addr v2, v0

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v4, v5}, Lorg/javia/arity/MoreMath;->sin(D)D

    move-result-wide v4

    mul-double/2addr v0, v4

    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final factorial()Lorg/javia/arity/Complex;
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->factorial(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/javia/arity/Complex;->lgamma()Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->exp()Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final gcd(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;
    .locals 12

    const-wide/16 v8, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v8

    if-nez v0, :cond_1

    iget-wide v0, p1, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v8

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p1, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1, v2, v3}, Lorg/javia/arity/MoreMath;->gcd(DD)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v8, v9}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object p0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    new-instance v4, Lorg/javia/arity/Complex;

    invoke-direct {v4, p1}, Lorg/javia/arity/Complex;-><init>(Lorg/javia/arity/Complex;)V

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->abs2()D

    move-result-wide v2

    invoke-virtual {v4}, Lorg/javia/arity/Complex;->abs2()D

    move-result-wide v0

    :goto_1
    const-wide v5, 0x46293e5939a08ceaL

    mul-double/2addr v5, v0

    cmpg-double v2, v2, v5

    if-gez v2, :cond_2

    iget-wide v2, v4, Lorg/javia/arity/Complex;->re:D

    iget-wide v5, v4, Lorg/javia/arity/Complex;->im:D

    invoke-virtual {p0, v4}, Lorg/javia/arity/Complex;->mod(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;

    move-result-object v7

    invoke-virtual {v4, v7}, Lorg/javia/arity/Complex;->set(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;

    invoke-virtual {p0, v2, v3, v5, v6}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    invoke-virtual {v4}, Lorg/javia/arity/Complex;->abs2()D

    move-result-wide v2

    move-wide v10, v2

    move-wide v2, v0

    move-wide v0, v10

    goto :goto_1

    :cond_2
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_3

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    neg-double v0, v0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    :cond_3
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    cmpg-double v0, v0, v8

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->negate()Lorg/javia/arity/Complex;

    goto :goto_0
.end method

.method public final isFinite()Z
    .locals 1

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->isInfinite()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->isNaN()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInfinite()Z
    .locals 2

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->isNaN()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isNaN()Z
    .locals 2

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lgamma()Lorg/javia/arity/Complex;
    .locals 15

    const-wide v7, 0x3fefffffffffffe6L

    const-wide/16 v5, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v9, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v2, v9

    add-double v3, v0, v2

    iget-wide v1, p0, Lorg/javia/arity/Complex;->re:D

    sget-object v9, Lorg/javia/arity/MoreMath;->GAMMA:[D

    const/4 v0, 0x0

    :goto_0
    array-length v10, v9

    if-ge v0, v10, :cond_0

    const-wide/high16 v10, 0x3ff0000000000000L

    add-double/2addr v1, v10

    add-double v10, v1, v1

    const-wide/high16 v12, 0x3ff0000000000000L

    sub-double/2addr v10, v12

    add-double/2addr v3, v10

    aget-wide v10, v9, v0

    mul-double v12, v10, v1

    div-double/2addr v12, v3

    add-double/2addr v7, v12

    iget-wide v12, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v10, v12

    div-double/2addr v10, v3

    sub-double/2addr v5, v10

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    const-wide/high16 v2, 0x3fe0000000000000L

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    const-wide v9, 0x4014f80000000000L

    add-double/2addr v2, v9

    iget-wide v9, p0, Lorg/javia/arity/Complex;->im:D

    iput-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->log()Lorg/javia/arity/Complex;

    iget-wide v11, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v11, v0

    iget-wide v13, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v13, v9

    sub-double/2addr v11, v13

    const-wide v13, 0x3fed67f1c864beb5L

    add-double/2addr v11, v13

    sub-double v2, v11, v2

    iget-wide v11, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v0, v11

    iget-wide v11, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v11, v9

    add-double/2addr v0, v11

    sub-double/2addr v0, v9

    invoke-virtual {p0, v7, v8, v5, v6}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v4

    invoke-virtual {v4}, Lorg/javia/arity/Complex;->log()Lorg/javia/arity/Complex;

    iget-wide v4, p0, Lorg/javia/arity/Complex;->re:D

    add-double/2addr v2, v4

    iput-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    return-object p0
.end method

.method public final log()Lorg/javia/arity/Complex;
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->abs()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final mod(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;
    .locals 9

    const-wide/16 v6, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v4, v2, v6

    if-nez v4, :cond_0

    iget-wide v4, p1, Lorg/javia/arity/Complex;->im:D

    cmpl-double v4, v4, v6

    if-nez v4, :cond_0

    iget-wide v2, p1, Lorg/javia/arity/Complex;->re:D

    rem-double/2addr v0, v2

    invoke-virtual {p0, v0, v1, v6, v7}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lorg/javia/arity/Complex;->div(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;

    move-result-object v4

    iget-wide v5, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v5, v6}, Ljava/lang/Math;->rint(D)D

    move-result-wide v5

    iget-wide v7, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v7, v8}, Ljava/lang/Math;->rint(D)D

    move-result-wide v7

    invoke-virtual {v4, v5, v6, v7, v8}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v4

    invoke-virtual {v4, p1}, Lorg/javia/arity/Complex;->mul(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;

    move-result-object v4

    iget-wide v5, p0, Lorg/javia/arity/Complex;->re:D

    sub-double/2addr v0, v5

    iget-wide v5, p0, Lorg/javia/arity/Complex;->im:D

    sub-double/2addr v2, v5

    invoke-virtual {v4, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method mul(D)Lorg/javia/arity/Complex;
    .locals 2

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    return-object p0
.end method

.method public final mul(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;
    .locals 14

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v6, p1, Lorg/javia/arity/Complex;->re:D

    iget-wide v4, p1, Lorg/javia/arity/Complex;->im:D

    const-wide/16 v8, 0x0

    cmpl-double v8, v0, v8

    if-nez v8, :cond_1

    const-wide/16 v8, 0x0

    cmpl-double v8, v4, v8

    if-nez v8, :cond_1

    mul-double v0, v2, v6

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object p0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    mul-double v8, v2, v6

    mul-double v10, v0, v4

    sub-double/2addr v8, v10

    mul-double v10, v2, v4

    mul-double v12, v0, v6

    add-double/2addr v10, v12

    invoke-virtual {p0, v8, v9, v10, v11}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v12

    invoke-virtual {v12}, Lorg/javia/arity/Complex;->isNaN()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v12

    invoke-virtual {v12}, Lorg/javia/arity/Complex;->isInfinite()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-direct {p0}, Lorg/javia/arity/Complex;->normalizeInfinity()Lorg/javia/arity/Complex;

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    :cond_2
    invoke-virtual {p1}, Lorg/javia/arity/Complex;->isInfinite()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-virtual {p0, v6, v7, v4, v5}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v4

    invoke-direct {v4}, Lorg/javia/arity/Complex;->normalizeInfinity()Lorg/javia/arity/Complex;

    iget-wide v6, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    :cond_3
    const-wide/16 v12, 0x0

    cmpl-double v12, v0, v12

    if-nez v12, :cond_6

    const-wide/16 v0, 0x0

    cmpl-double v0, v4, v0

    if-nez v0, :cond_4

    mul-double v0, v2, v6

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object p0

    goto :goto_0

    :cond_4
    const-wide/16 v0, 0x0

    cmpl-double v0, v6, v0

    if-nez v0, :cond_5

    const-wide/16 v0, 0x0

    mul-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object p0

    goto :goto_0

    :cond_5
    mul-double v0, v2, v6

    mul-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object p0

    goto :goto_0

    :cond_6
    const-wide/16 v12, 0x0

    cmpl-double v12, v2, v12

    if-nez v12, :cond_9

    const-wide/16 v2, 0x0

    cmpl-double v2, v6, v2

    if-nez v2, :cond_7

    neg-double v0, v0

    mul-double/2addr v0, v4

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object p0

    goto :goto_0

    :cond_7
    const-wide/16 v2, 0x0

    cmpl-double v2, v4, v2

    if-nez v2, :cond_8

    const-wide/16 v2, 0x0

    mul-double/2addr v0, v6

    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object p0

    goto/16 :goto_0

    :cond_8
    neg-double v2, v0

    mul-double/2addr v2, v4

    mul-double/2addr v0, v6

    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object p0

    goto/16 :goto_0

    :cond_9
    const-wide/16 v12, 0x0

    cmpl-double v12, v4, v12

    if-nez v12, :cond_a

    mul-double/2addr v2, v6

    mul-double/2addr v0, v6

    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object p0

    goto/16 :goto_0

    :cond_a
    const-wide/16 v12, 0x0

    cmpl-double v6, v6, v12

    if-nez v6, :cond_b

    neg-double v0, v0

    mul-double/2addr v0, v4

    mul-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object p0

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p0, v8, v9, v10, v11}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object p0

    goto/16 :goto_0
.end method

.method public final negate()Lorg/javia/arity/Complex;
    .locals 4

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    neg-double v0, v0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    neg-double v2, v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    return-object v0
.end method

.method public final permutations(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;
    .locals 10

    const-wide/16 v4, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    iget-wide v0, p1, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p1, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1, v2, v3}, Lorg/javia/arity/MoreMath;->permutations(DD)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v4, v5}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->lgamma()Lorg/javia/arity/Complex;

    iget-wide v4, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v6, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v8, p1, Lorg/javia/arity/Complex;->re:D

    sub-double/2addr v0, v8

    iget-wide v8, p1, Lorg/javia/arity/Complex;->im:D

    sub-double/2addr v2, v8

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->lgamma()Lorg/javia/arity/Complex;

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    sub-double v0, v4, v0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    sub-double v2, v6, v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->exp()Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final pow(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;
    .locals 9

    const-wide/high16 v4, 0x4000000000000000L

    const-wide/16 v6, 0x0

    iget-wide v0, p1, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v6

    if-nez v0, :cond_4

    iget-wide v0, p1, Lorg/javia/arity/Complex;->re:D

    cmpl-double v0, v0, v6

    if-nez v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L

    invoke-virtual {p0, v0, v1, v6, v7}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v6

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p1, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    cmpl-double v2, v0, v0

    if-nez v2, :cond_1

    invoke-virtual {p0, v0, v1, v6, v7}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-wide v0, p1, Lorg/javia/arity/Complex;->re:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->square()Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-wide v0, p1, Lorg/javia/arity/Complex;->re:D

    const-wide/high16 v2, 0x3fe0000000000000L

    cmpl-double v0, v0, v2

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->sqrt()Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lorg/javia/arity/Complex;->abs2()D

    move-result-wide v0

    iget-wide v2, p1, Lorg/javia/arity/Complex;->re:D

    div-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->arg()D

    move-result-wide v2

    iget-wide v4, p1, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Lorg/javia/arity/MoreMath;->cos(D)D

    move-result-wide v4

    mul-double/2addr v4, v0

    invoke-static {v2, v3}, Lorg/javia/arity/MoreMath;->sin(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-virtual {p0, v4, v5, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v6

    if-nez v0, :cond_5

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    cmpl-double v0, v0, v6

    if-lez v0, :cond_5

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p1, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iget-wide v2, p1, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-virtual {p0, v6, v7, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v2

    invoke-virtual {v2}, Lorg/javia/arity/Complex;->exp()Lorg/javia/arity/Complex;

    move-result-object v2

    iget-wide v3, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v3, v0

    iget-wide v5, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v0, v5

    invoke-virtual {v2, v3, v4, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lorg/javia/arity/Complex;->log()Lorg/javia/arity/Complex;

    move-result-object v0

    iget-wide v1, p1, Lorg/javia/arity/Complex;->re:D

    iget-wide v3, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v1, v3

    iget-wide v3, p1, Lorg/javia/arity/Complex;->im:D

    iget-wide v5, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v3, v5

    sub-double/2addr v1, v3

    iget-wide v3, p1, Lorg/javia/arity/Complex;->re:D

    iget-wide v5, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v3, v5

    iget-wide v5, p1, Lorg/javia/arity/Complex;->im:D

    iget-wide v7, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v5, v7

    add-double/2addr v3, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->exp()Lorg/javia/arity/Complex;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public set(DD)Lorg/javia/arity/Complex;
    .locals 0

    iput-wide p1, p0, Lorg/javia/arity/Complex;->re:D

    iput-wide p3, p0, Lorg/javia/arity/Complex;->im:D

    return-object p0
.end method

.method public set(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;
    .locals 2

    iget-wide v0, p1, Lorg/javia/arity/Complex;->re:D

    iput-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v0, p1, Lorg/javia/arity/Complex;->im:D

    iput-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    return-object p0
.end method

.method public final sin()Lorg/javia/arity/Complex;
    .locals 6

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->sin(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->sin(D)D

    move-result-wide v0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v2, v3}, Ljava/lang/Math;->cosh(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v2, v3}, Lorg/javia/arity/MoreMath;->cos(D)D

    move-result-wide v2

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v4, v5}, Ljava/lang/Math;->sinh(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final sinh()Lorg/javia/arity/Complex;
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->sinh(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lorg/javia/arity/Complex;->swap()Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->sin()Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-direct {v0}, Lorg/javia/arity/Complex;->swap()Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final sqrt()Lorg/javia/arity/Complex;
    .locals 8

    const-wide/16 v6, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v6

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    cmpg-double v0, v0, v6

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v6, v7}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    :goto_0
    return-object p0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    neg-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    invoke-virtual {p0, v6, v7, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/javia/arity/Complex;->abs()D

    move-result-wide v2

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    cmpl-double v2, v2, v6

    if-ltz v2, :cond_2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    add-double v4, v0, v0

    div-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    goto :goto_0

    :cond_2
    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    add-double v4, v0, v0

    div-double/2addr v2, v4

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_3

    :goto_1
    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    goto :goto_0

    :cond_3
    neg-double v0, v0

    goto :goto_1
.end method

.method public final square()Lorg/javia/arity/Complex;
    .locals 6

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L

    iget-wide v4, p0, Lorg/javia/arity/Complex;->re:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    mul-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    return-object v0
.end method

.method public final sub(Lorg/javia/arity/Complex;)Lorg/javia/arity/Complex;
    .locals 6

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->ulp(D)D

    move-result-wide v0

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v4, p1, Lorg/javia/arity/Complex;->re:D

    sub-double/2addr v2, v4

    iput-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p1, Lorg/javia/arity/Complex;->im:D

    sub-double/2addr v2, v4

    iput-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4090000000000000L

    mul-double/2addr v0, v4

    cmpg-double v0, v2, v0

    if-gez v0, :cond_0

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    :cond_0
    return-object p0
.end method

.method public final tan()Lorg/javia/arity/Complex;
    .locals 8

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->tan(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    iget-wide v2, p0, Lorg/javia/arity/Complex;->re:D

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/javia/arity/Complex;->im:D

    iget-wide v4, p0, Lorg/javia/arity/Complex;->im:D

    add-double/2addr v2, v4

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->cos(D)D

    move-result-wide v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cosh(D)D

    move-result-wide v6

    add-double/2addr v4, v6

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->sin(D)D

    move-result-wide v0

    div-double/2addr v0, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sinh(D)D

    move-result-wide v2

    div-double/2addr v2, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public final tanh()Lorg/javia/arity/Complex;
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/javia/arity/Complex;->re:D

    invoke-static {v0, v1}, Ljava/lang/Math;->tanh(D)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/javia/arity/Complex;->set(DD)Lorg/javia/arity/Complex;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lorg/javia/arity/Complex;->swap()Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-virtual {v0}, Lorg/javia/arity/Complex;->tan()Lorg/javia/arity/Complex;

    move-result-object v0

    invoke-direct {v0}, Lorg/javia/arity/Complex;->swap()Lorg/javia/arity/Complex;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    iget-wide v0, p0, Lorg/javia/arity/Complex;->im:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lorg/javia/arity/Complex;->re:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lorg/javia/arity/Complex;->re:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lorg/javia/arity/Complex;->im:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
