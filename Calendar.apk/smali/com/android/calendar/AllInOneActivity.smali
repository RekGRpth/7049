.class public Lcom/android/calendar/AllInOneActivity;
.super Landroid/app/Activity;
.source "AllInOneActivity.java"

# interfaces
.implements Landroid/app/ActionBar$OnNavigationListener;
.implements Landroid/app/ActionBar$TabListener;
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;
.implements Landroid/widget/SearchView$OnSuggestionListener;
.implements Lcom/android/calendar/CalendarController$EventHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/AllInOneActivity$QueryHandler;
    }
.end annotation


# static fields
.field private static final BUNDLE_KEY_CHECK_ACCOUNTS:Ljava/lang/String; = "key_check_for_accounts"

.field private static final BUNDLE_KEY_EVENT_ID:Ljava/lang/String; = "key_event_id"

.field private static final BUNDLE_KEY_RESTORE_TIME:Ljava/lang/String; = "key_restore_time"

.field private static final BUNDLE_KEY_RESTORE_VIEW:Ljava/lang/String; = "key_restore_view"

.field private static final BUTTON_AGENDA_INDEX:I = 0x3

.field private static final BUTTON_DAY_INDEX:I = 0x0

.field private static final BUTTON_MONTH_INDEX:I = 0x2

.field private static final BUTTON_WEEK_INDEX:I = 0x1

.field private static final DEBUG:Z = false

.field private static final EVENT_INFO_FRAGMENT_TAG:Ljava/lang/String; = "EventInfoFragment"

.field private static final HANDLER_KEY:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AllInOneActivity"

.field private static mIsClearEventsCompleted:Z

.field private static mIsMultipane:Z

.field private static mIsTabletConfig:Z

.field private static mScale:F

.field private static mShowAgendaWithMonth:Z

.field private static mShowEventDetailsWithAgenda:Z


# instance fields
.field private mActionBar:Landroid/app/ActionBar;

.field private mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

.field private mAgendaTab:Landroid/app/ActionBar$Tab;

.field private mBackToPreviousView:Z

.field mCalIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mCalendarControlsAnimationTime:I

.field private mCalendarsList:Landroid/view/View;

.field private mCheckForAccounts:Z

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mController:Lcom/android/calendar/CalendarController;

.field private mControlsAnimateHeight:I

.field private mControlsAnimateWidth:I

.field private mControlsMenu:Landroid/view/MenuItem;

.field private mControlsParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mCurrentView:I

.field private mDateRange:Landroid/widget/TextView;

.field mDayOfMonthIcon:Lcom/android/calendar/DayOfMonthDrawable;

.field private mDayTab:Landroid/app/ActionBar$Tab;

.field private mHandler:Lcom/android/calendar/AllInOneActivity$QueryHandler;

.field private mHideControls:Z

.field private mHideString:Ljava/lang/String;

.field private mHomeTime:Landroid/widget/TextView;

.field private final mHomeTimeUpdater:Ljava/lang/Runnable;

.field private mIntentAllDay:Z

.field private mIntentAttendeeResponse:I

.field private mIntentEventEndMillis:J

.field private mIntentEventStartMillis:J

.field private mMiniMonth:Landroid/view/View;

.field private mMiniMonthContainer:Landroid/view/View;

.field private mMonthTab:Landroid/app/ActionBar$Tab;

.field private final mObserver:Landroid/database/ContentObserver;

.field private mOnSaveInstanceStateCalled:Z

.field private mOptionsMenu:Landroid/view/Menu;

.field private mOptionsMenuExt:Lcom/mediatek/calendar/extension/IOptionsMenuExt;

.field mOrientation:I

.field private mPaused:Z

.field private mPreviousView:I

.field private mSearchMenu:Landroid/view/MenuItem;

.field private mSearchView:Landroid/widget/SearchView;

.field private mSecondaryPane:Landroid/view/View;

.field private mShowCalendarControls:Z

.field private mShowEventInfoFullScreen:Z

.field private mShowEventInfoFullScreenAgenda:Z

.field private mShowSideViews:Z

.field private mShowString:Ljava/lang/String;

.field private mShowWeekNum:Z

.field private final mSlideAnimationDoneListener:Landroid/animation/Animator$AnimatorListener;

.field private final mTimeChangesUpdater:Ljava/lang/Runnable;

.field private mTimeZone:Ljava/lang/String;

.field private mUpdateOnResume:Z

.field private mVerticalControlsParams:Landroid/widget/LinearLayout$LayoutParams;

.field private mViewEventId:J

.field private mWeekNum:I

.field private mWeekTab:Landroid/app/ActionBar$Tab;

.field private mWeekTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/AllInOneActivity;->mScale:F

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/AllInOneActivity;->mIsClearEventsCompleted:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mOnSaveInstanceStateCalled:Z

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mBackToPreviousView:Z

    iput-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->mPaused:Z

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mUpdateOnResume:Z

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    iput-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->mShowSideViews:Z

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mShowWeekNum:Z

    iput-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->mViewEventId:J

    iput-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->mIntentEventStartMillis:J

    iput-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->mIntentEventEndMillis:J

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->mIntentAttendeeResponse:I

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mIntentAllDay:Z

    iput-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->mCheckForAccounts:Z

    new-instance v0, Lcom/android/calendar/AllInOneActivity$1;

    invoke-direct {v0, p0}, Lcom/android/calendar/AllInOneActivity$1;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mSlideAnimationDoneListener:Landroid/animation/Animator$AnimatorListener;

    new-instance v0, Lcom/android/calendar/AllInOneActivity$2;

    invoke-direct {v0, p0}, Lcom/android/calendar/AllInOneActivity$2;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mHomeTimeUpdater:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/calendar/AllInOneActivity$3;

    invoke-direct {v0, p0}, Lcom/android/calendar/AllInOneActivity$3;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mTimeChangesUpdater:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/calendar/AllInOneActivity$4;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/AllInOneActivity$4;-><init>(Lcom/android/calendar/AllInOneActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/calendar/AllInOneActivity;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/AllInOneActivity;

    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mShowSideViews:Z

    return v0
.end method

.method static synthetic access$100(Lcom/android/calendar/AllInOneActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/calendar/AllInOneActivity;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mMiniMonth:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/calendar/AllInOneActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/calendar/AllInOneActivity;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mCalendarsList:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/calendar/AllInOneActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/calendar/AllInOneActivity;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mMiniMonthContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/calendar/AllInOneActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/AllInOneActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/AllInOneActivity;->mCheckForAccounts:Z

    return p1
.end method

.method static synthetic access$500(Lcom/android/calendar/AllInOneActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/calendar/AllInOneActivity;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/calendar/AllInOneActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/calendar/AllInOneActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/android/calendar/AllInOneActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/calendar/AllInOneActivity;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mHomeTimeUpdater:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/calendar/AllInOneActivity;J)V
    .locals 0
    .param p0    # Lcom/android/calendar/AllInOneActivity;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/calendar/AllInOneActivity;->updateSecondaryTitleFields(J)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/AllInOneActivity$QueryHandler;
    .locals 1
    .param p0    # Lcom/android/calendar/AllInOneActivity;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mHandler:Lcom/android/calendar/AllInOneActivity$QueryHandler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/calendar/AllInOneActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/calendar/AllInOneActivity;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mTimeChangesUpdater:Ljava/lang/Runnable;

    return-object v0
.end method

.method private clearOptionsMenu()V
    .locals 3

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mOptionsMenu:Landroid/view/Menu;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f100087

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private configureActionBar(I)V
    .locals 2
    .param p1    # I

    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->mIsTabletConfig:Z

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/AllInOneActivity;->createButtonsSpinner(IZ)V

    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->mIsMultipane:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto :goto_0
.end method

.method private createButtonsSpinner(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v3, Lcom/android/calendar/CalendarViewAdapter;

    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {v3, p0, p1, v0}, Lcom/android/calendar/CalendarViewAdapter;-><init>(Landroid/content/Context;IZ)V

    iput-object v3, p0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    invoke-virtual {v0, v3, p0}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private createTabs()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_0

    const-string v0, "AllInOneActivity"

    const-string v1, "ActionBar is null."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mDayTab:Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mDayTab:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0c0048

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mDayTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mDayTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mWeekTab:Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mWeekTab:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0c0049

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mWeekTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mWeekTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mMonthTab:Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mMonthTab:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0c004a

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mMonthTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mMonthTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mAgendaTab:Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mAgendaTab:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0c0047

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mAgendaTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mAgendaTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    goto/16 :goto_0
.end method

.method private initFragments(JILandroid/os/Bundle;)V
    .locals 25
    .param p1    # J
    .param p3    # I
    .param p4    # Landroid/os/Bundle;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mShowCalendarControls:Z

    if-eqz v3, :cond_0

    new-instance v23, Lcom/android/calendar/month/MonthByWeekFragment;

    const/4 v3, 0x1

    move-object/from16 v0, v23

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/android/calendar/month/MonthByWeekFragment;-><init>(JZ)V

    const v3, 0x7f100023

    move-object/from16 v0, v23

    invoke-virtual {v4, v3, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const v5, 0x7f100023

    check-cast v23, Lcom/android/calendar/CalendarController$EventHandler;

    move-object/from16 v0, v23

    invoke-virtual {v3, v5, v0}, Lcom/android/calendar/CalendarController;->registerEventHandler(ILcom/android/calendar/CalendarController$EventHandler;)V

    new-instance v24, Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsFragment;

    invoke-direct/range {v24 .. v24}, Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsFragment;-><init>()V

    const v3, 0x7f100003

    move-object/from16 v0, v24

    invoke-virtual {v4, v3, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const v5, 0x7f100003

    check-cast v24, Lcom/android/calendar/CalendarController$EventHandler;

    move-object/from16 v0, v24

    invoke-virtual {v3, v5, v0}, Lcom/android/calendar/CalendarController;->registerEventHandler(ILcom/android/calendar/CalendarController$EventHandler;)V

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mShowCalendarControls:Z

    if-eqz v3, :cond_1

    const/4 v3, 0x5

    move/from16 v0, p3

    if-ne v0, v3, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mMiniMonth:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mCalendarsList:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    const/16 v21, 0x0

    const/4 v3, 0x5

    move/from16 v0, p3

    if-ne v0, v3, :cond_8

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/GeneralPreferences;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v5, "preferred_startView"

    const/4 v6, 0x3

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/calendar/AllInOneActivity;->mPreviousView:I

    const-wide/16 v19, -0x1

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v16

    if-eqz v16, :cond_7

    :try_start_0
    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v19

    :cond_3
    :goto_0
    const-string v3, "beginTime"

    const-wide/16 v5, -0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v14

    const-string v3, "endTime"

    const-wide/16 v5, -0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v17

    new-instance v21, Lcom/android/calendar/CalendarController$EventInfo;

    invoke-direct/range {v21 .. v21}, Lcom/android/calendar/CalendarController$EventInfo;-><init>()V

    const-wide/16 v5, -0x1

    cmp-long v3, v17, v5

    if-eqz v3, :cond_4

    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    move-object/from16 v0, v21

    iput-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    :cond_4
    const-wide/16 v5, -0x1

    cmp-long v3, v14, v5

    if-eqz v3, :cond_5

    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    move-object/from16 v0, v21

    iput-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    invoke-virtual {v3, v14, v15}, Landroid/text/format/Time;->set(J)V

    :cond_5
    move-wide/from16 v0, v19

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/android/calendar/CalendarController;->setViewType(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    move-wide/from16 v0, v19

    invoke-virtual {v3, v0, v1}, Lcom/android/calendar/CalendarController;->setEventId(J)V

    :goto_1
    const v5, 0x7f100020

    const/4 v9, 0x1

    move-object/from16 v3, p0

    move/from16 v6, p3

    move-wide/from16 v7, p1

    invoke-direct/range {v3 .. v9}, Lcom/android/calendar/AllInOneActivity;->setMainPane(Landroid/app/FragmentTransaction;IIJZ)V

    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    new-instance v9, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-direct {v9, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v9, v0, v1}, Landroid/text/format/Time;->set(J)V

    const/4 v3, 0x5

    move/from16 v0, p3

    if-eq v0, v3, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v7, 0x20

    const/4 v10, 0x0

    const-wide/16 v11, -0x1

    move-object/from16 v6, p0

    move/from16 v13, p3

    invoke-virtual/range {v5 .. v13}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    :cond_6
    return-void

    :cond_7
    if-eqz p4, :cond_3

    const-string v3, "key_event_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "key_event_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v19

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    move/from16 v0, p3

    if-eq v3, v0, :cond_9

    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/AllInOneActivity;->mPreviousView:I

    goto :goto_1

    :cond_9
    const-string v3, "AllInOneActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "don\'t modify mPreviousView\'s value.mCurrentView:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",viewType:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",mPreviousView:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/AllInOneActivity;->mPreviousView:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/mediatek/calendar/LogUtil;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_0
    move-exception v3

    goto/16 :goto_0
.end method

.method private parseViewAction(Landroid/content/Intent;)J
    .locals 8
    .param p1    # Landroid/content/Intent;

    const/4 v6, 0x0

    const-wide/16 v2, -0x1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->isHierarchical()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "events"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/AllInOneActivity;->mViewEventId:J

    iget-wide v4, p0, Lcom/android/calendar/AllInOneActivity;->mViewEventId:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    const-string v4, "beginTime"

    const-wide/16 v5, 0x0

    invoke-virtual {p1, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/AllInOneActivity;->mIntentEventStartMillis:J

    const-string v4, "endTime"

    const-wide/16 v5, 0x0

    invoke-virtual {p1, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/AllInOneActivity;->mIntentEventEndMillis:J

    const-string v4, "attendeeStatus"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/android/calendar/AllInOneActivity;->mIntentAttendeeResponse:I

    const-string v4, "allDay"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/calendar/AllInOneActivity;->mIntentAllDay:Z

    iget-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->mIntentEventStartMillis:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-wide v2

    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public static setClearEventsCompletedStatus(Z)V
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/android/calendar/AllInOneActivity;->mIsClearEventsCompleted:Z

    return-void
.end method

.method private setMainPane(Landroid/app/FragmentTransaction;IIJZ)V
    .locals 10
    .param p1    # Landroid/app/FragmentTransaction;
    .param p2    # I
    .param p3    # I
    .param p4    # J
    .param p6    # Z

    iget-boolean v7, p0, Lcom/android/calendar/AllInOneActivity;->mOnSaveInstanceStateCalled:Z

    if-eqz v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p6, :cond_2

    iget v7, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    if-eq v7, p3, :cond_0

    :cond_2
    const/4 v7, 0x4

    if-eq p3, v7, :cond_6

    iget v7, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v8, 0x4

    if-eq v7, v8, :cond_6

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    iget v7, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_3

    invoke-virtual {v4, p2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v5

    instance-of v7, v5, Lcom/android/calendar/agenda/AgendaFragment;

    if-eqz v7, :cond_3

    check-cast v5, Lcom/android/calendar/agenda/AgendaFragment;

    invoke-virtual {v5, v4}, Lcom/android/calendar/agenda/AgendaFragment;->removeFragments(Landroid/app/FragmentManager;)V

    :cond_3
    iget v7, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    if-eq p3, v7, :cond_5

    iget v7, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v8, 0x5

    if-eq v7, v8, :cond_4

    iget v7, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    if-lez v7, :cond_4

    iget v7, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    iput v7, p0, Lcom/android/calendar/AllInOneActivity;->mPreviousView:I

    :cond_4
    iput p3, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    :cond_5
    const/4 v3, 0x0

    const/4 v6, 0x0

    packed-switch p3, :pswitch_data_0

    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Must be Agenda, Day, Week, or Month ViewType, not "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_0
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v7}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v7

    iget-object v8, p0, Lcom/android/calendar/AllInOneActivity;->mAgendaTab:Landroid/app/ActionBar$Tab;

    if-eq v7, v8, :cond_7

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v8, p0, Lcom/android/calendar/AllInOneActivity;->mAgendaTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v7, v8}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    :cond_7
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    :cond_8
    new-instance v3, Lcom/android/calendar/agenda/AgendaFragment;

    const/4 v7, 0x0

    invoke-direct {v3, p4, p5, v7}, Lcom/android/calendar/agenda/AgendaFragment;-><init>(JZ)V

    :cond_9
    :goto_2
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    invoke-virtual {v7, p3}, Lcom/android/calendar/CalendarViewAdapter;->setMainView(I)V

    sget-boolean v7, Lcom/android/calendar/AllInOneActivity;->mIsTabletConfig:Z

    if-nez v7, :cond_a

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    invoke-virtual {v7, p4, p5}, Lcom/android/calendar/CalendarViewAdapter;->setTime(J)V

    :cond_a
    sget-boolean v7, Lcom/android/calendar/AllInOneActivity;->mIsTabletConfig:Z

    if-nez v7, :cond_16

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mDateRange:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    const/4 v7, 0x1

    if-eq p3, v7, :cond_b

    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->clearOptionsMenu()V

    :cond_b
    const/4 v0, 0x0

    if-nez p1, :cond_c

    const/4 v0, 0x1

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object p1

    :cond_c
    if-eqz v1, :cond_d

    const/16 v7, 0x1003

    invoke-virtual {p1, v7}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    :cond_d
    invoke-virtual {p1, p2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    sget-boolean v7, Lcom/android/calendar/AllInOneActivity;->mShowAgendaWithMonth:Z

    if-eqz v7, :cond_e

    if-eqz v6, :cond_18

    const v7, 0x7f100021

    invoke-virtual {p1, v7, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mSecondaryPane:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_e
    :goto_4
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    check-cast v3, Lcom/android/calendar/CalendarController$EventHandler;

    invoke-virtual {v7, p2, v3}, Lcom/android/calendar/CalendarController;->registerEventHandler(ILcom/android/calendar/CalendarController$EventHandler;)V

    if-eqz v6, :cond_f

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    check-cast v6, Lcom/android/calendar/CalendarController$EventHandler;

    invoke-virtual {v7, p2, v6}, Lcom/android/calendar/CalendarController;->registerEventHandler(ILcom/android/calendar/CalendarController$EventHandler;)V

    :cond_f
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    :pswitch_1
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v7, :cond_10

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v7}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v7

    iget-object v8, p0, Lcom/android/calendar/AllInOneActivity;->mDayTab:Landroid/app/ActionBar$Tab;

    if-eq v7, v8, :cond_10

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v8, p0, Lcom/android/calendar/AllInOneActivity;->mDayTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v7, v8}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    :cond_10
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    if-eqz v7, :cond_11

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    :cond_11
    new-instance v3, Lcom/android/calendar/DayFragment;

    const/4 v7, 0x1

    invoke-direct {v3, p4, p5, v7}, Lcom/android/calendar/DayFragment;-><init>(JI)V

    goto/16 :goto_2

    :pswitch_2
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v7, :cond_12

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v7}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v7

    iget-object v8, p0, Lcom/android/calendar/AllInOneActivity;->mWeekTab:Landroid/app/ActionBar$Tab;

    if-eq v7, v8, :cond_12

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v8, p0, Lcom/android/calendar/AllInOneActivity;->mWeekTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v7, v8}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    :cond_12
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    if-eqz v7, :cond_13

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    :cond_13
    new-instance v3, Lcom/android/calendar/DayFragment;

    const/4 v7, 0x7

    invoke-direct {v3, p4, p5, v7}, Lcom/android/calendar/DayFragment;-><init>(JI)V

    goto/16 :goto_2

    :pswitch_3
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v7, :cond_14

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v7}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v7

    iget-object v8, p0, Lcom/android/calendar/AllInOneActivity;->mMonthTab:Landroid/app/ActionBar$Tab;

    if-eq v7, v8, :cond_14

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v8, p0, Lcom/android/calendar/AllInOneActivity;->mMonthTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v7, v8}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    :cond_14
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    if-eqz v7, :cond_15

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    :cond_15
    new-instance v3, Lcom/android/calendar/month/MonthByWeekFragment;

    const/4 v7, 0x0

    invoke-direct {v3, p4, p5, v7}, Lcom/android/calendar/month/MonthByWeekFragment;-><init>(JZ)V

    sget-boolean v7, Lcom/android/calendar/AllInOneActivity;->mShowAgendaWithMonth:Z

    if-eqz v7, :cond_9

    new-instance v6, Lcom/android/calendar/agenda/AgendaFragment;

    const/4 v7, 0x0

    invoke-direct {v6, p4, p5, v7}, Lcom/android/calendar/agenda/AgendaFragment;-><init>(JZ)V

    goto/16 :goto_2

    :cond_16
    const/4 v7, 0x1

    if-eq p3, v7, :cond_17

    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mDateRange:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    :cond_17
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mDateRange:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    :cond_18
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mSecondaryPane:Landroid/view/View;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    const v7, 0x7f100021

    invoke-virtual {v4, v7}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_19

    invoke-virtual {p1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_19
    iget-object v7, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const v8, 0x7f100021

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/calendar/CalendarController;->deregisterEventHandler(Ljava/lang/Integer;)V

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setTitleInActionBar(Lcom/android/calendar/CalendarController$EventInfo;)V
    .locals 13
    .param p1    # Lcom/android/calendar/CalendarController$EventInfo;

    const/16 v12, 0x8

    const/4 v5, 0x0

    iget-wide v8, p1, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v10, 0x400

    cmp-long v0, v8, v10

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    :goto_1
    iget-wide v8, p1, Lcom/android/calendar/CalendarController$EventInfo;->extraLong:J

    long-to-int v5, v8

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mDateRange:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mDateRange:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    :cond_2
    invoke-direct {p0, v1, v2}, Lcom/android/calendar/AllInOneActivity;->updateSecondaryTitleFields(J)V

    invoke-static {v7, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mDateRange:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mShowWeekNum:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mWeekTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mWeekTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    goto :goto_0

    :cond_3
    move-wide v3, v1

    goto :goto_1
.end method

.method private updateSecondaryTitleFields(J)V
    .locals 21
    .param p1    # J

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/Utils;->getShowWeekNumber(Landroid/content/Context;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mShowWeekNum:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTimeUpdater:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    const-wide/16 v6, -0x1

    cmp-long v3, p1, v6

    if-eqz v3, :cond_0

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/Utils;->getWeekNumberFromTime(JLandroid/content/Context;)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/calendar/AllInOneActivity;->mWeekNum:I

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mShowWeekNum:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v6, 0x3

    if-ne v3, v6, :cond_5

    sget-boolean v3, Lcom/android/calendar/AllInOneActivity;->mIsTabletConfig:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mWeekTextView:Landroid/widget/TextView;

    if-eqz v3, :cond_5

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0d0004

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/calendar/AllInOneActivity;->mWeekNum:I

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/AllInOneActivity;->mWeekNum:I

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v17

    invoke-virtual {v3, v6, v7, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mWeekTextView:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mWeekTextView:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTime:Landroid/widget/TextView;

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v6, 0x2

    if-eq v3, v6, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v6, 0x3

    if-eq v3, v6, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_9

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    new-instance v12, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-direct {v12, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Landroid/text/format/Time;->setToNow()V

    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget v3, v12, Landroid/text/format/Time;->isDst:I

    if-eqz v3, :cond_8

    const/4 v10, 0x1

    :goto_1
    const/4 v8, 0x1

    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    or-int/lit16 v8, v8, 0x80

    :cond_3
    new-instance v17, Ljava/lang/StringBuilder;

    move-object/from16 v3, p0

    move-wide v6, v4

    invoke-static/range {v3 .. v8}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v10, v7, v0}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTime:Landroid/widget/TextView;

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTime:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTime:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTimeUpdater:Ljava/lang/Runnable;

    invoke-virtual {v3, v6}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTime:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTimeUpdater:Ljava/lang/Runnable;

    const-wide/32 v17, 0xea60

    const-wide/32 v19, 0xea60

    rem-long v19, v4, v19

    sub-long v17, v17, v19

    move-wide/from16 v0, v17

    invoke-virtual {v3, v6, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_4
    :goto_2
    return-void

    :cond_5
    const-wide/16 v6, -0x1

    cmp-long v3, p1, v6

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mWeekTextView:Landroid/widget/TextView;

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v6, 0x2

    if-ne v3, v6, :cond_6

    sget-boolean v3, Lcom/android/calendar/AllInOneActivity;->mIsTabletConfig:Z

    if-eqz v3, :cond_6

    new-instance v12, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-direct {v12, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v12, v0, v1}, Landroid/text/format/Time;->set(J)V

    iget-wide v6, v12, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v11

    invoke-virtual {v12}, Landroid/text/format/Time;->setToNow()V

    const/4 v3, 0x0

    invoke-virtual {v12, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-wide v0, v12, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    invoke-static {v6, v7, v0, v1}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v14

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    invoke-static {v11, v14, v0, v1, v2}, Lcom/android/calendar/Utils;->getDayOfWeekString(IIJLandroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mWeekTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mWeekTextView:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mWeekTextView:Landroid/widget/TextView;

    if-eqz v3, :cond_1

    sget-boolean v3, Lcom/android/calendar/AllInOneActivity;->mIsTabletConfig:Z

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v6, 0x2

    if-eq v3, v6, :cond_1

    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mWeekTextView:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_8
    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTime:Landroid/widget/TextView;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTime:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2
.end method


# virtual methods
.method public eventsChanged()V
    .locals 9

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v2, 0x80

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    return-void
.end method

.method public getSupportedEventTypes()J
    .locals 2

    const-wide/16 v0, 0x422

    return-wide v0
.end method

.method public handleEvent(Lcom/android/calendar/CalendarController$EventInfo;)V
    .locals 23
    .param p1    # Lcom/android/calendar/CalendarController$EventInfo;

    const-wide/16 v14, -0x1

    move-object/from16 v0, p1

    iget-wide v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v5, 0x20

    cmp-long v3, v3, v5

    if-nez v3, :cond_10

    move-object/from16 v0, p1

    iget-wide v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->extraLong:J

    const-wide/16 v5, 0x4

    and-long/2addr v3, v5

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mBackToPreviousView:Z

    :cond_0
    :goto_0
    const/4 v3, 0x0

    const v4, 0x7f100020

    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/calendar/CalendarController$EventInfo;->viewType:I

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/calendar/AllInOneActivity;->setMainPane(Landroid/app/FragmentTransaction;IIJZ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3}, Landroid/widget/SearchView;->clearFocus()V

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mShowCalendarControls:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mOrientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_8

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateWidth:I

    :goto_1
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->viewType:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_2

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->viewType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_9

    :cond_2
    const/16 v21, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    if-nez v21, :cond_a

    const/4 v3, 0x1

    :goto_3
    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    if-nez v21, :cond_b

    const/4 v3, 0x1

    :goto_4
    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_3
    if-nez v21, :cond_4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    if-eqz v3, :cond_d

    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mShowSideViews:Z

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    if-nez v3, :cond_c

    const-string v3, "controlsOffset"

    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x1

    aput v13, v4, v5

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mSlideAnimationDoneListener:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCalendarControlsAnimationTime:I

    int-to-long v3, v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Landroid/animation/ObjectAnimator;->setFrameDelay(J)V

    invoke-virtual/range {v22 .. v22}, Landroid/animation/ObjectAnimator;->start()V

    :cond_5
    :goto_5
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    if-eqz v3, :cond_f

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v14

    :goto_6
    sget-boolean v3, Lcom/android/calendar/AllInOneActivity;->mIsTabletConfig:Z

    if-nez v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    invoke-virtual {v3, v14, v15}, Lcom/android/calendar/CalendarViewAdapter;->setTime(J)V

    :cond_6
    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/android/calendar/AllInOneActivity;->updateSecondaryTitleFields(J)V

    return-void

    :cond_7
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->viewType:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v4}, Lcom/android/calendar/CalendarController;->getPreviousViewType()I

    move-result v4

    if-eq v3, v4, :cond_0

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->viewType:I

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mBackToPreviousView:Z

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateHeight:I

    goto/16 :goto_1

    :cond_9
    const/16 v21, 0x0

    goto/16 :goto_2

    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_4

    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mMiniMonth:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mCalendarsList:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mMiniMonthContainer:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    :cond_d
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mShowSideViews:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mMiniMonth:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mCalendarsList:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mMiniMonthContainer:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v3}, Lcom/android/calendar/CalendarController;->getPreviousViewType()I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v3}, Lcom/android/calendar/CalendarController;->getPreviousViewType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    :cond_e
    const-string v3, "controlsOffset"

    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v13, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v22

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCalendarControlsAnimationTime:I

    int-to-long v3, v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Landroid/animation/ObjectAnimator;->setFrameDelay(J)V

    invoke-virtual/range {v22 .. v22}, Landroid/animation/ObjectAnimator;->start()V

    goto/16 :goto_5

    :cond_f
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v14

    goto/16 :goto_6

    :cond_10
    move-object/from16 v0, p1

    iget-wide v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v5, 0x2

    cmp-long v3, v3, v5

    if-nez v3, :cond_1b

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_14

    sget-boolean v3, Lcom/android/calendar/AllInOneActivity;->mShowEventDetailsWithAgenda:Z

    if-eqz v3, :cond_14

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    if-eqz v3, :cond_13

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    if-eqz v3, :cond_13

    invoke-virtual/range {p1 .. p1}, Lcom/android/calendar/CalendarController$EventInfo;->isAllDay()Z

    move-result v3

    if-eqz v3, :cond_11

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/android/calendar/Utils;->convertAlldayUtcToLocal(Landroid/text/format/Time;JLjava/lang/String;)J

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/android/calendar/Utils;->convertAlldayUtcToLocal(Landroid/text/format/Time;JLjava/lang/String;)J

    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v4, 0x20

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    const/4 v10, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v10}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    :cond_12
    :goto_8
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v14

    goto/16 :goto_7

    :cond_13
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v4, 0x20

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    const/4 v10, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v10}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_8

    :cond_14
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    if-eqz v3, :cond_15

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v4, 0x20

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v10}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    :cond_15
    invoke-virtual/range {p1 .. p1}, Lcom/android/calendar/CalendarController$EventInfo;->getResponse()I

    move-result v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_16

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mShowEventInfoFullScreenAgenda:Z

    if-nez v3, :cond_18

    :cond_16
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_19

    :cond_17
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/AllInOneActivity;->mShowEventInfoFullScreen:Z

    if-eqz v3, :cond_19

    :cond_18
    new-instance v20, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v16

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-class v3, Lcom/android/calendar/EventInfoActivity;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v3, 0x20020000

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v3, "beginTime"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v3, "endTime"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v3, "attendeeStatus"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_8

    :cond_19
    new-instance v2, Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    const/4 v11, 0x1

    const/4 v12, 0x1

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v12}, Lcom/android/calendar/EventInfoFragment;-><init>(Landroid/content/Context;JJJIZI)V

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->x:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/calendar/CalendarController$EventInfo;->y:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->getHeight()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/calendar/EventInfoFragment;->setDialogParams(III)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v19

    const-string v3, "EventInfoFragment"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v17

    if-eqz v17, :cond_1a

    invoke-virtual/range {v17 .. v17}, Landroid/app/Fragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_1a

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_1a
    const-string v3, "EventInfoFragment"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual/range {v19 .. v19}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_8

    :cond_1b
    move-object/from16 v0, p1

    iget-wide v3, v0, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v5, 0x400

    cmp-long v3, v3, v5

    if-nez v3, :cond_6

    invoke-direct/range {p0 .. p1}, Lcom/android/calendar/AllInOneActivity;->setTitleInActionBar(Lcom/android/calendar/CalendarController$EventInfo;)V

    sget-boolean v3, Lcom/android/calendar/AllInOneActivity;->mIsTabletConfig:Z

    if-nez v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v4}, Lcom/android/calendar/CalendarController;->getTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/android/calendar/CalendarViewAdapter;->setTime(J)V

    goto/16 :goto_7
.end method

.method public handleSelectSyncedCalendarsClicked(Landroid/view/View;)V
    .locals 14
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v2, 0x40

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x2

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v12, v4

    move-object v13, v4

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 9

    const/4 v4, 0x0

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mBackToPreviousView:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    iget v8, p0, Lcom/android/calendar/AllInOneActivity;->mPreviousView:I

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    :goto_0
    return-void

    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 21
    .param p1    # Landroid/os/Bundle;

    const-string v5, "preferences_tardis_1"

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6}, Lcom/android/calendar/Utils;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x7f0e0019

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/view/ContextThemeWrapper;->setTheme(I)V

    :cond_0
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string v5, "key_check_for_accounts"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "key_check_for_accounts"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/calendar/AllInOneActivity;->mCheckForAccounts:Z

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/calendar/AllInOneActivity;->mCheckForAccounts:Z

    if-eqz v5, :cond_2

    const-string v5, "preferences_skip_setup"

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6}, Lcom/android/calendar/Utils;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_2

    new-instance v5, Lcom/android/calendar/AllInOneActivity$QueryHandler;

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v6}, Lcom/android/calendar/AllInOneActivity$QueryHandler;-><init>(Lcom/android/calendar/AllInOneActivity;Landroid/content/ContentResolver;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mHandler:Lcom/android/calendar/AllInOneActivity$QueryHandler;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mHandler:Lcom/android/calendar/AllInOneActivity$QueryHandler;

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "_id"

    aput-object v11, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v5 .. v12}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/CalendarController;->getInstance(Landroid/content/Context;)Lcom/android/calendar/CalendarController;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v18, -0x1

    const/16 v20, -0x1

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v14

    if-eqz p1, :cond_7

    const-string v5, "key_restore_time"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    const-string v5, "key_restore_view"

    const/4 v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v20

    :cond_3
    :goto_0
    const/4 v5, -0x1

    move/from16 v0, v20

    if-ne v0, v5, :cond_4

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/Utils;->getViewTypeFromIntentAndSharedPref(Landroid/app/Activity;)I

    move-result v20

    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTimeUpdater:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    new-instance v17, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v0, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v19}, Landroid/text/format/Time;->set(J)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v5, 0x7f0c0053

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mHideString:Ljava/lang/String;

    const v5, 0x7f0c0054

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mShowString:Ljava/lang/String;

    invoke-virtual/range {v16 .. v16}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/calendar/AllInOneActivity;->mOrientation:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/AllInOneActivity;->mOrientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_9

    const v5, 0x7f0a001d

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateWidth:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mControlsParams:Landroid/widget/RelativeLayout$LayoutParams;

    if-nez v5, :cond_5

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateWidth:I

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mControlsParams:Landroid/widget/RelativeLayout$LayoutParams;

    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mControlsParams:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    :goto_1
    const v5, 0x7f0a0020

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateHeight:I

    const-string v5, "preferences_show_controls"

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6}, Lcom/android/calendar/Utils;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_a

    const/4 v5, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    const v5, 0x7f090007

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v5

    sput-boolean v5, Lcom/android/calendar/AllInOneActivity;->mIsMultipane:Z

    const v5, 0x7f090006

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v5

    sput-boolean v5, Lcom/android/calendar/AllInOneActivity;->mIsTabletConfig:Z

    const v5, 0x7f090001

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v5

    sput-boolean v5, Lcom/android/calendar/AllInOneActivity;->mShowAgendaWithMonth:Z

    const/high16 v5, 0x7f090000

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v5

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/calendar/AllInOneActivity;->mShowCalendarControls:Z

    const v5, 0x7f090003

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v5

    sput-boolean v5, Lcom/android/calendar/AllInOneActivity;->mShowEventDetailsWithAgenda:Z

    const v5, 0x7f090004

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v5

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/calendar/AllInOneActivity;->mShowEventInfoFullScreenAgenda:Z

    const v5, 0x7f090005

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v5

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/calendar/AllInOneActivity;->mShowEventInfoFullScreen:Z

    const v5, 0x7f0b000a

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/calendar/AllInOneActivity;->mCalendarControlsAnimationTime:I

    sget-boolean v5, Lcom/android/calendar/AllInOneActivity;->mIsMultipane:Z

    invoke-static {v5}, Lcom/android/calendar/Utils;->setAllowWeekForDetailView(Z)V

    const v5, 0x7f04000d

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/Activity;->setContentView(I)V

    sget-boolean v5, Lcom/android/calendar/AllInOneActivity;->mIsTabletConfig:Z

    if-eqz v5, :cond_b

    const v5, 0x7f100025

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mDateRange:Landroid/widget/TextView;

    const v5, 0x7f100026

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mWeekTextView:Landroid/widget/TextView;

    :goto_3
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/calendar/AllInOneActivity;->configureActionBar(I)V

    const v5, 0x7f10001f

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mHomeTime:Landroid/widget/TextView;

    const v5, 0x7f100023

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mMiniMonth:Landroid/view/View;

    sget-boolean v5, Lcom/android/calendar/AllInOneActivity;->mIsTabletConfig:Z

    if-eqz v5, :cond_6

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/AllInOneActivity;->mOrientation:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mMiniMonth:Landroid/view/View;

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateHeight:I

    invoke-direct {v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_6
    const v5, 0x7f100003

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mCalendarsList:Landroid/view/View;

    const v5, 0x7f100022

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mMiniMonthContainer:Landroid/view/View;

    const v5, 0x7f100021

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mSecondaryPane:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v5, v6, v0}, Lcom/android/calendar/CalendarController;->registerFirstEventHandler(ILcom/android/calendar/CalendarController$EventHandler;)V

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move/from16 v3, v20

    move-object/from16 v4, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/calendar/AllInOneActivity;->initFragments(JILandroid/os/Bundle;)V

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/GeneralPreferences;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-interface {v15, v0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static/range {p0 .. p0}, Lcom/mediatek/calendar/extension/ExtensionFactory;->getAllInOneOptionMenuExt(Landroid/content/Context;)Lcom/mediatek/calendar/extension/IOptionsMenuExt;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mOptionsMenuExt:Lcom/mediatek/calendar/extension/IOptionsMenuExt;

    return-void

    :cond_7
    invoke-virtual {v14}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v13

    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/calendar/AllInOneActivity;->parseViewAction(Landroid/content/Intent;)J

    move-result-wide v18

    :cond_8
    const-wide/16 v5, -0x1

    cmp-long v5, v18, v5

    if-nez v5, :cond_3

    invoke-static {v14}, Lcom/android/calendar/Utils;->timeFromIntentInMillis(Landroid/content/Intent;)J

    move-result-wide v18

    goto/16 :goto_0

    :cond_9
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit8 v5, v5, 0x2d

    div-int/lit8 v5, v5, 0x64

    const v6, 0x7f0a001e

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateWidth:I

    const v6, 0x7f0a001f

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateWidth:I

    goto/16 :goto_1

    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_2

    :cond_b
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f04001c

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/AllInOneActivity;->mDateRange:Landroid/widget/TextView;

    goto/16 :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iput-object p1, p0, Lcom/android/calendar/AllInOneActivity;->mOptionsMenu:Landroid/view/Menu;

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const/high16 v2, 0x7f0f0000

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v1, 0x7f1000c3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mSearchMenu:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mSearchMenu:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SearchView;

    iput-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-static {v1, p0}, Lcom/android/calendar/Utils;->setUpSearchView(Landroid/widget/SearchView;Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1, p0}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    :cond_0
    const v1, 0x7f1000c7

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->mShowCalendarControls:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_1
    :goto_0
    const v1, 0x7f1000c0

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-static {v0, p0, v1}, Lcom/android/calendar/Utils;->setTodayIcon(Landroid/graphics/drawable/LayerDrawable;Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mOptionsMenuExt:Lcom/mediatek/calendar/extension/IOptionsMenuExt;

    invoke-interface {v1, p1}, Lcom/mediatek/calendar/extension/IOptionsMenuExt;->onCreateOptionsMenu(Landroid/view/Menu;)V

    return v4

    :cond_2
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getViewType()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getViewType()I

    move-result v1

    if-ne v1, v4, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mShowString:Ljava/lang/String;

    :goto_1
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mHideString:Ljava/lang/String;

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    invoke-static {p0}, Lcom/android/calendar/GeneralPreferences;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->deregisterAllEventHandlers()V

    invoke-static {p0}, Lcom/android/calendar/CalendarController;->removeInstance(Landroid/content/Context;)V

    return-void
.end method

.method public onNavigationItemSelected(IJ)Z
    .locals 10
    .param p1    # I
    .param p2    # J

    const/4 v8, 0x2

    const/4 v9, 0x1

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v0, "AllInOneActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ItemSelected event from unknown button: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "AllInOneActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CurrentView:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Button:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Day:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mDayTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Week:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mWeekTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Month:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mMonthTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Agenda:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mAgendaTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    if-eq v0, v8, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const/4 v8, 0x3

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const/4 v8, 0x4

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    if-eq v0, v9, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    move-object v1, p0

    move-object v5, v4

    move v8, v9

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Intent;

    const/4 v8, 0x0

    const-wide/16 v6, -0x1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "KEY_HOME"

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->parseViewAction(Landroid/content/Intent;)J

    move-result-wide v10

    cmp-long v0, v10, v6

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/android/calendar/Utils;->timeFromIntentInMillis(Landroid/content/Intent;)J

    move-result-wide v10

    :cond_0
    cmp-long v0, v10, v6

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->mViewEventId:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    if-eqz v0, :cond_1

    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10, v11}, Landroid/text/format/Time;->set(J)V

    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->normalize(Z)J

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v2, 0x20

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 21
    .param p1    # Landroid/view/MenuItem;

    const/16 v18, 0x0

    const/16 v19, 0x0

    const-wide/16 v15, 0x2

    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mOptionsMenuExt:Lcom/mediatek/calendar/extension/IOptionsMenuExt;

    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mediatek/calendar/extension/IOptionsMenuExt;->onOptionsItemSelected(I)Z

    move-result v1

    move-wide v11, v15

    move/from16 v10, v19

    move-object/from16 v5, v18

    :goto_0
    return v1

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->refreshCalendars()V

    const/4 v1, 0x1

    move-wide v11, v15

    move/from16 v10, v19

    move-object/from16 v5, v18

    goto :goto_0

    :pswitch_2
    const/4 v10, 0x0

    new-instance v5, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-direct {v5, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/text/format/Time;->setToNow()V

    const-wide/16 v1, 0x8

    or-long v11, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v3, 0x20

    const/4 v6, 0x0

    const-wide/16 v8, -0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v2, p0

    move-object v7, v5

    invoke-virtual/range {v1 .. v14}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    new-instance v18, Landroid/text/format/Time;

    invoke-direct/range {v18 .. v18}, Landroid/text/format/Time;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getViewType()I

    move-result v20

    const/4 v1, 0x4

    move/from16 v0, v20

    if-ne v0, v1, :cond_1

    invoke-virtual/range {v18 .. v18}, Landroid/text/format/Time;->setToNow()V

    :goto_1
    const/4 v1, 0x0

    move-object/from16 v0, v18

    iput v1, v0, Landroid/text/format/Time;->second:I

    move-object/from16 v0, v18

    iget v1, v0, Landroid/text/format/Time;->minute:I

    const/16 v2, 0x1e

    if-le v1, v2, :cond_2

    move-object/from16 v0, v18

    iget v1, v0, Landroid/text/format/Time;->hour:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, v18

    iput v1, v0, Landroid/text/format/Time;->hour:I

    const/4 v1, 0x0

    move-object/from16 v0, v18

    iput v1, v0, Landroid/text/format/Time;->minute:I

    :cond_0
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v3, 0x1

    const-wide/16 v5, -0x1

    const/4 v2, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v7

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-wide/16 v13, -0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v14}, Lcom/android/calendar/CalendarController;->sendEventRelatedEvent(Ljava/lang/Object;JJJJIIJ)V

    const/4 v1, 0x1

    move-wide v11, v15

    move/from16 v10, v19

    move-object/from16 v5, v18

    goto/16 :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getTime()J

    move-result-wide v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    goto :goto_1

    :cond_2
    move-object/from16 v0, v18

    iget v1, v0, Landroid/text/format/Time;->minute:I

    if-lez v1, :cond_0

    move-object/from16 v0, v18

    iget v1, v0, Landroid/text/format/Time;->minute:I

    const/16 v2, 0x1e

    if-ge v1, v2, :cond_0

    const/16 v1, 0x1e

    move-object/from16 v0, v18

    iput v1, v0, Landroid/text/format/Time;->minute:I

    goto :goto_2

    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v3, 0x800

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    const/4 v1, 0x1

    move-wide v11, v15

    move/from16 v10, v19

    move-object/from16 v5, v18

    goto/16 :goto_0

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v3, 0x40

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    const/4 v1, 0x1

    move-wide v11, v15

    move/from16 v10, v19

    move-object/from16 v5, v18

    goto/16 :goto_0

    :pswitch_6
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_3
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    const-string v2, "preferences_show_controls"

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_4
    move-object/from16 v0, p0

    invoke-static {v0, v2, v1}, Lcom/android/calendar/Utils;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mShowString:Ljava/lang/String;

    :goto_5
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    if-nez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mMiniMonth:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mCalendarsList:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mMiniMonthContainer:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    const-string v2, "controlsOffset"

    const/4 v1, 0x2

    new-array v3, v1, [I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    if-eqz v1, :cond_7

    const/4 v1, 0x0

    :goto_6
    aput v1, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateWidth:I

    :goto_7
    aput v1, v3, v4

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v17

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/AllInOneActivity;->mCalendarControlsAnimationTime:I

    int-to-long v1, v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Landroid/animation/ObjectAnimator;->setFrameDelay(J)V

    invoke-virtual/range {v17 .. v17}, Landroid/animation/ObjectAnimator;->start()V

    const/4 v1, 0x1

    move-wide v11, v15

    move/from16 v10, v19

    move-object/from16 v5, v18

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mHideString:Ljava/lang/String;

    goto :goto_5

    :cond_7
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateWidth:I

    goto :goto_6

    :cond_8
    const/4 v1, 0x0

    goto :goto_7

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->close()V

    const/4 v1, 0x0

    move-wide v11, v15

    move/from16 v10, v19

    move-object/from16 v5, v18

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f1000c0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_7
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/CalendarController;->deregisterEventHandler(Ljava/lang/Integer;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->mPaused:Z

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mHomeTime:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mHomeTimeUpdater:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarViewAdapter;->onPause()V

    :cond_0
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/android/calendar/GeneralPreferences;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :cond_1
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getViewType()I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getViewType()I

    move-result v1

    invoke-static {p0, v1}, Lcom/android/calendar/Utils;->setDefaultView(Landroid/content/Context;I)V

    :cond_2
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mHandler:Lcom/android/calendar/AllInOneActivity$QueryHandler;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mTimeChangesUpdater:Ljava/lang/Runnable;

    invoke-static {v1, v2}, Lcom/android/calendar/Utils;->resetMidnightUpdater(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mCalIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {p0, v1}, Lcom/android/calendar/Utils;->clearTimeChangesReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mSearchMenu:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mSearchMenu:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mSearchMenu:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 13
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    const-string v0, "TARDIS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/calendar/Utils;->tardis()V

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mSearchMenu:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v2, 0x100

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v12

    move-object v1, p0

    move-object v5, v4

    move-object v11, p1

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 24

    invoke-super/range {p0 .. p0}, Landroid/app/Activity;->onResume()V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mOnSaveInstanceStateCalled:Z

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/calendar/AllInOneActivity;->mIsClearEventsCompleted:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/calendar/AllInOneActivity;->mIsClearEventsCompleted:Z

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v1, v2, v0}, Lcom/android/calendar/CalendarController;->registerFirstEventHandler(ILcom/android/calendar/CalendarController$EventHandler;)V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mOnSaveInstanceStateCalled:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/AllInOneActivity;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mUpdateOnResume:Z

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getTime()J

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v3}, Lcom/android/calendar/CalendarController;->getViewType()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/calendar/AllInOneActivity;->initFragments(JILandroid/os/Bundle;)V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mUpdateOnResume:Z

    :cond_1
    new-instance v5, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-direct {v5, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getTime()J

    move-result-wide v1

    invoke-virtual {v5, v1, v2}, Landroid/text/format/Time;->set(J)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v3, 0x400

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v2}, Lcom/android/calendar/CalendarController;->getDateFlags()J

    move-result-wide v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v2, p0

    move-object v6, v5

    invoke-virtual/range {v1 .. v13}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mActionBarMenuSpinnerAdapter:Lcom/android/calendar/CalendarViewAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/android/calendar/CalendarViewAdapter;->refresh(Landroid/content/Context;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/AllInOneActivity;->mControlsMenu:Landroid/view/MenuItem;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mHideControls:Z

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mShowString:Ljava/lang/String;

    :goto_1
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_3
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mPaused:Z

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/calendar/AllInOneActivity;->mViewEventId:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/calendar/AllInOneActivity;->mIntentEventStartMillis:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/calendar/AllInOneActivity;->mIntentEventEndMillis:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    const-wide/16 v20, -0x1

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/calendar/AllInOneActivity;->mIntentEventStartMillis:J

    cmp-long v1, v22, v1

    if-lez v1, :cond_4

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/calendar/AllInOneActivity;->mIntentEventEndMillis:J

    cmp-long v1, v22, v1

    if-gez v1, :cond_4

    move-wide/from16 v20, v22

    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v8, 0x2

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/calendar/AllInOneActivity;->mViewEventId:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/calendar/AllInOneActivity;->mIntentEventStartMillis:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/calendar/AllInOneActivity;->mIntentEventEndMillis:J

    const/16 v16, -0x1

    const/16 v17, -0x1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/AllInOneActivity;->mIntentAttendeeResponse:I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/AllInOneActivity;->mIntentAllDay:Z

    invoke-static {v1, v2}, Lcom/android/calendar/CalendarController$EventInfo;->buildViewExtraLong(IZ)J

    move-result-wide v18

    move-object/from16 v7, p0

    invoke-virtual/range {v6 .. v21}, Lcom/android/calendar/CalendarController;->sendEventRelatedEventWithExtra(Ljava/lang/Object;JJJJIIJJ)V

    const-wide/16 v1, -0x1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/android/calendar/AllInOneActivity;->mViewEventId:J

    const-wide/16 v1, -0x1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/android/calendar/AllInOneActivity;->mIntentEventStartMillis:J

    const-wide/16 v1, -0x1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/android/calendar/AllInOneActivity;->mIntentEventEndMillis:J

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/android/calendar/AllInOneActivity;->mIntentAllDay:Z

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mHandler:Lcom/android/calendar/AllInOneActivity$QueryHandler;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/AllInOneActivity;->mTimeChangesUpdater:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/AllInOneActivity;->mTimeZone:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/android/calendar/Utils;->setMidnightUpdater(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mTimeChangesUpdater:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->setTimeChangesReceiver(Landroid/content/Context;Ljava/lang/Runnable;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mCalIntentReceiver:Landroid/content/BroadcastReceiver;

    return-void

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v3, 0x80

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, -0x1

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    const-string v1, "AllInOneActivity"

    const-string v2, "After CLEAR EVENTS COMPLETED, send Event EVENTS_CHANGED."

    invoke-static {v1, v2}, Lcom/mediatek/calendar/LogUtil;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/AllInOneActivity;->mHideString:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mOnSaveInstanceStateCalled:Z

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "key_restore_time"

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "key_restore_view"

    iget v1, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const-string v0, "key_event_id"

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getEventId()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_0
    const-string v0, "key_check_for_accounts"

    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->mCheckForAccounts:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mSearchMenu:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mSearchMenu:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const-string v0, "preferences_week_start_day"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mPaused:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->mUpdateOnResume:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarController;->getTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v2}, Lcom/android/calendar/CalendarController;->getViewType()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/calendar/AllInOneActivity;->initFragments(JILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onSuggestionClick(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mSearchMenu:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    const/4 v0, 0x0

    return v0
.end method

.method public onSuggestionSelect(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 10
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    const/4 v8, 0x2

    const/4 v9, 0x1

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    const-string v0, "AllInOneActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TabSelected AllInOne="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " finishing:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mDayTab:Landroid/app/ActionBar$Tab;

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    if-eq v0, v8, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mWeekTab:Landroid/app/ActionBar$Tab;

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const/4 v8, 0x3

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mMonthTab:Landroid/app/ActionBar$Tab;

    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const/4 v8, 0x4

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mAgendaTab:Landroid/app/ActionBar$Tab;

    if-ne p1, v0, :cond_3

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    if-eq v0, v9, :cond_3

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    move-object v1, p0

    move-object v5, v4

    move v8, v9

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    :cond_3
    const-string v1, "AllInOneActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TabSelected event from unknown tab: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_4

    const-string v0, "null"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "AllInOneActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CurrentView:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/AllInOneActivity;->mCurrentView:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Tab:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Day:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mDayTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Week:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mWeekTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Month:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mMonthTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Agenda:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->mAgendaTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 9

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v2, 0x200

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    return-void
.end method

.method public setControlsOffset(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->mOrientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mMiniMonth:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mCalendarsList:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mControlsParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateWidth:I

    sub-int/2addr v1, p1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mMiniMonthContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mControlsParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mMiniMonth:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mCalendarsList:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mVerticalControlsParams:Landroid/widget/LinearLayout$LayoutParams;

    if-nez v0, :cond_1

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    iget v2, p0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateHeight:I

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mVerticalControlsParams:Landroid/widget/LinearLayout$LayoutParams;

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mVerticalControlsParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/android/calendar/AllInOneActivity;->mControlsAnimateHeight:I

    sub-int/2addr v1, p1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->mMiniMonthContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->mVerticalControlsParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
