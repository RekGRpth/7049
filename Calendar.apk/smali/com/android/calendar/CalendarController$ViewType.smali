.class public interface abstract Lcom/android/calendar/CalendarController$ViewType;
.super Ljava/lang/Object;
.source "CalendarController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/CalendarController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ViewType"
.end annotation


# static fields
.field public static final AGENDA:I = 0x1

.field public static final CURRENT:I = 0x0

.field public static final DAY:I = 0x2

.field public static final DETAIL:I = -0x1

.field public static final EDIT:I = 0x5

.field public static final MONTH:I = 0x4

.field public static final WEEK:I = 0x3
