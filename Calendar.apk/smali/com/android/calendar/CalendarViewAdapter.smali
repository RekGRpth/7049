.class public Lcom/android/calendar/CalendarViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "CalendarViewAdapter.java"


# static fields
.field public static final AGENDA_BUTTON_INDEX:I = 0x3

.field private static final BUTTON_VIEW_TYPE:I = 0x0

.field public static final DAY_BUTTON_INDEX:I = 0x0

.field public static final MONTH_BUTTON_INDEX:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MenuSpinnerAdapter"

.field static final VIEW_TYPE_NUM:I = 0x1

.field public static final WEEK_BUTTON_INDEX:I = 0x1


# instance fields
.field private final mButtonNames:[Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mCurrentMainView:I

.field private final mFormatter:Ljava/util/Formatter;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mMidnightHandler:Landroid/os/Handler;

.field private mMilliTime:J

.field private final mShowDate:Z

.field private final mStringBuilder:Ljava/lang/StringBuilder;

.field private final mTimeUpdater:Ljava/lang/Runnable;

.field private mTimeZone:Ljava/lang/String;

.field private mTodayJulianDay:J


# direct methods
.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Z

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mMidnightHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/calendar/CalendarViewAdapter$1;

    invoke-direct {v0, p0}, Lcom/android/calendar/CalendarViewAdapter$1;-><init>(Lcom/android/calendar/CalendarViewAdapter;)V

    iput-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeUpdater:Ljava/lang/Runnable;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mMidnightHandler:Landroid/os/Handler;

    iput p2, p0, Lcom/android/calendar/CalendarViewAdapter;->mCurrentMainView:I

    iput-object p1, p0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    iput-boolean p3, p0, Lcom/android/calendar/CalendarViewAdapter;->mShowDate:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/android/calendar/CalendarViewAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mFormatter:Ljava/util/Formatter;

    if-eqz p3, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/calendar/CalendarViewAdapter;->refresh(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/android/calendar/CalendarViewAdapter;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/calendar/CalendarViewAdapter;

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private buildDayOfWeek()Ljava/lang/String;
    .locals 17

    new-instance v12, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-direct {v12, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    invoke-virtual {v12, v1, v2}, Landroid/text/format/Time;->set(J)V

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    iget-wide v3, v12, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    int-to-long v10, v1

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mTodayJulianDay:J

    cmp-long v1, v10, v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    const v14, 0x7f0c0087

    const/4 v1, 0x1

    new-array v15, v1, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/CalendarViewAdapter;->mFormatter:Ljava/util/Formatter;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    const/4 v7, 0x2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-static/range {v1 .. v8}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    :goto_0
    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mTodayJulianDay:J

    const-wide/16 v3, 0x1

    sub-long/2addr v1, v3

    cmp-long v1, v10, v1

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    const v14, 0x7f0c0088

    const/4 v1, 0x1

    new-array v15, v1, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/CalendarViewAdapter;->mFormatter:Ljava/util/Formatter;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    const/4 v7, 0x2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-static/range {v1 .. v8}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mTodayJulianDay:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    cmp-long v1, v10, v1

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    const v14, 0x7f0c0089

    const/4 v1, 0x1

    new-array v15, v1, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/CalendarViewAdapter;->mFormatter:Ljava/util/Formatter;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    const/4 v7, 0x2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-static/range {v1 .. v8}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/CalendarViewAdapter;->mFormatter:Ljava/util/Formatter;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    const/4 v7, 0x2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-static/range {v1 .. v8}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0
.end method

.method private buildFullDate()Ljava/lang/String;
    .locals 9

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/CalendarViewAdapter;->mFormatter:Ljava/util/Formatter;

    iget-wide v2, p0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    iget-wide v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    const/16 v6, 0x14

    iget-object v7, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method private buildMonthDate()Ljava/lang/String;
    .locals 9

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/CalendarViewAdapter;->mFormatter:Ljava/util/Formatter;

    iget-wide v2, p0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    iget-wide v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    const/16 v6, 0x38

    iget-object v7, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method private buildMonthDayDate()Ljava/lang/String;
    .locals 9

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/CalendarViewAdapter;->mFormatter:Ljava/util/Formatter;

    iget-wide v2, p0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    iget-wide v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    const/16 v6, 0x18

    iget-object v7, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method private buildMonthYearDate()Ljava/lang/String;
    .locals 9

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/CalendarViewAdapter;->mFormatter:Ljava/util/Formatter;

    iget-wide v2, p0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    iget-wide v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    const/16 v6, 0x34

    iget-object v7, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method private buildWeekDate()Ljava/lang/String;
    .locals 17

    new-instance v13, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-direct {v13, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    invoke-virtual {v13, v1, v2}, Landroid/text/format/Time;->set(J)V

    const/16 v1, 0x9

    iput v1, v13, Landroid/text/format/Time;->hour:I

    const/4 v1, 0x0

    iput v1, v13, Landroid/text/format/Time;->minute:I

    const/4 v1, 0x0

    iput v1, v13, Landroid/text/format/Time;->second:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v12

    iget v10, v13, Landroid/text/format/Time;->weekDay:I

    sub-int v11, v10, v12

    if-eqz v11, :cond_1

    if-gez v11, :cond_0

    add-int/lit8 v11, v11, 0x7

    :cond_0
    iget v1, v13, Landroid/text/format/Time;->monthDay:I

    sub-int/2addr v1, v11

    iput v1, v13, Landroid/text/format/Time;->monthDay:I

    const/4 v1, 0x1

    invoke-virtual {v13, v1}, Landroid/text/format/Time;->normalize(Z)J

    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v13, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    const-wide/32 v1, 0x240c8400

    add-long/2addr v1, v3

    const-wide/32 v15, 0x5265c00

    sub-long v5, v1, v15

    new-instance v14, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-direct {v14, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v5, v6}, Landroid/text/format/Time;->set(J)V

    const/16 v7, 0x18

    iget v1, v13, Landroid/text/format/Time;->month:I

    iget v2, v14, Landroid/text/format/Time;->month:I

    if-eq v1, v2, :cond_2

    const/high16 v1, 0x10000

    or-int/2addr v7, v1

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/CalendarViewAdapter;->mFormatter:Ljava/util/Formatter;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-static/range {v1 .. v8}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9
.end method

.method private buildWeekNum()Ljava/lang/String;
    .locals 6

    iget-wide v1, p0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    iget-object v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/android/calendar/Utils;->getWeekNumberFromTime(JLandroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0004

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private setMidnightHandler()V
    .locals 7

    iget-object v5, p0, Lcom/android/calendar/CalendarViewAdapter;->mMidnightHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeUpdater:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v4, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    const v5, 0x15180

    iget v6, v4, Landroid/text/format/Time;->hour:I

    mul-int/lit16 v6, v6, 0xe10

    sub-int/2addr v5, v6

    iget v6, v4, Landroid/text/format/Time;->minute:I

    mul-int/lit8 v6, v6, 0x3c

    sub-int/2addr v5, v6

    iget v6, v4, Landroid/text/format/Time;->second:I

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v2, v5

    iget-object v5, p0, Lcom/android/calendar/CalendarViewAdapter;->mMidnightHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeUpdater:Ljava/lang/Runnable;

    invoke-virtual {v5, v6, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f040002

    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f100006

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f100007

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p1, :pswitch_data_0

    move-object v1, p2

    :cond_0
    :goto_0
    return-object v1

    :pswitch_0
    iget-object v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mShowDate:Z

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->buildMonthDayDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mShowDate:Z

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->buildWeekDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mShowDate:Z

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->buildMonthDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mShowDate:Z

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->buildMonthDayDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/16 v8, 0x8

    const v7, 0x7f040004

    const v6, 0x7f040003

    const/4 v5, 0x0

    iget-boolean v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mShowDate:Z

    if-eqz v4, :cond_3

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, v6, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v4, v6, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    const v4, 0x7f100008

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f100009

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mCurrentMainView:I

    packed-switch v4, :pswitch_data_0

    const/4 v2, 0x0

    :goto_1
    return-object v2

    :cond_1
    move-object v2, p2

    goto :goto_0

    :pswitch_0
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->buildDayOfWeek()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->buildFullDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_1
    iget-object v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/calendar/Utils;->getShowWeekNumber(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->buildWeekNum()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->buildMonthYearDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :pswitch_2
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->buildMonthYearDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_3
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->buildDayOfWeek()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->buildFullDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, v7, :cond_5

    :cond_4
    iget-object v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v4, v7, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_3
    move-object v1, v2

    check-cast v1, Landroid/widget/TextView;

    iget v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mCurrentMainView:I

    packed-switch v4, :pswitch_data_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    move-object v2, p2

    goto :goto_3

    :pswitch_4
    iget-object v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    aget-object v4, v4, v5

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_5
    iget-object v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_6
    iget-object v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_7
    iget-object v4, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mButtonNames:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/CalendarViewAdapter;->mMidnightHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public refresh(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    iget-object v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeUpdater:Ljava/lang/Runnable;

    invoke-static {p1, v3}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mTimeZone:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    iget-wide v3, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v3, v4}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v3

    int-to-long v3, v3

    iput-wide v3, p0, Lcom/android/calendar/CalendarViewAdapter;->mTodayJulianDay:J

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/android/calendar/CalendarViewAdapter;->setMidnightHandler()V

    return-void
.end method

.method public setMainView(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/CalendarViewAdapter;->mCurrentMainView:I

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/calendar/CalendarViewAdapter;->mMilliTime:J

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
