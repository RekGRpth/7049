.class public Lcom/android/calendar/EventInfoActivity;
.super Landroid/app/Activity;
.source "EventInfoActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EventInfoActivity"


# instance fields
.field private mEndMillis:J

.field private mEventId:J

.field private mInfoFragment:Lcom/android/calendar/EventInfoFragment;

.field private mStartMillis:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 27
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v25

    const/4 v8, 0x0

    const-wide/16 v1, 0x0

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/android/calendar/EventInfoActivity;->mEventId:J

    const/16 v18, 0x0

    if-eqz p1, :cond_2

    const-string v1, "key_event_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/android/calendar/EventInfoActivity;->mEventId:J

    const-string v1, "key_start_millis"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/android/calendar/EventInfoActivity;->mStartMillis:J

    const-string v1, "key_end_millis"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/android/calendar/EventInfoActivity;->mEndMillis:J

    const-string v1, "key_attendee_response"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v1, "key_fragment_is_dialog"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v18

    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v1, 0x7f090004

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_3

    const v1, 0x7f090005

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/CalendarController;->getInstance(Landroid/content/Context;)Lcom/android/calendar/CalendarController;

    move-result-object v1

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/calendar/EventInfoActivity;->mEventId:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/calendar/EventInfoActivity;->mStartMillis:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/calendar/EventInfoActivity;->mEndMillis:J

    invoke-virtual/range {v1 .. v8}, Lcom/android/calendar/CalendarController;->launchViewEvent(JJJI)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    if-eqz v25, :cond_0

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual/range {v25 .. v25}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "beginTime"

    const-wide/16 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/android/calendar/EventInfoActivity;->mStartMillis:J

    const-string v1, "endTime"

    const-wide/16 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/android/calendar/EventInfoActivity;->mEndMillis:J

    const-string v1, "attendeeStatus"

    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual/range {v25 .. v25}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v21

    if-eqz v21, :cond_0

    :try_start_0
    invoke-virtual/range {v21 .. v21}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/android/calendar/EventInfoActivity;->mEventId:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v22

    const-string v1, "EventInfoActivity"

    const-string v2, "No event id"

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const v1, 0x7f040038

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f1000bb

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/calendar/EventInfoActivity;->mInfoFragment:Lcom/android/calendar/EventInfoFragment;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v20

    if-eqz v20, :cond_4

    const/4 v1, 0x6

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoActivity;->mInfoFragment:Lcom/android/calendar/EventInfoFragment;

    if-nez v1, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v24

    new-instance v9, Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/android/calendar/EventInfoActivity;->mEventId:J

    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/android/calendar/EventInfoActivity;->mStartMillis:J

    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/android/calendar/EventInfoActivity;->mEndMillis:J

    if-eqz v18, :cond_5

    const/16 v19, 0x1

    :goto_2
    move-object/from16 v10, p0

    move/from16 v17, v8

    invoke-direct/range {v9 .. v19}, Lcom/android/calendar/EventInfoFragment;-><init>(Landroid/content/Context;JJJIZI)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/android/calendar/EventInfoActivity;->mInfoFragment:Lcom/android/calendar/EventInfoFragment;

    const v1, 0x7f1000bb

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoActivity;->mInfoFragment:Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, v24

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual/range {v24 .. v24}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_1

    :cond_5
    const/16 v19, 0x0

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
