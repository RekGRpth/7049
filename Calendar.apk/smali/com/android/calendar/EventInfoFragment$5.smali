.class Lcom/android/calendar/EventInfoFragment$5;
.super Ljava/lang/Object;
.source "EventInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/calendar/EventInfoFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/calendar/EventInfoFragment;


# direct methods
.method constructor <init>(Lcom/android/calendar/EventInfoFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/EventInfoFragment;->access$2500(Lcom/android/calendar/EventInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    new-instance v3, Lcom/android/calendar/DeleteEventHelper;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/EventInfoFragment;->access$2700(Lcom/android/calendar/EventInfoFragment;)Landroid/content/Context;

    move-result-object v4

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/EventInfoFragment;->access$2800(Lcom/android/calendar/EventInfoFragment;)Landroid/app/Activity;

    move-result-object v5

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/EventInfoFragment;->access$2900(Lcom/android/calendar/EventInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/EventInfoFragment;->access$3000(Lcom/android/calendar/EventInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-direct {v3, v4, v5, v0}, Lcom/android/calendar/DeleteEventHelper;-><init>(Landroid/content/Context;Landroid/app/Activity;Z)V

    invoke-static {v2, v3}, Lcom/android/calendar/EventInfoFragment;->access$2602(Lcom/android/calendar/EventInfoFragment;Lcom/android/calendar/DeleteEventHelper;)Lcom/android/calendar/DeleteEventHelper;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/EventInfoFragment;->access$2600(Lcom/android/calendar/EventInfoFragment;)Lcom/android/calendar/DeleteEventHelper;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-virtual {v0, v2}, Lcom/android/calendar/DeleteEventHelper;->setDeleteNotificationListener(Lcom/android/calendar/DeleteEventHelper$DeleteNotifyListener;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/EventInfoFragment;->access$2600(Lcom/android/calendar/EventInfoFragment;)Lcom/android/calendar/DeleteEventHelper;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$3100(Lcom/android/calendar/EventInfoFragment;)Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/calendar/DeleteEventHelper;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v0, v1}, Lcom/android/calendar/EventInfoFragment;->access$3202(Lcom/android/calendar/EventInfoFragment;Z)Z

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/EventInfoFragment;->access$2600(Lcom/android/calendar/EventInfoFragment;)Lcom/android/calendar/DeleteEventHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/EventInfoFragment;->access$3300(Lcom/android/calendar/EventInfoFragment;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$3400(Lcom/android/calendar/EventInfoFragment;)J

    move-result-wide v3

    iget-object v5, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v5}, Lcom/android/calendar/EventInfoFragment;->access$1300(Lcom/android/calendar/EventInfoFragment;)J

    move-result-wide v5

    const/4 v7, -0x1

    iget-object v8, p0, Lcom/android/calendar/EventInfoFragment$5;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v8}, Lcom/android/calendar/EventInfoFragment;->access$3500(Lcom/android/calendar/EventInfoFragment;)Ljava/lang/Runnable;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/DeleteEventHelper;->delete(JJJILjava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
