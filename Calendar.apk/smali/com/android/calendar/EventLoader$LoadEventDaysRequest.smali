.class Lcom/android/calendar/EventLoader$LoadEventDaysRequest;
.super Ljava/lang/Object;
.source "EventLoader.java"

# interfaces
.implements Lcom/android/calendar/EventLoader$LoadRequest;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/EventLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LoadEventDaysRequest"
.end annotation


# static fields
.field private static final PROJECTION:[Ljava/lang/String;


# instance fields
.field public eventDays:[Z

.field public numDays:I

.field public startDay:I

.field public uiCallback:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "startDay"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "endDay"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(II[ZLjava/lang/Runnable;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # [Z
    .param p4    # Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->startDay:I

    iput p2, p0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->numDays:I

    iput-object p3, p0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->eventDays:[Z

    iput-object p4, p0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->uiCallback:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public processRequest(Lcom/android/calendar/EventLoader;)V
    .locals 13
    .param p1    # Lcom/android/calendar/EventLoader;

    const/4 v11, 0x0

    invoke-static {p1}, Lcom/android/calendar/EventLoader;->access$000(Lcom/android/calendar/EventLoader;)Landroid/os/Handler;

    move-result-object v5

    invoke-static {p1}, Lcom/android/calendar/EventLoader;->access$100(Lcom/android/calendar/EventLoader;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v10, p0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->eventDays:[Z

    invoke-static {v10, v11}, Ljava/util/Arrays;->fill([ZZ)V

    iget v10, p0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->startDay:I

    iget v11, p0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->numDays:I

    sget-object v12, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->PROJECTION:[Ljava/lang/String;

    invoke-static {v0, v10, v11, v12}, Landroid/provider/CalendarContract$EventDays;->query(Landroid/content/ContentResolver;II[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    const-string v10, "startDay"

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    const-string v10, "endDay"

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iget v10, p0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->startDay:I

    sub-int v10, v3, v10

    const/4 v11, 0x0

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget v10, p0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->startDay:I

    sub-int v10, v7, v10

    const/16 v11, 0x1e

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v8

    move v6, v4

    :goto_0
    if-gt v6, v8, :cond_0

    iget-object v10, p0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->eventDays:[Z

    const/4 v11, 0x1

    aput-boolean v11, v10, v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    iget-object v10, p0, Lcom/android/calendar/EventLoader$LoadEventDaysRequest;->uiCallback:Ljava/lang/Runnable;

    invoke-virtual {v5, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :catchall_0
    move-exception v10

    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v10
.end method

.method public skipRequest(Lcom/android/calendar/EventLoader;)V
    .locals 0
    .param p1    # Lcom/android/calendar/EventLoader;

    return-void
.end method
