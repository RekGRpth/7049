.class public Lcom/android/calendar/ExpandableTextView;
.super Landroid/widget/LinearLayout;
.source "ExpandableTextView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field mButton:Landroid/widget/ImageButton;

.field private mCollapseDrawable:Landroid/graphics/drawable/Drawable;

.field private mCollapsed:Z

.field private mExpandDrawable:Landroid/graphics/drawable/Drawable;

.field private mMaxCollapsedLines:I

.field private mRelayout:Z

.field mTv:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mRelayout:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mCollapsed:Z

    const/16 v0, 0x8

    iput v0, p0, Lcom/android/calendar/ExpandableTextView;->mMaxCollapsedLines:I

    invoke-virtual {p0}, Lcom/android/calendar/ExpandableTextView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mRelayout:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mCollapsed:Z

    const/16 v0, 0x8

    iput v0, p0, Lcom/android/calendar/ExpandableTextView;->mMaxCollapsedLines:I

    invoke-virtual {p0}, Lcom/android/calendar/ExpandableTextView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mRelayout:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mCollapsed:Z

    const/16 v0, 0x8

    iput v0, p0, Lcom/android/calendar/ExpandableTextView;->mMaxCollapsedLines:I

    invoke-virtual {p0}, Lcom/android/calendar/ExpandableTextView;->init()V

    return-void
.end method

.method private findViews()V
    .locals 1

    const v0, 0x7f1000a0

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mTv:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mTv:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f1000a1

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public getText()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mTv:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mTv:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method init()V
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/ExpandableTextView;->mMaxCollapsedLines:I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mExpandDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mCollapseDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mCollapsed:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mCollapsed:Z

    iget-object v1, p0, Lcom/android/calendar/ExpandableTextView;->mButton:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mCollapsed:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mExpandDrawable:Landroid/graphics/drawable/Drawable;

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/android/calendar/ExpandableTextView;->mTv:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mCollapsed:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/calendar/ExpandableTextView;->mMaxCollapsedLines:I

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mCollapseDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    :cond_3
    const v0, 0x7fffffff

    goto :goto_3
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/16 v1, 0x8

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mRelayout:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_2

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-boolean v2, p0, Lcom/android/calendar/ExpandableTextView;->mRelayout:Z

    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mTv:Landroid/widget/TextView;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mTv:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/ExpandableTextView;->mMaxCollapsedLines:I

    if-le v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/ExpandableTextView;->mCollapsed:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mTv:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/calendar/ExpandableTextView;->mMaxCollapsedLines:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    :cond_3
    iget-object v0, p0, Lcom/android/calendar/ExpandableTextView;->mButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/calendar/ExpandableTextView;->mRelayout:Z

    iget-object v1, p0, Lcom/android/calendar/ExpandableTextView;->mTv:Landroid/widget/TextView;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/ExpandableTextView;->findViews()V

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/ExpandableTextView;->mTv:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
