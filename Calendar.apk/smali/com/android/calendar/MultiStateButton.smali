.class public Lcom/android/calendar/MultiStateButton;
.super Landroid/widget/Button;
.source "MultiStateButton.java"


# instance fields
.field private mButtonDrawable:Landroid/graphics/drawable/Drawable;

.field private mButtonResource:I

.field private mButtonResources:[I

.field private mMaxStates:I

.field private mState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/MultiStateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/calendar/MultiStateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/android/calendar/MultiStateButton;->mMaxStates:I

    iput v2, p0, Lcom/android/calendar/MultiStateButton;->mState:I

    new-array v0, v0, [I

    const v1, 0x7f020061

    aput v1, v0, v2

    iput-object v0, p0, Lcom/android/calendar/MultiStateButton;->mButtonResources:[I

    iget-object v0, p0, Lcom/android/calendar/MultiStateButton;->mButtonResources:[I

    iget v1, p0, Lcom/android/calendar/MultiStateButton;->mState:I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/calendar/MultiStateButton;->setButtonDrawable(I)V

    return-void
.end method


# virtual methods
.method public getState()I
    .locals 1

    iget v0, p0, Lcom/android/calendar/MultiStateButton;->mState:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v6, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Landroid/widget/TextView;->getGravity()I

    move-result v6

    and-int/lit8 v2, v6, 0x70

    invoke-virtual {p0}, Landroid/widget/TextView;->getGravity()I

    move-result v6

    and-int/lit8 v1, v6, 0x7

    iget-object v6, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iget-object v6, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    const/4 v5, 0x0

    const/4 v4, 0x0

    sparse-switch v2, :sswitch_data_0

    :goto_0
    sparse-switch v1, :sswitch_data_1

    :goto_1
    iget-object v6, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    add-int v7, v4, v3

    add-int v8, v5, v0

    invoke-virtual {v6, v4, v5, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v6, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void

    :sswitch_0
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v6

    sub-int v5, v6, v0

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v6

    sub-int/2addr v6, v0

    div-int/lit8 v5, v6, 0x2

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v6

    sub-int v4, v6, v3

    goto :goto_1

    :sswitch_3
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v6

    sub-int/2addr v6, v3

    div-int/lit8 v4, v6, 0x2

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_3
        0x5 -> :sswitch_2
    .end sparse-switch
.end method

.method public performClick()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/calendar/MultiStateButton;->transitionState()V

    invoke-super {p0}, Landroid/view/View;->performClick()Z

    move-result v0

    return v0
.end method

.method public setButtonDrawable(I)V
    .locals 3
    .param p1    # I

    if-eqz p1, :cond_0

    iget v1, p0, Lcom/android/calendar/MultiStateButton;->mButtonResource:I

    if-ne p1, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/android/calendar/MultiStateButton;->mButtonResource:I

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/calendar/MultiStateButton;->mButtonResource:I

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/MultiStateButton;->mButtonResource:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/calendar/MultiStateButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Landroid/view/View;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    iput-object p1, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    iget-object v0, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setMinHeight(I)V

    iget-object v0, p0, Lcom/android/calendar/MultiStateButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setWidth(I)V

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public setButtonResources([I)V
    .locals 2
    .param p1    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Button resources cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    array-length v0, p1

    iput v0, p0, Lcom/android/calendar/MultiStateButton;->mMaxStates:I

    iget v0, p0, Lcom/android/calendar/MultiStateButton;->mState:I

    iget v1, p0, Lcom/android/calendar/MultiStateButton;->mMaxStates:I

    if-lt v0, v1, :cond_1

    iget v0, p0, Lcom/android/calendar/MultiStateButton;->mMaxStates:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/MultiStateButton;->mState:I

    :cond_1
    iput-object p1, p0, Lcom/android/calendar/MultiStateButton;->mButtonResources:[I

    return-void
.end method

.method public setState(I)Z
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/calendar/MultiStateButton;->mMaxStates:I

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const-string v0, "Cal"

    const-string v1, "MultiStateButton state set to value greater than maxState or < 0"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iput p1, p0, Lcom/android/calendar/MultiStateButton;->mState:I

    iget-object v0, p0, Lcom/android/calendar/MultiStateButton;->mButtonResources:[I

    iget v1, p0, Lcom/android/calendar/MultiStateButton;->mState:I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/calendar/MultiStateButton;->setButtonDrawable(I)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public transitionState()V
    .locals 2

    iget v0, p0, Lcom/android/calendar/MultiStateButton;->mState:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/calendar/MultiStateButton;->mMaxStates:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/MultiStateButton;->mState:I

    iget-object v0, p0, Lcom/android/calendar/MultiStateButton;->mButtonResources:[I

    iget v1, p0, Lcom/android/calendar/MultiStateButton;->mState:I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/calendar/MultiStateButton;->setButtonDrawable(I)V

    return-void
.end method
