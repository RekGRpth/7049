.class public final Lcom/android/calendar/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final agenda_day_bar_background_color:I = 0x7f080033

.field public static final agenda_day_item_text_color:I = 0x7f080037

.field public static final agenda_general_background_color:I = 0x7f080036

.field public static final agenda_item_declined_color:I = 0x7f080031

.field public static final agenda_item_not_selected:I = 0x7f080030

.field public static final agenda_item_standard_color:I = 0x7f080032

.field public static final agenda_item_where_declined_text_color:I = 0x7f080039

.field public static final agenda_item_where_text_color:I = 0x7f080038

.field public static final agenda_list_separator_color:I = 0x7f08003c

.field public static final agenda_past_days_bar_background_color:I = 0x7f080034

.field public static final agenda_past_present_separator_color:I = 0x7f080035

.field public static final agenda_selected_background_color:I = 0x7f08003d

.field public static final agenda_selected_text_color:I = 0x7f08003e

.field public static final alert_event_other:I = 0x7f08004d

.field public static final alert_event_title:I = 0x7f08004c

.field public static final alert_past_event:I = 0x7f08004e

.field public static final appwidget_date:I = 0x7f080050

.field public static final appwidget_item_allday_color:I = 0x7f080055

.field public static final appwidget_item_declined_color:I = 0x7f080053

.field public static final appwidget_item_standard_color:I = 0x7f080054

.field public static final appwidget_month:I = 0x7f080052

.field public static final appwidget_no_events:I = 0x7f080059

.field public static final appwidget_row_in_progress:I = 0x7f08004f

.field public static final appwidget_title:I = 0x7f080056

.field public static final appwidget_week:I = 0x7f080051

.field public static final appwidget_when:I = 0x7f080057

.field public static final appwidget_where:I = 0x7f080058

.field public static final background_color:I = 0x7f080003

.field public static final calendar_ampm_label:I = 0x7f08000d

.field public static final calendar_date_banner_background:I = 0x7f08000f

.field public static final calendar_date_banner_text_color:I = 0x7f080010

.field public static final calendar_date_range_color:I = 0x7f080008

.field public static final calendar_event_text_color:I = 0x7f080006

.field public static final calendar_future_bg_color:I = 0x7f08000b

.field public static final calendar_grid_area_selected:I = 0x7f080011

.field public static final calendar_grid_line_highlight_color:I = 0x7f080014

.field public static final calendar_grid_line_inner_horizontal_color:I = 0x7f080012

.field public static final calendar_grid_line_inner_vertical_color:I = 0x7f080013

.field public static final calendar_hidden:I = 0x7f08002d

.field public static final calendar_hour_background:I = 0x7f08000e

.field public static final calendar_hour_label:I = 0x7f08000c

.field public static final calendar_owner_text_color:I = 0x7f080007

.field public static final calendar_secondary_hidden:I = 0x7f08002f

.field public static final calendar_secondary_visible:I = 0x7f08002e

.field public static final calendar_view_switch_menu_text_color:I = 0x7f08003a

.field public static final calendar_view_switch_menu_text_color_light:I = 0x7f08003b

.field public static final calendar_visible:I = 0x7f08002c

.field public static final day_clicked_background_color:I = 0x7f080041

.field public static final day_event_clicked_background_color:I = 0x7f080040

.field public static final day_past_background_color:I = 0x7f080042

.field public static final edit_event_separator:I = 0x7f08002b

.field public static final event_background:I = 0x7f080004

.field public static final event_center:I = 0x7f08002a

.field public static final event_info_body_color:I = 0x7f080048

.field public static final event_info_description_color:I = 0x7f080047

.field public static final event_info_headline_color:I = 0x7f080043

.field public static final event_info_headline_link_color:I = 0x7f080045

.field public static final event_info_headline_transparent_color:I = 0x7f080044

.field public static final event_info_label_background_color:I = 0x7f08004a

.field public static final event_info_label_color:I = 0x7f080049

.field public static final event_info_organizer_color:I = 0x7f080046

.field public static final mini_month_bg_color:I = 0x7f080015

.field public static final mini_month_today_outline_color:I = 0x7f080016

.field public static final month_bgcolor:I = 0x7f080028

.field public static final month_day_names_color:I = 0x7f08001b

.field public static final month_day_number:I = 0x7f080017

.field public static final month_day_number_other:I = 0x7f080019

.field public static final month_dna_conflict_time_color:I = 0x7f080029

.field public static final month_event_color:I = 0x7f08001d

.field public static final month_event_extra_color:I = 0x7f08001e

.field public static final month_event_extra_other_color:I = 0x7f080020

.field public static final month_event_other_color:I = 0x7f08001f

.field public static final month_grid_lines:I = 0x7f080023

.field public static final month_mini_day_number:I = 0x7f080018

.field public static final month_other_bgcolor:I = 0x7f080025

.field public static final month_other_month_day_number:I = 0x7f080024

.field public static final month_saturday:I = 0x7f080021

.field public static final month_selected_week_bgcolor:I = 0x7f080026

.field public static final month_sunday:I = 0x7f080022

.field public static final month_today_bgcolor:I = 0x7f080027

.field public static final month_today_number:I = 0x7f08001c

.field public static final month_week_num_color:I = 0x7f08001a

.field public static final new_event_hint_text_color:I = 0x7f08003f

.field public static final panel_text_foreground:I = 0x7f080005

.field public static final pressed:I = 0x7f080000

.field public static final selection:I = 0x7f080001

.field public static final text_shadow_color:I = 0x7f08004b

.field public static final today_highlight_color:I = 0x7f080002

.field public static final week_saturday:I = 0x7f080009

.field public static final week_sunday:I = 0x7f08000a


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
