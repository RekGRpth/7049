.class Lcom/android/calendar/agenda/AgendaByDayAdapter$MultipleDayInfo;
.super Ljava/lang/Object;
.source "AgendaByDayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/agenda/AgendaByDayAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MultipleDayInfo"
.end annotation


# instance fields
.field final mAllDay:Z

.field final mEndDay:I

.field mEventEndTimeMilli:J

.field final mEventId:J

.field mEventStartTimeMilli:J

.field final mInstanceId:J

.field final mPosition:I


# direct methods
.method constructor <init>(IIJJJJZ)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # J
    .param p5    # J
    .param p7    # J
    .param p9    # J
    .param p11    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$MultipleDayInfo;->mPosition:I

    iput p2, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$MultipleDayInfo;->mEndDay:I

    iput-wide p3, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$MultipleDayInfo;->mEventId:J

    iput-wide p5, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$MultipleDayInfo;->mEventStartTimeMilli:J

    iput-wide p7, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$MultipleDayInfo;->mEventEndTimeMilli:J

    iput-wide p9, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$MultipleDayInfo;->mInstanceId:J

    iput-boolean p11, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$MultipleDayInfo;->mAllDay:Z

    return-void
.end method
