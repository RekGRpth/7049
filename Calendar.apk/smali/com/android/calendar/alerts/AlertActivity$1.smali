.class Lcom/android/calendar/alerts/AlertActivity$1;
.super Ljava/lang/Object;
.source "AlertActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/alerts/AlertActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/calendar/alerts/AlertActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/alerts/AlertActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/calendar/alerts/AlertActivity$1;->this$0:Lcom/android/calendar/alerts/AlertActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 12
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v7, p0, Lcom/android/calendar/alerts/AlertActivity$1;->this$0:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v7, p2}, Lcom/android/calendar/alerts/AlertActivity;->getItemForView(Landroid/view/View;)Landroid/database/Cursor;

    move-result-object v8

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity$1;->this$0:Lcom/android/calendar/alerts/AlertActivity;

    const/4 v10, 0x0

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v0, v10, v11}, Lcom/android/calendar/alerts/AlertActivity;->access$400(Lcom/android/calendar/alerts/AlertActivity;J)V

    const/4 v0, 0x6

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v1, v0

    const/4 v0, 0x4

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v0, 0x5

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity$1;->this$0:Lcom/android/calendar/alerts/AlertActivity;

    invoke-static/range {v0 .. v6}, Lcom/android/calendar/alerts/AlertUtils;->buildEventViewIntent(Landroid/content/Context;JJJ)Landroid/content/Intent;

    move-result-object v9

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity$1;->this$0:Lcom/android/calendar/alerts/AlertActivity;

    invoke-static {v0}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v0

    const-class v10, Lcom/android/calendar/EventInfoActivity;

    invoke-virtual {v0, v10}, Landroid/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/app/TaskStackBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/TaskStackBuilder;->startActivities()V

    invoke-virtual {v7}, Landroid/app/Activity;->finish()V

    return-void
.end method
