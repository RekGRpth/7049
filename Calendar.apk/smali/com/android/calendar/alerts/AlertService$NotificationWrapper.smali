.class public Lcom/android/calendar/alerts/AlertService$NotificationWrapper;
.super Ljava/lang/Object;
.source "AlertService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/alerts/AlertService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NotificationWrapper"
.end annotation


# instance fields
.field mBegin:J

.field mEnd:J

.field mEventId:J

.field mNotification:Landroid/app/Notification;

.field mNw:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/alerts/AlertService$NotificationWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Notification;)V
    .locals 0
    .param p1    # Landroid/app/Notification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/calendar/alerts/AlertService$NotificationWrapper;->mNotification:Landroid/app/Notification;

    return-void
.end method

.method public constructor <init>(Landroid/app/Notification;IJJJZ)V
    .locals 0
    .param p1    # Landroid/app/Notification;
    .param p2    # I
    .param p3    # J
    .param p5    # J
    .param p7    # J
    .param p9    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/calendar/alerts/AlertService$NotificationWrapper;->mNotification:Landroid/app/Notification;

    iput-wide p3, p0, Lcom/android/calendar/alerts/AlertService$NotificationWrapper;->mEventId:J

    iput-wide p5, p0, Lcom/android/calendar/alerts/AlertService$NotificationWrapper;->mBegin:J

    iput-wide p7, p0, Lcom/android/calendar/alerts/AlertService$NotificationWrapper;->mEnd:J

    return-void
.end method


# virtual methods
.method public add(Lcom/android/calendar/alerts/AlertService$NotificationWrapper;)V
    .locals 1
    .param p1    # Lcom/android/calendar/alerts/AlertService$NotificationWrapper;

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationWrapper;->mNw:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationWrapper;->mNw:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationWrapper;->mNw:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method
