.class public Lcom/android/calendar/event/AttendeesView;
.super Landroid/widget/LinearLayout;
.source "AttendeesView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/event/AttendeesView$PresenceQueryHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final EMAIL_PROJECTION_CONTACT_ID_INDEX:I = 0x0

.field private static final EMAIL_PROJECTION_CONTACT_LOOKUP_INDEX:I = 0x1

.field private static final EMAIL_PROJECTION_PHOTO_ID_INDEX:I = 0x2

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "AttendeesView"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDefaultBadge:Landroid/graphics/drawable/Drawable;

.field private final mDefaultPhotoAlpha:I

.field private final mDividerForMaybe:Landroid/view/View;

.field private final mDividerForNo:Landroid/view/View;

.field private final mDividerForNoResponse:Landroid/view/View;

.field private final mDividerForYes:Landroid/view/View;

.field private final mEntries:[Ljava/lang/CharSequence;

.field private final mGrayscaleFilter:Landroid/graphics/ColorMatrixColorFilter;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mMaybe:I

.field private mNo:I

.field private mNoResponse:I

.field private final mNoResponsePhotoAlpha:I

.field private final mPresenceQueryHandler:Lcom/android/calendar/event/AttendeesView$PresenceQueryHandler;

.field mRecycledPhotos:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mValidator:Lcom/android/common/Rfc822Validator;

.field private mYes:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "lookup"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/event/AttendeesView;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/android/calendar/event/AttendeesView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    iput-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mInflater:Landroid/view/LayoutInflater;

    new-instance v2, Lcom/android/calendar/event/AttendeesView$PresenceQueryHandler;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/calendar/event/AttendeesView$PresenceQueryHandler;-><init>(Lcom/android/calendar/event/AttendeesView;Landroid/content/ContentResolver;)V

    iput-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mPresenceQueryHandler:Lcom/android/calendar/event/AttendeesView$PresenceQueryHandler;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mDefaultBadge:Landroid/graphics/drawable/Drawable;

    const v2, 0x7f0b0003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/event/AttendeesView;->mNoResponsePhotoAlpha:I

    const v2, 0x7f0b0004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/event/AttendeesView;->mDefaultPhotoAlpha:I

    const v2, 0x7f07000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mEntries:[Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mEntries:[Ljava/lang/CharSequence;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-direct {p0, v2}, Lcom/android/calendar/event/AttendeesView;->constructDividerView(Ljava/lang/CharSequence;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mDividerForYes:Landroid/view/View;

    iget-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mEntries:[Ljava/lang/CharSequence;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-direct {p0, v2}, Lcom/android/calendar/event/AttendeesView;->constructDividerView(Ljava/lang/CharSequence;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mDividerForNo:Landroid/view/View;

    iget-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mEntries:[Ljava/lang/CharSequence;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-direct {p0, v2}, Lcom/android/calendar/event/AttendeesView;->constructDividerView(Ljava/lang/CharSequence;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mDividerForMaybe:Landroid/view/View;

    iget-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mEntries:[Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-direct {p0, v2}, Lcom/android/calendar/event/AttendeesView;->constructDividerView(Ljava/lang/CharSequence;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mDividerForNoResponse:Landroid/view/View;

    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    new-instance v2, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v2, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    iput-object v2, p0, Lcom/android/calendar/event/AttendeesView;->mGrayscaleFilter:Landroid/graphics/ColorMatrixColorFilter;

    return-void
.end method

.method static synthetic access$000(Lcom/android/calendar/event/AttendeesView;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/calendar/event/AttendeesView;

    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/calendar/event/AttendeesView;Lcom/android/calendar/event/EditEventHelper$AttendeeItem;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/calendar/event/AttendeesView;
    .param p1    # Lcom/android/calendar/event/EditEventHelper$AttendeeItem;

    invoke-direct {p0, p1}, Lcom/android/calendar/event/AttendeesView;->updateAttendeeView(Lcom/android/calendar/event/EditEventHelper$AttendeeItem;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private addOneAttendee(Lcom/android/calendar/CalendarEventModel$Attendee;)V
    .locals 16
    .param p1    # Lcom/android/calendar/CalendarEventModel$Attendee;

    invoke-virtual/range {p0 .. p1}, Lcom/android/calendar/event/AttendeesView;->contains(Lcom/android/calendar/CalendarEventModel$Attendee;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v3, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/event/AttendeesView;->mDefaultBadge:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v1}, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;-><init>(Lcom/android/calendar/CalendarEventModel$Attendee;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p1

    iget v14, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mStatus:I

    const/4 v10, 0x0

    packed-switch v14, :pswitch_data_0

    :pswitch_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    if-nez v1, :cond_9

    const/4 v1, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/AttendeesView;->mNo:I

    if-nez v2, :cond_a

    const/4 v2, 0x0

    :goto_2
    add-int/2addr v2, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mMaybe:I

    if-nez v1, :cond_b

    const/4 v1, 0x0

    :goto_3
    add-int v13, v2, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/event/AttendeesView;->mDividerForNoResponse:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/AttendeesView;->mEntries:[Ljava/lang/CharSequence;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/event/AttendeesView;->mNoResponse:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v5}, Lcom/android/calendar/event/AttendeesView;->updateDividerViewLabel(Landroid/view/View;Ljava/lang/CharSequence;I)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mNoResponse:I

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/event/AttendeesView;->mDividerForNoResponse:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    const/4 v10, 0x1

    :cond_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mNoResponse:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/calendar/event/AttendeesView;->mNoResponse:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mNoResponse:I

    add-int v11, v13, v1

    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/calendar/event/AttendeesView;->constructAttendeeView(Lcom/android/calendar/event/EditEventHelper$AttendeeItem;)Landroid/view/View;

    move-result-object v15

    invoke-virtual {v15, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    if-nez v10, :cond_2

    add-int/lit8 v1, v11, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_2

    const v1, 0x7f10003a

    invoke-virtual {v12, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v9, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mIdentity:Ljava/lang/String;

    if-eqz v1, :cond_c

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mIdNamespace:Ljava/lang/String;

    if-eqz v1, :cond_c

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "mimetype=? AND data1=? AND data2=?"

    const/4 v1, 0x3

    new-array v7, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "vnd.android.cursor.item/identity"

    aput-object v2, v7, v1

    const/4 v1, 0x1

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mIdentity:Ljava/lang/String;

    aput-object v2, v7, v1

    const/4 v1, 0x2

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mIdNamespace:Ljava/lang/String;

    aput-object v2, v7, v1

    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/event/AttendeesView;->mPresenceQueryHandler:Lcom/android/calendar/event/AttendeesView$PresenceQueryHandler;

    iget v2, v3, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mUpdateCounts:I

    add-int/lit8 v2, v2, 0x1

    sget-object v5, Lcom/android/calendar/event/AttendeesView;->PROJECTION:[Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_1
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/event/AttendeesView;->mDividerForYes:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/AttendeesView;->mEntries:[Ljava/lang/CharSequence;

    const/4 v5, 0x1

    aget-object v2, v2, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v5}, Lcom/android/calendar/event/AttendeesView;->updateDividerViewLabel(Landroid/view/View;Ljava/lang/CharSequence;I)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    if-nez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/event/AttendeesView;->mDividerForYes:Landroid/view/View;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    const/4 v10, 0x1

    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    add-int/lit8 v11, v1, 0x0

    goto/16 :goto_4

    :pswitch_2
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    if-nez v1, :cond_5

    const/4 v13, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/event/AttendeesView;->mDividerForNo:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/AttendeesView;->mEntries:[Ljava/lang/CharSequence;

    const/4 v5, 0x3

    aget-object v2, v2, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/event/AttendeesView;->mNo:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v5}, Lcom/android/calendar/event/AttendeesView;->updateDividerViewLabel(Landroid/view/View;Ljava/lang/CharSequence;I)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mNo:I

    if-nez v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/event/AttendeesView;->mDividerForNo:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    const/4 v10, 0x1

    :cond_4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mNo:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/calendar/event/AttendeesView;->mNo:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mNo:I

    add-int v11, v13, v1

    goto/16 :goto_4

    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    add-int/lit8 v13, v1, 0x1

    goto :goto_6

    :pswitch_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    if-nez v1, :cond_7

    const/4 v1, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/AttendeesView;->mNo:I

    if-nez v2, :cond_8

    const/4 v2, 0x0

    :goto_8
    add-int v13, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/event/AttendeesView;->mDividerForMaybe:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/AttendeesView;->mEntries:[Ljava/lang/CharSequence;

    const/4 v5, 0x2

    aget-object v2, v2, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/event/AttendeesView;->mMaybe:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v5}, Lcom/android/calendar/event/AttendeesView;->updateDividerViewLabel(Landroid/view/View;Ljava/lang/CharSequence;I)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mMaybe:I

    if-nez v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/event/AttendeesView;->mDividerForMaybe:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    const/4 v10, 0x1

    :cond_6
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mMaybe:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/calendar/event/AttendeesView;->mMaybe:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mMaybe:I

    add-int v11, v13, v1

    goto/16 :goto_4

    :cond_7
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_8
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/AttendeesView;->mNo:I

    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_9
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/AttendeesView;->mNo:I

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_b
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/event/AttendeesView;->mMaybe:I

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    :cond_c
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private constructAttendeeView(Lcom/android/calendar/event/EditEventHelper$AttendeeItem;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/android/calendar/event/EditEventHelper$AttendeeItem;

    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040018

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mView:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/calendar/event/AttendeesView;->updateAttendeeView(Lcom/android/calendar/event/EditEventHelper$AttendeeItem;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private constructDividerView(Ljava/lang/CharSequence;)Landroid/view/View;
    .locals 4
    .param p1    # Ljava/lang/CharSequence;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/calendar/event/AttendeesView;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f040028

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    return-object v0
.end method

.method private updateAttendeeView(Lcom/android/calendar/event/EditEventHelper$AttendeeItem;)Landroid/view/View;
    .locals 8
    .param p1    # Lcom/android/calendar/event/EditEventHelper$AttendeeItem;

    iget-object v0, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mAttendee:Lcom/android/calendar/CalendarEventModel$Attendee;

    iget-object v5, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mView:Landroid/view/View;

    const v6, 0x7f100037

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v6, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    :goto_0
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mRemoved:Z

    if-eqz v6, :cond_3

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v6

    or-int/lit8 v6, v6, 0x10

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setPaintFlags(I)V

    :goto_1
    const v6, 0x7f100038

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v6, 0x0

    :goto_2
    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v3, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-boolean v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mRemoved:Z

    if-eqz v6, :cond_5

    const v6, 0x7f020038

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v6, p0, Lcom/android/calendar/event/AttendeesView;->mContext:Landroid/content/Context;

    const v7, 0x7f0c00d3

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_3
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v6, 0x7f100039

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/QuickContactBadge;

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/android/calendar/event/AttendeesView;->mRecycledPhotos:Ljava/util/HashMap;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/calendar/event/AttendeesView;->mRecycledPhotos:Ljava/util/HashMap;

    iget-object v7, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mAttendee:Lcom/android/calendar/CalendarEventModel$Attendee;

    iget-object v7, v7, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    :cond_0
    if-eqz v1, :cond_1

    iput-object v1, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mBadge:Landroid/graphics/drawable/Drawable;

    :cond_1
    iget-object v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mBadge:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mAttendee:Lcom/android/calendar/CalendarEventModel$Attendee;

    iget v6, v6, Lcom/android/calendar/CalendarEventModel$Attendee;->mStatus:I

    if-nez v6, :cond_6

    iget-object v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mBadge:Landroid/graphics/drawable/Drawable;

    iget v7, p0, Lcom/android/calendar/event/AttendeesView;->mNoResponsePhotoAlpha:I

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :goto_4
    iget-object v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mAttendee:Lcom/android/calendar/CalendarEventModel$Attendee;

    iget v6, v6, Lcom/android/calendar/CalendarEventModel$Attendee;->mStatus:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_7

    iget-object v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mBadge:Landroid/graphics/drawable/Drawable;

    iget-object v7, p0, Lcom/android/calendar/event/AttendeesView;->mGrayscaleFilter:Landroid/graphics/ColorMatrixColorFilter;

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :goto_5
    iget-object v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mContactLookupUri:Landroid/net/Uri;

    if-eqz v6, :cond_8

    iget-object v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mContactLookupUri:Landroid/net/Uri;

    invoke-virtual {v2, v6}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    :goto_6
    const/16 v6, 0x3c

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMaxHeight(I)V

    return-object v5

    :cond_2
    iget-object v6, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mName:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v6

    and-int/lit8 v6, v6, -0x11

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto/16 :goto_1

    :cond_4
    const/16 v6, 0x8

    goto :goto_2

    :cond_5
    const v6, 0x7f020040

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v6, p0, Lcom/android/calendar/event/AttendeesView;->mContext:Landroid/content/Context;

    const v7, 0x7f0c00cb

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_6
    iget-object v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mBadge:Landroid/graphics/drawable/Drawable;

    iget v7, p0, Lcom/android/calendar/event/AttendeesView;->mDefaultPhotoAlpha:I

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_4

    :cond_7
    iget-object v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mBadge:Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_5

    :cond_8
    iget-object v6, p1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mAttendee:Lcom/android/calendar/CalendarEventModel$Attendee;

    iget-object v6, v6, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-virtual {v2, v6, v7}, Landroid/widget/QuickContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    goto :goto_6
.end method

.method private updateDividerViewLabel(Landroid/view/View;Ljava/lang/CharSequence;I)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # I

    if-gtz p3, :cond_0

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    check-cast p1, Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public addAttendees(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    iget-object v4, p0, Lcom/android/calendar/event/AttendeesView;->mValidator:Lcom/android/common/Rfc822Validator;

    invoke-static {p1, v4}, Lcom/android/calendar/event/EditEventHelper;->getAddressesFromList(Ljava/lang/String;Lcom/android/common/Rfc822Validator;)Ljava/util/LinkedHashSet;

    move-result-object v1

    monitor-enter p0

    :try_start_0
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/util/Rfc822Token;

    new-instance v2, Lcom/android/calendar/CalendarEventModel$Attendee;

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/android/calendar/CalendarEventModel$Attendee;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v2, Lcom/android/calendar/CalendarEventModel$Attendee;->mName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v2, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    iput-object v4, v2, Lcom/android/calendar/CalendarEventModel$Attendee;->mName:Ljava/lang/String;

    :cond_0
    invoke-direct {p0, v2}, Lcom/android/calendar/event/AttendeesView;->addOneAttendee(Lcom/android/calendar/CalendarEventModel$Attendee;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public addAttendees(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$Attendee;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    invoke-direct {p0, v0}, Lcom/android/calendar/event/AttendeesView;->addOneAttendee(Lcom/android/calendar/CalendarEventModel$Attendee;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public addAttendees(Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/calendar/CalendarEventModel$Attendee;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    invoke-direct {p0, v0}, Lcom/android/calendar/event/AttendeesView;->addOneAttendee(Lcom/android/calendar/CalendarEventModel$Attendee;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public clearAttendees()V
    .locals 8

    const/4 v7, 0x0

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/android/calendar/event/AttendeesView;->mRecycledPhotos:Ljava/util/HashMap;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v4, v3, Landroid/widget/TextView;

    if-eqz v4, :cond_0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;

    iget-object v4, p0, Lcom/android/calendar/event/AttendeesView;->mRecycledPhotos:Ljava/util/HashMap;

    iget-object v5, v0, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mAttendee:Lcom/android/calendar/CalendarEventModel$Attendee;

    iget-object v5, v5, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    iget-object v6, v0, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mBadge:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    iput v7, p0, Lcom/android/calendar/event/AttendeesView;->mYes:I

    iput v7, p0, Lcom/android/calendar/event/AttendeesView;->mNo:I

    iput v7, p0, Lcom/android/calendar/event/AttendeesView;->mMaybe:I

    iput v7, p0, Lcom/android/calendar/event/AttendeesView;->mNoResponse:I

    return-void
.end method

.method public contains(Lcom/android/calendar/CalendarEventModel$Attendee;)Z
    .locals 6
    .param p1    # Lcom/android/calendar/CalendarEventModel$Attendee;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v4, v3, Landroid/widget/TextView;

    if-eqz v4, :cond_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;

    iget-object v4, p1, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    iget-object v5, v0, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mAttendee:Lcom/android/calendar/CalendarEventModel$Attendee;

    iget-object v5, v5, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getItem(I)Lcom/android/calendar/CalendarEventModel$Attendee;
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;

    iget-object v1, v1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mAttendee:Lcom/android/calendar/CalendarEventModel$Attendee;

    goto :goto_0
.end method

.method public isMarkAsRemoved(I)Z
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;

    iget-boolean v1, v1, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mRemoved:Z

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;

    iget-boolean v1, v0, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mRemoved:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, v0, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mRemoved:Z

    invoke-direct {p0, v0}, Lcom/android/calendar/event/AttendeesView;->updateAttendeeView(Lcom/android/calendar/event/EditEventHelper$AttendeeItem;)Landroid/view/View;

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 6
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_2

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v5, 0x7f100038

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const/16 v4, 0x8

    goto :goto_0

    :cond_2
    return-void
.end method

.method public setRfc822Validator(Lcom/android/common/Rfc822Validator;)V
    .locals 0
    .param p1    # Lcom/android/common/Rfc822Validator;

    iput-object p1, p0, Lcom/android/calendar/event/AttendeesView;->mValidator:Lcom/android/common/Rfc822Validator;

    return-void
.end method
