.class Lcom/android/calendar/event/EditEventFragment$QueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "EditEventFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/event/EditEventFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/calendar/event/EditEventFragment;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/EditEventFragment;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2    # Landroid/content/ContentResolver;

    iput-object p1, p0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 43
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v4}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v26

    if-eqz v26, :cond_1

    invoke-virtual/range {v26 .. v26}, Landroid/app/Activity;->isFinishing()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-wide v0, v4, Lcom/android/calendar/CalendarEventModel;->mStart:J

    move-wide/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-wide v0, v4, Lcom/android/calendar/CalendarEventModel;->mEnd:J

    move-wide/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mView:Lcom/android/calendar/event/EditEventView;

    move-wide/from16 v0, v29

    move-wide/from16 v2, v32

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/android/calendar/event/EditEventView;->updateRepeatsSpinnerStatus(JJ)J

    goto :goto_0

    :pswitch_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_3

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-static {v4}, Lcom/android/calendar/event/EditEventFragment;->access$100(Lcom/android/calendar/event/EditEventFragment;)Lcom/android/calendar/event/EditEventFragment$Done;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/android/calendar/event/EditEventFragment$Done;->setDoneCode(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/calendar/event/EditEventFragment;->access$202(Lcom/android/calendar/event/EditEventFragment;Z)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-static {v4}, Lcom/android/calendar/event/EditEventFragment;->access$100(Lcom/android/calendar/event/EditEventFragment;)Lcom/android/calendar/event/EditEventFragment$Done;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/calendar/event/EditEventFragment$Done;->run()V

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    new-instance v5, Lcom/android/calendar/CalendarEventModel;

    invoke-direct {v5}, Lcom/android/calendar/CalendarEventModel;-><init>()V

    iput-object v5, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p3

    invoke-static {v4, v0}, Lcom/android/calendar/event/EditEventHelper;->setModelFromCursor(Lcom/android/calendar/CalendarEventModel;Landroid/database/Cursor;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p3

    invoke-static {v4, v0}, Lcom/android/calendar/event/EditEventHelper;->setModelFromCursor(Lcom/android/calendar/CalendarEventModel;Landroid/database/Cursor;)V

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-static {v5}, Lcom/android/calendar/event/EditEventFragment;->access$300(Lcom/android/calendar/event/EditEventFragment;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/calendar/CalendarEventModel;->mUri:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-static {v5}, Lcom/android/calendar/event/EditEventFragment;->access$300(Lcom/android/calendar/event/EditEventFragment;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/calendar/CalendarEventModel;->mUri:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-static {v5}, Lcom/android/calendar/event/EditEventFragment;->access$400(Lcom/android/calendar/event/EditEventFragment;)J

    move-result-wide v5

    iput-wide v5, v4, Lcom/android/calendar/CalendarEventModel;->mOriginalStart:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-static {v5}, Lcom/android/calendar/event/EditEventFragment;->access$500(Lcom/android/calendar/event/EditEventFragment;)J

    move-result-wide v5

    iput-wide v5, v4, Lcom/android/calendar/CalendarEventModel;->mOriginalEnd:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v5, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-static {v4}, Lcom/android/calendar/event/EditEventFragment;->access$400(Lcom/android/calendar/event/EditEventFragment;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    iget-wide v11, v4, Lcom/android/calendar/CalendarEventModel;->mStart:J

    cmp-long v4, v8, v11

    if-nez v4, :cond_4

    const/4 v4, 0x1

    :goto_2
    iput-boolean v4, v5, Lcom/android/calendar/CalendarEventModel;->mIsFirstEventInSeries:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-static {v5}, Lcom/android/calendar/event/EditEventFragment;->access$400(Lcom/android/calendar/event/EditEventFragment;)J

    move-result-wide v5

    iput-wide v5, v4, Lcom/android/calendar/CalendarEventModel;->mStart:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-static {v5}, Lcom/android/calendar/event/EditEventFragment;->access$500(Lcom/android/calendar/event/EditEventFragment;)J

    move-result-wide v5

    iput-wide v5, v4, Lcom/android/calendar/CalendarEventModel;->mEnd:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-wide v0, v4, Lcom/android/calendar/CalendarEventModel;->mId:J

    move-wide/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-boolean v4, v4, Lcom/android/calendar/CalendarEventModel;->mHasAttendeeData:Z

    if-eqz v4, :cond_5

    const-wide/16 v4, -0x1

    cmp-long v4, v34, v4

    if-eqz v4, :cond_5

    sget-object v7, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v10, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v10, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mHandler:Lcom/android/calendar/event/EditEventFragment$QueryHandler;

    const/4 v5, 0x2

    const/4 v6, 0x0

    sget-object v8, Lcom/android/calendar/event/EditEventHelper;->ATTENDEES_PROJECTION:[Ljava/lang/String;

    const-string v9, "event_id=? AND attendeeEmail IS NOT NULL"

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-boolean v4, v4, Lcom/android/calendar/CalendarEventModel;->mHasAlarm:Z

    if-eqz v4, :cond_6

    sget-object v14, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/4 v4, 0x0

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v17, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v11, v4, Lcom/android/calendar/event/EditEventFragment;->mHandler:Lcom/android/calendar/event/EditEventFragment$QueryHandler;

    const/4 v12, 0x4

    const/4 v13, 0x0

    sget-object v15, Lcom/android/calendar/event/EditEventHelper;->REMINDERS_PROJECTION:[Ljava/lang/String;

    const-string v16, "event_id=?"

    const/16 v18, 0x0

    invoke-virtual/range {v11 .. v18}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v5, v5, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-wide v5, v5, Lcom/android/calendar/CalendarEventModel;->mCalendarId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v24, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v0, v4, Lcom/android/calendar/event/EditEventFragment;->mHandler:Lcom/android/calendar/event/EditEventFragment$QueryHandler;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    const/16 v20, 0x0

    sget-object v21, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v22, Lcom/android/calendar/event/EditEventHelper;->CALENDARS_PROJECTION:[Ljava/lang/String;

    const-string v23, "_id=?"

    const/16 v25, 0x0

    invoke-virtual/range {v18 .. v25}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/android/calendar/event/EditEventFragment;->access$600(Lcom/android/calendar/event/EditEventFragment;I)V

    goto/16 :goto_1

    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/android/calendar/event/EditEventFragment;->access$600(Lcom/android/calendar/event/EditEventFragment;I)V

    goto :goto_3

    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/android/calendar/event/EditEventFragment;->access$600(Lcom/android/calendar/event/EditEventFragment;I)V

    goto :goto_4

    :goto_5
    :pswitch_2
    :try_start_0
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_b

    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v39

    const/4 v4, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    const/4 v4, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v42

    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v41

    const/4 v4, 0x2

    move/from16 v0, v41

    if-ne v0, v4, :cond_8

    if-eqz v31, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, v31

    iput-object v0, v4, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v5, v5, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-object v5, v5, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, v4, Lcom/android/calendar/CalendarEventModel;->mIsOrganizer:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, v31

    iput-object v0, v4, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v5, v5, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    iget-object v5, v5, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, v4, Lcom/android/calendar/CalendarEventModel;->mIsOrganizer:Z

    :cond_7
    invoke-static/range {v39 .. v39}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v5, v5, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-object v5, v5, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    iput-object v5, v4, Lcom/android/calendar/CalendarEventModel;->mOrganizerDisplayName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v5, v5, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    iget-object v5, v5, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    iput-object v5, v4, Lcom/android/calendar/CalendarEventModel;->mOrganizerDisplayName:Ljava/lang/String;

    :cond_8
    :goto_6
    if-eqz v31, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-object v4, v4, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-object v4, v4, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move/from16 v0, v28

    iput v0, v4, Lcom/android/calendar/CalendarEventModel;->mOwnerAttendeeId:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move/from16 v0, v42

    iput v0, v4, Lcom/android/calendar/CalendarEventModel;->mSelfAttendeeStatus:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    move/from16 v0, v28

    iput v0, v4, Lcom/android/calendar/CalendarEventModel;->mOwnerAttendeeId:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    move/from16 v0, v42

    iput v0, v4, Lcom/android/calendar/CalendarEventModel;->mSelfAttendeeStatus:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_5

    :catchall_0
    move-exception v4

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_9
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, v39

    iput-object v0, v4, Lcom/android/calendar/CalendarEventModel;->mOrganizerDisplayName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, v39

    iput-object v0, v4, Lcom/android/calendar/CalendarEventModel;->mOrganizerDisplayName:Ljava/lang/String;

    goto :goto_6

    :cond_a
    new-instance v27, Lcom/android/calendar/CalendarEventModel$Attendee;

    move-object/from16 v0, v27

    move-object/from16 v1, v39

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/CalendarEventModel$Attendee;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v0, v42

    move-object/from16 v1, v27

    iput v0, v1, Lcom/android/calendar/CalendarEventModel$Attendee;->mStatus:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Lcom/android/calendar/CalendarEventModel;->addAttendee(Lcom/android/calendar/CalendarEventModel$Attendee;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Lcom/android/calendar/CalendarEventModel;->addAttendee(Lcom/android/calendar/CalendarEventModel$Attendee;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_5

    :cond_b
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/android/calendar/event/EditEventFragment;->access$600(Lcom/android/calendar/event/EditEventFragment;I)V

    goto/16 :goto_1

    :goto_7
    :pswitch_3
    :try_start_2
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v38

    const/4 v4, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v37

    move/from16 v0, v38

    move/from16 v1, v37

    invoke-static {v0, v1}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->valueOf(II)Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-result-object v40

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-object v4, v4, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    move-object/from16 v0, v40

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    iget-object v4, v4, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    move-object/from16 v0, v40

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_7

    :catchall_1
    move-exception v4

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_c
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-object v4, v4, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    iget-object v4, v4, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/android/calendar/event/EditEventFragment;->access$600(Lcom/android/calendar/event/EditEventFragment;I)V

    goto/16 :goto_1

    :pswitch_4
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-static {v4}, Lcom/android/calendar/event/EditEventFragment;->access$300(Lcom/android/calendar/event/EditEventFragment;)Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    iget-wide v4, v4, Lcom/android/calendar/CalendarEventModel;->mCalendarId:J

    const-wide/16 v8, -0x1

    cmp-long v4, v4, v8

    if-nez v4, :cond_f

    :cond_d
    invoke-static/range {p3 .. p3}, Lcom/android/calendar/Utils;->matrixCursorFromCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v5, v4, Lcom/android/calendar/event/EditEventFragment;->mView:Lcom/android/calendar/event/EditEventView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v4}, Landroid/app/Fragment;->isAdded()Z

    move-result v4

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v4}, Landroid/app/Fragment;->isResumed()Z

    move-result v4

    if-eqz v4, :cond_e

    const/4 v4, 0x1

    :goto_8
    move-object/from16 v0, v36

    invoke-virtual {v5, v0, v4}, Lcom/android/calendar/event/EditEventView;->setCalendarsCursor(Landroid/database/Cursor;Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_9
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    const/16 v5, 0x8

    invoke-static {v4, v5}, Lcom/android/calendar/event/EditEventFragment;->access$600(Lcom/android/calendar/event/EditEventFragment;I)V

    goto/16 :goto_1

    :cond_e
    const/4 v4, 0x0

    goto :goto_8

    :cond_f
    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p3

    invoke-static {v4, v0}, Lcom/android/calendar/event/EditEventHelper;->setModelFromCalendarCursor(Lcom/android/calendar/CalendarEventModel;Landroid/database/Cursor;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/EditEventFragment$QueryHandler;->this$0:Lcom/android/calendar/event/EditEventFragment;

    iget-object v4, v4, Lcom/android/calendar/event/EditEventFragment;->mOriginalModel:Lcom/android/calendar/CalendarEventModel;

    move-object/from16 v0, p3

    invoke-static {v4, v0}, Lcom/android/calendar/event/EditEventHelper;->setModelFromCalendarCursor(Lcom/android/calendar/CalendarEventModel;Landroid/database/Cursor;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_9

    :catchall_2
    move-exception v4

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    throw v4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
