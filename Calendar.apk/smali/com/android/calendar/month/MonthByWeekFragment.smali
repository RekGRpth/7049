.class public Lcom/android/calendar/month/MonthByWeekFragment;
.super Lcom/android/calendar/month/SimpleDayPickerFragment;
.source "MonthByWeekFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/android/calendar/CalendarController$EventHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/calendar/month/SimpleDayPickerFragment;",
        "Lcom/android/calendar/CalendarController$EventHandler;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AbsListView$OnScrollListener;",
        "Landroid/view/View$OnTouchListener;"
    }
.end annotation


# static fields
.field private static final INSTANCES_SORT_ORDER:Ljava/lang/String; = "startDay,startMinute,title"

.field private static final LOADER_DELAY:I = 0xc8

.field private static final LOADER_THROTTLE_DELAY:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "MonthFragment"

.field private static final WEEKS_BUFFER:I = 0x1

.field private static final WHERE_CALENDARS_VISIBLE:Ljava/lang/String; = "visible=1"

.field protected static mShowDetailsInMonth:Z


# instance fields
.field private final mDesiredDay:Landroid/text/format/Time;

.field private mEventUri:Landroid/net/Uri;

.field private mEventsLoadingDelay:I

.field protected mFirstLoadedJulianDay:I

.field protected mHideDeclined:Z

.field private mIsDetached:Z

.field protected mIsMiniMonth:Z

.field protected mLastLoadedJulianDay:I

.field private mLoader:Landroid/content/CursorLoader;

.field mLoadingRunnable:Ljava/lang/Runnable;

.field protected mMinimumTwoMonthFlingVelocity:F

.field private volatile mShouldLoad:Z

.field private mShowCalendarControls:Z

.field private final mTZUpdater:Ljava/lang/Runnable;

.field private final mUpdateLoader:Ljava/lang/Runnable;

.field private mUserScrolled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/MonthByWeekFragment;->mShowDetailsInMonth:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/month/MonthByWeekFragment;-><init>(JZ)V

    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 1
    .param p1    # J
    .param p3    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/calendar/month/SimpleDayPickerFragment;-><init>(J)V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mDesiredDay:Landroid/text/format/Time;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mShouldLoad:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mUserScrolled:Z

    new-instance v0, Lcom/android/calendar/month/MonthByWeekFragment$1;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/MonthByWeekFragment$1;-><init>(Lcom/android/calendar/month/MonthByWeekFragment;)V

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mTZUpdater:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/calendar/month/MonthByWeekFragment$2;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/MonthByWeekFragment$2;-><init>(Lcom/android/calendar/month/MonthByWeekFragment;)V

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mUpdateLoader:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/calendar/month/MonthByWeekFragment$3;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/MonthByWeekFragment$3;-><init>(Lcom/android/calendar/month/MonthByWeekFragment;)V

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoadingRunnable:Ljava/lang/Runnable;

    iput-boolean p3, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mIsMiniMonth:Z

    return-void
.end method

.method static synthetic access$000(Lcom/android/calendar/month/MonthByWeekFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/calendar/month/MonthByWeekFragment;

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mTZUpdater:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/calendar/month/MonthByWeekFragment;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/month/MonthByWeekFragment;

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mShouldLoad:Z

    return v0
.end method

.method static synthetic access$200(Lcom/android/calendar/month/MonthByWeekFragment;)Landroid/content/CursorLoader;
    .locals 1
    .param p0    # Lcom/android/calendar/month/MonthByWeekFragment;

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoader:Landroid/content/CursorLoader;

    return-object v0
.end method

.method static synthetic access$202(Lcom/android/calendar/month/MonthByWeekFragment;Landroid/content/CursorLoader;)Landroid/content/CursorLoader;
    .locals 0
    .param p0    # Lcom/android/calendar/month/MonthByWeekFragment;
    .param p1    # Landroid/content/CursorLoader;

    iput-object p1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoader:Landroid/content/CursorLoader;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/calendar/month/MonthByWeekFragment;)V
    .locals 0
    .param p0    # Lcom/android/calendar/month/MonthByWeekFragment;

    invoke-direct {p0}, Lcom/android/calendar/month/MonthByWeekFragment;->stopLoader()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/calendar/month/MonthByWeekFragment;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/calendar/month/MonthByWeekFragment;

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/calendar/month/MonthByWeekFragment;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0    # Lcom/android/calendar/month/MonthByWeekFragment;
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$500(Lcom/android/calendar/month/MonthByWeekFragment;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/calendar/month/MonthByWeekFragment;

    invoke-direct {p0}, Lcom/android/calendar/month/MonthByWeekFragment;->updateUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/calendar/month/MonthByWeekFragment;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/month/MonthByWeekFragment;

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mIsDetached:Z

    return v0
.end method

.method private stopLoader()V
    .locals 3

    iget-object v1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mUpdateLoader:Ljava/lang/Runnable;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mUpdateLoader:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoader:Landroid/content/CursorLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoader:Landroid/content/CursorLoader;

    invoke-virtual {v0}, Landroid/content/Loader;->stopLoading()V

    const-string v0, "MonthFragment"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MonthFragment"

    const-string v2, "Stopped loader from loading"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateLoadedDays()V
    .locals 8

    iget-object v6, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x2

    if-gt v5, v6, :cond_0

    :goto_0
    return-void

    :cond_0
    add-int/lit8 v6, v5, -0x2

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    add-int/lit8 v6, v5, -0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v6, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mTempTime:Landroid/text/format/Time;

    invoke-virtual {v6, v0, v1}, Landroid/text/format/Time;->set(J)V

    iget-object v6, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mTempTime:Landroid/text/format/Time;

    iget-wide v6, v6, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v6

    iput v6, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mFirstLoadedJulianDay:I

    iget-object v6, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mTempTime:Landroid/text/format/Time;

    invoke-virtual {v6, v2, v3}, Landroid/text/format/Time;->set(J)V

    iget-object v6, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mTempTime:Landroid/text/format/Time;

    iget-wide v6, v6, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v6

    iput v6, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLastLoadedJulianDay:I

    goto :goto_0
.end method

.method private updateUri()Landroid/net/Uri;
    .locals 10

    const/4 v9, 0x1

    iget-object v7, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mListView:Landroid/widget/ListView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/month/SimpleWeekView;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/calendar/month/SimpleWeekView;->getFirstJulianDay()I

    move-result v4

    iput v4, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mFirstLoadedJulianDay:I

    :cond_0
    iget-object v7, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mTempTime:Landroid/text/format/Time;

    iget v8, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mFirstLoadedJulianDay:I

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Landroid/text/format/Time;->setJulianDay(I)J

    iget-object v7, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mTempTime:Landroid/text/format/Time;

    invoke-virtual {v7, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v5

    iget v7, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mFirstLoadedJulianDay:I

    iget v8, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mNumWeeks:I

    add-int/lit8 v8, v8, 0x2

    mul-int/lit8 v8, v8, 0x7

    add-int/2addr v7, v8

    iput v7, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLastLoadedJulianDay:I

    iget-object v7, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mTempTime:Landroid/text/format/Time;

    iget v8, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLastLoadedJulianDay:I

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v7, v8}, Landroid/text/format/Time;->setJulianDay(I)J

    iget-object v7, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mTempTime:Landroid/text/format/Time;

    invoke-virtual {v7, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    sget-object v7, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, v5, v6}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    return-object v7
.end method


# virtual methods
.method public doResumeUpdates()V
    .locals 7

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mFirstDayOfWeek:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/Utils;->getShowWeekNumber(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mShowWeekNumber:Z

    iget-boolean v6, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mHideDeclined:Z

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/Utils;->getHideDeclinedEvents(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mHideDeclined:Z

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mHideDeclined:Z

    if-eq v6, v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoader:Landroid/content/CursorLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoader:Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthByWeekFragment;->updateWhere()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/CursorLoader;->setSelection(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/Utils;->getDaysPerWeek(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mDaysPerWeek:I

    invoke-virtual {p0}, Lcom/android/calendar/month/SimpleDayPickerFragment;->updateHeader()V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    iget-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/SimpleWeeksAdapter;->setSelectedDay(Landroid/text/format/Time;)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mTZUpdater:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mTodayUpdater:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    move-object v0, p0

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/month/SimpleDayPickerFragment;->goTo(JZZZ)Z

    return-void
.end method

.method public eventsChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoader:Landroid/content/CursorLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoader:Landroid/content/CursorLoader;

    invoke-virtual {v0}, Landroid/content/Loader;->forceLoad()V

    :cond_0
    return-void
.end method

.method public getSupportedEventTypes()J
    .locals 2

    const-wide/16 v0, 0xa0

    return-wide v0
.end method

.method public handleEvent(Lcom/android/calendar/CalendarController$EventInfo;)V
    .locals 14
    .param p1    # Lcom/android/calendar/CalendarController$EventInfo;

    const-wide/16 v8, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-wide v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v10, 0x20

    cmp-long v0, v0, v10

    if-nez v0, :cond_4

    const/4 v3, 0x1

    iget v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mDaysPerWeek:I

    iget v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mNumWeeks:I

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x2

    iget-object v1, p1, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    iget-object v10, p1, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    iget-wide v10, v10, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v1, v2, v10, v11}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mFirstVisibleDay:Landroid/text/format/Time;

    invoke-virtual {v2, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    iget-object v2, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mFirstVisibleDay:Landroid/text/format/Time;

    iget-wide v12, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v10, v11, v12, v13}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mDaysPerWeek:I

    iget v10, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mNumWeeks:I

    mul-int/2addr v2, v10

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v3, 0x0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mDesiredDay:Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mDesiredDay:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    iget-wide v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->extraLong:J

    const-wide/16 v10, 0x8

    and-long/2addr v0, v10

    cmp-long v0, v0, v8

    if-eqz v0, :cond_2

    move v6, v4

    :goto_0
    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/month/SimpleDayPickerFragment;->goTo(JZZZ)Z

    move-result v7

    if-eqz v6, :cond_1

    iget-object v2, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/android/calendar/month/MonthByWeekFragment$4;

    invoke-direct {v4, p0}, Lcom/android/calendar/month/MonthByWeekFragment$4;-><init>(Lcom/android/calendar/month/MonthByWeekFragment;)V

    if-eqz v7, :cond_3

    const-wide/16 v0, 0x1f4

    :goto_1
    invoke-virtual {v2, v4, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_2
    return-void

    :cond_2
    move v6, v5

    goto :goto_0

    :cond_3
    move-wide v0, v8

    goto :goto_1

    :cond_4
    iget-wide v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v4, 0x80

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthByWeekFragment;->eventsChanged()V

    goto :goto_2
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/calendar/month/SimpleDayPickerFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mIsMiniMonth:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080028

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mShowCalendarControls:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoadingRunnable:Ljava/lang/Runnable;

    iget v2, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventsLoadingDelay:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    iget-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/SimpleWeeksAdapter;->setListView(Landroid/widget/ListView;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    move-result-object v0

    check-cast v0, Landroid/content/CursorLoader;

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoader:Landroid/content/CursorLoader;

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/android/calendar/month/SimpleDayPickerFragment;->onAttach(Landroid/app/Activity;)V

    iget-object v2, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mTZUpdater:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    iget-object v2, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    iget-object v3, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v2, v3}, Lcom/android/calendar/month/SimpleWeeksAdapter;->setSelectedDay(Landroid/text/format/Time;)V

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mIsDetached:Z

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mMinimumTwoMonthFlingVelocity:F

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f090000

    invoke-static {p1, v2}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mShowCalendarControls:Z

    iget-boolean v2, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mShowCalendarControls:Z

    if-eqz v2, :cond_1

    const v2, 0x7f0b000a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventsLoadingDelay:I

    :cond_1
    const v2, 0x7f090002

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    sput-boolean v2, Lcom/android/calendar/month/MonthByWeekFragment;->mShowDetailsInMonth:Z

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mIsMiniMonth:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v7, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mUpdateLoader:Ljava/lang/Runnable;

    monitor-enter v7

    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    iget-wide v5, v3, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v1, v2, v5, v6}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    iget v2, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mNumWeeks:I

    mul-int/lit8 v2, v2, 0x7

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mFirstLoadedJulianDay:I

    invoke-direct {p0}, Lcom/android/calendar/month/MonthByWeekFragment;->updateUri()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthByWeekFragment;->updateWhere()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventUri:Landroid/net/Uri;

    sget-object v3, Lcom/android/calendar/Event;->EVENT_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "startDay,startMinute,title"

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/content/AsyncTaskLoader;->setUpdateThrottle(J)V

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "MonthFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MonthFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Returning new loader with uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mIsMiniMonth:Z

    if-eqz v1, :cond_0

    const v1, 0x7f04002e

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    const v1, 0x7f1000a3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mDayNamesHeader:Landroid/view/ViewGroup;

    return-object v0

    :cond_0
    const v1, 0x7f04002a

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDetach()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mIsDetached:Z

    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mShowCalendarControls:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLoadingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 8
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    iget-object v4, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mUpdateLoader:Ljava/lang/Runnable;

    monitor-enter v4

    :try_start_0
    const-string v3, "MonthFragment"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "MonthFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " cursor entries for uri "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v0, p1

    check-cast v0, Landroid/content/CursorLoader;

    move-object v1, v0

    iget-object v3, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventUri:Landroid/net/Uri;

    if-nez v3, :cond_1

    invoke-virtual {v1}, Landroid/content/CursorLoader;->getUri()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventUri:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/android/calendar/month/MonthByWeekFragment;->updateLoadedDays()V

    :cond_1
    invoke-virtual {v1}, Landroid/content/CursorLoader;->getUri()Landroid/net/Uri;

    move-result-object v3

    iget-object v5, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mEventUri:Landroid/net/Uri;

    invoke-virtual {v3, v5}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    move-result v3

    if-eqz v3, :cond_2

    monitor-exit v4

    :goto_0
    return-void

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mContext:Landroid/content/Context;

    iget v5, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mFirstLoadedJulianDay:I

    iget v6, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLastLoadedJulianDay:I

    invoke-static {v2, p2, v3, v5, v6}, Lcom/android/calendar/Event;->buildEventsFromCursor(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;II)V

    iget-object v3, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    check-cast v3, Lcom/android/calendar/month/MonthByWeekAdapter;

    iget v5, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mFirstLoadedJulianDay:I

    iget v6, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mLastLoadedJulianDay:I

    iget v7, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mFirstLoadedJulianDay:I

    sub-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v3, v5, v6, v2}, Lcom/android/calendar/month/MonthByWeekAdapter;->setEvents(IILjava/util/ArrayList;)V

    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/calendar/month/MonthByWeekFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 6
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    const/4 v5, 0x1

    iget-object v1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mUpdateLoader:Ljava/lang/Runnable;

    monitor-enter v1

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mShouldLoad:Z

    invoke-direct {p0}, Lcom/android/calendar/month/MonthByWeekFragment;->stopLoader()V

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mDesiredDay:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p2, v5, :cond_0

    iput-boolean v5, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mUserScrolled:Z

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mScrollStateChangedRunnable:Lcom/android/calendar/month/SimpleDayPickerFragment$ScrollStateRunnable;

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/month/SimpleDayPickerFragment$ScrollStateRunnable;->doScrollStateChange(Landroid/widget/AbsListView;I)V

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mUpdateLoader:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mShouldLoad:Z

    iget-object v0, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mUpdateLoader:Ljava/lang/Runnable;

    const-wide/16 v3, 0xc8

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mDesiredDay:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    const/4 v0, 0x0

    return v0
.end method

.method protected setMonthDisplayed(Landroid/text/format/Time;Z)V
    .locals 20
    .param p1    # Landroid/text/format/Time;
    .param p2    # Z

    invoke-super/range {p0 .. p2}, Lcom/android/calendar/month/SimpleDayPickerFragment;->setMonthDisplayed(Landroid/text/format/Time;Z)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/MonthByWeekFragment;->mIsMiniMonth:Z

    if-nez v2, :cond_1

    const/16 v19, 0x0

    move-object/from16 v0, p1

    iget v2, v0, Landroid/text/format/Time;->year:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/MonthByWeekFragment;->mDesiredDay:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->year:I

    if-ne v2, v3, :cond_2

    move-object/from16 v0, p1

    iget v2, v0, Landroid/text/format/Time;->month:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/MonthByWeekFragment;->mDesiredDay:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->month:I

    if-ne v2, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/MonthByWeekFragment;->mDesiredDay:Landroid/text/format/Time;

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/MonthByWeekFragment;->mDesiredDay:Landroid/text/format/Time;

    invoke-virtual {v2, v3}, Lcom/android/calendar/month/SimpleWeeksAdapter;->setSelectedDay(Landroid/text/format/Time;)V

    const/16 v19, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/CalendarController;->getInstance(Landroid/content/Context;)Lcom/android/calendar/CalendarController;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->minute:I

    const/16 v3, 0x1e

    if-lt v2, v3, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    const/16 v3, 0x1e

    iput v3, v2, Landroid/text/format/Time;->minute:I

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v15

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getTime()J

    move-result-wide v2

    cmp-long v2, v15, v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/MonthByWeekFragment;->mUserScrolled:Z

    if-eqz v2, :cond_0

    if-eqz v19, :cond_4

    const-wide/16 v17, 0x0

    :goto_2
    add-long v2, v15, v17

    invoke-virtual {v1, v2, v3}, Lcom/android/calendar/CalendarController;->setTime(J)V

    :cond_0
    const-wide/16 v3, 0x400

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mTempTime:Landroid/text/format/Time;

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const-wide/16 v11, 0x34

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v2, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p1

    invoke-virtual/range {v1 .. v14}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    :cond_1
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/android/calendar/month/SimpleWeeksAdapter;->setSelectedDay(Landroid/text/format/Time;)V

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    const/4 v3, 0x0

    iput v3, v2, Landroid/text/format/Time;->minute:I

    goto :goto_1

    :cond_4
    const-wide/32 v2, 0x240c8400

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mNumWeeks:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x3

    div-long v17, v2, v4

    goto :goto_2
.end method

.method protected setUpAdapter()V
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mFirstDayOfWeek:I

    iget-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/Utils;->getShowWeekNumber(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mShowWeekNumber:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "num_weeks"

    iget v4, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mNumWeeks:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "week_numbers"

    iget-boolean v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mShowWeekNumber:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "week_start"

    iget v4, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mFirstDayOfWeek:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "mini_month"

    iget-boolean v4, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mIsMiniMonth:Z

    if-eqz v4, :cond_0

    move v3, v2

    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "selected_day"

    iget-object v3, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v3, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mSelectedDay:Landroid/text/format/Time;

    iget-wide v4, v4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "days_per_week"

    iget v2, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mDaysPerWeek:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    if-nez v1, :cond_2

    new-instance v1, Lcom/android/calendar/month/MonthByWeekAdapter;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/calendar/month/MonthByWeekAdapter;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    iput-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    iget-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    iget-object v2, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :goto_1
    iget-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mAdapter:Lcom/android/calendar/month/SimpleWeeksAdapter;

    invoke-virtual {v1, v0}, Lcom/android/calendar/month/SimpleWeeksAdapter;->updateParams(Ljava/util/HashMap;)V

    goto :goto_1
.end method

.method protected setUpHeader()V
    .locals 5

    const/4 v4, 0x7

    iget-boolean v1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mIsMiniMonth:Z

    if-eqz v1, :cond_1

    invoke-super {p0}, Lcom/android/calendar/month/SimpleDayPickerFragment;->setUpHeader()V

    :cond_0
    return-void

    :cond_1
    new-array v1, v4, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mDayLabels:[Ljava/lang/String;

    const/4 v0, 0x1

    :goto_0
    if-gt v0, v4, :cond_0

    iget-object v1, p0, Lcom/android/calendar/month/SimpleDayPickerFragment;->mDayLabels:[Ljava/lang/String;

    add-int/lit8 v2, v0, -0x1

    const/16 v3, 0x14

    invoke-static {v0, v3}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected updateWhere()Ljava/lang/String;
    .locals 3

    const-string v0, "visible=1"

    iget-boolean v1, p0, Lcom/android/calendar/month/MonthByWeekFragment;->mHideDeclined:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND selfAttendeeStatus!=2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method
