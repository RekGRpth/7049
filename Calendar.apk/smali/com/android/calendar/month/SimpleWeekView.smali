.class public Lcom/android/calendar/month/SimpleWeekView;
.super Landroid/view/View;
.source "SimpleWeekView.java"


# static fields
.field protected static DAY_SEPARATOR_WIDTH:I = 0x0

.field protected static final DEFAULT_FOCUS_MONTH:I = -0x1

.field protected static DEFAULT_HEIGHT:I = 0x0

.field protected static final DEFAULT_NUM_DAYS:I = 0x7

.field protected static final DEFAULT_SELECTED_DAY:I = -0x1

.field protected static final DEFAULT_SHOW_WK_NUM:I = 0x0

.field protected static final DEFAULT_WEEK_START:I = 0x0

.field protected static MINI_DAY_NUMBER_TEXT_SIZE:I = 0x0

.field protected static MINI_TODAY_NUMBER_TEXT_SIZE:I = 0x0

.field protected static MINI_TODAY_OUTLINE_WIDTH:I = 0x0

.field protected static MINI_WK_NUMBER_TEXT_SIZE:I = 0x0

.field protected static MIN_HEIGHT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MonthView"

.field public static final VIEW_PARAMS_FOCUS_MONTH:Ljava/lang/String; = "focus_month"

.field public static final VIEW_PARAMS_HEIGHT:Ljava/lang/String; = "height"

.field public static final VIEW_PARAMS_NUM_DAYS:Ljava/lang/String; = "num_days"

.field public static final VIEW_PARAMS_SELECTED_DAY:Ljava/lang/String; = "selected_day"

.field public static final VIEW_PARAMS_SHOW_WK_NUM:Ljava/lang/String; = "show_wk_num"

.field public static final VIEW_PARAMS_WEEK:Ljava/lang/String; = "week"

.field public static final VIEW_PARAMS_WEEK_START:Ljava/lang/String; = "week_start"

.field protected static WEEK_NUM_MARGIN_BOTTOM:I

.field protected static mScale:F


# instance fields
.field protected mBGColor:I

.field protected mDayNumbers:[Ljava/lang/String;

.field protected mDaySeparatorColor:I

.field protected mFirstJulianDay:I

.field protected mFirstMonth:I

.field protected mFocusDay:[Z

.field protected mFocusMonthColor:I

.field protected mHasSelectedDay:Z

.field protected mHasToday:Z

.field protected mHeight:I

.field mLastHoverTime:Landroid/text/format/Time;

.field protected mLastMonth:I

.field protected mMonthNumPaint:Landroid/graphics/Paint;

.field protected mNumCells:I

.field protected mNumDays:I

.field protected mOddMonth:[Z

.field protected mOtherMonthColor:I

.field protected mPadding:I

.field protected mSelectedDay:I

.field protected mSelectedDayLine:Landroid/graphics/drawable/Drawable;

.field protected mSelectedLeft:I

.field protected mSelectedRight:I

.field protected mSelectedWeekBGColor:I

.field protected mShowWeekNum:Z

.field protected mTimeZone:Ljava/lang/String;

.field protected mToday:I

.field protected mTodayOutlineColor:I

.field protected mWeek:I

.field protected mWeekNumColor:I

.field protected mWeekStart:I

.field protected mWidth:I

.field protected p:Landroid/graphics/Paint;

.field protected r:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x20

    sput v0, Lcom/android/calendar/month/SimpleWeekView;->DEFAULT_HEIGHT:I

    const/16 v0, 0xa

    sput v0, Lcom/android/calendar/month/SimpleWeekView;->MIN_HEIGHT:I

    const/4 v0, 0x1

    sput v0, Lcom/android/calendar/month/SimpleWeekView;->DAY_SEPARATOR_WIDTH:I

    const/16 v0, 0xe

    sput v0, Lcom/android/calendar/month/SimpleWeekView;->MINI_DAY_NUMBER_TEXT_SIZE:I

    const/16 v0, 0xc

    sput v0, Lcom/android/calendar/month/SimpleWeekView;->MINI_WK_NUMBER_TEXT_SIZE:I

    const/16 v0, 0x12

    sput v0, Lcom/android/calendar/month/SimpleWeekView;->MINI_TODAY_NUMBER_TEXT_SIZE:I

    const/4 v0, 0x2

    sput v0, Lcom/android/calendar/month/SimpleWeekView;->MINI_TODAY_OUTLINE_WIDTH:I

    const/4 v0, 0x4

    sput v0, Lcom/android/calendar/month/SimpleWeekView;->WEEK_NUM_MARGIN_BOTTOM:I

    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v2, -0x1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput v3, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    iput v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mFirstJulianDay:I

    iput v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mFirstMonth:I

    iput v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mLastMonth:I

    iput v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mWeek:I

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->DEFAULT_HEIGHT:I

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mHeight:I

    iput-boolean v3, p0, Lcom/android/calendar/month/SimpleWeekView;->mShowWeekNum:Z

    iput-boolean v3, p0, Lcom/android/calendar/month/SimpleWeekView;->mHasSelectedDay:Z

    iput-boolean v3, p0, Lcom/android/calendar/month/SimpleWeekView;->mHasToday:Z

    iput v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedDay:I

    iput v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mToday:I

    iput v3, p0, Lcom/android/calendar/month/SimpleWeekView;->mWeekStart:I

    const/4 v1, 0x7

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumDays:I

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumDays:I

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumCells:I

    iput v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedLeft:I

    iput v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedRight:I

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mTimeZone:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mLastHoverTime:Landroid/text/format/Time;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mBGColor:I

    const v1, 0x7f080026

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedWeekBGColor:I

    const v1, 0x7f080018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mFocusMonthColor:I

    const v1, 0x7f080024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mOtherMonthColor:I

    const v1, 0x7f080023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mDaySeparatorColor:I

    const v1, 0x7f080016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mTodayOutlineColor:I

    const v1, 0x7f08001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mWeekNumColor:I

    const v1, 0x7f020027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedDayLine:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    sput v1, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    const/high16 v2, 0x3f800000

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->DEFAULT_HEIGHT:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/SimpleWeekView;->DEFAULT_HEIGHT:I

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->MIN_HEIGHT:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/SimpleWeekView;->MIN_HEIGHT:I

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->MINI_DAY_NUMBER_TEXT_SIZE:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/SimpleWeekView;->MINI_DAY_NUMBER_TEXT_SIZE:I

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->MINI_TODAY_NUMBER_TEXT_SIZE:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/SimpleWeekView;->MINI_TODAY_NUMBER_TEXT_SIZE:I

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->MINI_TODAY_OUTLINE_WIDTH:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/SimpleWeekView;->MINI_TODAY_OUTLINE_WIDTH:I

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->WEEK_NUM_MARGIN_BOTTOM:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/SimpleWeekView;->WEEK_NUM_MARGIN_BOTTOM:I

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->DAY_SEPARATOR_WIDTH:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/SimpleWeekView;->DAY_SEPARATOR_WIDTH:I

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->MINI_WK_NUMBER_TEXT_SIZE:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/SimpleWeekView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/SimpleWeekView;->MINI_WK_NUMBER_TEXT_SIZE:I

    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/month/SimpleWeekView;->initView()V

    return-void
.end method


# virtual methods
.method protected drawBackground(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mHasSelectedDay:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedWeekBGColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    const/4 v1, 0x1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mHeight:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedLeft:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedRight:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mWidth:I

    iget v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method protected drawDaySeparators(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mHasSelectedDay:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    const/4 v1, 0x1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mHeight:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedLeft:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedRight:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->MINI_TODAY_OUTLINE_WIDTH:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mTodayOutlineColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->r:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mShowWeekNum:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mDaySeparatorColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->DAY_SEPARATOR_WIDTH:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mWidth:I

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumCells:I

    div-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    add-int v6, v0, v1

    int-to-float v1, v6

    const/4 v2, 0x0

    int-to-float v3, v6

    iget v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_1
    return-void
.end method

.method protected drawWeekNums(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1    # Landroid/graphics/Canvas;

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mHeight:I

    sget v7, Lcom/android/calendar/month/SimpleWeekView;->MINI_DAY_NUMBER_TEXT_SIZE:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    sget v7, Lcom/android/calendar/month/SimpleWeekView;->DAY_SEPARATOR_WIDTH:I

    sub-int v5, v6, v7

    iget v3, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumCells:I

    const/4 v1, 0x0

    mul-int/lit8 v0, v3, 0x2

    iget-boolean v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mShowWeekNum:Z

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    sget v7, Lcom/android/calendar/month/SimpleWeekView;->MINI_WK_NUMBER_TEXT_SIZE:I

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    invoke-virtual {v6, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    iget v7, p0, Lcom/android/calendar/month/SimpleWeekView;->mWeekNumColor:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mWidth:I

    iget v7, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    mul-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    div-int/2addr v6, v0

    iget v7, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    add-int v4, v6, v7

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mDayNumbers:[Ljava/lang/String;

    aget-object v6, v6, v10

    int-to-float v7, v4

    int-to-float v8, v5

    iget-object v9, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v1, v1, 0x1

    :cond_0
    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mFocusDay:[Z

    aget-boolean v2, v6, v1

    iget-object v7, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    if-eqz v2, :cond_4

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mFocusMonthColor:I

    :goto_0
    invoke-virtual {v7, v6}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v10}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    :goto_1
    if-ge v1, v3, :cond_6

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mFocusDay:[Z

    aget-boolean v6, v6, v1

    if-eq v6, v2, :cond_1

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mFocusDay:[Z

    aget-boolean v2, v6, v1

    iget-object v7, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    if-eqz v2, :cond_5

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mFocusMonthColor:I

    :goto_2
    invoke-virtual {v7, v6}, Landroid/graphics/Paint;->setColor(I)V

    :cond_1
    iget-boolean v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mHasToday:Z

    if-eqz v6, :cond_2

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mToday:I

    if-ne v6, v1, :cond_2

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    sget v7, Lcom/android/calendar/month/SimpleWeekView;->MINI_TODAY_NUMBER_TEXT_SIZE:I

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v11}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    :cond_2
    mul-int/lit8 v6, v1, 0x2

    add-int/lit8 v6, v6, 0x1

    iget v7, p0, Lcom/android/calendar/month/SimpleWeekView;->mWidth:I

    iget v8, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    mul-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    mul-int/2addr v6, v7

    div-int/2addr v6, v0

    iget v7, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    add-int v4, v6, v7

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mDayNumbers:[Ljava/lang/String;

    aget-object v6, v6, v1

    int-to-float v7, v4

    int-to-float v8, v5

    iget-object v9, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-boolean v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mHasToday:Z

    if-eqz v6, :cond_3

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mToday:I

    if-ne v6, v1, :cond_3

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    sget v7, Lcom/android/calendar/month/SimpleWeekView;->MINI_DAY_NUMBER_TEXT_SIZE:I

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v10}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mOtherMonthColor:I

    goto :goto_0

    :cond_5
    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mOtherMonthColor:I

    goto :goto_2

    :cond_6
    return-void
.end method

.method public getDayFromLocation(F)Landroid/text/format/Time;
    .locals 7
    .param p1    # F

    iget-boolean v4, p0, Lcom/android/calendar/month/SimpleWeekView;->mShowWeekNum:Z

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/android/calendar/month/SimpleWeekView;->mWidth:I

    iget v5, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumCells:I

    div-int/2addr v4, v5

    iget v5, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    add-int v2, v4, v5

    :goto_0
    int-to-float v4, v2

    cmpg-float v4, p1, v4

    if-ltz v4, :cond_0

    iget v4, p0, Lcom/android/calendar/month/SimpleWeekView;->mWidth:I

    iget v5, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v4, p1, v4

    if-lez v4, :cond_2

    :cond_0
    const/4 v3, 0x0

    :goto_1
    return-object v3

    :cond_1
    iget v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    goto :goto_0

    :cond_2
    int-to-float v4, v2

    sub-float v4, p1, v4

    iget v5, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumDays:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/android/calendar/month/SimpleWeekView;->mWidth:I

    sub-int/2addr v5, v2

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-int v1, v4

    iget v4, p0, Lcom/android/calendar/month/SimpleWeekView;->mFirstJulianDay:I

    add-int v0, v4, v1

    new-instance v3, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/month/SimpleWeekView;->mTimeZone:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->setJulianDay(I)J

    goto :goto_1
.end method

.method public getFirstJulianDay()I
    .locals 1

    iget v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mFirstJulianDay:I

    return v0
.end method

.method public getFirstMonth()I
    .locals 1

    iget v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mFirstMonth:I

    return v0
.end method

.method public getLastMonth()I
    .locals 1

    iget v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mLastMonth:I

    return v0
.end method

.method protected initView()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->MINI_DAY_NUMBER_TEXT_SIZE:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/month/SimpleWeekView;->MINI_DAY_NUMBER_TEXT_SIZE:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mFocusMonthColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeekView;->mMonthNumPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p0, p1}, Lcom/android/calendar/month/SimpleWeekView;->drawBackground(Landroid/graphics/Canvas;)V

    invoke-virtual {p0, p1}, Lcom/android/calendar/month/SimpleWeekView;->drawWeekNums(Landroid/graphics/Canvas;)V

    invoke-virtual {p0, p1}, Lcom/android/calendar/month/SimpleWeekView;->drawDaySeparators(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1    # Landroid/view/MotionEvent;

    const/4 v11, 0x1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v7}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v7}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/calendar/month/SimpleWeekView;->getDayFromLocation(F)Landroid/text/format/Time;

    move-result-object v9

    if-eqz v9, :cond_3

    iget-object v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mLastHoverTime:Landroid/text/format/Time;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mLastHoverTime:Landroid/text/format/Time;

    invoke-static {v9, v1}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    invoke-virtual {v9, v11}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/16 v5, 0x10

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v8

    const/16 v1, 0x40

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityRecord;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v6}, Landroid/view/View;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    iput-object v9, p0, Lcom/android/calendar/month/SimpleWeekView;->mLastHoverTime:Landroid/text/format/Time;

    :cond_3
    move v1, v11

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mHeight:I

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iput p1, p0, Lcom/android/calendar/month/SimpleWeekView;->mWidth:I

    invoke-virtual {p0}, Lcom/android/calendar/month/SimpleWeekView;->updateSelectionPositions()V

    return-void
.end method

.method public setWeekParams(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 13
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v1, -0x1

    const/4 v8, 0x0

    const/4 v7, 0x1

    const-string v6, "week"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/security/InvalidParameterException;

    const-string v7, "You must specify the week number for this view"

    invoke-direct {v6, v7}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/android/calendar/month/SimpleWeekView;->mTimeZone:Ljava/lang/String;

    const-string v6, "height"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "height"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mHeight:I

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mHeight:I

    sget v9, Lcom/android/calendar/month/SimpleWeekView;->MIN_HEIGHT:I

    if-ge v6, v9, :cond_1

    sget v6, Lcom/android/calendar/month/SimpleWeekView;->MIN_HEIGHT:I

    iput v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mHeight:I

    :cond_1
    const-string v6, "selected_day"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "selected_day"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedDay:I

    :cond_2
    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedDay:I

    if-eq v6, v1, :cond_d

    move v6, v7

    :goto_0
    iput-boolean v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mHasSelectedDay:Z

    const-string v6, "num_days"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "num_days"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumDays:I

    :cond_3
    const-string v6, "show_wk_num"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "show_wk_num"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-eqz v6, :cond_e

    iput-boolean v7, p0, Lcom/android/calendar/month/SimpleWeekView;->mShowWeekNum:Z

    :cond_4
    :goto_1
    iget-boolean v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mShowWeekNum:Z

    if-eqz v6, :cond_f

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumDays:I

    add-int/lit8 v6, v6, 0x1

    :goto_2
    iput v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumCells:I

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumCells:I

    new-array v6, v6, [Ljava/lang/String;

    iput-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mDayNumbers:[Ljava/lang/String;

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumCells:I

    new-array v6, v6, [Z

    iput-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mFocusDay:[Z

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumCells:I

    new-array v6, v6, [Z

    iput-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mOddMonth:[Z

    const-string v6, "week"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mWeek:I

    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mWeek:I

    invoke-static {v6}, Lcom/android/calendar/Utils;->getJulianMondayFromWeeksSinceEpoch(I)I

    move-result v3

    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4, p2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Landroid/text/format/Time;->setJulianDay(I)J

    const/4 v2, 0x0

    iget-boolean v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mShowWeekNum:Z

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mDayNumbers:[Ljava/lang/String;

    invoke-virtual {v4}, Landroid/text/format/Time;->getWeekNumber()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    add-int/lit8 v2, v2, 0x1

    :cond_5
    const-string v6, "week_start"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "week_start"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mWeekStart:I

    :cond_6
    iget v6, v4, Landroid/text/format/Time;->weekDay:I

    iget v9, p0, Lcom/android/calendar/month/SimpleWeekView;->mWeekStart:I

    if-eq v6, v9, :cond_7

    const v6, 0x253d89

    if-ne v3, v6, :cond_7

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v4, v3}, Landroid/text/format/Time;->setJulianDay(I)J

    :cond_7
    iget v6, v4, Landroid/text/format/Time;->weekDay:I

    iget v9, p0, Lcom/android/calendar/month/SimpleWeekView;->mWeekStart:I

    if-eq v6, v9, :cond_9

    iget v6, v4, Landroid/text/format/Time;->weekDay:I

    iget v9, p0, Lcom/android/calendar/month/SimpleWeekView;->mWeekStart:I

    sub-int v0, v6, v9

    if-gez v0, :cond_8

    add-int/lit8 v0, v0, 0x7

    :cond_8
    iget v6, v4, Landroid/text/format/Time;->monthDay:I

    sub-int/2addr v6, v0

    iput v6, v4, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->normalize(Z)J

    :cond_9
    invoke-virtual {v4, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v9

    iget-wide v11, v4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v9, v10, v11, v12}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v6

    iput v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mFirstJulianDay:I

    iget v6, v4, Landroid/text/format/Time;->month:I

    iput v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mFirstMonth:I

    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, p2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/text/format/Time;->setToNow()V

    iput-boolean v8, p0, Lcom/android/calendar/month/SimpleWeekView;->mHasToday:Z

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mToday:I

    const-string v6, "focus_month"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    const-string v6, "focus_month"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_a
    :goto_3
    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumCells:I

    if-ge v2, v6, :cond_12

    iget v6, v4, Landroid/text/format/Time;->monthDay:I

    if-ne v6, v7, :cond_b

    iget v6, v4, Landroid/text/format/Time;->month:I

    iput v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mFirstMonth:I

    :cond_b
    iget-object v9, p0, Lcom/android/calendar/month/SimpleWeekView;->mOddMonth:[Z

    iget v6, v4, Landroid/text/format/Time;->month:I

    rem-int/lit8 v6, v6, 0x2

    if-ne v6, v7, :cond_10

    move v6, v7

    :goto_4
    aput-boolean v6, v9, v2

    iget v6, v4, Landroid/text/format/Time;->month:I

    if-ne v6, v1, :cond_11

    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mFocusDay:[Z

    aput-boolean v7, v6, v2

    :goto_5
    iget v6, v4, Landroid/text/format/Time;->year:I

    iget v9, v5, Landroid/text/format/Time;->year:I

    if-ne v6, v9, :cond_c

    iget v6, v4, Landroid/text/format/Time;->yearDay:I

    iget v9, v5, Landroid/text/format/Time;->yearDay:I

    if-ne v6, v9, :cond_c

    iput-boolean v7, p0, Lcom/android/calendar/month/SimpleWeekView;->mHasToday:Z

    iput v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mToday:I

    :cond_c
    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mDayNumbers:[Ljava/lang/String;

    iget v9, v4, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v10, v9, 0x1

    iput v10, v4, Landroid/text/format/Time;->monthDay:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v2

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->normalize(Z)J

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_d
    move v6, v8

    goto/16 :goto_0

    :cond_e
    iput-boolean v8, p0, Lcom/android/calendar/month/SimpleWeekView;->mShowWeekNum:Z

    goto/16 :goto_1

    :cond_f
    iget v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumDays:I

    goto/16 :goto_2

    :cond_10
    move v6, v8

    goto :goto_4

    :cond_11
    iget-object v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mFocusDay:[Z

    aput-boolean v8, v6, v2

    goto :goto_5

    :cond_12
    iget v6, v4, Landroid/text/format/Time;->monthDay:I

    if-ne v6, v7, :cond_13

    iget v6, v4, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v4, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->normalize(Z)J

    :cond_13
    iget v6, v4, Landroid/text/format/Time;->month:I

    iput v6, p0, Lcom/android/calendar/month/SimpleWeekView;->mLastMonth:I

    invoke-virtual {p0}, Lcom/android/calendar/month/SimpleWeekView;->updateSelectionPositions()V

    return-void
.end method

.method protected updateSelectionPositions()V
    .locals 4

    iget-boolean v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mHasSelectedDay:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedDay:I

    iget v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mWeekStart:I

    sub-int v0, v1, v2

    if-gez v0, :cond_0

    add-int/lit8 v0, v0, 0x7

    :cond_0
    iget-boolean v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mShowWeekNum:Z

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    iget v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mWidth:I

    iget v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumCells:I

    div-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedLeft:I

    add-int/lit8 v1, v0, 0x1

    iget v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mWidth:I

    iget v3, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mNumCells:I

    div-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/SimpleWeekView;->mPadding:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/month/SimpleWeekView;->mSelectedRight:I

    :cond_2
    return-void
.end method
