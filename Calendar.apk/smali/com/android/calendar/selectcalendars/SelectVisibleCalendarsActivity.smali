.class public Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsActivity;
.super Lcom/android/calendar/AbstractCalendarActivity;
.source "SelectVisibleCalendarsActivity.java"


# instance fields
.field private mFragment:Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/calendar/AbstractCalendarActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public handleSelectSyncedCalendarsClicked(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/android/calendar/selectcalendars/SelectSyncedCalendarsMultiAccountActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const v3, 0x7f1000bb

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f040038

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsFragment;

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsActivity;->mFragment:Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsFragment;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsActivity;->mFragment:Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsFragment;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsFragment;

    const v2, 0x7f040013

    invoke-direct {v1, v2}, Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsFragment;-><init>(I)V

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsActivity;->mFragment:Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsFragment;

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsActivity;->mFragment:Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsFragment;

    invoke-virtual {v0, v3, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsActivity;->mFragment:Lcom/android/calendar/selectcalendars/SelectVisibleCalendarsFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    const/4 v1, 0x4

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-static {p0}, Lcom/android/calendar/Utils;->returnToCalendarHome(Landroid/content/Context;)V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
