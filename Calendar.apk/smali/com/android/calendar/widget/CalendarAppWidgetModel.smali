.class Lcom/android/calendar/widget/CalendarAppWidgetModel;
.super Ljava/lang/Object;
.source "CalendarAppWidgetModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/widget/CalendarAppWidgetModel$DayInfo;,
        Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;,
        Lcom/android/calendar/widget/CalendarAppWidgetModel$RowInfo;
    }
.end annotation


# static fields
.field private static final LOGD:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field final mContext:Landroid/content/Context;

.field final mDayInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/calendar/widget/CalendarAppWidgetModel$DayInfo;",
            ">;"
        }
    .end annotation
.end field

.field final mEventInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mHomeTZName:Ljava/lang/String;

.field final mMaxJulianDay:I

.field final mNow:J

.field final mRowInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/calendar/widget/CalendarAppWidgetModel$RowInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mShowTZ:Z

.field final mTodayJulianDay:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const/16 v5, 0x32

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mNow:J

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    iget-wide v1, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mNow:J

    iget-wide v3, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mTodayJulianDay:I

    iget v1, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mTodayJulianDay:I

    add-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mMaxJulianDay:I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mEventInfos:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mRowInfos:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mDayInfos:Ljava/util/List;

    iput-object p1, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mContext:Landroid/content/Context;

    return-void
.end method

.method private populateDayInfo(ILandroid/text/format/Time;)Lcom/android/calendar/widget/CalendarAppWidgetModel$DayInfo;
    .locals 11
    .param p1    # I
    .param p2    # Landroid/text/format/Time;

    invoke-virtual {p2, p1}, Landroid/text/format/Time;->setJulianDay(I)J

    move-result-wide v1

    const v5, 0x80010

    iget v0, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mTodayJulianDay:I

    add-int/lit8 v0, v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v7, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mContext:Landroid/content/Context;

    const v8, 0x7f0c0089

    const/4 v0, 0x1

    new-array v9, v0, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mContext:Landroid/content/Context;

    move-wide v3, v1

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    :goto_0
    new-instance v0, Lcom/android/calendar/widget/CalendarAppWidgetModel$DayInfo;

    invoke-direct {v0, p1, v6}, Lcom/android/calendar/widget/CalendarAppWidgetModel$DayInfo;-><init>(ILjava/lang/String;)V

    return-object v0

    :cond_0
    or-int/lit8 v5, v5, 0x2

    iget-object v0, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mContext:Landroid/content/Context;

    move-wide v3, v1

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method private populateEventInfo(JZJJIILjava/lang/String;Ljava/lang/String;II)Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;
    .locals 11
    .param p1    # J
    .param p3    # Z
    .param p4    # J
    .param p6    # J
    .param p8    # I
    .param p9    # I
    .param p10    # Ljava/lang/String;
    .param p11    # Ljava/lang/String;
    .param p12    # I
    .param p13    # I

    new-instance v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;

    invoke-direct {v8}, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;-><init>()V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x18000

    const/4 v9, 0x0

    if-eqz p3, :cond_1

    or-int/lit8 v7, v7, 0x10

    iget-object v2, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mContext:Landroid/content/Context;

    move-wide v3, p4

    move-wide/from16 v5, p6

    invoke-static/range {v2 .. v7}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_0
    iput-wide p1, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->id:J

    iput-wide p4, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->start:J

    move-wide/from16 v0, p6

    iput-wide v0, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->end:J

    iput-boolean p3, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->allDay:Z

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->when:Ljava/lang/String;

    iput v9, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->visibWhen:I

    move/from16 v0, p12

    iput v0, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->color:I

    move/from16 v0, p13

    iput v0, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->selfAttendeeStatus:I

    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0044

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->title:Ljava/lang/String;

    :goto_1
    const/4 v2, 0x0

    iput v2, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->visibTitle:I

    invoke-static/range {p11 .. p11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x0

    iput v2, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->visibWhere:I

    move-object/from16 v0, p11

    iput-object v0, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->where:Ljava/lang/String;

    :goto_2
    return-object v8

    :cond_1
    or-int/lit8 v7, v7, 0x1

    iget-object v2, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    or-int/lit16 v7, v7, 0x80

    :cond_2
    move/from16 v0, p9

    move/from16 v1, p8

    if-le v0, v1, :cond_3

    or-int/lit8 v7, v7, 0x10

    :cond_3
    iget-object v2, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mContext:Landroid/content/Context;

    move-wide v3, p4

    move-wide/from16 v5, p6

    invoke-static/range {v2 .. v7}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mShowTZ:Z

    if-eqz v2, :cond_0

    const-string v2, " "

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mHomeTZName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_4
    move-object/from16 v0, p10

    iput-object v0, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->title:Ljava/lang/String;

    goto :goto_1

    :cond_5
    const/16 v2, 0x8

    iput v2, v8, Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;->visibWhere:I

    goto :goto_2
.end method


# virtual methods
.method public buildFromCursor(Landroid/database/Cursor;Ljava/lang/String;)V
    .locals 33
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/String;

    new-instance v26, Landroid/text/format/Time;

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    new-instance v25, Ljava/util/ArrayList;

    const/4 v3, 0x7

    move-object/from16 v0, v25

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/16 v23, 0x0

    :goto_0
    const/4 v3, 0x7

    move/from16 v0, v23

    if-ge v0, v3, :cond_0

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v23, v23, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual/range {v26 .. v26}, Landroid/text/format/Time;->setToNow()V

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    const/4 v3, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mShowTZ:Z

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mShowTZ:Z

    if-eqz v3, :cond_1

    invoke-static/range {p2 .. p2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v31

    move-object/from16 v0, v26

    iget v3, v0, Landroid/text/format/Time;->isDst:I

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    :goto_2
    const/16 v32, 0x1

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v0, v3, v1}, Ljava/util/TimeZone;->getDisplayName(ZI)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mHomeTZName:Ljava/lang/String;

    :cond_1
    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mContext:Landroid/content/Context;

    const/16 v31, 0x0

    move-object/from16 v0, v31

    invoke-static {v3, v0}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v30

    :cond_2
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v27

    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_6

    const/4 v6, 0x1

    :goto_3
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    if-eqz v6, :cond_3

    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-static {v0, v7, v8, v1}, Lcom/android/calendar/Utils;->convertAlldayUtcToLocal(Landroid/text/format/Time;JLjava/lang/String;)J

    move-result-wide v7

    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-static {v0, v9, v10, v1}, Lcom/android/calendar/Utils;->convertAlldayUtcToLocal(Landroid/text/format/Time;JLjava/lang/String;)J

    move-result-wide v9

    :cond_3
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mNow:J

    move-wide/from16 v31, v0

    cmp-long v3, v9, v31

    if-ltz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mEventInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mEventInfos:Ljava/util/List;

    move-object/from16 v31, v0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v16}, Lcom/android/calendar/widget/CalendarAppWidgetModel;->populateEventInfo(JZJJIILjava/lang/String;Ljava/lang/String;II)Lcom/android/calendar/widget/CalendarAppWidgetModel$EventInfo;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mTodayJulianDay:I

    invoke-static {v11, v3}, Ljava/lang/Math;->max(II)I

    move-result v22

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mMaxJulianDay:I

    invoke-static {v12, v3}, Ljava/lang/Math;->min(II)I

    move-result v29

    move/from16 v19, v22

    :goto_4
    move/from16 v0, v19

    move/from16 v1, v29

    if-gt v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mTodayJulianDay:I

    sub-int v3, v19, v3

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/LinkedList;

    new-instance v28, Lcom/android/calendar/widget/CalendarAppWidgetModel$RowInfo;

    const/4 v3, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v23

    invoke-direct {v0, v3, v1}, Lcom/android/calendar/widget/CalendarAppWidgetModel$RowInfo;-><init>(II)V

    if-eqz v6, :cond_7

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    :goto_5
    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_3

    :cond_7
    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mTodayJulianDay:I

    move/from16 v19, v0

    const/16 v18, 0x0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_9
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/LinkedList;

    invoke-virtual/range {v17 .. v17}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_b

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mTodayJulianDay:I

    move/from16 v0, v19

    if-eq v0, v3, :cond_a

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/widget/CalendarAppWidgetModel;->populateDayInfo(ILandroid/text/format/Time;)Lcom/android/calendar/widget/CalendarAppWidgetModel$DayInfo;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mDayInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mDayInfos:Ljava/util/List;

    move-object/from16 v0, v21

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mRowInfos:Ljava/util/List;

    new-instance v31, Lcom/android/calendar/widget/CalendarAppWidgetModel$RowInfo;

    const/16 v32, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/widget/CalendarAppWidgetModel$RowInfo;-><init>(II)V

    move-object/from16 v0, v31

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mRowInfos:Ljava/util/List;

    move-object/from16 v0, v17

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual/range {v17 .. v17}, Ljava/util/LinkedList;->size()I

    move-result v3

    add-int v18, v18, v3

    :cond_b
    add-int/lit8 v19, v19, 0x1

    const/16 v3, 0x14

    move/from16 v0, v18

    if-lt v0, v3, :cond_9

    :cond_c
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\nCalendarAppWidgetModel [eventInfos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel;->mEventInfos:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
