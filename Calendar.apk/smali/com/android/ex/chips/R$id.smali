.class public final Lcom/android/ex/chips/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final Numberpicker2:I = 0x7f1000b3

.field public static final account:I = 0x7f100004

.field public static final account_name:I = 0x7f100036

.field public static final account_status:I = 0x7f100001

.field public static final account_type:I = 0x7f100005

.field public static final action_add_account:I = 0x7f1000cb

.field public static final action_cancel:I = 0x7f100087

.field public static final action_create_event:I = 0x7f1000c1

.field public static final action_delete_all_events:I = 0x7f1000c5

.field public static final action_done:I = 0x7f100088

.field public static final action_hide_controls:I = 0x7f1000c7

.field public static final action_refresh:I = 0x7f1000c2

.field public static final action_search:I = 0x7f1000c3

.field public static final action_select_visible_calendars:I = 0x7f1000c4

.field public static final action_settings:I = 0x7f1000c6

.field public static final action_today:I = 0x7f1000c0

.field public static final add_attendees_label:I = 0x7f100076

.field public static final add_attendees_row:I = 0x7f100075

.field public static final agenda_choice_frame:I = 0x7f10000a

.field public static final agenda_event_info:I = 0x7f100011

.field public static final agenda_events_list:I = 0x7f100010

.field public static final agenda_item_color:I = 0x7f100012

.field public static final agenda_item_text_container:I = 0x7f100013

.field public static final agenda_sticky_header_list:I = 0x7f10000f

.field public static final alert_container:I = 0x7f10001a

.field public static final all_day_row:I = 0x7f100062

.field public static final appwidget_date:I = 0x7f10002a

.field public static final appwidget_loading:I = 0x7f10002b

.field public static final appwidget_no_events:I = 0x7f10002d

.field public static final attendees:I = 0x7f100077

.field public static final attendees_group:I = 0x7f100046

.field public static final availability:I = 0x7f100083

.field public static final availability_row:I = 0x7f100081

.field public static final badge:I = 0x7f100039

.field public static final btn_cancel:I = 0x7f1000b9

.field public static final btn_discard:I = 0x7f1000b7

.field public static final btn_done:I = 0x7f1000b8

.field public static final btn_ok:I = 0x7f1000ba

.field public static final bubble_layout:I = 0x7f10002f

.field public static final button_date:I = 0x7f100007

.field public static final button_view:I = 0x7f100006

.field public static final calendar:I = 0x7f100032

.field public static final calendar_group:I = 0x7f10004a

.field public static final calendar_list:I = 0x7f100003

.field public static final calendar_name:I = 0x7f100035

.field public static final calendar_selector_group:I = 0x7f100048

.field public static final calendar_selector_wrapper:I = 0x7f100069

.field public static final calendar_textview:I = 0x7f10004b

.field public static final calendar_textview_secondary:I = 0x7f10004c

.field public static final calendar_view:I = 0x7f10003e

.field public static final calendars:I = 0x7f100000

.field public static final calendars_spinner:I = 0x7f100049

.field public static final color:I = 0x7f100031

.field public static final color_chip:I = 0x7f100047

.field public static final color_square:I = 0x7f10001c

.field public static final contact_remove:I = 0x7f100038

.field public static final contact_separator:I = 0x7f10003a

.field public static final d0_label:I = 0x7f1000a5

.field public static final d1_label:I = 0x7f1000a6

.field public static final d2_label:I = 0x7f1000a7

.field public static final d3_label:I = 0x7f1000a8

.field public static final d4_label:I = 0x7f1000a9

.field public static final d5_label:I = 0x7f1000aa

.field public static final d6_label:I = 0x7f1000ab

.field public static final date:I = 0x7f10000e

.field public static final dateDisplay:I = 0x7f1000b1

.field public static final datePicker:I = 0x7f10003f

.field public static final date_bar:I = 0x7f100025

.field public static final date_group:I = 0x7f100024

.field public static final day:I = 0x7f10000d

.field public static final day_names:I = 0x7f1000a3

.field public static final day_of_week:I = 0x7f100028

.field public static final delete:I = 0x7f100095

.field public static final description:I = 0x7f10007a

.field public static final description_label:I = 0x7f100079

.field public static final description_row:I = 0x7f100078

.field public static final dismiss_all:I = 0x7f10001b

.field public static final edit:I = 0x7f100094

.field public static final edit_event:I = 0x7f100041

.field public static final email_attendees_button:I = 0x7f10009b

.field public static final email_attendees_container:I = 0x7f10009a

.field public static final end_date:I = 0x7f10005d

.field public static final end_date_home_tz:I = 0x7f100060

.field public static final end_time:I = 0x7f10005e

.field public static final end_time_home_tz:I = 0x7f100061

.field public static final event_bg:I = 0x7f100044

.field public static final event_info_buttons_container:I = 0x7f100093

.field public static final event_info_headline:I = 0x7f100092

.field public static final event_info_loading_msg:I = 0x7f10008f

.field public static final event_info_progress_bar:I = 0x7f100090

.field public static final event_info_scroll_view:I = 0x7f100091

.field public static final event_title:I = 0x7f10001d

.field public static final events_list:I = 0x7f100029

.field public static final expand_collapse:I = 0x7f1000a1

.field public static final expandable_text:I = 0x7f1000a0

.field public static final from_label:I = 0x7f100055

.field public static final from_row:I = 0x7f100054

.field public static final from_row_home_tz:I = 0x7f100058

.field public static final header:I = 0x7f100027

.field public static final home_time:I = 0x7f10001f

.field public static final image:I = 0x7f1000ae

.field public static final info_action_delete:I = 0x7f1000ca

.field public static final info_action_edit:I = 0x7f1000c9

.field public static final info_action_share:I = 0x7f1000c8

.field public static final is_all_day:I = 0x7f100063

.field public static final is_all_day_label:I = 0x7f100064

.field public static final launch_custom_app_button:I = 0x7f10009d

.field public static final launch_custom_app_container:I = 0x7f10009c

.field public static final list:I = 0x7f1000b5

.field public static final loading:I = 0x7f10002c

.field public static final loading_message:I = 0x7f100042

.field public static final location:I = 0x7f10004e

.field public static final long_attendee_list:I = 0x7f10009e

.field public static final main_frame:I = 0x7f1000bb

.field public static final main_pane:I = 0x7f100020

.field public static final manage_sync_set:I = 0x7f1000b6

.field public static final mini_month:I = 0x7f100023

.field public static final mini_month_container:I = 0x7f100022

.field public static final month:I = 0x7f10003c

.field public static final month_name:I = 0x7f1000a2

.field public static final mtk_events_list:I = 0x7f1000ad

.field public static final name:I = 0x7f100037

.field public static final no_events:I = 0x7f10002e

.field public static final organizer:I = 0x7f100074

.field public static final organizer_container:I = 0x7f100098

.field public static final organizer_label:I = 0x7f100073

.field public static final organizer_row:I = 0x7f100072

.field public static final pickDate:I = 0x7f1000b2

.field public static final pickers:I = 0x7f10003b

.field public static final presence_label:I = 0x7f100082

.field public static final privacy_label:I = 0x7f100085

.field public static final progress_circular:I = 0x7f10008e

.field public static final reminder:I = 0x7f100019

.field public static final reminder_add:I = 0x7f100080

.field public static final reminder_icon:I = 0x7f100018

.field public static final reminder_items_container:I = 0x7f10007f

.field public static final reminder_method_value:I = 0x7f10008a

.field public static final reminder_minutes_value:I = 0x7f100089

.field public static final reminder_remove:I = 0x7f10008b

.field public static final reminders_group_label:I = 0x7f10007e

.field public static final reminders_row:I = 0x7f10007d

.field public static final repeat_icon:I = 0x7f10001e

.field public static final repeats:I = 0x7f10007c

.field public static final repeats_label:I = 0x7f10007b

.field public static final response_container:I = 0x7f100099

.field public static final response_label:I = 0x7f10006d

.field public static final response_maybe:I = 0x7f100070

.field public static final response_no:I = 0x7f100071

.field public static final response_row:I = 0x7f10006c

.field public static final response_value:I = 0x7f10006e

.field public static final response_yes:I = 0x7f10006f

.field public static final scroll_view:I = 0x7f100043

.field public static final search_results:I = 0x7f1000b4

.field public static final secondary_pane:I = 0x7f100021

.field public static final selected_marker:I = 0x7f100017

.field public static final share:I = 0x7f10009f

.field public static final snooze_button:I = 0x7f1000b0

.field public static final start_date:I = 0x7f100056

.field public static final start_date_home_tz:I = 0x7f100059

.field public static final start_time:I = 0x7f100057

.field public static final start_time_home_tz:I = 0x7f10005a

.field public static final status:I = 0x7f100033

.field public static final switch_date_picker:I = 0x7f100051

.field public static final switch_gregorian:I = 0x7f100052

.field public static final switch_lunar:I = 0x7f100053

.field public static final switcher:I = 0x7f100040

.field public static final sync:I = 0x7f100034

.field public static final sync_settings:I = 0x7f100002

.field public static final text:I = 0x7f1000af

.field public static final text1:I = 0x7f10008c

.field public static final text2:I = 0x7f10008d

.field public static final time:I = 0x7f100030

.field public static final timezone_button:I = 0x7f100068

.field public static final timezone_button_row:I = 0x7f100067

.field public static final timezone_footer:I = 0x7f1000bc

.field public static final timezone_label:I = 0x7f10006b

.field public static final timezone_textView:I = 0x7f100066

.field public static final timezone_textview_row:I = 0x7f100065

.field public static final title:I = 0x7f100014

.field public static final to_label:I = 0x7f10005c

.field public static final to_row:I = 0x7f10005b

.field public static final to_row_home_tz:I = 0x7f10005f

.field public static final today_icon_background:I = 0x7f1000be

.field public static final today_icon_day:I = 0x7f1000bf

.field public static final top_button_date:I = 0x7f100009

.field public static final top_button_weekday:I = 0x7f100008

.field public static final top_divider_past_present:I = 0x7f10000c

.field public static final top_divider_simple:I = 0x7f10000b

.field public static final two_pane:I = 0x7f100045

.field public static final visibility:I = 0x7f100086

.field public static final visibility_row:I = 0x7f100084

.field public static final visible_check_box:I = 0x7f1000ac

.field public static final week_num:I = 0x7f100026

.field public static final what_label:I = 0x7f10006a

.field public static final when:I = 0x7f100015

.field public static final when_datetime:I = 0x7f100096

.field public static final when_label:I = 0x7f100050

.field public static final when_repeat:I = 0x7f100097

.field public static final when_row:I = 0x7f10004f

.field public static final where:I = 0x7f100016

.field public static final where_row:I = 0x7f10004d

.field public static final widget_row:I = 0x7f1000bd

.field public static final wk_label:I = 0x7f1000a4

.field public static final year:I = 0x7f10003d


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
