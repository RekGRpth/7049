.class public final Lcom/android/ex/chips/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final account_calendars:I = 0x7f040000

.field public static final account_item:I = 0x7f040001

.field public static final actionbar_pulldown_menu_button:I = 0x7f040002

.field public static final actionbar_pulldown_menu_top_button:I = 0x7f040003

.field public static final actionbar_pulldown_menu_top_button_no_date:I = 0x7f040004

.field public static final agenda_choice:I = 0x7f040005

.field public static final agenda_day:I = 0x7f040006

.field public static final agenda_fragment:I = 0x7f040007

.field public static final agenda_header_footer:I = 0x7f040008

.field public static final agenda_item:I = 0x7f040009

.field public static final agenda_reminder_item:I = 0x7f04000a

.field public static final alert_activity:I = 0x7f04000b

.field public static final alert_item:I = 0x7f04000c

.field public static final all_in_one:I = 0x7f04000d

.field public static final appwidget:I = 0x7f04000e

.field public static final appwidget_day:I = 0x7f04000f

.field public static final appwidget_loading:I = 0x7f040010

.field public static final appwidget_no_events:I = 0x7f040011

.field public static final bubble_event:I = 0x7f040012

.field public static final calendar_sync_item:I = 0x7f040013

.field public static final calendars_dropdown_item:I = 0x7f040014

.field public static final calendars_item:I = 0x7f040015

.field public static final chips_alternate_item:I = 0x7f040016

.field public static final chips_recipient_dropdown_item:I = 0x7f040017

.field public static final contact_item:I = 0x7f040018

.field public static final copy_chip_dialog_layout:I = 0x7f040019

.field public static final date_picker:I = 0x7f04001a

.field public static final date_picker_dialog:I = 0x7f04001b

.field public static final date_range_title:I = 0x7f04001c

.field public static final day_activity:I = 0x7f04001d

.field public static final edit_event:I = 0x7f04001e

.field public static final edit_event_1:I = 0x7f04001f

.field public static final edit_event_2:I = 0x7f040020

.field public static final edit_event_custom_actionbar:I = 0x7f040021

.field public static final edit_event_single_column:I = 0x7f040022

.field public static final edit_reminder_item:I = 0x7f040023

.field public static final email_autocomplete_item:I = 0x7f040024

.field public static final email_autocomplete_item_loading:I = 0x7f040025

.field public static final event_info:I = 0x7f040026

.field public static final event_info_dialog:I = 0x7f040027

.field public static final event_info_label:I = 0x7f040028

.field public static final expandable_textview:I = 0x7f040029

.field public static final full_month_by_week:I = 0x7f04002a

.field public static final full_month_header:I = 0x7f04002b

.field public static final mini_calendar_item:I = 0x7f04002c

.field public static final mini_month_header:I = 0x7f04002d

.field public static final month_by_week:I = 0x7f04002e

.field public static final more_item:I = 0x7f04002f

.field public static final mtk_event_selection_fragment:I = 0x7f040030

.field public static final notification:I = 0x7f040031

.field public static final picker:I = 0x7f040032

.field public static final quick_response_item:I = 0x7f040033

.field public static final search:I = 0x7f040034

.field public static final select_calendars_fragment:I = 0x7f040035

.field public static final select_calendars_multi_accounts_fragment:I = 0x7f040036

.field public static final select_calendars_to_clear_fragment:I = 0x7f040037

.field public static final simple_frame_layout:I = 0x7f040038

.field public static final timezone_footer:I = 0x7f040039

.field public static final widget_all_day_item:I = 0x7f04003a

.field public static final widget_item:I = 0x7f04003b


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
