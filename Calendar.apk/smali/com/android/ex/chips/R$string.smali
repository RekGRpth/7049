.class public final Lcom/android/ex/chips/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accessibility_add_attendee:I = 0x7f0c00d3

.field public static final accessibility_add_reminder:I = 0x7f0c00d1

.field public static final accessibility_all_day:I = 0x7f0c00d5

.field public static final accessibility_pick_end_date:I = 0x7f0c00ce

.field public static final accessibility_pick_end_time:I = 0x7f0c00cf

.field public static final accessibility_pick_start_date:I = 0x7f0c00cc

.field public static final accessibility_pick_start_time:I = 0x7f0c00cd

.field public static final accessibility_pick_time_zone:I = 0x7f0c00d0

.field public static final accessibility_reminder_privacy:I = 0x7f0c00da

.field public static final accessibility_reminder_showmeas:I = 0x7f0c00d9

.field public static final accessibility_reminder_time:I = 0x7f0c00d7

.field public static final accessibility_reminder_type:I = 0x7f0c00d8

.field public static final accessibility_remove_attendee:I = 0x7f0c00cb

.field public static final accessibility_remove_reminder:I = 0x7f0c00d2

.field public static final accessibility_repeats:I = 0x7f0c00d6

.field public static final accessibility_sync_cal:I = 0x7f0c00d4

.field public static final accounts:I = 0x7f0c0059

.field public static final acct_not_synced:I = 0x7f0c0058

.field public static final acessibility_cal_notification:I = 0x7f0c00db

.field public static final acessibility_selected_marker_description:I = 0x7f0c00df

.field public static final acessibility_snooze_notification:I = 0x7f0c00dc

.field public static final add_account:I = 0x7f0c0074

.field public static final agenda_today:I = 0x7f0c0087

.field public static final agenda_tomorrow:I = 0x7f0c0089

.field public static final agenda_view:I = 0x7f0c0047

.field public static final agenda_yesterday:I = 0x7f0c0088

.field public static final alert_title:I = 0x7f0c0065

.field public static final app_label:I = 0x7f0c0038

.field public static final attendees_invalid_tip:I = 0x7f0c000b

.field public static final attendees_label:I = 0x7f0c003d

.field public static final calendar_refresh:I = 0x7f0c0045

.field public static final cancel:I = 0x7f0c0014

.field public static final change_response_title:I = 0x7f0c00a7

.field public static final clear_all_selected_events_title:I = 0x7f0c0007

.field public static final clear_events:I = 0x7f0c0005

.field public static final click_to_view_all:I = 0x7f0c000d

.field public static final copy_db:I = 0x7f0c00c9

.field public static final copy_email:I = 0x7f0c00e3

.field public static final copy_number:I = 0x7f0c00e4

.field public static final create_an_account_desc:I = 0x7f0c0073

.field public static final creating_event:I = 0x7f0c0060

.field public static final creating_event_with_guest:I = 0x7f0c0063

.field public static final custom:I = 0x7f0c009f

.field public static final daily:I = 0x7f0c0097

.field public static final date_picker_decrement_day_button:I = 0x7f0c0019

.field public static final date_picker_decrement_month_button:I = 0x7f0c0015

.field public static final date_picker_decrement_year_button:I = 0x7f0c0013

.field public static final date_picker_dialog_title:I = 0x7f0c0011

.field public static final date_picker_increment_day_button:I = 0x7f0c0018

.field public static final date_picker_increment_month_button:I = 0x7f0c0016

.field public static final date_picker_increment_year_button:I = 0x7f0c0017

.field public static final date_time_done:I = 0x7f0c0012

.field public static final date_time_fmt:I = 0x7f0c0042

.field public static final day_view:I = 0x7f0c0048

.field public static final day_view_new_event_hint:I = 0x7f0c00dd

.field public static final delete_certain:I = 0x7f0c0009

.field public static final delete_completed:I = 0x7f0c0008

.field public static final delete_label:I = 0x7f0c0090

.field public static final delete_recurring_event_title:I = 0x7f0c00a6

.field public static final delete_this_event_title:I = 0x7f0c00a5

.field public static final description_label:I = 0x7f0c006d

.field public static final directory_searching_fmt:I = 0x7f0c00ca

.field public static final discard_label:I = 0x7f0c0092

.field public static final dismiss_all_label:I = 0x7f0c0094

.field public static final do_not_check:I = 0x7f0c00e0

.field public static final does_not_repeat:I = 0x7f0c0096

.field public static final done:I = 0x7f0c00e5

.field public static final edit_event_all_day_label:I = 0x7f0c006a

.field public static final edit_event_calendar_label:I = 0x7f0c006b

.field public static final edit_event_from_label:I = 0x7f0c0069

.field public static final edit_event_label:I = 0x7f0c008e

.field public static final edit_event_show_all:I = 0x7f0c006c

.field public static final edit_event_to_label:I = 0x7f0c0068

.field public static final edit_label:I = 0x7f0c008f

.field public static final email_guests_label:I = 0x7f0c007b

.field public static final email_picker_label:I = 0x7f0c007c

.field public static final email_subject_prefix:I = 0x7f0c007d

.field public static final empty_event:I = 0x7f0c0062

.field public static final event_create:I = 0x7f0c004c

.field public static final event_delete:I = 0x7f0c004e

.field public static final event_edit:I = 0x7f0c004d

.field public static final event_info_organizer:I = 0x7f0c0085

.field public static final event_info_reminders_label:I = 0x7f0c0086

.field public static final event_info_title:I = 0x7f0c0066

.field public static final event_info_title_invite:I = 0x7f0c0067

.field public static final event_view:I = 0x7f0c004b

.field public static final every_weekday:I = 0x7f0c0098

.field public static final gadget_no_events:I = 0x7f0c00c7

.field public static final gadget_title:I = 0x7f0c00c6

.field public static final google_email_domain:I = 0x7f0c00e1

.field public static final goto_today:I = 0x7f0c004f

.field public static final gregorian_radio_text:I = 0x7f0c0010

.field public static final hide_controls:I = 0x7f0c0053

.field public static final hint_attendees:I = 0x7f0c005f

.field public static final hint_description:I = 0x7f0c005e

.field public static final hint_what:I = 0x7f0c005c

.field public static final hint_where:I = 0x7f0c005d

.field public static final loading:I = 0x7f0c008a

.field public static final loading_failed:I = 0x7f0c0003

.field public static final lunar_date_formatter:I = 0x7f0c002e

.field public static final lunar_day:I = 0x7f0c0035

.field public static final lunar_fest_chongyang:I = 0x7f0c0023

.field public static final lunar_fest_chunjie:I = 0x7f0c001b

.field public static final lunar_fest_duanwu:I = 0x7f0c001c

.field public static final lunar_fest_ertong:I = 0x7f0c0029

.field public static final lunar_fest_funv:I = 0x7f0c0026

.field public static final lunar_fest_guoqing:I = 0x7f0c0020

.field public static final lunar_fest_jiandang:I = 0x7f0c002a

.field public static final lunar_fest_jianjun:I = 0x7f0c002b

.field public static final lunar_fest_jiaoshi:I = 0x7f0c002c

.field public static final lunar_fest_laodong:I = 0x7f0c001f

.field public static final lunar_fest_qingnian:I = 0x7f0c0024

.field public static final lunar_fest_qingren:I = 0x7f0c0025

.field public static final lunar_fest_qixi:I = 0x7f0c0022

.field public static final lunar_fest_shengdan:I = 0x7f0c002d

.field public static final lunar_fest_yuandan:I = 0x7f0c001e

.field public static final lunar_fest_yuanxiao:I = 0x7f0c0021

.field public static final lunar_fest_yuren:I = 0x7f0c0028

.field public static final lunar_fest_zhishu:I = 0x7f0c0027

.field public static final lunar_fest_zhongqiu:I = 0x7f0c001d

.field public static final lunar_leap:I = 0x7f0c002f

.field public static final lunar_month:I = 0x7f0c0034

.field public static final lunar_radio_text:I = 0x7f0c000f

.field public static final lunar_tenth_day:I = 0x7f0c0030

.field public static final lunar_thirtieth_day:I = 0x7f0c0032

.field public static final lunar_twentieth_day:I = 0x7f0c0031

.field public static final lunar_year:I = 0x7f0c0033

.field public static final menu_about_preferences:I = 0x7f0c00a9

.field public static final menu_general_preferences:I = 0x7f0c00a8

.field public static final menu_preferences:I = 0x7f0c0050

.field public static final menu_select_visible_calendars:I = 0x7f0c0051

.field public static final modify_all:I = 0x7f0c00a1

.field public static final modify_all_following:I = 0x7f0c00a2

.field public static final modify_event:I = 0x7f0c00a0

.field public static final month_view:I = 0x7f0c004a

.field public static final monthly:I = 0x7f0c009b

.field public static final monthly_on_day:I = 0x7f0c009d

.field public static final monthly_on_day_count:I = 0x7f0c009a

.field public static final more_string:I = 0x7f0c00e2

.field public static final new_event_dialog_label:I = 0x7f0c00a3

.field public static final new_event_dialog_option:I = 0x7f0c00a4

.field public static final no_calendars_found:I = 0x7f0c0072

.field public static final no_syncable_calendars:I = 0x7f0c0071

.field public static final no_title_label:I = 0x7f0c0044

.field public static final not_synced:I = 0x7f0c0057

.field public static final operation_failed:I = 0x7f0c0004

.field public static final picker_change_date:I = 0x7f0c001a

.field public static final prefDefault_alerts_vibrateWhen:I = 0x7f0c00c2

.field public static final prefDefault_alerts_vibrate_false:I = 0x7f0c00c4

.field public static final prefDefault_alerts_vibrate_true:I = 0x7f0c00c3

.field public static final prefDialogTitle_vibrateWhen:I = 0x7f0c00c5

.field public static final preferences_about_title:I = 0x7f0c00c0

.field public static final preferences_alerts_popup_title:I = 0x7f0c00b7

.field public static final preferences_alerts_ringtone_title:I = 0x7f0c00b6

.field public static final preferences_alerts_title:I = 0x7f0c00b4

.field public static final preferences_alerts_vibrateWhen_title:I = 0x7f0c00b5

.field public static final preferences_build_version:I = 0x7f0c00c1

.field public static final preferences_clear_search_history_summary:I = 0x7f0c00b2

.field public static final preferences_clear_search_history_title:I = 0x7f0c00b1

.field public static final preferences_default_reminder_default:I = 0x7f0c00ba

.field public static final preferences_default_reminder_dialog:I = 0x7f0c00b9

.field public static final preferences_default_reminder_title:I = 0x7f0c00b8

.field public static final preferences_general_title:I = 0x7f0c00ab

.field public static final preferences_hide_declined_title:I = 0x7f0c00ad

.field public static final preferences_home_tz_default:I = 0x7f0c00be

.field public static final preferences_home_tz_title:I = 0x7f0c00bd

.field public static final preferences_reminder_title:I = 0x7f0c00ac

.field public static final preferences_show_week_num_title:I = 0x7f0c00bf

.field public static final preferences_title:I = 0x7f0c00aa

.field public static final preferences_use_home_tz_descrip:I = 0x7f0c00bc

.field public static final preferences_use_home_tz_title:I = 0x7f0c00bb

.field public static final preferences_week_start_day_default:I = 0x7f0c00b0

.field public static final preferences_week_start_day_dialog:I = 0x7f0c00af

.field public static final preferences_week_start_day_title:I = 0x7f0c00ae

.field public static final presence_label:I = 0x7f0c006e

.field public static final privacy_label:I = 0x7f0c006f

.field public static final quick_response_custom_msg:I = 0x7f0c0084

.field public static final quick_response_dialog_title:I = 0x7f0c0082

.field public static final quick_response_email_failed:I = 0x7f0c0083

.field public static final quick_response_error_already_exist:I = 0x7f0c0036

.field public static final quick_response_error_null:I = 0x7f0c0037

.field public static final quick_response_settings:I = 0x7f0c007e

.field public static final quick_response_settings_edit_title:I = 0x7f0c0081

.field public static final quick_response_settings_summary:I = 0x7f0c007f

.field public static final quick_response_settings_title:I = 0x7f0c0080

.field public static final reminders_label:I = 0x7f0c0070

.field public static final repeats_label:I = 0x7f0c0043

.field public static final response_maybe:I = 0x7f0c0079

.field public static final response_no:I = 0x7f0c007a

.field public static final response_yes:I = 0x7f0c0078

.field public static final save_label:I = 0x7f0c0091

.field public static final saving_event:I = 0x7f0c0061

.field public static final saving_event_with_guest:I = 0x7f0c0064

.field public static final search:I = 0x7f0c0052

.field public static final search_history_cleared:I = 0x7f0c00b3

.field public static final search_setting_description:I = 0x7f0c000e

.field public static final search_title:I = 0x7f0c008d

.field public static final select_clear_calendars_title:I = 0x7f0c0006

.field public static final select_synced_calendars_button:I = 0x7f0c005b

.field public static final select_synced_calendars_title:I = 0x7f0c005a

.field public static final select_visible_calendars_title:I = 0x7f0c0055

.field public static final shareEvent:I = 0x7f0c0001

.field public static final share_label:I = 0x7f0c0002

.field public static final show_controls:I = 0x7f0c0054

.field public static final show_day_view:I = 0x7f0c0046

.field public static final show_newer_events:I = 0x7f0c008c

.field public static final show_older_events:I = 0x7f0c008b

.field public static final snooze_all_label:I = 0x7f0c0093

.field public static final snooze_label:I = 0x7f0c0095

.field public static final synced:I = 0x7f0c0056

.field public static final tardis:I = 0x7f0c00c8

.field public static final template_announce_item_index:I = 0x7f0c00de

.field public static final timezone_label:I = 0x7f0c003c

.field public static final today:I = 0x7f0c003e

.field public static final today_at_time_fmt:I = 0x7f0c0040

.field public static final tomorrow:I = 0x7f0c003f

.field public static final tomorrow_at_time_fmt:I = 0x7f0c0041

.field public static final unread_reminders:I = 0x7f0c000c

.field public static final valid_date_range:I = 0x7f0c0000

.field public static final view_event_calendar_label:I = 0x7f0c0075

.field public static final view_event_organizer_label:I = 0x7f0c0076

.field public static final view_event_response_label:I = 0x7f0c0077

.field public static final wait_deleting_tip:I = 0x7f0c000a

.field public static final week_view:I = 0x7f0c0049

.field public static final weekly:I = 0x7f0c0099

.field public static final what_label:I = 0x7f0c0039

.field public static final when_label:I = 0x7f0c003b

.field public static final where_label:I = 0x7f0c003a

.field public static final yearly:I = 0x7f0c009e

.field public static final yearly_plain:I = 0x7f0c009c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
