.class public final Lcom/android/ex/chips/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final CalendarActionBarStyle:I = 0x7f0e0018

.field public static final CalendarTheme:I = 0x7f0e0016

.field public static final CalendarTheme_WithActionBar:I = 0x7f0e0017

.field public static final CalendarTheme_WithActionBarWallpaper:I = 0x7f0e0019

.field public static final EditEventCustomActionButton:I = 0x7f0e001c

.field public static final EditEventCustomActionButtonImage:I = 0x7f0e001d

.field public static final EditEventCustomActionButtonText:I = 0x7f0e001e

.field public static final EditEventSeparator:I = 0x7f0e001b

.field public static final EditEvent_Layout:I = 0x7f0e0014

.field public static final MinusButton:I = 0x7f0e0000

.field public static final MonthView_DayLabel:I = 0x7f0e0001

.field public static final MonthView_MiniMonthLabel:I = 0x7f0e0002

.field public static final MultiStateButton:I = 0x7f0e001a

.field public static final NotificationPrimaryText:I = 0x7f0e0021

.field public static final NotificationSecondaryText:I = 0x7f0e0022

.field public static final RecipientEditTextView:I = 0x7f0e0023

.field public static final TextAppearance:I = 0x7f0e0003

.field public static final TextAppearance_AgendaView_ValueLabel:I = 0x7f0e0007

.field public static final TextAppearance_Date_Range_Title:I = 0x7f0e0015

.field public static final TextAppearance_EditEvent:I = 0x7f0e000c

.field public static final TextAppearance_EditEventCalSpinner:I = 0x7f0e000e

.field public static final TextAppearance_EditEvent_Button:I = 0x7f0e0009

.field public static final TextAppearance_EditEvent_CalSpinnerValue:I = 0x7f0e000f

.field public static final TextAppearance_EditEvent_Label:I = 0x7f0e0008

.field public static final TextAppearance_EditEvent_LabelSmall:I = 0x7f0e000a

.field public static final TextAppearance_EditEvent_Small:I = 0x7f0e000b

.field public static final TextAppearance_EditEvent_Spinner:I = 0x7f0e0010

.field public static final TextAppearance_EditEvent_SpinnerButton:I = 0x7f0e0011

.field public static final TextAppearance_EditEvent_Value:I = 0x7f0e000d

.field public static final TextAppearance_EditEvent_homeTime:I = 0x7f0e0012

.field public static final TextAppearance_EventInfo_Label:I = 0x7f0e0013

.field public static final TextAppearance_MonthView_DayLabel:I = 0x7f0e0006

.field public static final TextAppearance_MonthView_MiniDayLabel:I = 0x7f0e0005

.field public static final TextAppearance_SelectCalendar_Name:I = 0x7f0e0004

.field public static final WidgetDateStyle:I = 0x7f0e0020

.field public static final WidgetDayOfWeekStyle:I = 0x7f0e001f


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
