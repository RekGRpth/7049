.class Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment$2;
.super Ljava/lang/Object;
.source "SelectClearableCalendarsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;


# direct methods
.method constructor <init>(Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment$2;->this$0:Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const-string v1, "Calendar"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected view called: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/calendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    const-string v1, "Calendar"

    const-string v2, "Clear all events, ok"

    invoke-static {v1, v2}, Lcom/mediatek/calendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment$2;->this$0:Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;

    invoke-static {v2}, Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;->access$500(Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;)Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0c0090

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0c0007

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment$2;->this$0:Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;

    invoke-static {v2}, Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;->access$500(Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;)Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0c0009

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment$2;->this$0:Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;

    invoke-static {v3}, Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;->access$600(Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iget-object v1, p0, Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment$2;->this$0:Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;

    invoke-static {v1, v0}, Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;->access$702(Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_1
    const-string v1, "Calendar"

    const-string v2, "Clear all events, cancel"

    invoke-static {v1, v2}, Lcom/mediatek/calendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment$2;->this$0:Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;

    invoke-static {v1}, Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;->access$500(Lcom/mediatek/calendar/clearevents/SelectClearableCalendarsFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f1000b9
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
