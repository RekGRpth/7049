.class public Lcom/mediatek/calendar/extension/CalendarThemeExt;
.super Ljava/lang/Object;
.source "CalendarThemeExt.java"

# interfaces
.implements Lcom/mediatek/calendar/extension/ICalendarThemeExt;


# static fields
.field private static final THEME_COLOR_DEFAULT:I = -0x19cc4a1b


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/calendar/extension/CalendarThemeExt;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getThemeColor()I
    .locals 3

    iget-object v2, p0, Lcom/mediatek/calendar/extension/CalendarThemeExt;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v0

    if-nez v0, :cond_0

    const v0, -0x19cc4a1b

    :cond_0
    return v0
.end method

.method public isThemeManagerEnable()Z
    .locals 1

    invoke-static {}, Lcom/mediatek/calendar/features/Features;->isThemeManagerEnabled()Z

    move-result v0

    return v0
.end method
