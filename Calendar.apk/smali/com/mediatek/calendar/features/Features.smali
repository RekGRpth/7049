.class public Lcom/mediatek/calendar/features/Features;
.super Ljava/lang/Object;
.source "Features.java"


# static fields
.field private static final COMMON_FEATURES:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final FEATURE_CLEAR_ALL_EVENTS:I = 0x3

.field private static final FEATURE_THEME_MANAGER:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/mediatek/calendar/features/Features;->COMMON_FEATURES:Ljava/util/ArrayList;

    sget-object v0, Lcom/mediatek/calendar/features/Features;->COMMON_FEATURES:Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isFeatureEnabled(I)Z
    .locals 2
    .param p0    # I

    sget-object v0, Lcom/mediatek/calendar/features/Features;->COMMON_FEATURES:Ljava/util/ArrayList;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isThemeManagerEnabled()Z
    .locals 1

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/mediatek/calendar/features/Features;->isFeatureEnabled(I)Z

    move-result v0

    return v0
.end method
