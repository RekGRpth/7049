.class Lcom/mediatek/calendar/lunar/LunarDatePicker$1;
.super Ljava/lang/Object;
.source "LunarDatePicker.java"

# interfaces
.implements Landroid/widget/NumberPicker$OnValueChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/calendar/lunar/LunarDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;


# direct methods
.method constructor <init>(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValueChange(Landroid/widget/NumberPicker;II)V
    .locals 12
    .param p1    # Landroid/widget/NumberPicker;
    .param p2    # I
    .param p3    # I

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$000(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v8}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$100(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v7

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v7

    add-int/lit8 v1, v7, 0x1

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v7

    const/4 v8, 0x5

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    invoke-virtual {v7, v2, v1, v0}, Lcom/mediatek/calendar/lunar/LunarUtil;->calculateLunarByGregorian(III)[I

    move-result-object v4

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$400(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Landroid/widget/NumberPicker;

    move-result-object v7

    if-ne p1, v7, :cond_2

    const/16 v7, 0x1b

    if-le p2, v7, :cond_0

    const/4 v7, 0x1

    if-ne p3, v7, :cond_0

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v7

    const/4 v8, 0x5

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->add(II)V

    :goto_0
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    iget-object v8, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v8}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v8

    iget-object v9, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v9}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v9

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Ljava/util/Calendar;->get(I)I

    move-result v9

    iget-object v10, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v10}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v10

    const/4 v11, 0x5

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v10

    invoke-static {v7, v8, v9, v10}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$700(Lcom/mediatek/calendar/lunar/LunarDatePicker;III)V

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$800(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$900(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$1000(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V

    return-void

    :cond_0
    const/4 v7, 0x1

    if-ne p2, v7, :cond_1

    const/16 v7, 0x1b

    if-le p3, v7, :cond_1

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v7

    const/4 v8, 0x5

    const/4 v9, -0x1

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v7

    const/4 v8, 0x5

    sub-int v9, p3, p2

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$500(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Landroid/widget/NumberPicker;

    move-result-object v7

    if-ne p1, v7, :cond_e

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xa

    if-le p2, v7, :cond_4

    if-nez p3, :cond_4

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Lcom/mediatek/calendar/lunar/LunarUtil;->leapMonth(I)I

    move-result v3

    const/16 v7, 0xc

    if-ne v3, v7, :cond_3

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfLeapMonthInLunarYear(I)I

    move-result v5

    :goto_1
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v7

    const/4 v8, 0x5

    invoke-virtual {v7, v8, v5}, Ljava/util/Calendar;->add(II)V

    goto/16 :goto_0

    :cond_3
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    const/16 v9, 0xc

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v5

    goto :goto_1

    :cond_4
    if-nez p2, :cond_6

    const/16 v7, 0xa

    if-le p3, v7, :cond_6

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Lcom/mediatek/calendar/lunar/LunarUtil;->leapMonth(I)I

    move-result v3

    const/16 v7, 0xc

    if-ne v3, v7, :cond_5

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfLeapMonthInLunarYear(I)I

    move-result v5

    :goto_2
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v7

    const/4 v8, 0x5

    neg-int v9, v5

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->add(II)V

    goto/16 :goto_0

    :cond_5
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    add-int/lit8 v8, v8, -0x1

    const/16 v9, 0xc

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v5

    goto :goto_2

    :cond_6
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Lcom/mediatek/calendar/lunar/LunarUtil;->leapMonth(I)I

    move-result v3

    sub-int v7, p3, p2

    if-gez v7, :cond_a

    if-nez v3, :cond_7

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    add-int/lit8 v9, p3, 0x1

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v5

    :goto_3
    neg-int v5, v5

    :goto_4
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v7

    const/4 v8, 0x5

    invoke-virtual {v7, v8, v5}, Ljava/util/Calendar;->add(II)V

    goto/16 :goto_0

    :cond_7
    if-ge p3, v3, :cond_8

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    add-int/lit8 v9, p3, 0x1

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v5

    goto :goto_3

    :cond_8
    if-ne p3, v3, :cond_9

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfLeapMonthInLunarYear(I)I

    move-result v5

    goto :goto_3

    :cond_9
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    invoke-virtual {v7, v8, p3}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v5

    goto :goto_3

    :cond_a
    if-nez v3, :cond_b

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    add-int/lit8 v9, p2, 0x1

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v5

    goto :goto_4

    :cond_b
    if-ge p2, v3, :cond_c

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    add-int/lit8 v9, p2, 0x1

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v5

    goto :goto_4

    :cond_c
    if-ne p2, v3, :cond_d

    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfLeapMonthInLunarYear(I)I

    move-result v5

    goto :goto_4

    :cond_d
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    invoke-virtual {v7, v8, p2}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v5

    goto :goto_4

    :cond_e
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v7}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$600(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Landroid/widget/NumberPicker;

    move-result-object v7

    if-ne p1, v7, :cond_10

    sub-int v7, p3, p2

    if-lez v7, :cond_f

    const/4 v6, 0x1

    :goto_5
    iget-object v7, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    iget-object v8, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v8}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v9}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v9

    const/4 v10, 0x1

    aget v10, v4, v10

    const/4 v11, 0x2

    aget v11, v4, v11

    invoke-virtual {v8, v9, v10, v11, v6}, Lcom/mediatek/calendar/lunar/LunarUtil;->decreaseOrIncreaseALunarYear(Ljava/util/Calendar;III)Ljava/util/Calendar;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$202(Lcom/mediatek/calendar/lunar/LunarDatePicker;Ljava/util/Calendar;)Ljava/util/Calendar;

    goto/16 :goto_0

    :cond_f
    const/4 v6, -0x1

    goto :goto_5

    :cond_10
    new-instance v7, Ljava/lang/IllegalArgumentException;

    invoke-direct {v7}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v7
.end method
