.class public Lcom/mediatek/calendar/lunar/LunarDatePicker;
.super Landroid/widget/FrameLayout;
.source "LunarDatePicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;,
        Lcom/mediatek/calendar/lunar/LunarDatePicker$OnDateChangedListener;
    }
.end annotation


# static fields
.field private static final DATE_FORMAT:Ljava/lang/String; = "MM/dd/yyyy"

.field private static final DEFAULT_CALENDAR_VIEW_SHOWN:Z = true

.field private static final DEFAULT_ENABLED_STATE:Z = true

.field private static final DEFAULT_END_YEAR:I = 0x7f4

.field private static final DEFAULT_SPINNERS_SHOWN:Z = true

.field private static final DEFAULT_START_YEAR:I = 0x7b2

.field private static final PICKER_CHILD_COUNT:I = 0x1

.field private static final PICKER_CHILD_INPUT_TEXT_INDEX:I

.field private static final TAG:Ljava/lang/String;

.field private static sMaxDate:Ljava/util/Calendar;

.field private static sMinDate:Ljava/util/Calendar;


# instance fields
.field private final mCalendarView:Landroid/widget/CalendarView;

.field private mCurrentDate:Ljava/util/Calendar;

.field private mCurrentLocale:Ljava/util/Locale;

.field private final mDateFormat:Ljava/text/DateFormat;

.field private final mDaySpinner:Landroid/widget/NumberPicker;

.field private final mDaySpinnerInput:Landroid/widget/EditText;

.field private mIsEnabled:Z

.field private mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

.field private final mLunarLeapWordText:Ljava/lang/String;

.field private final mLunarMonthNumberTextArray:[Ljava/lang/String;

.field private final mLunarMonthTextArray:[Ljava/lang/String;

.field private final mMonthSpinner:Landroid/widget/NumberPicker;

.field private final mMonthSpinnerInput:Landroid/widget/EditText;

.field private mNumberOfMonths:I

.field private mOnDateChangedListener:Lcom/mediatek/calendar/lunar/LunarDatePicker$OnDateChangedListener;

.field private final mSpinners:Landroid/widget/LinearLayout;

.field private mTempDate:Ljava/util/Calendar;

.field private final mYearSpinner:Landroid/widget/NumberPicker;

.field private final mYearSpinnerInput:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v2, 0x0

    const-class v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->TAG:Ljava/lang/String;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    sget-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    const/16 v1, 0x7b2

    const/4 v3, 0x1

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    sget-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    const/16 v1, 0x7f4

    const/16 v2, 0xb

    const/16 v3, 0x1f

    const/16 v4, 0x17

    const/16 v5, 0x3b

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v0, 0x7f010006

    invoke-direct {p0, p1, p2, v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct/range {p0 .. p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "MM/dd/yyyy"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDateFormat:Ljava/text/DateFormat;

    const/16 v0, 0xc

    iput v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mNumberOfMonths:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mIsEnabled:Z

    invoke-static {p1}, Lcom/mediatek/calendar/lunar/LunarUtil;->getInstance(Landroid/content/Context;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v0, 0x7f070018

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunarMonthTextArray:[Ljava/lang/String;

    const v0, 0x7f070017

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunarMonthNumberTextArray:[Ljava/lang/String;

    const v0, 0x7f0c002f

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunarLeapWordText:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setCurrentLocale(Ljava/util/Locale;)V

    const/4 v11, 0x1

    const/4 v6, 0x1

    const/16 v12, 0x7b2

    const/16 v1, 0x7f4

    const v8, 0x7f04001a

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    const/4 v0, 0x1

    invoke-virtual {v7, v8, p0, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    new-instance v9, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;

    invoke-direct {v9, p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;-><init>(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V

    const v0, 0x7f10003b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mSpinners:Landroid/widget/LinearLayout;

    const v0, 0x7f10003e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CalendarView;

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCalendarView:Landroid/widget/CalendarView;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCalendarView:Landroid/widget/CalendarView;

    new-instance v2, Lcom/mediatek/calendar/lunar/LunarDatePicker$2;

    invoke-direct {v2, p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker$2;-><init>(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V

    invoke-virtual {v0, v2}, Landroid/widget/CalendarView;->setOnDateChangeListener(Landroid/widget/CalendarView$OnDateChangeListener;)V

    const v0, 0x7f10000d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v9}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    invoke-virtual {p0, v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getNumberPickerInputText(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinnerInput:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinnerInput:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinnerInput:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    :goto_0
    const v0, 0x7f10003c

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->setMinValue(I)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    iget v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mNumberOfMonths:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunarMonthTextArray:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v9}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    invoke-virtual {p0, v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getNumberPickerInputText(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinnerInput:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinnerInput:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinnerInput:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    :goto_1
    const v0, 0x7f10003d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v9}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    invoke-virtual {p0, v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getNumberPickerInputText(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinnerInput:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinnerInput:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinnerInput:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    :goto_2
    if-nez v11, :cond_3

    if-nez v6, :cond_3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setSpinnersShown(Z)V

    :goto_3
    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v12, v2, v3}, Ljava/util/Calendar;->set(III)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setMinDate(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0x1f

    const/16 v4, 0x17

    const/16 v5, 0x3b

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setMaxDate(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->init(IIILcom/mediatek/calendar/lunar/LunarDatePicker$OnDateChangedListener;)V

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->reorderSpinners()V

    return-void

    :cond_0
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinnerInput:Landroid/widget/EditText;

    sget-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->TAG:Ljava/lang/String;

    const-string v2, "mDaySpinner.getChildCount() != 3,It isn\'t init ok."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinnerInput:Landroid/widget/EditText;

    sget-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->TAG:Ljava/lang/String;

    const-string v2, "mMonthSpinner.getChildCount() != 3,It isn\'t init ok."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_2
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinnerInput:Landroid/widget/EditText;

    sget-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->TAG:Ljava/lang/String;

    const-string v2, "mYearSpinner.getChildCount() != 3,It isn\'t init ok."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_3
    invoke-virtual {p0, v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setSpinnersShown(Z)V

    invoke-virtual {p0, v6}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setCalendarViewShown(Z)V

    goto/16 :goto_3
.end method

.method static synthetic access$000(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V
    .locals 0
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateInputState()V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;
    .locals 1
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V
    .locals 0
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->notifyDateChanged()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;
    .locals 1
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/calendar/lunar/LunarDatePicker;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;
    .param p1    # Ljava/util/Calendar;

    iput-object p1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    return-object p1
.end method

.method static synthetic access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;
    .locals 1
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Landroid/widget/NumberPicker;
    .locals 1
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Landroid/widget/NumberPicker;
    .locals 1
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Landroid/widget/NumberPicker;
    .locals 1
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/calendar/lunar/LunarDatePicker;III)V
    .locals 0
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setDate(III)V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V
    .locals 0
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateSpinners()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V
    .locals 0
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateCalendarView()V

    return-void
.end method

.method private getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;
    .locals 3
    .param p1    # Ljava/util/Calendar;
    .param p2    # Ljava/util/Locale;

    if-nez p1, :cond_0

    invoke-static {p2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {p2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0
.end method

.method private isNewDate(III)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v1, p3, :cond_0

    iget-object v1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-eq v1, p2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyDateChanged()V
    .locals 4

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mOnDateChangedListener:Lcom/mediatek/calendar/lunar/LunarDatePicker$OnDateChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mOnDateChangedListener:Lcom/mediatek/calendar/lunar/LunarDatePicker$OnDateChangedListener;

    invoke-virtual {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getYear()I

    move-result v1

    invoke-virtual {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getMonth()I

    move-result v2

    invoke-virtual {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getDayOfMonth()I

    move-result v3

    invoke-interface {v0, p0, v1, v2, v3}, Lcom/mediatek/calendar/lunar/LunarDatePicker$OnDateChangedListener;->onDateChanged(Lcom/mediatek/calendar/lunar/LunarDatePicker;III)V

    :cond_0
    return-void
.end method

.method private reorderSpinners()V
    .locals 5

    iget-object v3, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mSpinners:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-char v3, v1, v0

    sparse-switch v3, :sswitch_data_0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    :sswitch_0
    iget-object v3, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mSpinners:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    invoke-direct {p0, v3, v2, v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setImeOptions(Landroid/widget/NumberPicker;II)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :sswitch_1
    iget-object v3, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mSpinners:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    invoke-direct {p0, v3, v2, v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setImeOptions(Landroid/widget/NumberPicker;II)V

    goto :goto_1

    :sswitch_2
    iget-object v3, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mSpinners:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    invoke-direct {p0, v3, v2, v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setImeOptions(Landroid/widget/NumberPicker;II)V

    goto :goto_1

    :cond_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x4d -> :sswitch_1
        0x64 -> :sswitch_0
        0x79 -> :sswitch_2
    .end sparse-switch
.end method

.method private setCurrentLocale(Ljava/util/Locale;)V
    .locals 1
    .param p1    # Ljava/util/Locale;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentLocale:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentLocale:Ljava/util/Locale;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-direct {p0, v0, p1}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    sget-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    invoke-direct {p0, v0, p1}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    sget-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    invoke-direct {p0, v0, p1}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    invoke-direct {p0, v0, p1}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    goto :goto_0
.end method

.method private setDate(III)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0
.end method

.method private setImeOptions(Landroid/widget/NumberPicker;II)V
    .locals 4
    .param p1    # Landroid/widget/NumberPicker;
    .param p2    # I
    .param p3    # I

    add-int/lit8 v2, p2, -0x1

    if-ge p3, v2, :cond_0

    const/4 v0, 0x5

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    sget-object v2, Lcom/mediatek/calendar/lunar/LunarDatePicker;->TAG:Ljava/lang/String;

    const-string v3, "spinner.getChildCount() != 3,It isn\'t init ok.return"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x6

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setImeOptions(I)V

    goto :goto_1
.end method

.method private updateCalendarView()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCalendarView:Landroid/widget/CalendarView;

    iget-object v1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/CalendarView;->setDate(JZZ)V

    return-void
.end method

.method private updateInputState()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinnerInput:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinnerInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinnerInput:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinnerInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinnerInput:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinnerInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method private updateSpinners()V
    .locals 29

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/Calendar;->get(I)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/Calendar;->get(I)I

    move-result v26

    add-int/lit8 v7, v26, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    move-object/from16 v26, v0

    const/16 v27, 0x5

    invoke-virtual/range {v26 .. v27}, Ljava/util/Calendar;->get(I)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v8, v7, v6}, Lcom/mediatek/calendar/lunar/LunarUtil;->calculateLunarByGregorian(III)[I

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aget v27, v12, v27

    invoke-virtual/range {v26 .. v27}, Lcom/mediatek/calendar/lunar/LunarUtil;->leapMonth(I)I

    move-result v11

    const/16 v26, 0x1

    aget v25, v12, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/mediatek/calendar/lunar/LunarUtil;->getLunarDateString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v13

    if-nez v11, :cond_2

    add-int/lit8 v25, v25, -0x1

    :cond_0
    :goto_0
    if-eqz v11, :cond_4

    const/16 v26, 0xd

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mNumberOfMonths:I

    const/4 v10, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aget v27, v12, v27

    const/16 v28, 0x1

    aget v28, v12, v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v24

    if-eqz v11, :cond_1

    move/from16 v0, v25

    if-ne v0, v11, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunarLeapWordText:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aget v27, v12, v27

    invoke-virtual/range {v26 .. v27}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfLeapMonthInLunarYear(I)I

    move-result v24

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    move-object/from16 v26, v0

    sget-object v27, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    invoke-virtual/range {v26 .. v27}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    aget v27, v12, v27

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setMinValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMinValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mNumberOfMonths:I

    move/from16 v27, v0

    add-int/lit8 v27, v27, -0x1

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mNumberOfMonths:I

    move/from16 v26, v0

    move/from16 v0, v26

    new-array v5, v0, [Ljava/lang/String;

    if-eqz v10, :cond_8

    const/4 v9, 0x0

    :goto_3
    if-ge v9, v11, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunarMonthTextArray:[Ljava/lang/String;

    move-object/from16 v26, v0

    aget-object v26, v26, v9

    aput-object v26, v5, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :cond_2
    move/from16 v0, v25

    if-ge v0, v11, :cond_3

    if-eqz v11, :cond_3

    add-int/lit8 v25, v25, -0x1

    goto/16 :goto_0

    :cond_3
    move/from16 v0, v25

    if-ne v0, v11, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunarLeapWordText:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_0

    add-int/lit8 v25, v25, -0x1

    goto/16 :goto_0

    :cond_4
    const/16 v26, 0xc

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mNumberOfMonths:I

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    move-object/from16 v26, v0

    sget-object v27, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    invoke-virtual/range {v26 .. v27}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setMinValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    aget v27, v12, v27

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setMinValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    goto/16 :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setMinValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setMinValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mNumberOfMonths:I

    move/from16 v27, v0

    add-int/lit8 v27, v27, -0x1

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    goto/16 :goto_2

    :cond_7
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunarLeapWordText:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunarMonthNumberTextArray:[Ljava/lang/String;

    move-object/from16 v27, v0

    add-int/lit8 v28, v11, -0x1

    aget-object v27, v27, v28

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v5, v11

    add-int/lit8 v9, v9, 0x1

    :goto_4
    const/16 v26, 0xd

    move/from16 v0, v26

    if-ge v9, v0, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunarMonthTextArray:[Ljava/lang/String;

    move-object/from16 v26, v0

    add-int/lit8 v27, v9, -0x1

    aget-object v26, v26, v27

    aput-object v26, v5, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunarMonthTextArray:[Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/NumberPicker;->getMinValue()I

    move-result v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/widget/NumberPicker;->getMaxValue()I

    move-result v28

    add-int/lit8 v28, v28, 0x1

    invoke-static/range {v26 .. v28}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/widget/NumberPicker;->getMaxValue()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/widget/NumberPicker;->getMinValue()I

    move-result v19

    sub-int v26, v14, v19

    add-int/lit8 v26, v26, 0x1

    move/from16 v0, v26

    new-array v4, v0, [Ljava/lang/String;

    move/from16 v9, v19

    :goto_5
    if-gt v9, v14, :cond_a

    sub-int v26, v9, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Lcom/mediatek/calendar/lunar/LunarUtil;->getLunarDayString(I)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v4, v26

    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    sget-object v26, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/Calendar;->get(I)I

    move-result v22

    sget-object v26, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/Calendar;->get(I)I

    move-result v26

    add-int/lit8 v21, v26, 0x1

    sget-object v26, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    const/16 v27, 0x5

    invoke-virtual/range {v26 .. v27}, Ljava/util/Calendar;->get(I)I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v22

    move/from16 v2, v21

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediatek/calendar/lunar/LunarUtil;->calculateLunarByGregorian(III)[I

    move-result-object v23

    sget-object v26, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/Calendar;->get(I)I

    move-result v17

    sget-object v26, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/Calendar;->get(I)I

    move-result v26

    add-int/lit8 v16, v26, 0x1

    sget-object v26, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    const/16 v27, 0x5

    invoke-virtual/range {v26 .. v27}, Ljava/util/Calendar;->get(I)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v17

    move/from16 v2, v16

    move/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediatek/calendar/lunar/LunarUtil;->calculateLunarByGregorian(III)[I

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aget v27, v23, v27

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setMinValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aget v27, v18, v27

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aget v27, v12, v27

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    aget v27, v12, v27

    invoke-virtual/range {v26 .. v27}, Landroid/widget/NumberPicker;->setValue(I)V

    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {p0, p1}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    return-void
.end method

.method public getCalendarView()Landroid/widget/CalendarView;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCalendarView:Landroid/widget/CalendarView;

    return-object v0
.end method

.method public getCalendarViewShown()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCalendarView:Landroid/widget/CalendarView;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    return v0
.end method

.method public getDateString()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mLunar:Lcom/mediatek/calendar/lunar/LunarUtil;

    iget-object v1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Lcom/mediatek/calendar/lunar/LunarUtil;->getLunarDateString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDayOfMonth()I
    .locals 2

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public getMaxDate()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCalendarView:Landroid/widget/CalendarView;

    invoke-virtual {v0}, Landroid/widget/CalendarView;->getMaxDate()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMinDate()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCalendarView:Landroid/widget/CalendarView;

    invoke-virtual {v0}, Landroid/widget/CalendarView;->getMinDate()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMonth()I
    .locals 2

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public getNumberPickerInputText(Landroid/widget/NumberPicker;)Landroid/widget/EditText;
    .locals 1
    .param p1    # Landroid/widget/NumberPicker;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method public getSpinnersShown()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mSpinners:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    return v0
.end method

.method public getYear()I
    .locals 2

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public init(IIILcom/mediatek/calendar/lunar/LunarDatePicker$OnDateChangedListener;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/mediatek/calendar/lunar/LunarDatePicker$OnDateChangedListener;

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setDate(III)V

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateSpinners()V

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateCalendarView()V

    iput-object p4, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mOnDateChangedListener:Lcom/mediatek/calendar/lunar/LunarDatePicker$OnDateChangedListener;

    return-void
.end method

.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mIsEnabled:Z

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/view/View;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {p0, v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setCurrentLocale(Ljava/util/Locale;)V

    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const-class v0, Landroid/widget/DatePicker;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityRecord;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Landroid/widget/DatePicker;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 6
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-super {p0, p1}, Landroid/view/View;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const/16 v0, 0x14

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const/16 v5, 0x14

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;

    invoke-virtual {v0}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    invoke-static {v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->access$1200(Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;)I

    move-result v1

    invoke-static {v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->access$1300(Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;)I

    move-result v2

    invoke-static {v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->access$1400(Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;)I

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setDate(III)V

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateSpinners()V

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateCalendarView()V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    new-instance v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;

    invoke-virtual {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getYear()I

    move-result v2

    invoke-virtual {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getMonth()I

    move-result v3

    invoke-virtual {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->getDayOfMonth()I

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;-><init>(Landroid/os/Parcelable;IIILcom/mediatek/calendar/lunar/LunarDatePicker$1;)V

    return-object v0
.end method

.method public setCalendarViewShown(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCalendarView:Landroid/widget/CalendarView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mIsEnabled:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCalendarView:Landroid/widget/CalendarView;

    invoke-virtual {v0, p1}, Landroid/widget/CalendarView;->setEnabled(Z)V

    iput-boolean p1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mIsEnabled:Z

    goto :goto_0
.end method

.method public setMaxDate(J)V
    .locals 5
    .param p1    # J

    const/4 v4, 0x6

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMaxDate failed!:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCalendarView:Landroid/widget/CalendarView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/CalendarView;->setMaxDate(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMaxDate:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateCalendarView()V

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateSpinners()V

    goto :goto_0
.end method

.method public setMinDate(J)V
    .locals 5
    .param p1    # J

    const/4 v4, 0x6

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMinDate failed!:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mTempDate:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCalendarView:Landroid/widget/CalendarView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/CalendarView;->setMinDate(J)V

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mCurrentDate:Ljava/util/Calendar;

    sget-object v1, Lcom/mediatek/calendar/lunar/LunarDatePicker;->sMinDate:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateCalendarView()V

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateSpinners()V

    goto :goto_0
.end method

.method public setSpinnersShown(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker;->mSpinners:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public updateDate(III)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->isNewDate(III)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->setDate(III)V

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateSpinners()V

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->updateCalendarView()V

    invoke-direct {p0}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->notifyDateChanged()V

    goto :goto_0
.end method
