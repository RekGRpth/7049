.class public Lcom/mediatek/calendar/selectevent/AgendaChoiceActivity;
.super Landroid/app/Activity;
.source "AgendaChoiceActivity.java"

# interfaces
.implements Lcom/mediatek/calendar/extension/IAgendaChoiceForExt;


# static fields
.field private static final KEY_OTHER_APP_RESTORE_TIME:Ljava/lang/String; = "other_app_request_time"


# instance fields
.field private mController:Lcom/android/calendar/CalendarController;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private setFragments(J)V
    .locals 3
    .param p1    # J

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    new-instance v0, Lcom/mediatek/calendar/selectevent/EventSelectionFragment;

    invoke-direct {v0, p1, p2}, Lcom/mediatek/calendar/selectevent/EventSelectionFragment;-><init>(J)V

    const v2, 0x7f10000a

    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/android/calendar/CalendarController;->getInstance(Landroid/content/Context;)Lcom/android/calendar/CalendarController;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/calendar/selectevent/AgendaChoiceActivity;->mController:Lcom/android/calendar/CalendarController;

    const v2, 0x7f040005

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const-wide/16 v0, -0x1

    if-eqz p1, :cond_0

    const-string v2, "other_app_request_time"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/mediatek/calendar/selectevent/AgendaChoiceActivity;->setFragments(J)V

    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "other_app_request_time"

    iget-object v1, p0, Lcom/mediatek/calendar/selectevent/AgendaChoiceActivity;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarController;->getTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method

.method public retSelectedEvent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method
