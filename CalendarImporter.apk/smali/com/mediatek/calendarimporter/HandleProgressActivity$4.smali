.class Lcom/mediatek/calendarimporter/HandleProgressActivity$4;
.super Landroid/os/Handler;
.source "HandleProgressActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/calendarimporter/HandleProgressActivity;->serviceConnected(Lcom/mediatek/calendarimporter/service/VCalService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/calendarimporter/HandleProgressActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/calendarimporter/HandleProgressActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;->this$0:Lcom/mediatek/calendarimporter/HandleProgressActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v4, 0x1

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v1, "HandleProgressActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serviceConnected. ProcessorMsgType:PROCESSOR_EXCEPTION. type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/calendarimporter/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->arg2:I

    if-ne v4, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;->this$0:Lcom/mediatek/calendarimporter/HandleProgressActivity;

    invoke-virtual {v1, v4}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->showDialog(I)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;->this$0:Lcom/mediatek/calendarimporter/HandleProgressActivity;

    const v2, 0x7f04000f

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;->this$0:Lcom/mediatek/calendarimporter/HandleProgressActivity;

    invoke-virtual {v1}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->finish()V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;->this$0:Lcom/mediatek/calendarimporter/HandleProgressActivity;

    # getter for: Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->access$500(Lcom/mediatek/calendarimporter/HandleProgressActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;->this$0:Lcom/mediatek/calendarimporter/HandleProgressActivity;

    # getter for: Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->access$500(Lcom/mediatek/calendarimporter/HandleProgressActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_1
    const-string v1, "HandleProgressActivity"

    const-string v2, "serviceConnected,ProcessorMsgType:PROCESSOR_FINISH. Start result Activity."

    invoke-static {v1, v2}, Lcom/mediatek/calendarimporter/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;->this$0:Lcom/mediatek/calendarimporter/HandleProgressActivity;

    const-class v2, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "SucceedCnt"

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "totalCnt"

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "accountName"

    iget-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;->this$0:Lcom/mediatek/calendarimporter/HandleProgressActivity;

    # getter for: Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountName:Ljava/lang/String;
    invoke-static {v2}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->access$300(Lcom/mediatek/calendarimporter/HandleProgressActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "HandleProgressActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serviceConnected,ProcessorMsgType:PROCESSOR_FINISH. DtStart = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "eventStartTime"

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;->this$0:Lcom/mediatek/calendarimporter/HandleProgressActivity;

    invoke-virtual {v1, v0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;->this$0:Lcom/mediatek/calendarimporter/HandleProgressActivity;

    invoke-virtual {v1}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->finish()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
