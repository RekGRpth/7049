.class public Lcom/mediatek/calendarimporter/HandleProgressActivity;
.super Landroid/app/Activity;
.source "HandleProgressActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lcom/mediatek/calendarimporter/BindServiceHelper$ServiceConnectedOperation;


# static fields
.field private static final ACCOUNT_NAME:Ljava/lang/String; = "TargetAccountName"

.field private static final DATA_URI:Ljava/lang/String; = "DataUri"

.field private static final ID_DIALOG_NO_CALENDAR_ALERT:I = 0x1

.field private static final ID_DIALOG_PROGRESS_BAR:I = 0x2

.field private static final TAG:Ljava/lang/String; = "HandleProgressActivity"


# instance fields
.field private mAccountList:Landroid/widget/ListView;

.field private mAccountName:Ljava/lang/String;

.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mDataUri:Landroid/net/Uri;

.field private final mDialogListener:Landroid/content/DialogInterface$OnClickListener;

.field private mFirstEnter:Z

.field private mHandler:Landroid/os/Handler;

.field private mProcessor:Lcom/mediatek/calendarimporter/service/ImportProcessor;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mService:Lcom/mediatek/calendarimporter/service/VCalService;

.field private mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountList:Landroid/widget/ListView;

    new-instance v0, Lcom/mediatek/calendarimporter/HandleProgressActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity$1;-><init>(Lcom/mediatek/calendarimporter/HandleProgressActivity;)V

    iput-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/calendarimporter/HandleProgressActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/calendarimporter/HandleProgressActivity;

    invoke-direct {p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->addParseRequest()V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/calendarimporter/HandleProgressActivity;)Lcom/mediatek/calendarimporter/service/VCalService;
    .locals 1
    .param p0    # Lcom/mediatek/calendarimporter/HandleProgressActivity;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/calendarimporter/HandleProgressActivity;)Lcom/mediatek/calendarimporter/service/ImportProcessor;
    .locals 1
    .param p0    # Lcom/mediatek/calendarimporter/HandleProgressActivity;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProcessor:Lcom/mediatek/calendarimporter/service/ImportProcessor;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/calendarimporter/HandleProgressActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/calendarimporter/HandleProgressActivity;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/calendarimporter/HandleProgressActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/calendarimporter/HandleProgressActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/mediatek/calendarimporter/HandleProgressActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/mediatek/calendarimporter/HandleProgressActivity;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/calendarimporter/HandleProgressActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/mediatek/calendarimporter/HandleProgressActivity;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private addParseRequest()V
    .locals 4

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mDataUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const-string v0, "HandleProgressActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addParseRequest. AccountName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/calendarimporter/service/ImportProcessor;

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountName:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mDataUri:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/mediatek/calendarimporter/service/ImportProcessor;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Landroid/net/Uri;)V

    iput-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProcessor:Lcom/mediatek/calendarimporter/service/ImportProcessor;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProcessor:Lcom/mediatek/calendarimporter/service/ImportProcessor;

    invoke-virtual {v0, v1}, Lcom/mediatek/calendarimporter/service/VCalService;->tryExecuteProcessor(Lcom/mediatek/calendarimporter/service/BaseProcessor;)V

    :cond_0
    return-void
.end method

.method private getAccount()[Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_1

    aget-object v3, v0, v2

    invoke-static {v3}, Lcom/mediatek/calendarimporter/utils/Utils;->isExchangeOrGoogleAccount(Landroid/accounts/Account;)Z

    move-result v3

    if-eqz v3, :cond_0

    aget-object v3, v0, v2

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    return-object v3
.end method

.method private showAccountListView()V
    .locals 5

    const v4, 0x1060012

    const/high16 v3, 0x7f030000

    invoke-virtual {p0, v3}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->setContentView(I)V

    invoke-static {p0, v4}, Lcom/mediatek/calendarimporter/utils/Utils;->getThemeMainColor(Landroid/content/Context;I)I

    move-result v1

    if-eq v1, v4, :cond_0

    const/high16 v3, 0x7f050000

    invoke-virtual {p0, v3}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const v3, 0x7f050001

    invoke-virtual {p0, v3}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_0
    const v3, 0x7f050002

    invoke-virtual {p0, v3}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountList:Landroid/widget/ListView;

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v3, 0x7f030002

    invoke-direct {p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->getAccount()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, p0, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountList:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v3, -0x777778

    invoke-virtual {p0, v3}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->setTitleColor(I)V

    iget-object v3, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountList:Landroid/widget/ListView;

    new-instance v4, Lcom/mediatek/calendarimporter/HandleProgressActivity$2;

    invoke-direct {v4, p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity$2;-><init>(Lcom/mediatek/calendarimporter/HandleProgressActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    const-string v0, "HandleProgressActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBackPressed() + mService:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProcessor:Lcom/mediatek/calendarimporter/service/ImportProcessor;

    invoke-virtual {v0, v1}, Lcom/mediatek/calendarimporter/service/VCalService;->tryCancelProcessor(Lcom/mediatek/calendarimporter/service/BaseProcessor;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;

    invoke-virtual {p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->finish()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v1}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mDataUri:Landroid/net/Uri;

    iput-boolean v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mFirstEnter:Z

    invoke-static {p0}, Lcom/mediatek/calendarimporter/utils/Utils;->hasExchangeOrGoogleAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mediatek/calendarimporter/BindServiceHelper;

    invoke-direct {v0, p0}, Lcom/mediatek/calendarimporter/BindServiceHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;

    invoke-virtual {v0}, Lcom/mediatek/calendarimporter/BindServiceHelper;->onBindService()V

    invoke-direct {p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->showAccountListView()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "HandleProgressActivity"

    const-string v1, "onCreate, should not be created when no account exists."

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 8
    .param p1    # I

    const v7, 0x7f040014

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v2, "HandleProgressActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreateDialog,id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    if-ne v6, p1, :cond_1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f040011

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f020001

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f040012

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f040013

    iget-object v4, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v7, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAlertDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAlertDialog:Landroid/app/AlertDialog;

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/4 v2, 0x2

    if-ne v2, p1, :cond_0

    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const v3, 0x7f040008

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setTitle(I)V

    iget-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0, v7}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v4, Lcom/mediatek/calendarimporter/HandleProgressActivity$3;

    invoke-direct {v4, p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity$3;-><init>(Lcom/mediatek/calendarimporter/HandleProgressActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    const-string v0, "HandleProgressActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy() + mService:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;

    invoke-virtual {v0}, Lcom/mediatek/calendarimporter/BindServiceHelper;->unBindService()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/calendarimporter/BindServiceHelper;

    invoke-direct {v0, p0}, Lcom/mediatek/calendarimporter/BindServiceHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;

    invoke-virtual {v0}, Lcom/mediatek/calendarimporter/BindServiceHelper;->onBindService()V

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mDataUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mProcessor:Lcom/mediatek/calendarimporter/service/ImportProcessor;

    invoke-virtual {v0, v1}, Lcom/mediatek/calendarimporter/service/VCalService;->tryCancelProcessor(Lcom/mediatek/calendarimporter/service/BaseProcessor;)V

    :cond_2
    invoke-static {p0}, Lcom/mediatek/calendarimporter/utils/Utils;->hasExchangeOrGoogleAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->showAccountListView()V

    :goto_0
    return-void

    :cond_3
    const-string v0, "HandleProgressActivity"

    const-string v1, "onNewIntent, should not continue when no account exists."

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->finish()V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "DataUri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mDataUri:Landroid/net/Uri;

    const-string v0, "TargetAccountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountName:Ljava/lang/String;

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "DataUri"

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mDataUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "TargetAccountName"

    iget-object v1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mAccountName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1    # Z

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mFirstEnter:Z

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/mediatek/calendarimporter/utils/Utils;->hasExchangeOrGoogleAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->showAccountListView()V

    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mFirstEnter:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity;->finish()V

    goto :goto_1
.end method

.method public serviceConnected(Lcom/mediatek/calendarimporter/service/VCalService;)V
    .locals 1
    .param p1    # Lcom/mediatek/calendarimporter/service/VCalService;

    iput-object p1, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    new-instance v0, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;

    invoke-direct {v0, p0}, Lcom/mediatek/calendarimporter/HandleProgressActivity$4;-><init>(Lcom/mediatek/calendarimporter/HandleProgressActivity;)V

    iput-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public serviceUnConnected()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/calendarimporter/HandleProgressActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    return-void
.end method
