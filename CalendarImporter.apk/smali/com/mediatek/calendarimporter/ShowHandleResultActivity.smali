.class public Lcom/mediatek/calendarimporter/ShowHandleResultActivity;
.super Landroid/app/Activity;
.source "ShowHandleResultActivity.java"


# static fields
.field private static final EXTRA_BEGIN_TIME:Ljava/lang/String; = "beginTime"

.field private static final KEY_VIEW_TYPE:Ljava/lang/String; = "VIEW"

.field private static final MONTH_VIEW:Ljava/lang/String; = "MONTH"

.field private static final TAG:Ljava/lang/String; = "ShowHandleResultActivity"


# instance fields
.field private mEventDtStart:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/calendarimporter/ShowHandleResultActivity;)J
    .locals 2
    .param p0    # Lcom/mediatek/calendarimporter/ShowHandleResultActivity;

    iget-wide v0, p0, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->mEventDtStart:J

    return-wide v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const v10, 0x7f050009

    const v9, 0x7f050008

    const v7, 0x1060012

    const/4 v8, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v5, "ShowHandleResultActivity"

    const-string v6, "onCreate."

    invoke-static {v5, v6}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->requestWindowFeature(I)Z

    const v5, 0x7f030001

    invoke-virtual {p0, v5}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->setContentView(I)V

    invoke-static {p0, v7}, Lcom/mediatek/calendarimporter/utils/Utils;->getThemeMainColor(Landroid/content/Context;I)I

    move-result v0

    if-eq v0, v7, :cond_0

    const v5, 0x7f050003

    invoke-virtual {p0, v5}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    const v5, 0x7f050004

    invoke-virtual {p0, v5}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_0
    const v5, 0x7f050007

    invoke-virtual {p0, v5}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    const/16 v5, 0x64

    invoke-virtual {v3, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    const v5, 0x7f050005

    invoke-virtual {p0, v5}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v5, "eventStartTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v1, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->mEventDtStart:J

    const v5, 0x7f04000a

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0, v9}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v10}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v9}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/mediatek/calendarimporter/ShowHandleResultActivity$1;

    invoke-direct {v6, p0}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity$1;-><init>(Lcom/mediatek/calendarimporter/ShowHandleResultActivity;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v10}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/mediatek/calendarimporter/ShowHandleResultActivity$2;

    invoke-direct {v6, p0}, Lcom/mediatek/calendarimporter/ShowHandleResultActivity$2;-><init>(Lcom/mediatek/calendarimporter/ShowHandleResultActivity;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    const-string v0, "eventStartTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/calendarimporter/ShowHandleResultActivity;->mEventDtStart:J

    return-void
.end method
