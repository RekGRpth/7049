.class public Lcom/mediatek/calendarimporter/ShowPreviewActivity;
.super Landroid/app/Activity;
.source "ShowPreviewActivity.java"

# interfaces
.implements Lcom/mediatek/calendarimporter/BindServiceHelper$ServiceConnectedOperation;


# static fields
.field private static final TAG:Ljava/lang/String; = "ShowPreviewActivity"


# instance fields
.field private mCancelButton:Landroid/widget/Button;

.field private mErrorCertainButton:Landroid/widget/Button;

.field private mImportButton:Landroid/widget/Button;

.field private mImportErrorIcon:Landroid/widget/ImageView;

.field private mIntent:Landroid/content/Intent;

.field private mPreview:Landroid/widget/TextView;

.field private mProcessor:Lcom/mediatek/calendarimporter/service/PreviewProcessor;

.field private mService:Lcom/mediatek/calendarimporter/service/VCalService;

.field private mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;

.field private mTitle:Landroid/widget/TextView;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/calendarimporter/ShowPreviewActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/calendarimporter/ShowPreviewActivity;

    invoke-direct {p0}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->showJudgeAccessActivity()V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/calendarimporter/ShowPreviewActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/calendarimporter/ShowPreviewActivity;

    invoke-direct {p0}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->setImportErrorView()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/calendarimporter/ShowPreviewActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/calendarimporter/ShowPreviewActivity;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mImportButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/calendarimporter/ShowPreviewActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/calendarimporter/ShowPreviewActivity;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mCancelButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/calendarimporter/ShowPreviewActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/calendarimporter/ShowPreviewActivity;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mPreview:Landroid/widget/TextView;

    return-object v0
.end method

.method private setImportErrorView()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mImportErrorIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mImportButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mErrorCertainButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mTitle:Landroid/widget/TextView;

    const v1, 0x1040014

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mPreview:Landroid/widget/TextView;

    const v1, 0x7f04000f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private showJudgeAccessActivity()V
    .locals 3

    const-string v0, "ShowPreviewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showJudgeAccessActivity. uri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mIntent:Landroid/content/Intent;

    const-class v1, Lcom/mediatek/calendarimporter/JudgeAccessActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    const-string v0, "ShowPreviewActivity"

    const-string v1, "onBackPressed."

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mProcessor:Lcom/mediatek/calendarimporter/service/PreviewProcessor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    iget-object v1, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mProcessor:Lcom/mediatek/calendarimporter/service/PreviewProcessor;

    invoke-virtual {v0, v1}, Lcom/mediatek/calendarimporter/service/VCalService;->tryCancelProcessor(Lcom/mediatek/calendarimporter/service/BaseProcessor;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const v6, 0x7f05000c

    const v5, 0x1060012

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v2, "ShowPreviewActivity"

    const-string v3, "onCreate."

    invoke-static {v2, v3}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->requestWindowFeature(I)Z

    const v2, 0x7f030003

    invoke-virtual {p0, v2}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mIntent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mUri:Landroid/net/Uri;

    invoke-static {p0, v5}, Lcom/mediatek/calendarimporter/utils/Utils;->getThemeMainColor(Landroid/content/Context;I)I

    move-result v0

    if-eq v0, v5, :cond_0

    invoke-virtual {p0, v6}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f05000d

    invoke-virtual {p0, v2}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_0
    new-instance v2, Lcom/mediatek/calendarimporter/BindServiceHelper;

    invoke-direct {v2, p0}, Lcom/mediatek/calendarimporter/BindServiceHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;

    invoke-virtual {v2}, Lcom/mediatek/calendarimporter/BindServiceHelper;->onBindService()V

    invoke-virtual {p0, v6}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mTitle:Landroid/widget/TextView;

    const v2, 0x7f05000f

    invoke-virtual {p0, v2}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mPreview:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mPreview:Landroid/widget/TextView;

    const v3, 0x7f040009

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const v2, 0x7f05000b

    invoke-virtual {p0, v2}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mImportErrorIcon:Landroid/widget/ImageView;

    const v2, 0x7f050011

    invoke-virtual {p0, v2}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mErrorCertainButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mErrorCertainButton:Landroid/widget/Button;

    new-instance v3, Lcom/mediatek/calendarimporter/ShowPreviewActivity$1;

    invoke-direct {v3, p0}, Lcom/mediatek/calendarimporter/ShowPreviewActivity$1;-><init>(Lcom/mediatek/calendarimporter/ShowPreviewActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f050012

    invoke-virtual {p0, v2}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mImportButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mImportButton:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setClickable(Z)V

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mImportButton:Landroid/widget/Button;

    new-instance v3, Lcom/mediatek/calendarimporter/ShowPreviewActivity$2;

    invoke-direct {v3, p0}, Lcom/mediatek/calendarimporter/ShowPreviewActivity$2;-><init>(Lcom/mediatek/calendarimporter/ShowPreviewActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f050010

    invoke-virtual {p0, v2}, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mCancelButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mImportButton:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setClickable(Z)V

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mCancelButton:Landroid/widget/Button;

    new-instance v3, Lcom/mediatek/calendarimporter/ShowPreviewActivity$3;

    invoke-direct {v3, p0}, Lcom/mediatek/calendarimporter/ShowPreviewActivity$3;-><init>(Lcom/mediatek/calendarimporter/ShowPreviewActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 3

    const-string v0, "ShowPreviewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy() + mService:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    iget-object v1, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mProcessor:Lcom/mediatek/calendarimporter/service/PreviewProcessor;

    invoke-virtual {v0, v1}, Lcom/mediatek/calendarimporter/service/VCalService;->tryCancelProcessor(Lcom/mediatek/calendarimporter/service/BaseProcessor;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;

    invoke-virtual {v0}, Lcom/mediatek/calendarimporter/BindServiceHelper;->unBindService()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;

    invoke-virtual {v0}, Lcom/mediatek/calendarimporter/BindServiceHelper;->unBindService()V

    iput-object p1, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mIntent:Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mServiceHelper:Lcom/mediatek/calendarimporter/BindServiceHelper;

    invoke-virtual {v0}, Lcom/mediatek/calendarimporter/BindServiceHelper;->onBindService()V

    return-void
.end method

.method public serviceConnected(Lcom/mediatek/calendarimporter/service/VCalService;)V
    .locals 3
    .param p1    # Lcom/mediatek/calendarimporter/service/VCalService;

    const-string v1, "ShowPreviewActivity"

    const-string v2, "serviceConnected."

    invoke-static {v1, v2}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    new-instance v0, Lcom/mediatek/calendarimporter/ShowPreviewActivity$4;

    invoke-direct {v0, p0}, Lcom/mediatek/calendarimporter/ShowPreviewActivity$4;-><init>(Lcom/mediatek/calendarimporter/ShowPreviewActivity;)V

    new-instance v1, Lcom/mediatek/calendarimporter/service/PreviewProcessor;

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mUri:Landroid/net/Uri;

    invoke-direct {v1, p0, v2, v0}, Lcom/mediatek/calendarimporter/service/PreviewProcessor;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mProcessor:Lcom/mediatek/calendarimporter/service/PreviewProcessor;

    iget-object v1, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    iget-object v2, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mProcessor:Lcom/mediatek/calendarimporter/service/PreviewProcessor;

    invoke-virtual {v1, v2}, Lcom/mediatek/calendarimporter/service/VCalService;->tryExecuteProcessor(Lcom/mediatek/calendarimporter/service/BaseProcessor;)V

    return-void
.end method

.method public serviceUnConnected()V
    .locals 2

    const-string v0, "ShowPreviewActivity"

    const-string v1, "serviceUnConnected."

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/calendarimporter/ShowPreviewActivity;->mService:Lcom/mediatek/calendarimporter/service/VCalService;

    return-void
.end method
