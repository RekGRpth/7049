.class public Lcom/mediatek/vcalendar/VCalComposer;
.super Ljava/lang/Object;
.source "VCalComposer.java"


# static fields
.field static final DEFAULT_ACCOUNT_NAME:Ljava/lang/String; = "PC Sync"

.field public static final MULTI_EVENT_MULTI_COUNT_MODE:I = 0x2

.field public static final SINGLE_ACCOUNT_MODE:I = 0x0

.field public static final SINGLE_EVENT_MODE:I = 0x1

.field static final TAG:Ljava/lang/String; = "VCalComposer"


# instance fields
.field private mCalendarIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mCancelRequest:Z

.field private final mContext:Landroid/content/Context;

.field private mCurrentCnt:I

.field private mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

.field private mDstFile:Ljava/io/File;

.field private mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

.field private final mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

.field private mMemoryFileName:Ljava/lang/String;

.field private final mMode:I

.field private mSelection:Ljava/lang/String;

.field private mSrcAccountName:Ljava/lang/String;

.field private mSrcEventId:J

.field private mTotalCnt:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcEventId:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCancelRequest:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    const-string v0, "vCalendar"

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMemoryFileName:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mContext:Landroid/content/Context;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMode:I

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Landroid/accounts/Account;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 1
    .param p1    # Ljava/io/File;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/mediatek/vcalendar/VCalComposer;-><init>(Ljava/io/File;Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Landroid/content/Context;JLcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 2
    .param p1    # Ljava/io/File;
    .param p2    # Landroid/content/Context;
    .param p3    # J
    .param p5    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcEventId:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCancelRequest:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    const-string v0, "vCalendar"

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMemoryFileName:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDstFile:Ljava/io/File;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcAccountName:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mContext:Landroid/content/Context;

    iput-object p5, p0, Lcom/mediatek/vcalendar/VCalComposer;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iput-wide p3, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcEventId:J

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMode:I

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 1
    .param p1    # Ljava/io/File;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    const-string v0, "PC Sync"

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/mediatek/vcalendar/VCalComposer;-><init>(Ljava/io/File;Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Landroid/content/Context;Ljava/lang/String;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 2
    .param p1    # Ljava/io/File;
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcEventId:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCancelRequest:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    const-string v0, "vCalendar"

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMemoryFileName:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mContext:Landroid/content/Context;

    iput-object p4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMode:I

    iput-object p1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDstFile:Ljava/io/File;

    iput-object p3, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSelection:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 3
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcEventId:J

    iput-boolean v2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCancelRequest:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    const-string v0, "vCalendar"

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMemoryFileName:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDstFile:Ljava/io/File;

    iput-object p2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcAccountName:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediatek/vcalendar/VCalComposer;->mContext:Landroid/content/Context;

    iput-object p4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iput v2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMode:I

    return-void
.end method

.method private initTools()Z
    .locals 4

    const/4 v0, 0x1

    iget v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMode:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcAccountName:Ljava/lang/String;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcAccountName:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/vcalendar/VCalComposer;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0, v3}, Lcom/mediatek/vcalendar/DbOperationHelper;-><init>(Ljava/lang/String;ILandroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/DbOperationHelper;->getCalendarIdList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCalendarIdList:Ljava/util/ArrayList;

    new-instance v1, Lcom/mediatek/vcalendar/FileOperationHelper;

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDstFile:Ljava/io/File;

    iget-object v3, p0, Lcom/mediatek/vcalendar/VCalComposer;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v3}, Lcom/mediatek/vcalendar/FileOperationHelper;-><init>(Ljava/io/File;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    if-nez v1, :cond_0

    new-instance v1, Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Lcom/mediatek/vcalendar/DbOperationHelper;-><init>(ILandroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDstFile:Ljava/io/File;

    if-nez v1, :cond_2

    const-string v0, "VCalComposer"

    const-string v1, "The destination file can not be null"

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    new-instance v1, Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSelection:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/mediatek/vcalendar/DbOperationHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/DbOperationHelper;->getCalendarIdList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCalendarIdList:Ljava/util/ArrayList;

    new-instance v1, Lcom/mediatek/vcalendar/FileOperationHelper;

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDstFile:Ljava/io/File;

    iget-object v3, p0, Lcom/mediatek/vcalendar/VCalComposer;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v3}, Lcom/mediatek/vcalendar/FileOperationHelper;-><init>(Ljava/io/File;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private prepareSingleEventHandle()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget v2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMode:I

    if-eq v2, v1, :cond_1

    const-string v1, "VCalComposer"

    const-string v2, "startEventCompose() can be called only in SINGLE_EVENT_MODE"

    invoke-static {v1, v2}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcEventId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    const-string v1, "VCalComposer"

    const-string v2, "The EventId should be set firstly"

    invoke-static {v1, v2}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/vcalendar/VCalComposer;->initTools()Z

    move-result v2

    if-nez v2, :cond_3

    const-string v1, "VCalComposer"

    const-string v2, "initTools failed"

    invoke-static {v1, v2}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-wide v3, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcEventId:J

    invoke-virtual {v2, v3, v4}, Lcom/mediatek/vcalendar/DbOperationHelper;->isGivenIdEventExisted(J)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public buildVEventString(J)Ljava/lang/String;
    .locals 6
    .param p1    # J

    const/4 v3, 0x0

    iput-wide p1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcEventId:J

    invoke-direct {p0}, Lcom/mediatek/vcalendar/VCalComposer;->prepareSingleEventHandle()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-object v3

    :cond_0
    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/DbOperationHelper;->getCalendarIdList()Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCalendarIdList:Ljava/util/ArrayList;

    const/4 v4, 0x1

    iput v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/DbOperationHelper;->getNextVEventInfo()Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->buildEvent(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;)Lcom/mediatek/vcalendar/component/VEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/component/Component;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "VCalComposer"

    const-string v5, "startAccountCompose: BuileEvent failed"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public cancelCurrentCompose()V
    .locals 3

    const-string v0, "VCalComposer"

    const-string v1, "///////cancelCurrentParse"

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCancelRequest:Z

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCurrentCnt:I

    iget v2, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    invoke-interface {v0, v1, v2}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationCanceled(II)V

    return-void
.end method

.method public getAccountsMemoryFile()Landroid/content/res/AssetFileDescriptor;
    .locals 22

    const-string v4, "VCalComposer"

    const-string v5, "getAccountsMemoryFile : start to create the AccountsMemory File "

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mCancelRequest:Z

    if-eqz v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    new-instance v4, Lcom/mediatek/vcalendar/DbOperationHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/vcalendar/VCalComposer;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/vcalendar/VCalComposer;->mSelection:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lcom/mediatek/vcalendar/DbOperationHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/DbOperationHelper;->getCalendarIdList()Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mCalendarIdList:Ljava/util/ArrayList;

    const-string v4, "VCalComposer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startAccountsCompose:: accountCnt: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/vcalendar/VCalComposer;->mCalendarIdList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mCurrentCnt:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/DbOperationHelper;->getVEventCount()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    if-gtz v4, :cond_2

    const-string v4, "VCalComposer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startAccountsCompose:: No event matched the selection : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/vcalendar/VCalComposer;->mSelection:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const-wide/16 v10, -0x1

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mCancelRequest:Z

    if-nez v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/DbOperationHelper;->hasNextVEvent()Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "VCalComposer"

    const-string v5, "startAccountsCompose:: Compose while doing!"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/DbOperationHelper;->getNextVEventInfo()Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;->getCalendarId()J

    move-result-wide v18

    const-string v4, "VCalComposer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getAccountsMemoryFile : currentCalendarId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; tempId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    cmp-long v4, v18, v10

    if-eqz v4, :cond_4

    const-wide/16 v4, -0x1

    cmp-long v4, v10, v4

    if-eqz v4, :cond_3

    invoke-static {}, Lcom/mediatek/vcalendar/component/VCalendar;->getVCalendarTrail()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-static {}, Lcom/mediatek/vcalendar/component/VCalendar;->getVCalendarHead()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v10, v18

    :cond_4
    const/16 v20, 0x0

    :try_start_0
    invoke-static {v14}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->buildEvent(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;)Lcom/mediatek/vcalendar/component/VEvent;
    :try_end_0
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/mediatek/vcalendar/component/VEvent;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    invoke-virtual/range {v20 .. v20}, Lcom/mediatek/vcalendar/component/VEvent;->getTitle()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mMemoryFileName:Ljava/lang/String;

    :cond_5
    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mCurrentCnt:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/mediatek/vcalendar/VCalComposer;->mCurrentCnt:I

    goto/16 :goto_1

    :catch_0
    move-exception v13

    const-string v4, "VCalComposer"

    const-string v5, "getAccountsMemoryFile: BuildEvent failed"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;->printStackTrace()V

    goto/16 :goto_1

    :cond_6
    invoke-static {}, Lcom/mediatek/vcalendar/component/VCalendar;->getVCalendarTrail()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    const/4 v8, 0x0

    :try_start_1
    const-string v16, "calenderAssetFile"

    new-instance v17, Landroid/os/MemoryFile;

    array-length v4, v12

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v4}, Landroid/os/MemoryFile;-><init>(Ljava/lang/String;I)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    array-length v6, v12

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v4, v5, v6}, Landroid/os/MemoryFile;->writeBytes([BIII)V

    invoke-virtual/range {v17 .. v17}, Landroid/os/MemoryFile;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v15

    invoke-static {v15}, Landroid/os/ParcelFileDescriptor;->dup(Ljava/io/FileDescriptor;)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    new-instance v2, Landroid/content/res/AssetFileDescriptor;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v17 .. v17}, Landroid/os/MemoryFile;->length()I

    move-result v6

    int-to-long v6, v6

    invoke-direct/range {v2 .. v7}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    const-string v4, "VCalComposer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Memory file length: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catch_1
    move-exception v13

    :goto_2
    :try_start_3
    invoke-virtual {v13}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    goto/16 :goto_0

    :catch_2
    move-exception v13

    move-object v2, v8

    :goto_3
    invoke-virtual {v13}, Ljava/lang/SecurityException;->printStackTrace()V

    goto/16 :goto_0

    :catch_3
    move-exception v13

    move-object v2, v8

    :goto_4
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catchall_1
    move-exception v4

    move-object v2, v8

    goto/16 :goto_0

    :catch_4
    move-exception v13

    goto :goto_4

    :catch_5
    move-exception v13

    goto :goto_3

    :catch_6
    move-exception v13

    move-object v2, v8

    goto :goto_2
.end method

.method public getMemoryFileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMemoryFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getVCalEnd()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/mediatek/vcalendar/component/VCalendar;->getVCalendarTrail()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVCalHead()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/mediatek/vcalendar/component/VCalendar;->getVCalendarHead()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public startAccountsCompose()V
    .locals 12

    const/4 v11, 0x0

    iget v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMode:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_1

    iget v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mMode:I

    if-eqz v8, :cond_1

    const-string v8, "VCalComposer"

    const-string v9, "startAccountsCompose() cannot be called only in SINGLE_EVENT_MODE"

    invoke-static {v8, v9}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v11, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCancelRequest:Z

    const-string v8, "VCalComposer"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "startAccountsCompose:: accountCnt: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCalendarIdList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCancelRequest:Z

    if-nez v8, :cond_0

    invoke-direct {p0}, Lcom/mediatek/vcalendar/VCalComposer;->initTools()Z

    move-result v8

    if-nez v8, :cond_2

    const-string v8, "VCalComposer"

    const-string v9, "initTools failed"

    invoke-static {v8, v9}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iput v11, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCurrentCnt:I

    iget v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_3

    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v8}, Lcom/mediatek/vcalendar/DbOperationHelper;->getVEventCount()I

    move-result v8

    iput v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    :cond_3
    iget v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    if-lez v8, :cond_0

    const-wide/16 v0, -0x1

    :goto_1
    iget-boolean v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCancelRequest:Z

    if-nez v8, :cond_6

    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v8}, Lcom/mediatek/vcalendar/DbOperationHelper;->hasNextVEvent()Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    iget v9, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    invoke-virtual {v8, v9}, Lcom/mediatek/vcalendar/FileOperationHelper;->setVEventsCount(I)V

    const-string v8, "VCalComposer"

    const-string v9, "startAccountsCompose:: Compose while doing!"

    invoke-static {v8, v9}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v8}, Lcom/mediatek/vcalendar/DbOperationHelper;->getNextVEventInfo()Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;->getCalendarId()J

    move-result-wide v4

    cmp-long v8, v4, v0

    if-eqz v8, :cond_5

    const-wide/16 v8, -0x1

    cmp-long v8, v0, v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v8}, Lcom/mediatek/vcalendar/FileOperationHelper;->writeVCalendarTrail()V

    :cond_4
    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v8}, Lcom/mediatek/vcalendar/FileOperationHelper;->writeVCalendarHead()V

    :cond_5
    const/4 v6, 0x0

    :try_start_0
    invoke-static {v3}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->buildEvent(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;)Lcom/mediatek/vcalendar/component/VEvent;
    :try_end_0
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/vcalendar/component/VEvent;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v8, v7, v11}, Lcom/mediatek/vcalendar/FileOperationHelper;->addNextVEventString(Ljava/lang/String;Z)V

    iget v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCurrentCnt:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCurrentCnt:I

    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v9, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCurrentCnt:I

    iget v10, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    invoke-interface {v8, v9, v10}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalProcessStatusUpdate(II)V

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v8, "VCalComposer"

    const-string v9, "startAccountCompose: BuileEvent failed"

    invoke-static {v8, v9}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;->printStackTrace()V

    goto :goto_1

    :cond_6
    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v9, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCurrentCnt:I

    iget v10, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    const/4 v11, 0x0

    invoke-interface {v8, v9, v10, v11}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationFinished(IILjava/lang/Object;)V

    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v8}, Lcom/mediatek/vcalendar/FileOperationHelper;->close()V

    goto/16 :goto_0
.end method

.method public startEventCompose(J)V
    .locals 8
    .param p1    # J

    const/4 v7, 0x1

    iput-wide p1, p0, Lcom/mediatek/vcalendar/VCalComposer;->mSrcEventId:J

    invoke-direct {p0}, Lcom/mediatek/vcalendar/VCalComposer;->prepareSingleEventHandle()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/DbOperationHelper;->getCalendarIdList()Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCalendarIdList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mCalendarIdList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eq v4, v7, :cond_1

    const-string v4, "VCalComposer"

    const-string v5, "startEventCompose : for single event compose, the calendar should only be a single one"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iput v7, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    new-instance v4, Lcom/mediatek/vcalendar/FileOperationHelper;

    iget-object v5, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDstFile:Ljava/io/File;

    iget-object v6, p0, Lcom/mediatek/vcalendar/VCalComposer;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5, v6}, Lcom/mediatek/vcalendar/FileOperationHelper;-><init>(Ljava/io/File;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    iget v5, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    invoke-virtual {v4, v5}, Lcom/mediatek/vcalendar/FileOperationHelper;->setVEventsCount(I)V

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/DbOperationHelper;->getNextVEventInfo()Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->buildEvent(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;)Lcom/mediatek/vcalendar/component/VEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/component/Component;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/FileOperationHelper;->writeVCalendarHead()V

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Lcom/mediatek/vcalendar/FileOperationHelper;->addNextVEventString(Ljava/lang/String;Z)V
    :try_end_0
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v5, p0, Lcom/mediatek/vcalendar/VCalComposer;->mTotalCnt:I

    invoke-interface {v4, v7, v5}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalProcessStatusUpdate(II)V

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/FileOperationHelper;->writeVCalendarTrail()V

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalComposer;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/FileOperationHelper;->close()V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "VCalComposer"

    const-string v5, "startAccountCompose: BuileEvent failed"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;->printStackTrace()V

    goto :goto_0
.end method
