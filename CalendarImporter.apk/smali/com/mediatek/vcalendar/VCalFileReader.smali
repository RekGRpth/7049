.class public Lcom/mediatek/vcalendar/VCalFileReader;
.super Ljava/lang/Object;
.source "VCalFileReader.java"


# static fields
.field private static final LINE_END_LENGTH:I = 0x2

.field private static final MARK_LENGTH:I = 0x2004

.field private static final MART_LINE_LENGTH:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "VCalFileReader"


# instance fields
.field private mBufferReader:Ljava/io/BufferedReader;

.field private final mComponentBeginLineList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mComponentCount:I

.field private final mComponentEndLineList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mCurrentCompnentType:Ljava/lang/String;

.field private mFirstComponentString:Ljava/lang/String;

.field private final mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentCount:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentBeginLineList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentEndLineList:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mUri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mContext:Landroid/content/Context;

    const-string v0, "VCalFileReader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Constructor: srcUri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/vcalendar/VCalFileReader;->createBufferReader()V

    sget-object v0, Lcom/mediatek/vcalendar/component/VCalendar;->TIMEZONE_LIST:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const-string v0, ""

    sput-object v0, Lcom/mediatek/vcalendar/component/VCalendar;->sVersion:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentBeginLineList:Ljava/util/ArrayList;

    const-string v1, "BEGIN:VEVENT"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentBeginLineList:Ljava/util/ArrayList;

    const-string v1, "BEGIN:VTODO"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentEndLineList:Ljava/util/ArrayList;

    const-string v1, "END:VEVENT"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentEndLineList:Ljava/util/ArrayList;

    const-string v1, "END:VTODO"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/mediatek/vcalendar/VCalFileReader;->initSummaryAndTz()V

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    invoke-direct {p0}, Lcom/mediatek/vcalendar/VCalFileReader;->createBufferReader()V

    return-void
.end method

.method private createBufferReader()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    const-string v2, "VCalFileReader"

    const-string v3, "createBufferReader succeed."

    invoke-static {v2, v3}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private findNextComponent()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v3, 0x2004

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v2, v3}, Ljava/io/BufferedReader;->mark(I)V

    :goto_0
    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentBeginLineList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mCurrentCompnentType:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->reset()V

    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_0
    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v2, v3}, Ljava/io/BufferedReader;->mark(I)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private initSummaryAndTz()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    const-string v8, "BEGIN:VCALENDAR"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v8, 0x0

    iput v8, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentCount:I

    :cond_1
    const-string v8, "VERSION"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    sput-object v7, Lcom/mediatek/vcalendar/component/VCalendar;->sVersion:Ljava/lang/String;

    :cond_2
    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentBeginLineList:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    iget v8, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentCount:I

    iget v8, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentCount:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    const/4 v2, 0x1

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mCurrentCompnentType:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :cond_3
    const-string v8, "BEGIN:VTIMEZONE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v4, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\r\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "END:VTIMEZONE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->buildTimezone(Ljava/lang/String;)Lcom/mediatek/vcalendar/component/VTimezone;
    :try_end_0
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    :goto_1
    sget-object v8, Lcom/mediatek/vcalendar/component/VCalendar;->TIMEZONE_LIST:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    const/4 v4, 0x0

    :cond_5
    if-eqz v2, :cond_0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\r\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentEndLineList:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mCurrentCompnentType:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mFirstComponentString:Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v2, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    const-string v8, "VCalFileReader"

    const-string v9, "initSummaryAndTz: parse one timezone failed, but do not stop parse"

    invoke-static {v8, v9}, Lcom/mediatek/vcalendar/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;->printStackTrace()V

    goto :goto_1

    :cond_6
    const-string v8, "VCalFileReader"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "initSummaryAndTz end. the Events Count: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentCount:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "VCalFileReader"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "initSummaryAndTz end. the Timezone Count: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/mediatek/vcalendar/component/VCalendar;->TIMEZONE_LIST:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    const-string v0, "VCalFileReader"

    const-string v1, "closeBufferReader succeed."

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getComponentsCount()I
    .locals 1

    iget v0, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentCount:I

    return v0
.end method

.method public getFirstComponentInfo()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mFirstComponentString:Ljava/lang/String;

    return-object v0
.end method

.method public hasNextComponent()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    const/16 v3, 0x2004

    invoke-virtual {v2, v3}, Ljava/io/BufferedReader;->mark(I)V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentBeginLineList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->reset()V

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->reset()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public readNextComponent()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    const-string v4, "VCalFileReader"

    const-string v5, "readNextComponent: started."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/vcalendar/VCalFileReader;->findNextComponent()Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "VCalFileReader"

    const-string v5, "readNextComponent: findNextComponent = false, has no component yet."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    const/16 v5, 0xc8

    invoke-virtual {v4, v5}, Ljava/io/BufferedReader;->mark(I)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mBufferReader:Ljava/io/BufferedReader;

    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentEndLineList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v3, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mCurrentCompnentType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "invalid format: begin with VEVENT, but end with VTODO etc."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalFileReader;->mComponentBeginLineList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "invalid format: embeded VEVENTS etc."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method
