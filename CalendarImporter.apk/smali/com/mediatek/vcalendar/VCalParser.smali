.class public Lcom/mediatek/vcalendar/VCalParser;
.super Ljava/lang/Object;
.source "VCalParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;
    }
.end annotation


# static fields
.field static final DEFAULT_ACCOUNT_NAME:Ljava/lang/String; = "PC Sync"

.field private static final TAG:Ljava/lang/String; = "VCalParser"


# instance fields
.field private mCalendarId:J

.field private mCancelRequest:Z

.field private final mContext:Landroid/content/Context;

.field private mCurrentAccountName:Ljava/lang/String;

.field private mCurrentCnt:I

.field private mCurrentUri:Landroid/net/Uri;

.field private mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

.field private mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

.field private final mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

.field private mTotalCnt:I

.field private final mUriAccountPears:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mVcsString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/accounts/Account;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/mediatek/vcalendar/VCalParser;-><init>(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    const-string v0, "PC Sync"

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/mediatek/vcalendar/VCalParser;-><init>(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    iput-object p1, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    new-instance v0, Landroid/util/Pair;

    iget-object v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object p3, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    iput-object p4, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    iput-object p3, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iput-object p2, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    const-string v0, "PC Sync"

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    return-void
.end method

.method private initTools()Z
    .locals 9

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    new-instance v2, Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-object v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    invoke-direct {v2, v5, v3, v6}, Lcom/mediatek/vcalendar/DbOperationHelper;-><init>(Ljava/lang/String;ILandroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/DbOperationHelper;->getCalendarIdList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v2, v4, :cond_1

    const-string v2, "VCalParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initTools: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " calendars exist in the given account."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    :goto_1
    iget-wide v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    const-wide/16 v7, -0x1

    cmp-long v2, v5, v7

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-interface {v2, v3, v3, v4}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    move v2, v3

    goto :goto_0

    :cond_1
    const-string v2, "VCalParser"

    const-string v5, "initTools: the given calendar account does not exsit."

    invoke-static {v2, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string v2, "VCalParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initTools: accountName: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v2, Lcom/mediatek/vcalendar/FileOperationHelper;

    iget-object v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    invoke-direct {v2, v5, v6}, Lcom/mediatek/vcalendar/FileOperationHelper;-><init>(Landroid/net/Uri;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move v2, v4

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v2, "VCalParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initTools: the given Uri cannot be parsed, Uri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move v2, v3

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v2, "VCalParser"

    const-string v4, "initTools: IOException Occured when I/O operation. "

    invoke-static {v2, v4}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v2, v3

    goto/16 :goto_0
.end method


# virtual methods
.method public cancelCurrentParse()V
    .locals 3

    const-string v0, "VCalParser"

    const-string v1, "cancelCurrentParse"

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    iget v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v0, v1, v2}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationCanceled(II)V

    return-void
.end method

.method public close()V
    .locals 2

    const-string v0, "VCalParser"

    const-string v1, "close."

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/FileOperationHelper;->close()V

    :cond_0
    return-void
.end method

.method public startParse()V
    .locals 14

    const/4 v13, 0x1

    const/4 v12, -0x1

    const/4 v11, 0x0

    const-string v7, "VCalParser"

    const-string v8, "startParse: started."

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput-boolean v11, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    const-string v7, "VCalParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "startParse,fileIndex:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/util/Pair;

    iget-object v7, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Landroid/net/Uri;

    iput-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/util/Pair;

    iget-object v7, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    iput-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/vcalendar/VCalParser;->initTools()Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "VCalParser"

    const-string v8, "startParse: initTools failed."

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v7}, Lcom/mediatek/vcalendar/FileOperationHelper;->getVEventsCount()I

    move-result v7

    iput v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v8, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v7, v8}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationStarted(I)V

    const-string v7, "VCalParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "startParse: Events total count:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    if-ne v7, v12, :cond_2

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    const/4 v8, 0x2

    invoke-interface {v7, v11, v12, v8}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    :cond_2
    iput v11, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    :goto_2
    iget-boolean v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v7}, Lcom/mediatek/vcalendar/FileOperationHelper;->hasNextVEvent()Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v7}, Lcom/mediatek/vcalendar/FileOperationHelper;->getNextVEventString()Ljava/lang/String;

    move-result-object v2

    :try_start_0
    invoke-static {v2}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->buildEvent(Ljava/lang/String;)Lcom/mediatek/vcalendar/component/VEvent;
    :try_end_0
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    const/4 v6, 0x0

    new-instance v0, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;

    invoke-direct {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;-><init>()V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getEventValues()Landroid/content/ContentValues;

    move-result-object v7

    const-string v8, "calendar_id"

    iget-wide v9, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_1
    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getEventValues()Landroid/content/ContentValues;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/mediatek/vcalendar/component/VEvent;->toEventsContentValue(Landroid/content/ContentValues;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getAlarmsList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/mediatek/vcalendar/component/VEvent;->toAlarmsContentValue(Ljava/util/LinkedList;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getAttendeesList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/mediatek/vcalendar/component/VEvent;->toAttendeesContentValue(Ljava/util/LinkedList;)V

    new-instance v6, Ljava/lang/Long;

    invoke-virtual {v5}, Lcom/mediatek/vcalendar/component/VEvent;->getDtStart()J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, Ljava/lang/Long;-><init>(J)V
    :try_end_1
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v7}, Lcom/mediatek/vcalendar/FileOperationHelper;->hasNextVEvent()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v7, v0, v11}, Lcom/mediatek/vcalendar/DbOperationHelper;->addNextContentValue(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;Z)V

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v8, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    iget v9, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v7, v8, v9}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalProcessStatusUpdate(II)V

    :goto_3
    iget v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    goto :goto_2

    :catch_0
    move-exception v1

    const-string v7, "VCalParser"

    const-string v8, "startAccountCompose: BuileEvent failed"

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v8, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    iget v9, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v7, v8, v9, v11}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v1

    const-string v7, "VCalParser"

    const-string v8, "startParse: VEvent to contentvalues failed"

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v8, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    iget v9, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v7, v8, v9, v11}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;->printStackTrace()V

    goto/16 :goto_2

    :cond_3
    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v7, v0, v13}, Lcom/mediatek/vcalendar/DbOperationHelper;->addNextContentValue(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;Z)V

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v8, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    add-int/lit8 v8, v8, 0x1

    iget v9, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v7, v8, v9, v6}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationFinished(IILjava/lang/Object;)V

    sget-object v7, Lcom/mediatek/vcalendar/component/VCalendar;->TIMEZONE_LIST:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    goto :goto_3

    :cond_4
    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v7}, Lcom/mediatek/vcalendar/FileOperationHelper;->close()V

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-boolean v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    const/4 v8, 0x0

    invoke-virtual {v7, v8, v13}, Lcom/mediatek/vcalendar/DbOperationHelper;->addNextContentValue(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;Z)V

    sget-object v7, Lcom/mediatek/vcalendar/component/VCalendar;->TIMEZONE_LIST:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_1

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0
.end method

.method public startParsePreview()V
    .locals 9

    const/4 v8, -0x1

    const/4 v7, 0x0

    const-string v4, "VCalParser"

    const-string v5, "startParsePreview: started"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v4, Lcom/mediatek/vcalendar/FileOperationHelper;

    iget-object v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5, v6}, Lcom/mediatek/vcalendar/FileOperationHelper;-><init>(Landroid/net/Uri;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/FileOperationHelper;->getFirstEventStr()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "VCalParser"

    const-string v5, "startParsePreview: it is not a vcs file."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-interface {v4, v7, v8, v7}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    :cond_0
    const/4 v3, 0x0

    if-eqz v2, :cond_1

    :try_start_1
    invoke-static {v2}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->buildEvent(Ljava/lang/String;)Lcom/mediatek/vcalendar/component/VEvent;
    :try_end_1
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    :cond_1
    :goto_0
    if-nez v3, :cond_2

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-interface {v4, v7, v8, v7}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    const-string v4, "VCalParser"

    const-string v5, "startParse: buildEvents failed, vEvent = null"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "VCalParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startParsePreview: the given Uri cannot be parsed, Uri="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v4, "VCalParser"

    const-string v5, "startParsePreview: IOException Occured when I/O operation. "

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v4, "VCalParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startParsePreview : build calendar failed : \n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;->printStackTrace()V

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;

    invoke-direct {v1}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;-><init>()V

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/FileOperationHelper;->getVEventsCount()I

    move-result v4

    iput v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    iget v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    # setter for: Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->mEventsCnt:I
    invoke-static {v1, v4}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$002(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;I)I

    # getter for: Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->mEventsCnt:I
    invoke-static {v1}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$000(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;)I

    move-result v4

    if-gtz v4, :cond_3

    const-string v4, "VCalParser"

    const-string v5, "startParsePreview: No VEvent exsits in the file."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    const/4 v5, 0x2

    invoke-interface {v4, v7, v8, v5}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Lcom/mediatek/vcalendar/component/VEvent;->getTitle()Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->mFirstEventSummary:Ljava/lang/String;
    invoke-static {v1, v4}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$102(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v3}, Lcom/mediatek/vcalendar/component/VEvent;->getOrganizer()Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->mFirstEventOrganizer:Ljava/lang/String;
    invoke-static {v1, v4}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$202(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;Ljava/lang/String;)Ljava/lang/String;

    :try_start_2
    invoke-virtual {v3}, Lcom/mediatek/vcalendar/component/VEvent;->getDtStart()J

    move-result-wide v4

    # setter for: Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->mFisrtEventStartTime:J
    invoke-static {v1, v4, v5}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$302(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;J)J

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/mediatek/vcalendar/component/VEvent;->getTime(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->mFirstEventDuration:Ljava/lang/String;
    invoke-static {v1, v4}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$402(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    iget v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v4, v5, v6, v1}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationFinished(IILjava/lang/Object;)V

    goto/16 :goto_1

    :catch_3
    move-exception v0

    const-string v4, "VCalParser"

    const-string v5, "startParsePreview: vEvent.getTime failed."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;->printStackTrace()V

    goto :goto_2
.end method

.method public startParseVcsContent()V
    .locals 13

    const/4 v12, 0x0

    const/4 v11, 0x1

    const-string v7, "VCalParser"

    const-string v8, "startParseVcsContent"

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    const-string v8, "BEGIN:VEVENT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    const-string v8, "END:VEVENT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    const-string v7, "VCalParser"

    const-string v8, "startParseVcsContent: the given Content do not contains a VEvent."

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "VCalParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "The failed string : \n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    const-string v8, "BEGIN:VEVENT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    const-string v8, "END:VEVENT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    const-string v8, "END:VEVENT"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int v2, v7, v8

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    invoke-virtual {v7, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    :try_start_0
    invoke-static {v5}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->buildEvent(Ljava/lang/String;)Lcom/mediatek/vcalendar/component/VEvent;
    :try_end_0
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    :goto_1
    if-nez v6, :cond_2

    const-string v7, "VCalParser"

    const-string v8, "startParse: buildEvents failed, vEvent = null"

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v7, "VCalParser"

    const-string v8, "startAccountCompose: BuileEvent failed"

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;->printStackTrace()V

    goto :goto_1

    :cond_2
    new-instance v7, Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-object v8, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    iget-object v9, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8, v12, v9}, Lcom/mediatek/vcalendar/DbOperationHelper;-><init>(Ljava/lang/String;ILandroid/content/Context;)V

    iput-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v7}, Lcom/mediatek/vcalendar/DbOperationHelper;->getCalendarIdList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v7, v11, :cond_3

    const-string v7, "VCalParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "startParseVcsContent: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " calendars exist in the given account."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    :goto_2
    iget-wide v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    const-wide/16 v9, -0x1

    cmp-long v7, v7, v9

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-interface {v7, v12, v12, v11}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    goto/16 :goto_0

    :cond_3
    const-string v7, "VCalParser"

    const-string v8, "startParseVcsContent: the given calendar account does not exsit."

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    new-instance v0, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;

    invoke-direct {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;-><init>()V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getEventValues()Landroid/content/ContentValues;

    move-result-object v7

    const-string v8, "calendar_id"

    iget-wide v9, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_1
    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getEventValues()Landroid/content/ContentValues;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/mediatek/vcalendar/component/VEvent;->toEventsContentValue(Landroid/content/ContentValues;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getAlarmsList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/mediatek/vcalendar/component/VEvent;->toAlarmsContentValue(Ljava/util/LinkedList;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getAttendeesList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/mediatek/vcalendar/component/VEvent;->toAttendeesContentValue(Ljava/util/LinkedList;)V
    :try_end_1
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v7, v0, v11}, Lcom/mediatek/vcalendar/DbOperationHelper;->addNextContentValue(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;Z)V

    iget-object v7, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    const/4 v8, 0x0

    invoke-interface {v7, v11, v11, v8}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationFinished(IILjava/lang/Object;)V

    sget-object v7, Lcom/mediatek/vcalendar/component/VCalendar;->TIMEZONE_LIST:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    :catch_1
    move-exception v1

    const-string v7, "VCalParser"

    const-string v8, "startParse: VEvent to contentvalues failed"

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;->printStackTrace()V

    goto :goto_3
.end method
