.class public Lcom/mediatek/vcalendar/component/VComponentBuilder;
.super Ljava/lang/Object;
.source "VComponentBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/vcalendar/component/VComponentBuilder$1;,
        Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;,
        Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VComponentBuilder"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildEvent(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;)Lcom/mediatek/vcalendar/component/VEvent;
    .locals 1
    .param p0    # Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    new-instance v0, Lcom/mediatek/vcalendar/component/VEvent;

    invoke-direct {v0}, Lcom/mediatek/vcalendar/component/VEvent;-><init>()V

    invoke-virtual {v0, p0}, Lcom/mediatek/vcalendar/component/VEvent;->parseVEventInfo(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;)V

    return-object v0
.end method

.method public static buildEvent(Ljava/lang/String;)Lcom/mediatek/vcalendar/component/VEvent;
    .locals 3
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-static {v1, p0}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->parseComponent(Lcom/mediatek/vcalendar/component/Component;Ljava/lang/String;)Lcom/mediatek/vcalendar/component/Component;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "VEVENT"

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/component/Component;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    const-string v2, "buildEvent: Expected VEVENT"

    invoke-direct {v1, v2}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast v0, Lcom/mediatek/vcalendar/component/VEvent;

    return-object v0
.end method

.method public static buildTimezone(Ljava/lang/String;)Lcom/mediatek/vcalendar/component/VTimezone;
    .locals 3
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-static {v1, p0}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->parseComponent(Lcom/mediatek/vcalendar/component/Component;Ljava/lang/String;)Lcom/mediatek/vcalendar/component/Component;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "VTIMEZONE"

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/component/Component;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    const-string v2, "buildTimezone: Expected VTIMEZONE"

    invoke-direct {v1, v2}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast v0, Lcom/mediatek/vcalendar/component/VTimezone;

    return-object v0
.end method

.method private static extractParameter(Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;)Lcom/mediatek/vcalendar/parameter/Parameter;
    .locals 11
    .param p0    # Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const/16 v10, 0x22

    const/4 v9, -0x1

    iget-object v6, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mLine:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v2, -0x1

    :goto_0
    iget v7, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    if-ge v7, v3, :cond_d

    iget v7, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v7, 0x3a

    if-ne v0, v7, :cond_2

    if-eqz v4, :cond_1

    if-ne v2, v9, :cond_0

    new-instance v7, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Expected \'=\' within parameter in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    add-int/lit8 v7, v2, 0x1

    iget v8, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/mediatek/vcalendar/parameter/Parameter;->setValue(Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-object v4

    :cond_2
    const/16 v7, 0x3b

    if-ne v0, v7, :cond_6

    if-nez v4, :cond_4

    iget v5, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    :cond_3
    :goto_2
    iget v7, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    goto :goto_0

    :cond_4
    if-ne v2, v9, :cond_5

    new-instance v7, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Expected \'=\' within parameter in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_5
    add-int/lit8 v7, v2, 0x1

    iget v8, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/mediatek/vcalendar/parameter/Parameter;->setValue(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    const/16 v7, 0x3d

    if-ne v0, v7, :cond_8

    iget v2, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    if-nez v4, :cond_7

    if-eq v5, v9, :cond_7

    add-int/lit8 v7, v5, 0x1

    invoke-virtual {v6, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/mediatek/vcalendar/parameter/ParameterFactory;->createParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/mediatek/vcalendar/parameter/Parameter;

    move-result-object v4

    const/4 v5, -0x1

    goto :goto_2

    :cond_7
    new-instance v7, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Expected one \';\' before one \'=\' in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_8
    if-ne v0, v10, :cond_3

    if-nez v4, :cond_9

    new-instance v7, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Expected parameter before \'\"\' in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_9
    if-ne v2, v9, :cond_a

    new-instance v7, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Expected \'=\' within parameter in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_a
    iget v7, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    add-int/lit8 v8, v2, 0x1

    if-le v7, v8, :cond_b

    new-instance v7, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Parameter value cannot contain a \'\"\' in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_b
    iget v7, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v10, v7}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    if-gez v1, :cond_c

    new-instance v7, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Expected closing \'\"\' in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_c
    iget v7, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/mediatek/vcalendar/parameter/Parameter;->setValue(Ljava/lang/String;)V

    add-int/lit8 v7, v1, 0x1

    iput v7, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    goto/16 :goto_1

    :cond_d
    new-instance v7, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Expected \':\' before end of line in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method private static extractValue(Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;)Ljava/lang/String;
    .locals 5
    .param p0    # Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mLine:Ljava/lang/String;

    iget v2, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    iget v2, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x3a

    if-eq v2, v3, :cond_1

    :cond_0
    const-string v2, "VComponentBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected \':\' before end of line in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/vcalendar/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    iget v2, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    goto :goto_0
.end method

.method private static normalizeText(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;

    move-object v0, p0

    const-string v1, "\r\n "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\r\n\t"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\r "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "VComponentBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "normalizeText: normalized Text : \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static parseComponent(Lcom/mediatek/vcalendar/component/Component;Ljava/lang/String;)Lcom/mediatek/vcalendar/component/Component;
    .locals 2
    .param p0    # Lcom/mediatek/vcalendar/component/Component;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->normalizeText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->parseComponentImpl(Lcom/mediatek/vcalendar/component/Component;Ljava/lang/String;)Lcom/mediatek/vcalendar/component/Component;

    move-result-object v1

    return-object v1
.end method

.method private static parseComponentImpl(Lcom/mediatek/vcalendar/component/Component;Ljava/lang/String;)Lcom/mediatek/vcalendar/component/Component;
    .locals 12
    .param p0    # Lcom/mediatek/vcalendar/component/Component;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    move-object v1, p0

    move-object v3, p0

    new-instance v8, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;-><init>(Lcom/mediatek/vcalendar/component/VComponentBuilder$1;)V

    const/4 v9, 0x0

    iput v9, v8, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    const-string v9, "\r\n"

    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    move-object v0, v7

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v6, v0, v4

    :try_start_0
    invoke-static {v6, v8, v1}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->parseLine(Ljava/lang/String;Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;Lcom/mediatek/vcalendar/component/Component;)Lcom/mediatek/vcalendar/component/Component;
    :try_end_0
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-nez v3, :cond_0

    move-object v3, v1

    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v9, "VComponentBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cannot parse "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v2}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    return-object v3
.end method

.method private static parseLine(Ljava/lang/String;Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;Lcom/mediatek/vcalendar/component/Component;)Lcom/mediatek/vcalendar/component/Component;
    .locals 12
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;
    .param p2    # Lcom/mediatek/vcalendar/component/Component;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const/16 v11, 0x3b

    const/4 v10, 0x0

    iput-object p0, p1, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mLine:Ljava/lang/String;

    iget-object v9, p1, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mLine:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v0, 0x0

    iput v10, p1, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    :goto_0
    iget v9, p1, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    if-ge v9, v4, :cond_0

    iget v9, p1, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    invoke-virtual {p0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v11, :cond_0

    const/16 v9, 0x3a

    if-ne v0, v9, :cond_1

    :cond_0
    iget v9, p1, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    invoke-virtual {p0, v10, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    if-nez p2, :cond_2

    const-string v9, "BEGIN"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    new-instance v9, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    const-string v10, "Expected BEGIN"

    invoke-direct {v9, v10}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_1
    iget v9, p1, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p1, Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;->mIndex:I

    goto :goto_0

    :cond_2
    const-string v9, "BEGIN"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-static {p1}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->extractValue(Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/mediatek/vcalendar/component/ComponentFactory;->createComponent(Ljava/lang/String;Lcom/mediatek/vcalendar/component/Component;)Lcom/mediatek/vcalendar/component/Component;

    move-result-object v1

    if-eqz p2, :cond_3

    invoke-virtual {p2, v1}, Lcom/mediatek/vcalendar/component/Component;->addChild(Lcom/mediatek/vcalendar/component/Component;)V

    :cond_3
    :goto_1
    return-object v1

    :cond_4
    const-string v9, "END"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-static {p1}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->extractValue(Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;)Ljava/lang/String;

    move-result-object v2

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/mediatek/vcalendar/component/Component;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    :cond_5
    new-instance v9, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unexpected END "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_6
    invoke-virtual {p2}, Lcom/mediatek/vcalendar/component/Component;->getParent()Lcom/mediatek/vcalendar/component/Component;

    move-result-object v1

    goto :goto_1

    :cond_7
    const/4 v9, 0x0

    invoke-static {v5, v9}, Lcom/mediatek/vcalendar/property/PropertyFactory;->createProperty(Ljava/lang/String;Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v7

    if-ne v0, v11, :cond_8

    const/4 v6, 0x0

    :goto_2
    invoke-static {p1}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->extractParameter(Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;)Lcom/mediatek/vcalendar/parameter/Parameter;

    move-result-object v6

    if-eqz v6, :cond_8

    invoke-virtual {v7, v6}, Lcom/mediatek/vcalendar/property/Property;->addParameter(Lcom/mediatek/vcalendar/parameter/Parameter;)V

    goto :goto_2

    :cond_8
    invoke-static {p1}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->extractValue(Lcom/mediatek/vcalendar/component/VComponentBuilder$ParserState;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_a

    const-string v9, "VComponentBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parseLine,value = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "ENCODING"

    invoke-virtual {v7, v9}, Lcom/mediatek/vcalendar/property/Property;->getFirstParameter(Ljava/lang/String;)Lcom/mediatek/vcalendar/parameter/Parameter;

    move-result-object v3

    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/mediatek/vcalendar/parameter/Parameter;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/vcalendar/valuetype/Text;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "VComponentBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parseLine, after decode, property value="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/vcalendar/LogUtil;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v7, v8}, Lcom/mediatek/vcalendar/property/Property;->setValue(Ljava/lang/String;)V

    const-string v9, "VComponentBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parseLine, "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/mediatek/vcalendar/property/Property;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " added to component:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p2}, Lcom/mediatek/vcalendar/component/Component;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v7}, Lcom/mediatek/vcalendar/component/Component;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_a
    move-object v1, p2

    goto/16 :goto_1
.end method
