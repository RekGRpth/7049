.class public Lcom/mediatek/vcalendar/component/VEvent;
.super Lcom/mediatek/vcalendar/component/Component;
.source "VEvent.java"


# static fields
.field public static final CREATE_TIME_COLUMN_NAME:Ljava/lang/String; = "createTime"

.field public static final MODIFY_TIME_COLUMN_NAME:Ljava/lang/String; = "modifyTime"

.field private static final TAG:Ljava/lang/String; = "VEvent"

.field public static final VEVENT_BEGIN:Ljava/lang/String; = "BEGIN:VEVENT"

.field public static final VEVENT_END:Ljava/lang/String; = "END:VEVENT"


# direct methods
.method public constructor <init>()V
    .locals 2

    const-string v0, "VEVENT"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/vcalendar/component/Component;-><init>(Ljava/lang/String;Lcom/mediatek/vcalendar/component/Component;)V

    const-string v0, "VEvent"

    const-string v1, "Constructor: VEvent component created!"

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private getDtEnd()J
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const-string v5, "DTEND"

    invoke-virtual {p0, v5}, Lcom/mediatek/vcalendar/component/VEvent;->getFirstProperty(Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v1

    check-cast v1, Lcom/mediatek/vcalendar/property/DtEnd;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/property/DtEnd;->getValueMillis()J

    move-result-wide v5

    :goto_0
    return-wide v5

    :cond_0
    const-string v5, "DTSTART"

    invoke-virtual {p0, v5}, Lcom/mediatek/vcalendar/component/VEvent;->getFirstProperty(Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v0

    check-cast v0, Lcom/mediatek/vcalendar/property/DtStart;

    if-nez v0, :cond_1

    new-instance v5, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    const-string v6, "DTSTART is a required property."

    invoke-direct {v5, v6}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    const-string v5, "DURATION"

    invoke-virtual {p0, v5}, Lcom/mediatek/vcalendar/component/VEvent;->getFirstProperty(Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v2

    check-cast v2, Lcom/mediatek/vcalendar/property/Duration;

    if-eqz v2, :cond_2

    const-string v5, "VEvent"

    const-string v6, "Can not get DtEnd, return value based on the duration."

    invoke-static {v5, v6}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/property/DtStart;->getValueMillis()J

    move-result-wide v5

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/property/Duration;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/mediatek/vcalendar/valuetype/DDuration;->getDurationMillis(Ljava/lang/String;)J

    move-result-wide v7

    add-long/2addr v5, v7

    goto :goto_0

    :cond_2
    const-string v5, "VEvent"

    const-string v6, "Can not get DtEnd & Duration, return value based on the dtstart."

    invoke-static {v5, v6}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/property/DtStart;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcDateMillis(Ljava/lang/String;)J

    move-result-wide v3

    const-wide/32 v5, 0x5265c00

    add-long/2addr v5, v3

    goto :goto_0
.end method

.method private isAllDayEvent()Z
    .locals 7

    const/4 v3, 0x1

    const-string v4, "VEvent"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isAllDayEvent: sVersion = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/mediatek/vcalendar/component/VCalendar;->sVersion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "DTSTART"

    invoke-virtual {p0, v4}, Lcom/mediatek/vcalendar/component/VEvent;->getFirstProperty(Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v0

    check-cast v0, Lcom/mediatek/vcalendar/property/DtStart;

    const-string v4, "VALUE"

    invoke-virtual {v0, v4}, Lcom/mediatek/vcalendar/property/DtStart;->getFirstParameter(Ljava/lang/String;)Lcom/mediatek/vcalendar/parameter/Parameter;

    move-result-object v1

    check-cast v1, Lcom/mediatek/vcalendar/parameter/Value;

    if-eqz v1, :cond_0

    const-string v4, "DATE"

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/parameter/Value;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "VEvent"

    const-string v5, "isAllDayEvent: TRUE."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v3

    :cond_0
    const-string v4, "X-ALLDAY"

    invoke-virtual {p0, v4}, Lcom/mediatek/vcalendar/component/VEvent;->getFirstProperty(Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v4, "1"

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/property/Property;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "VEvent"

    const-string v5, "isAllDayEvent: TRUE."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getDtStart()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/vcalendar/component/VEvent;->mPropsMap:Ljava/util/LinkedHashMap;

    const-string v1, "DTSTART"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    const-string v1, "DTSTART is a required property."

    invoke-direct {v0, v1}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "DTSTART"

    invoke-virtual {p0, v0}, Lcom/mediatek/vcalendar/component/VEvent;->getFirstProperty(Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v0

    check-cast v0, Lcom/mediatek/vcalendar/property/DtStart;

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/property/DtStart;->getValueMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public getOrganizer()Ljava/lang/String;
    .locals 9

    const/4 v5, 0x0

    const-string v6, "VEvent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getOrganizer: sVersion = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/mediatek/vcalendar/component/VCalendar;->sVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "ORGANIZER"

    invoke-virtual {p0, v6}, Lcom/mediatek/vcalendar/component/VEvent;->getFirstProperty(Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/property/Property;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/mediatek/vcalendar/valuetype/CalAddress;->getUserMail(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    const-string v6, "ATTENDEE"

    invoke-virtual {p0, v6}, Lcom/mediatek/vcalendar/component/VEvent;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v6, "VEvent"

    const-string v7, "getOrganizer: no attendee property."

    invoke-static {v6, v7}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/vcalendar/property/Property;

    const-string v6, "ROLE"

    invoke-virtual {v3, v6}, Lcom/mediatek/vcalendar/property/Property;->getFirstParameter(Ljava/lang/String;)Lcom/mediatek/vcalendar/parameter/Parameter;

    move-result-object v4

    if-eqz v4, :cond_3

    const-string v6, "CHAIR"

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/parameter/Parameter;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    const-string v6, "ORGANIZER"

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/parameter/Parameter;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_4
    invoke-virtual {v3}, Lcom/mediatek/vcalendar/property/Property;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/mediatek/vcalendar/valuetype/CalAddress;->getUserMail(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public getTime(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mediatek/vcalendar/component/VEvent;->isAllDayEvent()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v5, 0x2012

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/mediatek/vcalendar/component/VEvent;->getDtStart()J

    move-result-wide v1

    invoke-direct {p0}, Lcom/mediatek/vcalendar/component/VEvent;->getDtEnd()J

    move-result-wide v3

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const/16 v5, 0x11

    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    or-int/lit16 v5, v5, 0x80

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    const-string v1, "SUMMARY"

    invoke-virtual {p0, v1}, Lcom/mediatek/vcalendar/component/VEvent;->getFirstProperty(Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v0

    check-cast v0, Lcom/mediatek/vcalendar/property/Summary;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/mediatek/vcalendar/property/Summary;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public parseCursorInfo(Landroid/database/Cursor;)V
    .locals 12
    .param p1    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const/4 v3, 0x1

    const-string v9, "VEvent"

    const-string v10, "parseCursorInfo : begin."

    invoke-static {v9, v10}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/mediatek/vcalendar/component/Component;->parseCursorInfo(Landroid/database/Cursor;)V

    const-string v9, "_id"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    new-instance v9, Lcom/mediatek/vcalendar/property/Uid;

    invoke-direct {v9, v8}, Lcom/mediatek/vcalendar/property/Uid;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_0
    const-string v9, "title"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_1

    new-instance v9, Lcom/mediatek/vcalendar/property/Summary;

    invoke-direct {v9, v8}, Lcom/mediatek/vcalendar/property/Summary;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_1
    invoke-static {}, Lcom/mediatek/vcalendar/Util;->isVersionIcsOrLower()Z

    move-result v9

    if-eqz v9, :cond_9

    const-string v9, "VEvent"

    const-string v10, "parseCursorInfo,OS Version is <=15."

    invoke-static {v9, v10}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v9, "modifyTime"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_3

    const-string v9, "createTime"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    :cond_3
    invoke-static {v8}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    new-instance v9, Lcom/mediatek/vcalendar/property/DtStamp;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcTimeString(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/mediatek/vcalendar/property/DtStamp;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_4
    const-string v9, "eventStatus"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Lcom/mediatek/vcalendar/property/Status;->getStatusString(I)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    new-instance v9, Lcom/mediatek/vcalendar/property/Status;

    invoke-direct {v9, v8}, Lcom/mediatek/vcalendar/property/Status;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_5
    const-string v9, "organizer"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-static {v8}, Lcom/mediatek/vcalendar/valuetype/CalAddress;->getUserCalAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    new-instance v9, Lcom/mediatek/vcalendar/property/Property;

    const-string v10, "ORGANIZER"

    invoke-direct {v9, v10, v8}, Lcom/mediatek/vcalendar/property/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_6
    const-string v9, "eventLocation"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_7

    new-instance v9, Lcom/mediatek/vcalendar/property/Location;

    invoke-direct {v9, v8}, Lcom/mediatek/vcalendar/property/Location;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_7
    const-string v9, "description"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8

    new-instance v9, Lcom/mediatek/vcalendar/property/Description;

    const-string v10, "\r\n"

    const-string v11, "\n"

    invoke-virtual {v8, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/mediatek/vcalendar/property/Description;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_8
    const-wide/16 v4, -0x1

    const-string v9, "allDay"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-ne v9, v3, :cond_a

    :goto_0
    const-string v9, "dtstart"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-eqz v9, :cond_b

    new-instance v9, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    const-string v10, "Cannot create DtStart, the needed \"DtStart\" does not exist in DB."

    invoke-direct {v9, v10}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_9
    const-string v9, "VEvent"

    const-string v10, "parseCursorInfo,OS Version is > 15."

    invoke-static {v9, v10}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "createTime"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    if-gez v9, :cond_2

    new-instance v9, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    const-string v10, "Cannot create DtStamp, the needed \"createTime\"  does not exist in DB."

    invoke-direct {v9, v10}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_a
    const/4 v3, 0x0

    goto :goto_0

    :cond_b
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    if-eqz v3, :cond_10

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcDateString(J)Ljava/lang/String;

    move-result-object v8

    new-instance v2, Lcom/mediatek/vcalendar/property/DtStart;

    invoke-direct {v2, v8}, Lcom/mediatek/vcalendar/property/DtStart;-><init>(Ljava/lang/String;)V

    new-instance v7, Lcom/mediatek/vcalendar/parameter/Value;

    const-string v9, "DATE"

    invoke-direct {v7, v9}, Lcom/mediatek/vcalendar/parameter/Value;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Lcom/mediatek/vcalendar/property/DtStart;->addParameter(Lcom/mediatek/vcalendar/parameter/Parameter;)V

    :goto_1
    invoke-virtual {p0, v2}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    const-string v9, "duration"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_c

    new-instance v9, Lcom/mediatek/vcalendar/property/Duration;

    invoke-direct {v9, v8}, Lcom/mediatek/vcalendar/property/Duration;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_c
    iget-object v9, p0, Lcom/mediatek/vcalendar/component/VEvent;->mPropsMap:Ljava/util/LinkedHashMap;

    const-string v10, "DURATION"

    invoke-virtual {v9, v10}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_d

    const-string v9, "dtend"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_d

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    if-eqz v3, :cond_11

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcDateString(J)Ljava/lang/String;

    move-result-object v8

    new-instance v1, Lcom/mediatek/vcalendar/property/DtEnd;

    invoke-direct {v1, v8}, Lcom/mediatek/vcalendar/property/DtEnd;-><init>(Ljava/lang/String;)V

    new-instance v7, Lcom/mediatek/vcalendar/parameter/Value;

    const-string v9, "DATE"

    invoke-direct {v7, v9}, Lcom/mediatek/vcalendar/parameter/Value;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Lcom/mediatek/vcalendar/property/DtEnd;->addParameter(Lcom/mediatek/vcalendar/parameter/Parameter;)V

    :goto_2
    invoke-virtual {p0, v1}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_d
    const-string v9, "rrule"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_e

    new-instance v9, Lcom/mediatek/vcalendar/property/RRule;

    invoke-direct {v9, v8}, Lcom/mediatek/vcalendar/property/RRule;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_e
    const-string v9, "eventTimezone"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_f

    new-instance v9, Lcom/mediatek/vcalendar/property/Property;

    const-string v10, "X-TIMEZONE"

    invoke-direct {v9, v10, v8}, Lcom/mediatek/vcalendar/property/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_f
    return-void

    :cond_10
    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcTimeString(J)Ljava/lang/String;

    move-result-object v8

    new-instance v2, Lcom/mediatek/vcalendar/property/DtStart;

    invoke-direct {v2, v8}, Lcom/mediatek/vcalendar/property/DtStart;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_11
    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcTimeString(J)Ljava/lang/String;

    move-result-object v8

    new-instance v1, Lcom/mediatek/vcalendar/property/DtEnd;

    invoke-direct {v1, v8}, Lcom/mediatek/vcalendar/property/DtEnd;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public parseVEventInfo(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;)V
    .locals 7
    .param p1    # Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;->getVEventCursor()Landroid/database/Cursor;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/mediatek/vcalendar/component/VEvent;->parseCursorInfo(Landroid/database/Cursor;)V

    invoke-virtual {p1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;->getVEventCursor()Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    invoke-virtual {p1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;->getAlertsCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v5, 0x0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-ge v5, v6, :cond_0

    new-instance v0, Lcom/mediatek/vcalendar/component/VAlarm;

    invoke-direct {v0, p0}, Lcom/mediatek/vcalendar/component/VAlarm;-><init>(Lcom/mediatek/vcalendar/component/Component;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-static {v1, v6}, Lcom/mediatek/vcalendar/DbOperationHelper;->matrixCursorFromCursorRow(Landroid/database/Cursor;I)Landroid/database/MatrixCursor;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/mediatek/vcalendar/component/VAlarm;->parseCursorInfo(Landroid/database/Cursor;)V

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    invoke-virtual {p0, v0}, Lcom/mediatek/vcalendar/component/VEvent;->addChild(Lcom/mediatek/vcalendar/component/Component;)V

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    invoke-virtual {p1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;->getAttendeesCursor()Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v5, 0x0

    :goto_1
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-ge v5, v6, :cond_1

    new-instance v2, Lcom/mediatek/vcalendar/property/Attendee;

    const/4 v6, 0x0

    invoke-direct {v2, v6}, Lcom/mediatek/vcalendar/property/Attendee;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-static {v3, v6}, Lcom/mediatek/vcalendar/DbOperationHelper;->matrixCursorFromCursorRow(Landroid/database/Cursor;I)Landroid/database/MatrixCursor;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/vcalendar/property/Attendee;->parseDbCursorInfo(Landroid/database/Cursor;)V

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    invoke-virtual {p0, v2}, Lcom/mediatek/vcalendar/component/VEvent;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method public toAlarmsContentValue(Ljava/util/LinkedList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/mediatek/vcalendar/component/Component;->toAlarmsContentValue(Ljava/util/LinkedList;)V

    sget-object v8, Lcom/mediatek/vcalendar/component/VCalendar;->sVersion:Ljava/lang/String;

    const-string v9, "1.0"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/vcalendar/component/VEvent;->getDtStart()J

    move-result-wide v4

    const-string v8, "VEvent"

    const-string v9, "toAlarmsContentValue: version 1.0 "

    invoke-static {v8, v9}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "AALARM"

    invoke-virtual {p0, v8}, Lcom/mediatek/vcalendar/component/VEvent;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/vcalendar/property/Property;

    check-cast v0, Lcom/mediatek/vcalendar/property/AAlarm;

    invoke-virtual {v0, p1, v4, v5}, Lcom/mediatek/vcalendar/property/AAlarm;->toAlarmsContentValue(Ljava/util/LinkedList;J)V

    goto :goto_0

    :cond_0
    const-string v8, "DALARM"

    invoke-virtual {p0, v8}, Lcom/mediatek/vcalendar/component/VEvent;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/vcalendar/property/Property;

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/property/Property;->getValue()Ljava/lang/String;

    move-result-object v3

    const-string v8, "AALARM"

    invoke-virtual {p0, v8}, Lcom/mediatek/vcalendar/component/VEvent;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/vcalendar/property/Property;

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/property/Property;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    move-object v8, v2

    check-cast v8, Lcom/mediatek/vcalendar/property/DAlarm;

    invoke-virtual {v8, p1, v4, v5}, Lcom/mediatek/vcalendar/property/DAlarm;->toAlarmsContentValue(Ljava/util/LinkedList;J)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/vcalendar/component/VEvent;->getComponents()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/vcalendar/component/Component;

    invoke-virtual {v1, p1}, Lcom/mediatek/vcalendar/component/Component;->toAlarmsContentValue(Ljava/util/LinkedList;)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method public toAttendeesContentValue(Ljava/util/LinkedList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/mediatek/vcalendar/component/Component;->toAttendeesContentValue(Ljava/util/LinkedList;)V

    const-string v3, "ATTENDEE"

    invoke-virtual {p0, v3}, Lcom/mediatek/vcalendar/component/VEvent;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    const-string v3, "VEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "toAttendeesContentValue, attendees list:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    const-string v3, "ATTENDEE"

    invoke-virtual {p0, v3}, Lcom/mediatek/vcalendar/component/VEvent;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/vcalendar/property/Property;

    check-cast v2, Lcom/mediatek/vcalendar/property/Attendee;

    invoke-virtual {v2, p1}, Lcom/mediatek/vcalendar/property/Attendee;->toAttendeesContentValue(Ljava/util/LinkedList;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public toEventsContentValue(Landroid/content/ContentValues;)V
    .locals 14
    .param p1    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const/4 v13, 0x1

    const-string v10, "VEvent"

    const-string v11, "toEventsContentValue."

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/mediatek/vcalendar/component/Component;->toEventsContentValue(Landroid/content/ContentValues;)V

    iget-object v10, p0, Lcom/mediatek/vcalendar/component/VEvent;->mPropsMap:Ljava/util/LinkedHashMap;

    const-string v11, "UID"

    invoke-virtual {v10, v11}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "VEvent"

    const-string v11, "VEVENT did not contains the required UID!!"

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v10, p0, Lcom/mediatek/vcalendar/component/VEvent;->mPropsMap:Ljava/util/LinkedHashMap;

    const-string v11, "DTSTART"

    invoke-virtual {v10, v11}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    new-instance v10, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    const-string v11, "VEVENT did not contains the required DTSTART"

    invoke-direct {v10, v11}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/vcalendar/component/VEvent;->getComponents()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_2

    const-string v10, "hasAlarm"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    sget-object v10, Lcom/mediatek/vcalendar/component/VCalendar;->sVersion:Ljava/lang/String;

    const-string v11, "1.0"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    iget-object v10, p0, Lcom/mediatek/vcalendar/component/VEvent;->mPropsMap:Ljava/util/LinkedHashMap;

    const-string v11, "DTEND"

    invoke-virtual {v10, v11}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/mediatek/vcalendar/component/VEvent;->mPropsMap:Ljava/util/LinkedHashMap;

    const-string v11, "DURATION"

    invoke-virtual {v10, v11}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "VEvent"

    const-string v11, "toEventsContentValue : DTEND DURATION cannot exist at the same VEvent"

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    const-string v11, "DTEND, DURATION cannot exist at the same VEvent"

    invoke-direct {v10, v11}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_3
    invoke-virtual {p0}, Lcom/mediatek/vcalendar/component/VEvent;->getPropertyNames()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string v10, "VEvent"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "toEventsContentValue : propertyName = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "VEvent"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "toEventsContentValue :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\'s count = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p0, v8}, Lcom/mediatek/vcalendar/component/VEvent;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v8}, Lcom/mediatek/vcalendar/component/VEvent;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/vcalendar/property/Property;

    invoke-virtual {v7, p1}, Lcom/mediatek/vcalendar/property/Property;->toEventsContentValue(Landroid/content/ContentValues;)V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/mediatek/vcalendar/component/VEvent;->isAllDayEvent()Z

    move-result v10

    if-eqz v10, :cond_6

    const-string v10, "allDay"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_6
    iget-object v10, p0, Lcom/mediatek/vcalendar/component/VEvent;->mPropsMap:Ljava/util/LinkedHashMap;

    const-string v11, "DTSTART"

    invoke-virtual {v10, v11}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/mediatek/vcalendar/component/VEvent;->mPropsMap:Ljava/util/LinkedHashMap;

    const-string v11, "DTEND"

    invoke-virtual {v10, v11}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    iget-object v10, p0, Lcom/mediatek/vcalendar/component/VEvent;->mPropsMap:Ljava/util/LinkedHashMap;

    const-string v11, "DURATION"

    invoke-virtual {v10, v11}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "DTSTART"

    invoke-virtual {p0, v10}, Lcom/mediatek/vcalendar/component/VEvent;->getFirstProperty(Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v1

    check-cast v1, Lcom/mediatek/vcalendar/property/DtStart;

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/property/DtStart;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcDateMillis(Ljava/lang/String;)J

    move-result-wide v5

    const-string v10, "dtend"

    const-wide/32 v11, 0x5265c00

    add-long/2addr v11, v5

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_7
    invoke-virtual {p0}, Lcom/mediatek/vcalendar/component/VEvent;->getComponents()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/vcalendar/component/Component;

    invoke-virtual {v0, p1}, Lcom/mediatek/vcalendar/component/Component;->toEventsContentValue(Landroid/content/ContentValues;)V

    goto :goto_1

    :cond_8
    iget-object v10, p0, Lcom/mediatek/vcalendar/component/VEvent;->mPropsMap:Ljava/util/LinkedHashMap;

    const-string v11, "X-TIMEZONE"

    invoke-virtual {v10, v11}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    const-string v10, "X-TIMEZONE"

    invoke-virtual {p0, v10}, Lcom/mediatek/vcalendar/component/VEvent;->getFirstProperty(Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v9

    const-string v11, "eventTimezone"

    invoke-virtual {v9}, Lcom/mediatek/vcalendar/property/Property;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    const-string v10, "UTC"

    :goto_2
    invoke-virtual {p1, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    const-string v10, "VEvent"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "event\'s EVENT_TIMEZONE:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "eventTimezone"

    invoke-virtual {p1, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "duration"

    invoke-virtual {p1, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    const-string v10, "VEvent"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Remove DTEND when event has DURATION:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "duration"

    invoke-virtual {p1, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "dtend"

    invoke-virtual {p1, v10}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    :cond_9
    const-string v10, "rrule"

    invoke-virtual {p1, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a

    const-string v10, "dtend"

    invoke-virtual {p1, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a

    const-string v10, "dtend"

    invoke-virtual {p1, v10}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v10, "duration"

    invoke-virtual {p1, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    invoke-direct {p0}, Lcom/mediatek/vcalendar/component/VEvent;->getDtEnd()J

    move-result-wide v10

    invoke-virtual {p0}, Lcom/mediatek/vcalendar/component/VEvent;->getDtStart()J

    move-result-wide v12

    sub-long/2addr v10, v12

    const-wide/32 v12, 0xea60

    div-long/2addr v10, v12

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/valuetype/DDuration;->getDurationString(J)Ljava/lang/String;

    move-result-object v2

    const-string v10, "duration"

    invoke-virtual {p1, v10, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    return-void

    :cond_b
    invoke-virtual {v9}, Lcom/mediatek/vcalendar/property/Property;->getValue()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_2

    :cond_c
    const-string v10, "eventTimezone"

    const-string v11, "UTC"

    invoke-virtual {p1, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3
.end method
