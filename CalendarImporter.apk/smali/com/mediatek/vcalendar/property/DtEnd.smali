.class public Lcom/mediatek/vcalendar/property/DtEnd;
.super Lcom/mediatek/vcalendar/property/Property;
.source "DtEnd.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DtEnd"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "DTEND"

    invoke-direct {p0, v0, p1}, Lcom/mediatek/vcalendar/property/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "DtEnd"

    const-string v1, "Constructor: DTEND property created."

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getValueMillis()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const-string v2, "TZID"

    invoke-virtual {p0, v2}, Lcom/mediatek/vcalendar/property/DtEnd;->getFirstParameter(Ljava/lang/String;)Lcom/mediatek/vcalendar/parameter/Parameter;

    move-result-object v0

    check-cast v0, Lcom/mediatek/vcalendar/parameter/TzId;

    if-nez v0, :cond_0

    const-string v1, "UTC"

    :goto_0
    iget-object v2, p0, Lcom/mediatek/vcalendar/property/DtEnd;->mValue:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcTimeMillis(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    return-wide v2

    :cond_0
    invoke-virtual {v0}, Lcom/mediatek/vcalendar/parameter/TzId;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public toEventsContentValue(Landroid/content/ContentValues;)V
    .locals 6
    .param p1    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const-string v4, "DtEnd"

    const-string v5, "toEventsContentValue started."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/mediatek/vcalendar/property/Property;->toEventsContentValue(Landroid/content/ContentValues;)V

    const-string v4, "TZID"

    invoke-virtual {p0, v4}, Lcom/mediatek/vcalendar/property/DtEnd;->getFirstParameter(Ljava/lang/String;)Lcom/mediatek/vcalendar/parameter/Parameter;

    move-result-object v2

    check-cast v2, Lcom/mediatek/vcalendar/parameter/TzId;

    if-nez v2, :cond_0

    const-string v3, "UTC"

    :goto_0
    iget-object v4, p0, Lcom/mediatek/vcalendar/property/DtEnd;->mValue:Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcTimeMillis(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    const-string v4, "dtend"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-void

    :cond_0
    invoke-virtual {v2}, Lcom/mediatek/vcalendar/parameter/TzId;->getValue()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
