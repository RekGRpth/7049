.class public Lcom/mediatek/vcalendar/property/DtStart;
.super Lcom/mediatek/vcalendar/property/Property;
.source "DtStart.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "DtStart"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "DTSTART"

    invoke-direct {p0, v0, p1}, Lcom/mediatek/vcalendar/property/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "DtStart"

    const-string v1, "Constructor: DtStart property created"

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getValueMillis()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const-string v2, "TZID"

    invoke-virtual {p0, v2}, Lcom/mediatek/vcalendar/property/DtStart;->getFirstParameter(Ljava/lang/String;)Lcom/mediatek/vcalendar/parameter/Parameter;

    move-result-object v0

    check-cast v0, Lcom/mediatek/vcalendar/parameter/TzId;

    if-nez v0, :cond_0

    const-string v1, "UTC"

    :goto_0
    iget-object v2, p0, Lcom/mediatek/vcalendar/property/DtStart;->mValue:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcTimeMillis(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    return-wide v2

    :cond_0
    invoke-virtual {v0}, Lcom/mediatek/vcalendar/parameter/TzId;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public toEventsContentValue(Landroid/content/ContentValues;)V
    .locals 7
    .param p1    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const-string v5, "DtStart"

    const-string v6, "toEventsContentValue started."

    invoke-static {v5, v6}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/mediatek/vcalendar/property/Property;->toEventsContentValue(Landroid/content/ContentValues;)V

    const-string v5, "TZID"

    invoke-virtual {p0, v5}, Lcom/mediatek/vcalendar/property/DtStart;->getFirstParameter(Ljava/lang/String;)Lcom/mediatek/vcalendar/parameter/Parameter;

    move-result-object v3

    check-cast v3, Lcom/mediatek/vcalendar/parameter/TzId;

    if-nez v3, :cond_0

    const-string v4, "UTC"

    :goto_0
    const-string v5, "VALUE"

    invoke-virtual {p0, v5}, Lcom/mediatek/vcalendar/property/DtStart;->getFirstParameter(Ljava/lang/String;)Lcom/mediatek/vcalendar/parameter/Parameter;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v5, "DATE"

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/parameter/Parameter;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/vcalendar/property/DtStart;->mValue:Ljava/lang/String;

    invoke-static {v5}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcDateMillis(Ljava/lang/String;)J

    move-result-wide v0

    const-string v5, "dtstart"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {v3}, Lcom/mediatek/vcalendar/parameter/TzId;->getValue()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/vcalendar/property/DtStart;->mValue:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcTimeMillis(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    const-string v5, "dtstart"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1
.end method
