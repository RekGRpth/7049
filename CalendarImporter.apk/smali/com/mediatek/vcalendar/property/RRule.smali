.class public Lcom/mediatek/vcalendar/property/RRule;
.super Lcom/mediatek/vcalendar/property/Property;
.source "RRule.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RRULE"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "RRULE"

    invoke-direct {p0, v0, p1}, Lcom/mediatek/vcalendar/property/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "RRULE"

    const-string v1, "Constructor : RRULE property created"

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public toEventsContentValue(Landroid/content/ContentValues;)V
    .locals 2
    .param p1    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const-string v0, "RRULE"

    const-string v1, "toEventsContentValue: started."

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/mediatek/vcalendar/property/Property;->toEventsContentValue(Landroid/content/ContentValues;)V

    const-string v0, "rrule"

    iget-object v1, p0, Lcom/mediatek/vcalendar/property/RRule;->mValue:Ljava/lang/String;

    invoke-static {v1}, Lcom/mediatek/vcalendar/valuetype/Recur;->updateRRuleToRfc5545Version(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
