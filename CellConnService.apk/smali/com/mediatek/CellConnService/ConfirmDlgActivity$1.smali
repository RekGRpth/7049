.class Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "ConfirmDlgActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/CellConnService/ConfirmDlgActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/16 v7, 0x191

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-string v2, "ConfirmDlgActivity"

    const-string v3, "BroadcastReceiver onReceive"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "ConfirmDlgActivity"

    const-string v3, "BroadcastReceiver AIRPLANE_MODE_CHANGED"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "state"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "ConfirmDlgActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BroadcastReceiver AIRPLANE_MODE_CHANGED airplaneModeON = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "ConfirmDlgActivity"

    const-string v3, "BroadcastReceiver AIRPLANE_MODE_CHANGED for gemini"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2, v5}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$002(Lcom/mediatek/CellConnService/ConfirmDlgActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    iget-object v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v3}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v3

    invoke-static {v2, v3, v6}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$200(Lcom/mediatek/CellConnService/ConfirmDlgActivity;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v2

    if-ne v7, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    iget-object v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v3}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v3

    invoke-static {v2, v3, v5}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$200(Lcom/mediatek/CellConnService/ConfirmDlgActivity;IZ)V

    goto :goto_0

    :cond_2
    const-string v2, "android.intent.action.DUAL_SIM_MODE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "ConfirmDlgActivity"

    const-string v3, "BroadcastReceiver ACTION_DUAL_SIM_MODE_CHANGED"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "mode"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "ConfirmDlgActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BroadcastReceiver duslSimMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v1, :cond_3

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2, v5}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$002(Lcom/mediatek/CellConnService/ConfirmDlgActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    iget-object v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v3}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v3

    invoke-static {v2, v3, v6}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$200(Lcom/mediatek/CellConnService/ConfirmDlgActivity;IZ)V

    goto :goto_0

    :cond_3
    if-ne v5, v1, :cond_5

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$300(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v2

    if-ne v5, v2, :cond_4

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2, v5}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$002(Lcom/mediatek/CellConnService/ConfirmDlgActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    iget-object v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v3}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v3

    invoke-static {v2, v3, v6}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$200(Lcom/mediatek/CellConnService/ConfirmDlgActivity;IZ)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$300(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v2

    if-ne v7, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    iget-object v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v3}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v3

    invoke-static {v2, v3, v5}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$200(Lcom/mediatek/CellConnService/ConfirmDlgActivity;IZ)V

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x2

    if-ne v2, v1, :cond_7

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$300(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2, v5}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$002(Lcom/mediatek/CellConnService/ConfirmDlgActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    iget-object v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v3}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v3

    invoke-static {v2, v3, v6}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$200(Lcom/mediatek/CellConnService/ConfirmDlgActivity;IZ)V

    goto/16 :goto_0

    :cond_6
    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$300(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v2

    if-ne v5, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v2

    if-ne v7, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    iget-object v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v3}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v3

    invoke-static {v2, v3, v5}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$200(Lcom/mediatek/CellConnService/ConfirmDlgActivity;IZ)V

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x3

    if-ne v2, v1, :cond_0

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v2}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v2

    if-ne v7, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    iget-object v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;->this$0:Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-static {v3}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I

    move-result v3

    invoke-static {v2, v3, v5}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->access$200(Lcom/mediatek/CellConnService/ConfirmDlgActivity;IZ)V

    goto/16 :goto_0
.end method
