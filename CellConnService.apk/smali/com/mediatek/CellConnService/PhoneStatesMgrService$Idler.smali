.class Lcom/mediatek/CellConnService/PhoneStatesMgrService$Idler;
.super Ljava/lang/Object;
.source "PhoneStatesMgrService.java"

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/CellConnService/PhoneStatesMgrService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Idler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;


# direct methods
.method private constructor <init>(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$Idler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final queueIdle()Z
    .locals 2

    const-string v0, "PhoneStatesMgrService"

    const-string v1, "queueIdle() show confirm dialog, wait"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, 0x190

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$Idler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    iget-object v1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$Idler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1700(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
