.class public Lcom/mediatek/CellConnService/PhoneStatesMgrService;
.super Landroid/app/Service;
.source "PhoneStatesMgrService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/CellConnService/PhoneStatesMgrService$HanderEx;,
        Lcom/mediatek/CellConnService/PhoneStatesMgrService$Idler;,
        Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;,
        Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;
    }
.end annotation


# static fields
.field static final CONFIRM_CARDNAME:Ljava/lang/String; = "confirm_cardName"

.field static final CONFIRM_RESULT:Ljava/lang/String; = "confirm_result"

.field static final CONFIRM_RESULT_CANCEL:I = 0x1c4

.field static final CONFIRM_RESULT_DISMISS:I = 0x1c5

.field static final CONFIRM_RESULT_INVALID:I = 0x1c2

.field static final CONFIRM_RESULT_OK:I = 0x1c3

.field static final CONFIRM_RESULT_PREFERSLOT:Ljava/lang/String; = "confirm_result_preferSlot"

.field static final CONFIRM_ROAMINGWITHPREFER:Ljava/lang/String; = "confirm_roamingWithPrefer"

.field static final CONFIRM_SLOT:Ljava/lang/String; = "confirm_slot"

.field static final CONFIRM_TYPE:Ljava/lang/String; = "confirm_type"

.field static final CONFIRM_TYPE_FDN:I = 0x194

.field static final CONFIRM_TYPE_PIN:I = 0x192

.field static final CONFIRM_TYPE_RADIO:I = 0x191

.field static final CONFIRM_TYPE_ROAMING:I = 0x195

.field static final CONFIRM_TYPE_SIMLOCKED:I = 0x196

.field static final CONFIRM_TYPE_SIMMELOCK:I = 0x193

.field static final CONFIRM_TYPE_SLOTLOCKED:I = 0x197

.field private static final GET_MELOCK_RETRYCOUNT:I = 0x6f

.field private static final MSG_ID_CHECKFDN:I = 0xcc

.field private static final MSG_ID_CHECKPIN1:I = 0xc9

.field private static final MSG_ID_CHECKPIN2:I = 0xcd

.field private static final MSG_ID_CHECKPUK1:I = 0xca

.field private static final MSG_ID_CHECKPUK2:I = 0xce

.field private static final MSG_ID_CHECKRADIO:I = 0xc8

.field private static final MSG_ID_CHECKROAMING:I = 0xcf

.field private static final MSG_ID_CHECKSIMMELOCK:I = 0xcb

.field public static final PHONE_STATE_FDNENABLE_MARK:I = 0xf000

.field public static final PHONE_STATE_NORMAL:I = 0x0

.field public static final PHONE_STATE_NOSIM:I = 0xa

.field public static final PHONE_STATE_PIN1LOCKED:I = 0x3

.field public static final PHONE_STATE_PIN1UNLOCKED:I = 0x4

.field public static final PHONE_STATE_PUK1LOCKED:I = 0x5

.field public static final PHONE_STATE_PUK1UNLOCKED:I = 0x6

.field public static final PHONE_STATE_RADIOOFF:I = 0x1

.field public static final PHONE_STATE_RADIOON:I = 0x2

.field public static final PHONE_STATE_ROAMING:I = 0x9

.field public static final PHONE_STATE_SIMMELOCKED:I = 0x7

.field public static final PHONE_STATE_SIMMEUNLOCKED:I = 0x8

.field public static final PHONE_STATE_SIMNOTREADY:I = 0xb

.field public static final PHONE_STATE_UNKNOWN:I = -0x1

.field public static final REQUEST_TYPE_FDN:I = 0x130

.field public static final REQUEST_TYPE_PIN1:I = 0x12e

.field public static final REQUEST_TYPE_PIN2:I = 0x131

.field public static final REQUEST_TYPE_ROAMING:I = 0x132

.field public static final REQUEST_TYPE_UNKNOWN:I = 0x12c

.field private static final SIMLOCK_TYPE_PIN:I = 0x1

.field private static final SIMLOCK_TYPE_SIMMELOCK:I = 0x2

.field public static final START_TYPE:Ljava/lang/String; = "start_type"

.field public static final START_TYPE_REQ:Ljava/lang/String; = "request"

.field public static final START_TYPE_RSP:Ljava/lang/String; = "response"

.field private static final TAG:Ljava/lang/String; = "PhoneStatesMgrService"

.field private static final UNLOCK_ICC_SML_QUERYLEFTTIMES:I = 0x6e

.field static final VERIFY_RESULT:Ljava/lang/String; = "verfiy_result"

.field static final VERIFY_TYPE:Ljava/lang/String; = "verfiy_type"

.field static final VERIFY_TYPE_PIN:I = 0x1f5

.field static final VERIFY_TYPE_PIN2:I = 0x1f8

.field static final VERIFY_TYPE_PUK:I = 0x1f6

.field static final VERIFY_TYPE_PUK2:I = 0x1f9

.field static final VERIFY_TYPE_SIMMELOCK:I = 0x1f7

.field static sInstance:Lcom/mediatek/CellConnService/PhoneStatesMgrService;


# instance fields
.field private bConfirmDlgIsShowed:Z

.field mBinder:Lcom/mediatek/CellConnService/IPhoneStatesMgrService$Stub;

.field private mContext:Landroid/content/Context;

.field private mICallBackList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;",
            ">;"
        }
    .end annotation
.end field

.field private mIdleIntent:Landroid/content/Intent;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field mMELockHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$HanderEx;

.field private mPhoneStates:[I

.field private mPreferSlot:I

.field private mRequestNoPrefer:Z

.field private mResult:I

.field private mSIMCount:I

.field private volatile mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

.field private volatile mServiceLooper:Landroid/os/Looper;

.field private mUserConfirmed:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput v1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mSIMCount:I

    iput-boolean v1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mUserConfirmed:Z

    iput v1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mPreferSlot:I

    iput-boolean v1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->bConfirmDlgIsShowed:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mResult:I

    iput-boolean v1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mRequestNoPrefer:Z

    new-instance v0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$1;

    invoke-direct {v0, p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$1;-><init>(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)V

    iput-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mBinder:Lcom/mediatek/CellConnService/IPhoneStatesMgrService$Stub;

    new-instance v0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$2;

    invoke-direct {v0, p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$2;-><init>(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)V

    iput-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIdleIntent:Landroid/content/Intent;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    iget-boolean v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mUserConfirmed:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mUserConfirmed:Z

    return p1
.end method

.method static synthetic access$1000(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)I
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-direct {p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->getDualSimMode()I

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mRequestNoPrefer:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/CellConnService/PhoneStatesMgrService;II)V
    .locals 0
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(II)V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/CellConnService/PhoneStatesMgrService;III)V
    .locals 0
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V
    .locals 0
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p1    # Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->showConfirmDlg(Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V
    .locals 0
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p1    # Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->showVerifyDlg(Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/mediatek/CellConnService/PhoneStatesMgrService;I)I
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->getRetryPukCount(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/mediatek/CellConnService/PhoneStatesMgrService;I)I
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->getRetryMELockCount(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)Landroid/content/Intent;
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    iget-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIdleIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/CellConnService/PhoneStatesMgrService;I)I
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->getRetryPinCount(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)[I
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    iget-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mPhoneStates:[I

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;I)Z
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->isRadioOff(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)I
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    iget v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mSIMCount:I

    return v0
.end method

.method static synthetic access$600(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)I
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    iget v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mResult:I

    return v0
.end method

.method static synthetic access$602(Lcom/mediatek/CellConnService/PhoneStatesMgrService;I)I
    .locals 0
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mResult:I

    return p1
.end method

.method static synthetic access$700(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    iget-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    iget-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)V
    .locals 0
    .param p0    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-direct {p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->waitForLooper()V

    return-void
.end method

.method private broadcastAirPlaneModeEvent(I)V
    .locals 3
    .param p1    # I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private broadcastCallback(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    const-string v5, "PhoneStatesMgrService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "broadcastCallback by slot callbacklist size = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " slot = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_3

    iget-object v5, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getCallback()Lcom/mediatek/CellConnService/IPhoneStatesCallback;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v5, "PhoneStatesMgrService"

    const-string v7, "broadcastCallback by slot get call back is null"

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-ne p1, v5, :cond_0

    const/16 v5, 0x132

    :try_start_1
    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqType()I

    move-result v7

    if-ne v5, v7, :cond_2

    iget v5, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mPreferSlot:I

    invoke-interface {v2, p2, v5}, Lcom/mediatek/CellConnService/IPhoneStatesCallback;->onCompleteWithPrefer(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    iget-object v5, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v3, v3, -0x1

    const-string v5, "PhoneStatesMgrService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "broadcastCallback by slot remove["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " type = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqType()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " size = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    :cond_2
    :try_start_3
    invoke-interface {v2, p2}, Lcom/mediatek/CellConnService/IPhoneStatesCallback;->onComplete(I)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_4
    const-string v5, "PhoneStatesMgrService"

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    return-void
.end method

.method private broadcastCallback(III)V
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/16 v10, 0x132

    const-string v6, "PhoneStatesMgrService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "broadcastCallback ++ reqType is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {p2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->reqestTypeToString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->isSIMReady(I)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->isRadioOff(I)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "PhoneStatesMgrService"

    const-string v7, "broadcastCallback sim not ready"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v7, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    const-string v6, "PhoneStatesMgrService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "broadcastCallback[1] callbacklist size = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_1

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    const-string v6, "PhoneStatesMgrService"

    const-string v8, "broadcastCallback callback list is empty return [1]"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v7

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    :cond_2
    const/4 v6, 0x2

    if-ne v6, p3, :cond_3

    if-lez v3, :cond_3

    :try_start_1
    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;

    invoke-virtual {v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getCallback()Lcom/mediatek/CellConnService/IPhoneStatesCallback;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    :try_start_2
    invoke-interface {v2, p3}, Lcom/mediatek/CellConnService/IPhoneStatesCallback;->onComplete(I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    const-string v6, "PhoneStatesMgrService"

    const-string v8, "broadcastCallback remove[0]"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v7

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v6, "PhoneStatesMgrService"

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v3, :cond_a

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;

    invoke-virtual {v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getCallback()Lcom/mediatek/CellConnService/IPhoneStatesCallback;

    move-result-object v2

    if-nez v2, :cond_5

    const-string v6, "PhoneStatesMgrService"

    const-string v8, "broadcastCallback get call back is null"

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqType()I

    move-result v6

    if-ne p2, v6, :cond_7

    invoke-virtual {v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v6

    if-ne p1, v6, :cond_7

    if-ne v10, p2, :cond_6

    :try_start_4
    iget v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mPreferSlot:I

    invoke-interface {v2, p3, v6}, Lcom/mediatek/CellConnService/IPhoneStatesCallback;->onCompleteWithPrefer(II)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_4
    :try_start_5
    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    const-string v6, "PhoneStatesMgrService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "broadcastCallback remove["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v3, v3, -0x1

    const-string v6, "PhoneStatesMgrService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "broadcastCallback onComplete is called and current callbacklist size is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    :cond_6
    :try_start_6
    invoke-interface {v2, p3}, Lcom/mediatek/CellConnService/IPhoneStatesCallback;->onComplete(I)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    :catch_1
    move-exception v0

    :try_start_7
    const-string v6, "PhoneStatesMgrService"

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_7
    const/16 v6, 0x130

    invoke-virtual {v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqType()I

    move-result v8

    if-ne v6, v8, :cond_8

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    const/16 v8, 0xcc

    invoke-static {v6, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v6

    iput v6, v4, Landroid/os/Message;->arg1:I

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    invoke-virtual {v6, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    monitor-exit v7

    goto/16 :goto_0

    :cond_8
    const/16 v6, 0x131

    invoke-virtual {v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqType()I

    move-result v8

    if-ne v6, v8, :cond_9

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    const/16 v8, 0xcd

    invoke-static {v6, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v6

    iput v6, v4, Landroid/os/Message;->arg1:I

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    invoke-virtual {v6, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    monitor-exit v7

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqType()I

    move-result v6

    if-ne v10, v6, :cond_4

    iput p3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mResult:I

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    const/16 v8, 0xcf

    invoke-static {v6, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v6

    iput v6, v4, Landroid/os/Message;->arg1:I

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    invoke-virtual {v6, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    monitor-exit v7

    goto/16 :goto_0

    :cond_a
    monitor-exit v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0
.end method

.method private broadcastDualSimModeEvent(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "dual_sim_mode_setting"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DUAL_SIM_MODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "mode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public static confirmResultToString(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const-string v0, "CONFIRM_RESULT_NULL"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "CONFIRM_RESULT_OK"

    goto :goto_0

    :pswitch_1
    const-string v0, "CONFIRM_RESULT_CANCEL"

    goto :goto_0

    :pswitch_2
    const-string v0, "CONFIRM_RESULT_DISMISS"

    goto :goto_0

    :pswitch_3
    const-string v0, "CONFIRM_RESULT_INVALID"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1c2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static confirmTypeToString(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const-string v0, "CONFIRM_TYPE_NULL"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "CONFIRM_TYPE_RADIO"

    goto :goto_0

    :pswitch_1
    const-string v0, "CONFIRM_TYPE_PIN"

    goto :goto_0

    :pswitch_2
    const-string v0, "CONFIRM_TYPE_SIMMELOCK"

    goto :goto_0

    :pswitch_3
    const-string v0, "CONFIRM_TYPE_FDN"

    goto :goto_0

    :pswitch_4
    const-string v0, "CONFIRM_TYPE_ROAMING"

    goto :goto_0

    :pswitch_5
    const-string v0, "CONFIRM_TYPE_SIMLOCKED"

    goto :goto_0

    :pswitch_6
    const-string v0, "CONFIRM_TYPE_SLOTLOCKED"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private getDualSimMode()I
    .locals 3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "dual_sim_mode_setting"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method static getInstance()Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .locals 1

    sget-object v0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->sInstance:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    return-object v0
.end method

.method private getRetryMELockCount(I)I
    .locals 5
    .param p1    # I

    const-string v2, "PhoneStatesMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRetryMELockCount slot = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/mediatek/CellConnService/PhoneStatesMgrService$3;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$3;-><init>(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mMELockHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$HanderEx;

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mMELockHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$HanderEx;

    const/16 v3, 0x6f

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mMELockHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$HanderEx;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mMELockHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$HanderEx;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mMELockHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$HanderEx;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v2, "PhoneStatesMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRetryMELockCount nRetryCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mMELockHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$HanderEx;

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$HanderEx;->getRetryCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mMELockHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$HanderEx;

    invoke-virtual {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$HanderEx;->getRetryCount()I

    move-result v2

    return v2

    :catch_0
    move-exception v0

    :try_start_2
    const-string v2, "PhoneStatesMgrService"

    const-string v4, "getRetryMELockCount exception"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method private getRetryPinCount(I)I
    .locals 2
    .param p1    # I

    const/4 v1, -0x1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-string v0, "gsm.sim.retry.pin1.2"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "gsm.sim.retry.pin1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method private getRetryPukCount(I)I
    .locals 2
    .param p1    # I

    const/4 v1, -0x1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-string v0, "gsm.sim.retry.puk1.2"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "gsm.sim.retry.puk1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method private isPINLockDisplay(I)Z
    .locals 9
    .param p1    # I

    const-wide/16 v7, 0x1

    const/4 v2, 0x0

    if-gez p1, :cond_0

    :goto_0
    return v2

    :cond_0
    const-string v3, "PhoneStatesMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isPINLockDisplay slot = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "sim_lock_state_setting"

    const-wide/16 v5, 0x0

    invoke-static {v3, v4, v5, v6}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v0, v1

    const-string v3, "PhoneStatesMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isPINLockDisplay pinLockState = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    mul-int/lit8 v5, p1, 0x2

    ushr-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    and-long/2addr v3, v7

    cmp-long v3, v7, v3

    if-nez v3, :cond_1

    const-string v2, "PhoneStatesMgrService"

    const-string v3, "isPINLockDisplay SIMLOCK_TYPE_PIN return true"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const-string v3, "PhoneStatesMgrService"

    const-string v4, "isPINLockDisplay SIMLOCK_TYPE_PIN return false"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isRadioOff(I)Z
    .locals 8
    .param p1    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v3, "PhoneStatesMgrService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isRadioOff verify slot "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :try_start_0
    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v3, "PhoneStatesMgrService"

    const-string v6, "isRadioOff iTel is null"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v5

    :cond_0
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    const-string v6, "PhoneStatesMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isRadioOff slot "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " radio off? "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v0, :cond_1

    move v3, v4

    :goto_2
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_2

    :goto_3
    move v5, v4

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :cond_1
    move v3, v5

    goto :goto_2

    :cond_2
    move v4, v5

    goto :goto_3
.end method

.method private isRadioOffBySimManagement(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "RADIO_STATUS"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {p0, p1}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v2

    const/4 v1, 0x1

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v3, v2, Landroid/provider/Telephony$SIMInfo;->mICCId:Ljava/lang/String;

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    :cond_0
    return v1
.end method

.method private isRoaming(I)Z
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/telephony/TelephonyManager;->isNetworkRoamingGemini(I)Z

    move-result v0

    const-string v1, "PhoneStatesMgrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isRoaming slot = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " roaming = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method private isSimLockDisplay(II)Z
    .locals 10
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    const-wide/16 v8, 0x1

    const/4 v3, 0x1

    if-gez p1, :cond_0

    :goto_0
    return v2

    :cond_0
    const-string v4, "PhoneStatesMgrService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isSimLockDisplay slot = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "sim_lock_state_setting"

    const-wide/16 v6, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v0, v1

    const-string v4, "PhoneStatesMgrService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isSimLockDisplay simLockState = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    mul-int/lit8 v6, p1, 0x2

    ushr-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    if-ne v3, p2, :cond_2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    and-long/2addr v4, v8

    cmp-long v4, v8, v4

    if-nez v4, :cond_1

    const-string v2, "PhoneStatesMgrService"

    const-string v4, "isSimLockDisplay SIMLOCK_TYPE_PIN return true"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    goto :goto_0

    :cond_1
    const-string v3, "PhoneStatesMgrService"

    const-string v4, "isSimLockDisplay SIMLOCK_TYPE_PIN return false"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v4, 0x2

    if-ne v4, p2, :cond_4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    ushr-long/2addr v4, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    and-long/2addr v4, v8

    cmp-long v4, v8, v4

    if-nez v4, :cond_3

    const-string v2, "PhoneStatesMgrService"

    const-string v4, "isSimLockDisplay SIMLOCK_TYPE_SIMMELOCK return true"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    goto/16 :goto_0

    :cond_3
    const-string v3, "PhoneStatesMgrService"

    const-string v4, "isSimLockDisplay SIMLOCK_TYPE_SIMMELOCK return false"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    const-string v2, "PhoneStatesMgrService"

    const-string v4, "isSimLockDisplay return true "

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    goto/16 :goto_0
.end method

.method public static msgIdToString(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const-string v0, "MSG_ID_NULL"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "MSG_ID_CHECKRADIO"

    goto :goto_0

    :pswitch_1
    const-string v0, "MSG_ID_CHECKPIN1"

    goto :goto_0

    :pswitch_2
    const-string v0, "MSG_ID_CHECKPUK1"

    goto :goto_0

    :pswitch_3
    const-string v0, "MSG_ID_CHECKSIMMELOCK"

    goto :goto_0

    :pswitch_4
    const-string v0, "MSG_ID_CHECKFDN"

    goto :goto_0

    :pswitch_5
    const-string v0, "MSG_ID_CHECKPIN2"

    goto :goto_0

    :pswitch_6
    const-string v0, "MSG_ID_CHECKPUK2"

    goto :goto_0

    :pswitch_7
    const-string v0, "MSG_ID_CHECKROAMING"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static phoneStateToString(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    sparse-switch p0, :sswitch_data_0

    const-string v0, "PHONE_STATE_NULL"

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "PHONE_STATE_UNKNOWN"

    goto :goto_0

    :sswitch_1
    const-string v0, "PHONE_STATE_NORMAL"

    goto :goto_0

    :sswitch_2
    const-string v0, "PHONE_STATE_RADIOOFF"

    goto :goto_0

    :sswitch_3
    const-string v0, "PHONE_STATE_RADIOON"

    goto :goto_0

    :sswitch_4
    const-string v0, "PHONE_STATE_PIN1LOCKED"

    goto :goto_0

    :sswitch_5
    const-string v0, "PHONE_STATE_PIN1UNLOCKED"

    goto :goto_0

    :sswitch_6
    const-string v0, "PHONE_STATE_PUK1LOCKED"

    goto :goto_0

    :sswitch_7
    const-string v0, "PHONE_STATE_PUK1UNLOCKED"

    goto :goto_0

    :sswitch_8
    const-string v0, "PHONE_STATE_SIMMELOCKED"

    goto :goto_0

    :sswitch_9
    const-string v0, "PHONE_STATE_SIMMEUNLOCKED"

    goto :goto_0

    :sswitch_a
    const-string v0, "PHONE_STATE_ROAMING"

    goto :goto_0

    :sswitch_b
    const-string v0, "PHONE_STATE_NOSIM"

    goto :goto_0

    :sswitch_c
    const-string v0, "PHONE_STATE_FDNENABLE"

    goto :goto_0

    :sswitch_d
    const-string v0, "PHONE_STATE_SIMNOTREADY"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x5 -> :sswitch_6
        0x6 -> :sswitch_7
        0x7 -> :sswitch_8
        0x8 -> :sswitch_9
        0x9 -> :sswitch_a
        0xa -> :sswitch_b
        0xb -> :sswitch_d
        0xf000 -> :sswitch_c
    .end sparse-switch
.end method

.method private powerRadioOn(I)V
    .locals 7
    .param p1    # I

    const/4 v2, 0x3

    const/4 v6, 0x0

    const/4 v0, 0x1

    const-string v3, "PhoneStatesMgrService"

    const-string v4, "powerRadioOn +++"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->isRadioOff(I)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v2, "PhoneStatesMgrService"

    const-string v3, "powerRadioOn radio is on"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "airplane_mode_on"

    const/4 v5, -0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v0, v3, :cond_5

    const-string v3, "PhoneStatesMgrService"

    const-string v4, "powerRadioOn: airplane mode is on"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "airplane_mode_on"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "state"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    if-nez p1, :cond_3

    invoke-direct {p0, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->isRadioOffBySimManagement(I)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    const-string v2, "PhoneStatesMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "powerRadioOn change to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "dual_sim_mode_setting"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DUAL_SIM_MODE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "mode"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    if-ne v0, p1, :cond_0

    invoke-direct {p0, v6}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->isRadioOffBySimManagement(I)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v0, 0x2

    :goto_2
    const-string v2, "PhoneStatesMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "powerRadioOn change to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "dual_sim_mode_setting"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DUAL_SIM_MODE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "mode"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    const-string v2, "PhoneStatesMgrService"

    const-string v3, "powerRadioOn: airplane mode is off"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->getDualSimMode()I

    move-result v0

    const-string v2, "PhoneStatesMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "powerRadioOn: airplane mode is off and dualSimMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_6

    add-int/lit8 v0, p1, 0x1

    :goto_3
    invoke-direct {p0, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastDualSimModeEvent(I)V

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x3

    goto :goto_3
.end method

.method private removeRequest()V
    .locals 5

    const-string v1, "PhoneStatesMgrService"

    const-string v2, "removeRequest"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const-string v1, "PhoneStatesMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeRequest callbacklist size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static reqestTypeToString(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const-string v0, "REQUEST_TYPE_NULL"

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "REQUEST_TYPE_UNKNOWN"

    goto :goto_0

    :pswitch_2
    const-string v0, "REQUEST_TYPE_PIN1"

    goto :goto_0

    :pswitch_3
    const-string v0, "REQUEST_TYPE_FDN"

    goto :goto_0

    :pswitch_4
    const-string v0, "REQUEST_TYPE_PIN2"

    goto :goto_0

    :pswitch_5
    const-string v0, "REQUEST_TYPE_ROAMING"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private showConfirmDlg(Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V
    .locals 13
    .param p1    # Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;
    .param p2    # I

    const/16 v12, 0x193

    const/16 v11, 0x192

    const/4 v8, 0x0

    const/4 v7, 0x1

    const-string v6, "PhoneStatesMgrService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "showConfirmDlg confirmType = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->confirmTypeToString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_1

    const-string v6, "PhoneStatesMgrService"

    const-string v7, "showConfirmDlg reqItem is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v9

    invoke-static {v6, v9}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v6, "PhoneStatesMgrService"

    const-string v7, "showConfirmDlg: check radio get simInfo is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v6

    const/16 v7, 0x12c

    const/4 v8, 0x3

    invoke-direct {p0, v6, v7, v8}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto :goto_0

    :cond_2
    if-eq v11, p2, :cond_3

    if-ne v12, p2, :cond_6

    :cond_3
    invoke-virtual {p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v5

    const-string v6, "PhoneStatesMgrService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "showConfirmDlg slot = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    if-ne v11, p2, :cond_5

    const/4 v4, 0x1

    :cond_4
    :goto_1
    if-ne v4, v7, :cond_6

    invoke-direct {p0, v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->isPINLockDisplay(I)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "PhoneStatesMgrService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "showConfirmDlg pin lock screen pinlockType = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " has not yet shown"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    if-ne v12, p2, :cond_4

    const/4 v4, 0x2

    goto :goto_1

    :cond_6
    iget-object v0, v2, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/String;

    const-string v6, ""

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :cond_7
    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    const-class v10, Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    invoke-direct {v6, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIdleIntent:Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIdleIntent:Landroid/content/Intent;

    const-string v9, "confirm_type"

    invoke-virtual {v6, v9, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIdleIntent:Landroid/content/Intent;

    const-string v9, "confirm_cardName"

    invoke-virtual {v6, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIdleIntent:Landroid/content/Intent;

    const-string v9, "confirm_slot"

    invoke-virtual {p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v10

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v6, 0x195

    if-ne v6, p2, :cond_9

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_8

    const-string v6, "PhoneStatesMgrService"

    const-string v9, "showConfirmDlg getInsertedSIMList is null"

    invoke-static {v6, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    if-eqz v3, :cond_9

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    iget-boolean v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mRequestNoPrefer:Z

    if-nez v6, :cond_b

    if-le v1, v7, :cond_b

    invoke-virtual {p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v6

    if-nez v6, :cond_a

    move v6, v7

    :goto_2
    invoke-direct {p0, v6}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->isRoaming(I)Z

    move-result v6

    if-nez v6, :cond_b

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIdleIntent:Landroid/content/Intent;

    const-string v8, "confirm_roamingWithPrefer"

    invoke-virtual {v6, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_9
    :goto_3
    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIdleIntent:Landroid/content/Intent;

    const/high16 v8, 0x10000000

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v6, "PhoneStatesMgrService"

    const-string v8, "showConfirmDlg() waiting queue idle"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->bConfirmDlgIsShowed:Z

    if-nez v6, :cond_0

    iput-boolean v7, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->bConfirmDlgIsShowed:Z

    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIdleIntent:Landroid/content/Intent;

    invoke-virtual {p0, v6}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    move v6, v8

    goto :goto_2

    :cond_b
    iget-object v6, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIdleIntent:Landroid/content/Intent;

    const-string v9, "confirm_roamingWithPrefer"

    invoke-virtual {v6, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_3
.end method

.method private showVerifyDlg(Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V
    .locals 9
    .param p1    # Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;
    .param p2    # I

    const/16 v8, 0x1f7

    const/16 v7, 0x1f6

    const/16 v6, 0x1f5

    const-string v3, "PhoneStatesMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "showVerifyDlg verifyType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->verifyTypeToString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v3, "PhoneStatesMgrService"

    const-string v4, "showVerifyDlg reqItem is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v2

    const-string v3, "PhoneStatesMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "showVerifyDlg slot = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    if-eq v6, p2, :cond_1

    if-ne v7, p2, :cond_3

    :cond_1
    const/4 v1, 0x1

    :cond_2
    :goto_1
    const/4 v3, 0x1

    if-ne v1, v3, :cond_4

    invoke-direct {p0, v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->isPINLockDisplay(I)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "PhoneStatesMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "showVerifyDlg pin lock screen pinlockType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " has not yet shown"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    if-ne v8, p2, :cond_2

    const/4 v1, 0x2

    goto :goto_1

    :cond_4
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    if-nez v0, :cond_5

    const-string v3, "PhoneStatesMgrService"

    const-string v4, "showVerifyDlg new intent failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    if-eq v6, p2, :cond_6

    if-ne v7, p2, :cond_8

    :cond_6
    const-string v3, "com.android.phone.SetupUnlockPINLock"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :cond_7
    :goto_2
    const-string v3, "Phone.GEMINI_SIM_ID_KEY"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const v3, 0x10008000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_8
    if-ne v8, p2, :cond_9

    const-string v3, "com.android.phone.SetupUnlockSIMLock"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    :cond_9
    const/16 v3, 0x1f8

    if-eq v3, p2, :cond_a

    const/16 v3, 0x1f9

    if-ne v3, p2, :cond_7

    :cond_a
    const-string v3, "com.android.phone.SetupUnlockPIN2Lock"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2
.end method

.method public static verifyTypeToString(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const-string v0, "VERIFY_TYPE_NULL"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "VERIFY_TYPE_PIN"

    goto :goto_0

    :pswitch_1
    const-string v0, "VERIFY_TYPE_PUK"

    goto :goto_0

    :pswitch_2
    const-string v0, "VERIFY_TYPE_SIMMELOCK"

    goto :goto_0

    :pswitch_3
    const-string v0, "VERIFY_TYPE_PIN2"

    goto :goto_0

    :pswitch_4
    const-string v0, "VERIFY_TYPE_PUK2"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private waitForLooper()V
    .locals 2

    :goto_0
    iget-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    if-nez v0, :cond_0

    monitor-enter p0

    :try_start_0
    const-string v0, "PhoneStatesMgrService"

    const-string v1, "waitForLooper"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, 0x64

    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public clearRoamingNeeded(I)V
    .locals 3
    .param p1    # I

    const-string v0, "PhoneStatesMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearRoamingNeeded slot = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-string v0, "gsm.roaming.indicator.needed.2"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "gsm.roaming.indicator.needed"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public fdnRequest(I)Z
    .locals 6
    .param p1    # I

    const/4 v0, 0x0

    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v3, "PhoneStatesMgrService"

    const-string v4, "fdnRequest iTel is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/ITelephony;->isFDNEnabledGemini(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    const-string v3, "PhoneStatesMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fdnRequest fdn enable is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "PhoneStatesMgrService"

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method public hasIccCardGemini(I)Z
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    :try_start_0
    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2, p1}, Lcom/android/internal/telephony/ITelephony;->hasIccCardGemini(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public isRoaming2Local(I)Z
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const-string v0, "PhoneStatesMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRoaming2Local slot = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-string v0, "PhoneStatesMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRoaming2Local = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "gsm.roaming.indicator.tolocal.2"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "gsm.roaming.indicator.tolocal.2"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "PhoneStatesMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRoaming2Local = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "gsm.roaming.indicator.tolocal"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "gsm.roaming.indicator.tolocal"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public isRoamingNeeded(I)Z
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const-string v0, "PhoneStatesMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRoamingNeeded slot = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-string v0, "PhoneStatesMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRoamingNeeded = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "gsm.roaming.indicator.needed.2"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "gsm.roaming.indicator.needed.2"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "PhoneStatesMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRoamingNeeded = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "gsm.roaming.indicator.needed"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "gsm.roaming.indicator.needed"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public isSIMReady(I)Z
    .locals 6
    .param p1    # I

    const/4 v2, 0x1

    const-string v3, "PhoneStatesMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isSIMReady slot = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v0

    const-string v3, "PhoneStatesMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isSIMReady simstate = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq v0, v2, :cond_0

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    const/4 v3, 0x4

    if-eq v0, v3, :cond_0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_1

    :cond_0
    move v1, v2

    :goto_0
    const-string v2, "PhoneStatesMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSIMReady result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "PhoneStatesMgrService"

    const-string v1, "onBind "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mBinder:Lcom/mediatek/CellConnService/IPhoneStatesMgrService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v3, "PhoneStatesMgrService"

    const-string v4, "onCreate"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x2

    iput v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mSIMCount:I

    iget v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mSIMCount:I

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mPhoneStates:[I

    iget-object v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mPhoneStates:[I

    if-nez v3, :cond_0

    const-string v3, "PhoneStatesMgrService"

    const-string v4, "onCreate new mPhoneStates failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mSIMCount:I

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mPhoneStates:[I

    const/4 v4, -0x1

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    if-nez v1, :cond_2

    const-string v3, "PhoneStatesMgrService"

    const-string v4, "onCreate new intent failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    if-nez v3, :cond_3

    const-string v3, "PhoneStatesMgrService"

    const-string v4, "onCreate new mICallBackList failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    new-instance v2, Ljava/lang/Thread;

    const/4 v3, 0x0

    const-string v4, "Phone States ServiceThread"

    invoke-direct {v2, v3, p0, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    if-nez v2, :cond_4

    const-string v3, "PhoneStatesMgrService"

    const-string v4, "onCreate new serviceThread failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    const-string v3, "android.intent.action.DUAL_SIM_MODE"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->waitForLooper()V

    iget-object v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-direct {p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->waitForLooper()V

    iget-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    const-string v0, "PhoneStatesMgrService"

    const-string v1, "onDestroy "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 10
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    const-string v7, "PhoneStatesMgrService"

    const-string v8, "onStart"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    invoke-direct {p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->waitForLooper()V

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v7, "start_type"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    const-string v7, "response"

    const-string v8, "start_type"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "PhoneStatesMgrService"

    const-string v8, "onStart response"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->bConfirmDlgIsShowed:Z

    iget-object v8, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    monitor-enter v8

    :try_start_0
    iget-object v7, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_2

    const-string v7, "PhoneStatesMgrService"

    const-string v9, "onStart response callback list is null"

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v8

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    :cond_2
    :try_start_1
    iget-object v7, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mICallBackList:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v4, :cond_3

    const-string v7, "PhoneStatesMgrService"

    const-string v8, "onStart response reqItem is null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v7, "confirm_type"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_f

    const-string v7, "confirm_result"

    const/16 v8, 0x1c2

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v7, "PhoneStatesMgrService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "confirm response and confirmType = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->confirmTypeToString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " confirmRet = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->confirmResultToString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    const/16 v7, 0x1c3

    if-ne v7, v0, :cond_4

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->powerRadioOn(I)V

    goto/16 :goto_0

    :cond_4
    const/16 v7, 0x1c4

    if-ne v7, v0, :cond_5

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    const/16 v8, 0x12c

    const/4 v9, 0x2

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto/16 :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->removeRequest()V

    goto/16 :goto_0

    :pswitch_1
    const/16 v7, 0x1c3

    if-ne v7, v0, :cond_6

    const/16 v7, 0x1f5

    invoke-direct {p0, v4, v7}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->showVerifyDlg(Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_6
    const/16 v7, 0x1c4

    if-ne v7, v0, :cond_7

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    const/16 v8, 0x12c

    const/4 v9, 0x2

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto/16 :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->removeRequest()V

    goto/16 :goto_0

    :pswitch_2
    const/16 v7, 0x1c3

    if-ne v7, v0, :cond_8

    const/16 v7, 0x1f7

    invoke-direct {p0, v4, v7}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->showVerifyDlg(Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_8
    const/16 v7, 0x1c4

    if-ne v7, v0, :cond_9

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    const/16 v8, 0x12c

    const/4 v9, 0x2

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto/16 :goto_0

    :cond_9
    invoke-direct {p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->removeRequest()V

    goto/16 :goto_0

    :pswitch_3
    const/16 v7, 0x1c3

    if-ne v7, v0, :cond_a

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    const/16 v8, 0x12c

    const/4 v9, 0x2

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto/16 :goto_0

    :cond_a
    const/16 v7, 0x1c4

    if-ne v7, v0, :cond_b

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    const/16 v8, 0x12c

    const/4 v9, 0x2

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto/16 :goto_0

    :cond_b
    invoke-direct {p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->removeRequest()V

    goto/16 :goto_0

    :pswitch_4
    const-string v7, "confirm_result_preferSlot"

    const/4 v8, -0x1

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mPreferSlot:I

    const/16 v7, 0x1c3

    if-ne v7, v0, :cond_d

    iget v7, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mResult:I

    if-nez v7, :cond_c

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    const/16 v8, 0x132

    const/4 v9, 0x0

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    :goto_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "roaming_reminder_mode_setting"

    const/4 v9, -0x1

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->clearRoamingNeeded(I)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    const/16 v8, 0x132

    const/4 v9, 0x4

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto :goto_1

    :cond_d
    const/16 v7, 0x1c4

    if-ne v7, v0, :cond_e

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    const/16 v8, 0x132

    const/4 v9, 0x2

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto/16 :goto_0

    :cond_e
    invoke-direct {p0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->removeRequest()V

    goto/16 :goto_0

    :pswitch_5
    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    const/16 v8, 0x12c

    const/4 v9, 0x2

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto/16 :goto_0

    :cond_f
    const-string v7, "verfiy_type"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_0

    const-string v7, "PhoneStatesMgrService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "verify response and verifyType = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v6}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->verifyTypeToString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v6, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_6
    const-string v7, "verfiy_result"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_10

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    const/16 v8, 0x12e

    const/4 v9, 0x0

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto/16 :goto_0

    :cond_10
    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    const/16 v8, 0x12e

    const/4 v9, 0x2

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto/16 :goto_0

    :pswitch_7
    const-string v7, "verfiy_result"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_13

    const/16 v7, 0x131

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqType()I

    move-result v8

    if-eq v7, v8, :cond_11

    const/16 v7, 0x130

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqType()I

    move-result v8

    if-ne v7, v8, :cond_12

    :cond_11
    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqType()I

    move-result v8

    const/4 v9, 0x0

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto/16 :goto_0

    :cond_12
    const/16 v7, 0x132

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqType()I

    move-result v8

    if-ne v7, v8, :cond_0

    iget-object v7, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    const/16 v8, 0xcf

    invoke-static {v7, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    iput v7, v3, Landroid/os/Message;->arg1:I

    iget-object v7, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    invoke-virtual {v7, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_13
    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v7

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqType()I

    move-result v8

    const/4 v9, 0x2

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->broadcastCallback(III)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1f5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public pinRequest(I)Z
    .locals 4
    .param p1    # I

    const-string v1, "PhoneStatesMgrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pinRequest slot = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "PhoneStatesMgrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pinRequest result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pukRequest(I)Z
    .locals 4
    .param p1    # I

    const-string v1, "PhoneStatesMgrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pukRequest slot = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const-string v1, "PhoneStatesMgrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pukRequest slot = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " SimState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x3

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v2

    if-eq v1, v2, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->getRetryPinCount(I)I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "PhoneStatesMgrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pukRequest result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public roamingRequest(I)Z
    .locals 6
    .param p1    # I

    const/4 v1, 0x0

    const/4 v5, -0x1

    const/4 v0, 0x1

    const-string v2, "PhoneStatesMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "roamingRequest slot = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->isRoaming(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "PhoneStatesMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "roamingRequest slot = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is roaming"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "roaming_reminder_mode_setting"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0, p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->isRoamingNeeded(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "PhoneStatesMgrService"

    const-string v2, "roamingRequest reminder once and need to indicate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    const-string v0, "PhoneStatesMgrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "roamingRequest slot = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not roaming"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "roaming_reminder_mode_setting"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_2

    const-string v1, "PhoneStatesMgrService"

    const-string v2, "roamingRequest reminder always"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "PhoneStatesMgrService"

    const-string v2, "roamingRequest result = false"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public run()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->prepare()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceLooper:Landroid/os/Looper;

    new-instance v0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;-><init>(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$1;)V

    iput-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    const-string v0, "PhoneStatesMgrService"

    const-string v1, "run"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->mServiceHandler:Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;

    if-nez v0, :cond_0

    const-string v0, "PhoneStatesMgrService"

    const-string v1, "mServiceHandler is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method

.method public setRoaming2Local(I)V
    .locals 3
    .param p1    # I

    const-string v0, "PhoneStatesMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRoaming2Local slot = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-string v0, "gsm.roaming.indicator.tolocal.2"

    const-string v1, "true"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "gsm.roaming.indicator.tolocal"

    const-string v1, "true"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public simMELockRequest(I)Z
    .locals 4
    .param p1    # I

    const-string v1, "PhoneStatesMgrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "simMELockRequest slot = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "PhoneStatesMgrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "simMELockRequest result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
