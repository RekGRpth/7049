.class final Lcom/a/a/a/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private volatile a:Lcom/a/a/a/a/a;

.field private synthetic b:Lcom/a/a/a/a/d;


# direct methods
.method private constructor <init>(Lcom/a/a/a/a/d;)V
    .locals 0

    iput-object p1, p0, Lcom/a/a/a/a/e;->b:Lcom/a/a/a/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/a/a/a/d;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/a/a/a/a/e;-><init>(Lcom/a/a/a/a/d;)V

    return-void
.end method

.method static synthetic a(Lcom/a/a/a/a/e;)Lcom/a/a/a/a/a;
    .locals 1

    iget-object v0, p0, Lcom/a/a/a/a/e;->a:Lcom/a/a/a/a/a;

    return-object v0
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/a/a/a/a/e;->b:Lcom/a/a/a/a/d;

    invoke-static {v0}, Lcom/a/a/a/a/d;->a(Lcom/a/a/a/a/d;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/a/a/a/a/d;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Service certificate mismatch for %s, dropping connection"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "com.googlecode.eyesfree.brailleback"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/a/a/a/a/e;->b:Lcom/a/a/a/a/d;

    invoke-static {v0}, Lcom/a/a/a/a/d;->b(Lcom/a/a/a/a/d;)Lcom/a/a/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/a/f;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/a/a/a/a/d;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Connected to self braille service"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p2}, Lcom/a/a/a/a/b;->b(Landroid/os/IBinder;)Lcom/a/a/a/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a/e;->a:Lcom/a/a/a/a/a;

    iget-object v0, p0, Lcom/a/a/a/a/e;->b:Lcom/a/a/a/a/d;

    invoke-static {v0}, Lcom/a/a/a/a/d;->b(Lcom/a/a/a/a/d;)Lcom/a/a/a/a/f;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/a/a/a/a/e;->b:Lcom/a/a/a/a/d;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/a/a/a/a/d;->a(Lcom/a/a/a/a/d;I)I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    invoke-static {}, Lcom/a/a/a/a/d;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Disconnected from self braille service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/a/a/e;->a:Lcom/a/a/a/a/a;

    iget-object v0, p0, Lcom/a/a/a/a/e;->b:Lcom/a/a/a/a/d;

    invoke-static {v0}, Lcom/a/a/a/a/d;->b(Lcom/a/a/a/a/d;)Lcom/a/a/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/a/f;->a()V

    return-void
.end method
