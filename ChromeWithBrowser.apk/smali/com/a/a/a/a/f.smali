.class final Lcom/a/a/a/a/f;
.super Landroid/os/Handler;


# instance fields
.field private synthetic a:Lcom/a/a/a/a/d;


# direct methods
.method private constructor <init>(Lcom/a/a/a/a/d;)V
    .locals 0

    iput-object p1, p0, Lcom/a/a/a/a/f;->a:Lcom/a/a/a/a/d;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/a/a/a/d;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/a/a/a/a/f;-><init>(Lcom/a/a/a/a/d;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/a/a/a/a/f;->a:Lcom/a/a/a/a/d;

    invoke-static {v0}, Lcom/a/a/a/a/d;->c(Lcom/a/a/a/a/d;)I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/a/a/a/a/f;->a:Lcom/a/a/a/a/d;

    invoke-static {v1}, Lcom/a/a/a/a/d;->c(Lcom/a/a/a/a/d;)I

    move-result v1

    shl-int/2addr v0, v1

    const/4 v1, 0x1

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/a/a/a/a/f;->sendEmptyMessageDelayed(IJ)Z

    iget-object v0, p0, Lcom/a/a/a/a/f;->a:Lcom/a/a/a/a/d;

    invoke-static {v0}, Lcom/a/a/a/a/d;->d(Lcom/a/a/a/a/d;)I

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/f;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/a/a/a/a/f;->a:Lcom/a/a/a/a/d;

    invoke-static {v0}, Lcom/a/a/a/a/d;->e(Lcom/a/a/a/a/d;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/a/a/a/a/f;->a:Lcom/a/a/a/a/d;

    invoke-static {v0}, Lcom/a/a/a/a/d;->f(Lcom/a/a/a/a/d;)Lcom/a/a/a/a/e;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/a/a/a/a/f;->a:Lcom/a/a/a/a/d;

    invoke-static {v0}, Lcom/a/a/a/a/d;->g(Lcom/a/a/a/a/d;)V

    :cond_1
    iget-object v0, p0, Lcom/a/a/a/a/f;->a:Lcom/a/a/a/a/d;

    invoke-static {v0}, Lcom/a/a/a/a/d;->h(Lcom/a/a/a/a/d;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/a/a/a/a/f;->a:Lcom/a/a/a/a/d;

    invoke-static {v0}, Lcom/a/a/a/a/d;->g(Lcom/a/a/a/a/d;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
