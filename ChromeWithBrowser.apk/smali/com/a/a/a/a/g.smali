.class public final Lcom/a/a/a/a/g;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final a:Landroid/os/Parcelable$Creator;


# instance fields
.field private b:Landroid/view/accessibility/AccessibilityNodeInfo;

.field private c:Ljava/lang/CharSequence;

.field private d:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/a/a/a/a/h;

    invoke-direct {v0}, Lcom/a/a/a/a/h;-><init>()V

    sput-object v0, Lcom/a/a/a/a/g;->a:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/a/a/a/a/g;->d:Landroid/os/Bundle;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/a/a/a/a/g;->d:Landroid/os/Bundle;

    sget-object v0, Landroid/view/accessibility/AccessibilityNodeInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo;

    iput-object v0, p0, Lcom/a/a/a/a/g;->b:Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a/g;->c:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a/g;->d:Landroid/os/Bundle;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/a/a/a/a/g;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a(Landroid/view/View;)Lcom/a/a/a/a/g;
    .locals 2

    invoke-static {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    new-instance v1, Lcom/a/a/a/a/g;

    invoke-direct {v1}, Lcom/a/a/a/a/g;-><init>()V

    iput-object v0, v1, Lcom/a/a/a/a/g;->b:Landroid/view/accessibility/AccessibilityNodeInfo;

    return-object v1
.end method

.method private b()Landroid/os/Bundle;
    .locals 2

    iget-object v0, p0, Lcom/a/a/a/a/g;->d:Landroid/os/Bundle;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/a/g;->d:Landroid/os/Bundle;

    :cond_0
    iget-object v0, p0, Lcom/a/a/a/a/g;->d:Landroid/os/Bundle;

    return-object v0
.end method


# virtual methods
.method public final a(I)Lcom/a/a/a/a/g;
    .locals 2

    invoke-direct {p0}, Lcom/a/a/a/a/g;->b()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selectionStart"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/a/a/a/a/g;
    .locals 0

    iput-object p1, p0, Lcom/a/a/a/a/g;->c:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final a()V
    .locals 4

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/a/a/a/a/g;->b:Landroid/view/accessibility/AccessibilityNodeInfo;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Accessibility node info can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/a/a/a/a/g;->d:Landroid/os/Bundle;

    const-string v1, "selectionStart"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/a/a/a/a/g;->d:Landroid/os/Bundle;

    const-string v2, "selectionEnd"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/a/a/a/a/g;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_2

    if-gtz v0, :cond_1

    if-lez v1, :cond_5

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Selection can\'t be set without text"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-gez v0, :cond_3

    if-ltz v1, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Selection end without start"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v2, p0, Lcom/a/a/a/a/g;->c:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-gt v0, v2, :cond_4

    if-le v1, v2, :cond_5

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Selection out of bounds"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-void
.end method

.method public final b(I)Lcom/a/a/a/a/g;
    .locals 2

    invoke-direct {p0}, Lcom/a/a/a/a/g;->b()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selectionEnd"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/a/a/a/a/g;->b:Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {v0, p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->writeToParcel(Landroid/os/Parcel;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/a/a/g;->b:Landroid/view/accessibility/AccessibilityNodeInfo;

    iget-object v0, p0, Lcom/a/a/a/a/g;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/a/a/a/a/g;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    return-void
.end method
