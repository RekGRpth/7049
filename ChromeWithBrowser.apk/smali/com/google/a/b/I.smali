.class final Lcom/google/a/b/I;
.super Lcom/google/a/b/K;


# instance fields
.field private synthetic a:Ljava/util/Set;

.field private synthetic b:Lcom/google/a/a/d;

.field private synthetic c:Ljava/util/Set;


# direct methods
.method constructor <init>(Ljava/util/Set;Lcom/google/a/a/d;Ljava/util/Set;)V
    .locals 1

    iput-object p1, p0, Lcom/google/a/b/I;->a:Ljava/util/Set;

    iput-object p2, p0, Lcom/google/a/b/I;->b:Lcom/google/a/a/d;

    iput-object p3, p0, Lcom/google/a/b/I;->c:Ljava/util/Set;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/a/b/K;-><init>(B)V

    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/a/b/I;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/a/b/I;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    iget-object v0, p0, Lcom/google/a/b/I;->c:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/a/b/I;->a:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2

    iget-object v0, p0, Lcom/google/a/b/I;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/a/b/I;->b:Lcom/google/a/a/d;

    invoke-static {v0, v1}, Lcom/google/a/b/x;->a(Ljava/util/Iterator;Lcom/google/a/a/d;)Lcom/google/a/b/M;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/a/b/I;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/a/b/x;->a(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method
