.class public abstract Lcom/google/a/b/a;
.super Lcom/google/a/b/M;


# instance fields
.field private a:Lcom/google/a/b/c;

.field private b:Ljava/lang/Object;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/a/b/M;-><init>()V

    sget-object v0, Lcom/google/a/b/c;->b:Lcom/google/a/b/c;

    iput-object v0, p0, Lcom/google/a/b/a;->a:Lcom/google/a/b/c;

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
.end method

.method protected final b()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/google/a/b/c;->c:Lcom/google/a/b/c;

    iput-object v0, p0, Lcom/google/a/b/a;->a:Lcom/google/a/b/c;

    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasNext()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/a/b/a;->a:Lcom/google/a/b/c;

    sget-object v3, Lcom/google/a/b/c;->d:Lcom/google/a/b/c;

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/a/a/a;->b(Z)V

    sget-object v0, Lcom/google/a/b/b;->a:[I

    iget-object v3, p0, Lcom/google/a/b/a;->a:Lcom/google/a/b/c;

    invoke-virtual {v3}, Lcom/google/a/b/c;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/a/b/c;->d:Lcom/google/a/b/c;

    iput-object v0, p0, Lcom/google/a/b/a;->a:Lcom/google/a/b/c;

    invoke-virtual {p0}, Lcom/google/a/b/a;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/a;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/a/b/a;->a:Lcom/google/a/b/c;

    sget-object v3, Lcom/google/a/b/c;->c:Lcom/google/a/b/c;

    if-eq v0, v3, :cond_0

    sget-object v0, Lcom/google/a/b/c;->a:Lcom/google/a/b/c;

    iput-object v0, p0, Lcom/google/a/b/a;->a:Lcom/google/a/b/c;

    move v2, v1

    :cond_0
    :goto_1
    :pswitch_0
    return v2

    :cond_1
    move v0, v2

    goto :goto_0

    :pswitch_1
    move v2, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/a/b/a;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/a/b/c;->b:Lcom/google/a/b/c;

    iput-object v0, p0, Lcom/google/a/b/a;->a:Lcom/google/a/b/c;

    iget-object v0, p0, Lcom/google/a/b/a;->b:Ljava/lang/Object;

    return-object v0
.end method
