.class Lcom/google/a/b/o;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:Ljava/util/Iterator;

.field private b:Ljava/util/Collection;

.field private synthetic c:Lcom/google/a/b/n;


# direct methods
.method constructor <init>(Lcom/google/a/b/n;)V
    .locals 2

    iput-object p1, p0, Lcom/google/a/b/o;->c:Lcom/google/a/b/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lcom/google/a/b/o;->c:Lcom/google/a/b/n;

    iget-object v0, v0, Lcom/google/a/b/n;->b:Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/a/b/o;->b:Ljava/util/Collection;

    iget-object v0, p1, Lcom/google/a/b/n;->d:Lcom/google/a/b/e;

    iget-object v1, p1, Lcom/google/a/b/n;->b:Ljava/util/Collection;

    invoke-static {v0, v1}, Lcom/google/a/b/e;->a(Lcom/google/a/b/e;Ljava/util/Collection;)Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/o;->a:Ljava/util/Iterator;

    return-void
.end method

.method constructor <init>(Lcom/google/a/b/n;Ljava/util/Iterator;)V
    .locals 1

    iput-object p1, p0, Lcom/google/a/b/o;->c:Lcom/google/a/b/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lcom/google/a/b/o;->c:Lcom/google/a/b/n;

    iget-object v0, v0, Lcom/google/a/b/n;->b:Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/a/b/o;->b:Ljava/util/Collection;

    iput-object p2, p0, Lcom/google/a/b/o;->a:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/a/b/o;->c:Lcom/google/a/b/n;

    invoke-virtual {v0}, Lcom/google/a/b/n;->a()V

    iget-object v0, p0, Lcom/google/a/b/o;->c:Lcom/google/a/b/n;

    iget-object v0, v0, Lcom/google/a/b/n;->b:Ljava/util/Collection;

    iget-object v1, p0, Lcom/google/a/b/o;->b:Ljava/util/Collection;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method public hasNext()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/a/b/o;->a()V

    iget-object v0, p0, Lcom/google/a/b/o;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/a/b/o;->a()V

    iget-object v0, p0, Lcom/google/a/b/o;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    iget-object v0, p0, Lcom/google/a/b/o;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    iget-object v0, p0, Lcom/google/a/b/o;->c:Lcom/google/a/b/n;

    iget-object v0, v0, Lcom/google/a/b/n;->d:Lcom/google/a/b/e;

    invoke-static {v0}, Lcom/google/a/b/e;->b(Lcom/google/a/b/e;)I

    iget-object v0, p0, Lcom/google/a/b/o;->c:Lcom/google/a/b/n;

    invoke-virtual {v0}, Lcom/google/a/b/n;->b()V

    return-void
.end method
