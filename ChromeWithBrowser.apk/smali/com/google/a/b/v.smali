.class public final Lcom/google/a/b/v;
.super Lcom/google/a/b/t;


# instance fields
.field private transient a:I


# direct methods
.method private constructor <init>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/a/b/t;-><init>(Ljava/util/Map;)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/a/b/v;->a:I

    return-void
.end method

.method public static f()Lcom/google/a/b/v;
    .locals 1

    new-instance v0, Lcom/google/a/b/v;

    invoke-direct {v0}, Lcom/google/a/b/v;-><init>()V

    return-object v0
.end method


# virtual methods
.method final bridge synthetic a()Ljava/util/Collection;
    .locals 1

    iget v0, p0, Lcom/google/a/b/v;->a:I

    invoke-static {v0}, Lcom/google/a/b/B;->a(I)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/a/b/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/a/b/t;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b()V
    .locals 0

    invoke-super {p0}, Lcom/google/a/b/t;->b()V

    return-void
.end method

.method public final bridge synthetic c()Ljava/util/Set;
    .locals 1

    invoke-super {p0}, Lcom/google/a/b/t;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic d()Ljava/util/Map;
    .locals 1

    invoke-super {p0}, Lcom/google/a/b/t;->d()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method final e()Ljava/util/Set;
    .locals 1

    iget v0, p0, Lcom/google/a/b/v;->a:I

    invoke-static {v0}, Lcom/google/a/b/B;->a(I)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/google/a/b/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .locals 1

    invoke-super {p0}, Lcom/google/a/b/t;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/a/b/t;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
