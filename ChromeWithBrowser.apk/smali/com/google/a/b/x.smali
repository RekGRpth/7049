.class public final Lcom/google/a/b/x;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/google/a/b/N;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/a/b/y;

    invoke-direct {v0}, Lcom/google/a/b/y;-><init>()V

    sput-object v0, Lcom/google/a/b/x;->a:Lcom/google/a/b/N;

    new-instance v0, Lcom/google/a/b/z;

    invoke-direct {v0}, Lcom/google/a/b/z;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Iterator;)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v0
.end method

.method public static a(Ljava/util/Iterator;Lcom/google/a/a/d;)Lcom/google/a/b/M;
    .locals 1

    invoke-static {p0}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/a/b/A;

    invoke-direct {v0, p0, p1}, Lcom/google/a/b/A;-><init>(Ljava/util/Iterator;Lcom/google/a/a/d;)V

    return-object v0
.end method

.method static a(Z)V
    .locals 1

    const-string v0, "no calls to next() since the last call to remove()"

    invoke-static {p0, v0}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    return-void
.end method

.method static b(Ljava/util/Iterator;)V
    .locals 1

    invoke-static {p0}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    return-void
.end method
