.class public final Lcom/google/android/a/a/a;
.super Lorg/apache/http/entity/AbstractHttpEntity;


# static fields
.field private static a:[B


# instance fields
.field private b:[Lcom/google/android/a/a/b;

.field private c:[B

.field private d:Lorg/apache/http/params/HttpParams;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/a/a/a;->a:[B

    return-void
.end method

.method public constructor <init>([Lcom/google/android/a/a/b;)V
    .locals 2

    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/a/a;->e:Z

    const-string v0, "multipart/form-data"

    invoke-virtual {p0, v0}, Lcom/google/android/a/a/a;->setContentType(Ljava/lang/String;)V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "parts cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/a/a/a;->b:[Lcom/google/android/a/a/b;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/a/a/a;->d:Lorg/apache/http/params/HttpParams;

    return-void
.end method

.method private a()[B
    .locals 5

    iget-object v0, p0, Lcom/google/android/a/a/a;->c:[B

    if-nez v0, :cond_1

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    const/16 v0, 0xb

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1e

    new-array v2, v0, [B

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    sget-object v3, Lcom/google/android/a/a/a;->a:[B

    sget-object v4, Lcom/google/android/a/a/a;->a:[B

    array-length v4, v4

    invoke-virtual {v1, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    aget-byte v3, v3, v4

    aput-byte v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v2, p0, Lcom/google/android/a/a/a;->c:[B

    :cond_1
    iget-object v0, p0, Lcom/google/android/a/a/a;->c:[B

    return-object v0
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/a/a/a;->isRepeatable()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/a/a/a;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Content has been consumed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/a/a;->e:Z

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iget-object v1, p0, Lcom/google/android/a/a/a;->b:[Lcom/google/android/a/a/b;

    iget-object v2, p0, Lcom/google/android/a/a/a;->c:[B

    invoke-static {v0, v1, v2}, Lcom/google/android/a/a/b;->a(Ljava/io/OutputStream;[Lcom/google/android/a/a/b;[B)V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v1
.end method

.method public final getContentLength()J
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/a/a/a;->b:[Lcom/google/android/a/a/b;

    invoke-direct {p0}, Lcom/google/android/a/a/a;->a()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/a/a/b;->a([Lcom/google/android/a/a/b;[B)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final getContentType()Lorg/apache/http/Header;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "multipart/form-data"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, "; boundary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-direct {p0}, Lcom/google/android/a/a/a;->a()[B

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/util/EncodingUtils;->getAsciiString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-Type"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public final isRepeatable()Z
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/a/a/a;->b:[Lcom/google/android/a/a/b;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/a/a/a;->b:[Lcom/google/android/a/a/b;

    invoke-static {}, Lcom/google/android/a/a/b;->e()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/a/a/a;->b:[Lcom/google/android/a/a/b;

    invoke-direct {p0}, Lcom/google/android/a/a/a;->a()[B

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/a/a/b;->a(Ljava/io/OutputStream;[Lcom/google/android/a/a/b;[B)V

    return-void
.end method
