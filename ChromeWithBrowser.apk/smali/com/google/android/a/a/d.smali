.class public final Lcom/google/android/a/a/d;
.super Lcom/google/android/a/a/c;


# instance fields
.field private a:[B

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const-string v0, "text/plain"

    const-string v1, "US-ASCII"

    const-string v2, "8bit"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/a/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "NULs may not be present in string parts"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p2, p0, Lcom/google/android/a/a/d;->b:Ljava/lang/String;

    return-void
.end method

.method private g()[B
    .locals 2

    iget-object v0, p0, Lcom/google/android/a/a/d;->a:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/a/a/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/http/util/EncodingUtils;->getBytes(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/a/d;->a:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/a/a/d;->a:[B

    return-object v0
.end method


# virtual methods
.method protected final a(Ljava/io/OutputStream;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/a/a/d;->g()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method protected final f()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/a/a/d;->g()[B

    move-result-object v0

    array-length v0, v0

    int-to-long v0, v0

    return-wide v0
.end method
