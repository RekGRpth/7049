.class Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;
.super Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;


# instance fields
.field private final mBookmarkId:J

.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;J)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;-><init>(Lcom/google/android/apps/chrome/AsyncTaskFragment;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;->mContext:Landroid/content/Context;

    iput-wide p2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;->mBookmarkId:J

    return-void
.end method


# virtual methods
.method protected onTaskFinished()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$800(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;->onRemove()V

    return-void
.end method

.method protected runBackgroundTask()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarksUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;->mBookmarkId:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method protected setDependentUIEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$200(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$700(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$900(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$800(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;->setBackEnabled(Z)V

    return-void
.end method
