.class Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;
.super Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mBookmarkId:Ljava/lang/Long;

.field private final mContext:Landroid/content/Context;

.field private mResult:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

.field final synthetic this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;Ljava/lang/Long;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;-><init>(Lcom/google/android/apps/chrome/AsyncTaskFragment;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mBookmarkId:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method protected onTaskFinished()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mBookmarkId:Ljava/lang/Long;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mResult:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    # invokes: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->handleDefaultBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$000(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mResult:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    # invokes: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->handleGetBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$100(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    goto :goto_0
.end method

.method protected runBackgroundTask()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mBookmarkId:Ljava/lang/Long;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->getDefaultBookmarkFolder(Landroid/content/Context;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mResult:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mBookmarkId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->getBookmarkNode(Landroid/content/Context;JI)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    goto :goto_0
.end method

.method protected setDependentUIEnabled(Z)V
    .locals 3

    const v2, 0x7f07004d

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$200(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$300(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    sget-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$3;->$SwitchMap$com$google$android$apps$chrome$AddEditBookmarkFragment$Mode:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;
    invoke-static {v1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$400(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$500(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$600(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$700(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$600(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$500(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(I)V

    :cond_0
    :pswitch_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
