.class public abstract Lcom/google/android/apps/chrome/ApplicationSwitches;
.super Ljava/lang/Object;


# static fields
.field public static final APPROXIMATION_SCALE:Ljava/lang/String; = "approximation-scale"

.field public static final APPROXIMATION_THUMBNAILS:Ljava/lang/String; = "approximation-thumbnails"

.field public static final AUTO_LOGIN_EXTRA_ACCOUNT:Ljava/lang/String; = "auto-login-extra-account"

.field public static final COMPROMISE_SIZE_THUMBNAILS:Ljava/lang/String; = "compromise-size-thumbnails"

.field public static final DEFAULT_COUNTRY_CODE_AT_INSTALL:Ljava/lang/String; = "default-country-code"

.field public static final DISABLE_FIRST_RUN_EXPERIENCE:Ljava/lang/String; = "disable-fre"

.field public static final DISABLE_INSTANT:Ljava/lang/String; = "disable-instant"

.field public static final DISABLE_SNAPSHOT:Ljava/lang/String; = "disable-snapshot"

.field public static final ENABLE_FULLSCREEN:Ljava/lang/String; = "enable-fullscreen"

.field public static final ENABLE_PERSISTENT_FULLSCREEN:Ljava/lang/String; = "enable-persistent-fullscreen"

.field public static final EXACT_MATCH_AUTO_LOGIN:Ljava/lang/String; = "exact-match-auto-login"

.field public static final FORCE_CRASH_DUMP_UPLOAD:Ljava/lang/String; = "force-dump-upload"

.field public static final FORCE_FIRST_RUN_EXPERIENCE:Ljava/lang/String; = "force-fre"

.field public static final FULL_SIZE_THUMBNAILS:Ljava/lang/String; = "full-size-thumbnails"

.field public static final HARDWARE_ACCELERATION:Ljava/lang/String; = "hardware-acceleration"

.field public static final HOME_PAGE:Ljava/lang/String; = "homepage"

.field public static final NOTIFICATION_CENTER_LOGGING:Ljava/lang/String; = "notification-center-logging"

.field public static final NO_RESTORE_STATE:Ljava/lang/String; = "no-restore-state"

.field public static final PRELOAD_WEBVIEW_CONTAINER:Ljava/lang/String; = "preload-webview-container"

.field public static final RENDER_PROCESS_LIMIT:Ljava/lang/String; = "renderer-process-limit"

.field public static final SIXTEEN_BIT_THUMBNAILS:Ljava/lang/String; = "sixteen-bit-thumbnails"

.field public static final SMALL_SIZE_THUMBNAILS:Ljava/lang/String; = "small-size-thumbnails"

.field public static final STRICT_MODE:Ljava/lang/String; = "strict-mode"

.field public static final SYNC_URL:Ljava/lang/String; = "sync-url"

.field public static final THIRTYTWO_BIT_THUMBNAILS:Ljava/lang/String; = "thirtytwo-bit-thumbnails"

.field public static final THUMBNAILS:Ljava/lang/String; = "thumbnails"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
