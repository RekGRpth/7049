.class public Lcom/google/android/apps/chrome/AsyncTaskFragment;
.super Landroid/app/Fragment;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DELAY_BEFORE_PROGRESS_DIALOG_MS:I = 0x12c

.field private static final MINIMUM_DIALOG_STAY_MS:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "AsyncTaskFragment"


# instance fields
.field protected mCurrentTask:Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;

.field private mDialogMessage:Ljava/lang/String;

.field private mDialogStaysEnough:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private mHasDialogStayedEnough:Z

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mShouldShowDialog:Z

.field private mShowProgressDialog:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/AsyncTaskFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/chrome/AsyncTaskFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$1;-><init>(Lcom/google/android/apps/chrome/AsyncTaskFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mShowProgressDialog:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/chrome/AsyncTaskFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$2;-><init>(Lcom/google/android/apps/chrome/AsyncTaskFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mDialogStaysEnough:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/AsyncTaskFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mShouldShowDialog:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/AsyncTaskFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->showDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/AsyncTaskFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mHasDialogStayedEnough:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/AsyncTaskFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mHasDialogStayedEnough:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/AsyncTaskFragment;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/AsyncTaskFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->taskFinished()V

    return-void
.end method

.method private hideDialog()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method private showDialog()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mShouldShowDialog:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mDialogMessage:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mDialogStaysEnough:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private taskFinished()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mShowProgressDialog:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mDialogStaysEnough:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->hideDialog()V

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mShouldShowDialog:Z

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mHasDialogStayedEnough:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;

    return-void
.end method

.method private updateTaskDependentUI()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->updateDependentUI()V

    :cond_0
    return-void
.end method


# virtual methods
.method public cancelFragmentAsyncTask()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->cancel(Z)Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->taskFinished()V

    :cond_0
    return-void
.end method

.method public isFragmentAsyncTaskRunning()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->isFragmentAsyncTaskRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->updateTaskDependentUI()V

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->showDialog()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-super {p0, v0}, Landroid/app/Fragment;->setRetainInstance(Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->cancelFragmentAsyncTask()V

    return-void
.end method

.method public onDetach()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->hideDialog()V

    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->hideDialog()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->showDialog()V

    goto :goto_0
.end method

.method public runFragmentAsyncTask(Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->isFragmentAsyncTaskRunning()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;

    iput-object p2, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mDialogMessage:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mShouldShowDialog:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mHasDialogStayedEnough:Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->mShowProgressDialog:Ljava/lang/Runnable;

    const-wide/16 v3, 0x12c

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setRetainInstance(Z)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/AsyncTaskFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    return-void
.end method
