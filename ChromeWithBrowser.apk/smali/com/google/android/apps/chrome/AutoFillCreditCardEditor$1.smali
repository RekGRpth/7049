.class Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field mCurrentCardNumber:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;->this$0:Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;->this$0:Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;

    # getter for: Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNumberText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->access$100(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;->mCurrentCardNumber:Ljava/lang/String;

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;->mCurrentCardNumber:Ljava/lang/String;

    const/16 v2, 0x2a

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;->this$0:Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;

    # getter for: Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNumberText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->access$100(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;->this$0:Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;

    # getter for: Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNumberText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->access$100(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;->this$0:Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;

    # getter for: Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNumberText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->access$100(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;->mCurrentCardNumber:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;->this$0:Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;

    # getter for: Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNumberText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->access$100(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;->mCurrentCardNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
