.class public Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;


# static fields
.field public static final FRAGMENT_NAME:Ljava/lang/String; = "com.google.android.apps.chrome.AutoFillCreditCardEditor"


# instance fields
.field private mExpirationMonth:Landroid/widget/Spinner;

.field private mExpirationYear:Landroid/widget/Spinner;

.field private mGUID:Ljava/lang/String;

.field private mNameText:Landroid/widget/EditText;

.field private mNumberText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNumberText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->deleteCreditCard()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->saveCreditCard()V

    return-void
.end method

.method private addCardDataToEditFields()V
    .locals 5

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mGUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getAutofillCreditCard(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v3

    const-string v0, "name"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNameText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const-string v0, "obfuscated"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNumberText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const-string v0, "month"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-ne v4, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_2
    const-string v0, "year"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_3

    move v3, v2

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v1

    if-ge v3, v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v1, v3}, Landroid/widget/Spinner;->setSelection(I)V

    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    check-cast v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v0, v2}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_3
    return-void

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method private deleteCreditCard()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mGUID:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mGUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->deleteAutofillCreditCard(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private hookupSaveCancelDeleteButtons(Landroid/view/View;)V
    .locals 3

    const v0, 0x7f0f0019

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mGUID:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mGUID:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    const v0, 0x7f0f001a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$3;-><init>(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f001b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$4;-><init>(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    new-instance v1, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$2;-><init>(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private saveCreditCard()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNameText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "name"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNumberText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "number"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v2, "month"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v2, "year"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mGUID:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setAutofillCreditCard(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    :goto_0
    return-void

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mGUID:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mGUID:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0
.end method


# virtual methods
.method addSpinnerAdapters()V
    .locals 6

    const v5, 0x1090009

    const v4, 0x1090008

    const/4 v1, 0x1

    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v2, v0, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    move v0, v1

    :goto_0
    const/16 v3, 0xc

    if-gt v0, v3, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v2, v0, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    move v0, v1

    :goto_1
    add-int/lit8 v3, v1, 0xa

    if-ge v0, v3, :cond_1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v2, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    invoke-super {p0, p3}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040004

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0700dc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const v0, 0x7f0f0015

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNameText:Landroid/widget/EditText;

    const v0, 0x7f0f0016

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNumberText:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNumberText:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$CardNumberKeyListener;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$CardNumberKeyListener;-><init>(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mNumberText:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor$1;-><init>(Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v0, 0x7f0f0017

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    const v0, 0x7f0f0018

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "guid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mGUID:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mGUID:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->mGUID:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->addSpinnerAdapters()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->addCardDataToEditFields()V

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/AutoFillCreditCardEditor;->hookupSaveCancelDeleteButtons(Landroid/view/View;)V

    return-object v1
.end method
