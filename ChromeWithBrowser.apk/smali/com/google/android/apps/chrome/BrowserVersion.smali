.class public Lcom/google/android/apps/chrome/BrowserVersion;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTermsOfServiceHtml()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/BrowserVersion;->nativeGetTermsOfServiceHtml()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTermsOfServiceString()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/BrowserVersion;->nativeGetTermsOfServiceString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isOfficialBuild()Z
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/BrowserVersion;->nativeIsOfficialBuild()Z

    move-result v0

    return v0
.end method

.method private static native nativeGetTermsOfServiceHtml()Ljava/lang/String;
.end method

.method private static native nativeGetTermsOfServiceString()Ljava/lang/String;
.end method

.method private static native nativeIsOfficialBuild()Z
.end method
