.class public Lcom/google/android/apps/chrome/ChromeActivity;
.super Lcom/google/android/apps/chrome/ChromeBaseActivity;


# instance fields
.field private mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeBaseActivity;-><init>()V

    return-void
.end method

.method public static getApplicationLabel(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lorg/chromium/base/BuildInfo;->getPackageLabel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isOfficialBuild()Z
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/BrowserVersion;->isOfficialBuild()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getChromeApplication()Lcom/google/android/apps/chrome/ChromeMobileApplication;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    return-object v0
.end method

.method public getCurrentTab()Lcom/google/android/apps/chrome/Tab;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/EmptyTabModel;->getInstance()Lcom/google/android/apps/chrome/EmptyTabModel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrentView()Lorg/chromium/content/browser/ContentView;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    return-object v0
.end method

.method public getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-object v0
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-void
.end method
