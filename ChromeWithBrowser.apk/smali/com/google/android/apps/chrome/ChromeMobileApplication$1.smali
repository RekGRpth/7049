.class Lcom/google/android/apps/chrome/ChromeMobileApplication$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$initSyncManager:Z

.field final synthetic val$retVal:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeMobileApplication;Landroid/content/Context;Ljava/util/concurrent/atomic/AtomicBoolean;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;->val$retVal:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-boolean p4, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;->val$initSyncManager:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;->val$context:Landroid/content/Context;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lorg/chromium/content/browser/AndroidBrowserProcess;->init(Landroid/content/Context;I)Z
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;->val$initSyncManager:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    # getter for: Lcom/google/android/apps/chrome/ChromeMobileApplication;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unable to start browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;->val$retVal:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method
