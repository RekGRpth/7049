.class public Lcom/google/android/apps/chrome/ChromeMobileApplication;
.super Landroid/app/Application;


# static fields
.field private static final CHROME_MANDATORY_PAKS:[Ljava/lang/String;

.field public static final COMMAND_LINE_FILE:Ljava/lang/String; = "/data/local/chrome-command-line"

.field private static final DEFAULT_CLIENT_ID:Ljava/lang/String; = "chrome-mobile"

.field private static final NATIVE_LIBRARY:Ljava/lang/String; = "chromeview"

.field private static final PRIVATE_DATA_DIRECTORY_SUFFIX:Ljava/lang/String; = "chrome"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mChromeIsInForeground:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->TAG:Ljava/lang/String;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "chrome.pak"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "en-US.pak"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "resources.pak"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "chrome_100_percent.pak"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->CHROME_MANDATORY_PAKS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mChromeIsInForeground:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static destroyIncognitoProfile()V
    .locals 0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->nativeDestroyIncognitoProfile()V

    return-void
.end method

.method public static flushPersistentData()V
    .locals 0

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->nativeFlushPersistentData()V

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void
.end method

.method public static initializeApplicationParameters()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->CHROME_MANDATORY_PAKS:[Ljava/lang/String;

    invoke-static {v0}, Lorg/chromium/content/browser/ResourceExtractor;->setMandatoryPaksToExtract([Ljava/lang/String;)V

    const-string v0, "chromeview"

    invoke-static {v0}, Lorg/chromium/content/app/LibraryLoader;->setLibraryToLoad(Ljava/lang/String;)V

    const-string v0, "chrome"

    invoke-static {v0}, Lorg/chromium/base/PathUtils;->setPrivateDataDirectorySuffix(Ljava/lang/String;)V

    return-void
.end method

.method private static native nativeDestroyIncognitoProfile()V
.end method

.method private static native nativeFlushPersistentData()V
.end method

.method private static native nativeRemoveSessionCookies()V
.end method

.method public static removeSessionCookies()V
    .locals 0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->nativeRemoveSessionCookies()V

    return-void
.end method


# virtual methods
.method public isChromeInForeground()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mChromeIsInForeground:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->initializeApplicationParameters()V

    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->setMinimumAndroidLogLevel(I)V

    return-void
.end method

.method public setChromeInForeground(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mChromeIsInForeground:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public startBrowserProcessesAndLoadLibrariesSync(Z)V
    .locals 1

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/data/local/chrome-command-line"

    invoke-static {v0}, Lorg/chromium/content/common/CommandLine;->initFromFile(Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->ensureInitialized()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startChromeBrowserProcesses(Z)Z

    const/16 v0, 0x25

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    return-void
.end method

.method public startChromeBrowserProcesses(Z)Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    new-instance v2, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;

    invoke-direct {v2, p0, v0, v1, p1}, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;-><init>(Lcom/google/android/apps/chrome/ChromeMobileApplication;Landroid/content/Context;Ljava/util/concurrent/atomic/AtomicBoolean;Z)V

    invoke-static {v2}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlocking(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method
