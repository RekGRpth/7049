.class Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;
.super Landroid/os/Handler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;->this$0:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    const/16 v1, 0x27

    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    # getter for: Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->access$200(Lcom/google/android/apps/chrome/ChromeNotificationCenter;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;->this$0:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->handleMessage(Landroid/os/Message;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v1, :cond_1

    const-string v1, "ChromeNotificationCenter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Message sent after notification center destroyed: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getNotificationTypeMap()Ljava/util/Map;
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->access$400()Ljava/util/Map;

    move-result-object v0

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Field;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
