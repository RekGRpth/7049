.class public Lcom/google/android/apps/chrome/ChromeNotificationCenter;
.super Ljava/lang/Object;


# static fields
.field public static final ACCESSIBILITY_STATE_CHANGED:I = 0x3e
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final ACCOUNT_ADDED:I = 0x32
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final ANIMATION_FINISHED:I = 0x29
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final ANIMATION_STARTED:I = 0x28
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final APP_ON_DESTORY:I = 0x27
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final AUTOCOMPLETE_SUGGESTIONS_RECEIVED:I = 0x1f
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final AUTO_LOGIN_DISABLED:I = 0x3a
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final AUTO_LOGIN_RESULT:I = 0x39
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final CONTEXT_MENU_CREATED:I = 0x1e
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final DEFAULT_SEARCH_ENGINE_CHANGED:I = 0x2a
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final DEFERRED_STARTUP:I = 0x26
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final FAVICON_CHANGED:I = 0x14
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final FIND_TOOLBAR_HIDE:I = 0x2d
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final FIND_TOOLBAR_SHOW:I = 0x2c
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final FIND_TOOLBAR_SHOWN:I = 0x3b
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final HTTP_POST_DOWNLOAD_FINISHED:I = 0xe
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final INFOBAR_ADDED:I = 0x17
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final INFOBAR_REMOVED:I = 0x18
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final INTERSTITIAL_PAGE_SHOWN:I = 0x33
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final LOAD_PROGRESS_CHANGED:I = 0xb
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final LOAD_STARTED:I = 0x1a
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final LOAD_STOPPED:I = 0x1b
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final NATIVE_LIBRARY_READY:I = 0x25
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final NEW_TAB_PAGE_READY:I = 0x20
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final ON_PAUSE:I = 0x13
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OUT_OF_MEMORY:I = 0x31
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OVERVIEW_MODE_HIDE_FINISHED:I = 0x1
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OVERVIEW_MODE_HIDE_START:I = 0xf
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OVERVIEW_MODE_SHOW_FINISHED:I = 0xd
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OVERVIEW_MODE_SHOW_START:I = 0x0
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PAGE_LOAD_FAILED:I = 0x1c
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PAGE_LOAD_FINISHED:I = 0x9
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PAGE_LOAD_STARTED:I = 0x8
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PAGE_TITLE_CHANGED:I = 0xa
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PAGE_URL_CHANGED:I = 0x19
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final POP_UP_MENU_HIDDEN:I = 0x37
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final POP_UP_MENU_SHOWN:I = 0x36
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PRERENDERED_PAGE_SHOWN:I = 0x23
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final SIDE_SWITCH_MODE_END:I = 0x12
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final SIDE_SWITCH_MODE_START:I = 0x11
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TABS_CLOSE_ALL_REQUEST:I = 0x2b
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TABS_STATE_LOADED:I = 0x10
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TABS_VIEW_HIDDEN:I = 0x35
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TABS_VIEW_SHOWN:I = 0x34
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_BACKGROUND_COLOR_CHANGED:I = 0x3f
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CLOSED:I = 0x5
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CLOSING:I = 0x24
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CONTENT_VIEW_CHANGED:I = 0x16
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CONTEXTUAL_ACTION_BAR_STATE_CHANGED:I = 0x22
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CRASHED:I = 0x6
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CREATED:I = 0x2
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CREATING:I = 0x30
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_MODEL_SELECTED:I = 0xc
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_MOVED:I = 0x4
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_PREFETCH_COMMITTED:I = 0x7
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_PRE_SELECTED:I = 0x15
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_SELECTED:I = 0x3
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_STRIP_DRAG_START:I = 0x2e
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_STRIP_DRAG_STOP:I = 0x2f
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ChromeNotificationCenter"

.field public static final TEMPLATE_URL_SERVICE_LOADED:I = 0x21
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final THUMBNAIL_UPDATED:I = 0x3d
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final VOICE_SEARCH_RESULTS:I = 0x3c
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final VOICE_SEARCH_START_REQUEST:I = 0x38
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field private static sNotificationMap:Ljava/util/Map;


# instance fields
.field private mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

.field private mIsDestroyed:Z

.field private mNotificationLogger:Landroid/os/Handler;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$1;-><init>(Lcom/google/android/apps/chrome/ChromeNotificationCenter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ChromeNotificationCenter;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mIsDestroyed:Z

    return v0
.end method

.method static synthetic access$400()Ljava/util/Map;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getNotificationTypeMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static broadcastImmediateNotification(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method public static broadcastImmediateNotification(ILandroid/os/Bundle;)V
    .locals 3

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput p0, v0, Landroid/os/Message;->what:I

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->broadcast(Landroid/os/Message;Z)V

    return-void
.end method

.method public static broadcastNotification(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method public static broadcastNotification(II)V
    .locals 2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput p0, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->broadcast(Landroid/os/Message;)V

    return-void
.end method

.method public static broadcastNotification(ILandroid/os/Bundle;)V
    .locals 2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput p0, v0, Landroid/os/Message;->what:I

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->broadcast(Landroid/os/Message;)V

    return-void
.end method

.method protected static disable()V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mIsDestroyed:Z

    return-void
.end method

.method protected static enable()V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mIsDestroyed:Z

    return-void
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;
    .locals 1

    # getter for: Lcom/google/android/apps/chrome/ChromeNotificationCenter$LazyHolder;->INSTANCE:Lcom/google/android/apps/chrome/ChromeNotificationCenter;
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$LazyHolder;->access$000()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    return-object v0
.end method

.method private static getNotificationTypeMap()Ljava/util/Map;
    .locals 6

    sget-object v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->sNotificationMap:Ljava/util/Map;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->sNotificationMap:Ljava/util/Map;

    const-class v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    const-class v4, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    sget-object v5, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->sNotificationMap:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Duplicate ChromeNotificationType defined"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->sNotificationMap:Ljava/util/Map;

    return-object v0
.end method

.method static getNotificationTypes()[Ljava/lang/Integer;
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getNotificationTypeMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    return-object v0
.end method

.method public static registerForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$100(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, p0, v1, p0}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->request(ILandroid/os/Handler;I)V

    return-void
.end method

.method public static registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 5

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p0, v0

    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$100(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v3}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->request(ILandroid/os/Handler;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static unregisterForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$100(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, p0, v1, p0}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->cancelRequest(ILandroid/os/Handler;I)V

    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$100(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public static unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 5

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p0, v0

    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$100(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v3}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->cancelRequest(ILandroid/os/Handler;I)V

    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$100(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Handler;->removeMessages(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public disableLogging()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mNotificationLogger:Landroid/os/Handler;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getNotificationTypes()[Ljava/lang/Integer;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    iget-object v5, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mNotificationLogger:Landroid/os/Handler;

    invoke-virtual {v4, v3, v5, v3}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->cancelRequest(ILandroid/os/Handler;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mNotificationLogger:Landroid/os/Handler;

    :cond_1
    return-void
.end method

.method public enableLogging()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mNotificationLogger:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$NotificationLogger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$NotificationLogger;-><init>(Lcom/google/android/apps/chrome/ChromeNotificationCenter$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mNotificationLogger:Landroid/os/Handler;

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getNotificationTypes()[Ljava/lang/Integer;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    iget-object v5, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mNotificationLogger:Landroid/os/Handler;

    invoke-virtual {v4, v3, v5, v3}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->request(ILandroid/os/Handler;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected getBroadcasterForTest()Lcom/google/android/apps/chrome/third_party/Broadcaster;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    return-object v0
.end method
