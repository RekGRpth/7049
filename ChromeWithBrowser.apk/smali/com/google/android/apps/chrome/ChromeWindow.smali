.class public Lcom/google/android/apps/chrome/ChromeWindow;
.super Lorg/chromium/ui/gfx/ActivityNativeWindow;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/ui/gfx/ActivityNativeWindow;-><init>(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method protected showCallbackNonExistentError(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeWindow;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->createWarningInfoBar(Ljava/lang/CharSequence;)Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->setExpireOnNavigation(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/chromium/ui/gfx/ActivityNativeWindow;->showCallbackNonExistentError(Ljava/lang/String;)V

    goto :goto_0
.end method
