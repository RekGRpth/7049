.class public interface abstract Lcom/google/android/apps/chrome/ContentViewHolder;
.super Ljava/lang/Object;


# virtual methods
.method public abstract addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
.end method

.method public abstract clearChildFocus(Landroid/view/View;)V
.end method

.method public abstract enableTabSwiping(Z)V
.end method

.method public abstract getChildCount()I
.end method

.method public abstract hideKeyboard(Ljava/lang/Runnable;)V
.end method

.method public abstract setFocusable(Z)V
.end method

.method public abstract setFullscreenHandler(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V
.end method

.method public abstract setLayoutManager(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;)V
.end method

.method public abstract setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
.end method
