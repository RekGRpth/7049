.class public Lcom/google/android/apps/chrome/GeolocationTracker;
.super Ljava/lang/Object;


# static fields
.field private static final LOCATION_PROVIDER:Ljava/lang/String; = "network"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLastKnownLocation(Landroid/content/Context;J)Landroid/location/Location;
    .locals 3

    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-lez v1, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/chrome/GeolocationTracker;->getLocationAge(Landroid/location/Location;)J

    move-result-wide v1

    cmp-long v1, v1, p1

    if-lez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method private static getLocationAge(Landroid/location/Location;)J
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method public static refreshLastKnownLocation(Landroid/content/Context;J)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/GeolocationTracker;->getLastKnownLocation(Landroid/content/Context;J)Landroid/location/Location;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "network"

    new-instance v2, Lcom/google/android/apps/chrome/GeolocationTracker$DummyListener;

    invoke-direct {v2, v3}, Lcom/google/android/apps/chrome/GeolocationTracker$DummyListener;-><init>(Lcom/google/android/apps/chrome/GeolocationTracker$1;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    :cond_0
    return-void
.end method
