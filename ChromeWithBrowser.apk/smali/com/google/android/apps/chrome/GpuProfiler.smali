.class public Lcom/google/android/apps/chrome/GpuProfiler;
.super Ljava/lang/Object;


# static fields
.field private static final ACTION_START:Ljava/lang/String; = "GPU_PROFILER_START"

.field private static final ACTION_STOP:Ljava/lang/String; = "GPU_PROFILER_STOP"

.field private static final FILE_EXTRA:Ljava/lang/String; = "file"

.field private static final TAG:Ljava/lang/String; = "GpuProfiler"


# instance fields
.field private final mBroadcastReceiver:Lcom/google/android/apps/chrome/GpuProfiler$ProfilerBroadcastReceiver;

.field private final mContext:Landroid/content/Context;

.field private mFilename:Ljava/lang/String;

.field private final mIntentFilter:Lcom/google/android/apps/chrome/GpuProfiler$ProfilerIntentFilter;

.field private mIsProfiling:Z

.field private mNativeGpuProfiler:I

.field private mShowToasts:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mShowToasts:Z

    iput-object p1, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/apps/chrome/GpuProfiler$ProfilerBroadcastReceiver;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/GpuProfiler$ProfilerBroadcastReceiver;-><init>(Lcom/google/android/apps/chrome/GpuProfiler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mBroadcastReceiver:Lcom/google/android/apps/chrome/GpuProfiler$ProfilerBroadcastReceiver;

    new-instance v0, Lcom/google/android/apps/chrome/GpuProfiler$ProfilerIntentFilter;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/GpuProfiler$ProfilerIntentFilter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mIntentFilter:Lcom/google/android/apps/chrome/GpuProfiler$ProfilerIntentFilter;

    return-void
.end method

.method private native nativeDestroy(I)V
.end method

.method private native nativeInit()I
.end method

.method private native nativeStartProfiling(ILjava/lang/String;)Z
.end method

.method private native nativeStopProfiling(I)V
.end method


# virtual methods
.method protected finalize()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mNativeGpuProfiler:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mNativeGpuProfiler:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/GpuProfiler;->nativeDestroy(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mNativeGpuProfiler:I

    :cond_0
    return-void
.end method

.method public getBroadcastReceiver()Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mBroadcastReceiver:Lcom/google/android/apps/chrome/GpuProfiler$ProfilerBroadcastReceiver;

    return-object v0
.end method

.method public getIntentFilter()Landroid/content/IntentFilter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mIntentFilter:Lcom/google/android/apps/chrome/GpuProfiler$ProfilerIntentFilter;

    return-object v0
.end method

.method public getOutputPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mFilename:Ljava/lang/String;

    return-object v0
.end method

.method public isProfiling()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mIsProfiling:Z

    return v0
.end method

.method logAndToastError(Ljava/lang/String;)V
    .locals 2

    const-string v0, "GpuProfiler"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mShowToasts:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method logAndToastInfo(Ljava/lang/String;)V
    .locals 2

    const-string v0, "GpuProfiler"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mShowToasts:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method protected onProfilingStopped()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/GpuProfiler;->isProfiling()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GpuProfiler"

    const-string v1, "Received onProfilingStopped, but we aren\'t profiling"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mContext:Landroid/content/Context;

    const v1, 0x7f07012b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mFilename:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/GpuProfiler;->logAndToastInfo(Ljava/lang/String;)V

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->setEnabledToMatchNative()V

    iput-boolean v4, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mIsProfiling:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mFilename:Ljava/lang/String;

    goto :goto_0
.end method

.method public registerReceiver(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/GpuProfiler;->getBroadcastReceiver()Landroid/content/BroadcastReceiver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/GpuProfiler;->getIntentFilter()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public startProfiling(Ljava/lang/String;Z)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mShowToasts:Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/GpuProfiler;->isProfiling()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "GpuProfiler"

    const-string v2, "Received startProfiling, but we\'re already profiling"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    iget v2, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mNativeGpuProfiler:I

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/GpuProfiler;->nativeInit()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mNativeGpuProfiler:I

    :cond_1
    iget v2, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mNativeGpuProfiler:I

    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/chrome/GpuProfiler;->nativeStartProfiling(ILjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mContext:Landroid/content/Context;

    const v2, 0x7f07012d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/GpuProfiler;->logAndToastError(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mContext:Landroid/content/Context;

    const v2, 0x7f07012a

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/GpuProfiler;->logAndToastInfo(Ljava/lang/String;)V

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->setEnabledToMatchNative()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mFilename:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mIsProfiling:Z

    move v0, v1

    goto :goto_0
.end method

.method public startProfiling(Z)Z
    .locals 5

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mShowToasts:Z

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mContext:Landroid/content/Context;

    const v1, 0x7f07012c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/GpuProfiler;->logAndToastError(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd-HHmmss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "chrome-profile-results-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/GpuProfiler;->startProfiling(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public stopProfiling()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/GpuProfiler;->isProfiling()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/GpuProfiler;->mNativeGpuProfiler:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/GpuProfiler;->nativeStopProfiling(I)V

    :cond_0
    return-void
.end method

.method public unregisterReceiver(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/GpuProfiler;->getBroadcastReceiver()Landroid/content/BroadcastReceiver;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
