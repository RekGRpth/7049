.class public Lcom/google/android/apps/chrome/HttpAuthDialog;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/chrome/browser/ChromeHttpAuthHandler$AutofillObserver;


# instance fields
.field private final mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

.field private final mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mPasswordView:Landroid/widget/EditText;

.field private mUsernameView:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/HttpAuthDialog;->createDialog()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/HttpAuthDialog;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/HttpAuthDialog;)Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/HttpAuthDialog;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/HttpAuthDialog;->getUsername()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/HttpAuthDialog;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/HttpAuthDialog;->getPassword()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private createDialog()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040019

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0f005c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mUsernameView:Landroid/widget/EditText;

    const v0, 0x7f0f005e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mPasswordView:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mPasswordView:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/apps/chrome/HttpAuthDialog$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/HttpAuthDialog$1;-><init>(Lcom/google/android/apps/chrome/HttpAuthDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const v0, 0x7f0f005a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f005b

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0f005d

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getMessageBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getUsernameLabelText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getPasswordLabelText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getOkButtonText()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getCancelButtonText()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getMessageTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/chrome/HttpAuthDialog$4;

    invoke-direct {v3, p0}, Lcom/google/android/apps/chrome/HttpAuthDialog$4;-><init>(Lcom/google/android/apps/chrome/HttpAuthDialog;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/chrome/HttpAuthDialog$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/HttpAuthDialog$3;-><init>(Lcom/google/android/apps/chrome/HttpAuthDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/HttpAuthDialog$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/HttpAuthDialog$2;-><init>(Lcom/google/android/apps/chrome/HttpAuthDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mDialog:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    return-void
.end method

.method private getPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mPasswordView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mUsernameView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->cancel()V

    return-void
.end method

.method public onAutofillDataAvailable(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mUsernameView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mPasswordView:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mUsernameView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    return-void
.end method

.method public reshow()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/apps/chrome/HttpAuthDialog;->getUsername()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/HttpAuthDialog;->getPassword()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/HttpAuthDialog;->createDialog()V

    iget-object v3, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mUsernameView:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mPasswordView:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mUsernameView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0
.end method

.method public show()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog;->mUsernameView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method
