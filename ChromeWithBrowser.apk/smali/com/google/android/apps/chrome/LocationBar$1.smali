.class Lcom/google/android/apps/chrome/LocationBar$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->onVoiceResults(Landroid/os/Bundle;)Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->getMatch()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->getMatch()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->getConfidence()F

    move-result v0

    const v2, 0x3f666666

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_3

    invoke-static {v1}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->nativeQualifyPartialURLQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {v1}, Lcom/google/android/apps/chrome/LocationBar;->getUrlForSearchQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/apps/chrome/LocationBar;->loadUrl(Ljava/lang/String;I)V
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/LocationBar;->access$1000(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/String;I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->setSearchQuery(Ljava/lang/String;)V

    goto :goto_0
.end method
