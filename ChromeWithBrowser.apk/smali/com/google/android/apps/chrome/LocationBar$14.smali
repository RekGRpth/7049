.class Lcom/google/android/apps/chrome/LocationBar$14;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$14;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    if-ne v0, v2, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$14;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->clearFocus()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$14;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mContentOverlay:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$3000(Lcom/google/android/apps/chrome/LocationBar;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return v2
.end method
