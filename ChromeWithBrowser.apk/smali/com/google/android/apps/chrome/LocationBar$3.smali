.class Lcom/google/android/apps/chrome/LocationBar$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private findMatchingSuggestion(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/apps/chrome/OmniboxSuggestion;
    .locals 3

    const/4 v1, 0x0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/LocationBar;->shouldAutocomplete()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$1900(Lcom/google/android/apps/chrome/LocationBar;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getMatchedQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getSuggestion()Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getSuggestion()Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/LocationBar$3;->urlTextMatchesSuggestion(Ljava/lang/String;Lcom/google/android/apps/chrome/OmniboxSuggestion;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private populatePotentialUrlCandidates(Ljava/util/Set;Ljava/lang/String;)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/google/android/apps/chrome/LocationBar;->simplifyUrlForDisplay(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    invoke-static {p2, v0}, Lcom/google/android/apps/chrome/LocationBar;->simplifyUrlForDisplay(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private urlTextMatchesSuggestion(Ljava/lang/String;Lcom/google/android/apps/chrome/OmniboxSuggestion;)Z
    .locals 3

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/LocationBar$3;->populatePotentialUrlCandidates(Ljava/util/Set;Ljava/lang/String;)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->isUrlSuggestion()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/chrome/LocationBar$3;->populatePotentialUrlCandidates(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/chrome/LocationBar$3;->populatePotentialUrlCandidates(Ljava/util/Set;Ljava/lang/String;)V

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v4, -0x1

    invoke-static {p3}, Lcom/google/android/apps/chrome/KeyNavigationUtil;->isGoDown(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v2}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->setSelection(I)V

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-static {p3}, Lcom/google/android/apps/chrome/KeyNavigationUtil;->isGoUp(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-static {p3}, Lcom/google/android/apps/chrome/KeyNavigationUtil;->isGoRight(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    if-eq v0, v4, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/OmniboxPopupAdapter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$1300(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/OmniboxPopupAdapter;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v3}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    const-string v4, ""

    invoke-virtual {v3, v4, v1}, Lcom/google/android/apps/chrome/LocationBar;->setUrlBarText(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getSuggestion()Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->clearListSelection()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setSelection(I)V

    move v0, v2

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p3}, Lcom/google/android/apps/chrome/KeyNavigationUtil;->isEnter(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    move v0, v1

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    if-eq v0, v4, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/OmniboxPopupAdapter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$1300(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/OmniboxPopupAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v1}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getSuggestion()Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getSuggestion()Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getTransition()I

    move-result v0

    :cond_6
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v3, v3, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-static {v3}, Lcom/google/android/apps/chrome/AndroidUtils;->hideKeyboard(Landroid/view/View;)Z

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/LocationBar;->loadUrl(Ljava/lang/String;I)V
    invoke-static {v3, v1, v0}, Lcom/google/android/apps/chrome/LocationBar;->access$1000(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/String;I)V

    move v0, v2

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$1400(Lcom/google/android/apps/chrome/LocationBar;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/google/android/apps/chrome/LocationBar$3;->findMatchingSuggestion(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mDismissedSuggestionItems:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$1500(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/google/android/apps/chrome/LocationBar$3;->findMatchingSuggestion(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v0

    :cond_8
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getTransition()I

    move-result v0

    :goto_2
    if-nez v1, :cond_b

    iget-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v4, v4, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mSimplifiedUrlLocation:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$1600(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v4, v4, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mSimplifiedUrlLocation:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$1600(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a

    :cond_9
    iget-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v4, v4, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mOriginalUrlLocation:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$1700(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v4, v4, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mOriginalUrlLocation:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$1700(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    :cond_a
    move-object v1, v3

    :cond_b
    if-nez v1, :cond_c

    iget-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v4

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$3;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    move-object v1, v3

    :cond_c
    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {v1}, Lcom/google/android/apps/chrome/utilities/URLUtilities;->fixUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_d
    # invokes: Lcom/google/android/apps/chrome/LocationBar;->nativeGetUrlForSearchQuery(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/chrome/LocationBar;->access$1800(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_e
    move v0, v2

    goto :goto_2
.end method
