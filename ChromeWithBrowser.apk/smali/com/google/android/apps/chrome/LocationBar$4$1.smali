.class Lcom/google/android/apps/chrome/LocationBar$4$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/LocationBar$4;

.field final synthetic val$editableText:Landroid/text/Editable;

.field final synthetic val$fullText:Ljava/lang/String;

.field final synthetic val$textWithoutAutocomplete:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/LocationBar$4;Landroid/text/Editable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->this$1:Lcom/google/android/apps/chrome/LocationBar$4;

    iput-object p2, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->val$editableText:Landroid/text/Editable;

    iput-object p3, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->val$fullText:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->val$textWithoutAutocomplete:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->this$1:Lcom/google/android/apps/chrome/LocationBar$4;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mLastUrlEditWasDelete:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$2300(Lcom/google/android/apps/chrome/LocationBar;)Z

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->val$editableText:Landroid/text/Editable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->val$editableText:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->val$fullText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->this$1:Lcom/google/android/apps/chrome/LocationBar$4;

    iget-object v1, v1, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->this$1:Lcom/google/android/apps/chrome/LocationBar$4;

    iget-object v2, v2, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->val$textWithoutAutocomplete:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->start(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4$1;->this$1:Lcom/google/android/apps/chrome/LocationBar$4;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->access$2202(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
