.class Lcom/google/android/apps/chrome/LocationBar$8;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRefineSuggestion(Lcom/google/android/apps/chrome/OmniboxSuggestion;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    const/4 v1, -0x1

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->access$202(Lcom/google/android/apps/chrome/LocationBar;I)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/LocationBar;->setUrlBarText(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    return-void
.end method

.method public onSelection(Lcom/google/android/apps/chrome/OmniboxSuggestion;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getTransition()I

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/LocationBar;->loadUrl(Ljava/lang/String;I)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/LocationBar;->access$1000(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->hideSuggestions()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-static {v0}, Lcom/google/android/apps/chrome/AndroidUtils;->hideKeyboard(Landroid/view/View;)Z

    return-void
.end method

.method public onSetUrlToSuggestion(Lcom/google/android/apps/chrome/OmniboxSuggestion;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/LocationBar;->setUrlBarText(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setSelection(I)V

    return-void
.end method
