.class Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
.super Landroid/widget/ListPopupWindow;


# instance fields
.field private final mAnchorPosition:[I

.field private mListItemCount:I

.field private mPreviousAnchorYPosition:I

.field private final mPreviousAppSize:Landroid/graphics/Rect;

.field private final mSuggestionHeight:I

.field private final mTempAppSize:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBar;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/LocationBar;Landroid/content/Context;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    const/4 v0, 0x0

    const v1, 0x101006b

    invoke-direct {p0, p2, v0, v1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mPreviousAppSize:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mTempAppSize:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mSuggestionHeight:I

    return-void
.end method


# virtual methods
.method public invalidateSuggestionViews()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/apps/chrome/SuggestionView;

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public show()V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mListItemCount:I

    :cond_0
    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->setInputMethodMode(I)V

    invoke-super {p0}, Landroid/widget/ListPopupWindow;->show()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mListItemCount:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mPreviousAppSize:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    aget v0, v0, v3

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mPreviousAnchorYPosition:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mListItemCount:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mTempAppSize:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mPreviousAnchorYPosition:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    aget v1, v1, v3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mTempAppSize:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mPreviousAppSize:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mTempAppSize:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    aget v1, v1, v3

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mListItemCount:I

    iget v2, p0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->mSuggestionHeight:I

    mul-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeight()I

    move-result v2

    if-ge v2, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeight()I

    move-result v1

    if-lt v1, v0, :cond_0

    goto :goto_0
.end method
