.class Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;
.super Landroid/view/inputmethod/InputConnectionWrapper;


# instance fields
.field private final mTempSelectionChar:[C

.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBar$UrlBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/LocationBar$UrlBar;Landroid/view/inputmethod/InputConnection;Z)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-direct {p0, p2, p3}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    const/4 v0, 0x1

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->mTempSelectionChar:[C

    return-void
.end method


# virtual methods
.method public commitText(Ljava/lang/CharSequence;I)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    if-ne p2, v0, :cond_3

    if-lez v2, :cond_3

    if-eq v2, v3, :cond_3

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v4

    if-lt v3, v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;
    invoke-static {v4}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$600(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v4

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I
    invoke-static {v4}, Lcom/google/android/apps/chrome/LocationBar;->access$200(Lcom/google/android/apps/chrome/LocationBar;)I

    move-result v4

    if-ne v4, v2, :cond_3

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-ne v4, v0, :cond_3

    add-int/lit8 v4, v2, 0x1

    iget-object v5, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->mTempSelectionChar:[C

    invoke-interface {v1, v2, v4, v5, v6}, Landroid/text/Editable;->getChars(II[CI)V

    iget-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->mTempSelectionChar:[C

    aget-char v4, v4, v6

    invoke-interface {p1, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;
    invoke-static {v4}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$700(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Landroid/text/TextWatcher;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;
    invoke-static {v4}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$700(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Landroid/text/TextWatcher;

    move-result-object v4

    invoke-interface {v4, v1, v6, v6, v6}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v4, v5, v3}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setSelection(II)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;
    invoke-static {v3}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$700(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Landroid/text/TextWatcher;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;
    invoke-static {v3}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$700(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Landroid/text/TextWatcher;

    move-result-object v3

    invoke-interface {v3, v1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;
    invoke-static {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$600(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/LocationBar;->access$202(Lcom/google/android/apps/chrome/LocationBar;I)I

    goto :goto_0

    :cond_3
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    goto :goto_0
.end method
