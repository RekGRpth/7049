.class Lcom/google/android/apps/chrome/LocationBar$UrlBar;
.super Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;


# static fields
.field private static final KEYBOARD_RETRY_ATTEMPTS:I = 0xa

.field private static final KEYBOARD_RETRY_DELAY_MS:J = 0x64L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mDismissedSuggestionItems:Ljava/util/ArrayList;

.field mInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

.field private mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

.field private mLocationBarTextWatcher:Landroid/text/TextWatcher;

.field private mOriginalUrlLocation:Ljava/lang/String;

.field private mScrollingGestureDetector:Landroid/view/GestureDetector;

.field private mShowKeyboardOnWindowFocus:Z

.field private mSimplifiedUrlLocation:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mDismissedSuggestionItems:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/LocationBar$UrlBar$4;-><init>(Lcom/google/android/apps/chrome/LocationBar$UrlBar;Landroid/view/inputmethod/InputConnection;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setTextColor(I)V

    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/LocationBar$UrlBar$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar$1;-><init>(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mScrollingGestureDetector:Landroid/view/GestureDetector;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mDismissedSuggestionItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mSimplifiedUrlLocation:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mOriginalUrlLocation:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Lcom/google/android/apps/chrome/LocationBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Landroid/text/TextWatcher;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/LocationBar$UrlBar;Landroid/text/TextWatcher;)Landroid/text/TextWatcher;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;

    return-object p1
.end method

.method private static getUrlContentsPrePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/16 v1, 0x2f

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method private scrollToTLD()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    if-gtz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setSelection(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public focusSearch(I)Landroid/view/View;
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputConnectionWrapper;->setTarget(Landroid/view/inputmethod/InputConnection;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

    return-object v0
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    invoke-virtual {v1, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar;->backKeyPressed()V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTextContextMenuItem(I)Z
    .locals 7

    const/4 v3, 0x1

    const/4 v1, 0x0

    const v0, 0x1020022

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v2

    if-eqz v2, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    :goto_0
    invoke-virtual {v2}, Landroid/content/ClipData;->getItemCount()I

    move-result v5

    if-ge v0, v5, :cond_0

    invoke-virtual {v2, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/ClipData$Item;->coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/google/android/apps/chrome/LocationBar;->nativeSanitizePastedText(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$100(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getSelectionStart()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getSelectionEnd()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2, v1, v0, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move v0, v3

    :goto_1
    return v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mOriginalUrlLocation:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mSimplifiedUrlLocation:Ljava/lang/String;

    if-nez v0, :cond_4

    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onTextContextMenuItem(I)Z

    move-result v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getSelectionStart()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getSelectionEnd()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    if-nez v0, :cond_7

    const v0, 0x1020020

    if-eq p1, v0, :cond_5

    const v0, 0x1020021

    if-ne p1, v0, :cond_7

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mSimplifiedUrlLocation:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mSimplifiedUrlLocation:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v2, v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mOriginalUrlLocation:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mSimplifiedUrlLocation:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mSimplifiedUrlLocation:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int v0, v2, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mOriginalUrlLocation:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/LocationBar;->setIgnoreURLBarModification(Z)V

    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setSelection(II)V

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onTextContextMenuItem(I)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setSelection(I)V

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/LocationBar;->setIgnoreURLBarModification(Z)V

    goto/16 :goto_1

    :cond_7
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onTextContextMenuItem(I)Z

    move-result v0

    goto/16 :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mScrollingGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    add-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getRight()I

    move-result v1

    int-to-float v1, v1

    sub-float v0, v1, v0

    const/high16 v1, 0x41700000

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/LocationBar;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->onClick(Landroid/view/View;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->hideSelectActionBar()V

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onWindowFocusChanged(Z)V

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mShowKeyboardOnWindowFocus:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/LocationBar$UrlBar$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar$3;-><init>(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->post(Ljava/lang/Runnable;)Z

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mShowKeyboardOnWindowFocus:Z

    :cond_1
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onWindowVisibilityChanged(I)V

    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mShowKeyboardOnWindowFocus:Z

    :cond_0
    return-void
.end method

.method protected openKeyboard()V
    .locals 3

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    new-instance v2, Lcom/google/android/apps/chrome/LocationBar$UrlBar$2;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar$2;-><init>(Lcom/google/android/apps/chrome/LocationBar$UrlBar;Ljava/util/concurrent/atomic/AtomicInteger;Landroid/os/Handler;)V

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public setAutocompleteText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->access$202(Lcom/google/android/apps/chrome/LocationBar;I)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    const/4 v3, 0x1

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mIgnoreURLBarModification:Z
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/LocationBar;->access$302(Lcom/google/android/apps/chrome/LocationBar;Z)Z

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->append(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$400(Lcom/google/android/apps/chrome/LocationBar;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/LocationBar;->updateSnapshotLabelVisibility()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$500(Lcom/google/android/apps/chrome/LocationBar;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$200(Lcom/google/android/apps/chrome/LocationBar;)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setSelection(II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mIgnoreURLBarModification:Z
    invoke-static {v0, v4}, Lcom/google/android/apps/chrome/LocationBar;->access$302(Lcom/google/android/apps/chrome/LocationBar;Z)Z

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v1, v4}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setUrl(Ljava/lang/String;Z)Z

    goto :goto_2
.end method

.method setLocationBar(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto :goto_0
.end method

.method public setUrl(Ljava/lang/String;Z)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    invoke-static {p1, v1}, Lcom/google/android/apps/chrome/LocationBar;->simplifyUrlForDisplay(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getUrlContentsPrePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mSimplifiedUrlLocation:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getUrlContentsPrePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mOriginalUrlLocation:Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->scrollToTLD()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :catch_0
    move-exception v2

    move-object p1, v0

    :cond_1
    iput-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mOriginalUrlLocation:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mSimplifiedUrlLocation:Ljava/lang/String;

    move-object v0, p1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
