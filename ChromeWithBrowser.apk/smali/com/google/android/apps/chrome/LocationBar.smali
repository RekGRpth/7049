.class public Lcom/google/android/apps/chrome/LocationBar;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/chrome/OnNativeLibraryReadyUIListener;
.implements Lcom/google/android/apps/chrome/bridge/AutocompleteController$OnSuggestionsReceivedListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CONTENT_OVERLAY_COLOR:I

.field private static final LOG_TAG:Ljava/lang/String;

.field private static NOTIFICATIONS:[I = null

.field private static final OMNIBOX_POPUP_RESIZE_DELAY_MS:J = 0x1eL

.field private static final OMNIBOX_SUGGESTIONS_TRANSITION_DURATION_MS:I = 0xc8

.field private static final OMNIBOX_SUGGESTION_START_DELAY_MS:J = 0x1eL

.field private static final VOICE_SEARCH_CONFIDENCE_NAVIGATE_THRESHOLD:F = 0.9f


# instance fields
.field mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

.field private mAutocompleteIndex:I

.field private mClearSuggestionsOnDismiss:Z

.field private mContentOverlay:Landroid/view/View;

.field private final mDeferredNativeRunnables:Ljava/util/List;

.field protected mDeleteButton:Landroid/widget/ImageButton;

.field private final mDomainAndRegistryColor:I

.field private mForceResizeSuggestionPopupRunnable:Ljava/lang/Runnable;

.field private mIgnoreURLBarModification:Z

.field private mInstantTrigger:Ljava/lang/Runnable;

.field private mLastUrlEditWasDelete:Z

.field private mLoadProgress:I

.field private mLoadProgressSimulator:Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;

.field private mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

.field protected mMicButton:Landroid/widget/ImageButton;

.field private mNativeLocationBar:I

.field protected mNavigationButton:Landroid/widget/ImageView;

.field private mNavigationButtonType:Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;

.field private mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mRequestSuggestions:Ljava/lang/Runnable;

.field private final mSchemeToDomainColor:I

.field protected mSecurityButton:Landroid/widget/ImageButton;

.field private mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

.field private mSecurityButtonShown:Z

.field private mSecurityIconType:Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;

.field private final mSnapshotPrefixLabel:Landroid/widget/TextView;

.field private final mSpanList:Ljava/util/ArrayList;

.field private final mStartSchemeDefaultColor:I

.field private final mStartSchemeEvSecureColor:I

.field private final mStartSchemeSecureColor:I

.field private final mStartSchemeSecurityErrorColor:I

.field private final mStartSchemeSecurityWarningColor:I

.field private final mSuggestionItems:Ljava/util/List;

.field private final mSuggestionListAdapter:Lcom/google/android/apps/chrome/OmniboxPopupAdapter;

.field private mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

.field private mSuggestionsShown:Z

.field private mTextChangeListeners:Ljava/util/ArrayList;

.field protected mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

.field private final mTrailingUrlColor:I

.field private final mUiHandler:Landroid/os/Handler;

.field protected mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

.field private mUrlFocusContentOverlay:Landroid/graphics/drawable/Drawable;

.field protected mUrlHasFocus:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/LocationBar;->LOG_TAG:Ljava/lang/String;

    const/16 v0, 0xa6

    invoke-static {v0, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/LocationBar;->CONTENT_OVERLAY_COLOR:I

    new-array v0, v1, [I

    const/16 v1, 0x3c

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/LocationBar;->NOTIFICATIONS:[I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mIgnoreURLBarModification:Z

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mLastUrlEditWasDelete:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mClearSuggestionsOnDismiss:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/chrome/LocationBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/LocationBar$1;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mStartSchemeDefaultColor:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mStartSchemeSecurityWarningColor:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mStartSchemeSecurityErrorColor:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mStartSchemeEvSecureColor:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mStartSchemeSecureColor:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSchemeToDomainColor:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mDomainAndRegistryColor:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTrailingUrlColor:I

    iput v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mLoadProgress:I

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040021

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f0f006b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    sget-boolean v0, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Missing navigation type view."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;->GLOBE:Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButtonType:Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;

    const v0, 0x7f0f006c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    sget-object v0, Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;->NONE:Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityIconType:Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;

    const v0, 0x7f0f006d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    sget-boolean v0, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Missing snapshot prefix view."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    const v0, 0x7f0f0072

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mDeleteButton:Landroid/widget/ImageButton;

    const v0, 0x7f0f006e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    const v1, 0x7f070030

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setHint(I)V

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "htc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getInputType()I

    move-result v1

    or-int/lit16 v1, v1, 0xb0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setInputType(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setLocationBar(Lcom/google/android/apps/chrome/LocationBar;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/LocationBar;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/OmniboxPopupAdapter;

    const v0, 0x7f0f0073

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mMicButton:Landroid/widget/ImageButton;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/LocationBar;->nativeSanitizePastedText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/LocationBar;->loadUrl(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/LocationBar;)Landroid/animation/AnimatorSet;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/LocationBar;)Landroid/animation/AnimatorSet;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/OmniboxPopupAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/OmniboxPopupAdapter;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/LocationBar;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1800(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/LocationBar;->nativeGetUrlForSearchQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/chrome/LocationBar;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->shouldAutocomplete()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/LocationBar;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateDeleteButtonVisibility()V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/LocationBar;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I

    return p1
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateNavigationButton()V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/LocationBar;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/LocationBar;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLastUrlEditWasDelete:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/google/android/apps/chrome/LocationBar;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mLastUrlEditWasDelete:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateSuggestionPopupPosition()V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/apps/chrome/LocationBar;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mClearSuggestionsOnDismiss:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/google/android/apps/chrome/LocationBar;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mClearSuggestionsOnDismiss:Z

    return p1
.end method

.method static synthetic access$2602(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mForceResizeSuggestionPopupRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$2702(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mInstantTrigger:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/google/android/apps/chrome/LocationBar;Lcom/google/android/apps/chrome/OmniboxSuggestion;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/LocationBar;->conditionallyPrerender(Lcom/google/android/apps/chrome/OmniboxSuggestion;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/apps/chrome/LocationBar;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLoadProgress:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/LocationBar;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mIgnoreURLBarModification:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/google/android/apps/chrome/LocationBar;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mContentOverlay:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/LocationBar;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mIgnoreURLBarModification:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/LocationBar;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateSnapshotLabelVisibility()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/LocationBar;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    return-object v0
.end method

.method private changeLocationBarIcon(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->shouldAnimateIconChanges()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    goto :goto_3
.end method

.method private clearSuggestions(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mDismissedSuggestionItems:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$1500(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mDismissedSuggestionItems:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$1500(Lcom/google/android/apps/chrome/LocationBar$UrlBar;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/OmniboxPopupAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->notifyDataSetChanged()V

    :cond_1
    return-void
.end method

.method private conditionallyPrerender(Lcom/google/android/apps/chrome/OmniboxSuggestion;)V
    .locals 3

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "disable-instant"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->allowPrerender()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->isUrlSuggestion()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->isInjectingAccessibilityScript()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getTransition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->prerenderUrl(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method private deEmphasizeUrl()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/style/CharacterStyle;

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method private emphasizeUrl()V
    .locals 11

    const/16 v3, 0x9

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/16 v10, 0x21

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v0, "chrome://"

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSchemeToDomainColor:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v4, v0, v2, v3, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mDomainAndRegistryColor:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {v4, v0, v3, v1, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isShowingSnapshot()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getBaseUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/URLUtilities;->getDomainAndRegistry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v0, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https"

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v5, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int v7, v6, v3

    if-lez v0, :cond_6

    iget v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mStartSchemeDefaultColor:I

    invoke-static {}, Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;->values()[Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;

    move-result-object v8

    iget v9, p0, Lcom/google/android/apps/chrome/LocationBar;->mNativeLocationBar:I

    invoke-direct {p0, v9}, Lcom/google/android/apps/chrome/LocationBar;->nativeGetSecurityLevel(I)I

    move-result v9

    aget-object v8, v8, v9

    sget-object v9, Lcom/google/android/apps/chrome/LocationBar$15;->$SwitchMap$com$google$android$apps$chrome$LocationBar$SecurityLevel:[I

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;->ordinal()I

    move-result v8

    aget v8, v9, v8

    packed-switch v8, :pswitch_data_0

    sget-boolean v8, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    if-nez v8, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :pswitch_0
    iget v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mStartSchemeSecurityWarningColor:I

    :cond_5
    :goto_3
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v8, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v4, v8, v2, v1, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    :cond_6
    if-eqz v6, :cond_7

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    iget v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mSchemeToDomainColor:I

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v4, v1, v0, v6, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    :cond_7
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mDomainAndRegistryColor:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v4, v0, v6, v7, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v7, v0, :cond_0

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mTrailingUrlColor:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {v4, v0, v7, v1, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    :pswitch_1
    iget v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mStartSchemeSecurityErrorColor:I

    new-instance v8, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v8}, Landroid/text/style/StrikethroughSpan;-><init>()V

    iget-object v9, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v4, v8, v2, v1, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_3

    :pswitch_2
    iget v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mStartSchemeEvSecureColor:I

    goto :goto_3

    :pswitch_3
    iget v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mStartSchemeSecureColor:I

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    goto :goto_0
.end method

.method public static getUrlForSearchQuery(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/LocationBar;->nativeGetUrlForSearchQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initSuggestionList()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/LocationBar$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/LocationBar$6;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getRootView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0f007b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    new-instance v0, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;-><init>(Lcom/google/android/apps/chrome/LocationBar;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    new-instance v1, Lcom/google/android/apps/chrome/LocationBar$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/LocationBar$7;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->setListSelector(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->setSoftInputMode(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getSuggestionPopupBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateSuggestionPopupPosition()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/OmniboxPopupAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/OmniboxPopupAdapter;

    new-instance v1, Lcom/google/android/apps/chrome/LocationBar$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/LocationBar$8;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->setSuggestionSelectionHandler(Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;)V

    goto :goto_0
.end method

.method private loadUrl(Ljava/lang/String;I)V
    .locals 3

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "chrome://newtab/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getUrl()Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/apps/chrome/GeolocationHeader;->getGeoHeader(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x2000000

    or-int/2addr v2, p2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->setUrlToPageUrl()V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->omniboxSearch()V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->requestFocus()Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->stop(Z)V

    :cond_3
    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->setUrlToPageUrl()V

    goto :goto_0
.end method

.method private native nativeDestroy(I)V
.end method

.method private native nativeGetSecurityLevel(I)I
.end method

.method private static native nativeGetUrlForSearchQuery(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native nativeInit()I
.end method

.method private native nativeOnSecurityButtonClicked(ILandroid/content/Context;)V
.end method

.method private static native nativeSanitizePastedText(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native nativeSearchProviderIsGoogle()Z
.end method

.method private setSecurityIcon(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private shouldAutocomplete()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v1

    invoke-static {v0}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v0

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static simplifyUrlForDisplay(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v0, "http"

    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz p1, :cond_1

    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    const-string v3, "www."

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "www."

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v2, v2, 0x4

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    const-string v2, "file"

    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v1}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    invoke-virtual {v1}, Ljava/net/URL;->getRef()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Ljava/net/URL;->getRef()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    const-string v2, "/"

    invoke-virtual {v1}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_4
    move-object p0, v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_5
    move-object v0, p0

    goto :goto_1
.end method

.method private startVoiceRecognition()V
    .locals 1

    const/16 v0, 0x38

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    return-void
.end method

.method private static suggestionTypeToNavigationButtonType(Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;)Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/LocationBar$15;->$SwitchMap$com$google$android$apps$chrome$OmniboxSuggestion$Type:[I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;->GLOBE:Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;->MAGNIFIER:Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;->MAGNIFIER:Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateContentOverlay()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionsShown:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlHasFocus:Z

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mContentOverlay:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f0079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mContentOverlay:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mContentOverlay:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/chrome/LocationBar$14;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/LocationBar$14;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mContentOverlay:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionsShown:Z

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/LocationBar;->getContentOverlayBackgroundDrawable(Z)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mContentOverlay:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mContentOverlay:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mContentOverlay:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateDeleteButtonVisibility()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->updateDeleteButton(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateNavigationButton()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getSuggestion()Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getType()Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->suggestionTypeToNavigationButtonType(Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;)Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->setNavigationButtonType(Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;->GLOBE:Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->setNavigationButtonType(Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;)V

    goto :goto_0
.end method

.method private updateSnapshotLabelVisibility()V
    .locals 5

    const/16 v3, 0x8

    const/high16 v4, -0x80000000

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlHasFocus:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->isShowingSnapshot()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_1

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getMeasuredWidth()I

    move-result v2

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getMeasuredHeight()I

    move-result v3

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->measure(II)V

    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v1, :cond_0

    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateSuggestionPopupPosition()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Calling positionSuggestionView before creating the popup."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->positionSuggestionPopup(Landroid/widget/ListPopupWindow;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->show()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->invalidateSuggestionViews()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mForceResizeSuggestionPopupRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mForceResizeSuggestionPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mForceResizeSuggestionPopupRunnable:Ljava/lang/Runnable;

    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/LocationBar$9;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/LocationBar$9;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mForceResizeSuggestionPopupRunnable:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mForceResizeSuggestionPopupRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x1e

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/LocationBar;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public addTextChangeListener(Lcom/google/android/apps/chrome/LocationBar$TextChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method backKeyPressed()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->hideSuggestions()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-static {v0}, Lcom/google/android/apps/chrome/AndroidUtils;->hideKeyboard(Landroid/view/View;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->setUrlToPageUrl()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->requestFocus()Z

    :cond_0
    return-void
.end method

.method public clearLoadProgressIndicator()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->setLoadProgress(I)V

    return-void
.end method

.method public destroy()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNativeLocationBar:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNativeLocationBar:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->nativeDestroy(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNativeLocationBar:I

    :cond_0
    return-void
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0

    return-void
.end method

.method protected getContentOverlayBackgroundDrawable(Z)Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlFocusContentOverlay:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    sget v1, Lcom/google/android/apps/chrome/LocationBar;->CONTENT_OVERLAY_COLOR:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlFocusContentOverlay:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlFocusContentOverlay:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method protected getContentView()Lorg/chromium/content/browser/ContentView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    goto :goto_0
.end method

.method protected getCurrentTab()Lcom/google/android/apps/chrome/Tab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    goto :goto_0
.end method

.method public getLoadProgress()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLoadProgress:I

    return v0
.end method

.method protected getNavigationButtonType()Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButtonType:Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;

    return-object v0
.end method

.method protected getSuggestionListPopup()Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    return-object v0
.end method

.method protected getSuggestionPopupAnchorView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method protected getSuggestionPopupBackground()Landroid/graphics/drawable/Drawable;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method getUrlBar()Lcom/google/android/apps/chrome/LocationBar$UrlBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    return-object v0
.end method

.method public hideSuggestions()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->stop(Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->setSuggestionsListVisibility(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->cancelPrerender()V

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/LocationBar;->clearSuggestions(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateNavigationButton()V

    goto :goto_0
.end method

.method public isSecurityButtonShown()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButtonShown:Z

    return v0
.end method

.method protected isVoiceSearchEnabled()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilities;->isRecognitionIntentPresent(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public notifyTextChanged(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/LocationBar$TextChangeListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/LocationBar$TextChangeListener;->locationBarTextChanged(Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mDeleteButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    const-string v0, ""

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->setUrlBarText(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->hideSuggestions()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateDeleteButtonVisibility()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNativeLocationBar:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->nativeOnSecurityButtonClicked(ILandroid/content/Context;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mMicButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->omniboxVoiceSearch()V

    const/16 v0, 0x38

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/LocationBar;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 11

    const/4 v10, 0x2

    const/high16 v9, 0x3f800000

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setCursorVisible(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    new-instance v0, Lcom/google/android/apps/chrome/LocationBar$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/LocationBar$2;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    new-array v2, v10, [Landroid/animation/Animator;

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    sget-object v4, Lcom/google/android/apps/chrome/ToolbarPhone;->ALPHA_ANIM_PROPERTY:Landroid/util/Property;

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    sget-object v4, Lcom/google/android/apps/chrome/ToolbarPhone;->ALPHA_ANIM_PROPERTY:Landroid/util/Property;

    new-array v5, v7, [F

    aput v9, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    new-array v2, v10, [Landroid/animation/Animator;

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    sget-object v4, Lcom/google/android/apps/chrome/ToolbarPhone;->ALPHA_ANIM_PROPERTY:Landroid/util/Property;

    new-array v5, v7, [F

    aput v9, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    sget-object v4, Lcom/google/android/apps/chrome/ToolbarPhone;->ALPHA_ANIM_PROPERTY:Landroid/util/Property;

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-void
.end method

.method public onNativeLibraryReady()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->nativeInit()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNativeLocationBar:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mMicButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    new-instance v1, Lcom/google/android/apps/chrome/LocationBar$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/LocationBar$3;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v0, Lcom/google/android/apps/chrome/LocationBar$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/LocationBar$4;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    # setter for: Lcom/google/android/apps/chrome/LocationBar$UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->access$702(Lcom/google/android/apps/chrome/LocationBar$UrlBar;Landroid/text/TextWatcher;)Landroid/text/TextWatcher;

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setSelectAllOnFocus(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    new-instance v1, Lcom/google/android/apps/chrome/LocationBar$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/LocationBar$5;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;-><init>(Lcom/google/android/apps/chrome/bridge/AutocompleteController$OnSuggestionsReceivedListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    sget-object v0, Lcom/google/android/apps/chrome/LocationBar;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public onSuggestionsReceived([Lcom/google/android/apps/chrome/OmniboxSuggestion;Ljava/lang/String;)V
    .locals 10

    const-wide/16 v8, 0x1e

    const/4 v7, -0x1

    const/4 v5, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I

    if-eq v1, v7, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I

    if-lt v1, v2, :cond_2

    iget v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    array-length v2, p1

    if-ne v0, v2, :cond_6

    move v2, v3

    move v4, v3

    :goto_1
    array-length v0, p1

    if-ge v2, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getSuggestion()Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v6

    aget-object v7, p1, v2

    invoke-virtual {v6, v7}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getMatchedQuery()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    aget-object v6, p1, v2

    invoke-direct {v4, v6, v1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;-><init>(Lcom/google/android/apps/chrome/OmniboxSuggestion;Ljava/lang/String;)V

    invoke-interface {v0, v2, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v4, v5

    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/google/android/apps/chrome/LocationBar;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Invalid autocomplete index: %d, for url text: \'%s\'."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v3

    aput-object v0, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v7, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I

    const-string p2, ""

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setSelection(I)V

    :cond_3
    move-object v1, v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    aget-object v6, p1, v2

    invoke-direct {v4, v6, v1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;-><init>(Lcom/google/android/apps/chrome/OmniboxSuggestion;Ljava/lang/String;)V

    invoke-interface {v0, v2, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v4, v5

    goto :goto_2

    :cond_5
    move v0, v3

    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->hideSuggestions()V

    :goto_4
    return-void

    :cond_6
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/LocationBar;->clearSuggestions(Z)V

    move v0, v3

    :goto_5
    array-length v2, p1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionItems:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    aget-object v6, p1, v0

    invoke-direct {v4, v6, v1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;-><init>(Lcom/google/android/apps/chrome/OmniboxSuggestion;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mLastUrlEditWasDelete:Z

    if-nez v2, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->shouldAutocomplete()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mInstantTrigger:Ljava/lang/Runnable;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mInstantTrigger:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/LocationBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mInstantTrigger:Ljava/lang/Runnable;

    :cond_8
    aget-object v2, p1, v3

    new-instance v3, Lcom/google/android/apps/chrome/LocationBar$12;

    invoke-direct {v3, p0, v2}, Lcom/google/android/apps/chrome/LocationBar$12;-><init>(Lcom/google/android/apps/chrome/LocationBar;Lcom/google/android/apps/chrome/OmniboxSuggestion;)V

    iput-object v3, p0, Lcom/google/android/apps/chrome/LocationBar;->mInstantTrigger:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mInstantTrigger:Ljava/lang/Runnable;

    invoke-virtual {p0, v2, v8, v9}, Lcom/google/android/apps/chrome/LocationBar;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v2, ""

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v2, v1, p2}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setAutocompleteText(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->initSuggestionList()V

    if-eqz v4, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/OmniboxPopupAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->notifySuggestionsChanged()V

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/LocationBar;->setSuggestionsListVisibility(Z)V

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/widget/ListView;->postInvalidateDelayed(J)V

    :cond_b
    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateNavigationButton()V

    goto :goto_4

    :cond_c
    move v0, v5

    move v4, v5

    goto :goto_3
.end method

.method protected onUrlFocusChange(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlHasFocus:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateDeleteButtonVisibility()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateSnapshotLabelVisibility()V

    if-eqz p1, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->deEmphasizeUrl()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onUrlFocusChange(Z)V

    :cond_1
    if-nez p1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->isSecurityButtonShown()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->changeLocationBarIcon(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setCursorVisible(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateContentOverlay()V

    if-eqz p1, :cond_2

    invoke-static {}, Lcom/google/android/apps/chrome/LocationBar;->nativeSearchProviderIsGoogle()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/GeolocationHeader;->primeLocationForGeoHeader(Landroid/content/Context;)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->hideSuggestions()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->setUrlToPageUrl()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->emphasizeUrl()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public performSearchQuery(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/chrome/LocationBar;->getUrlForSearchQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->loadUrl(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/LocationBar;->setSearchQuery(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected positionSuggestionPopup(Landroid/widget/ListPopupWindow;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getWidth()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    return-void
.end method

.method public refreshUrlStar()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateButtonStatus()V

    :cond_0
    return-void
.end method

.method public removeTextChangeListener(Lcom/google/android/apps/chrome/LocationBar$TextChangeListener;)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mTextChangeListeners:Ljava/util/ArrayList;

    :cond_1
    return-void
.end method

.method public requestUrlFocus()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->requestFocus()Z

    return-void
.end method

.method public setAutocompleteController(Lcom/google/android/apps/chrome/bridge/AutocompleteController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    return-void
.end method

.method public setIgnoreURLBarModification(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mIgnoreURLBarModification:Z

    return-void
.end method

.method protected setLoadProgress(I)V
    .locals 4

    const/16 v1, 0x64

    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLoadProgress:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-gez p1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    sget-boolean v0, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-le p1, v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    iput p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mLoadProgress:I

    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLoadProgress:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUiHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/LocationBar$13;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/LocationBar$13;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected setNavigationButtonType(Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButtonType:Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/LocationBar$15;->$SwitchMap$com$google$android$apps$chrome$LocationBar$NavigationButtonType:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    const v1, 0x7f020081

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButtonType:Lcom/google/android/apps/chrome/LocationBar$NavigationButtonType;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    const v1, 0x7f020083

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setSearchQuery(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mNativeLocationBar:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/chrome/LocationBar$10;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/LocationBar$10;-><init>(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/chrome/LocationBar;->setUrlBarText(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setSelection(II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->stop(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->start(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;Z)V

    new-instance v0, Lcom/google/android/apps/chrome/LocationBar$11;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/LocationBar$11;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected setSuggestionsListVisibility(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionsShown:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->setInputMethodMode(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getSuggestionPopupAnchorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->setAnchorView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateSuggestionPopupPosition()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->show()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateContentOverlay()V

    return-void

    :cond_1
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->dismiss()V

    goto :goto_0
.end method

.method setToolbar(Lcom/google/android/apps/chrome/ToolbarDelegate;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    return-void
.end method

.method setUrlBarText(Ljava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mIgnoreURLBarModification:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setUrl(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSpanList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateSnapshotLabelVisibility()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mIgnoreURLBarModification:Z

    return-void
.end method

.method public setUrlToPageUrl()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/chrome/LocationBar;->setUrlBarText(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v0, "chrome://newtab/"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "chrome://welcome/"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const-string v0, ""

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/chrome/LocationBar;->setUrlBarText(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isShowingSnapshot()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    :goto_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->setUrlBarText(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, ""

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/chrome/LocationBar;->setUrlBarText(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method protected shouldAnimateIconChanges()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlHasFocus:Z

    return v0
.end method

.method public simulatePageLoadProgress()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLoadProgressSimulator:Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;-><init>(Lcom/google/android/apps/chrome/LocationBar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLoadProgressSimulator:Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLoadProgressSimulator:Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->start()V

    return-void
.end method

.method protected updateDeleteButton(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mDeleteButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public updateLoadProgress(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLoadProgressSimulator:Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mLoadProgressSimulator:Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->cancel()V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/LocationBar;->setLoadProgress(I)V

    return-void
.end method

.method public updateLoadingState(ZZ)V
    .locals 2

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->setUrlToPageUrl()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->updateNavigationButton()V

    invoke-static {}, Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;->values()[Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mNativeLocationBar:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/LocationBar;->nativeGetSecurityLevel(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->updateSecurityIcon(Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;)V

    sget-object v1, Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;->NONE:Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;

    if-eq v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->deEmphasizeUrl()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->emphasizeUrl()V

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/LocationBar;->emphasizeUrl()V

    :cond_2
    return-void
.end method

.method protected updateMicButtonState()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar;->mMicButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar;->isVoiceSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected updateSecurityButton(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mUrlHasFocus:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->changeLocationBarIcon(Z)V

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButtonShown:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getToolbarView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateSecurityIcon(Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityIconType:Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityIconType:Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;

    sget-object v0, Lcom/google/android/apps/chrome/LocationBar$15;->$SwitchMap$com$google$android$apps$chrome$LocationBar$SecurityLevel:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->updateSecurityButton(Z)V

    :cond_2
    :goto_1
    sget-object v0, Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;->NONE:Lcom/google/android/apps/chrome/LocationBar$SecurityLevel;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBar;->updateSecurityButton(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200a3

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200a1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200a2

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
