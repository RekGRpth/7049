.class Lcom/google/android/apps/chrome/LocationBarPhone$1;
.super Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBarPhone;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/LocationBarPhone;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBarPhone$1;->this$0:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-direct {p0}, Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;-><init>()V

    return-void
.end method


# virtual methods
.method public resize(II)Landroid/graphics/Shader;
    .locals 8

    const/4 v7, 0x2

    const/16 v6, 0xff

    const/4 v1, 0x0

    new-instance v0, Landroid/graphics/LinearGradient;

    int-to-float v3, p1

    new-array v5, v7, [I

    const/4 v2, 0x0

    const/16 v4, 0x32

    invoke-static {v4, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    aput v4, v5, v2

    const/4 v2, 0x1

    const/4 v4, -0x1

    aput v4, v5, v2

    new-array v6, v7, [F

    fill-array-data v6, :array_0

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    return-object v0

    nop

    :array_0
    .array-data 4
        0x0
        0x3e99999a
    .end array-data
.end method
