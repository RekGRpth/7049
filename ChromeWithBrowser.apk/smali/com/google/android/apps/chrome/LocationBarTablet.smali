.class public Lcom/google/android/apps/chrome/LocationBarTablet;
.super Lcom/google/android/apps/chrome/LocationBar;


# static fields
.field private static final KEYBOARD_MODE_CHANGE_DELAY_MS:I = 0x12c


# instance fields
.field private mBookmarkButton:Landroid/view/View;

.field private mLoadProgressDrawable:Landroid/graphics/drawable/ClipDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/LocationBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/LocationBar;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBarTablet;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ClipDrawable;

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mLoadProgressDrawable:Landroid/graphics/drawable/ClipDrawable;

    const v0, 0x7f0f0075

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mBookmarkButton:Landroid/view/View;

    return-void
.end method

.method protected onUrlFocusChange(Z)V
    .locals 2

    const/16 v1, 0x8

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/LocationBar;->onUrlFocusChange(Z)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-static {v0}, Lcom/google/android/apps/chrome/AndroidUtils;->hideKeyboard(Landroid/view/View;)Z

    goto :goto_0
.end method

.method public requestUrlFocus()V
    .locals 3

    const/16 v2, 0x20

    invoke-super {p0}, Lcom/google/android/apps/chrome/LocationBar;->requestUrlFocus()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    if-eq v1, v2, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-static {v0}, Lcom/google/android/apps/chrome/AndroidUtils;->showKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected setLoadProgress(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, v1}, Lcom/google/android/apps/chrome/LocationBar;->setLoadProgress(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mLoadProgressDrawable:Landroid/graphics/drawable/ClipDrawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    return-void
.end method

.method public simulatePageLoadProgress()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBarTablet;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBarTablet;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "chrome://newtab"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/LocationBar;->simulatePageLoadProgress()V

    goto :goto_0
.end method

.method protected updateDeleteButton(Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/LocationBar;->updateDeleteButton(Z)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBarTablet;->mBookmarkButton:Landroid/view/View;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
