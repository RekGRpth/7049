.class Lcom/google/android/apps/chrome/Main$15;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Main;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$15;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$15;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main$15;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->queryCurrentActionBarHeight()I
    invoke-static {v3}, Lcom/google/android/apps/chrome/Main;->access$8000(Lcom/google/android/apps/chrome/Main;)I

    move-result v3

    int-to-float v3, v3

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabStripHeight:F
    invoke-static {}, Lcom/google/android/apps/chrome/Main;->access$8100()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    :cond_0
    return-void
.end method
