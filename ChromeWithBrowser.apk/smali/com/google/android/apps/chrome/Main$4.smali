.class Lcom/google/android/apps/chrome/Main$4;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Main;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$printCallback:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Main;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$4;->this$0:Lcom/google/android/apps/chrome/Main;

    iput-object p2, p0, Lcom/google/android/apps/chrome/Main$4;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/chrome/Main$4;->val$account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/apps/chrome/Main$4;->val$printCallback:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$4;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1200(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$4;->val$activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$4;->val$account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$4;->val$printCallback:Ljava/lang/Runnable;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->setupCloudPrint(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V

    goto :goto_0
.end method
