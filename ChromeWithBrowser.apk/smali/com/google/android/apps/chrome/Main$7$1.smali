.class Lcom/google/android/apps/chrome/Main$7$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/Main$7;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Main$7;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$7$1;->this$1:Lcom/google/android/apps/chrome/Main$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7$1;->this$1:Lcom/google/android/apps/chrome/Main$7;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mDeferedStartupNotified:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$7400(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7$1;->this$1:Lcom/google/android/apps/chrome/Main$7;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1200(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7$1;->this$1:Lcom/google/android/apps/chrome/Main$7;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/Main;->mDeferedStartupNotified:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main;->access$7402(Lcom/google/android/apps/chrome/Main;Z)Z

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/Main$7$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Main$7$1$1;-><init>(Lcom/google/android/apps/chrome/Main$7$1;)V

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    :cond_0
    return-void
.end method
