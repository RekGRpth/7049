.class Lcom/google/android/apps/chrome/Main$7;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Main;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/Main$7;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method

.method private postDeferredStartupIfNeeded()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mDeferedStartupNotified:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$7400(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1100(Lcom/google/android/apps/chrome/Main;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/Main$7$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Main$7$1;-><init>(Lcom/google/android/apps/chrome/Main$7;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/Main$7;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$7;->postDeferredStartupIfNeeded()V

    :cond_0
    :goto_0
    return-void

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mStartUpSnapshotHandler:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$7100(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mContentContainer:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$7000(Lcom/google/android/apps/chrome/Main;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->removeSnapshotIfNeeded(Landroid/view/ViewGroup;Z)V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$7;->postDeferredStartupIfNeeded()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->showUpdateInfobarIfNecessary()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$7200(Lcom/google/android/apps/chrome/Main;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mStartUpSnapshotHandler:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$7100(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mContentContainer:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$7000(Lcom/google/android/apps/chrome/Main;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->removeSnapshotIfNeeded(Landroid/view/ViewGroup;Z)V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$7;->postDeferredStartupIfNeeded()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->showUpdateInfobarIfNecessary()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$7200(Lcom/google/android/apps/chrome/Main;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "shown"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/app/ActionBar;->hide()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v1}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->showControlsForContextualMenuBar()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5600(Lcom/google/android/apps/chrome/Main;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->hideControlsForContextualMenuBar()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$7300(Lcom/google/android/apps/chrome/Main;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onStateRestored()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5000(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ContentViewHolder;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/apps/chrome/ContentViewHolder;->enableTabSwiping(Z)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.speech.extra.LANGUAGE_MODEL"

    const-string v2, "web_search"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "calling_package"

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Main;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.speech.extra.WEB_SEARCH_ONLY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    const/16 v2, 0x68

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/Main;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    const v1, 0x7f070054

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$7;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->updateMicButtonState()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x8 -> :sswitch_1
        0x9 -> :sswitch_2
        0x10 -> :sswitch_5
        0x1b -> :sswitch_3
        0x22 -> :sswitch_4
        0x38 -> :sswitch_6
    .end sparse-switch
.end method
