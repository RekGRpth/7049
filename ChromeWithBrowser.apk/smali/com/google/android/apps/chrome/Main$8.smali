.class Lcom/google/android/apps/chrome/Main$8;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/chrome/browser/NavigationPopup$NavigationPopupDelegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Main;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$8;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public openHistory()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$8;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-static {}, Lorg/chromium/chrome/browser/NavigationPopup;->getHistoryUrl()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_MENU:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-interface {v0, v1, v1, v2}, Lcom/google/android/apps/chrome/TabModel;->bringToFrontOrLaunchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)V

    return-void
.end method
