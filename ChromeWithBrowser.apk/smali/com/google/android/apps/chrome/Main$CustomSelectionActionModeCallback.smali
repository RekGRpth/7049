.class Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ActionMode$Callback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Main;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/Main$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;-><init>(Lcom/google/android/apps/chrome/Main;)V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->showControlsForContextualMenuBar()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5600(Lcom/google/android/apps/chrome/Main;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->hideControlsForContextualMenuBar()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$7300(Lcom/google/android/apps/chrome/Main;)V

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
