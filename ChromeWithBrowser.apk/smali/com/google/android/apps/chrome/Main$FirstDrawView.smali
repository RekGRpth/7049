.class Lcom/google/android/apps/chrome/Main$FirstDrawView;
.super Landroid/view/View;


# instance fields
.field private final mDrawRunnable:Ljava/lang/Runnable;

.field private mHasDrawn:Z

.field private mRunnablePosted:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/Main;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/Main;Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$FirstDrawView;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main$FirstDrawView;->mHasDrawn:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main$FirstDrawView;->mRunnablePosted:Z

    new-instance v0, Lcom/google/android/apps/chrome/Main$FirstDrawView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Main$FirstDrawView$1;-><init>(Lcom/google/android/apps/chrome/Main$FirstDrawView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main$FirstDrawView;->mDrawRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/Main$FirstDrawView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/Main$FirstDrawView;->mHasDrawn:Z

    return p1
.end method


# virtual methods
.method public hasDrawn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main$FirstDrawView;->mHasDrawn:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main$FirstDrawView;->mRunnablePosted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$FirstDrawView;->mDrawRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main$FirstDrawView;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main$FirstDrawView;->mRunnablePosted:Z

    :cond_0
    return-void
.end method
