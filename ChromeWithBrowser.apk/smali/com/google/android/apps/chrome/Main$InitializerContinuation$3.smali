.class Lcom/google/android/apps/chrome/Main$InitializerContinuation$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/Main$InitializerContinuation;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation$3;->this$1:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation$3;->this$1:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    # getter for: Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mGLPreLoader:Lcom/google/android/apps/chrome/gl/GLPreLoader;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->access$300(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)Lcom/google/android/apps/chrome/gl/GLPreLoader;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation$3;->this$1:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    # getter for: Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mGLPreLoader:Lcom/google/android/apps/chrome/gl/GLPreLoader;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->access$300(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)Lcom/google/android/apps/chrome/gl/GLPreLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/gl/GLPreLoader;->destroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation$3;->this$1:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    # setter for: Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mGLPreLoader:Lcom/google/android/apps/chrome/gl/GLPreLoader;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->access$302(Lcom/google/android/apps/chrome/Main$InitializerContinuation;Lcom/google/android/apps/chrome/gl/GLPreLoader;)Lcom/google/android/apps/chrome/gl/GLPreLoader;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation$3;->this$1:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    # getter for: Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandlerThread:Landroid/os/HandlerThread;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->access$900(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)Landroid/os/HandlerThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation$3;->this$1:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    # setter for: Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandlerThread:Landroid/os/HandlerThread;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->access$902(Lcom/google/android/apps/chrome/Main$InitializerContinuation;Landroid/os/HandlerThread;)Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation$3;->this$1:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    # setter for: Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandler:Landroid/os/Handler;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->access$1002(Lcom/google/android/apps/chrome/Main$InitializerContinuation;Landroid/os/Handler;)Landroid/os/Handler;

    return-void
.end method
