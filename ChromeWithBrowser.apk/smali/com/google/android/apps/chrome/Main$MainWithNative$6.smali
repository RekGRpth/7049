.class Lcom/google/android/apps/chrome/Main$MainWithNative$6;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Main$MainWithNative;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$6;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$6;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1500(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/tabs/TabsView;

    move-result-object v0

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/TabsView;->onViewportSizeChanged(II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$6;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mCurrentOrientation:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5200(Lcom/google/android/apps/chrome/Main;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$6;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative;->updateOrientation()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main$MainWithNative;->access$5300(Lcom/google/android/apps/chrome/Main$MainWithNative;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$6;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v1, v1, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mCurrentOrientation:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$5200(Lcom/google/android/apps/chrome/Main;)I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$6;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mShowingContextualActionBar:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5400(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$6;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5500(Lcom/google/android/apps/chrome/Main;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$6;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->showControlsForContextualMenuBar()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5600(Lcom/google/android/apps/chrome/Main;)V

    :cond_0
    return-void
.end method
