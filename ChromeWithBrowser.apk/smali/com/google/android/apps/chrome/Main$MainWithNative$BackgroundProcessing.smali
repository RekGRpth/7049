.class Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;
.super Ljava/lang/Object;


# static fields
.field private static final SUSPEND_TIMERS_AFTER_MS:I = 0x493e0


# instance fields
.field private mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

.field private mWebKitTimersAreSuspended:Z

.field final synthetic this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/Main$MainWithNative;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mWebKitTimersAreSuspended:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/Main$MainWithNative;Lcom/google/android/apps/chrome/Main$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;-><init>(Lcom/google/android/apps/chrome/Main$MainWithNative;)V

    return-void
.end method

.method static synthetic access$2202(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;)Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mWebKitTimersAreSuspended:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mWebKitTimersAreSuspended:Z

    return p1
.end method

.method static synthetic access$3600(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->onDestroy()V

    return-void
.end method

.method static synthetic access$3700(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->onPause()V

    return-void
.end method

.method static synthetic access$4000(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->onResume()V

    return-void
.end method

.method private onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1100(Lcom/google/android/apps/chrome/Main;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    :cond_0
    return-void
.end method

.method private onPause()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;-><init>(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;Lcom/google/android/apps/chrome/Main$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1100(Lcom/google/android/apps/chrome/Main;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method private onResume()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->this$1:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1100(Lcom/google/android/apps/chrome/Main;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mWebKitTimersAreSuspended:Z

    if-eqz v0, :cond_0

    invoke-static {v1}, Lorg/chromium/chrome/browser/ProcessUtils;->toggleWebKitSharedTimers(Z)V

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mWebKitTimersAreSuspended:Z

    goto :goto_0
.end method
