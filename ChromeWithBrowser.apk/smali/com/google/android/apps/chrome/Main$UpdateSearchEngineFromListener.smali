.class Lcom/google/android/apps/chrome/Main$UpdateSearchEngineFromListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$TemplateURLServiceLoadedListener;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$UpdateSearchEngineFromListener;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public onTemplateURLServiceLoaded()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$UpdateSearchEngineFromListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNativeForSearchEngine()V

    const/16 v0, 0x21

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->unregisterTemplateURLServiceLoadedListener(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$TemplateURLServiceLoadedListener;)V

    return-void
.end method
