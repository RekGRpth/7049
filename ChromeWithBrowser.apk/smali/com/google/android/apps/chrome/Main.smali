.class public Lcom/google/android/apps/chrome/Main;
.super Lcom/google/android/apps/chrome/ChromeActivity;

# interfaces
.implements Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;
.implements Lcom/google/android/apps/chrome/sync/SyncChooseAccountFragment$Listener;
.implements Lorg/chromium/chrome/browser/ApplicationLifetime$Observer;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ACTION_CLOSE_TABS:Ljava/lang/String; = "com.google.android.apps.chrome.ACTION_CLOSE_TABS"

.field private static final ACTION_LOW_MEMORY:Ljava/lang/String; = "com.google.android.apps.chrome.ACTION_LOW_MEMORY"

.field private static final ACTION_TRIM_MEMORY:Ljava/lang/String; = "com.google.android.apps.chrome.ACTION_TRIM_MEMORY"

.field private static final ADD_ACCOUNT_RESULT:I = 0x66

.field private static final BOOKMARK_REQUEST:I = 0x67

.field private static final DEFERRED_STARTUP_DELAY_MS:I = 0x3e8

.field private static final EXTRA_SHARE_SCREENSHOT:Ljava/lang/String; = "share_screenshot"

.field private static final FIRST_RUN_EXPERIENCE_RESULT:I = 0x65

.field private static final FRE_RUNNING:Ljava/lang/String; = "First run is running"

.field private static final HELP_URL:Ljava/lang/String; = "https://support.google.com/chrome/?p=chrome_help"

.field public static final INTENT_EXTRA_DISABLE_CRASH_DUMP_UPLOADING:Ljava/lang/String; = "disable_crash_dump_uploading"

.field public static final INTENT_EXTRA_TEST_RENDER_PROCESS_LIMIT:Ljava/lang/String; = "render_process_limit"

.field private static final IS_ENG_IMAGE:Z

.field private static final MAX_FEEDBACK_SCREENSHOT_DIMENSION:I = 0x258

.field private static final MAX_SHARE_SCREENSHOT_DIMENSION:I = 0x190

.field private static final NEW_VERSION_AVAILABLE_DELAY_MS:J = 0x64L

.field private static final NOTIFICATIONS:[I

.field private static final SLIDE_DURATION_MS:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "Main"

.field private static final VOICE_RECOGNITION_RESULT:I = 0x68

.field private static mTabStripHeight:F


# instance fields
.field private mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

.field private mChromeWindow:Lcom/google/android/apps/chrome/ChromeWindow;

.field private mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

.field private mContentContainer:Landroid/view/ViewGroup;

.field private mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

.field private mContextMenu:Landroid/view/Menu;

.field private mControlContainer:Landroid/view/ViewGroup;

.field private mCreatedTabOnStartup:Z

.field private mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

.field private mCurrentOrientation:I

.field private mCustomSelectionActionModeCallback:Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;

.field private mDeferedStartupNotified:Z

.field private mDestroyed:Z

.field private mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

.field private mFirstDrawView:Lcom/google/android/apps/chrome/Main$FirstDrawView;

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

.field private mGpuProfiler:Lcom/google/android/apps/chrome/GpuProfiler;

.field private final mHandler:Landroid/os/Handler;

.field private mInitializerContinuation:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

.field private mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;

.field private mIsOnFirstRun:Z

.field private mIsTablet:Z

.field private mKillChromeOnDestroy:Z

.field private mMemoryUsageMonitor:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

.field private mMenu:Landroid/view/Menu;

.field private mMenuAnchor:Landroid/view/View;

.field private mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

.field private mPartnerBookmarksShim:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

.field private mPopupMenu:Lcom/google/android/apps/chrome/PhoneMenuPopup;

.field private mPowerBroadcastReceiver:Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

.field private mRestartChromeOnDestroy:Z

.field private mShowUpdateInfoBar:Z

.field private mShowingContextualActionBar:Z

.field private mStartUpSnapshotHandler:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

.field private mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

.field private mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;

.field private mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

.field public mToolbar:Lcom/google/android/apps/chrome/Toolbar;

.field private mUIInitialized:Z

.field private mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

.field private mUmaStatsOptInListener:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

.field private mUpdateURL:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/Main;->$assertionsDisabled:Z

    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/chrome/Main;->IS_ENG_IMAGE:Z

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/Main;->NOTIFICATIONS:[I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :array_0
    .array-data 4
        0x6
        0x8
        0x9
        0x1b
        0x22
        0x10
        0x38
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mHandler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    iput-object v2, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    iput-object v2, p0, Lcom/google/android/apps/chrome/Main;->mPartnerBookmarksShim:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mShowingContextualActionBar:Z

    iput v1, p0, Lcom/google/android/apps/chrome/Main;->mCurrentOrientation:I

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mUIInitialized:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mIsOnFirstRun:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mShowUpdateInfoBar:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mDeferedStartupNotified:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mCreatedTabOnStartup:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mKillChromeOnDestroy:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mRestartChromeOnDestroy:Z

    new-instance v0, Lcom/google/android/apps/chrome/Main$7;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Main$7;-><init>(Lcom/google/android/apps/chrome/Main;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/Main$InitializerContinuation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mInitializerContinuation:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/Main;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/Main;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mDestroyed:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/tabs/TabsView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/MemoryUsageMonitor;)Lcom/google/android/apps/chrome/MemoryUsageMonitor;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mMemoryUsageMonitor:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    return-object p1
.end method

.method static synthetic access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/IntentHandler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/IntentHandler;)Lcom/google/android/apps/chrome/IntentHandler;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    return-object p1
.end method

.method static synthetic access$3000(Lcom/google/android/apps/chrome/Main;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mIsOnFirstRun:Z

    return v0
.end method

.method static synthetic access$3002(Lcom/google/android/apps/chrome/Main;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/Main;->mIsOnFirstRun:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->launchFirstRunExperience()V

    return-void
.end method

.method static synthetic access$3200(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/UmaStatsOptInListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mUmaStatsOptInListener:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/UmaStatsOptInListener;)Lcom/google/android/apps/chrome/UmaStatsOptInListener;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mUmaStatsOptInListener:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->launchWelcomePage()V

    return-void
.end method

.method static synthetic access$3400()[I
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/Main;->NOTIFICATIONS:[I

    return-object v0
.end method

.method static synthetic access$3500(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->hideMenus()V

    return-void
.end method

.method static synthetic access$3900(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->hideSuggestions()V

    return-void
.end method

.method static synthetic access$4100(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/OverviewBehavior;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4200(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->checkIfUpdateInfobarNecessary()V

    return-void
.end method

.method static synthetic access$4300(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    return-object v0
.end method

.method static synthetic access$4302(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    return-object p1
.end method

.method static synthetic access$4402(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;)Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mPartnerBookmarksShim:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/google/android/apps/chrome/Main;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    return v0
.end method

.method static synthetic access$4600(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->toggleOverview()V

    return-void
.end method

.method static synthetic access$4700(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main;->addOrEditBookmark(Lcom/google/android/apps/chrome/Tab;)V

    return-void
.end method

.method static synthetic access$4800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCustomSelectionActionModeCallback:Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/google/android/apps/chrome/Main;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mControlContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ContentViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    return-object v0
.end method

.method static synthetic access$5102(Lcom/google/android/apps/chrome/Main;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mMenuAnchor:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$5200(Lcom/google/android/apps/chrome/Main;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentOrientation:I

    return v0
.end method

.method static synthetic access$5202(Lcom/google/android/apps/chrome/Main;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/Main;->mCurrentOrientation:I

    return p1
.end method

.method static synthetic access$5400(Lcom/google/android/apps/chrome/Main;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mShowingContextualActionBar:Z

    return v0
.end method

.method static synthetic access$5500(Lcom/google/android/apps/chrome/Main;)Landroid/animation/ObjectAnimator;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$5502(Lcom/google/android/apps/chrome/Main;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic access$5600(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->showControlsForContextualMenuBar()V

    return-void
.end method

.method static synthetic access$5700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    return-object v0
.end method

.method static synthetic access$5702(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;)Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    return-object p1
.end method

.method static synthetic access$5800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/OverviewBehavior;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    return-object v0
.end method

.method static synthetic access$5802(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/OverviewBehavior;)Lcom/google/android/apps/chrome/OverviewBehavior;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    return-object p1
.end method

.method static synthetic access$5902(Lcom/google/android/apps/chrome/Main;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/Main;->mUIInitialized:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/Main;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main;->showInvalidStartupErrorDialog(I)V

    return-void
.end method

.method static synthetic access$6000(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->setDefaultCountryCodeForSearchEngineLocalization()V

    return-void
.end method

.method static synthetic access$6100(Lcom/google/android/apps/chrome/Main;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mCreatedTabOnStartup:Z

    return v0
.end method

.method static synthetic access$6102(Lcom/google/android/apps/chrome/Main;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/Main;->mCreatedTabOnStartup:Z

    return p1
.end method

.method static synthetic access$6200(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/uma/UmaSessionStats;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    return-object v0
.end method

.method static synthetic access$6202(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/uma/UmaSessionStats;)Lcom/google/android/apps/chrome/uma/UmaSessionStats;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    return-object p1
.end method

.method static synthetic access$6300(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ChromeWindow;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mChromeWindow:Lcom/google/android/apps/chrome/ChromeWindow;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->importBookmarksAfterOTAIfNeeded()V

    return-void
.end method

.method static synthetic access$6802(Lcom/google/android/apps/chrome/Main;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/Main;->mShowUpdateInfoBar:Z

    return p1
.end method

.method static synthetic access$6900(Lcom/google/android/apps/chrome/Main;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->showUpdateInfoBar()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/Main$FirstDrawView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mFirstDrawView:Lcom/google/android/apps/chrome/Main$FirstDrawView;

    return-object v0
.end method

.method static synthetic access$7000(Lcom/google/android/apps/chrome/Main;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$7100(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/StartUpSnapshotHandler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mStartUpSnapshotHandler:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    return-object v0
.end method

.method static synthetic access$7200(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->showUpdateInfobarIfNecessary()V

    return-void
.end method

.method static synthetic access$7300(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->hideControlsForContextualMenuBar()V

    return-void
.end method

.method static synthetic access$7400(Lcom/google/android/apps/chrome/Main;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mDeferedStartupNotified:Z

    return v0
.end method

.method static synthetic access$7402(Lcom/google/android/apps/chrome/Main;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/Main;->mDeferedStartupNotified:Z

    return p1
.end method

.method static synthetic access$7500(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->onDeferredStartup()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->inflateUIElements()V

    return-void
.end method

.method static synthetic access$8000(Lcom/google/android/apps/chrome/Main;)I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->queryCurrentActionBarHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$8100()F
    .locals 1

    sget v0, Lcom/google/android/apps/chrome/Main;->mTabStripHeight:F

    return v0
.end method

.method private addOrEditBookmark(Lcom/google/android/apps/chrome/Tab;)V
    .locals 6

    const/4 v5, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getBookmarkId()J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-nez v3, :cond_3

    const-string v1, "folder"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "title"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "url"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onBookmarkUiVisibilityChange(Z)V

    :cond_2
    const/16 v1, 0x67

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/Main;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_3
    const-string v3, "folder"

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v3, "_id"

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_1
.end method

.method private checkIfUpdateInfobarNecessary()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->isNewerVersionAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getMarketURLGetter()Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/omaha/MarketURLGetter;->getMarketURL(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mUpdateURL:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/Main$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Main$6;-><init>(Lcom/google/android/apps/chrome/Main;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method private static configureStrictMode()V
    .locals 4

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v1

    sget-boolean v0, Lcom/google/android/apps/chrome/Main;->IS_ENG_IMAGE:Z

    if-nez v0, :cond_0

    const-string v0, "strict-mode"

    invoke-virtual {v1, v0}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-static {}, Landroid/os/StrictMode;->enableDefaults()V

    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>(Landroid/os/StrictMode$ThreadPolicy;)V

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyFlashScreen()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyDeathOnNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    const-string v2, "death"

    const-string v3, "strict-mode"

    invoke-virtual {v1, v3}, Lorg/chromium/content/common/CommandLine;->getSwitchValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyDeath()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    new-instance v1, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-static {}, Landroid/os/StrictMode;->getVmPolicy()Landroid/os/StrictMode$VmPolicy;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>(Landroid/os/StrictMode$VmPolicy;)V

    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyDeath()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v1

    invoke-static {v1}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    :cond_1
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    :cond_2
    return-void
.end method

.method private createTabModelSelectorImpl(Landroid/os/Bundle;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/ChromeWindow;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeWindow;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mChromeWindow:Lcom/google/android/apps/chrome/ChromeWindow;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mChromeWindow:Lcom/google/android/apps/chrome/ChromeWindow;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeWindow;->restoreInstanceState(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "is_incognito_selected"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_1

    if-nez p1, :cond_1

    :goto_1
    new-instance v2, Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main;->mChromeWindow:Lcom/google/android/apps/chrome/ChromeWindow;

    invoke-direct {v2, p0, v0, v1, v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;ZZLorg/chromium/ui/gfx/NativeWindow;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->shouldUseAccessibilityMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main;->mContentContainer:Landroid/view/ViewGroup;

    invoke-direct {v0, v3}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->registerForNotifications()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->setChromeViewHolder(Lcom/google/android/apps/chrome/ContentViewHolder;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->overviewVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    invoke-interface {v3}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v3, :cond_4

    move v3, v1

    :goto_1
    if-nez v0, :cond_1

    if-eqz v4, :cond_5

    :cond_1
    if-nez v3, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/OverviewBehavior;->setEnableAnimations(Z)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    :goto_2
    return-object v0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v3, v2

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->clearOverviewMode()V

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/OverviewBehavior;->setEnableAnimations(Z)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    goto :goto_2
.end method

.method private getMenuThemeResourceId()I
    .locals 5

    const v1, 0x103006e

    const v0, 0x103006b

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v4, :cond_2

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/chrome/TabModel;->isIncognito()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v2, 0x1

    :cond_3
    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private goBack()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->goBack()V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/UmaRecordAction;->back(Z)V

    :cond_0
    return-void
.end method

.method private goForward()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->canGoForward()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->goForward()V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/UmaRecordAction;->forward(Z)V

    :cond_0
    return-void
.end method

.method private handleAccountSelection()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccountNames()Ljava/util/List;

    move-result-object v0

    sget-boolean v2, Lcom/google/android/apps/chrome/Main;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->onFirstRunSignIn(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method private hideControlsForContextualMenuBar()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    const-string v0, "controlTopMargin"

    const/4 v1, 0x1

    new-array v1, v1, [I

    aput v3, v1, v3

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/apps/chrome/Main$16;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Main$16;-><init>(Lcom/google/android/apps/chrome/Main;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/Main;->mShowingContextualActionBar:Z

    return-void
.end method

.method private hideMenus()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mPopupMenu:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mPopupMenu:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mPopupMenu:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NavigationPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NavigationPopup;->dismiss()V

    :cond_1
    return-void
.end method

.method private hideSuggestions()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->hideSuggestions()V

    :cond_0
    return-void
.end method

.method private importBookmarksAfterOTAIfNeeded()V
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->isDatabaseFilePresent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->importFromDatabaseAfterOTA()Z

    goto :goto_0
.end method

.method private inflateUIElements()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mFirstDrawView:Lcom/google/android/apps/chrome/Main$FirstDrawView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const v0, 0x7f0f007e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Toolbar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->initializeTabStackVisuals()V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v0, :cond_0

    const v0, 0x7f0f0080

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mStartUpSnapshotHandler:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->showLastUrlIfReady()Z

    return-void
.end method

.method private launchFirstRunExperience()V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->checkPerformFRE(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mIsOnFirstRun:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->clearState()V

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->isStableChannelBuild(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_4

    move v0, v1

    :goto_1
    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->setCrashUploadPreference(Landroid/content/Context;Z)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mCreatedTabOnStartup:Z

    invoke-static {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->userHasSeenToS(Landroid/content/Context;)Z

    move-result v4

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v6, "Nexus "

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v4, :cond_5

    if-nez v5, :cond_3

    if-eqz v0, :cond_5

    :cond_3
    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->launchWelcomePage()V

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunFlowComplete(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->handleAccountSelection()V

    if-eqz v3, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->beginUMAStatsInfoBarCountdown(Landroid/content/Context;)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    invoke-static {p0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    invoke-static {p0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->init(Lcom/google/android/apps/chrome/Main;)V

    :cond_7
    if-eqz v4, :cond_8

    if-eqz v0, :cond_9

    :cond_8
    move v2, v1

    :cond_9
    invoke-static {p0, v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->createLaunchFREIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    const/16 v2, 0x65

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/chrome/Main;->startActivityForResult(Landroid/content/Intent;I)V

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mIsOnFirstRun:Z

    goto :goto_0

    :cond_a
    move v0, v2

    goto :goto_3
.end method

.method private launchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mUIInitialized:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/OverviewBehavior;->hideOverview(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LINK:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/apps/chrome/TabModel;->launchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/Tab;->setSnapshotId(Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->receivedExternalIntent()V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0, p1, p2, p4, p5}, Lcom/google/android/apps/chrome/TabModel;->launchUrlFromExternalApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    goto :goto_0
.end method

.method private launchNewTabFromShortcut(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    new-instance v1, Lcom/google/android/apps/chrome/Main$13;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/Main$13;-><init>(Lcom/google/android/apps/chrome/Main;Z)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/ContentViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->shortcutNewIncognitoTab()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->shortcutNewTab()V

    goto :goto_0
.end method

.method private launchWelcomePage()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    const-string v1, "chrome://welcome/"

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_OVERVIEW:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/TabModel;->createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    invoke-interface {v0, v1, v4}, Lcom/google/android/apps/chrome/TabModel;->moveTab(II)V

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mCreatedTabOnStartup:Z

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v1

    if-le v1, v5, :cond_0

    invoke-interface {v0, v5}, Lcom/google/android/apps/chrome/TabModel;->setIndex(I)V

    :cond_0
    return-void
.end method

.method private onDeferredStartup()V
    .locals 3

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/RevenueStats;

    new-instance v0, Lcom/google/android/apps/chrome/Main$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Main$5;-><init>(Lcom/google/android/apps/chrome/Main;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/Main$5;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-static {p0}, Lcom/google/android/apps/chrome/nfc/BeamController;->setupAndQueueInit(Lcom/google/android/apps/chrome/Main;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mPartnerBookmarksShim:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->initialize(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mPartnerBookmarksShim:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->kickOffReading()V

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lorg/chromium/base/ActivityStatus;->onStateChange(Landroid/app/Activity;I)V

    invoke-static {p0}, Lorg/chromium/base/SystemMonitor;->create(Landroid/content/Context;)V

    invoke-static {p0}, Lorg/chromium/chrome/browser/ApplicationLifetime;->setObserver(Lorg/chromium/chrome/browser/ApplicationLifetime$Observer;)V

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void
.end method

.method private preInitializeUI()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    const v0, 0x7f0f0076

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentContainer:Landroid/view/ViewGroup;

    const v0, 0x7f0f007b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/Main;->mTabStripHeight:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    int-to-float v1, v0

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v2, "enable-fullscreen"

    invoke-virtual {v0, v2}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    :goto_0
    new-instance v0, Lcom/google/android/apps/chrome/Main$FirstDrawView;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/chrome/Main$FirstDrawView;-><init>(Lcom/google/android/apps/chrome/Main;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mFirstDrawView:Lcom/google/android/apps/chrome/Main$FirstDrawView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mFirstDrawView:Lcom/google/android/apps/chrome/Main$FirstDrawView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mStartUpSnapshotHandler:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mContentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->addSnapshotIfReady(Landroid/view/ViewGroup;)Z

    const v0, 0x7f0f0078

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v0, v1, v0

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v1

    const-string v2, "top-controls-height"

    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/chromium/content/common/CommandLine;->appendSwitchWithValue(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private prepareMenu(Landroid/view/Menu;)V
    .locals 11

    const v10, 0x7f0f0102

    const v9, 0x7f0f0101

    const v8, 0x7f0f00f0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->isIncognito()Z

    move-result v5

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v4

    if-eqz v6, :cond_b

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    if-eqz v0, :cond_b

    move v0, v1

    :goto_0
    iget-boolean v3, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v3, :cond_10

    if-nez v6, :cond_c

    move v3, v1

    :goto_1
    invoke-interface {p1, v8, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    const v3, 0x7f0f0100

    invoke-interface {p1, v3, v6}, Landroid/view/Menu;->setGroupVisible(IZ)V

    if-eqz v6, :cond_0

    if-eqz v5, :cond_e

    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    invoke-interface {v4}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v3

    if-lez v3, :cond_d

    move v3, v1

    :goto_2
    invoke-interface {v8, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    :goto_3
    if-eqz v6, :cond_1

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-eqz v3, :cond_2

    invoke-interface {v7}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    :cond_1
    const v3, 0x7f0f00f4

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-nez v5, :cond_15

    move v3, v1

    :goto_4
    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    if-nez v6, :cond_5

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v5

    const v3, 0x7f0f00fc

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_3

    if-eqz v4, :cond_16

    invoke-virtual {v4}, Lorg/chromium/content/browser/ContentView;->canGoForward()Z

    move-result v3

    if-eqz v3, :cond_16

    move v3, v1

    :goto_5
    invoke-interface {v7, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_3
    const v3, 0x7f0f00fb

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_4

    if-eqz v4, :cond_17

    invoke-virtual {v4}, Lorg/chromium/content/browser/ContentView;->canGoBack()Z

    move-result v3

    if-eqz v3, :cond_17

    move v3, v1

    :goto_6
    invoke-interface {v7, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_4
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/Tab;->getBookmarkId()J

    move-result-wide v3

    const-wide/16 v7, -0x1

    cmp-long v3, v3, v7

    if-eqz v3, :cond_18

    move v3, v1

    :goto_7
    const v4, 0x7f0f00fd

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-nez v3, :cond_19

    move v3, v1

    :goto_8
    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_5
    const v3, 0x7f0f00f7

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v3

    if-eqz v3, :cond_1a

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v4

    const-string v7, "chrome://"

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Tab;->isNTP()Z

    move-result v3

    if-eqz v3, :cond_1a

    :cond_6
    move v3, v1

    :goto_9
    iget-boolean v4, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-eqz v4, :cond_1c

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v4

    if-lez v4, :cond_1b

    if-nez v0, :cond_1b

    move v4, v1

    :goto_a
    and-int/2addr v3, v4

    :goto_b
    invoke-interface {v5, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v3

    if-eqz v3, :cond_1e

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v3

    invoke-virtual {v3}, Lorg/chromium/content/browser/ContentView;->getUseDesktopUserAgent()Z

    move-result v3

    if-eqz v3, :cond_1e

    move v3, v1

    :goto_c
    invoke-interface {v5, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    :cond_7
    const v3, 0x7f0f00f5

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-eqz v4, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v5

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v0, :cond_9

    :cond_8
    if-nez v6, :cond_1f

    :cond_9
    move v3, v1

    :goto_d
    if-eqz v5, :cond_20

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v5, "chrome://"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_20

    move v0, v1

    :goto_e
    if-eqz v3, :cond_21

    if-eqz v0, :cond_21

    :goto_f
    invoke-interface {v4, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_a
    return-void

    :cond_b
    move v0, v2

    goto/16 :goto_0

    :cond_c
    move v3, v2

    goto/16 :goto_1

    :cond_d
    move v3, v2

    goto/16 :goto_2

    :cond_e
    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/chrome/TabModelSelector;->getTotalTabCount()I

    move-result v3

    if-lez v3, :cond_f

    move v3, v1

    :goto_10
    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_3

    :cond_f
    move v3, v2

    goto :goto_10

    :cond_10
    invoke-interface {v7}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v3

    if-nez v3, :cond_11

    if-eqz v5, :cond_12

    :cond_11
    if-eqz v0, :cond_13

    :cond_12
    move v4, v1

    :goto_11
    if-nez v4, :cond_14

    move v3, v1

    :goto_12
    invoke-interface {p1, v8, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    const v3, 0x7f0f0103

    invoke-interface {p1, v3, v4}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto/16 :goto_3

    :cond_13
    move v4, v2

    goto :goto_11

    :cond_14
    move v3, v2

    goto :goto_12

    :cond_15
    move v3, v2

    goto/16 :goto_4

    :cond_16
    move v3, v2

    goto/16 :goto_5

    :cond_17
    move v3, v2

    goto/16 :goto_6

    :cond_18
    move v3, v2

    goto/16 :goto_7

    :cond_19
    move v3, v2

    goto/16 :goto_8

    :cond_1a
    move v3, v2

    goto/16 :goto_9

    :cond_1b
    move v4, v2

    goto/16 :goto_a

    :cond_1c
    if-nez v6, :cond_1d

    move v4, v1

    :goto_13
    and-int/2addr v3, v4

    goto/16 :goto_b

    :cond_1d
    move v4, v2

    goto :goto_13

    :cond_1e
    move v3, v2

    goto/16 :goto_c

    :cond_1f
    move v3, v2

    goto :goto_d

    :cond_20
    move v0, v2

    goto :goto_e

    :cond_21
    move v1, v2

    goto :goto_f
.end method

.method private prepareViewHierarchyForScreenshot(Landroid/view/View;Z)V
    .locals 2

    const/4 v0, 0x0

    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    check-cast p1, Landroid/view/ViewGroup;

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/google/android/apps/chrome/Main;->prepareViewHierarchyForScreenshot(Landroid/view/View;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    instance-of v1, p1, Landroid/view/SurfaceView;

    if-eqz v1, :cond_2

    if-nez p2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setWillNotDraw(Z)V

    :cond_2
    return-void
.end method

.method private queryCurrentActionBarHeight()I
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10102eb

    aput v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private setAlarmToRelaunchChrome()V
    .locals 7

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/Main;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x40000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v2, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    add-long/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    :cond_0
    return-void
.end method

.method private setDefaultCountryCodeForSearchEngineLocalization()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setDefaultCountryCodeAtInstall(Ljava/lang/String;)V

    return-void
.end method

.method private setWindowFlags()V
    .locals 4

    const/high16 v3, 0x1000000

    const/4 v0, 0x0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "hardware_acceleration"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v1

    const-string v2, "hardware-acceleration"

    invoke-virtual {v1, v2}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    sget-boolean v1, Lcom/google/android/apps/chrome/Main;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/ContentViewHolder;->getChildCount()I

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    :cond_3
    return-void
.end method

.method private shareUrl(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p3, :cond_0

    const-string v1, "share_screenshot"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    const v1, 0x7f0700b7

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/Main;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private showControlsForContextualMenuBar()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    const-string v0, "controlTopMargin"

    new-array v1, v6, [I

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->queryCurrentActionBarHeight()I

    move-result v3

    int-to-float v3, v3

    sget v4, Lcom/google/android/apps/chrome/Main;->mTabStripHeight:F

    sub-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    aput v2, v1, v5

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/apps/chrome/Main$14;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Main$14;-><init>(Lcom/google/android/apps/chrome/Main;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/apps/chrome/Main$15;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Main$15;-><init>(Lcom/google/android/apps/chrome/Main;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const v0, 0x7f0f007a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCurrentContextualMenuBarAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iput-boolean v6, p0, Lcom/google/android/apps/chrome/Main;->mShowingContextualActionBar:Z

    return-void
.end method

.method private showInvalidStartupErrorDialog(I)V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x104000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/Main$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/Main$2;-><init>(Lcom/google/android/apps/chrome/Main;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/Main$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Main$1;-><init>(Lcom/google/android/apps/chrome/Main;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private showUpdateInfoBar()Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    move-object v4, v0

    :goto_0
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/Tab;->isNTP()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->isOverlayVisable()Z

    move-result v5

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main;->mUpdateURL:Ljava/lang/String;

    if-nez v3, :cond_4

    move v3, v2

    :goto_2
    if-nez v0, :cond_1

    if-nez v5, :cond_1

    if-eqz v3, :cond_5

    :cond_1
    move v2, v1

    :goto_3
    return v2

    :cond_2
    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v3, v1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mUpdateURL:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f070140

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f070141

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;

    invoke-direct {v5, p0, v1, v3, v0}, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/chrome/Tab;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    goto :goto_3
.end method

.method private showUpdateInfobarIfNecessary()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mShowUpdateInfoBar:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->showUpdateInfoBar()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mShowUpdateInfoBar:Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private supportsFullscreen()Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const v1, 0x7f070250

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/Main;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "unknown"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "dev"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private toggleOverview()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    new-instance v2, Lcom/google/android/apps/chrome/Main$12;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/Main$12;-><init>(Lcom/google/android/apps/chrome/Main;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/ContentViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->setAccessibilityState(Z)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/OverviewBehavior;->hideOverview(Z)V

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Lorg/chromium/content/browser/ContentView;->setAccessibilityState(Z)V

    goto :goto_1
.end method

.method private waitForDebuggerIfNeeded()V
    .locals 2

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "wait-for-java-debugger"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Main"

    const-string v1, "Waiting for Java debugger to connect..."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Debug;->waitForDebugger()V

    const-string v0, "Main"

    const-string v1, "Java debugger connected. Resuming execution."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_1

    invoke-static {}, Lorg/chromium/content/browser/ContentVideoView;->getContentVideoView()Lorg/chromium/content/browser/ContentVideoView;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-static {p0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {p0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->getInflatedView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->requestQueryFocus()V

    :cond_1
    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->requestUrlFocus()V

    goto :goto_1

    :sswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x54 -> :sswitch_0
        0xaa -> :sswitch_1
        0xac -> :sswitch_1
        0xad -> :sswitch_1
        0xb1 -> :sswitch_1
        0xb2 -> :sswitch_1
        0xb3 -> :sswitch_1
        0xb4 -> :sswitch_1
        0xb5 -> :sswitch_1
        0xb6 -> :sswitch_1
    .end sparse-switch
.end method

.method protected generateScaledScreenshot(Landroid/view/View;IZ)Landroid/graphics/Bitmap;
    .locals 15

    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v5

    const/4 v1, 0x1

    :try_start_0
    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/Main;->prepareViewHierarchyForScreenshot(Landroid/view/View;Z)V

    if-nez v5, :cond_0

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-double v6, v1

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-double v8, v1

    double-to-int v3, v8

    double-to-int v1, v6

    if-lez p2, :cond_1

    move/from16 v0, p2

    int-to-double v10, v0

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v12

    div-double/2addr v10, v12

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v3, v8

    mul-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v1, v6

    :cond_1
    const/4 v6, 0x1

    invoke-static {v4, v3, v1, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz p3, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-eq v3, v4, :cond_2

    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    :try_start_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v2

    :cond_2
    :goto_0
    if-nez v5, :cond_3

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    :cond_3
    :goto_1
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/chrome/Main;->prepareViewHierarchyForScreenshot(Landroid/view/View;Z)V

    return-object v1

    :cond_4
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    if-lez v1, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    if-lez v1, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-double v6, v1

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-double v8, v1

    double-to-int v3, v8

    double-to-int v1, v6

    if-lez p2, :cond_7

    move/from16 v0, p2

    int-to-double v3, v0

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    div-double v10, v3, v10

    mul-double v3, v8, v10

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v3, v3

    mul-double/2addr v10, v6

    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v10

    long-to-int v1, v10

    move v4, v3

    move v3, v1

    :goto_2
    if-eqz p3, :cond_5

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    :goto_3
    invoke-static {v4, v3, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v10, Landroid/graphics/Canvas;

    invoke-direct {v10, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    int-to-double v11, v4

    div-double v8, v11, v8

    double-to-float v4, v8

    int-to-double v8, v3

    div-double v6, v8, v6

    double-to-float v3, v6

    invoke-virtual {v10, v4, v3}, Landroid/graphics/Canvas;->scale(FF)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v14, v1

    move-object v1, v2

    move-object v2, v14

    :goto_4
    :try_start_3
    const-string v3, "Main"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Unable to capture screenshot and scale it down."

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v5, :cond_3

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    goto :goto_1

    :cond_5
    :try_start_4
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v1

    if-nez v5, :cond_6

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    :cond_6
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/chrome/Main;->prepareViewHierarchyForScreenshot(Landroid/view/View;Z)V

    throw v1

    :catch_1
    move-exception v1

    move-object v14, v1

    move-object v1, v2

    move-object v2, v14

    goto :goto_4

    :cond_7
    move v4, v3

    move v3, v1

    goto :goto_2

    :cond_8
    move-object v1, v2

    goto/16 :goto_0
.end method

.method public getAccessibilityOverviewLayout()Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    return-object v0
.end method

.method public getChromeViewHolder()Lcom/google/android/apps/chrome/ContentViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    return-object v0
.end method

.method public getControlTopMargin()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    return v0
.end method

.method public getCurrentContextMenu()Landroid/view/Menu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContextMenu:Landroid/view/Menu;

    return-object v0
.end method

.method public getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    return-object v0
.end method

.method public getGpuProfiler()Lcom/google/android/apps/chrome/GpuProfiler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mGpuProfiler:Lcom/google/android/apps/chrome/GpuProfiler;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/GpuProfiler;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/GpuProfiler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mGpuProfiler:Lcom/google/android/apps/chrome/GpuProfiler;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mGpuProfiler:Lcom/google/android/apps/chrome/GpuProfiler;

    return-object v0
.end method

.method getLocationBar()Lcom/google/android/apps/chrome/LocationBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    return-object v0
.end method

.method public getNativeWindow()Lcom/google/android/apps/chrome/ChromeWindow;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mChromeWindow:Lcom/google/android/apps/chrome/ChromeWindow;

    return-object v0
.end method

.method public getOptionsMenu()Landroid/view/Menu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method getPowerBroadcastReceiver()Lcom/google/android/apps/chrome/PowerBroadcastReceiver;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mPowerBroadcastReceiver:Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mPowerBroadcastReceiver:Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mPowerBroadcastReceiver:Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    return-object v0
.end method

.method public getTabsView()Lcom/google/android/apps/chrome/tabs/TabsView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;

    return-object v0
.end method

.method public isOnFirstRun()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mIsOnFirstRun:Z

    return v0
.end method

.method public isOverlayVisable()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/TabsView;->isTabInteractive()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->onFirstRunSignIn(Landroid/accounts/Account;)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mInitializerContinuation:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 5

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mUIInitialized:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->back()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->systemBackForNavigation()V

    :goto_1
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->systemBack()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->moveTaskToBack(Z)Z

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v2, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/OverviewBehavior;->hideOverview(Z)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->back()Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getLaunchType()Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_MENU:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-ne v2, v3, :cond_4

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v3

    const-string v4, "https://support.google.com/chrome/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->closeTab(Lcom/google/android/apps/chrome/Tab;)Z

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getAppAssociatedWith()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    sget-object v4, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-ne v2, v4, :cond_5

    if-nez v3, :cond_5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->moveTaskToBack(Z)Z

    :cond_5
    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LINK:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-eq v2, v3, :cond_6

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-eq v2, v3, :cond_6

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-eq v2, v3, :cond_6

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_RESTORE:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-ne v2, v3, :cond_8

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getParentId()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_8

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-eq v2, v4, :cond_7

    :goto_2
    invoke-interface {v3, v1, v0}, Lcom/google/android/apps/chrome/TabModel;->closeTab(Lcom/google/android/apps/chrome/Tab;Z)Z

    :goto_3
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->systemBack()V

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->moveTaskToBack(Z)Z

    goto :goto_3

    :cond_9
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->systemBackForNavigation()V

    goto :goto_3
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->hideMenus()V

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/Tab;->contextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onContextMenuClosed(Landroid/view/Menu;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContextMenu:Landroid/view/Menu;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContextMenu:Landroid/view/Menu;

    if-eq v0, p1, :cond_2

    sget-boolean v0, Lcom/google/android/apps/chrome/Main;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Wrong instance of context menu closing!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContextMenu:Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/Tab;->contextMenuClosed(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v3, -0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/data/local/chrome-command-line"

    invoke-static {v0}, Lorg/chromium/content/common/CommandLine;->initFromFile(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->waitForDebuggerIfNeeded()V

    invoke-static {}, Lcom/google/android/apps/chrome/Main;->configureStrictMode()V

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "enable-test-intents"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "render_process_limit"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "render_process_limit"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v3, :cond_1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "--renderer-process-limit="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lorg/chromium/content/common/CommandLine;->appendSwitchesAndArguments([Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/nfc/BeamController;->onCreate()V

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    invoke-static {p0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    invoke-static {p0}, Lorg/chromium/content/browser/DeviceUtils;->addDeviceSpecificUserAgentSwitch(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->supportsFullscreen()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "enable-fullscreen"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->appendSwitch(Ljava/lang/String;)V

    :cond_2
    const-string v1, "enable-fullscreen"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "enable-top-controls-position-calculation"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->appendSwitch(Ljava/lang/String;)V

    const-string v1, "enable-compositor-frame-message"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->appendSwitch(Ljava/lang/String;)V

    :cond_3
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/Main;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v2, 0x7f0a0026

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    const-string v1, "onCreate->setContentView(R.layout.main)"

    invoke-static {v1}, Lorg/chromium/content/common/TraceEvent;->begin(Ljava/lang/String;)V

    const v1, 0x7f040022

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/Main;->setContentView(I)V

    const-string v1, "onCreate->setContentView(R.layout.main)"

    invoke-static {v1}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    const-string v1, "disable-snapshot"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->appendSwitch(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;-><init>(Lcom/google/android/apps/chrome/Main;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mStartUpSnapshotHandler:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    new-instance v0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    invoke-direct {v0, p0, p1, v6}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;-><init>(Lcom/google/android/apps/chrome/Main;Landroid/os/Bundle;Lcom/google/android/apps/chrome/Main$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mInitializerContinuation:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mInitializerContinuation:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    # invokes: Lcom/google/android/apps/chrome/Main$InitializerContinuation;->startBackgroundTasks()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->access$6500(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V

    invoke-static {p0}, Lorg/chromium/content/browser/ResourceExtractor;->get(Landroid/content/Context;)Lorg/chromium/content/browser/ResourceExtractor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ResourceExtractor;->startExtractingResources()V

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->enable()V

    invoke-static {p0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    new-instance v0, Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;

    invoke-direct {v0, p0, v6}, Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;-><init>(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/Main$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mCustomSelectionActionModeCallback:Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main;->createTabModelSelectorImpl(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->setWindowFlags()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mStartUpSnapshotHandler:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->prepareSnapshot()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->preInitializeUI()V

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContextMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/Main;->mContextMenu:Landroid/view/Menu;

    instance-of v0, p2, Lorg/chromium/content/browser/ContentView;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    sget-boolean v1, Lcom/google/android/apps/chrome/Main;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    if-eq p2, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/Tab;->popuplateContextMenu(Landroid/view/ContextMenu;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/ChromeActivity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mDestroyed:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mInitializerContinuation:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    # invokes: Lcom/google/android/apps/chrome/Main$InitializerContinuation;->onDestroy()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->access$7900(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V

    const/4 v0, 0x6

    invoke-static {p0, v0}, Lorg/chromium/base/ActivityStatus;->onStateChange(Landroid/app/Activity;I)V

    const/16 v0, 0x27

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->disable()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mMemoryUsageMonitor:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mMemoryUsageMonitor:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->destroy()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->onActivityDestroyed()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->destroy()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mChromeWindow:Lcom/google/android/apps/chrome/ChromeWindow;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mChromeWindow:Lcom/google/android/apps/chrome/ChromeWindow;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeWindow;->destroy()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->destroy()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/android/apps/chrome/Main;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->unregisterForNotifications()V

    :cond_6
    invoke-static {}, Lorg/chromium/chrome/browser/ApplicationLifetime;->removeObserver()V

    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->onDestroy()V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mRestartChromeOnDestroy:Z

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->setAlarmToRelaunchChrome()V

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mKillChromeOnDestroy:Z

    if-eqz v0, :cond_8

    const-string v0, "Main"

    const-string v1, "Forcefully killing process..."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    :cond_8
    return-void
.end method

.method public onFirstRunSignIn(Landroid/accounts/Account;)V
    .locals 2

    if-nez p1, :cond_0

    sget-boolean v0, Lcom/google/android/apps/chrome/Main;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/Main$3;

    invoke-direct {v0, p0, p0, p1}, Lcom/google/android/apps/chrome/Main$3;-><init>(Lcom/google/android/apps/chrome/Main;Landroid/app/Activity;Landroid/accounts/Account;)V

    new-instance v1, Lcom/google/android/apps/chrome/Main$4;

    invoke-direct {v1, p0, p0, p1, v0}, Lcom/google/android/apps/chrome/Main$4;-><init>(Lcom/google/android/apps/chrome/Main;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->signInToSelectedAccount(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/4 v0, 0x1

    const/16 v1, 0x52

    if-ne p1, v1, :cond_2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lorg/chromium/content/browser/ContentVideoView;->getContentVideoView()Lorg/chromium/content/browser/ContentVideoView;

    move-result-object v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mUIInitialized:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0f0074

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/Main;->showPopupMenu(Landroid/view/View;)Z

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main;->mMenuAnchor:Landroid/view/View;

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v3, v1}, Landroid/view/View;->setY(F)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mMenuAnchor:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/Main;->showPopupMenu(Landroid/view/View;)Z

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/ChromeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mUIInitialized:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/ChromeActivity;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v2

    :cond_1
    :goto_0
    return v2

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v3

    const v4, 0x7f070250

    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/Main;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "unknown"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    if-nez v0, :cond_4

    if-eqz v3, :cond_4

    new-instance v0, Lcom/google/android/apps/chrome/Main$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Main$8;-><init>(Lcom/google/android/apps/chrome/Main;)V

    new-instance v4, Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v3}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v3

    invoke-direct {v4, p0, v0, v3, v2}, Lorg/chromium/chrome/browser/NavigationPopup;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/NavigationPopup$NavigationPopupDelegate;Lorg/chromium/content/browser/NavigationClient;Z)V

    iput-object v4, p0, Lcom/google/android/apps/chrome/Main;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NavigationPopup;->shouldBeShown()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main;->mMenuAnchor:Landroid/view/View;

    invoke-virtual {v0, v2}, Lorg/chromium/chrome/browser/NavigationPopup;->setAnchorView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080073

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v2, v0}, Lorg/chromium/chrome/browser/NavigationPopup;->setWidth(I)V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v2, v0}, Lorg/chromium/chrome/browser/NavigationPopup;->setHorizontalOffset(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NavigationPopup;->show()V

    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/ChromeActivity;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .locals 7

    const/4 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v4

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-eqz v2, :cond_2

    if-gtz v4, :cond_3

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v2, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    move v2, v0

    :goto_1
    if-eqz v2, :cond_a

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    add-int/lit8 v2, v2, -0x7

    iget-boolean v5, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-eqz v5, :cond_5

    if-lez v2, :cond_5

    const/16 v5, 0x9

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    if-gt v2, v5, :cond_5

    add-int/lit8 v1, v2, -0x1

    invoke-interface {v3, v1}, Lcom/google/android/apps/chrome/TabModel;->setIndex(I)V

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    invoke-interface {v3}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v2

    sparse-switch p1, :sswitch_data_0

    move v0, v1

    goto :goto_0

    :sswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/Main;->launchNewTabFromShortcut(Z)V

    goto :goto_0

    :sswitch_1
    invoke-interface {v3}, Lcom/google/android/apps/chrome/TabModel;->isIncognito()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/Main;->launchNewTabFromShortcut(Z)V

    goto :goto_0

    :sswitch_2
    iget-boolean v5, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v5, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v1

    if-eqz v1, :cond_7

    add-int/lit8 v1, v2, -0x1

    rem-int/2addr v1, v4

    invoke-interface {v3, v1}, Lcom/google/android/apps/chrome/TabModel;->setIndex(I)V

    goto :goto_0

    :cond_7
    add-int/lit8 v1, v2, 0x1

    rem-int/2addr v1, v4

    invoke-interface {v3, v1}, Lcom/google/android/apps/chrome/TabModel;->setIndex(I)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->closeCurrentTab()Z

    goto :goto_0

    :sswitch_4
    const/16 v1, 0x2c

    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->shortcutFindInPage()V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-eqz v1, :cond_8

    const-string v2, "chrome://newtab/#bookmarks"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v6, v3}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    :goto_2
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->shortcutAllBookmarks()V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    const-string v2, "chrome://newtab/#bookmarks"

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_KEYBOARD:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-interface {v1, v2, v6, v3}, Lcom/google/android/apps/chrome/TabModel;->launchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    goto :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_0

    :sswitch_6
    const v1, 0x7f0f00c8

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_0

    :sswitch_7
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->goForward()V

    goto/16 :goto_0

    :sswitch_8
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->goBack()V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/Main;->addOrEditBookmark(Lcom/google/android/apps/chrome/Tab;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->zoomIn()Z

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->zoomOut()Z

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->zoomReset()Z

    goto/16 :goto_0

    :cond_a
    packed-switch p1, :pswitch_data_0

    move v0, v1

    goto/16 :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->launchNTP()Lcom/google/android/apps/chrome/Tab;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_c
        0x1e -> :sswitch_5
        0x20 -> :sswitch_9
        0x22 -> :sswitch_4
        0x28 -> :sswitch_6
        0x2a -> :sswitch_0
        0x30 -> :sswitch_1
        0x33 -> :sswitch_3
        0x3d -> :sswitch_2
        0x45 -> :sswitch_b
        0x46 -> :sswitch_a
        0x47 -> :sswitch_8
        0x48 -> :sswitch_7
        0x51 -> :sswitch_a
        0x86 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch
.end method

.method public onLowMemory()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->onLowMemory()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mMemoryUsageMonitor:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mMemoryUsageMonitor:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->onTrimMemory(I)V

    :cond_0
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mInitializerContinuation:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    # invokes: Lcom/google/android/apps/chrome/Main$InitializerContinuation;->onNewIntent(Landroid/content/Intent;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->access$7600(Lcom/google/android/apps/chrome/Main$InitializerContinuation;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(I)Z
    .locals 7

    const/4 v6, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_OVERVIEW:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    :goto_0
    packed-switch p1, :pswitch_data_0

    :goto_1
    :pswitch_0
    return v2

    :cond_0
    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_MENU:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->goBack()V

    :cond_1
    :goto_2
    move v2, v1

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->goForward()V

    goto :goto_2

    :pswitch_3
    if-eqz v4, :cond_1

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/Tab;->getProgress()I

    move-result v0

    const/16 v2, 0x64

    if-ge v0, v2, :cond_2

    invoke-virtual {v4}, Lorg/chromium/content/browser/ContentView;->stopLoading()V

    goto :goto_2

    :cond_2
    invoke-virtual {v4}, Lorg/chromium/content/browser/ContentView;->reload()V

    invoke-static {v1}, Lcom/google/android/apps/chrome/UmaRecordAction;->reload(Z)V

    goto :goto_2

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    new-instance v2, Lcom/google/android/apps/chrome/Main$9;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/Main$9;-><init>(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)V

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/ContentViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuNewTab()V

    goto :goto_2

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    new-instance v2, Lcom/google/android/apps/chrome/Main$10;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/Main$10;-><init>(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)V

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/ContentViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuNewIncognitoTab()V

    goto :goto_2

    :pswitch_6
    if-eqz v5, :cond_1

    const-string v2, "chrome://newtab/#bookmarks"

    invoke-virtual {v5, v2, v0, v6}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuAllBookmarks()V

    goto :goto_2

    :pswitch_7
    if-eqz v5, :cond_1

    const-string v2, "chrome://newtab/#open_tabs"

    invoke-virtual {v5, v2, v0, v6}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuOpenTabs()V

    goto :goto_2

    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    new-instance v2, Lcom/google/android/apps/chrome/Main$11;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/Main$11;-><init>(Lcom/google/android/apps/chrome/Main;)V

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/ContentViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuCloseTab()V

    goto :goto_2

    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->closeAllTabs()V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuCloseAllTabs()V

    goto :goto_2

    :pswitch_a
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->closeAllTabs()V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuCloseAllTabs()V

    goto :goto_2

    :pswitch_b
    const/16 v0, 0x2c

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuFindInPage()V

    goto :goto_2

    :pswitch_c
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Main;->addOrEditBookmark(Lcom/google/android/apps/chrome/Tab;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuAddToBookmarks()V

    goto/16 :goto_2

    :pswitch_d
    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModel;->isIncognito()Z

    move-result v2

    if-nez v2, :cond_3

    const/16 v0, 0x190

    invoke-virtual {p0, v4, v0, v1}, Lcom/google/android/apps/chrome/Main;->generateScaledScreenshot(Landroid/view/View;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_3
    invoke-virtual {v4}, Lorg/chromium/content/browser/ContentView;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lorg/chromium/content/browser/ContentView;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/apps/chrome/Main;->shareUrl(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuShare()V

    goto/16 :goto_2

    :pswitch_e
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v3, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, p0, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v3, 0x20020000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x258

    invoke-virtual {p0, v3, v4, v2}, Lcom/google/android/apps/chrome/Main;->generateScaledScreenshot(Landroid/view/View;IZ)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-static {v2}, Lcom/google/android/apps/chrome/SendGoogleFeedback;->setCurrentScreenshot(Landroid/graphics/Bitmap;)V

    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->startActivity(Landroid/content/Intent;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuSettings()V

    goto/16 :goto_2

    :pswitch_f
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v2

    const-string v3, "https://support.google.com/chrome/?p=chrome_help"

    sget-object v4, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_MENU:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-interface {v2, v3, v0, v4}, Lcom/google/android/apps/chrome/TabModel;->launchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    goto/16 :goto_2

    :pswitch_10
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getOptionsMenu()Landroid/view/Menu;

    move-result-object v0

    const v3, 0x7f0f00f7

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isNTP()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    invoke-virtual {v3}, Lorg/chromium/content/browser/ContentView;->getUseDesktopUserAgent()Z

    move-result v4

    if-nez v4, :cond_5

    move v2, v1

    :cond_5
    invoke-virtual {v3, v2, v0}, Lorg/chromium/content/browser/ContentView;->setUseDesktopUserAgent(ZZ)V

    goto/16 :goto_2

    :cond_6
    move v0, v2

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x7f0f00f1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_d
        :pswitch_b
        :pswitch_10
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_c
        :pswitch_3
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->onOptionsItemSelected(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    const/4 v0, 0x4

    invoke-static {p0, v0}, Lorg/chromium/base/ActivityStatus;->onStateChange(Landroid/app/Activity;I)V

    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mInitializerContinuation:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    # invokes: Lcom/google/android/apps/chrome/Main$InitializerContinuation;->onPause()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->access$7700(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V

    invoke-static {}, Lcom/google/android/apps/chrome/nfc/BeamController;->pauseRegistration()V

    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mInitializerContinuation:Lcom/google/android/apps/chrome/Main$InitializerContinuation;

    # invokes: Lcom/google/android/apps/chrome/Main$InitializerContinuation;->onResume()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->access$7800(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V

    const/4 v0, 0x3

    invoke-static {p0, v0}, Lorg/chromium/base/ActivityStatus;->onStateChange(Landroid/app/Activity;I)V

    invoke-static {}, Lcom/google/android/apps/chrome/nfc/BeamController;->onResume()V

    invoke-static {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->onResume(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->overviewVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mAccessibilityOverviewLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->hideOverview(Z)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-static {p1}, Lcom/google/android/apps/chrome/Tab;->saveEncryptionKey(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mChromeWindow:Lcom/google/android/apps/chrome/ChromeWindow;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeWindow;->saveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "is_incognito_selected"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->isIncognito()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "First run is running"

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mIsOnFirstRun:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {}, Lcom/google/android/apps/chrome/nfc/BeamController;->pauseRegistration()V

    return-void
.end method

.method public onTerminate(Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mKillChromeOnDestroy:Z

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/Main;->mRestartChromeOnDestroy:Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->finish()V

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->onTrimMemory(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mMemoryUsageMonitor:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mMemoryUsageMonitor:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->onTrimMemory(I)V

    :cond_0
    return-void
.end method

.method public onUserLeaveHint()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mStartUpSnapshotHandler:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->takeSnapshot()V

    return-void
.end method

.method openUrlInCurrentModel(Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v1

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v5

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->isIncognito()Z

    move-result v6

    move-object v1, p1

    move-object v3, p2

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/TabModel;->createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZ)Lcom/google/android/apps/chrome/Tab;

    return-void
.end method

.method public processUrlViewIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x1

    sget-object v0, Lcom/google/android/apps/chrome/Main$17;->$SwitchMap$com$google$android$apps$chrome$IntentHandler$TabOpenType:[I

    invoke-virtual {p3}, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/Main;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown TabOpenType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/Main;->mUIInitialized:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v1

    invoke-interface {v1, v5}, Lcom/google/android/apps/chrome/OverviewBehavior;->hideOverview(Z)V

    :cond_0
    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/TabModel;->getTabIndexByUrl(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->setIndex(I)V

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->reload()V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->tabClobbered()V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    return-void

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/Main;->launchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_1
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/Main;->launchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    :goto_1
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->tabClobbered()V

    goto :goto_0

    :cond_3
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/Main;->launchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    :pswitch_3
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/Main;->launchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public processWebSearchIntent(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.WEB_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Main;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public setControlTopMargin(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public showPopupMenu(Landroid/view/View;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mUIInitialized:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mMenu:Landroid/view/Menu;

    if-nez v0, :cond_3

    new-instance v0, Landroid/widget/PopupMenu;

    invoke-direct {v0, p0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const v3, 0x7f0e0001

    invoke-virtual {v0, v3}, Landroid/widget/PopupMenu;->inflate(I)V

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main;->mMenu:Landroid/view/Menu;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mMenu:Landroid/view/Menu;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Main;->prepareMenu(Landroid/view/Menu;)V

    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getMenuThemeResourceId()I

    move-result v3

    invoke-direct {v0, p0, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main;->mPopupMenu:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    if-nez v3, :cond_4

    new-instance v3, Lcom/google/android/apps/chrome/PhoneMenuPopup;

    iget-object v4, p0, Lcom/google/android/apps/chrome/Main;->mMenu:Landroid/view/Menu;

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/chrome/PhoneMenuPopup;-><init>(Lcom/google/android/apps/chrome/Main;Landroid/view/Menu;)V

    iput-object v3, p0, Lcom/google/android/apps/chrome/Main;->mPopupMenu:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/chrome/Main;->mPopupMenu:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    invoke-virtual {v3, v0, p1}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->createPopup(Landroid/content/Context;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mPopupMenu:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main;->mPopupMenu:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->dismiss()V

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/chrome/Main;->mPopupMenu:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v4

    iget-boolean v5, p0, Lcom/google/android/apps/chrome/Main;->mIsTablet:Z

    if-nez v5, :cond_6

    move v2, v1

    :cond_6
    invoke-virtual {v3, v0, v4, v2}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->show(ZZZ)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->menuShow()V

    move v2, v1

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public startAccountRequest()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/sync/AccountAdder;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/AccountAdder;-><init>()V

    const/16 v1, 0x66

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/sync/AccountAdder;->addAccount(Landroid/app/Activity;I)V

    return-void
.end method
