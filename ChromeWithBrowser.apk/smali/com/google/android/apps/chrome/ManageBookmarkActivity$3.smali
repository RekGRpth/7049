.class Lcom/google/android/apps/chrome/ManageBookmarkActivity$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

.field final synthetic val$selectFolder:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ManageBookmarkActivity;Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$3;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$3;->val$selectFolder:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$3;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    return-void
.end method

.method public onSelection(JLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$3;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$3;->val$selectFolder:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->setParentFolderInfo(JLjava/lang/String;)V

    return-void
.end method

.method public triggerNewFolderCreation(JLjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$3;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$3;->val$selectFolder:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    # invokes: Lcom/google/android/apps/chrome/ManageBookmarkActivity;->newFolder(Landroid/app/Fragment;JLjava/lang/String;)V
    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->access$200(Lcom/google/android/apps/chrome/ManageBookmarkActivity;Landroid/app/Fragment;JLjava/lang/String;)V

    return-void
.end method
