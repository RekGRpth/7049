.class public Lcom/google/android/apps/chrome/ManageBookmarkActivity;
.super Lcom/google/android/apps/chrome/ChromeBaseActivity;


# static fields
.field public static final ADD_FOLDER_FRAGMENT_TAG:Ljava/lang/String; = "AddFolder"

.field protected static final ADD_FOLDER_SELECT_FOLDER_FRAGMENT_TAG:Ljava/lang/String; = "AddFolderSelectFolder"

.field public static final BASE_ADD_EDIT_FRAGMENT_TAG:Ljava/lang/String; = "AddEdit"

.field public static final BASE_SELECT_FOLDER_FRAGMENT_TAG:Ljava/lang/String; = "SelectFolder"

.field private static final BOOKMARK_ID_URI_PARAM:Ljava/lang/String; = "id"

.field public static final BOOKMARK_INTENT_ID:Ljava/lang/String; = "_id"

.field public static final BOOKMARK_INTENT_IS_FOLDER:Ljava/lang/String; = "folder"

.field public static final BOOKMARK_INTENT_TITLE:Ljava/lang/String; = "title"

.field public static final BOOKMARK_INTENT_URL:Ljava/lang/String; = "url"

.field private static final BOOKMARK_IS_FOLDER_URI_PARAM:Ljava/lang/String; = "isfolder"

.field private static final TAG:Ljava/lang/String; = "ManageBookmarkActivity"


# instance fields
.field private mIsBackEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeBaseActivity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->mIsBackEnabled:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ManageBookmarkActivity;Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->setActionListenerOnFolderSelection(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/ManageBookmarkActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->mIsBackEnabled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ManageBookmarkActivity;Landroid/app/Fragment;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->newFolder(Landroid/app/Fragment;JLjava/lang/String;)V

    return-void
.end method

.method private generateBaseFragment()Lcom/google/android/apps/chrome/AddEditBookmarkFragment;
    .locals 8

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "intent can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    const-string v5, "editbookmark"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v0, "isfolder"

    invoke-virtual {v2, v0, v4}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v3

    const-string v0, "id"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v3, v5, v6}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->newEditInstance(ZJ)Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_1

    invoke-static {v4, v1, v1, v1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->newInstance(ZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    move-result-object v0

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->setActionListenerOnAddEdit(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)V

    return-object v0

    :cond_2
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    if-eqz v5, :cond_6

    const-string v0, "folder"

    invoke-virtual {v5, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    const-string v0, "title"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "title"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const-string v2, "url"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "url"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    const-string v6, "_id"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "_id"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move v7, v3

    move-object v3, v5

    move-object v5, v2

    move v2, v7

    :goto_4
    invoke-static {v2, v3, v0, v5}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->newInstance(ZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v5, v2

    move v2, v3

    move-object v3, v1

    goto :goto_4

    :cond_4
    move-object v2, v1

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto :goto_2

    :cond_6
    move-object v0, v1

    move v2, v4

    move-object v3, v1

    move-object v5, v1

    goto :goto_4

    :cond_7
    move-object v0, v1

    goto :goto_0
.end method

.method private initializeFragmentState()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "AddEdit"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->setActionListenerOnAddEdit(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "SelectFolder"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v1, v2

    check-cast v1, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->setActionListenerOnFolderSelection(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "AddFolder"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v3, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->setActionListenerOnAddEdit(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "AddFolderSelectFolder"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->setActionListenerOnFolderSelection(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)V

    invoke-virtual {v3, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v3, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :goto_0
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v3, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0

    :cond_2
    invoke-virtual {v3, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0
.end method

.method private newFolder(Landroid/app/Fragment;JLjava/lang/String;)V
    .locals 4

    invoke-static {p2, p3, p4}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->newAddNewFolderInstance(JLjava/lang/String;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->setActionListenerOnAddEdit(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0f0085

    const-string v3, "AddFolder"

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    return-void
.end method

.method private setActionListenerOnAddEdit(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)V
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;-><init>(Lcom/google/android/apps/chrome/ManageBookmarkActivity;Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->setOnActionListener(Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;)V

    return-void
.end method

.method private setActionListenerOnFolderSelection(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)V
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/ManageBookmarkActivity$3;-><init>(Lcom/google/android/apps/chrome/ManageBookmarkActivity;Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->setOnActionListener(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->mIsBackEnabled:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeBaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeBaseActivity;->onCreate(Landroid/os/Bundle;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Z)V
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    invoke-static {p0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_1
    const v0, 0x7f040023

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->setContentView(I)V

    if-nez p1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0f0085

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->generateBaseFragment()Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    move-result-object v2

    const-string v3, "AddEdit"

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/ManageBookmarkActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity$1;-><init>(Lcom/google/android/apps/chrome/ManageBookmarkActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessage(Landroid/nfc/NdefMessage;Landroid/app/Activity;[Landroid/app/Activity;)V

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ManageBookmarkActivity"

    const-string v2, "Failed to start browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->finish()V

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->initializeFragmentState()V

    goto :goto_0
.end method

.method protected onUserLeaveHint()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->finish()V

    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeBaseActivity;->onUserLeaveHint()V

    return-void
.end method
