.class Lcom/google/android/apps/chrome/MemoryUsageMonitor$4;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/MemoryUsageMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$4;->this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$4;->this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    # getter for: Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->access$400(Lcom/google/android/apps/chrome/MemoryUsageMonitor;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$4;->this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    # getter for: Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mMaxActiveTabs:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->access$500(Lcom/google/android/apps/chrome/MemoryUsageMonitor;)I

    move-result v0

    if-lt v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$4;->this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    # getter for: Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mDelegate:Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->access$600(Lcom/google/android/apps/chrome/MemoryUsageMonitor;)Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$4;->this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    # getter for: Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->access$400(Lcom/google/android/apps/chrome/MemoryUsageMonitor;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;->saveStateAndDestroyTab(I)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method
