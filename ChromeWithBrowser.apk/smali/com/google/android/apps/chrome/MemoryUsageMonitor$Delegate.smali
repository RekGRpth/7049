.class public interface abstract Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;
.super Ljava/lang/Object;


# virtual methods
.method public abstract clearCachedNtpAndThumbnails()V
.end method

.method public abstract getTabModel(Z)Lcom/google/android/apps/chrome/TabModel;
.end method

.method public abstract registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
.end method

.method public abstract saveStateAndDestroyTab(I)V
.end method

.method public abstract unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
.end method
