.class Lcom/google/android/apps/chrome/MemoryUsageMonitor;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final FREE_AS_MUCH_AS_POSSIBLE:I = 0x7fffffff

.field private static final NOTIFICATIONS:[I

.field private static final TAG:Ljava/lang/String; = "MemoryUsageMonitor"


# instance fields
.field private mDelegate:Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;

.field private final mHandler:Landroid/os/Handler;

.field private final mMaxActiveTabs:I

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mTabs:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->$assertionsDisabled:Z

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->NOTIFICATIONS:[I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :array_0
    .array-data 4
        0x31
        0x30
        0x3
        0x24
    .end array-data
.end method

.method constructor <init>(ILcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor$1;-><init>(Lcom/google/android/apps/chrome/MemoryUsageMonitor;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;

    iput p1, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mMaxActiveTabs:I

    const-string v0, "MemoryUsageMonitor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Max active tabs = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mMaxActiveTabs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p2, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mDelegate:Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mDelegate:Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;

    sget-object v1, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/MemoryUsageMonitor;ILcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->onTabCreating(ILcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/MemoryUsageMonitor;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->onTabSelected(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/MemoryUsageMonitor;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->onTabClosing(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/MemoryUsageMonitor;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->freeMemory(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/MemoryUsageMonitor;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/MemoryUsageMonitor;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mMaxActiveTabs:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/MemoryUsageMonitor;)Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mDelegate:Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;

    return-object v0
.end method

.method private freeMemory(I)V
    .locals 13

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mDelegate:Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;->getTabModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v2

    if-eq v0, v2, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->isSavedAndViewDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mDelegate:Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;->getTabModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v2

    const/4 v0, 0x0

    :goto_1
    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_3

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v3

    if-eq v0, v3, :cond_2

    invoke-interface {v2, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Tab;->isSavedAndViewDestroyed()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {v2, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Tab;->getRenderProcessPid()I

    move-result v0

    move v2, v0

    :goto_2
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getRenderProcessPid()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getRenderProcessPrivateSizeKBytes()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v10, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_5
    const/4 v0, -0x1

    move v2, v0

    goto :goto_2

    :cond_6
    new-instance v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$3;

    invoke-direct {v0, p0, v2, v10}, Lcom/google/android/apps/chrome/MemoryUsageMonitor$3;-><init>(Lcom/google/android/apps/chrome/MemoryUsageMonitor;ILjava/util/HashMap;)V

    invoke-static {v9, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v6, -0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    move v8, v0

    :goto_4
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Tab;->getParentId()I

    move-result v0

    move v3, v0

    :goto_5
    move v12, v1

    move v1, v4

    move v4, v12

    :goto_6
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_b

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getRenderProcessPid()I

    move-result v7

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getParentId()I

    move-result v11

    if-eq v11, v8, :cond_8

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v11

    if-eq v11, v3, :cond_8

    if-eq v7, v6, :cond_f

    const/4 v11, -0x1

    if-eq v6, v11, :cond_7

    add-int/2addr v1, v5

    if-gt v1, p1, :cond_b

    :cond_7
    move v5, v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v6, v7

    :goto_7
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->saveStateAndDestroy()V

    move v12, v5

    move v5, v1

    move v1, v12

    :cond_8
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_6

    :cond_9
    const/4 v0, -0x1

    move v8, v0

    goto :goto_4

    :cond_a
    const/4 v0, -0x1

    move v3, v0

    goto :goto_5

    :cond_b
    if-ge v1, p1, :cond_e

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_8
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_d

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getRenderProcessPid()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->purgeRenderProcessNativeMemory()V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mDelegate:Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;->clearCachedNtpAndThumbnails()V

    :cond_e
    return-void

    :cond_f
    move v12, v1

    move v1, v5

    move v5, v12

    goto :goto_7
.end method

.method private freezeTabsIfNecessary()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/MemoryUsageMonitor$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor$4;-><init>(Lcom/google/android/apps/chrome/MemoryUsageMonitor;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static getDefaultDelegate(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor$2;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V

    return-object v0
.end method

.method static getDefaultMaxActiveTabs(Landroid/content/Context;)I
    .locals 2

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private onTabClosing(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method private onTabCreating(ILcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)V
    .locals 3

    sget-boolean v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    if-nez p3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_RESTORE:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-ne p2, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->freezeTabsIfNecessary()V

    return-void

    :cond_3
    sget-boolean v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private onTabSelected(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mTabs:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->freezeTabsIfNecessary()V

    return-void
.end method


# virtual methods
.method destroy()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mDelegate:Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;

    sget-object v1, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->mDelegate:Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 2

    const v1, 0x7fffffff

    const/16 v0, 0xf

    if-gt p1, v0, :cond_1

    const/16 v0, 0xa

    if-lt p1, v0, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->freeMemory(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->freeMemory(I)V

    goto :goto_0
.end method
