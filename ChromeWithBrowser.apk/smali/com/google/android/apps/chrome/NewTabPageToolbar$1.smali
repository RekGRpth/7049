.class Lcom/google/android/apps/chrome/NewTabPageToolbar$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    # invokes: Lcom/google/android/apps/chrome/NewTabPageToolbar;->sendSyncEnabled()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$000(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/chrome/NewTabPageToolbar;->updateSyncPromoState(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$100(Lcom/google/android/apps/chrome/NewTabPageToolbar;Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    # getter for: Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$200(Lcom/google/android/apps/chrome/NewTabPageToolbar;)Landroid/widget/Toast;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    # getter for: Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$200(Lcom/google/android/apps/chrome/NewTabPageToolbar;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    # getter for: Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$200(Lcom/google/android/apps/chrome/NewTabPageToolbar;)Landroid/widget/Toast;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    # getter for: Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$200(Lcom/google/android/apps/chrome/NewTabPageToolbar;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->urlChanged()V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    # getter for: Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$300(Lcom/google/android/apps/chrome/NewTabPageToolbar;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    # invokes: Lcom/google/android/apps/chrome/NewTabPageToolbar;->onPageLoadFinished()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$400(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    const v1, 0x7f0f0090

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateAccounts()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x3 -> :sswitch_0
        0x9 -> :sswitch_3
        0x1a -> :sswitch_2
        0x23 -> :sswitch_2
        0x32 -> :sswitch_4
    .end sparse-switch
.end method
