.class Lcom/google/android/apps/chrome/NewTabPageToolbar$8;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$8;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/NewTabPageToolbar$8;->doInBackground([Ljava/lang/Void;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$8;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getCurrentLocalDocuments(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/NewTabPageToolbar$8;->onPostExecute(Ljava/util/Set;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/Set;)V
    .locals 7

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "createTime"

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getCreateTime()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v4, "title"

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, " "

    const-string v6, "%20"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "url"

    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    const-string v4, "snapshotId"

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    :goto_1
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    # getter for: Lcom/google/android/apps/chrome/NewTabPageToolbar;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$700()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error JSON encoding document: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getJobId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getSnapshotViewableState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->READY:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    if-ne v4, v5, :cond_0

    const-string v4, "printJobId"

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getJobId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_4
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, v1}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$8;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    # getter for: Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$800(Lcom/google/android/apps/chrome/NewTabPageToolbar;)Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$8;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    # getter for: Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$800(Lcom/google/android/apps/chrome/NewTabPageToolbar;)Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ntp.snapshots("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/chromium/content/browser/ContentView;->evaluateJavaScript(Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_5
    # getter for: Lcom/google/android/apps/chrome/NewTabPageToolbar;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Can\'t finish sendSnapshotDocuments - ContentView was destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
