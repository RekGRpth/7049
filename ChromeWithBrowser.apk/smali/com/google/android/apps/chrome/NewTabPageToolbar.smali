.class Lcom/google/android/apps/chrome/NewTabPageToolbar;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;


# static fields
.field private static final SYNC_PROMO_FADE_DURATION_MS:J = 0x1f4L

.field private static final SYNC_PROMO_HIDE_DELAY_MS:J = 0x64L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final NOTIFICATIONS:[I

.field private mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private mBookmarksButton:Landroid/view/View;

.field private final mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mConfiguration:Landroid/content/res/Configuration;

.field private final mHandler:Landroid/os/Handler;

.field private mHideSyncPromoRunnable:Ljava/lang/Runnable;

.field private mIncognitoButton:Landroid/view/View;

.field private mMostVisitedButton:Landroid/view/View;

.field private mOpenTabsButton:Landroid/view/View;

.field private final mShowTooltips:Z

.field private final mSnapshotStateBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mState:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

.field private mTab:Lcom/google/android/apps/chrome/Tab;

.field private mToast:Landroid/widget/Toast;

.field private final mTooltipCollisionRect:Landroid/graphics/Rect;

.field private mTooltipViews:Ljava/util/List;

.field private mView:Lorg/chromium/content/browser/ContentView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/NewTabPageToolbar;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->NOTIFICATIONS:[I

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mConfiguration:Landroid/content/res/Configuration;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTooltipCollisionRect:Landroid/graphics/Rect;

    new-instance v0, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar$1;-><init>(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    new-instance v0, Lcom/google/android/apps/chrome/NewTabPageToolbar$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar$2;-><init>(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mSnapshotStateBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {p1}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mShowTooltips:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :array_0
    .array-data 4
        0x0
        0x3
        0x1a
        0x9
        0x32
        0x23
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->sendSyncEnabled()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/NewTabPageToolbar;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->updateSyncPromoState(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/NewTabPageToolbar;)Landroid/widget/Toast;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/NewTabPageToolbar;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTab:Lcom/google/android/apps/chrome/Tab;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->onPageLoadFinished()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->sendSnapshotDocuments()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/NewTabPageToolbar;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/NewTabPageToolbar;)Lorg/chromium/content/browser/ContentView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;

    return-object v0
.end method

.method private childViewContainsPoint(Landroid/view/View;II)Z
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTooltipCollisionRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTooltipCollisionRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTooltipCollisionRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p2, p3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method private static createToolbar(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/ChromeActivity;Z)Lcom/google/android/apps/chrome/NewTabPageToolbar;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040027

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/NewTabPageToolbar;

    if-eqz p2, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->setVisibility(I)V

    :cond_0
    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->init(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/ChromeActivity;)V

    return-object v0
.end method

.method private eventIsForToolbar(Landroid/view/MotionEvent;)Z
    .locals 3

    const v0, 0x7f0f0091

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->childViewContainsPoint(Landroid/view/View;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static findOrCreateIfNeeded(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/ChromeActivity;Ljava/lang/String;)Lcom/google/android/apps/chrome/NewTabPageToolbar;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findToolbar(Lorg/chromium/content/browser/ContentView;)Lcom/google/android/apps/chrome/NewTabPageToolbar;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    if-eqz p2, :cond_1

    const-string v0, "chrome://newtab/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/ShortcutActivity;->BOOKMARK_SHORTCUT_URL:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->createToolbar(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/ChromeActivity;Z)Lcom/google/android/apps/chrome/NewTabPageToolbar;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static findToolbar(Lorg/chromium/content/browser/ContentView;)Lcom/google/android/apps/chrome/NewTabPageToolbar;
    .locals 5

    const/4 v1, 0x0

    if-nez p0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentView;->getChildCount()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, Lorg/chromium/content/browser/ContentView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v4, v0, Lcom/google/android/apps/chrome/NewTabPageToolbar;

    if-eqz v4, :cond_1

    check-cast v0, Lcom/google/android/apps/chrome/NewTabPageToolbar;

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private init(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTab:Lcom/google/android/apps/chrome/Tab;

    iput-object p2, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mShowTooltips:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTooltipViews:Ljava/util/List;

    :cond_0
    const v0, 0x7f0f0092

    const v1, 0x7f02009a

    const v2, 0x7f070021

    const v3, 0x7f070210

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->initButton(IIII)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mIncognitoButton:Landroid/view/View;

    const v0, 0x7f0f0093

    const v1, 0x7f020097

    const v2, 0x7f07001e

    const v3, 0x7f070211

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->initButton(IIII)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mMostVisitedButton:Landroid/view/View;

    const v0, 0x7f0f0094

    const v1, 0x7f020012

    const v2, 0x7f07001d

    const v3, 0x7f070212

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->initButton(IIII)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mBookmarksButton:Landroid/view/View;

    const v0, 0x7f0f0095

    const v1, 0x7f0200a5

    const v2, 0x7f07001f

    const v3, 0x7f070213

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->initButton(IIII)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mOpenTabsButton:Landroid/view/View;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mState:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->updateIconStates()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->sendSnapshotDocuments()V

    return-void
.end method

.method private initButton(IIII)Landroid/view/View;
    .locals 3

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0f0096

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v2, v0, Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    const v0, 0x7f0f0098

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v2, v0, Landroid/widget/TextView;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mShowTooltips:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTooltipViews:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    const v0, 0x7f02009d

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-object v1
.end method

.method private isSignedIn()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSignedIn()Z

    move-result v0

    return v0
.end method

.method private isSyncEnabled()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    return v0
.end method

.method private loadNtpSection(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;

    const-string v1, "ntp.openSection(\'%s\')"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->getHashtag()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->evaluateJavaScript(Ljava/lang/String;)V

    return-void
.end method

.method private onPageLoadFinished()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->sendSyncEnabled()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->sendSnapshotDocuments()V

    return-void
.end method

.method private sendSnapshotDocuments()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/NewTabPageToolbar$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar$8;-><init>(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar$8;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private sendSyncEnabled()V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;

    const-string v1, "ntp.setSyncEnabled(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->isSyncEnabled()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->evaluateJavaScript(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->TAG:Ljava/lang/String;

    const-string v1, "Can\'t send syncEnabled - ContentView was destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setState(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mState:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mState:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, p0}, Lorg/chromium/content/browser/ContentView;->removeView(Landroid/view/View;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->updateIconStates()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->updateSyncPromoState(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, p0}, Lorg/chromium/content/browser/ContentView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, p0}, Lorg/chromium/content/browser/ContentView;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ntp.setIncognitoMode("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->evaluateJavaScript(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showTooltip(Landroid/view/View;I)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    const/16 v1, 0x51

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/Toast;->setGravity(III)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private updateIconState(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mState:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    if-ne v0, p1, :cond_0

    const v0, 0x7f02009d

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mState:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p2, v0}, Landroid/view/View;->setSelected(Z)V

    return-void

    :cond_0
    const v0, 0x7f02009b

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private updateIconStates()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->INCOGNITO:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mIncognitoButton:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->updateIconState(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;Landroid/view/View;)V

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->MOST_VISITED:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mMostVisitedButton:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->updateIconState(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;Landroid/view/View;)V

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARKS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mBookmarksButton:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->updateIconState(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;Landroid/view/View;)V

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->OPEN_TABS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mOpenTabsButton:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->updateIconState(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;Landroid/view/View;)V

    return-void
.end method

.method private updateSyncPromoState(Z)V
    .locals 12

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mHideSyncPromoRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mHideSyncPromoRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    const v0, 0x7f0f008e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mState:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    sget-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->OPEN_TABS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->isSignedIn()Z

    move-result v0

    if-nez v0, :cond_2

    const v0, 0x7f0f008f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    if-nez v0, :cond_1

    const v0, 0x7f0f0090

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    check-cast v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setCanCancel(Z)V

    new-instance v1, Lcom/google/android/apps/chrome/NewTabPageToolbar$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar$3;-><init>(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setListener(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    new-instance v11, Lcom/google/android/apps/chrome/NewTabPageToolbar$4;

    invoke-direct {v11, p0, v2}, Lcom/google/android/apps/chrome/NewTabPageToolbar$4;-><init>(Lcom/google/android/apps/chrome/NewTabPageToolbar;Landroid/view/View;)V

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getLinearInterpolator()Landroid/view/animation/LinearInterpolator;

    move-result-object v9

    new-instance v0, Lcom/google/android/apps/chrome/NewTabPageToolbar$5;

    const/4 v4, 0x0

    const-wide/16 v5, 0x1f4

    const-wide/16 v7, 0x0

    move-object v1, p0

    move-object v10, v2

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/chrome/NewTabPageToolbar$5;-><init>(Lcom/google/android/apps/chrome/NewTabPageToolbar;Landroid/view/View;FFJJLandroid/view/animation/Interpolator;Landroid/view/View;)V

    invoke-virtual {v11, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    new-instance v0, Lcom/google/android/apps/chrome/NewTabPageToolbar$6;

    invoke-direct {v0, p0, v11}, Lcom/google/android/apps/chrome/NewTabPageToolbar$6;-><init>(Lcom/google/android/apps/chrome/NewTabPageToolbar;Lcom/google/android/apps/chrome/ChromeAnimation;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mHideSyncPromoRunnable:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mHideSyncPromoRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/google/android/apps/chrome/NewTabPageToolbar$7;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/chrome/NewTabPageToolbar$7;-><init>(Lcom/google/android/apps/chrome/NewTabPageToolbar;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mHideSyncPromoRunnable:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mHideSyncPromoRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method


# virtual methods
.method public onActivityResume()V
    .locals 1

    const v0, 0x7f0f0090

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateAccounts()V

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTab:Lcom/google/android/apps/chrome/Tab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->onPageLoadFinished()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mSnapshotStateBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.apps.chrome.snapshot.ACTION_SNAPSHOT_STATE_UPDATE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->addSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mBookmarksButton:Landroid/view/View;

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARKS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->loadNtpSection(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->ntpSectionBookmarks()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mMostVisitedButton:Landroid/view/View;

    if-ne p1, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->MOST_VISITED:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->loadNtpSection(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->ntpSectionMostVisited()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mOpenTabsButton:Landroid/view/View;

    if-ne p1, v0, :cond_3

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->OPEN_TABS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->loadNtpSection(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->ntpSectionOpenTabs()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mIncognitoButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->INCOGNITO:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->loadNtpSection(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->ntpSectionIncognito()V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mConfiguration:Landroid/content/res/Configuration;

    invoke-virtual {v0, p1}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    move-result v0

    and-int/lit16 v0, v0, 0x80

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, p0}, Lorg/chromium/content/browser/ContentView;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTab:Lcom/google/android/apps/chrome/Tab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentView;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findOrCreateIfNeeded(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/ChromeActivity;Ljava/lang/String;)Lcom/google/android/apps/chrome/NewTabPageToolbar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->urlChanged()V

    goto :goto_0
.end method

.method public onCrash()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->setState(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mSnapshotStateBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->removeSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    new-instance v0, Landroid/content/res/Configuration;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mConfiguration:Landroid/content/res/Configuration;

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 4

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mShowTooltips:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTooltipViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-ne v2, p1, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/view/View;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->showTooltip(Landroid/view/View;I)V

    :cond_1
    return v3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->eventIsForToolbar(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public syncStateChanged()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->sendSyncEnabled()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->updateSyncPromoState(Z)V

    return-void
.end method

.method public urlChanged()V
    .locals 7

    const v6, 0x7f0f0097

    const/4 v1, 0x0

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getUrl()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v0, "#enablesync"

    invoke-virtual {v3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->goBack()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStates;->create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->build()Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mIncognitoButton:Landroid/view/View;

    if-eqz v4, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mMostVisitedButton:Landroid/view/View;

    if-eqz v4, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mOpenTabsButton:Landroid/view/View;

    if-eqz v4, :cond_3

    move v0, v2

    :goto_3
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mOpenTabsButton:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar;->mBookmarksButton:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v4, :cond_4

    :goto_4
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4, v3}, Lcom/google/android/apps/chrome/NewTabPageUtil;->setSelectedNtpSection(Landroid/content/Context;ZLjava/lang/String;)Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->setState(Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    move v2, v1

    goto :goto_4
.end method
