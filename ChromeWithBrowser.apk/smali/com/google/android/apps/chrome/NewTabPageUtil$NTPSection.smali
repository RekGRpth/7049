.class public final enum Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

.field public static final enum BOOKMARKS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

.field public static final enum BOOKMARK_SHORTCUT:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

.field public static final enum INCOGNITO:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

.field public static final enum MOST_VISITED:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

.field public static final enum OPEN_TABS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;


# instance fields
.field private mHashtag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    const-string v1, "BOOKMARKS"

    const-string v2, "bookmarks"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARKS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    new-instance v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    const-string v1, "MOST_VISITED"

    const-string v2, "most_visited"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->MOST_VISITED:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    new-instance v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    const-string v1, "OPEN_TABS"

    const-string v2, "open_tabs"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->OPEN_TABS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    new-instance v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    const-string v1, "INCOGNITO"

    const-string v2, "incognito"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->INCOGNITO:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    new-instance v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    const-string v1, "BOOKMARK_SHORTCUT"

    const-string v2, "bookmark_shortcut"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARK_SHORTCUT:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    sget-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARKS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->MOST_VISITED:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->OPEN_TABS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->INCOGNITO:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARK_SHORTCUT:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->$VALUES:[Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->mHashtag:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->$VALUES:[Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    return-object v0
.end method


# virtual methods
.method public final getHashtag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->mHashtag:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->getHashtag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
