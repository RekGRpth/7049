.class public Lcom/google/android/apps/chrome/NewTabPageUtil;
.super Ljava/lang/Object;


# static fields
.field private static final INCOGNITO_SECTIONS:Ljava/util/Set;

.field private static final NORMAL_SECTIONS:Ljava/util/Set;

.field private static final STICKY_SECTIONS:Ljava/util/Set;

.field private static final TAG:Ljava/lang/String;

.field private static final TAGS_TO_SECTIONS:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    const-class v1, Lcom/google/android/apps/chrome/NewTabPageUtil;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil;->TAG:Ljava/lang/String;

    new-array v1, v6, [Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    sget-object v2, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->MOST_VISITED:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v2, v1, v0

    sget-object v2, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARKS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->OPEN_TABS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARK_SHORTCUT:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v2, v1, v5

    invoke-static {v1}, Lcom/google/android/apps/chrome/utilities/CollectionsUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil;->NORMAL_SECTIONS:Ljava/util/Set;

    new-array v1, v4, [Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    sget-object v2, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->INCOGNITO:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v2, v1, v0

    sget-object v2, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARKS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/apps/chrome/utilities/CollectionsUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil;->INCOGNITO_SECTIONS:Ljava/util/Set;

    new-array v1, v6, [Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    sget-object v2, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->MOST_VISITED:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v2, v1, v0

    sget-object v2, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARKS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->OPEN_TABS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->INCOGNITO:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    aput-object v2, v1, v5

    invoke-static {v1}, Lcom/google/android/apps/chrome/utilities/CollectionsUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil;->STICKY_SECTIONS:Ljava/util/Set;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil;->TAGS_TO_SECTIONS:Ljava/util/Map;

    invoke-static {}, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->values()[Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    sget-object v4, Lcom/google/android/apps/chrome/NewTabPageUtil;->TAGS_TO_SECTIONS:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->getHashtag()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static appendNtpSectionIfNeeded(Landroid/content/Context;ZLjava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {p2}, Lcom/google/android/apps/chrome/NewTabPageUtil;->isNewTabPage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    invoke-static {p2, p1}, Lcom/google/android/apps/chrome/NewTabPageUtil;->getSectionFromUrl(Ljava/lang/String;Z)Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/chrome/NewTabPageUtil;->getDefaultSectionPropertyKey(Z)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/NewTabPageUtil;->getSectionFromTag(Ljava/lang/String;Z)Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    move-result-object v0

    if-nez v0, :cond_2

    if-eqz p1, :cond_3

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->INCOGNITO:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    :cond_2
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->getHashtag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->MOST_VISITED:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    goto :goto_1
.end method

.method private static getDefaultSectionPropertyKey(Z)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ntp_section_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getSectionFromTag(Ljava/lang/String;Z)Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil;->TAGS_TO_SECTIONS:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    if-eqz p1, :cond_1

    sget-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil;->INCOGNITO_SECTIONS:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil;->NORMAL_SECTIONS:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getSectionFromUrl(Ljava/lang/String;Z)Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;
    .locals 4

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/google/android/apps/chrome/NewTabPageUtil;->isNewTabPage(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/16 v1, 0x23

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    if-gez v1, :cond_1

    sget-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to determine NTP section: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARKS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":[\\d]+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARKS:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    goto :goto_0

    :cond_2
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/NewTabPageUtil;->getSectionFromTag(Ljava/lang/String;Z)Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    move-result-object v0

    goto :goto_0
.end method

.method public static isNewTabPage(Ljava/lang/String;)Z
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-string v0, "chrome://newtab/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static setSelectedNtpSection(Landroid/content/Context;ZLjava/lang/String;)Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;
    .locals 4

    invoke-static {p2}, Lcom/google/android/apps/chrome/NewTabPageUtil;->isNewTabPage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "#cached_ntp"

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->MOST_VISITED:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p2, p1}, Lcom/google/android/apps/chrome/NewTabPageUtil;->getSectionFromUrl(Ljava/lang/String;Z)Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil;->STICKY_SECTIONS:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/chrome/NewTabPageUtil;->getDefaultSectionPropertyKey(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->getHashtag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method
