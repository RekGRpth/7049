.class Lcom/google/android/apps/chrome/OmniboxPopupAdapter;
.super Landroid/widget/BaseAdapter;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

.field private mSelectionHandler:Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;

.field private final mSuggestionItems:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/LocationBar;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    iput-object p3, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->mSuggestionItems:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-eqz p2, :cond_0

    instance-of v0, p2, Lcom/google/android/apps/chrome/SuggestionView;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/android/apps/chrome/SuggestionView;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    iget-object v2, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->mSelectionHandler:Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p2, v0, v2, v1}, Lcom/google/android/apps/chrome/SuggestionView;->init(Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;Z)V

    return-object p2

    :cond_0
    new-instance p2, Lcom/google/android/apps/chrome/SuggestionView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-direct {p2, v0, v1}, Lcom/google/android/apps/chrome/SuggestionView;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/LocationBar;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public notifySuggestionsChanged()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setSuggestionSelectionHandler(Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter;->mSelectionHandler:Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;

    return-void
.end method
