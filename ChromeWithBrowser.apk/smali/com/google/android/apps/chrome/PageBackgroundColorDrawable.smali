.class Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;
.super Landroid/graphics/drawable/Drawable;


# instance fields
.field mBackgroundPaint:Landroid/graphics/Paint;

.field mRect:Landroid/graphics/Rect;

.field mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 1

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->mBackgroundPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->mRect:Landroid/graphics/Rect;

    iput-object p1, p0, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-void
.end method

.method private subtractChildViewRegions(Landroid/graphics/Region;Landroid/view/View;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-wide/16 v5, 0x0

    invoke-virtual {p2}, Landroid/view/View;->getRotationX()F

    move-result v0

    float-to-double v3, v0

    cmpl-double v0, v3, v5

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getRotationY()F

    move-result v0

    float-to-double v3, v0

    cmpl-double v0, v3, v5

    if-eqz v0, :cond_2

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    return v1

    :cond_2
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    check-cast p2, Landroid/view/ViewGroup;

    move v0, v1

    :goto_1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->subtractChildViewRegions(Landroid/graphics/Region;Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->mRect:Landroid/graphics/Rect;

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->mRect:Landroid/graphics/Rect;

    sget-object v2, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Region;->op(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    new-instance v1, Landroid/graphics/Region;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/graphics/Region;-><init>(Landroid/graphics/Rect;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->subtractChildViewRegions(Landroid/graphics/Region;Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->mBackgroundPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/Region;->set(Landroid/graphics/Rect;)Z

    :cond_0
    :goto_0
    new-instance v0, Landroid/graphics/RegionIterator;

    invoke-direct {v0, v1}, Landroid/graphics/RegionIterator;-><init>(Landroid/graphics/Region;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RegionIterator;->next(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->mRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getBackgroundColor()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method
