.class Lcom/google/android/apps/chrome/PhoneMenuPopup$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/PhoneMenuPopup;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5

    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    # getter for: Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->access$000(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Landroid/widget/ListPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    # getter for: Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->access$000(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Landroid/widget/ListPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    # getter for: Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->access$000(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Landroid/widget/ListPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-static {p3}, Lcom/google/android/apps/chrome/KeyNavigationUtil;->isGoUp(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    # getter for: Lcom/google/android/apps/chrome/PhoneMenuPopup;->mShowIconRow:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->access$100(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    if-gt v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    # getter for: Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->access$000(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Landroid/widget/ListPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->clearListSelection()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    # invokes: Lcom/google/android/apps/chrome/PhoneMenuPopup;->requestFocusToEnabledIconRowButton()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->access$200(Lcom/google/android/apps/chrome/PhoneMenuPopup;)V

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    # getter for: Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->access$000(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Landroid/widget/ListPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/widget/ListPopupWindow;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    if-ne v1, v4, :cond_1

    invoke-virtual {v3, v2}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_4
    invoke-static {p3}, Lcom/google/android/apps/chrome/KeyNavigationUtil;->isGoDown(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    # getter for: Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->access$000(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Landroid/widget/ListPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/widget/ListPopupWindow;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    if-ne v1, v4, :cond_1

    invoke-virtual {v3}, Landroid/widget/ListView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_5
    invoke-static {p3}, Lcom/google/android/apps/chrome/KeyNavigationUtil;->isEnter(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    # getter for: Lcom/google/android/apps/chrome/PhoneMenuPopup;->mShowIconRow:Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->access$100(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Z

    move-result v2

    if-eqz v2, :cond_6

    add-int/lit8 v0, v0, -0x1

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    # getter for: Lcom/google/android/apps/chrome/PhoneMenuPopup;->mAdapter:Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;
    invoke-static {v2}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->access$300(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->dismiss()V

    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    # getter for: Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMainActivity:Lcom/google/android/apps/chrome/Main;
    invoke-static {v2}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->access$400(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Lcom/google/android/apps/chrome/Main;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/Main;->onOptionsItemSelected(I)Z

    move v0, v1

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v3, 0x52

    if-ne v0, v3, :cond_a

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p3}, Landroid/view/KeyEvent;->startTracking()V

    invoke-virtual {p1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v0

    invoke-virtual {v0, p3, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    move v0, v1

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_9

    invoke-virtual {p1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;->this$0:Lcom/google/android/apps/chrome/PhoneMenuPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->dismiss()V

    move v0, v1

    goto/16 :goto_0

    :cond_9
    move v0, v2

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto/16 :goto_0
.end method
