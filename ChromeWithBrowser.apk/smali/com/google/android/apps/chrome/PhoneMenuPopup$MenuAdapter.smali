.class Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;
.super Landroid/widget/BaseAdapter;


# static fields
.field private static final VIEW_TYPE_COUNT:I = 0x1

.field private static final VIEW_TYPE_MENUITEM:I


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mMenuItems:Ljava/util/List;

.field private final mNumMenuItems:I


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/view/LayoutInflater;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;->mMenuItems:Ljava/util/List;

    iput-object p2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;->mNumMenuItems:I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;->mNumMenuItems:I

    return v0
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;->mMenuItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p2, :cond_0

    new-instance v1, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter$ViewHolder;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter$ViewHolder;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f040025

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v0, 0x7f0f0089

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter$ViewHolder;->text:Landroid/widget/TextView;

    const v0, 0x7f0f008a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuItemIcon;

    iput-object v0, v1, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter$ViewHolder;->image:Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuItemIcon;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v3, v0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter$ViewHolder;->image:Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuItemIcon;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuItemIcon;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, v0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter$ViewHolder;->image:Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuItemIcon;

    if-nez v1, :cond_1

    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuItemIcon;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter$ViewHolder;->image:Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuItemIcon;

    invoke-interface {v2}, Landroid/view/MenuItem;->isChecked()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuItemIcon;->setChecked(Z)V

    iget-object v1, v0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter$ViewHolder;->text:Landroid/widget/TextView;

    invoke-interface {v2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-interface {v2}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v1

    iget-object v0, v0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter$ViewHolder;->text:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setEnabled(Z)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter$ViewHolder;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
