.class public Lcom/google/android/apps/chrome/PhoneMenuPopup;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final LAST_ITEM_HIDE_FRACTION:F = 0.15f


# instance fields
.field private mAdapter:Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;

.field private final mAdditionalVerticalOffset:I

.field private mBookmarkButton:Landroid/widget/ImageButton;

.field private mIconRowView:Landroid/view/View;

.field private mInflater:Landroid/view/LayoutInflater;

.field private final mItemRowHeight:I

.field private final mMainActivity:Lcom/google/android/apps/chrome/Main;

.field private final mMenu:Landroid/view/Menu;

.field private mPopup:Landroid/widget/ListPopupWindow;

.field private mShowIconRow:Z

.field private final mVerticalFadeDistance:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/PhoneMenuPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/Main;Landroid/view/Menu;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mShowIconRow:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mBookmarkButton:Landroid/widget/ImageButton;

    iput-object p1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMainActivity:Lcom/google/android/apps/chrome/Main;

    iput-object p2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMenu:Landroid/view/Menu;

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMainActivity:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x1010387

    aput v2, v1, v3

    invoke-virtual {p1, v1}, Lcom/google/android/apps/chrome/Main;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mItemRowHeight:I

    sget-boolean v2, Lcom/google/android/apps/chrome/PhoneMenuPopup;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mItemRowHeight:I

    if-gtz v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    const v1, 0x7f080074

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mAdditionalVerticalOffset:I

    const v1, 0x7f080075

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mVerticalFadeDistance:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Landroid/widget/ListPopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mShowIconRow:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/PhoneMenuPopup;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->requestFocusToEnabledIconRowButton()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mAdapter:Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/PhoneMenuPopup;)Lcom/google/android/apps/chrome/Main;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMainActivity:Lcom/google/android/apps/chrome/Main;

    return-object v0
.end method

.method private handleIconRowClickEvents(Landroid/view/View;Landroid/view/Menu;Lcom/google/android/apps/chrome/Main;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f00fb

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f00fc

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMenu:Landroid/view/Menu;

    const v3, 0x7f0f00fd

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/chrome/PhoneMenuPopup$4;

    invoke-direct {v3, p0}, Lcom/google/android/apps/chrome/PhoneMenuPopup$4;-><init>(Lcom/google/android/apps/chrome/PhoneMenuPopup;)V

    const v4, 0x7f0f0086

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-interface {v0}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setEnabled(Z)V

    invoke-interface {v0}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {v4, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v5, Lcom/google/android/apps/chrome/PhoneMenuPopup$5;

    invoke-direct {v5, p0, p3, v0}, Lcom/google/android/apps/chrome/PhoneMenuPopup$5;-><init>(Lcom/google/android/apps/chrome/PhoneMenuPopup;Lcom/google/android/apps/chrome/Main;Landroid/view/MenuItem;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0087

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-interface {v1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    invoke-interface {v1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v4, Lcom/google/android/apps/chrome/PhoneMenuPopup$6;

    invoke-direct {v4, p0, p3, v1}, Lcom/google/android/apps/chrome/PhoneMenuPopup$6;-><init>(Lcom/google/android/apps/chrome/PhoneMenuPopup;Lcom/google/android/apps/chrome/Main;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0088

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mBookmarkButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mBookmarkButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mBookmarkButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/PhoneMenuPopup$7;

    invoke-direct {v1, p0, p3, v2}, Lcom/google/android/apps/chrome/PhoneMenuPopup$7;-><init>(Lcom/google/android/apps/chrome/PhoneMenuPopup;Lcom/google/android/apps/chrome/Main;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private populateHeaderViewInfo(Landroid/content/Context;Landroid/view/View;)Ljava/util/ArrayList;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Landroid/widget/ListView;

    invoke-direct {v1, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/ListView$FixedViewInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v2, v1}, Landroid/widget/ListView$FixedViewInfo;-><init>(Landroid/widget/ListView;)V

    iput-object p2, v2, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    const/4 v1, 0x0

    iput-boolean v1, v2, Landroid/widget/ListView$FixedViewInfo;->isSelectable:Z

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private requestFocusToEnabledIconRowButton()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mIconRowView:Landroid/view/View;

    const v1, 0x7f0f0086

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mIconRowView:Landroid/view/View;

    const v2, 0x7f0f0087

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mIconRowView:Landroid/view/View;

    const v3, 0x7f0f0088

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/View;->isFocusable()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    goto :goto_0
.end method

.method private setMenuHeight(ZILandroid/graphics/Rect;)V
    .locals 5

    const/4 v4, 0x1

    if-nez p1, :cond_1

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    aget v2, v1, v4

    iget v3, p3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    aput v2, v1, v4

    aget v2, v1, v4

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v3

    aget v1, v1, v4

    sub-int v1, v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int v0, v1, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mItemRowHeight:I

    div-int v1, v0, v1

    if-ge v1, p2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    iget v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mItemRowHeight:I

    int-to-float v2, v2

    const v3, 0x3e19999a

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sub-int/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setHeight(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setHeight(I)V

    goto :goto_0
.end method

.method private updateBookmarkButton()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f00fd

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mBookmarkButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mBookmarkButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200b3

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mBookmarkButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mBookmarkButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070201

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mBookmarkButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200b4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mBookmarkButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mBookmarkButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070202

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public createPopup(Landroid/content/Context;Landroid/view/View;)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/PhoneMenuPopup$1;

    const/4 v1, 0x0

    const v2, 0x1010300

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/google/android/apps/chrome/PhoneMenuPopup$1;-><init>(Lcom/google/android/apps/chrome/PhoneMenuPopup;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mInflater:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p2}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setInputMethodMode(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    new-instance v1, Lcom/google/android/apps/chrome/PhoneMenuPopup$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/PhoneMenuPopup$2;-><init>(Lcom/google/android/apps/chrome/PhoneMenuPopup;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080073

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    return-void
.end method

.method public dismiss()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    :cond_0
    return-void
.end method

.method public getContentView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    return-object v0
.end method

.method public isShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mShowIconRow:Z

    if-eqz v0, :cond_0

    add-int/lit8 p3, p3, -0x1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mAdapter:Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->dismiss()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMainActivity:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/Main;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    :cond_1
    return-void
.end method

.method public show(ZZZ)V
    .locals 8

    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMenu:Landroid/view/Menu;

    invoke-interface {v2}, Landroid/view/Menu;->size()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMenu:Landroid/view/Menu;

    invoke-interface {v5, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mShowIconRow:Z

    new-instance v2, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;

    iget-object v3, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mInflater:Landroid/view/LayoutInflater;

    invoke-direct {v2, v4, v3}, Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;-><init>(Ljava/util/List;Landroid/view/LayoutInflater;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mAdapter:Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mShowIconRow:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f040024

    invoke-virtual {v2, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mIconRowView:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mIconRowView:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMenu:Landroid/view/Menu;

    iget-object v5, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMainActivity:Lcom/google/android/apps/chrome/Main;

    invoke-direct {p0, v2, v3, v5}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->handleIconRowClickEvents(Landroid/view/View;Landroid/view/Menu;Lcom/google/android/apps/chrome/Main;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->updateBookmarkButton()V

    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMainActivity:Lcom/google/android/apps/chrome/Main;

    iget-object v3, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mIconRowView:Landroid/view/View;

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->populateHeaderViewInfo(Landroid/content/Context;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Landroid/widget/HeaderViewListAdapter;

    iget-object v5, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mAdapter:Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;

    invoke-direct {v3, v2, v7, v5}, Landroid/widget/HeaderViewListAdapter;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    :goto_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mMainActivity:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Main;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mShowIconRow:Z

    if-eqz v4, :cond_2

    move v0, v1

    :cond_2
    add-int/2addr v0, v3

    invoke-direct {p0, p2, v0, v2}, Lcom/google/android/apps/chrome/PhoneMenuPopup;->setMenuHeight(ZILandroid/graphics/Rect;)V

    if-eqz p3, :cond_3

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v3}, Landroid/widget/ListPopupWindow;->getWidth()I

    move-result v3

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    iget-object v3, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v3, v0}, Landroid/widget/ListPopupWindow;->setHorizontalOffset(I)V

    :cond_3
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v3}, Landroid/widget/ListPopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    const/4 v3, 0x2

    new-array v3, v3, [I

    iget-object v4, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->getLocationInWindow([I)V

    aget v3, v3, v1

    iget-object v4, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-ge v3, v2, :cond_7

    iget v0, v0, Landroid/graphics/Rect;->top:I

    neg-int v0, v0

    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    iget v3, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mAdditionalVerticalOffset:I

    add-int/2addr v0, v3

    invoke-virtual {v2, v0}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p0}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    iget v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mVerticalFadeDistance:I

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVerticalFadingEdgeEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mVerticalFadeDistance:I

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFadingEdgeLength(I)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/PhoneMenuPopup$3;-><init>(Lcom/google/android/apps/chrome/PhoneMenuPopup;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/ImageButton;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    :cond_5
    return-void

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mPopup:Landroid/widget/ListPopupWindow;

    iget-object v3, p0, Lcom/google/android/apps/chrome/PhoneMenuPopup;->mAdapter:Lcom/google/android/apps/chrome/PhoneMenuPopup$MenuAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_1

    :cond_7
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    neg-int v0, v0

    goto :goto_2
.end method
