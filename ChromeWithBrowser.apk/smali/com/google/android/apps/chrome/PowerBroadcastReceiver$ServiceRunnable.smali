.class Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final DELAY_TO_POST_MS:J = 0x1388L


# instance fields
.field private final mApplicationContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;->mApplicationContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public delayToRun()J
    .locals 2

    const-wide/16 v0, 0x1388

    return-wide v0
.end method

.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;->resumeDelayedSyncs(Landroid/content/Context;)Z

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeActivity;->isOfficialBuild()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method
