.class public final Lcom/google/android/apps/chrome/R$color;
.super Ljava/lang/Object;


# static fields
.field public static final bg_tabstrip_incognito:I = 0x7f0a002a

.field public static final bg_tabstrip_normal:I = 0x7f0a0029

.field public static final find_result_bar_active_border_color:I = 0x7f0a0013

.field public static final find_result_bar_active_color:I = 0x7f0a0012

.field public static final find_result_bar_background_border_color:I = 0x7f0a000f

.field public static final find_result_bar_background_color:I = 0x7f0a000e

.field public static final find_result_bar_no_results_background_color:I = 0x7f0a0014

.field public static final find_result_bar_result_border_color:I = 0x7f0a0011

.field public static final find_result_bar_result_color:I = 0x7f0a0010

.field public static final fre_background_color:I = 0x7f0a001e

.field public static final fre_light_text_color_tablet:I = 0x7f0a0024

.field public static final fre_negative_button_color:I = 0x7f0a0022

.field public static final fre_negative_button_color_tablet:I = 0x7f0a0025

.field public static final fre_positive_button_color:I = 0x7f0a0021

.field public static final fre_text_color:I = 0x7f0a0020

.field public static final fre_text_color_tablet:I = 0x7f0a0023

.field public static final fre_thin_line_color:I = 0x7f0a001f

.field public static final infobar_background_separator:I = 0x7f0a0016

.field public static final infobar_text:I = 0x7f0a0015

.field public static final js_modal_dialog_title_separator:I = 0x7f0a0000

.field public static final light_background_color:I = 0x7f0a0026

.field public static final locationbar_default_text:I = 0x7f0a0001

.field public static final locationbar_domain_and_registry:I = 0x7f0a0008

.field public static final locationbar_scheme_to_domain:I = 0x7f0a0007

.field public static final locationbar_start_scheme_default:I = 0x7f0a0002

.field public static final locationbar_start_scheme_ev_secure:I = 0x7f0a0005

.field public static final locationbar_start_scheme_secure:I = 0x7f0a0006

.field public static final locationbar_start_scheme_security_error:I = 0x7f0a0004

.field public static final locationbar_start_scheme_security_warning:I = 0x7f0a0003

.field public static final locationbar_trailing_url:I = 0x7f0a0009

.field public static final omnibox_divider:I = 0x7f0a000a

.field public static final preference_header_text_color:I = 0x7f0a002b

.field public static final tab_back:I = 0x7f0a001c

.field public static final tab_back_incognito:I = 0x7f0a001d

.field public static final tab_strip_bottom_stroke:I = 0x7f0a0028

.field public static final tab_strip_fade_mode_overlay:I = 0x7f0a0027

.field public static final tab_switcher_background:I = 0x7f0a0017

.field public static final tab_title_bar_shadow:I = 0x7f0a001a

.field public static final tab_title_bar_shadow_incognito:I = 0x7f0a001b

.field public static final tab_title_bar_text:I = 0x7f0a0018

.field public static final tab_title_bar_text_incognito:I = 0x7f0a0019

.field public static final toolbar_incognito_tab_count_color:I = 0x7f0a000c

.field public static final toolbar_switcher_tab_count_color:I = 0x7f0a000d

.field public static final toolbar_tab_count_color:I = 0x7f0a000b


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
