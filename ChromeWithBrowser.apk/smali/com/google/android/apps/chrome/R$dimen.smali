.class public final Lcom/google/android/apps/chrome/R$dimen;
.super Ljava/lang/Object;


# static fields
.field public static final action_bar_switch_padding:I = 0x7f080092

.field public static final bookmark_dialog_height:I = 0x7f08009d

.field public static final bookmark_dialog_padding:I = 0x7f08009e

.field public static final border_close_button_margin_bottom:I = 0x7f080051

.field public static final border_close_button_margin_left:I = 0x7f08004e

.field public static final border_close_button_margin_right:I = 0x7f080050

.field public static final border_close_button_margin_top:I = 0x7f08004f

.field public static final border_frame_a_bottom:I = 0x7f080045

.field public static final border_frame_a_left:I = 0x7f080042

.field public static final border_frame_a_right:I = 0x7f080044

.field public static final border_frame_a_top:I = 0x7f080043

.field public static final border_frame_b_bottom:I = 0x7f080049

.field public static final border_frame_b_left:I = 0x7f080046

.field public static final border_frame_b_right:I = 0x7f080048

.field public static final border_frame_b_top:I = 0x7f080047

.field public static final border_frame_margin_bottom:I = 0x7f08004d

.field public static final border_frame_margin_left:I = 0x7f08004a

.field public static final border_frame_margin_right:I = 0x7f08004c

.field public static final border_frame_margin_top:I = 0x7f08004b

.field public static final border_min_content_size_to_be_visible:I = 0x7f080052

.field public static final border_texture_a_x:I = 0x7f08001d

.field public static final border_texture_a_y:I = 0x7f08001e

.field public static final border_texture_b_x:I = 0x7f08001f

.field public static final border_texture_b_y:I = 0x7f080020

.field public static final border_texture_c_x:I = 0x7f080021

.field public static final border_texture_c_y:I = 0x7f080022

.field public static final border_texture_content_bottom:I = 0x7f080028

.field public static final border_texture_content_left:I = 0x7f080025

.field public static final border_texture_content_right:I = 0x7f080027

.field public static final border_texture_content_top:I = 0x7f080026

.field public static final border_texture_d_x:I = 0x7f080023

.field public static final border_texture_d_y:I = 0x7f080024

.field public static final border_texture_thickness_bottom:I = 0x7f080031

.field public static final border_texture_thickness_left:I = 0x7f08002e

.field public static final border_texture_thickness_right:I = 0x7f080030

.field public static final border_texture_thickness_top:I = 0x7f08002f

.field public static final border_texture_title_bottom:I = 0x7f08002c

.field public static final border_texture_title_fade:I = 0x7f08002d

.field public static final border_texture_title_left:I = 0x7f080029

.field public static final border_texture_title_right:I = 0x7f08002b

.field public static final border_texture_title_top:I = 0x7f08002a

.field public static final bubble_anim_dist:I = 0x7f080087

.field public static final bubble_tip_margin:I = 0x7f08008a

.field public static final bubble_width:I = 0x7f080084

.field public static final certificate_viewer_padding:I = 0x7f08000a

.field public static final close_button_slop:I = 0x7f08000e

.field public static final control_container_height:I = 0x7f08000c

.field public static final even_out_scrolling:I = 0x7f080014

.field public static final favicon_colorstrip_corner_radii:I = 0x7f080005

.field public static final favicon_colorstrip_height:I = 0x7f080003

.field public static final favicon_colorstrip_padding:I = 0x7f080004

.field public static final favicon_colorstrip_width:I = 0x7f080002

.field public static final favicon_fold_border:I = 0x7f080008

.field public static final favicon_fold_corner_radii:I = 0x7f080007

.field public static final favicon_fold_shadow:I = 0x7f080009

.field public static final favicon_fold_size:I = 0x7f080006

.field public static final favicon_size:I = 0x7f080001

.field public static final find_in_page_buffer:I = 0x7f08009c

.field public static final find_in_page_height:I = 0x7f08005e

.field public static final find_in_page_right_margin:I = 0x7f08005f

.field public static final find_in_page_separator_width:I = 0x7f08005d

.field public static final find_in_page_text_size:I = 0x7f08005c

.field public static final find_in_page_width:I = 0x7f080060

.field public static final find_result_bar_active_min_height:I = 0x7f080058

.field public static final find_result_bar_draw_width:I = 0x7f080056

.field public static final find_result_bar_min_gap_between_stacks:I = 0x7f08005a

.field public static final find_result_bar_result_min_height:I = 0x7f080057

.field public static final find_result_bar_stacked_result_height:I = 0x7f08005b

.field public static final find_result_bar_touch_width:I = 0x7f080055

.field public static final find_result_bar_vertical_padding:I = 0x7f080059

.field public static final fre_button_padding:I = 0x7f08007c

.field public static final fre_button_text_size:I = 0x7f08007d

.field public static final fre_button_text_size_tablet:I = 0x7f08007e

.field public static final fre_large_text_size:I = 0x7f08007f

.field public static final fre_large_text_size_tablet:I = 0x7f080080

.field public static final fre_margin:I = 0x7f080079

.field public static final fre_margin_tablet:I = 0x7f08007a

.field public static final fre_normal_text_size:I = 0x7f080081

.field public static final fre_normal_text_size_tablet:I = 0x7f080082

.field public static final fre_small_margin_tablet:I = 0x7f08007b

.field public static final fre_tablet_window_size:I = 0x7f080083

.field public static final gutter_distance:I = 0x7f080019

.field public static final infobar_button_padding:I = 0x7f080091

.field public static final infobar_button_text_size:I = 0x7f08008e

.field public static final infobar_min_height:I = 0x7f08008b

.field public static final infobar_padding:I = 0x7f08008f

.field public static final infobar_subtext_size:I = 0x7f08008d

.field public static final infobar_text_size:I = 0x7f08008c

.field public static final infobar_text_vertical_margin:I = 0x7f080090

.field public static final link_preview_overlay_radius:I = 0x7f080000

.field public static final location_bar_icon_width:I = 0x7f080071

.field public static final location_bar_url_text_size:I = 0x7f08006f

.field public static final max_tilt_scroll_speed:I = 0x7f080017

.field public static final menu_item_divider_width:I = 0x7f080072

.field public static final menu_vertical_fade_distance:I = 0x7f080075

.field public static final menu_vertical_offset:I = 0x7f080074

.field public static final menu_width:I = 0x7f080073

.field public static final min_spacing:I = 0x7f080012

.field public static final omnibox_suggestion_height:I = 0x7f080070

.field public static final over_scroll:I = 0x7f080013

.field public static final over_scroll_slide:I = 0x7f08001c

.field public static final preference_header_left_padding:I = 0x7f080093

.field public static final preference_icon_minWidth:I = 0x7f080096

.field public static final preference_item_padding_inner:I = 0x7f080095

.field public static final preference_item_padding_side:I = 0x7f080094

.field public static final search_bubble_margin:I = 0x7f080088

.field public static final select_bookmark_folder_item_inc_left:I = 0x7f080054

.field public static final select_bookmark_folder_item_left:I = 0x7f080053

.field public static final side_swipe_accumulate_threshold:I = 0x7f08001a

.field public static final side_swipe_constant:I = 0x7f08001b

.field public static final side_switcher_switch_width:I = 0x7f08009f

.field public static final stack_buffer_height:I = 0x7f080010

.field public static final stack_buffer_width:I = 0x7f080011

.field public static final stacked_tab_visible_size:I = 0x7f08000f

.field public static final swipe_commit_distance:I = 0x7f080016

.field public static final swipe_region:I = 0x7f080018

.field public static final swipe_space_between_tabs:I = 0x7f080015

.field public static final tab_landscape_shadow_margin_bottom:I = 0x7f080041

.field public static final tab_landscape_shadow_margin_left:I = 0x7f08003e

.field public static final tab_landscape_shadow_margin_right:I = 0x7f080040

.field public static final tab_landscape_shadow_margin_top:I = 0x7f08003f

.field public static final tab_portrait_shadow_margin_bottom:I = 0x7f08003d

.field public static final tab_portrait_shadow_margin_left:I = 0x7f08003a

.field public static final tab_portrait_shadow_margin_right:I = 0x7f08003c

.field public static final tab_portrait_shadow_margin_top:I = 0x7f08003b

.field public static final tab_shadow_radius:I = 0x7f080039

.field public static final tab_strip_and_toolbar_height:I = 0x7f08000d

.field public static final tab_strip_height:I = 0x7f08000b

.field public static final tab_title_bar_shadow_x_offset:I = 0x7f080032

.field public static final tab_title_bar_shadow_x_offset_incognito:I = 0x7f080034

.field public static final tab_title_bar_shadow_y_offset:I = 0x7f080033

.field public static final tab_title_bar_shadow_y_offset_incognito:I = 0x7f080035

.field public static final tab_title_favicon_size:I = 0x7f080038

.field public static final tab_title_text_left_padding:I = 0x7f080037

.field public static final tab_title_text_size:I = 0x7f080036

.field public static final tabs_bubble_margin:I = 0x7f080089

.field public static final toolbar_buttons_left_padding:I = 0x7f08006a

.field public static final toolbar_height:I = 0x7f080068

.field public static final toolbar_height_no_shadow:I = 0x7f080067

.field public static final toolbar_new_tab_image_right_padding:I = 0x7f08006d

.field public static final toolbar_new_tab_text_left_padding:I = 0x7f08006e

.field public static final toolbar_shadow_height:I = 0x7f080069

.field public static final toolbar_tab_count_layout_size:I = 0x7f080061

.field public static final toolbar_tab_count_left:I = 0x7f080062

.field public static final toolbar_tab_count_text_size_1_digit:I = 0x7f080065

.field public static final toolbar_tab_count_text_size_2_digit:I = 0x7f080066

.field public static final toolbar_tab_count_top_1_digit:I = 0x7f080063

.field public static final toolbar_tab_count_top_2_digit:I = 0x7f080064

.field public static final toolbar_url_expanded_padding:I = 0x7f08006b

.field public static final toolbar_url_with_security_x_translation:I = 0x7f08006c

.field public static final tooltip_border_width:I = 0x7f080078

.field public static final tooltip_min_edge_margin:I = 0x7f080076

.field public static final tooltip_top_margin:I = 0x7f080077

.field public static final welcome_page_padding:I = 0x7f080085

.field public static final welcome_page_ver_padding:I = 0x7f080086

.field public static final widget_column_width:I = 0x7f08009a

.field public static final widget_favicon_size:I = 0x7f08009b

.field public static final widget_horizontal_spacing:I = 0x7f080098

.field public static final widget_thumbnail_height:I = 0x7f080097

.field public static final widget_vertical_spacing:I = 0x7f080099


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
