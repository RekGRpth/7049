.class public final Lcom/google/android/apps/chrome/R$drawable;
.super Ljava/lang/Object;


# static fields
.field public static final back:I = 0x7f020000

.field public static final back_button_selector:I = 0x7f020001

.field public static final back_disabled:I = 0x7f020002

.field public static final bg_bookmarks_widget_holo:I = 0x7f020003

.field public static final bg_findinpage_popup:I = 0x7f020004

.field public static final bg_tabstrip_background_tab:I = 0x7f020005

.field public static final bg_tabstrip_fader_transition:I = 0x7f020006

.field public static final bg_tabstrip_fader_with_stroke_transition:I = 0x7f020007

.field public static final bg_tabstrip_incognito:I = 0x7f020008

.field public static final bg_tabstrip_tab:I = 0x7f020009

.field public static final bg_tabstrip_with_stroke_faded:I = 0x7f02000a

.field public static final bg_tabstrip_with_stroke_normal:I = 0x7f02000b

.field public static final bg_toolbar_tablet:I = 0x7f02000c

.field public static final blocked_popups:I = 0x7f02000d

.field public static final bookmark_star_lit_selector:I = 0x7f02000e

.field public static final bookmark_star_selector:I = 0x7f02000f

.field public static final bookmark_widget_preview:I = 0x7f020010

.field public static final bookmark_widget_thumb_selector:I = 0x7f020011

.field public static final bookmarks_icon:I = 0x7f020012

.field public static final bookmarks_widget_thumb_selector_focused:I = 0x7f020013

.field public static final bookmarks_widget_thumb_selector_longpressed:I = 0x7f020014

.field public static final bookmarks_widget_thumb_selector_pressed:I = 0x7f020015

.field public static final border_fade:I = 0x7f020016

.field public static final border_fade_incognito:I = 0x7f020017

.field public static final border_frame:I = 0x7f020018

.field public static final border_frame_incognito:I = 0x7f020019

.field public static final border_incognito:I = 0x7f02001a

.field public static final border_shadow:I = 0x7f02001b

.field public static final border_standard:I = 0x7f02001c

.field public static final border_tab:I = 0x7f02001d

.field public static final border_tab_incognito:I = 0x7f02001e

.field public static final border_thumb_bookmarks_widget_holo:I = 0x7f02001f

.field public static final browser_thumbnail:I = 0x7f020020

.field public static final btn_omnibox_bookmark_normal:I = 0x7f020021

.field public static final btn_omnibox_bookmark_pressed:I = 0x7f020022

.field public static final btn_omnibox_bookmark_selected_normal:I = 0x7f020023

.field public static final btn_omnibox_bookmark_selected_pressed:I = 0x7f020024

.field public static final btn_omnibox_reload:I = 0x7f020025

.field public static final btn_omnibox_reload_normal:I = 0x7f020026

.field public static final btn_omnibox_reload_pressed:I = 0x7f020027

.field public static final btn_omnibox_stop_loading:I = 0x7f020028

.field public static final btn_omnibox_stop_loading_normal:I = 0x7f020029

.field public static final btn_omnibox_stop_loading_pressed:I = 0x7f02002a

.field public static final btn_suggestion_refine:I = 0x7f02002b

.field public static final btn_suggestion_search_page:I = 0x7f02002c

.field public static final btn_tabstrip_new_tab:I = 0x7f02002d

.field public static final btn_tabstrip_new_tab_normal:I = 0x7f02002e

.field public static final btn_tabstrip_new_tab_pressed:I = 0x7f02002f

.field public static final btn_tabstrip_switch_incognito:I = 0x7f020030

.field public static final btn_tabstrip_switch_normal:I = 0x7f020031

.field public static final btn_tabstrip_tab_close:I = 0x7f020032

.field public static final btn_tabstrip_tab_close_normal:I = 0x7f020033

.field public static final btn_tabstrip_tab_close_pressed:I = 0x7f020034

.field public static final btn_toolbar_reload:I = 0x7f020035

.field public static final btn_toolbar_reload_disabled:I = 0x7f020036

.field public static final btn_toolbar_reload_normal:I = 0x7f020037

.field public static final btn_toolbar_stop_loading:I = 0x7f020038

.field public static final btn_toolbar_stop_loading_disabled:I = 0x7f020039

.field public static final btn_toolbar_stop_loading_normal:I = 0x7f02003a

.field public static final bubble_blue:I = 0x7f02003b

.field public static final bubble_green:I = 0x7f02003c

.field public static final bubble_point_blue:I = 0x7f02003d

.field public static final bubble_point_green:I = 0x7f02003e

.field public static final cardstack:I = 0x7f02003f

.field public static final cardstack_disabled:I = 0x7f020040

.field public static final cardstack_disabled_light:I = 0x7f020041

.field public static final cardstack_incognito:I = 0x7f020042

.field public static final cardstack_light:I = 0x7f020043

.field public static final cardstack_normal:I = 0x7f020044

.field public static final cardstack_switcher:I = 0x7f020045

.field public static final close:I = 0x7f020046

.field public static final close_find_normal:I = 0x7f020047

.field public static final close_find_normal_incognito:I = 0x7f020048

.field public static final close_tab:I = 0x7f020049

.field public static final close_tab_incognito:I = 0x7f02004a

.field public static final control_content_fade:I = 0x7f02004b

.field public static final delete:I = 0x7f02004c

.field public static final delete_pressed:I = 0x7f02004d

.field public static final delete_url_selector:I = 0x7f02004e

.field public static final downloading:I = 0x7f02004f

.field public static final empty_screen_layered_list:I = 0x7f020050

.field public static final find_next:I = 0x7f020051

.field public static final find_next_disabled:I = 0x7f020052

.field public static final find_next_disabled_incognito:I = 0x7f020053

.field public static final find_next_incognito:I = 0x7f020054

.field public static final find_next_normal:I = 0x7f020055

.field public static final find_next_normal_incognito:I = 0x7f020056

.field public static final find_prev:I = 0x7f020057

.field public static final find_prev_disabled:I = 0x7f020058

.field public static final find_prev_disabled_incognito:I = 0x7f020059

.field public static final find_prev_incognito:I = 0x7f02005a

.field public static final find_prev_normal:I = 0x7f02005b

.field public static final find_prev_normal_incognito:I = 0x7f02005c

.field public static final folder_icon:I = 0x7f02005d

.field public static final font_preview_background:I = 0x7f02005e

.field public static final forward:I = 0x7f02005f

.field public static final forward_button_selector:I = 0x7f020060

.field public static final forward_disabled:I = 0x7f020061

.field public static final fre_blue_btn:I = 0x7f020062

.field public static final fre_blue_btn_pressed:I = 0x7f020063

.field public static final fre_blue_button:I = 0x7f020064

.field public static final fre_checkmark:I = 0x7f020065

.field public static final fre_dropdown_background:I = 0x7f020066

.field public static final fre_dropdown_background_pressed:I = 0x7f020067

.field public static final fre_dropdown_button:I = 0x7f020068

.field public static final fre_grey_btn:I = 0x7f020069

.field public static final fre_grey_btn_pressed:I = 0x7f02006a

.field public static final fre_grey_button:I = 0x7f02006b

.field public static final fre_progress_unit:I = 0x7f02006c

.field public static final fre_progress_unit_background:I = 0x7f02006d

.field public static final fre_progress_unit_checked:I = 0x7f02006e

.field public static final fre_spinner_background_normal:I = 0x7f02006f

.field public static final fre_spinner_background_pressed:I = 0x7f020070

.field public static final fre_spinner_selector:I = 0x7f020071

.field public static final fre_syncfographic:I = 0x7f020072

.field public static final fre_tablet_background:I = 0x7f020073

.field public static final fre_topbar_logo:I = 0x7f020074

.field public static final globe_favicon:I = 0x7f020075

.field public static final globe_incognito_favicon:I = 0x7f020076

.field public static final ic_bookmark_widget_bookmark_holo_dark:I = 0x7f020077

.field public static final ic_list_data_empty:I = 0x7f020078

.field public static final ic_list_data_large:I = 0x7f020079

.field public static final ic_list_data_small:I = 0x7f02007a

.field public static final ic_location_allowed:I = 0x7f02007b

.field public static final ic_location_denied:I = 0x7f02007c

.field public static final ic_menu_search_holo_light:I = 0x7f02007d

.field public static final ic_menu_share_holo_light:I = 0x7f02007e

.field public static final ic_stat_notify:I = 0x7f02007f

.field public static final ic_suggestion_bookmark:I = 0x7f020080

.field public static final ic_suggestion_globe:I = 0x7f020081

.field public static final ic_suggestion_history:I = 0x7f020082

.field public static final ic_suggestion_magnifier:I = 0x7f020083

.field public static final ic_suggestion_voice:I = 0x7f020084

.field public static final ic_tabstrip_incognito_guy:I = 0x7f020085

.field public static final infobar_autologin:I = 0x7f020086

.field public static final infobar_button_bg:I = 0x7f020087

.field public static final infobar_button_enabled:I = 0x7f020088

.field public static final infobar_button_pressed:I = 0x7f020089

.field public static final infobar_button_text:I = 0x7f02008a

.field public static final infobar_dismiss:I = 0x7f02008b

.field public static final infobar_info_background:I = 0x7f02008c

.field public static final infobar_update_uma:I = 0x7f02008d

.field public static final infobar_warning_background:I = 0x7f02008e

.field public static final location_bar_background:I = 0x7f02008f

.field public static final logo_card_back:I = 0x7f020090

.field public static final logo_etched:I = 0x7f020091

.field public static final menu_btn_bg:I = 0x7f020092

.field public static final menu_btn_bg_light:I = 0x7f020093

.field public static final mic:I = 0x7f020094

.field public static final mic_pressed:I = 0x7f020095

.field public static final mic_selector:I = 0x7f020096

.field public static final most_visited_icon:I = 0x7f020097

.field public static final new_tab_light:I = 0x7f020098

.field public static final new_tab_logo:I = 0x7f020099

.field public static final ntp_button_incognito:I = 0x7f02009a

.field public static final ntp_toolbar_button_background:I = 0x7f02009b

.field public static final ntp_toolbar_button_background_pressed_img:I = 0x7f02009c

.field public static final ntp_toolbar_button_background_selected:I = 0x7f02009d

.field public static final ntp_toolbar_button_background_selected_img:I = 0x7f02009e

.field public static final ntp_toolbar_button_background_selected_pressed_img:I = 0x7f02009f

.field public static final ntp_toolbar_button_divider:I = 0x7f0200a0

.field public static final omnibox_https_invalid:I = 0x7f0200a1

.field public static final omnibox_https_valid:I = 0x7f0200a2

.field public static final omnibox_https_warning:I = 0x7f0200a3

.field public static final ondemand_overlay:I = 0x7f0200a4

.field public static final open_tabs_icon:I = 0x7f0200a5

.field public static final overlay_url_bookmark_widget_holo:I = 0x7f0200a6

.field public static final sad_tab:I = 0x7f0200a7

.field public static final sad_tab_lower_panel_background:I = 0x7f0200a8

.field public static final sad_tab_panel_background:I = 0x7f0200a9

.field public static final sad_tab_panel_drop_shadow:I = 0x7f0200aa

.field public static final sad_tab_reload_button:I = 0x7f0200ab

.field public static final sad_tab_reload_button_normal:I = 0x7f0200ac

.field public static final sad_tab_reload_button_pressed:I = 0x7f0200ad

.field public static final sad_tab_upper_panel_background:I = 0x7f0200ae

.field public static final sadfavicon:I = 0x7f0200af

.field public static final snapshot_prefix_background:I = 0x7f0200b0

.field public static final spinner_16_inner_holo:I = 0x7f0200b1

.field public static final spinner_16_outer_holo:I = 0x7f0200b2

.field public static final star:I = 0x7f0200b3

.field public static final star_lit:I = 0x7f0200b4

.field public static final tabstrip_background_tab_text_truncator:I = 0x7f0200b5

.field public static final tabstrip_foreground_tab_text_truncator:I = 0x7f0200b6

.field public static final textbox:I = 0x7f0200b7

.field public static final throbber_animation:I = 0x7f0200b8

.field public static final thumb_bookmark_widget_folder_back_holo:I = 0x7f0200b9

.field public static final thumb_bookmark_widget_folder_holo:I = 0x7f0200ba

.field public static final thumbnail_bookmarks_widget_no_bookmark_holo:I = 0x7f0200bb

.field public static final toolbar:I = 0x7f0200bc

.field public static final toolbar_background:I = 0x7f0200bd

.field public static final toolbar_incognito:I = 0x7f0200be

.field public static final toolbar_shadow:I = 0x7f0200bf

.field public static final toolbar_switcher:I = 0x7f0200c0

.field public static final warning:I = 0x7f0200c1

.field public static final wb_location_bar_center:I = 0x7f0200c2

.field public static final wb_location_bar_left:I = 0x7f0200c3

.field public static final wb_location_bar_right:I = 0x7f0200c4

.field public static final wb_tab:I = 0x7f0200c5

.field public static final wb_tabstrip_stroke_1x1_color:I = 0x7f0200c6

.field public static final website_location:I = 0x7f0200c7

.field public static final website_popups:I = 0x7f0200c8

.field public static final website_storage_usage:I = 0x7f0200c9

.field public static final window_background:I = 0x7f0200ca


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
