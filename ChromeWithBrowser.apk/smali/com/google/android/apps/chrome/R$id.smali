.class public final Lcom/google/android/apps/chrome/R$id;
.super Ljava/lang/Object;


# static fields
.field public static final ANCHOR_MENU:I = 0x7f0f00e6

.field public static final DEFAULT_MENU:I = 0x7f0f0104

.field public static final DEVELOPERS_TOOLS_MENU:I = 0x7f0f0114

.field public static final DO_NOT_TRACK_MENU:I = 0x7f0f0116

.field public static final IMAGE_MENU:I = 0x7f0f00ec

.field public static final MAIN_MENU:I = 0x7f0f0118

.field public static final OK:I = 0x7f0f000a

.field public static final PAGE_MENU:I = 0x7f0f00f0

.field public static final PHONE_ICON_MENU_ITEMS:I = 0x7f0f00fa

.field public static final PHONE_OVERVIEW_MODE_MENU:I = 0x7f0f0100

.field public static final PRIVACY_MENU:I = 0x7f0f010c

.field public static final SYNC_ACCOUNT_MENU:I = 0x7f0f010f

.field public static final SYNC_MENU:I = 0x7f0f0112

.field public static final TABLET_EMPTY_MODE_MENU:I = 0x7f0f0103

.field public static final TOP_LEVEL_MENU:I = 0x7f0f0106

.field public static final accessibility:I = 0x7f0f00e3

.field public static final action_bar_black_background:I = 0x7f0f007a

.field public static final all_bookmarks_menu_id:I = 0x7f0f00f3

.field public static final allow_block_spinner:I = 0x7f0f00dd

.field public static final app_shortcut:I = 0x7f0f002c

.field public static final autofill_credit_card_cancel:I = 0x7f0f001a

.field public static final autofill_credit_card_delete:I = 0x7f0f0019

.field public static final autofill_credit_card_editor_month_spinner:I = 0x7f0f0017

.field public static final autofill_credit_card_editor_name_edit:I = 0x7f0f0015

.field public static final autofill_credit_card_editor_number_edit:I = 0x7f0f0016

.field public static final autofill_credit_card_editor_year_spinner:I = 0x7f0f0018

.field public static final autofill_credit_card_save:I = 0x7f0f001b

.field public static final autofill_label:I = 0x7f0f002a

.field public static final autofill_name:I = 0x7f0f0029

.field public static final autofill_profile_cancel:I = 0x7f0f0027

.field public static final autofill_profile_delete:I = 0x7f0f0026

.field public static final autofill_profile_editor_address_line_1_edit:I = 0x7f0f001e

.field public static final autofill_profile_editor_address_line_2_edit:I = 0x7f0f001f

.field public static final autofill_profile_editor_city_edit:I = 0x7f0f0020

.field public static final autofill_profile_editor_company_name_edit:I = 0x7f0f001d

.field public static final autofill_profile_editor_country_edit:I = 0x7f0f0023

.field public static final autofill_profile_editor_email_address_edit:I = 0x7f0f0025

.field public static final autofill_profile_editor_name_edit:I = 0x7f0f001c

.field public static final autofill_profile_editor_phone_number_edit:I = 0x7f0f0024

.field public static final autofill_profile_editor_state_edit:I = 0x7f0f0021

.field public static final autofill_profile_editor_zip_code_edit:I = 0x7f0f0022

.field public static final autofill_profile_save:I = 0x7f0f0028

.field public static final autofill_settings:I = 0x7f0f00d8

.field public static final back_button:I = 0x7f0f00cc

.field public static final back_menu_id:I = 0x7f0f00fb

.field public static final bandwidth:I = 0x7f0f00e4

.field public static final bookmark_action_title:I = 0x7f0f0005

.field public static final bookmark_button:I = 0x7f0f0075

.field public static final bookmark_fields:I = 0x7f0f000c

.field public static final bookmark_folder_input_row:I = 0x7f0f0012

.field public static final bookmark_folder_label:I = 0x7f0f0013

.field public static final bookmark_folder_list:I = 0x7f0f00a5

.field public static final bookmark_folder_select:I = 0x7f0f0014

.field public static final bookmark_this_page_id:I = 0x7f0f00fd

.field public static final bookmark_title_input:I = 0x7f0f000e

.field public static final bookmark_title_label:I = 0x7f0f000d

.field public static final bookmark_url_input:I = 0x7f0f0011

.field public static final bookmark_url_input_row:I = 0x7f0f000f

.field public static final bookmark_url_label:I = 0x7f0f0010

.field public static final bookmarks_button:I = 0x7f0f0094

.field public static final bookmarks_list:I = 0x7f0f002b

.field public static final bottom_stroke:I = 0x7f0f00bf

.field public static final bugfeature_id:I = 0x7f0f00a8

.field public static final button_cancel:I = 0x7f0f0063

.field public static final button_container:I = 0x7f0f0007

.field public static final button_divider:I = 0x7f0f000b

.field public static final button_ok:I = 0x7f0f0064

.field public static final cancel:I = 0x7f0f0008

.field public static final cancel_feedback_button:I = 0x7f0f00b0

.field public static final choose_folder_title:I = 0x7f0f00a3

.field public static final chrome_info:I = 0x7f0f0042

.field public static final close_all_incognito_tabs_menu_id:I = 0x7f0f0102

.field public static final close_all_tabs_menu_id:I = 0x7f0f0101

.field public static final close_btn:I = 0x7f0f0003

.field public static final close_button:I = 0x7f0f0067

.field public static final close_find_button:I = 0x7f0f003c

.field public static final close_tab_menu_id:I = 0x7f0f00ff

.field public static final compositor_view_holder:I = 0x7f0f0031

.field public static final compositor_view_stub:I = 0x7f0f0078

.field public static final confirm_passphrase:I = 0x7f0f00b2

.field public static final content:I = 0x7f0f008e

.field public static final contentPanel:I = 0x7f0f00b6

.field public static final content_container:I = 0x7f0f0076

.field public static final content_overlay:I = 0x7f0f0032

.field public static final content_overlay_stub:I = 0x7f0f0079

.field public static final control_container:I = 0x7f0f007b

.field public static final copy_link_address_context_menu_id:I = 0x7f0f00e9

.field public static final copy_link_text_context_menu_id:I = 0x7f0f00ea

.field public static final date_picker:I = 0x7f0f0033

.field public static final delete_button:I = 0x7f0f0072

.field public static final delete_exception_button:I = 0x7f0f00df

.field public static final description_edit_id:I = 0x7f0f00ae

.field public static final description_title_id:I = 0x7f0f00ad

.field public static final divider:I = 0x7f0f0097

.field public static final do_not_track:I = 0x7f0f00e5

.field public static final do_not_track_description:I = 0x7f0f0035

.field public static final email_text_id:I = 0x7f0f00aa

.field public static final email_title_id:I = 0x7f0f00a9

.field public static final empty_container:I = 0x7f0f0036

.field public static final empty_container_stub:I = 0x7f0f0083

.field public static final empty_folders:I = 0x7f0f00a6

.field public static final empty_incognito_toggle_button:I = 0x7f0f0039

.field public static final empty_menu_button:I = 0x7f0f003a

.field public static final empty_new_tab_button:I = 0x7f0f0038

.field public static final empty_toolbar_background:I = 0x7f0f0037

.field public static final explanation:I = 0x7f0f005a

.field public static final fader_view:I = 0x7f0f00be

.field public static final favicon:I = 0x7f0f002f

.field public static final features:I = 0x7f0f00d0

.field public static final find_in_page_id:I = 0x7f0f00f6

.field public static final find_next_button:I = 0x7f0f0040

.field public static final find_prev_button:I = 0x7f0f003f

.field public static final find_query:I = 0x7f0f003d

.field public static final find_status:I = 0x7f0f003e

.field public static final find_toolbar:I = 0x7f0f003b

.field public static final find_toolbar_stub:I = 0x7f0f0081

.field public static final find_toolbar_tablet_stub:I = 0x7f0f0082

.field public static final forward_button:I = 0x7f0f00cd

.field public static final forward_menu_id:I = 0x7f0f00fc

.field public static final fragment_container:I = 0x7f0f0085

.field public static final fre_account_layout:I = 0x7f0f0041

.field public static final fre_buttons:I = 0x7f0f0044

.field public static final fre_container:I = 0x7f0f004f

.field public static final fre_notification_bar:I = 0x7f0f0049

.field public static final fre_progress_indicator:I = 0x7f0f004d

.field public static final fre_signed_text:I = 0x7f0f0055

.field public static final fre_signing_text:I = 0x7f0f0056

.field public static final fre_spinner_text:I = 0x7f0f0057

.field public static final fre_syncinfograph:I = 0x7f0f0048

.field public static final fre_tablet_layout:I = 0x7f0f0050

.field public static final fre_tablet_line1:I = 0x7f0f0051

.field public static final fre_tablet_line2:I = 0x7f0f0053

.field public static final fre_tablet_panel:I = 0x7f0f0047

.field public static final fre_tos_text:I = 0x7f0f0054

.field public static final google_accounts_spinner:I = 0x7f0f0043

.field public static final google_icon:I = 0x7f0f004a

.field public static final header_summary:I = 0x7f0f00e1

.field public static final header_switch:I = 0x7f0f00e2

.field public static final header_title:I = 0x7f0f00e0

.field public static final help_id:I = 0x7f0f00f9

.field public static final icon:I = 0x7f0f0066

.field public static final image:I = 0x7f0f0096

.field public static final incognito_button:I = 0x7f0f0092

.field public static final incognito_button_stub:I = 0x7f0f00c6

.field public static final incognito_button_wrapper:I = 0x7f0f00c5

.field public static final incognito_indicator:I = 0x7f0f00c1

.field public static final incognito_indicator_stub:I = 0x7f0f00c0

.field public static final incognito_list_view:I = 0x7f0f0001

.field public static final incognito_toggle_button:I = 0x7f0f00c7

.field public static final infobar_button:I = 0x7f0f005f

.field public static final infobar_content:I = 0x7f0f0065

.field public static final infobar_message:I = 0x7f0f0061

.field public static final infobar_spacer:I = 0x7f0f0060

.field public static final infobar_sub_message:I = 0x7f0f0062

.field public static final js_modal_dialog_prompt:I = 0x7f0f0068

.field public static final label:I = 0x7f0f0030

.field public static final list:I = 0x7f0f00b4

.field public static final list_item:I = 0x7f0f002d

.field public static final loading_icon:I = 0x7f0f004c

.field public static final location_bar:I = 0x7f0f00c8

.field public static final location_bar_icon:I = 0x7f0f006a

.field public static final location_icon:I = 0x7f0f00d1

.field public static final manage_saved_passwords:I = 0x7f0f00d9

.field public static final menu_anchor_stub:I = 0x7f0f0084

.field public static final menu_button:I = 0x7f0f0074

.field public static final menu_id_about_chrome:I = 0x7f0f010a

.field public static final menu_id_clear_browsing_data:I = 0x7f0f010d

.field public static final menu_id_disconnect_sync_account:I = 0x7f0f0110

.field public static final menu_id_do_not_track_learn:I = 0x7f0f0117

.field public static final menu_id_help_default:I = 0x7f0f0105

.field public static final menu_id_help_developers_tools:I = 0x7f0f0115

.field public static final menu_id_help_privacy:I = 0x7f0f010e

.field public static final menu_id_help_settings:I = 0x7f0f0109

.field public static final menu_id_help_sync_account:I = 0x7f0f0111

.field public static final menu_id_import_bookmarks:I = 0x7f0f010b

.field public static final menu_id_reset_sync:I = 0x7f0f0113

.field public static final menu_id_send_feedback:I = 0x7f0f0108

.field public static final menu_id_sign_in_to_chrome:I = 0x7f0f0107

.field public static final menu_item_back:I = 0x7f0f0086

.field public static final menu_item_bookmark:I = 0x7f0f0088

.field public static final menu_item_forward:I = 0x7f0f0087

.field public static final menu_item_icon:I = 0x7f0f008a

.field public static final menu_item_text:I = 0x7f0f0089

.field public static final message:I = 0x7f0f00b3

.field public static final mic_button:I = 0x7f0f0073

.field public static final month:I = 0x7f0f008c

.field public static final most_visited_button:I = 0x7f0f0093

.field public static final name:I = 0x7f0f00a7

.field public static final navigation_button:I = 0x7f0f006b

.field public static final new_folder_btn:I = 0x7f0f00a4

.field public static final new_incognito_tab_menu_id:I = 0x7f0f00f2

.field public static final new_tab_button:I = 0x7f0f00bd

.field public static final new_tab_menu_id:I = 0x7f0f00f1

.field public static final next_tab_menu_id:I = 0x7f0f011a

.field public static final normal_list_view:I = 0x7f0f0000

.field public static final notification_icon:I = 0x7f0f004b

.field public static final ntp_buttons_container:I = 0x7f0f0091

.field public static final open_image_context_menu_id:I = 0x7f0f00ee

.field public static final open_image_in_new_tab_context_menu_id:I = 0x7f0f00ef

.field public static final open_in_incognito_tab_context_menu_id:I = 0x7f0f00e8

.field public static final open_in_new_tab_context_menu_id:I = 0x7f0f00e7

.field public static final open_menu_id:I = 0x7f0f0119

.field public static final open_tabs_button:I = 0x7f0f0095

.field public static final open_tabs_menu_id:I = 0x7f0f00f4

.field public static final options_button:I = 0x7f0f006f

.field public static final options_layout:I = 0x7f0f00ca

.field public static final options_tab_count_label:I = 0x7f0f00cb

.field public static final parentPanel:I = 0x7f0f00b5

.field public static final passphrase:I = 0x7f0f00b1

.field public static final password:I = 0x7f0f005e

.field public static final password_label:I = 0x7f0f005d

.field public static final password_pref_editor_cancel:I = 0x7f0f009c

.field public static final password_pref_editor_delete:I = 0x7f0f009b

.field public static final password_pref_editor_name:I = 0x7f0f0099

.field public static final password_pref_editor_url:I = 0x7f0f009a

.field public static final pickers:I = 0x7f0f008b

.field public static final popup_exception_pattern:I = 0x7f0f00dc

.field public static final popups_icon:I = 0x7f0f00d3

.field public static final preferences_id:I = 0x7f0f00f8

.field public static final preload_web_view:I = 0x7f0f0077

.field public static final preview:I = 0x7f0f009d

.field public static final progress:I = 0x7f0f00bb

.field public static final prompt_text:I = 0x7f0f00b8

.field public static final refresh_button:I = 0x7f0f00ce

.field public static final remove:I = 0x7f0f0009

.field public static final request_desktop_site_id:I = 0x7f0f00f7

.field public static final root_container:I = 0x7f0f004e

.field public static final sad_tab_image:I = 0x7f0f009f

.field public static final sad_tab_message:I = 0x7f0f00a1

.field public static final sad_tab_reload_button:I = 0x7f0f00a2

.field public static final sad_tab_title:I = 0x7f0f00a0

.field public static final save_exception_button:I = 0x7f0f00de

.field public static final save_image_context_menu_id:I = 0x7f0f00ed

.field public static final save_link_as_context_menu_id:I = 0x7f0f00eb

.field public static final scrollView:I = 0x7f0f00b7

.field public static final search_engine:I = 0x7f0f00d7

.field public static final security_button:I = 0x7f0f006c

.field public static final seekbar:I = 0x7f0f009e

.field public static final send_feedback_button:I = 0x7f0f00af

.field public static final send_report_checkbox:I = 0x7f0f0052

.field public static final share_page_id:I = 0x7f0f00f5

.field public static final sign_in_to_account:I = 0x7f0f0045

.field public static final skip_sign_in:I = 0x7f0f0046

.field public static final snapshot_prefix_label:I = 0x7f0f006d

.field public static final stop_reload_menu_id:I = 0x7f0f00fe

.field public static final suppress_js_modal_dialogs:I = 0x7f0f0069

.field public static final sync_promo:I = 0x7f0f0090

.field public static final sync_promo_stub:I = 0x7f0f008f

.field public static final tab_close_btn:I = 0x7f0f00bc

.field public static final tab_icon:I = 0x7f0f00ba

.field public static final tab_strip:I = 0x7f0f00c2

.field public static final tab_strip_incognito:I = 0x7f0f00c4

.field public static final tab_strip_incognito_stub:I = 0x7f0f00c3

.field public static final tab_title:I = 0x7f0f0002

.field public static final tabstrip:I = 0x7f0f007d

.field public static final tabstrip_stub:I = 0x7f0f007c

.field public static final terms:I = 0x7f0f0058

.field public static final terms_accept:I = 0x7f0f0059

.field public static final text:I = 0x7f0f0098

.field public static final thumb:I = 0x7f0f002e

.field public static final time_picker:I = 0x7f0f0034

.field public static final title_divider:I = 0x7f0f0006

.field public static final title_holder:I = 0x7f0f0004

.field public static final toolbar:I = 0x7f0f007f

.field public static final toolbar_buttons:I = 0x7f0f00c9

.field public static final toolbar_shadow:I = 0x7f0f0080

.field public static final toolbar_stub:I = 0x7f0f007e

.field public static final url_action_button:I = 0x7f0f0071

.field public static final url_action_container:I = 0x7f0f0070

.field public static final url_bar:I = 0x7f0f006e

.field public static final url_edit_id:I = 0x7f0f00ac

.field public static final url_title_id:I = 0x7f0f00ab

.field public static final usage_icon:I = 0x7f0f00d2

.field public static final use_current:I = 0x7f0f00da

.field public static final use_default:I = 0x7f0f00db

.field public static final username:I = 0x7f0f005c

.field public static final username_label:I = 0x7f0f005b

.field public static final verifying:I = 0x7f0f00b9

.field public static final video_view:I = 0x7f0f00cf

.field public static final website_settings_description:I = 0x7f0f00d6

.field public static final website_settings_headline:I = 0x7f0f00d5

.field public static final website_settings_icon:I = 0x7f0f00d4

.field public static final year:I = 0x7f0f008d


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
