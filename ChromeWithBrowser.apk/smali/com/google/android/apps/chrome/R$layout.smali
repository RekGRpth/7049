.class public final Lcom/google/android/apps/chrome/R$layout;
.super Ljava/lang/Object;


# static fields
.field public static final accessibility_tab_switcher:I = 0x7f040000

.field public static final accessibility_tab_switcher_list_item:I = 0x7f040001

.field public static final add_bookmark:I = 0x7f040002

.field public static final add_bookmark_content:I = 0x7f040003

.field public static final autofill_credit_card_editor:I = 0x7f040004

.field public static final autofill_profile_editor:I = 0x7f040005

.field public static final autofill_text:I = 0x7f040006

.field public static final bookmarkthumbnailwidget:I = 0x7f040007

.field public static final bookmarkthumbnailwidget_item:I = 0x7f040008

.field public static final bookmarkthumbnailwidget_item_folder:I = 0x7f040009

.field public static final compositor_view_holder:I = 0x7f04000a

.field public static final content_overlay:I = 0x7f04000b

.field public static final date_time_picker_dialog:I = 0x7f04000c

.field public static final do_not_track:I = 0x7f04000d

.field public static final empty_background_view:I = 0x7f04000e

.field public static final find_toolbar:I = 0x7f04000f

.field public static final fre_choose_account:I = 0x7f040010

.field public static final fre_notification_bar:I = 0x7f040011

.field public static final fre_root:I = 0x7f040012

.field public static final fre_root_tablet:I = 0x7f040013

.field public static final fre_signed:I = 0x7f040014

.field public static final fre_signing:I = 0x7f040015

.field public static final fre_spinner_dropdown:I = 0x7f040016

.field public static final fre_spinner_text:I = 0x7f040017

.field public static final fre_tosanduma:I = 0x7f040018

.field public static final http_auth_dialog:I = 0x7f040019

.field public static final incognito_button_instance:I = 0x7f04001a

.field public static final infobar_button:I = 0x7f04001b

.field public static final infobar_text:I = 0x7f04001c

.field public static final infobar_text_and_button:I = 0x7f04001d

.field public static final infobar_text_and_two_buttons:I = 0x7f04001e

.field public static final infobar_wrapper:I = 0x7f04001f

.field public static final js_modal_dialog:I = 0x7f040020

.field public static final location_bar:I = 0x7f040021

.field public static final main:I = 0x7f040022

.field public static final manage_bookmark:I = 0x7f040023

.field public static final menu_icon_row:I = 0x7f040024

.field public static final menu_item:I = 0x7f040025

.field public static final month_picker:I = 0x7f040026

.field public static final new_tab_page_toolbar:I = 0x7f040027

.field public static final new_tab_page_toolbar_button:I = 0x7f040028

.field public static final password_pref_editor:I = 0x7f040029

.field public static final preference_font_size_preview:I = 0x7f04002a

.field public static final preference_seekbar:I = 0x7f04002b

.field public static final preference_text_message:I = 0x7f04002c

.field public static final sad_tab:I = 0x7f04002d

.field public static final select_bookmark_folder:I = 0x7f04002e

.field public static final select_bookmark_folder_content:I = 0x7f04002f

.field public static final select_bookmark_folder_item:I = 0x7f040030

.field public static final send_feedback:I = 0x7f040031

.field public static final sync_custom_passphrase:I = 0x7f040032

.field public static final sync_encryption_type:I = 0x7f040033

.field public static final sync_enter_passphrase:I = 0x7f040034

.field public static final sync_passphrase_activity:I = 0x7f040035

.field public static final tab:I = 0x7f040036

.field public static final tab_strip_incognito_indicator_instance:I = 0x7f040037

.field public static final tab_strip_instance:I = 0x7f040038

.field public static final tab_strip_manager:I = 0x7f040039

.field public static final tab_strip_manager_instance:I = 0x7f04003a

.field public static final toolbar:I = 0x7f04003b

.field public static final video_player:I = 0x7f04003c

.field public static final website_features:I = 0x7f04003d

.field public static final website_settings:I = 0x7f04003e


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
