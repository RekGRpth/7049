.class public final Lcom/google/android/apps/chrome/R$string;
.super Ljava/lang/Object;


# static fields
.field public static final accept_cookies_summary:I = 0x7f0700ff

.field public static final accept_cookies_title:I = 0x7f0700fe

.field public static final accessibility_autofill_cc_month:I = 0x7f070218

.field public static final accessibility_autofill_cc_name_textbox:I = 0x7f070216

.field public static final accessibility_autofill_cc_number_textbox:I = 0x7f070217

.field public static final accessibility_autofill_cc_year:I = 0x7f070219

.field public static final accessibility_autofill_profile_address1:I = 0x7f07021c

.field public static final accessibility_autofill_profile_address2:I = 0x7f07021d

.field public static final accessibility_autofill_profile_city:I = 0x7f07021e

.field public static final accessibility_autofill_profile_company:I = 0x7f07021b

.field public static final accessibility_autofill_profile_country:I = 0x7f070221

.field public static final accessibility_autofill_profile_email:I = 0x7f070223

.field public static final accessibility_autofill_profile_name:I = 0x7f07021a

.field public static final accessibility_autofill_profile_phone:I = 0x7f070222

.field public static final accessibility_autofill_profile_state:I = 0x7f07021f

.field public static final accessibility_autofill_profile_zip:I = 0x7f070220

.field public static final accessibility_bookmark_title_textbox:I = 0x7f070214

.field public static final accessibility_bookmark_url_textbox:I = 0x7f070215

.field public static final accessibility_content_view:I = 0x7f070004

.field public static final accessibility_date_picker_month:I = 0x7f07000c

.field public static final accessibility_date_picker_year:I = 0x7f07000d

.field public static final accessibility_datetime_picker_date:I = 0x7f07000a

.field public static final accessibility_datetime_picker_time:I = 0x7f07000b

.field public static final accessibility_feedback_description_input:I = 0x7f07022d

.field public static final accessibility_feedback_url_input:I = 0x7f07022c

.field public static final accessibility_find_toolbar_btn_close:I = 0x7f07020a

.field public static final accessibility_find_toolbar_btn_next:I = 0x7f070208

.field public static final accessibility_find_toolbar_btn_prev:I = 0x7f070209

.field public static final accessibility_fre_account_spinner:I = 0x7f070224

.field public static final accessibility_http_auth_password_input:I = 0x7f070230

.field public static final accessibility_http_auth_username_input:I = 0x7f07022f

.field public static final accessibility_infobar_btn_close:I = 0x7f07020f

.field public static final accessibility_js_modal_dialog_prompt:I = 0x7f07001b

.field public static final accessibility_ntp_toolbar_btn_bookmarks:I = 0x7f070212

.field public static final accessibility_ntp_toolbar_btn_incognito:I = 0x7f070210

.field public static final accessibility_ntp_toolbar_btn_most_visited:I = 0x7f070211

.field public static final accessibility_ntp_toolbar_btn_other_devices:I = 0x7f070213

.field public static final accessibility_omnibox_btn_find_in_page:I = 0x7f07022b

.field public static final accessibility_omnibox_btn_refine:I = 0x7f07022a

.field public static final accessibility_omnibox_input:I = 0x7f0701fa

.field public static final accessibility_popup_exception_pattern:I = 0x7f07022e

.field public static final accessibility_sync_custom_confirm_passphrase:I = 0x7f070226

.field public static final accessibility_sync_custom_passphrase:I = 0x7f070225

.field public static final accessibility_sync_enter_custom_passphrase:I = 0x7f070228

.field public static final accessibility_sync_enter_google_passphrase:I = 0x7f070227

.field public static final accessibility_sync_enter_passphrase:I = 0x7f070229

.field public static final accessibility_tab_switcher_close_tab:I = 0x7f070239

.field public static final accessibility_tab_switcher_close_tab_2:I = 0x7f07023a

.field public static final accessibility_tab_switcher_close_tab_finished:I = 0x7f07023b

.field public static final accessibility_tab_switcher_close_tab_finished_2:I = 0x7f07023c

.field public static final accessibility_tab_switcher_closed_child_tab:I = 0x7f070247

.field public static final accessibility_tab_switcher_closed_child_tab_2:I = 0x7f070248

.field public static final accessibility_tab_switcher_closed_child_tab_3:I = 0x7f070249

.field public static final accessibility_tab_switcher_end_of_stack_reached:I = 0x7f07024a

.field public static final accessibility_tab_switcher_end_of_stack_reached_2:I = 0x7f07024b

.field public static final accessibility_tab_switcher_grey_tab:I = 0x7f070243

.field public static final accessibility_tab_switcher_grey_tab_2:I = 0x7f070244

.field public static final accessibility_tab_switcher_incognito_opened_in_bg:I = 0x7f070246

.field public static final accessibility_tab_switcher_incognito_stack:I = 0x7f07023f

.field public static final accessibility_tab_switcher_incognito_stack_switched:I = 0x7f070242

.field public static final accessibility_tab_switcher_standard_opened_in_bg:I = 0x7f070245

.field public static final accessibility_tab_switcher_standard_stack:I = 0x7f07023d

.field public static final accessibility_tab_switcher_standard_stack_2:I = 0x7f07023e

.field public static final accessibility_tab_switcher_standard_stack_switched:I = 0x7f070240

.field public static final accessibility_tab_switcher_standard_stack_switched_2:I = 0x7f070241

.field public static final accessibility_tab_switcher_swipe_finished:I = 0x7f070233

.field public static final accessibility_tab_switcher_swipe_tip:I = 0x7f070231

.field public static final accessibility_tab_switcher_swipe_tip_2:I = 0x7f070232

.field public static final accessibility_tab_switcher_swipe_to_dismiss_cancelled:I = 0x7f070238

.field public static final accessibility_tab_switcher_swipe_to_dismiss_finished:I = 0x7f070237

.field public static final accessibility_tab_switcher_swipe_to_dismiss_start:I = 0x7f070235

.field public static final accessibility_tab_switcher_swipe_to_dismiss_start_2:I = 0x7f070236

.field public static final accessibility_tab_switcher_tab:I = 0x7f070234

.field public static final accessibility_tabstrip_btn_close_tab:I = 0x7f0701f9

.field public static final accessibility_tabstrip_btn_empty_new_tab:I = 0x7f07020c

.field public static final accessibility_tabstrip_btn_incognito_toggle_incognito:I = 0x7f07020e

.field public static final accessibility_tabstrip_btn_incognito_toggle_standard:I = 0x7f07020d

.field public static final accessibility_tabstrip_btn_new_tab:I = 0x7f0701fb

.field public static final accessibility_tabstrip_ic_incognito_indicator:I = 0x7f07020b

.field public static final accessibility_tabstrip_tab:I = 0x7f0701f8

.field public static final accessibility_toolbar_btn_back:I = 0x7f0701fd

.field public static final accessibility_toolbar_btn_bookmark_page:I = 0x7f070201

.field public static final accessibility_toolbar_btn_delete_url:I = 0x7f070205

.field public static final accessibility_toolbar_btn_edit_bookmark_page:I = 0x7f070202

.field public static final accessibility_toolbar_btn_forward:I = 0x7f0701fc

.field public static final accessibility_toolbar_btn_menu:I = 0x7f070200

.field public static final accessibility_toolbar_btn_mic:I = 0x7f070204

.field public static final accessibility_toolbar_btn_new_tab:I = 0x7f070206

.field public static final accessibility_toolbar_btn_refresh:I = 0x7f0701fe

.field public static final accessibility_toolbar_btn_security_lock:I = 0x7f070203

.field public static final accessibility_toolbar_btn_stop_loading:I = 0x7f0701ff

.field public static final accessibility_toolbar_btn_tabswitcher_toggle:I = 0x7f070207

.field public static final acquiring_auth_token:I = 0x7f070091

.field public static final actionbar_share:I = 0x7f070002

.field public static final actionbar_textselection_title:I = 0x7f0700c7

.field public static final actionbar_web_search:I = 0x7f070003

.field public static final add_account_cancel:I = 0x7f07008a

.field public static final add_account_continue:I = 0x7f070089

.field public static final add_account_message:I = 0x7f070088

.field public static final add_account_title:I = 0x7f070087

.field public static final add_bookmark:I = 0x7f070041

.field public static final add_folder:I = 0x7f070043

.field public static final add_popup_exceptions_title:I = 0x7f070170

.field public static final all_bookmarks:I = 0x7f070031

.field public static final allow_block_spinner:I = 0x7f070197

.field public static final allow_exception_button:I = 0x7f070171

.field public static final always_prefetch_bandwidth_entry:I = 0x7f070196

.field public static final always_use_domain:I = 0x7f0701f6

.field public static final app_name:I = 0x7f070251

.field public static final application_version_title:I = 0x7f0701cd

.field public static final auto_login_failed:I = 0x7f070138

.field public static final auto_login_infobar_cancel_button_text:I = 0x7f070137

.field public static final auto_login_infobar_login_button_text:I = 0x7f070136

.field public static final auto_login_infobar_message:I = 0x7f070135

.field public static final autofill_cancel:I = 0x7f0700ec

.field public static final autofill_create_or_edit_credit_card:I = 0x7f0700dc

.field public static final autofill_create_or_edit_profile:I = 0x7f0700db

.field public static final autofill_credit_card_editor_month_spinner_prompt:I = 0x7f070133

.field public static final autofill_credit_card_editor_name:I = 0x7f070131

.field public static final autofill_credit_card_editor_number:I = 0x7f070132

.field public static final autofill_credit_card_editor_year_spinner_prompt:I = 0x7f070134

.field public static final autofill_credit_cards_title:I = 0x7f0700ea

.field public static final autofill_delete:I = 0x7f0700ed

.field public static final autofill_delete_profile_message:I = 0x7f0700ee

.field public static final autofill_profile_editor_address_line_1:I = 0x7f070121

.field public static final autofill_profile_editor_address_line_1_hint:I = 0x7f070122

.field public static final autofill_profile_editor_address_line_2:I = 0x7f070123

.field public static final autofill_profile_editor_address_line_2_hint:I = 0x7f070124

.field public static final autofill_profile_editor_city:I = 0x7f070125

.field public static final autofill_profile_editor_company_name:I = 0x7f070120

.field public static final autofill_profile_editor_country:I = 0x7f070128

.field public static final autofill_profile_editor_email_address:I = 0x7f07011f

.field public static final autofill_profile_editor_heading:I = 0x7f07011d

.field public static final autofill_profile_editor_name:I = 0x7f07011e

.field public static final autofill_profile_editor_phone_number:I = 0x7f070129

.field public static final autofill_profile_editor_state:I = 0x7f070126

.field public static final autofill_profile_editor_zip_code:I = 0x7f070127

.field public static final autofill_profiles_title:I = 0x7f0700e9

.field public static final autofill_save:I = 0x7f0700eb

.field public static final autofill_summary:I = 0x7f0700e6

.field public static final autofill_title:I = 0x7f0700e4

.field public static final autofill_title_long:I = 0x7f0700e5

.field public static final back:I = 0x7f070024

.field public static final bing_search_engine_name:I = 0x7f07018e

.field public static final block_block_spinner:I = 0x7f070198

.field public static final block_exception_button:I = 0x7f070172

.field public static final block_popups_summary:I = 0x7f07016c

.field public static final block_popups_title:I = 0x7f07016b

.field public static final bookmark_folder:I = 0x7f070047

.field public static final bookmark_folder_tree_error:I = 0x7f070050

.field public static final bookmark_missing_name:I = 0x7f070048

.field public static final bookmark_missing_url:I = 0x7f070049

.field public static final bookmark_name:I = 0x7f070045

.field public static final bookmark_shortcut_choose_bookmark:I = 0x7f070053

.field public static final bookmark_shortcut_name:I = 0x7f070052

.field public static final bookmark_this_page:I = 0x7f070033

.field public static final bookmark_url:I = 0x7f070046

.field public static final bookmarks:I = 0x7f070022

.field public static final bookmarks_widget_name:I = 0x7f070051

.field public static final bug_feature_bug:I = 0x7f070187

.field public static final cancel:I = 0x7f0700f4

.field public static final cancel_feedback_button:I = 0x7f0700b6

.field public static final cannot_download:I = 0x7f0700ae

.field public static final category_about_chrome:I = 0x7f0700d6

.field public static final category_accessibility:I = 0x7f0700cb

.field public static final category_add_popup_exceptions:I = 0x7f07016f

.field public static final category_advanced:I = 0x7f0700d2

.field public static final category_autofill_create_credit_card:I = 0x7f0700da

.field public static final category_autofill_create_profile:I = 0x7f0700d9

.field public static final category_autofill_settings:I = 0x7f0700d7

.field public static final category_bandwidth:I = 0x7f0700cc

.field public static final category_basics:I = 0x7f0700ca

.field public static final category_content_settings:I = 0x7f0700d0

.field public static final category_dev_tools:I = 0x7f0700d4

.field public static final category_forms:I = 0x7f0700cd

.field public static final category_language:I = 0x7f0700d5

.field public static final category_legal_information_summary:I = 0x7f0701cb

.field public static final category_legal_information_title:I = 0x7f0701ca

.field public static final category_manage_popup_exceptions:I = 0x7f07016d

.field public static final category_manage_saved_passwords:I = 0x7f0700d8

.field public static final category_privacy:I = 0x7f0700d3

.field public static final category_sync_settings:I = 0x7f0700ce

.field public static final category_sync_settings_after_sign_in:I = 0x7f0700cf

.field public static final category_under_the_hood:I = 0x7f0700d1

.field public static final category_website_settings:I = 0x7f0700df

.field public static final category_website_settings_subtitle:I = 0x7f0700e0

.field public static final certtitle:I = 0x7f07001c

.field public static final channel_name:I = 0x7f070250

.field public static final choose_account_cancel:I = 0x7f07008c

.field public static final choose_account_sign_in:I = 0x7f07008b

.field public static final choose_folder:I = 0x7f07004a

.field public static final choose_search_engine:I = 0x7f0701ed

.field public static final clear:I = 0x7f0700f3

.field public static final clear_browsing_data_progress_message:I = 0x7f0700fd

.field public static final clear_browsing_data_progress_title:I = 0x7f0700fc

.field public static final clear_browsing_data_summary:I = 0x7f0700f6

.field public static final clear_browsing_data_title:I = 0x7f0700f5

.field public static final clear_cache_title:I = 0x7f0700f7

.field public static final clear_cookies_and_site_data_title:I = 0x7f0700f9

.field public static final clear_data_cancel:I = 0x7f0701c8

.field public static final clear_data_delete:I = 0x7f0701c9

.field public static final clear_formdata_title:I = 0x7f0700fb

.field public static final clear_history_title:I = 0x7f0700f8

.field public static final clear_passwords_title:I = 0x7f0700fa

.field public static final close_all_incognito_tabs:I = 0x7f07003c

.field public static final close_all_tabs:I = 0x7f07003b

.field public static final closetab:I = 0x7f07002c

.field public static final content_provider_search_description:I = 0x7f0701f7

.field public static final contextmenu_copy_link_address:I = 0x7f0700ba

.field public static final contextmenu_copy_link_text:I = 0x7f0700bb

.field public static final contextmenu_download_link:I = 0x7f0700bf

.field public static final contextmenu_open_image:I = 0x7f0700c0

.field public static final contextmenu_open_image_in_new_tab:I = 0x7f0700c1

.field public static final contextmenu_open_in_incognito_tab:I = 0x7f0700b9

.field public static final contextmenu_open_in_new_tab:I = 0x7f0700b8

.field public static final contextmenu_save_image:I = 0x7f0700bc

.field public static final contextmenu_save_link:I = 0x7f0700be

.field public static final contextmenu_save_link_as_text:I = 0x7f0700bd

.field public static final crash_dump_always_upload:I = 0x7f07010e

.field public static final crash_dump_always_upload_value:I = 0x7f070257

.field public static final crash_dump_never_upload:I = 0x7f07010d

.field public static final crash_dump_never_upload_value:I = 0x7f070256

.field public static final crash_dump_only_with_wifi:I = 0x7f07010f

.field public static final crash_dump_only_with_wifi_value:I = 0x7f070258

.field public static final crash_dump_upload_summary:I = 0x7f07010c

.field public static final crash_dump_upload_title:I = 0x7f07010b

.field public static final create_autofill_profile_summary:I = 0x7f0700e8

.field public static final create_autofill_profile_title:I = 0x7f0700e7

.field public static final current_search_engine:I = 0x7f0701c0

.field public static final date_picker_dialog_clear:I = 0x7f070006

.field public static final date_picker_dialog_set:I = 0x7f070005

.field public static final date_picker_dialog_title:I = 0x7f070007

.field public static final date_time_picker_dialog_title:I = 0x7f070008

.field public static final default_block_spinner:I = 0x7f070199

.field public static final default_folder_error:I = 0x7f07004c

.field public static final default_search_engine:I = 0x7f0701ec

.field public static final delete_exception_button:I = 0x7f070174

.field public static final description_title:I = 0x7f0700b3

.field public static final do_not_track_description:I = 0x7f070111

.field public static final do_not_track_learn:I = 0x7f070112

.field public static final do_not_track_title:I = 0x7f070110

.field public static final dont_reload_this_page:I = 0x7f070016

.field public static final download_no_sdcard_dlg_msg:I = 0x7f0700ab

.field public static final download_no_sdcard_dlg_title:I = 0x7f0700aa

.field public static final download_pending:I = 0x7f0700af

.field public static final download_sdcard_busy_dlg_msg:I = 0x7f0700ad

.field public static final download_sdcard_busy_dlg_title:I = 0x7f0700ac

.field public static final edit_bookmark:I = 0x7f070042

.field public static final edit_bookmark_menu:I = 0x7f070034

.field public static final edit_folder:I = 0x7f070044

.field public static final email_title:I = 0x7f0700b4

.field public static final embed_chrome_view_activity_unreachable:I = 0x7f0701d4

.field public static final enable_chrome_to_mobile:I = 0x7f070118

.field public static final enable_javascript_summary:I = 0x7f070106

.field public static final enable_javascript_title:I = 0x7f070105

.field public static final enable_location_summary:I = 0x7f070104

.field public static final enable_location_title:I = 0x7f070102

.field public static final enable_location_title_alt1:I = 0x7f070103

.field public static final enable_on_demand_zoom_summary:I = 0x7f070108

.field public static final enable_on_demand_zoom_title:I = 0x7f070107

.field public static final enable_remote_debugging:I = 0x7f070038

.field public static final enable_remote_debugging_summary:I = 0x7f070039

.field public static final enable_sync:I = 0x7f070117

.field public static final enable_tilt_scroll:I = 0x7f07011b

.field public static final enable_tilt_scroll_summary:I = 0x7f07011c

.field public static final entered_fullscreen:I = 0x7f070130

.field public static final entered_fullscreen_drag_to_exit:I = 0x7f07012f

.field public static final error_proceed_anyway:I = 0x7f070062

.field public static final error_quit:I = 0x7f070063

.field public static final error_unexpected_system_problem:I = 0x7f070064

.field public static final executable_path_title:I = 0x7f0701d1

.field public static final exit_fullscreen:I = 0x7f07012e

.field public static final external_app_leave_incognito_cancel:I = 0x7f0700c5

.field public static final external_app_leave_incognito_ok:I = 0x7f0700c6

.field public static final external_app_leave_incognito_warning:I = 0x7f0700c2

.field public static final external_app_leave_incognito_warning_alt1:I = 0x7f0700c3

.field public static final external_app_leave_incognito_warning_title:I = 0x7f0700c4

.field public static final feature_request_feature_bug:I = 0x7f070188

.field public static final find_in_page:I = 0x7f07002e

.field public static final find_in_page_count:I = 0x7f070252

.field public static final flash_version_title:I = 0x7f0701d0

.field public static final font_size_alt1:I = 0x7f0701e4

.field public static final font_size_alt2:I = 0x7f0701e5

.field public static final font_size_preview:I = 0x7f0701e7

.field public static final font_size_preview_text:I = 0x7f0701dc

.field public static final font_size_preview_text_alt1:I = 0x7f0701dd

.field public static final font_size_preview_text_alt2:I = 0x7f0701de

.field public static final font_size_preview_text_alt3:I = 0x7f0701df

.field public static final font_size_preview_text_alt4:I = 0x7f0701e0

.field public static final font_size_summary:I = 0x7f070114

.field public static final font_size_title:I = 0x7f070113

.field public static final force_enable_zoom_summary:I = 0x7f07024d

.field public static final force_enable_zoom_title:I = 0x7f07024c

.field public static final forward:I = 0x7f070025

.field public static final fre_accept_continue:I = 0x7f07019c

.field public static final fre_add_account:I = 0x7f0701a1

.field public static final fre_chrome_features_part1:I = 0x7f0701a6

.field public static final fre_chrome_features_part1_alt1:I = 0x7f0701a8

.field public static final fre_chrome_features_part2:I = 0x7f0701a7

.field public static final fre_chrome_features_part2_alt1:I = 0x7f0701a9

.field public static final fre_chrome_features_part2_alt2:I = 0x7f0701aa

.field public static final fre_label:I = 0x7f07019a

.field public static final fre_no_accounts:I = 0x7f0701ab

.field public static final fre_privacy_notice_title:I = 0x7f0701a2

.field public static final fre_send_report_check:I = 0x7f07019b

.field public static final fre_sign_in_text:I = 0x7f07019d

.field public static final fre_signed_text_part1:I = 0x7f0701a4

.field public static final fre_signed_text_part2:I = 0x7f0701a5

.field public static final fre_signin_chrome:I = 0x7f0701ac

.field public static final fre_signing_text_part1:I = 0x7f0701a3

.field public static final fre_skip_text:I = 0x7f07019e

.field public static final fre_skip_text_alt1:I = 0x7f07019f

.field public static final fre_skip_text_alt2:I = 0x7f0701a0

.field public static final fre_sync_link_after_fre_customize:I = 0x7f0701bd

.field public static final fre_sync_link_after_fre_edit:I = 0x7f0701bf

.field public static final fre_sync_link_after_fre_settings:I = 0x7f0701be

.field public static final fre_sync_promo_after_fre_part1:I = 0x7f0701b8

.field public static final fre_sync_promo_after_fre_part2_alt1:I = 0x7f0701b9

.field public static final fre_sync_promo_after_fre_part2_alt2:I = 0x7f0701ba

.field public static final fre_sync_promo_after_fre_part2_alt3:I = 0x7f0701bb

.field public static final fre_sync_promo_after_fre_part2_alt4:I = 0x7f0701bc

.field public static final fre_welcome_page_learn:I = 0x7f0701b7

.field public static final fre_welcome_page_search:I = 0x7f0701ad

.field public static final fre_welcome_page_swipe_alt1:I = 0x7f0701af

.field public static final fre_welcome_page_swipe_alt2:I = 0x7f0701b0

.field public static final fre_welcome_page_swipe_alt3:I = 0x7f0701b1

.field public static final fre_welcome_page_swipe_alt4:I = 0x7f0701b2

.field public static final fre_welcome_page_swipe_alt5:I = 0x7f0701b3

.field public static final fre_welcome_page_swipe_alt6:I = 0x7f0701b4

.field public static final fre_welcome_page_swipe_alt7:I = 0x7f0701b5

.field public static final fre_welcome_page_swipe_alt8:I = 0x7f0701b6

.field public static final fre_welcome_page_tabs:I = 0x7f0701ae

.field public static final geolocation_settings_page_summary_allowed:I = 0x7f070176

.field public static final geolocation_settings_page_summary_not_allowed:I = 0x7f070177

.field public static final geolocation_settings_page_title:I = 0x7f070175

.field public static final google_chrome_version_title:I = 0x7f0701cc

.field public static final google_search_engine_name:I = 0x7f07018c

.field public static final hardware_acceleration_summary:I = 0x7f07011a

.field public static final hardware_acceleration_title:I = 0x7f070119

.field public static final help:I = 0x7f0700c9

.field public static final home:I = 0x7f07002b

.field public static final home_page_title:I = 0x7f0700de

.field public static final http:I = 0x7f070255

.field public static final import_bookmarks:I = 0x7f070032

.field public static final import_bookmarks_cancel:I = 0x7f070059

.field public static final import_bookmarks_failed_header:I = 0x7f07005c

.field public static final import_bookmarks_failed_message:I = 0x7f07005d

.field public static final import_bookmarks_ok:I = 0x7f070058

.field public static final import_bookmarks_progress_header:I = 0x7f07005a

.field public static final import_bookmarks_progress_message:I = 0x7f07005b

.field public static final import_bookmarks_prompt:I = 0x7f070057

.field public static final import_bookmarks_retry:I = 0x7f07005e

.field public static final invalid_bookmark:I = 0x7f070056

.field public static final invalid_os_version:I = 0x7f070060

.field public static final inverted_rendering:I = 0x7f0701e1

.field public static final inverted_rendering_alt1:I = 0x7f0701e2

.field public static final inverted_rendering_alt2:I = 0x7f0701e3

.field public static final javascript_version_title:I = 0x7f0701cf

.field public static final js_modal_dialog_cancel:I = 0x7f070019

.field public static final js_modal_dialog_confirm:I = 0x7f070018

.field public static final large_font_size:I = 0x7f070192

.field public static final learn_privacy_features_title:I = 0x7f0701c7

.field public static final learn_privacy_features_url:I = 0x7f070259

.field public static final learn_remote_debugging_title:I = 0x7f07003a

.field public static final learn_remote_debugging_url:I = 0x7f070254

.field public static final leave_this_page:I = 0x7f070013

.field public static final loading_bookmark:I = 0x7f07004d

.field public static final low_memory_error:I = 0x7f070000

.field public static final manage_search_engines:I = 0x7f0701eb

.field public static final manage_sync_data:I = 0x7f07006e

.field public static final media_player_error_button:I = 0x7f070011

.field public static final media_player_error_text_invalid_progressive_playback:I = 0x7f07000f

.field public static final media_player_error_text_unknown:I = 0x7f070010

.field public static final media_player_error_title:I = 0x7f07000e

.field public static final media_player_loading_video:I = 0x7f070012

.field public static final medium_font_size:I = 0x7f070191

.field public static final message_infobar_ok_text:I = 0x7f070139

.field public static final month_picker_dialog_title:I = 0x7f070009

.field public static final navigation_error_summary:I = 0x7f0701c2

.field public static final navigation_error_title:I = 0x7f0701c1

.field public static final network_predictions_summary:I = 0x7f0701c6

.field public static final network_predictions_title:I = 0x7f0701c5

.field public static final never_prefetch_bandwidth_entry:I = 0x7f070194

.field public static final new_folder:I = 0x7f07004b

.field public static final newincognitotab:I = 0x7f07002a

.field public static final newtab:I = 0x7f070029

.field public static final nfc_beam_error_bad_url:I = 0x7f0701db

.field public static final nfc_beam_error_overlay_active:I = 0x7f0701da

.field public static final no_bookmark_folders:I = 0x7f070055

.field public static final no_saved_website_settings:I = 0x7f0701d8

.field public static final ntp_bookmarks:I = 0x7f07001d

.field public static final ntp_incognito:I = 0x7f070021

.field public static final ntp_most_visited:I = 0x7f07001e

.field public static final ntp_open_tabs:I = 0x7f070020

.field public static final ntp_other_devices:I = 0x7f07001f

.field public static final ok:I = 0x7f070027

.field public static final on_startup_title:I = 0x7f0700e1

.field public static final open:I = 0x7f070023

.field public static final open_home_page:I = 0x7f07018a

.field public static final open_last_pages:I = 0x7f07018b

.field public static final open_source_license_title:I = 0x7f0701d3

.field public static final open_source_license_url:I = 0x7f07025b

.field public static final opening_file_error:I = 0x7f070001

.field public static final origin_settings_storage_bytes:I = 0x7f070180

.field public static final origin_settings_storage_gbytes:I = 0x7f070183

.field public static final origin_settings_storage_kbytes:I = 0x7f070181

.field public static final origin_settings_storage_mbytes:I = 0x7f070182

.field public static final origin_settings_storage_tbytes:I = 0x7f070184

.field public static final origin_settings_storage_usage:I = 0x7f07017f

.field public static final os_version_missing_features:I = 0x7f070061

.field public static final os_version_title:I = 0x7f07025a

.field public static final page_scaling:I = 0x7f0701e6

.field public static final password_pref_editor_never_saved:I = 0x7f0700f1

.field public static final password_pref_editor_title:I = 0x7f0700dd

.field public static final performance_feature_bug:I = 0x7f070189

.field public static final popup_exceptions_title:I = 0x7f07016e

.field public static final popup_settings_page_summary_allowed:I = 0x7f070185

.field public static final popup_settings_page_summary_not_allowed:I = 0x7f070186

.field public static final popups_blocked_infobar_button_show:I = 0x7f07013b

.field public static final popups_blocked_infobar_text:I = 0x7f07013a

.field public static final pref_use_current:I = 0x7f0700e2

.field public static final pref_use_default:I = 0x7f0700e3

.field public static final preferences:I = 0x7f0700c8

.field public static final prefetch_bandwidth_summary:I = 0x7f070116

.field public static final prefetch_bandwidth_title:I = 0x7f070115

.field public static final print:I = 0x7f070036

.field public static final print_ellipses:I = 0x7f070037

.field public static final privacy_policy_title:I = 0x7f0701d6

.field public static final privacy_policy_url:I = 0x7f07025d

.field public static final profile_path_title:I = 0x7f0701d2

.field public static final profiler_error_toast:I = 0x7f07012d

.field public static final profiler_no_storage_toast:I = 0x7f07012c

.field public static final profiler_started_toast:I = 0x7f07012a

.field public static final profiler_stopped_toast:I = 0x7f07012b

.field public static final reload:I = 0x7f070028

.field public static final reload_this_page:I = 0x7f070015

.field public static final remove:I = 0x7f07003e

.field public static final removing_bookmark:I = 0x7f07004f

.field public static final request_desktop_site:I = 0x7f0701d9

.field public static final reset_search_engine:I = 0x7f0701f1

.field public static final reset_search_engine_alt1:I = 0x7f0701f2

.field public static final reset_search_engine_alt2:I = 0x7f0701f3

.field public static final reset_sync:I = 0x7f07006d

.field public static final rlz_access_point:I = 0x7f07025e

.field public static final save:I = 0x7f07003d

.field public static final save_bookmark:I = 0x7f07003f

.field public static final save_exception_button:I = 0x7f070173

.field public static final save_page_as:I = 0x7f07002d

.field public static final save_passwords_summary:I = 0x7f070101

.field public static final save_passwords_title:I = 0x7f070100

.field public static final saved_passwords_exceptions_title:I = 0x7f0700f0

.field public static final saved_passwords_none_text:I = 0x7f0700f2

.field public static final saved_passwords_title:I = 0x7f0700ef

.field public static final saving_bookmark:I = 0x7f07004e

.field public static final search_engine_changed:I = 0x7f0701ef

.field public static final search_engine_changed_alt1:I = 0x7f0701f0

.field public static final search_engine_domain_should_change:I = 0x7f0701f5

.field public static final search_engine_domain_will_change:I = 0x7f0701f4

.field public static final search_engine_name_and_domain:I = 0x7f0701ee

.field public static final search_engine_summary:I = 0x7f07010a

.field public static final search_engine_title:I = 0x7f070109

.field public static final search_suggestions_summary:I = 0x7f0701c4

.field public static final search_suggestions_title:I = 0x7f0701c3

.field public static final select_bookmark_folder:I = 0x7f070040

.field public static final send_feedback:I = 0x7f0700b0

.field public static final send_feedback_alt:I = 0x7f0700b1

.field public static final send_feedback_button:I = 0x7f0700b5

.field public static final share_link_chooser_title:I = 0x7f0700b7

.field public static final share_page:I = 0x7f07005f

.field public static final show_history:I = 0x7f070035

.field public static final show_history_label:I = 0x7f070017

.field public static final sign_in_auto_login:I = 0x7f070085

.field public static final sign_in_auto_login_description:I = 0x7f070086

.field public static final sign_in_choose_google_account:I = 0x7f07007b

.field public static final sign_in_google_account:I = 0x7f07007a

.field public static final sign_in_no_existing_google_account:I = 0x7f07007d

.field public static final sign_in_select_google_account:I = 0x7f07007c

.field public static final sign_in_send_to_device:I = 0x7f070083

.field public static final sign_in_send_to_device_description:I = 0x7f070084

.field public static final sign_in_services:I = 0x7f07007e

.field public static final sign_in_sync:I = 0x7f07007f

.field public static final sign_in_sync_alt1:I = 0x7f070080

.field public static final sign_in_sync_alt2:I = 0x7f070081

.field public static final sign_in_sync_description:I = 0x7f070082

.field public static final signout_cancel:I = 0x7f070090

.field public static final signout_message:I = 0x7f07008e

.field public static final signout_signout:I = 0x7f07008f

.field public static final signout_title:I = 0x7f07008d

.field public static final small_font_size:I = 0x7f070190

.field public static final snapshot_cannot_be_downloaded:I = 0x7f070161

.field public static final snapshot_cannot_be_opened:I = 0x7f070160

.field public static final snapshot_download_default_title:I = 0x7f070143

.field public static final snapshot_download_description_default:I = 0x7f070144

.field public static final snapshot_download_description_print:I = 0x7f070145

.field public static final snapshot_download_failed_infobar:I = 0x7f07013e

.field public static final snapshot_downloaded_infobar_button_open:I = 0x7f07013d

.field public static final snapshot_downloading_infobar_text:I = 0x7f07013c

.field public static final snapshot_error_opening_mime_type:I = 0x7f070147

.field public static final snapshot_external_public_directory_sub_path:I = 0x7f070146

.field public static final snapshot_external_storage_bad_removal:I = 0x7f070158

.field public static final snapshot_external_storage_bad_removal_alt:I = 0x7f070163

.field public static final snapshot_external_storage_checking:I = 0x7f070159

.field public static final snapshot_external_storage_checking_alt:I = 0x7f070164

.field public static final snapshot_external_storage_downloaded:I = 0x7f070155

.field public static final snapshot_external_storage_generic_error:I = 0x7f07015f

.field public static final snapshot_external_storage_generic_error_alt:I = 0x7f07016a

.field public static final snapshot_external_storage_nofs:I = 0x7f07015a

.field public static final snapshot_external_storage_nofs_alt:I = 0x7f070165

.field public static final snapshot_external_storage_opened:I = 0x7f070156

.field public static final snapshot_external_storage_read_only:I = 0x7f070157

.field public static final snapshot_external_storage_read_only_alt:I = 0x7f070162

.field public static final snapshot_external_storage_removed:I = 0x7f07015b

.field public static final snapshot_external_storage_removed_alt:I = 0x7f070166

.field public static final snapshot_external_storage_shared:I = 0x7f07015c

.field public static final snapshot_external_storage_shared_alt:I = 0x7f070167

.field public static final snapshot_external_storage_unable_to_create_folder:I = 0x7f070154

.field public static final snapshot_external_storage_unmountable:I = 0x7f07015d

.field public static final snapshot_external_storage_unmountable_alt:I = 0x7f070168

.field public static final snapshot_external_storage_unmounted:I = 0x7f07015e

.field public static final snapshot_external_storage_unmounted_alt:I = 0x7f070169

.field public static final snapshot_open_failed_infobar:I = 0x7f07013f

.field public static final snapshot_visible_url_1:I = 0x7f070148

.field public static final snapshot_visible_url_10:I = 0x7f070151

.field public static final snapshot_visible_url_11:I = 0x7f070152

.field public static final snapshot_visible_url_12:I = 0x7f070153

.field public static final snapshot_visible_url_2:I = 0x7f070149

.field public static final snapshot_visible_url_3:I = 0x7f07014a

.field public static final snapshot_visible_url_4:I = 0x7f07014b

.field public static final snapshot_visible_url_5:I = 0x7f07014c

.field public static final snapshot_visible_url_6:I = 0x7f07014d

.field public static final snapshot_visible_url_7:I = 0x7f07014e

.field public static final snapshot_visible_url_8:I = 0x7f07014f

.field public static final snapshot_visible_url_9:I = 0x7f070150

.field public static final stay_on_this_page:I = 0x7f070014

.field public static final stop:I = 0x7f070026

.field public static final suppress_js_modal_dialogs:I = 0x7f07001a

.field public static final sync_android_master_sync_disabled:I = 0x7f07006f

.field public static final sync_autofill:I = 0x7f070067

.field public static final sync_before_sign_in_summary:I = 0x7f0701d7

.field public static final sync_bookmarks:I = 0x7f070068

.field public static final sync_create_account:I = 0x7f070094

.field public static final sync_custom_passphrase:I = 0x7f070095

.field public static final sync_data_types:I = 0x7f070065

.field public static final sync_encryption:I = 0x7f07006c

.field public static final sync_enter_custom_passphrase_hint:I = 0x7f070096

.field public static final sync_enter_custom_passphrase_hint_confirm:I = 0x7f070097

.field public static final sync_enter_google_passphrase:I = 0x7f070098

.field public static final sync_enter_google_passphrase_hint:I = 0x7f07009a

.field public static final sync_enter_google_passphrase_long:I = 0x7f070099

.field public static final sync_error_asp_error:I = 0x7f0700a6

.field public static final sync_error_connection:I = 0x7f0700a3

.field public static final sync_error_domain:I = 0x7f0700a4

.field public static final sync_error_ga:I = 0x7f0700a1

.field public static final sync_error_generic:I = 0x7f0700a0

.field public static final sync_error_passphrase:I = 0x7f0700a2

.field public static final sync_error_password_incorrect:I = 0x7f0700a5

.field public static final sync_error_service_unavailable:I = 0x7f0700a7

.field public static final sync_error_service_unavailable_auto_retry:I = 0x7f0700a8

.field public static final sync_error_service_unavailable_no_retry:I = 0x7f0700a9

.field public static final sync_everything_pref:I = 0x7f070066

.field public static final sync_loading:I = 0x7f07009f

.field public static final sync_need_passphrase:I = 0x7f070070

.field public static final sync_need_password:I = 0x7f070071

.field public static final sync_omnibox:I = 0x7f070069

.field public static final sync_passphrase_cancel:I = 0x7f070072

.field public static final sync_passphrase_cannot_be_blank:I = 0x7f07009b

.field public static final sync_passphrase_incorrect:I = 0x7f07009d

.field public static final sync_passphrase_ok:I = 0x7f070073

.field public static final sync_passphrase_type_cancel:I = 0x7f070074

.field public static final sync_passphrase_type_custom:I = 0x7f070079

.field public static final sync_passphrase_type_gaia:I = 0x7f070077

.field public static final sync_passphrase_type_keystore:I = 0x7f070078

.field public static final sync_passphrase_type_none:I = 0x7f070076

.field public static final sync_passphrase_type_title:I = 0x7f070075

.field public static final sync_passphrases_do_not_match:I = 0x7f07009c

.field public static final sync_passwords:I = 0x7f07006a

.field public static final sync_recent_tabs:I = 0x7f07006b

.field public static final sync_setup_progress:I = 0x7f070092

.field public static final sync_sign_in_or_sign_up:I = 0x7f070093

.field public static final sync_verifying:I = 0x7f07009e

.field public static final tab_count:I = 0x7f070253

.field public static final tab_count_overflow:I = 0x7f07002f

.field public static final tab_loading_default_title:I = 0x7f070142

.field public static final tab_switcher_button:I = 0x7f0701e8

.field public static final tab_switcher_button_alt1:I = 0x7f0701e9

.field public static final tab_switcher_button_alt2:I = 0x7f0701ea

.field public static final terms_of_service_title:I = 0x7f0701d5

.field public static final terms_of_service_url:I = 0x7f07025c

.field public static final text_off:I = 0x7f07024f

.field public static final text_on:I = 0x7f07024e

.field public static final type_to_search:I = 0x7f070030

.field public static final update_available_infobar:I = 0x7f070140

.field public static final update_available_infobar_button:I = 0x7f070141

.field public static final url_title:I = 0x7f0700b2

.field public static final very_large_font_size:I = 0x7f070193

.field public static final very_small_font_size:I = 0x7f07018f

.field public static final voice_search_error:I = 0x7f070054

.field public static final webkit_version_title:I = 0x7f0701ce

.field public static final website_settings_clear_all:I = 0x7f07017c

.field public static final website_settings_embedded_in:I = 0x7f07017d

.field public static final website_settings_popup_exceptions:I = 0x7f07017e

.field public static final webstorage_clear_data_dialog_cancel_button:I = 0x7f07017b

.field public static final webstorage_clear_data_dialog_message:I = 0x7f070179

.field public static final webstorage_clear_data_dialog_ok_button:I = 0x7f07017a

.field public static final webstorage_clear_data_dialog_title:I = 0x7f070178

.field public static final wifi_prefetch_bandwidth_entry:I = 0x7f070195

.field public static final yahoo_search_engine_name:I = 0x7f07018d


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
