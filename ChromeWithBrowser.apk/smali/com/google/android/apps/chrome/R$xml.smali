.class public final Lcom/google/android/apps/chrome/R$xml;
.super Ljava/lang/Object;


# static fields
.field public static final about_chrome_preferences:I = 0x7f060000

.field public static final accessibility_preferences:I = 0x7f060001

.field public static final autofill_settings_preferences:I = 0x7f060002

.field public static final bandwidth_preferences:I = 0x7f060003

.field public static final basics_preferences:I = 0x7f060004

.field public static final bookmarkthumbnailwidget_info:I = 0x7f060005

.field public static final content_settings_preferences:I = 0x7f060006

.field public static final dev_tools_preferences:I = 0x7f060007

.field public static final do_not_track_preferences:I = 0x7f060008

.field public static final homepage_preference_buttons:I = 0x7f060009

.field public static final language_preferences:I = 0x7f06000a

.field public static final legal_information_preferences:I = 0x7f06000b

.field public static final manage_popup_exceptions:I = 0x7f06000c

.field public static final popup_exceptions_preferences:I = 0x7f06000d

.field public static final preference_header_item_layout:I = 0x7f06000e

.field public static final preference_header_switch_item_layout:I = 0x7f06000f

.field public static final preference_headers_phone:I = 0x7f060010

.field public static final preference_headers_tablet:I = 0x7f060011

.field public static final privacy_preferences:I = 0x7f060012

.field public static final search_engine_preferences:I = 0x7f060013

.field public static final searchable:I = 0x7f060014

.field public static final sign_in_preferences:I = 0x7f060015

.field public static final single_website_settings_preferences:I = 0x7f060016

.field public static final sync_customization_preferences:I = 0x7f060017

.field public static final syncadapter:I = 0x7f060018

.field public static final under_the_hood_preferences:I = 0x7f060019

.field public static final website_settings_preferences:I = 0x7f06001a


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
