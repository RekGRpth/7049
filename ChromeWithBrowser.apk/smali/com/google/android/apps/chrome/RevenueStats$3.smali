.class Lcom/google/android/apps/chrome/RevenueStats$3;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/RevenueStats;

.field final synthetic val$peek:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/RevenueStats;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/RevenueStats$3;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/RevenueStats$3;->val$peek:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/RevenueStats$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 6

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/RevenueStats$3;->val$peek:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats$3;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    # getter for: Lcom/google/android/apps/chrome/RevenueStats;->mBaseRlzURI:Landroid/net/Uri;
    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->access$500(Lcom/google/android/apps/chrome/RevenueStats;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "peek"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats$3;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    # getter for: Lcom/google/android/apps/chrome/RevenueStats;->mApplicationContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->access$000(Lcom/google/android/apps/chrome/RevenueStats;)Landroid/content/Context;

    move-result-object v0

    const-string v2, ""

    move-object v4, v3

    move-object v5, v3

    # invokes: Lcom/google/android/apps/chrome/RevenueStats;->getData(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/RevenueStats;->access$600(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats$3;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    # getter for: Lcom/google/android/apps/chrome/RevenueStats;->mBaseRlzURI:Landroid/net/Uri;
    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->access$500(Lcom/google/android/apps/chrome/RevenueStats;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/RevenueStats$3;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats$3;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    # getter for: Lcom/google/android/apps/chrome/RevenueStats;->mRlz:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->access$700(Lcom/google/android/apps/chrome/RevenueStats;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    # invokes: Lcom/google/android/apps/chrome/RevenueStats;->nativeSetRlzParameterValue(Ljava/lang/String;)V
    invoke-static {p1}, Lcom/google/android/apps/chrome/RevenueStats;->access$800(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/RevenueStats$3;->val$peek:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats$3;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    # getter for: Lcom/google/android/apps/chrome/RevenueStats;->mRlzHasBeenNotified:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->access$900(Lcom/google/android/apps/chrome/RevenueStats;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats$3;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    # getter for: Lcom/google/android/apps/chrome/RevenueStats;->mApplicationContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->access$000(Lcom/google/android/apps/chrome/RevenueStats;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setRlzNotified(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats$3;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    # setter for: Lcom/google/android/apps/chrome/RevenueStats;->mRlzHasBeenNotified:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/RevenueStats;->access$902(Lcom/google/android/apps/chrome/RevenueStats;Z)Z

    :cond_0
    return-void
.end method
