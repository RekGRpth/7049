.class public Lcom/google/android/apps/chrome/RevenueStats;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CLIENT_ID_PROVIDER_URI:Landroid/net/Uri;

.field private static final DEBUG_LOG:Z = false

.field private static final DEFAULT_CLIENT_ID:Ljava/lang/String; = "chrome-mobile"

.field private static final DEFAULT_RLZ:Ljava/lang/String; = ""

.field private static final LOCK:Ljava/lang/Object;

.field private static final RLZ_PROVIDER_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;

.field private static final TASK_TIMEOUT_MS:I = 0x1388

.field private static final USE_PEEK:Z

.field private static sInstance:Lcom/google/android/apps/chrome/RevenueStats;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private final mBaseRlzURI:Landroid/net/Uri;

.field private mClientId:Ljava/lang/String;

.field private final mRlz:Ljava/util/concurrent/atomic/AtomicReference;

.field private mRlzHasBeenNotified:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/google/android/apps/chrome/RevenueStats;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/RevenueStats;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/RevenueStats;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/RevenueStats;->TAG:Ljava/lang/String;

    const-string v0, "content://com.google.settings/partner"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/RevenueStats;->CLIENT_ID_PROVIDER_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.partnersetup.rlzappprovider/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/RevenueStats;->RLZ_PROVIDER_URI:Landroid/net/Uri;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/apps/chrome/RevenueStats;->USE_PEEK:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/RevenueStats;->sInstance:Lcom/google/android/apps/chrome/RevenueStats;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/RevenueStats;->LOCK:Ljava/lang/Object;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mRlz:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mApplicationContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/RevenueStats;->retrieveAndStoreClientId()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07025e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/RevenueStats;->RLZ_PROVIDER_URI:Landroid/net/Uri;

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mBaseRlzURI:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getRlzNotified()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mRlzHasBeenNotified:Z

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mRlzHasBeenNotified:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/RevenueStats;->retrieveAndStoreRlz(Z)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/RevenueStats;->registerRlzObserver()V

    return-void

    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/RevenueStats;->USE_PEEK:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/RevenueStats;->retrieveAndStoreRlz(Z)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/RevenueStats;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/RevenueStats;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mClientId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/RevenueStats;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/RevenueStats;->mClientId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/apps/chrome/RevenueStats;->nativeSetSearchClient(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/RevenueStats;->USE_PEEK:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/RevenueStats;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/RevenueStats;->retrieveAndStoreRlz(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/RevenueStats;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mBaseRlzURI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$600(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static/range {p0 .. p5}, Lcom/google/android/apps/chrome/RevenueStats;->getData(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/RevenueStats;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mRlz:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic access$800(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/apps/chrome/RevenueStats;->nativeSetRlzParameterValue(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/RevenueStats;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mRlzHasBeenNotified:Z

    return v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/RevenueStats;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/RevenueStats;->mRlzHasBeenNotified:Z

    return p1
.end method

.method private static getData(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x0

    sget-boolean v0, Lcom/google/android/apps/chrome/RevenueStats;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_5

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    move-object v0, v6

    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_2
    return-object p2

    :catch_0
    move-exception v0

    move-object v1, v6

    :goto_3
    :try_start_2
    sget-object v2, Lcom/google/android/apps/chrome/RevenueStats;->TAG:Ljava/lang/String;

    const-string v3, "Exception while retrieving"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v6

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_4
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :cond_4
    move-object p2, v0

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_3

    :cond_5
    move-object v0, v6

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/RevenueStats;
    .locals 2

    sget-object v1, Lcom/google/android/apps/chrome/RevenueStats;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/RevenueStats;->sInstance:Lcom/google/android/apps/chrome/RevenueStats;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/RevenueStats;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/RevenueStats;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/RevenueStats;->sInstance:Lcom/google/android/apps/chrome/RevenueStats;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/google/android/apps/chrome/RevenueStats;->sInstance:Lcom/google/android/apps/chrome/RevenueStats;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static native nativeSetRlzParameterValue(Ljava/lang/String;)V
.end method

.method private static native nativeSetSearchClient(Ljava/lang/String;)V
.end method

.method private registerRlzObserver()V
    .locals 4

    new-instance v0, Lcom/google/android/apps/chrome/RevenueStats$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/RevenueStats$2;-><init>(Lcom/google/android/apps/chrome/RevenueStats;Landroid/os/Handler;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/RevenueStats;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/RevenueStats;->mBaseRlzURI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method private retrieveAndStoreClientId()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/RevenueStats$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/RevenueStats$1;-><init>(Lcom/google/android/apps/chrome/RevenueStats;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/RevenueStats$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private retrieveAndStoreRlz(Z)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/RevenueStats$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/RevenueStats$3;-><init>(Lcom/google/android/apps/chrome/RevenueStats;Z)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/RevenueStats$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public static retrieveClientId(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    const/4 v5, 0x1

    const/4 v6, 0x0

    sget-boolean v0, Lcom/google/android/apps/chrome/RevenueStats;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-object v1, Lcom/google/android/apps/chrome/RevenueStats;->CLIENT_ID_PROVIDER_URI:Landroid/net/Uri;

    const-string v2, "chrome-mobile"

    new-array v3, v5, [Ljava/lang/String;

    const-string v0, "value"

    aput-object v0, v3, v6

    const-string v4, "name=?"

    new-array v5, v5, [Ljava/lang/String;

    const-string v0, "chrome_client_id"

    aput-object v0, v5, v6

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/RevenueStats;->getData(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onFirstSearch()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/RevenueStats;->mRlzHasBeenNotified:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/RevenueStats;->retrieveAndStoreRlz(Z)V

    :cond_0
    return-void
.end method
