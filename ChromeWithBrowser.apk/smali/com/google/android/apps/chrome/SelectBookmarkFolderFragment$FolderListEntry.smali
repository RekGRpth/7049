.class Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;
.super Ljava/lang/Object;


# instance fields
.field final mDepth:I

.field final mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    iput p2, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;->mDepth:I

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
