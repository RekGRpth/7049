.class Lcom/google/android/apps/chrome/SuggestionView$1;
.super Landroid/view/View;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/SuggestionView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/SuggestionView;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$000(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView$1;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/SuggestionView;->access$000(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView$1;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v2}, Lcom/google/android/apps/chrome/SuggestionView;->access$000(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$000(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    if-nez p1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/SuggestionView$1;->setClickable(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/SuggestionView$1;->setFocusable(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView$1;->setClickable(Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView$1;->setFocusable(Z)V

    goto :goto_0
.end method
