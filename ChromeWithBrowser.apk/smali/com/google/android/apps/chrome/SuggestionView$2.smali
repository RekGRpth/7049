.class Lcom/google/android/apps/chrome/SuggestionView$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/SuggestionView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/SuggestionView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/SuggestionView$2;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "text"

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$2;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;
    invoke-static {v2}, Lcom/google/android/apps/chrome/SuggestionView;->access$300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x2c

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    return-void
.end method
