.class Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;
.super Landroid/view/ViewGroup;

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field private mRelayoutRunnable:Ljava/lang/Runnable;

.field private mSuggestionIcon:Landroid/graphics/drawable/Drawable;

.field private mSuggestionIconLeft:I

.field private mSuggestionIconType:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

.field private mTextLeft:I

.field private mTextLine1:Landroid/widget/TextView;

.field private mTextLine2:Landroid/widget/TextView;

.field private mViewPositionHolder:[I

.field final synthetic this$0:Lcom/google/android/apps/chrome/SuggestionView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/SuggestionView;Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v4, -0x2

    const/high16 v0, -0x80000000

    const/high16 v3, 0x41900000

    iput-object p1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    invoke-direct {p0, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    iput v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mViewPositionHolder:[I

    new-instance v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer$1;-><init>(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mRelayoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->setClickable(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->setFocusable(Z)V

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionHeight:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/SuggestionView;->access$700(Lcom/google/android/apps/chrome/SuggestionView;)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer$2;-><init>(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/SuggestionView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionHeight:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/SuggestionView;->access$700(Lcom/google/android/apps/chrome/SuggestionView;)I

    move-result v2

    invoke-direct {v1, v4, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->TITLE_COLOR_STANDARD_FONT:I
    invoke-static {}, Lcom/google/android/apps/chrome/SuggestionView;->access$900()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionHeight:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/SuggestionView;->access$700(Lcom/google/android/apps/chrome/SuggestionView;)I

    move-result v2

    invoke-direct {v1, v4, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->URL_COLOR:I
    invoke-static {}, Lcom/google/android/apps/chrome/SuggestionView;->access$1000()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->setSuggestionIcon(Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    return-object v0
.end method

.method private getSuggestionIconLeftPosition()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mViewPositionHolder:[I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->getLocationOnScreen([I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mViewPositionHolder:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mViewPositionHolder:[I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getLocationOnScreen([I)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mViewPositionHolder:[I

    aget v0, v2, v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method private getSuggestionTextLeftPosition()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;
    invoke-static {v1}, Lcom/google/android/apps/chrome/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/chrome/SuggestionView;->access$1200(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;
    invoke-static {v2}, Lcom/google/android/apps/chrome/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v2

    const v3, 0x7f0f006e

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    # setter for: Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/SuggestionView;->access$1202(Lcom/google/android/apps/chrome/SuggestionView;Landroid/view/View;)Landroid/view/View;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/chrome/SuggestionView;->access$1200(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mViewPositionHolder:[I

    invoke-virtual {v1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mViewPositionHolder:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/chrome/SuggestionView;->access$1200(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mViewPositionHolder:[I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getLocationOnScreen([I)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mViewPositionHolder:[I

    aget v0, v2, v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method private setSuggestionIcon(Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIconType:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f020081

    sget-object v1, Lcom/google/android/apps/chrome/SuggestionView$4;->$SwitchMap$com$google$android$apps$chrome$SuggestionView$SuggestionIconType:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIcon:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIcon:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIconType:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->invalidate()V

    goto :goto_0

    :pswitch_0
    const v0, 0x7f020080

    goto :goto_1

    :pswitch_1
    const v0, 0x7f020083

    goto :goto_1

    :pswitch_2
    const v0, 0x7f020082

    goto :goto_1

    :pswitch_3
    const v0, 0x7f020084

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    if-eq p2, v1, :cond_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getMeasuredHeight()I

    move-result v2

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    :goto_1
    add-int v4, v3, v1

    if-le v4, v2, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    if-ne p2, v3, :cond_3

    :cond_1
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    const/4 v1, 0x0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1

    :cond_3
    sub-int v0, v2, v1

    goto :goto_2

    :cond_4
    sub-int v0, v2, v3

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    if-ne p2, v1, :cond_1

    add-int/2addr v0, v3

    goto :goto_2
.end method

.method public invalidate()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getSuggestionTextLeftPosition()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mRelayoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mRelayoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x14

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/view/ViewGroup;->invalidate()V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1200(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1200(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/LocationBar;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1200(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1200(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/LocationBar;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getSuggestionIconLeftPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    iget v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    const/high16 v2, 0x40000000

    div-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1200(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;
    invoke-static {v1}, Lcom/google/android/apps/chrome/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v1

    const v2, 0x7f0f006e

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/SuggestionView;->access$1202(Lcom/google/android/apps/chrome/SuggestionView;Landroid/view/View;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1200(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    const v2, 0x7f0f006b

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1102(Lcom/google/android/apps/chrome/SuggestionView;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getSuggestionTextLeftPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    invoke-virtual {v0, v1, p3, p4, p5}, Landroid/widget/TextView;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    invoke-virtual {v0, v1, p3, p4, p5}, Landroid/widget/TextView;->layout(IIII)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getSuggestionIconLeftPosition()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    if-eq v1, v0, :cond_2

    iget v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;
    invoke-static {v1}, Lcom/google/android/apps/chrome/SuggestionView;->access$1400(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->invalidate()V

    :cond_2
    iput v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    return-void
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 5

    const/4 v0, 0x1

    const/high16 v4, -0x80000000

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v2

    if-ne p1, v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getSuggestionIconLeftPosition()I

    move-result v3

    if-eq v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    if-eq v2, v4, :cond_3

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mRelayoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mRelayoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x14

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void

    :cond_2
    iget v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getSuggestionTextLeftPosition()I

    move-result v3

    if-eq v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    if-ne v2, v4, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5

    const/high16 v4, -0x80000000

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-static {p1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionHeight:I
    invoke-static {v3}, Lcom/google/android/apps/chrome/SuggestionView;->access$700(Lcom/google/android/apps/chrome/SuggestionView;)I

    move-result v3

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->measure(II)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    if-eq v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-static {p1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionHeight:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/SuggestionView;->access$700(Lcom/google/android/apps/chrome/SuggestionView;)I

    move-result v2

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    :cond_1
    return-void
.end method
