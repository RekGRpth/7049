.class public Lcom/google/android/apps/chrome/SuggestionView;
.super Landroid/view/ViewGroup;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DIVIDER_HEIGHT_PX:I = 0x1

.field private static final RELAYOUT_DELAY_MS:J = 0x14L

.field private static final SEARCH_SUGGEST_MATCH_COLOR:I

.field private static final SUGGESTION_TEXT_SIZE_SP:I = 0x12

.field private static final TITLE_COLOR_STANDARD_FONT:I

.field private static final URL_COLOR:I


# instance fields
.field private mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

.field private mDividerDimensions:Landroid/graphics/Rect;

.field private mDividerPaint:Landroid/graphics/Paint;

.field private mDividerSidePadding:I

.field private mIsLastSuggestion:Z

.field private mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

.field private mNavigationButton:Landroid/widget/ImageView;

.field private mRefineIcon:Landroid/graphics/drawable/Drawable;

.field private mRefineIconType:Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;

.field private mRefineView:Landroid/view/View;

.field private mRefineWidth:I

.field private mSelectionHandler:Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;

.field private mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

.field private mSuggestionHeight:I

.field private mSuggestionItem:Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

.field private mUrlBar:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    const/16 v3, 0xa1

    const/16 v2, 0x33

    const-class v0, Lcom/google/android/apps/chrome/SuggestionView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/SuggestionView;->$assertionsDisabled:Z

    invoke-static {v2, v2, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/SuggestionView;->TITLE_COLOR_STANDARD_FONT:I

    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/SuggestionView;->SEARCH_SUGGEST_MATCH_COLOR:I

    const/16 v0, 0x99

    invoke-static {v1, v0, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/SuggestionView;->URL_COLOR:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mDividerDimensions:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mDividerPaint:Landroid/graphics/Paint;

    iput-object p2, p0, Lcom/google/android/apps/chrome/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionHeight:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mDividerPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v1, v4, [I

    const v2, 0x101030e

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;-><init>(Lcom/google/android/apps/chrome/SuggestionView;Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/chrome/SuggestionView$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/SuggestionView$1;-><init>(Lcom/google/android/apps/chrome/SuggestionView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42400000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineWidth:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41000000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mDividerSidePadding:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$1000()I
    .locals 1

    sget v0, Lcom/google/android/apps/chrome/SuggestionView;->URL_COLOR:I

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/chrome/SuggestionView;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/SuggestionView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/apps/chrome/SuggestionView;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mUrlBar:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/LocationBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/OmniboxSuggestion;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSelectionHandler:Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/SuggestionView;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionHeight:I

    return v0
.end method

.method static synthetic access$900()I
    .locals 1

    sget v0, Lcom/google/android/apps/chrome/SuggestionView;->TITLE_COLOR_STANDARD_FONT:I

    return v0
.end method

.method private setRefinable(Z)V
    .locals 2

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;->REFINE:Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView;->setRefineIcon(Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/chrome/SuggestionView$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/SuggestionView$3;-><init>(Lcom/google/android/apps/chrome/SuggestionView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setRefineContentDescription()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineIconType:Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;

    sget-object v1, Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;->REFINE:Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;

    if-ne v0, v1, :cond_0

    const v0, 0x7f07022a

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const v0, 0x7f07022b

    goto :goto_0
.end method

.method private setRefineIcon(Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineIconType:Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;->FIND_IN_PAGE:Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;

    if-ne p1, v0, :cond_1

    const v0, 0x7f02002c

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineIconType:Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/SuggestionView;->setRefineContentDescription()V

    goto :goto_0

    :cond_1
    const v0, 0x7f02002b

    goto :goto_1
.end method

.method private setSuggestedQuery(Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;ZZZ)V
    .locals 5

    const/4 v2, -0x1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getMatchedQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getSuggestion()Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDescription()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_2

    sget-boolean v0, Lcom/google/android/apps/chrome/SuggestionView;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Invalid suggestion sent with no displayable text"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    :cond_2
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v4

    if-eqz p4, :cond_3

    move v1, v2

    :goto_1
    if-ne v1, v2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v0

    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    :goto_2
    return-void

    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    move v1, v0

    goto :goto_1

    :cond_4
    if-eqz p3, :cond_5

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    :goto_3
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    const/16 v3, 0x21

    invoke-interface {v4, v0, v1, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v0

    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto :goto_2

    :cond_5
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    sget v2, Lcom/google/android/apps/chrome/SuggestionView;->SEARCH_SUGGEST_MATCH_COLOR:I

    invoke-direct {v0, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    goto :goto_3
.end method

.method private setUrlText(Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;)Z
    .locals 7

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getMatchedQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getSuggestion()Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/LocationBar;->simplifyUrlForDisplay(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    invoke-virtual {v3, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-static {v3}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v3

    if-ltz v4, :cond_1

    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v4

    const/16 v6, 0x21

    invoke-interface {v3, v5, v4, v2, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v2

    sget-object v5, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    if-ltz v4, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mIsLastSuggestion:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mDividerDimensions:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mDividerPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public init(Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;Z)V
    .locals 7

    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionItem:Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mIsLastSuggestion:Z

    if-ne v0, p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/SuggestionView;->mIsLastSuggestion:Z

    iput-object p1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionItem:Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getSuggestion()Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    iput-object p2, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSelectionHandler:Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->getMatchedQuery()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    sget-object v0, Lcom/google/android/apps/chrome/SuggestionView$4;->$SwitchMap$com$google$android$apps$chrome$OmniboxSuggestion$Type:[I

    iget-object v3, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getType()Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/SuggestionView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Suggestion type ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getType()Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is not handled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->isStarred()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    sget-object v3, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->BOOKMARK:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    # invokes: Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->setSuggestionIcon(Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;)V
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->access$100(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    move v3, v2

    :goto_2
    if-eqz v3, :cond_6

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/SuggestionView;->setUrlText(Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;)Z

    move-result v0

    :goto_3
    invoke-direct {p0, p1, v2, v3, v0}, Lcom/google/android/apps/chrome/SuggestionView;->setSuggestedQuery(Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;ZZZ)V

    if-nez v4, :cond_2

    move v1, v2

    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/SuggestionView;->setRefinable(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getType()Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->HISTORY_URL:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    sget-object v3, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->HISTORY:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    # invokes: Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->setSuggestionIcon(Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;)V
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->access$100(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    sget-object v3, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->GLOBE:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    # invokes: Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->setSuggestionIcon(Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;)V
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->access$100(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;)V

    goto :goto_1

    :cond_5
    move v3, v1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    move v0, v1

    goto :goto_3

    :pswitch_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getType()Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->VOICE_SUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    if-ne v0, v5, :cond_8

    sget-object v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->VOICE:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    :goto_4
    # invokes: Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->setSuggestionIcon(Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;)V
    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->access$100(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;)V

    if-nez v4, :cond_9

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/SuggestionView;->setRefinable(Z)V

    :cond_7
    :goto_5
    invoke-direct {p0, p1, v1, v1, v1}, Lcom/google/android/apps/chrome/SuggestionView;->setSuggestedQuery(Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;ZZZ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_8
    sget-object v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->MAGNIFIER:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getType()Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->SEARCH_WHAT_YOU_TYPED:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    if-ne v0, v2, :cond_7

    sget-object v0, Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;->FIND_IN_PAGE:Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/SuggestionView;->setRefineIcon(Lcom/google/android/apps/chrome/SuggestionView$RefineIconType;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/chrome/SuggestionView$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/SuggestionView$2;-><init>(Lcom/google/android/apps/chrome/SuggestionView;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public invalidate()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->invalidate()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->invalidate()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->getMeasuredWidth()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->getMeasuredWidth()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineWidth:I

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5

    const/high16 v3, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/SuggestionView;->setMeasuredDimension(II)V

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    iget v2, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineWidth:I

    sub-int/2addr v0, v2

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v2, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionHeight:I

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->measure(II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getMeasuredWidth()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionContentsContainer;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    iget v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineWidth:I

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestionHeight:I

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mDividerDimensions:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mDividerSidePadding:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->getMeasuredWidth()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/chrome/SuggestionView;->mDividerSidePadding:I

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public setSelected(Z)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setSelected(Z)V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SuggestionView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSelectionHandler:Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;->onSetUrlToSuggestion(Lcom/google/android/apps/chrome/OmniboxSuggestion;)V

    :cond_0
    return-void
.end method
