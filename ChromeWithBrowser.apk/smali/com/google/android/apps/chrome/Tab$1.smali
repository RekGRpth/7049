.class Lcom/google/android/apps/chrome/Tab$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Tab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->canUpdateHistoryThumbnail()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$000(Lcom/google/android/apps/chrome/Tab;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->shouldUpdateThumbnail()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$100(Lcom/google/android/apps/chrome/Tab;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getHeight()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getWidth()I

    move-result v1

    aput v1, v0, v2

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getHeight()I

    move-result v1

    aput v1, v0, v3

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mThumbnailWidth:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->access$200(Lcom/google/android/apps/chrome/Tab;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mThumbnailHeight:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/Tab;->access$300(Lcom/google/android/apps/chrome/Tab;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->scaleToFitTargetSize([III)F

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/Tab;->access$400(Lcom/google/android/apps/chrome/Tab;)Lorg/chromium/content/browser/ContentView;

    move-result-object v2

    const/4 v3, 0x0

    aget v3, v0, v3

    const/4 v4, 0x1

    aget v0, v0, v4

    invoke-virtual {v2, v3, v0}, Lorg/chromium/content/browser/ContentView;->getBitmap(II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$1;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->updateHistoryThumbnail(Landroid/graphics/Bitmap;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/Tab;->access$600(Lcom/google/android/apps/chrome/Tab;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :catch_0
    move-exception v0

    # getter for: Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/Tab;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OutOfMemoryError thrown while trying to fetch a tab bitmap."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/16 v0, 0x31

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    move-object v0, v1

    goto :goto_1
.end method
