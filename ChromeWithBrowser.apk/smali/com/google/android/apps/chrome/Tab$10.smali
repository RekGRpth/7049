.class Lcom/google/android/apps/chrome/Tab$10;
.super Landroid/os/AsyncTask;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Tab;

.field final synthetic val$printJobId:Ljava/lang/String;

.field final synthetic val$snapshotId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/Tab$10;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab$10;->this$0:Lcom/google/android/apps/chrome/Tab;

    iput-object p2, p0, Lcom/google/android/apps/chrome/Tab$10;->val$snapshotId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/Tab$10;->val$printJobId:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$10;->val$snapshotId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$10;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$10;->val$snapshotId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getDocumentsWithSnapshotId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/Tab$10;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$10;->val$printJobId:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$10;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$10;->val$printJobId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getDocumentsWithJobId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/Tab$10;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V
    .locals 4

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$10;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$10;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->setSnapshotId(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getJobId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getSnapshotViewableState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->READY:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$10;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->openDownloadedFileAsNewTaskActivity(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/Tab$10;->onPostExecute(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V

    return-void
.end method
