.class Lcom/google/android/apps/chrome/Tab$11;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Tab;

.field final synthetic val$authHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Tab;Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab$11;->this$0:Lcom/google/android/apps/chrome/Tab;

    iput-object p2, p0, Lcom/google/android/apps/chrome/Tab$11;->val$authHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public tokenAvailable(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    # getter for: Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/Tab;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Received NULL token from account manager."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string p1, ""

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$11;->val$authHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    const-string v1, "fw-cookie"

    invoke-virtual {v0, v1, p1}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->proceed(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
