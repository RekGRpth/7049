.class Lcom/google/android/apps/chrome/Tab$4;
.super Lorg/chromium/content/browser/ContentViewClient;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Tab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewClient;-><init>()V

    return-void
.end method

.method private showSadTab()V
    .locals 9

    const/4 v8, -0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$400(Lcom/google/android/apps/chrome/Tab;)Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->access$1600(Lcom/google/android/apps/chrome/Tab;)I

    move-result v1

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/Tab;->nativeGetLocalizedString(II)Ljava/lang/String;
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/Tab;->access$3100(Lcom/google/android/apps/chrome/Tab;II)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/Tab;->access$1600(Lcom/google/android/apps/chrome/Tab;)I

    move-result v2

    const/4 v3, 0x1

    # invokes: Lcom/google/android/apps/chrome/Tab;->nativeGetLocalizedString(II)Ljava/lang/String;
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/Tab;->access$3100(Lcom/google/android/apps/chrome/Tab;II)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I
    invoke-static {v3}, Lcom/google/android/apps/chrome/Tab;->access$1600(Lcom/google/android/apps/chrome/Tab;)I

    move-result v3

    const/4 v4, 0x4

    # invokes: Lcom/google/android/apps/chrome/Tab;->nativeGetLocalizedString(II)Ljava/lang/String;
    invoke-static {v0, v3, v4}, Lcom/google/android/apps/chrome/Tab;->access$3100(Lcom/google/android/apps/chrome/Tab;II)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I
    invoke-static {v3}, Lcom/google/android/apps/chrome/Tab;->access$1600(Lcom/google/android/apps/chrome/Tab;)I

    move-result v3

    const/4 v5, 0x2

    # invokes: Lcom/google/android/apps/chrome/Tab;->nativeGetLocalizedString(II)Ljava/lang/String;
    invoke-static {v0, v3, v5}, Lcom/google/android/apps/chrome/Tab;->access$3100(Lcom/google/android/apps/chrome/Tab;II)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    iget-object v5, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I
    invoke-static {v5}, Lcom/google/android/apps/chrome/Tab;->access$1600(Lcom/google/android/apps/chrome/Tab;)I

    move-result v5

    const/4 v6, 0x3

    # invokes: Lcom/google/android/apps/chrome/Tab;->nativeGetLocalizedString(II)Ljava/lang/String;
    invoke-static {v3, v5, v6}, Lcom/google/android/apps/chrome/Tab;->access$3100(Lcom/google/android/apps/chrome/Tab;II)Ljava/lang/String;

    move-result-object v5

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v6, "$1"

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    add-int/lit8 v6, v0, 0x2

    invoke-virtual {v3, v0, v6, v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v0

    new-instance v6, Lcom/google/android/apps/chrome/Tab$4$1;

    invoke-direct {v6, p0}, Lcom/google/android/apps/chrome/Tab$4$1;-><init>(Lcom/google/android/apps/chrome/Tab$4;)V

    const/16 v7, 0x21

    invoke-virtual {v3, v6, v0, v5, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$3300(Lcom/google/android/apps/chrome/Tab;)Landroid/content/Context;

    move-result-object v0

    new-instance v5, Lcom/google/android/apps/chrome/Tab$4$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/chrome/Tab$4$2;-><init>(Lcom/google/android/apps/chrome/Tab$4;)V

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/SadTabViewFactory;->createSadTabView(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    # setter for: Lcom/google/android/apps/chrome/Tab;->mSadTabView:Landroid/view/View;
    invoke-static {v6, v0}, Lcom/google/android/apps/chrome/Tab;->access$3202(Lcom/google/android/apps/chrome/Tab;Landroid/view/View;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$400(Lcom/google/android/apps/chrome/Tab;)Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mSadTabView:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->access$3200(Lcom/google/android/apps/chrome/Tab;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v8, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPositionsForTabToNonFullscreen()V

    :cond_2
    return-void
.end method


# virtual methods
.method public bridge synthetic getSelectActionModeCallback(Landroid/content/Context;Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)Landroid/view/ActionMode$Callback;
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/Tab$4;->getSelectActionModeCallback(Landroid/content/Context;Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)Lorg/chromium/content/browser/SelectActionModeCallback;

    move-result-object v0

    return-object v0
.end method

.method public getSelectActionModeCallback(Landroid/content/Context;Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)Lorg/chromium/content/browser/SelectActionModeCallback;
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/ChromeSelectActionModeCallback;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/ChromeSelectActionModeCallback;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)V

    return-object v0
.end method

.method public onBackgroundColorChanged(I)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mId:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/Tab;->access$1800(Lcom/google/android/apps/chrome/Tab;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "color"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0x3f

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method public onContextualActionBarHidden()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/chrome/Tab;->notifyContextualActionBarStateChanged(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Tab;->access$3700(Lcom/google/android/apps/chrome/Tab;Z)V

    return-void
.end method

.method public onContextualActionBarShown()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/apps/chrome/Tab;->notifyContextualActionBarStateChanged(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Tab;->access$3700(Lcom/google/android/apps/chrome/Tab;Z)V

    return-void
.end method

.method public onImeEvent()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/Tab;->mAppAssociatedWith:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Tab;->access$3802(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method public onImeStateChangeRequested(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setForceNonFullscreenViewport(Z)V

    goto :goto_0
.end method

.method public onOffsetsForFullscreenChanged(FF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->isShowingSadTab()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$3400(Lcom/google/android/apps/chrome/Tab;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPositionsForTabToNonFullscreen()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # setter for: Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenTopControlsOffsetY:F
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/Tab;->access$3502(Lcom/google/android/apps/chrome/Tab;F)F

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # setter for: Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenContentOffsetY:F
    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/Tab;->access$3602(Lcom/google/android/apps/chrome/Tab;F)F

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPositionsForTab(FF)V

    goto :goto_1
.end method

.method public onTabCrash()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mIsHidden:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2600(Lcom/google/android/apps/chrome/Tab;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ActivityStatus;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/Tab;->mTabCrashedInBackground:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Tab;->access$2702(Lcom/google/android/apps/chrome/Tab;Z)Z

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/Tab;->mIsLoading:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Tab;->access$1202(Lcom/google/android/apps/chrome/Tab;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findOrCreateIfNeeded(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/ChromeActivity;Ljava/lang/String;)Lcom/google/android/apps/chrome/NewTabPageToolbar;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->onCrash()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->logTabCrashed()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2800(Lcom/google/android/apps/chrome/Tab;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->handleTabCrash()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2900(Lcom/google/android/apps/chrome/Tab;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->notifyTabCrashed()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$3000(Lcom/google/android/apps/chrome/Tab;)V

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab$4;->showSadTab()V

    goto :goto_0
.end method

.method public onUpdateTitle(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->updateTitle(Ljava/lang/String;)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/Tab;->access$2500(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$4;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->notifyPageTitleChanged()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$1100(Lcom/google/android/apps/chrome/Tab;)V

    :cond_0
    return-void
.end method
