.class public Lcom/google/android/apps/chrome/Tab;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/TabThumbnailProvider;
.implements Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BOOKMARKS_URL:Ljava/lang/String; = "chrome://newtab/#bookmarks"

.field public static CHANNEL_NAME_OVERRIDE_FOR_TEST:Ljava/lang/String; = null

.field public static final CHROME_SCHEME:Ljava/lang/String; = "chrome://"

.field private static final CONSIDERED_READY_LOAD_PERCENTAGE:I = 0x64

.field public static final CRASH_REASON_URL:Ljava/lang/String; = "https://support.google.com/chrome/?p=e_awsnap"

.field public static final DEFAULT_PAGE_LOAD:I = 0x1

.field private static final FAVICON_DIMENSION:I = 0x10

.field public static final FIND_SELECTION_ACTION_ACTIVATE_SELECTION:I = 0x2

.field public static final FIND_SELECTION_ACTION_CLEAR_SELECTION:I = 0x1

.field public static final FIND_SELECTION_ACTION_KEEP_SELECTION:I = 0x0

.field public static final FULL_PRERENDERED_PAGE_LOAD:I = 0x3

.field public static final HTTPS_SCHEME:Ljava/lang/String; = "https://"

.field public static final HTTP_SCHEME:Ljava/lang/String; = "http://"

.field private static final IDS_SAD_TAB_HELP_LINK:I = 0x3

.field private static final IDS_SAD_TAB_HELP_MESSAGE:I = 0x2

.field private static final IDS_SAD_TAB_MESSAGE:I = 0x1

.field private static final IDS_SAD_TAB_RELOAD_LABEL:I = 0x4

.field private static final IDS_SAD_TAB_TITLE:I = 0x0

.field public static final INVALID_RENDER_PROCESS_PID:I = -0x80000000

.field public static final INVALID_TAB_ID:I = -0x1

.field static final KEY_CHECKER:J = 0x0L

.field private static final MIN_SPDYPROXY_AUTH_REQUEST_INTERVAL_MS:J = 0x1f4L

.field private static final MIN_SPDYPROXY_TOKEN_INVALIDATION_INTERVAL_MS:J = 0x36ee80L

.field public static final NO_PARENT_ID:I = -0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NTP_TAB_ID:I = -0x2

.field public static final NTP_URL:Ljava/lang/String; = "chrome://newtab/"

.field public static final PAGE_LOAD_FAILED:I = 0x0

.field public static final PARTIAL_PRERENDERED_PAGE_LOAD:I = 0x2

.field private static final PRINT_JOB_PREFIX:Ljava/lang/String; = "chrome://printjob/"

.field public static final SAVED_STATE_FILE_PREFIX:Ljava/lang/String; = "tab"

.field public static final SAVED_STATE_FILE_PREFIX_INCOGNITO:Ljava/lang/String; = "cryptonito"

.field private static final SAVED_STATE_VERSION:I = 0x2

.field private static final SNAPSHOT_PREFIX:Ljava/lang/String; = "chrome://snapshot/"

.field private static final TAG:Ljava/lang/String;

.field private static final THUMBNAIL_CAPTURE_DELAY:I = 0x1f4

.field private static final THUMBNAIL_H_PHONE:I = 0x48

.field private static final THUMBNAIL_H_TABLET:I = 0xa0

.field private static final THUMBNAIL_W_PHONE:I = 0x6c

.field private static final THUMBNAIL_W_TABLET:I = 0xe6

.field public static final WELCOME_URL:Ljava/lang/String; = "chrome://welcome/"

.field private static final idCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static mKey:Ljava/security/Key;

.field private static sIncognitoKeyGenerator:Ljava/util/concurrent/FutureTask;


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private mAppAssociatedWith:Ljava/lang/String;

.field private mBaseUrl:Ljava/lang/String;

.field private mCachedFavicon:Landroid/graphics/Bitmap;

.field private mCachedFaviconUrl:Ljava/lang/String;

.field private mChromeWebContentsDelegateAndroid:Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;

.field private final mCloseContentsRunnable:Ljava/lang/Runnable;

.field private mContentView:Lorg/chromium/content/browser/ContentView;

.field private mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

.field private final mContext:Landroid/content/Context;

.field private mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

.field private mDownloadListener:Lcom/google/android/apps/chrome/ChromeDownloadListener;

.field private mFindMatchRectsListener:Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;

.field private mFindResultListener:Lcom/google/android/apps/chrome/Tab$FindResultListener;

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

.field private mHandler:Landroid/os/Handler;

.field private final mId:I

.field private final mIncognito:Z

.field private mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

.field private mInterceptNavigationDelegate:Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;

.field private mIsClosing:Z

.field private mIsHidden:Z

.field private mIsLoading:Z

.field private mIsTabStateDirty:Z

.field private mLastLoadedUrl:Ljava/lang/String;

.field private mLastShownTimestamp:J

.field private mLaunchType:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

.field private mNativeTabAndroidImpl:I

.field private final mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

.field private mOpenerTabId:I

.field private mParentId:I

.field private mParentIsIncognito:Z

.field private mPendingLoadUrl:Ljava/lang/String;

.field private mPopupInfoBar:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

.field private mPreviousFullscreenContentOffsetY:F

.field private mPreviousFullscreenTopControlsOffsetY:F

.field private mSadTabView:Landroid/view/View;

.field private mSavedState:[B

.field private mSavedStateVersion:I

.field private mSnapshotDownloadErrorInfoBar:Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

.field private mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

.field private mSnapshotId:Ljava/lang/String;

.field private mSpdyProxyAuthRequestTimestamp:J

.field private mSpdyProxyAuthTokenInvalidationTimestamp:J

.field private mTabCrashedInBackground:Z

.field private mThumbnailHeight:I

.field private final mThumbnailRunnable:Ljava/lang/Runnable;

.field private mThumbnailWidth:I

.field private mTitle:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;

.field private mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

.field private mWelcomePageHelper:Lcom/google/android/apps/chrome/WelcomePageHelper;

.field private mWindowId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/Tab;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/Tab;->idCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/Tab;->mKey:Ljava/security/Key;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/gfx/NativeWindow;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;I)V
    .locals 6

    const/4 v0, 0x1

    const/4 v5, -0x1

    const/high16 v4, 0x7fc00000

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsHidden:Z

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/Tab;->mIsClosing:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsTabStateDirty:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/Tab;->mLastShownTimestamp:J

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/Tab;->mIsLoading:Z

    iput v5, p0, Lcom/google/android/apps/chrome/Tab;->mParentId:I

    iput-object v3, p0, Lcom/google/android/apps/chrome/Tab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;

    iput-object v3, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotId:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    new-instance v0, Lcom/google/android/apps/chrome/Tab$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Tab$1;-><init>(Lcom/google/android/apps/chrome/Tab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/chrome/Tab$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Tab$2;-><init>(Lcom/google/android/apps/chrome/Tab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mCloseContentsRunnable:Ljava/lang/Runnable;

    iput v4, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenTopControlsOffsetY:F

    iput v4, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenContentOffsetY:F

    if-ne p1, v5, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/Tab;->generateNextId()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    :goto_0
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ChromeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iput-object p4, p0, Lcom/google/android/apps/chrome/Tab;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    iput-boolean p3, p0, Lcom/google/android/apps/chrome/Tab;->mIncognito:Z

    iput-object p5, p0, Lcom/google/android/apps/chrome/Tab;->mLaunchType:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    iput p6, p0, Lcom/google/android/apps/chrome/Tab;->mParentId:I

    iput p6, p0, Lcom/google/android/apps/chrome/Tab;->mOpenerTabId:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mHandler:Landroid/os/Handler;

    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    add-int/lit8 v0, p1, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->incrementIdCounterTo(I)V

    goto :goto_0
.end method

.method private constructor <init>(IZ)V
    .locals 5

    const/4 v0, 0x1

    const/high16 v4, 0x7fc00000

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsHidden:Z

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/Tab;->mIsClosing:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsTabStateDirty:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/Tab;->mLastShownTimestamp:J

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/Tab;->mIsLoading:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/Tab;->mParentId:I

    iput-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;

    iput-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotId:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    new-instance v0, Lcom/google/android/apps/chrome/Tab$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Tab$1;-><init>(Lcom/google/android/apps/chrome/Tab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/chrome/Tab$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Tab$2;-><init>(Lcom/google/android/apps/chrome/Tab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mCloseContentsRunnable:Ljava/lang/Runnable;

    iput v4, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenTopControlsOffsetY:F

    iput v4, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenContentOffsetY:F

    iput p1, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/Tab;->mIncognito:Z

    iput-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mContext:Landroid/content/Context;

    iput-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iput-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/gfx/NativeWindow;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;I)V
    .locals 7

    const/4 v1, -0x1

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/Tab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/gfx/NativeWindow;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;I)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/Tab;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->canUpdateHistoryThumbnail()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/Tab;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->shouldUpdateThumbnail()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/Tab;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->updateTitle()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->notifyPageTitleChanged()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/Tab;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsLoading:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/google/android/apps/chrome/Tab;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/Tab;->mIsLoading:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->notifyLoadProgress()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/Tab$FindResultListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mFindResultListener:Lcom/google/android/apps/chrome/Tab$FindResultListener;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/Tab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    return v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/Tab;IIIILandroid/graphics/Rect;Z)Z
    .locals 1

    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/chrome/Tab;->nativeAddNewContents(IIIILandroid/graphics/Rect;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/Tab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    return v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/chrome/Tab;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mCloseContentsRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/Tab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailWidth:I

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/Tab;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/google/android/apps/chrome/Tab;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/Tab;->mIsTabStateDirty:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->updateNTPToolbarUrl(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->notifyPageUrlChanged()V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->updateTitle(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/chrome/Tab;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsHidden:Z

    return v0
.end method

.method static synthetic access$2702(Lcom/google/android/apps/chrome/Tab;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/Tab;->mTabCrashedInBackground:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->logTabCrashed()V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->handleTabCrash()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/Tab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailHeight:I

    return v0
.end method

.method static synthetic access$3000(Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->notifyTabCrashed()V

    return-void
.end method

.method static synthetic access$3100(Lcom/google/android/apps/chrome/Tab;II)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/Tab;->nativeGetLocalizedString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3200(Lcom/google/android/apps/chrome/Tab;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSadTabView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/google/android/apps/chrome/Tab;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mSadTabView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/google/android/apps/chrome/Tab;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/google/android/apps/chrome/Tab;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->isShowingSadTab()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3502(Lcom/google/android/apps/chrome/Tab;F)F
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenTopControlsOffsetY:F

    return p1
.end method

.method static synthetic access$3602(Lcom/google/android/apps/chrome/Tab;F)F
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenContentOffsetY:F

    return p1
.end method

.method static synthetic access$3700(Lcom/google/android/apps/chrome/Tab;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->notifyContextualActionBarStateChanged(Z)V

    return-void
.end method

.method static synthetic access$3802(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mAppAssociatedWith:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3900(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->didFinishPageLoad(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/Tab;)Lorg/chromium/content/browser/ContentView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/google/android/apps/chrome/Tab;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->notifyPageLoadFailed(I)V

    return-void
.end method

.method static synthetic access$4102(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mBaseUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4200(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->didStartPageLoad(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4402(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;)Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    return-object p1
.end method

.method static synthetic access$4502(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/infobar/MessageInfoBar;)Lcom/google/android/apps/chrome/infobar/MessageInfoBar;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadErrorInfoBar:Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

    return-object p1
.end method

.method static synthetic access$4600(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->handleSnapshotOrPrintJobUrl(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4700(Lcom/google/android/apps/chrome/Tab;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIncognito:Z

    return v0
.end method

.method static synthetic access$4800(Lcom/google/android/apps/chrome/Tab;)Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mChromeWebContentsDelegateAndroid:Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/Tab;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->updateHistoryThumbnail(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInterceptNavigationDelegate:Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/Tab;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->notifyPageLoad(I)V

    return-void
.end method

.method private addShortcutToBookmark(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;III)V
    .locals 7

    iget-object v6, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-static/range {v0 .. v6}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getShortcutToBookmark(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;IIILandroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Tab;->installBookmarkShortcut(Landroid/content/Intent;)V

    return-void
.end method

.method private canUpdateHistoryThumbnail()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "chrome://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mLastLoadedUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/utilities/URLUtilities;->samePage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createChromeWebContentsDelegateAndroid()Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/Tab$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Tab$3;-><init>(Lcom/google/android/apps/chrome/Tab;)V

    return-object v0
.end method

.method private createContentViewClient()Lorg/chromium/content/browser/ContentViewClient;
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/Tab$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Tab$4;-><init>(Lcom/google/android/apps/chrome/Tab;)V

    return-object v0
.end method

.method public static createMockTab(IZ)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/Tab;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/Tab;-><init>(IZ)V

    return-object v0
.end method

.method private createWebContentsObserverAndroid()Lorg/chromium/content/browser/WebContentsObserverAndroid;
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/Tab$5;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/Tab$5;-><init>(Lcom/google/android/apps/chrome/Tab;Lorg/chromium/content/browser/ContentViewCore;)V

    return-object v0
.end method

.method public static deleteStateFile(ILandroid/app/Activity;Z)V
    .locals 1

    invoke-static {p2, p0}, Lcom/google/android/apps/chrome/Tab;->getFilename(ZI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->deleteFile(Ljava/lang/String;)Z

    return-void
.end method

.method private destroyContentView(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mTitle:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mTitle:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->unregisterForContextMenu(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/Tab;->nativeDestroyWebContents(IZ)V

    goto :goto_0
.end method

.method private destroyInternal(Z)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/high16 v1, 0x7fc00000

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->destroyContentView(Z)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mWelcomePageHelper:Lcom/google/android/apps/chrome/WelcomePageHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mWelcomePageHelper:Lcom/google/android/apps/chrome/WelcomePageHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->destroy()V

    iput-object v3, p0, Lcom/google/android/apps/chrome/Tab;->mWelcomePageHelper:Lcom/google/android/apps/chrome/WelcomePageHelper;

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeDestroy(I)V

    iput v2, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->destroy()V

    iput-object v3, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    :cond_2
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/Tab;->mTabCrashedInBackground:Z

    iput v1, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenTopControlsOffsetY:F

    iput v1, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenContentOffsetY:F

    return-void
.end method

.method private didFinishPageLoad(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsLoading:Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mLastLoadedUrl:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mPendingLoadUrl:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mPendingLoadUrl:Ljava/lang/String;

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsTabStateDirty:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->updateTitle()Z

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->notifyPageLoad(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/Tab;->triggerIncognitoKeyGeneration()V

    :cond_1
    return-void
.end method

.method private didStartPageLoad(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsLoading:Z

    iput-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mLastLoadedUrl:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mUrl:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mBaseUrl:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->removeSadTabIfNeeded()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->onPageStarted(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->updateTitle()Z

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeSaveTabIdForSessionSync(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->stopCurrentAccessibilityNotifications()V

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->notifyPageLoad(I)V

    return-void
.end method

.method private editBookmark(JZ)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const-class v2, Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "folder"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/IntentHandler;->startActivityForTrustedIntent(Landroid/content/Intent;Landroid/content/Context;)V

    return-void
.end method

.method private static generateNextId()I
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/Tab;->idCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method

.method private static getCipher(I)Ljavax/crypto/Cipher;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    const-string v0, "AES"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/Tab;->getKey()Ljava/security/Key;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v2, "cipher"

    const-string v3, "Error in the cipher alogrithm"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "cipher"

    const-string v3, "Wrong key to use in cipher"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v2, "cipher"

    const-string v3, "Error in creating cipher instance"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0
.end method

.method public static getFilename(ZI)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cryptonito"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tab"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getKey()Ljava/security/Key;
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/Tab;->mKey:Ljava/security/Key;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/Tab;->triggerIncognitoKeyGeneration()V

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/Tab;->sIncognitoKeyGenerator:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/Key;

    sput-object v0, Lcom/google/android/apps/chrome/Tab;->mKey:Ljava/security/Key;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/Tab;->sIncognitoKeyGenerator:Ljava/util/concurrent/FutureTask;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/Tab;->mKey:Ljava/security/Key;

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private getScaledFavicon(II)Landroid/graphics/Bitmap;
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeGetFaviconBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x1

    invoke-static {v0, p1, p2, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private handleSnapshotOrPrintJobUrl(Ljava/lang/String;)Z
    .locals 5

    const/4 v1, 0x0

    const/16 v4, 0x12

    const/4 v2, 0x0

    const-string v0, "chrome://snapshot/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v3, "chrome://printjob/"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    move v0, v2

    :goto_1
    return v0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v3, Lcom/google/android/apps/chrome/Tab$10;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/chrome/Tab$10;-><init>(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;Ljava/lang/String;)V

    new-array v0, v2, [Ljava/lang/Void;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/Tab$10;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    goto :goto_1
.end method

.method private handleTabCrash()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createFindAndUploadLastCrashIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->crashUploadAttempt()V

    return-void
.end method

.method static incrementIdCounterTo(I)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/Tab;->idCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    sub-int v0, p0, v0

    if-gtz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/google/android/apps/chrome/Tab;->idCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    goto :goto_0
.end method

.method private initContentView(Landroid/app/Activity;I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    const/4 v1, 0x1

    invoke-static {p1, p2, v0, v1}, Lorg/chromium/content/browser/ContentView;->newInstance(Landroid/content/Context;ILorg/chromium/ui/gfx/NativeWindow;I)Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->setContentViewClient(Lorg/chromium/content/browser/ContentViewClient;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->registerForContextMenu(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mDownloadListener:Lcom/google/android/apps/chrome/ChromeDownloadListener;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->setDownloadDelegate(Lorg/chromium/content/browser/ContentViewDownloadDelegate;)V

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mChromeWebContentsDelegateAndroid:Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;

    iget v3, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/Tab;->nativeInitWebContents(ILorg/chromium/content/browser/ContentViewCore;Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;I)V

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    iget v1, p0, Lcom/google/android/apps/chrome/Tab;->mWindowId:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/Tab;->nativeSetWindowId(II)V

    new-instance v0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;-><init>(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/Tab$1;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->setInterceptNavigationDelegate(Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mWelcomePageHelper:Lcom/google/android/apps/chrome/WelcomePageHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mWelcomePageHelper:Lcom/google/android/apps/chrome/WelcomePageHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->destroy()V

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/WelcomePageHelper;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;-><init>(Lcom/google/android/apps/chrome/Tab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mWelcomePageHelper:Lcom/google/android/apps/chrome/WelcomePageHelper;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;-><init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/Tab;I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->createWebContentsObserverAndroid()Lorg/chromium/content/browser/WebContentsObserverAndroid;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->onContentViewChanged()V

    goto :goto_0
.end method

.method private initHistoryThumbnailSize(Landroid/app/Activity;)V
    .locals 4

    invoke-static {p1}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v1, 0xe6

    const/16 v0, 0xa0

    :goto_0
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    int-to-float v1, v1

    iget v3, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailWidth:I

    int-to-float v0, v0

    iget v1, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailHeight:I

    return-void

    :cond_0
    const/16 v1, 0x6c

    const/16 v0, 0x48

    goto :goto_0
.end method

.method private static isDownloadableScheme(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URI;->getScheme()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_0

    sget-object v2, Lcom/google/android/apps/chrome/utilities/URLUtilities;->DOWNLOADABLE_SCHEMES:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private isFrozen()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isShowingSadTab()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSadTabView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSadTabView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSadTabView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isStableChannelBuild(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "stable"

    sget-object v1, Lcom/google/android/apps/chrome/Tab;->CHANNEL_NAME_OVERRIDE_FOR_TEST:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f070250

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "stable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private logTabCrashed()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsHidden:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->logRendererCrash()V

    :cond_0
    return-void
.end method

.method private native nativeActivateNearestFindResult(IFF)V
.end method

.method private native nativeAddNewContents(IIIILandroid/graphics/Rect;Z)Z
.end method

.method private native nativeCancelPrerender(I)V
.end method

.method private native nativeContextMenuClosed(I)V
.end method

.method private native nativeCreateHistoricalTab(I)V
.end method

.method private static native nativeCreateHistoricalTabFromState([BI)V
.end method

.method private native nativeCustomContextMenuItemSelected(II)V
.end method

.method private native nativeDestroy(I)V
.end method

.method private native nativeDestroyWebContents(IZ)V
.end method

.method private native nativeFaviconIsValid(I)Z
.end method

.method private native nativeGetBookmarkId(I)J
.end method

.method private native nativeGetDownloadWarningText(ILjava/lang/String;)Ljava/lang/String;
.end method

.method private native nativeGetFaviconBitmap(I)Landroid/graphics/Bitmap;
.end method

.method private native nativeGetLocalizedString(II)Ljava/lang/String;
.end method

.method private native nativeGetPopupBlockedCount(I)I
.end method

.method private native nativeGetPreviousFindText(I)Ljava/lang/String;
.end method

.method private native nativeGetRenderProcessPid(I)I
.end method

.method private native nativeGetRenderProcessPrivateSizeKBytes(I)I
.end method

.method private native nativeGetStateAsByteArray(I)[B
.end method

.method private native nativeGetWebContentDisplayHost(I)Ljava/lang/String;
.end method

.method private native nativeHasPrerenderedUrl(ILjava/lang/String;)Z
.end method

.method private native nativeInit()I
.end method

.method private native nativeInitWebContents(ILorg/chromium/content/browser/ContentViewCore;Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;I)V
.end method

.method private native nativeIsInitialNavigation(I)Z
.end method

.method private native nativeLaunchBlockedPopups(I)V
.end method

.method private native nativeLoadUrl(ILjava/lang/String;Ljava/lang/String;I)I
.end method

.method private native nativePrerenderUrl(ILjava/lang/String;III)V
.end method

.method private native nativePurgeRenderProcessNativeMemory(I)V
.end method

.method private native nativeRequestFindMatchRects(II)V
.end method

.method private static native nativeRestoreStateFromByteArray([BI)I
.end method

.method private native nativeSaveTabIdForSessionSync(I)V
.end method

.method private native nativeSetInterceptNavigationDelegate(ILorg/chromium/components/navigation_interception/InterceptNavigationDelegate;)V
.end method

.method private native nativeSetWindowId(II)V
.end method

.method private native nativeShouldUpdateThumbnail(ILjava/lang/String;)Z
.end method

.method private native nativeStartFinding(ILjava/lang/String;ZZ)V
.end method

.method private native nativeStopFinding(II)V
.end method

.method private native nativeUpdateThumbnail(ILandroid/graphics/Bitmap;)V
.end method

.method private notifyContextualActionBarStateChanged(Z)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "shown"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/16 v1, 0x22

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private notifyFaviconHasChanged()V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0x14

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private notifyInterstitialShown()V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0x33

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private notifyLoadProgress()V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "progress"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getProgress()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0xb

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private notifyPageLoad(I)V
    .locals 3

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "tabId"

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "url"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getUrl()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private notifyPageLoadFailed(I)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "errorCode"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0x1c

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private notifyPageTitleChanged()V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "title"

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0xa

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private notifyPageUrlChanged()V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0x19

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private notifyTabCrashed()V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v1, 0x6

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private static openStateFile(Landroid/app/Activity;IZ)Ljava/io/InputStream;
    .locals 2

    :try_start_0
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-static {p2, p1}, Lcom/google/android/apps/chrome/Tab;->getFilename(ZI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static readState(ILandroid/app/Activity;)Lcom/google/android/apps/chrome/Tab$TabState;
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p0, v1}, Lcom/google/android/apps/chrome/Tab;->openStateFile(Landroid/app/Activity;IZ)Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/chrome/Tab;->readState(Ljava/io/InputStream;Landroid/content/Context;Z)Lcom/google/android/apps/chrome/Tab$TabState;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p0, v2}, Lcom/google/android/apps/chrome/Tab;->openStateFile(Landroid/app/Activity;IZ)Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0, p1, v2}, Lcom/google/android/apps/chrome/Tab;->readState(Ljava/io/InputStream;Landroid/content/Context;Z)Lcom/google/android/apps/chrome/Tab$TabState;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static readState(Ljava/io/InputStream;Landroid/content/Context;Z)Lcom/google/android/apps/chrome/Tab$TabState;
    .locals 2

    if-eqz p2, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->getCipher(I)Ljavax/crypto/Cipher;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Ljavax/crypto/CipherInputStream;

    invoke-direct {v0, p0, v1}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    move-object p0, v0

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/Tab;->readStateInternal(Ljava/io/InputStream;Landroid/content/Context;Z)Lcom/google/android/apps/chrome/Tab$TabState;

    move-result-object v0

    return-object v0
.end method

.method private static readStateInternal(Ljava/io/InputStream;Landroid/content/Context;Z)Lcom/google/android/apps/chrome/Tab$TabState;
    .locals 6

    const/4 v0, 0x0

    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/apps/chrome/Tab$TabState;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/Tab$TabState;-><init>()V

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/chrome/Tab$TabState;->lastShownTimestamp:J

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, v0, Lcom/google/android/apps/chrome/Tab$TabState;->state:[B

    iget-object v2, v0, Lcom/google/android/apps/chrome/Tab$TabState;->state:[B

    invoke-virtual {v1, v2}, Ljava/io/DataInputStream;->readFully([B)V

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, v0, Lcom/google/android/apps/chrome/Tab$TabState;->parentId:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/chrome/Tab$TabState;->openerAppId:Ljava/lang/String;

    const-string v2, ""

    iget-object v3, v0, Lcom/google/android/apps/chrome/Tab$TabState;->openerAppId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/chrome/Tab$TabState;->openerAppId:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, v0, Lcom/google/android/apps/chrome/Tab$TabState;->savedStateVersion:I
    :try_end_3
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_2
    :try_start_4
    iput-boolean p2, v0, Lcom/google/android/apps/chrome/Tab$TabState;->isIncognito:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_5
    sget-object v2, Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;

    const-string v3, "Failed to read opener app id state from tab state"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    throw v0

    :catch_1
    move-exception v2

    :try_start_6
    invoke-static {p1}, Lcom/google/android/apps/chrome/Tab;->isStableChannelBuild(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    iput v2, v0, Lcom/google/android/apps/chrome/Tab$TabState;->savedStateVersion:I

    :goto_3
    sget-object v2, Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to read saved state version id from tab state. Assuming version "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Lcom/google/android/apps/chrome/Tab$TabState;->savedStateVersion:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    const/4 v2, 0x1

    iput v2, v0, Lcom/google/android/apps/chrome/Tab$TabState;->savedStateVersion:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3
.end method

.method private reloadTabIfNecessary()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mTabCrashedInBackground:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->reload()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mTabCrashedInBackground:Z

    :cond_0
    return-void
.end method

.method private removeSadTabIfNeeded()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSadTabView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSadTabView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mSadTabView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->removeView(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSadTabView:Landroid/view/View;

    return-void
.end method

.method public static restoreEncryptionKey(Landroid/os/Bundle;)Z
    .locals 3

    const-string v0, "incognito_key"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v1, v0, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/chrome/Tab;->mKey:Ljava/security/Key;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "cipher"

    const-string v2, "Error the key data is empty"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static restoreState([BI)I
    .locals 1

    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/Tab;->nativeRestoreStateFromByteArray([BI)I

    move-result v0

    return v0
.end method

.method public static saveEncryptionKey(Landroid/os/Bundle;)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/Tab;->mKey:Ljava/security/Key;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/Tab;->mKey:Ljava/security/Key;

    invoke-interface {v0}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "incognito_key"

    invoke-virtual {p0, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method public static saveState(ILjava/lang/Object;Landroid/app/Activity;Z)V
    .locals 3

    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/chrome/Tab$TabState;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Tab$TabState;->state:[B

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p3, p0}, Lcom/google/android/apps/chrome/Tab;->getFilename(ZI)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/app/Activity;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    if-eqz p3, :cond_2

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->getCipher(I)Ljavax/crypto/Cipher;

    move-result-object v2

    if-eqz v2, :cond_2

    new-instance v0, Ljavax/crypto/CipherOutputStream;

    invoke-direct {v0, v1, v2}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    :goto_1
    invoke-static {v0, p1, p3}, Lcom/google/android/apps/chrome/Tab;->saveStateInternal(Ljava/io/OutputStream;Ljava/lang/Object;Z)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private static saveStateInternal(Ljava/io/OutputStream;Ljava/lang/Object;Z)V
    .locals 4

    check-cast p1, Lcom/google/android/apps/chrome/Tab$TabState;

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, p0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    if-eqz p2, :cond_0

    const-wide/16 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    :cond_0
    iget-wide v2, p1, Lcom/google/android/apps/chrome/Tab$TabState;->lastShownTimestamp:J

    invoke-virtual {v1, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-object v0, p1, Lcom/google/android/apps/chrome/Tab$TabState;->state:[B

    array-length v0, v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v0, p1, Lcom/google/android/apps/chrome/Tab$TabState;->state:[B

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->write([B)V

    iget v0, p1, Lcom/google/android/apps/chrome/Tab$TabState;->parentId:I

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v0, p1, Lcom/google/android/apps/chrome/Tab$TabState;->openerAppId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/chrome/Tab$TabState;->openerAppId:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget v0, p1, Lcom/google/android/apps/chrome/Tab$TabState;->savedStateVersion:I

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lcom/google/android/apps/chrome/utilities/StreamUtils;->closeQuietly(Ljava/io/Closeable;)V

    return-void

    :cond_1
    :try_start_1
    const-string v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/google/android/apps/chrome/utilities/StreamUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw v0
.end method

.method private saveToClipboard(Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    invoke-static {p1, p1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070064

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/ui/gfx/NativeWindow;->showError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setInterceptNavigationDelegate(Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mInterceptNavigationDelegate:Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/Tab;->nativeSetInterceptNavigationDelegate(ILorg/chromium/components/navigation_interception/InterceptNavigationDelegate;)V

    return-void
.end method

.method private setLastShownTimestamp(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/chrome/Tab;->mLastShownTimestamp:J

    return-void
.end method

.method private shouldUpdateThumbnail()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/Tab;->nativeShouldUpdateThumbnail(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private showContextMenu(Lcom/google/android/apps/chrome/ChromeContextMenuInfo;)V
    .locals 3

    iget-boolean v0, p1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isAnchor:Z

    if-nez v0, :cond_1

    iget-boolean v0, p1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isEditable:Z

    if-nez v0, :cond_1

    iget-boolean v0, p1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isImage:Z

    if-nez v0, :cond_1

    iget-boolean v0, p1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isSelectedText:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isCustomMenu()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->openContextMenu(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->performHapticFeedback(I)Z

    iget-boolean v0, p1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isImage:Z

    iget-boolean v1, p1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isAnchor:Z

    iget-boolean v2, p1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isSelectedText:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/UmaRecordAction;->recordShowContextMenu(ZZZ)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0x1e

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method private showInternal()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsHidden:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->reloadTabIfNecessary()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/Tab;->mLastShownTimestamp:J

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->onShow()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isShowingInterstitialPage()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->notifyLoadProgress()V

    :cond_0
    return-void
.end method

.method private snapshotStateDownloading()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    new-instance v1, Lcom/google/android/apps/chrome/Tab$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Tab$6;-><init>(Lcom/google/android/apps/chrome/Tab;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->setExpireOnNavigation(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Tab;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    :cond_0
    return-void
.end method

.method private snapshotStateError()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadErrorInfoBar:Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContext:Landroid/content/Context;

    const v1, 0x7f07013e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/Tab$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Tab$7;-><init>(Lcom/google/android/apps/chrome/Tab;)V

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->createWarningInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;Ljava/lang/CharSequence;)Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadErrorInfoBar:Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadErrorInfoBar:Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Tab;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    :cond_1
    return-void
.end method

.method private snapshotStateNeverReady()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->dismiss()V

    :cond_0
    return-void
.end method

.method private snapshotStateReady(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->dismiss()V

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;-><init>(Landroid/net/Uri;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->setExpireOnNavigation(Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Tab;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotId:Ljava/lang/String;

    return-void
.end method

.method private swapWebContents(I)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->destroyContentView(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/Tab;->initContentView(Landroid/app/Activity;I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->onShow()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0x16

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private static declared-synchronized triggerIncognitoKeyGeneration()V
    .locals 3

    const-class v1, Lcom/google/android/apps/chrome/Tab;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/Tab;->mKey:Ljava/security/Key;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/Tab;->sIncognitoKeyGenerator:Ljava/util/concurrent/FutureTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v2, Lcom/google/android/apps/chrome/Tab$8;

    invoke-direct {v2}, Lcom/google/android/apps/chrome/Tab$8;-><init>()V

    invoke-direct {v0, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    sput-object v0, Lcom/google/android/apps/chrome/Tab;->sIncognitoKeyGenerator:Ljava/util/concurrent/FutureTask;

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    sget-object v2, Lcom/google/android/apps/chrome/Tab;->sIncognitoKeyGenerator:Ljava/util/concurrent/FutureTask;

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private updateHistoryThumbnail(Landroid/graphics/Bitmap;)V
    .locals 10

    const-wide/high16 v8, 0x3ff0000000000000L

    const/high16 v7, 0x3f800000

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailWidth:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailHeight:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v0, v1, :cond_5

    :cond_1
    const/4 v0, 0x2

    :try_start_0
    new-array v2, v0, [I

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    aput v1, v2, v0

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    aput v1, v2, v0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailWidth:I

    int-to-float v0, v0

    const/4 v1, 0x0

    aget v1, v2, v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailHeight:I

    int-to-float v1, v1

    const/4 v3, 0x1

    aget v3, v2, v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v3

    cmpg-float v0, v3, v7

    if-gez v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailWidth:I

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v4, v8, v4

    double-to-int v1, v4

    mul-int/2addr v0, v1

    move v1, v0

    :goto_1
    cmpg-float v0, v3, v7

    if-gez v0, :cond_4

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailHeight:I

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    div-double v3, v8, v3

    double-to-int v3, v3

    mul-int/2addr v0, v3

    :goto_2
    invoke-static {v2, v1, v0}, Lcom/google/android/apps/chrome/utilities/MathUtils;->scaleToFitTargetSize([III)F

    move-result v3

    const/4 v4, 0x0

    aget v2, v2, v4

    sub-int v2, v1, v2

    int-to-float v2, v2

    const/high16 v4, 0x40000000

    div-float/2addr v2, v4

    div-float/2addr v2, v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v3, v3}, Landroid/graphics/Canvas;->scale(FF)V

    const/4 v4, 0x0

    new-instance v5, Landroid/graphics/Paint;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v1, p1, v2, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    cmpg-float v1, v3, v7

    if-gez v1, :cond_2

    iget v1, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailWidth:I

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailHeight:I

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->updateThumbnail(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;

    const-string v1, "OutOfMemoryError while updating the history thumbnail."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x31

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    goto/16 :goto_0

    :cond_3
    :try_start_1
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailWidth:I

    move v1, v0

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailHeight:I
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->updateThumbnail(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0
.end method

.method private updateNTPToolbarUrl(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findOrCreateIfNeeded(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/ChromeActivity;Ljava/lang/String;)Lcom/google/android/apps/chrome/NewTabPageToolbar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->urlChanged()V

    :cond_0
    return-void
.end method

.method private updateThumbnail(Landroid/graphics/Bitmap;)V
    .locals 3

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/Tab;->nativeUpdateThumbnail(ILandroid/graphics/Bitmap;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "url"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x3d

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method private updateTitle()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->updateTitle(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateTitle(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mTitle:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsTabStateDirty:Z

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mTitle:Ljava/lang/String;

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public activateNearestFindResult(FF)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/chrome/Tab;->nativeActivateNearestFindResult(IFF)V

    return-void
.end method

.method public addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    return-void
.end method

.method protected associateWithApp(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mAppAssociatedWith:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Tab;->getLocationBar(Landroid/app/Activity;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/Tab$9;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/Tab$9;-><init>(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/LocationBar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->addTextChangeListener(Lcom/google/android/apps/chrome/LocationBar$TextChangeListener;)V

    return-void
.end method

.method public cancelPrerender()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeCancelPrerender(I)V

    return-void
.end method

.method public contextItemSelected(Landroid/view/MenuItem;)Z
    .locals 8

    const/4 v7, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isCustomMenu()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/google/android/apps/chrome/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->getCustomItemSize()I

    move-result v1

    if-lt v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->getCustomItemAt(I)Lcom/google/android/apps/chrome/ChromeContextMenuInfo$CustomMenuItem;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo$CustomMenuItem;->mAction:I

    iget v1, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/Tab;->nativeCustomContextMenuItemSelected(II)V

    move v0, v7

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/chrome/Main;

    invoke-static {p1}, Lcom/google/android/apps/chrome/UmaRecordAction;->recordContextMenuItemSelected(Landroid/view/MenuItem;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v7

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->linkUrl:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/Main;->openUrlInCurrentModel(Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)V

    goto :goto_1

    :pswitch_2
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v1

    add-int/lit8 v4, v1, 0x1

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v1, v1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->linkUrl:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/TabModel;->createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZ)Lcom/google/android/apps/chrome/Tab;

    goto :goto_1

    :cond_3
    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v4

    goto :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->srcUrl:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v2, v1}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->srcUrl:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/Main;->openUrlInCurrentModel(Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)V

    goto :goto_1

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->unfilteredLinkUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->saveToClipboard(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->linkText:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->saveToClipboard(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mDownloadListener:Lcom/google/android/apps/chrome/ChromeDownloadListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v1, v1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->srcUrl:Ljava/lang/String;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->onDownloadStartNoStream(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mDownloadListener:Lcom/google/android/apps/chrome/ChromeDownloadListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v1, v1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->unfilteredLinkUrl:Ljava/lang/String;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->onDownloadStartNoStream(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f0f00e7
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public contextMenuClosed(Landroid/view/Menu;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeContextMenuClosed(I)V

    return-void
.end method

.method createHistoricalTab()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->isFrozen()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeCreateHistoricalTab(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    iget v1, p0, Lcom/google/android/apps/chrome/Tab;->mSavedStateVersion:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Tab;->nativeCreateHistoricalTabFromState([BI)V

    goto :goto_0
.end method

.method public deleteState(Landroid/app/Activity;)V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/Tab;->mIncognito:Z

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/chrome/Tab;->deleteStateFile(ILandroid/app/Activity;Z)V

    return-void
.end method

.method destroy()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->destroyInternal(Z)V

    return-void
.end method

.method public faviconIsValid()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeFaviconIsValid(I)Z

    move-result v0

    return v0
.end method

.method public generateFullsizeBitmap()Landroid/graphics/Bitmap;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentView;->getHeight()I

    move-result v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v3, v1, v2}, Lorg/chromium/content/browser/ContentView;->getBitmap(II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;

    const-string v3, "OutOfMemoryError thrown while trying to fetch a tab bitmap."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/16 v1, 0x31

    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    goto :goto_0
.end method

.method protected getAppAssociatedWith()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mAppAssociatedWith:Ljava/lang/String;

    return-object v0
.end method

.method public getBackgroundColor()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getBackgroundColor()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mBaseUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getBitmap()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;
    .locals 7

    const/4 v2, 0x0

    const-string v0, "Tab.getBitmap"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->begin(Ljava/lang/String;)V

    const/high16 v3, 0x3f800000

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->getWidth()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v4}, Lorg/chromium/content/browser/ContentView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v4}, Lorg/chromium/content/browser/ContentViewCore;->getScaledPerformanceOptimizedBitmap(II)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    move-object v3, v0

    :goto_0
    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->canUpdateHistoryThumbnail()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/Tab;->updateHistoryThumbnail(Landroid/graphics/Bitmap;)V

    :cond_0
    const-string v0, "Tab.getBitmap"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    if-nez v3, :cond_1

    move-object v0, v2

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_2
    sget-object v4, Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;

    const-string v5, "OutOfMemoryError thrown while trying to fetch a tab bitmap."

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/16 v0, 0x31

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    move v6, v3

    move-object v3, v1

    move v1, v6

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;

    invoke-direct {v0, v3, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;-><init>(Landroid/graphics/Bitmap;F)V

    goto :goto_1

    :catch_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_2
.end method

.method public getBlockedPopupCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeGetPopupBlockedCount(I)I

    move-result v0

    return v0
.end method

.method public getBookmarkId()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->isFrozen()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeGetBookmarkId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getChromeWebContentsDelegateAndroidForTest()Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mChromeWebContentsDelegateAndroid:Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDisplayHost()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeGetWebContentDisplayHost(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method getDownloadWarningText(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/Tab;->nativeGetDownloadWarningText(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFallbackTextureId()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isNTP()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getFavicon()Landroid/graphics/Bitmap;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mCachedFaviconUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mCachedFavicon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mCachedFaviconUrl:Ljava/lang/String;

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mCachedFavicon:Landroid/graphics/Bitmap;

    if-nez v2, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->isFrozen()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    :goto_0
    return-object v0

    :cond_3
    const/high16 v0, 0x41800000

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/chrome/Tab;->getScaledFavicon(II)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->faviconIsValid()Z

    move-result v2

    if-eqz v2, :cond_2

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mCachedFavicon:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mCachedFaviconUrl:Ljava/lang/String;

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mCachedFavicon:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    return v0
.end method

.method getInfoBarContainer()Lcom/google/android/apps/chrome/infobar/InfoBarContainer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    return-object v0
.end method

.method public getInfoBars()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getInfoBars()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getLastShownTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/Tab;->mLastShownTimestamp:J

    return-wide v0
.end method

.method public getLaunchType()Lcom/google/android/apps/chrome/TabModel$TabLaunchType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mLaunchType:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    return-object v0
.end method

.method getLocationBar(Landroid/app/Activity;)Lcom/google/android/apps/chrome/LocationBar;
    .locals 1

    instance-of v0, p1, Lcom/google/android/apps/chrome/Main;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/chrome/Main;

    iget-object v0, p1, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getNativeInfoBarContainer()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getNative()I

    move-result v0

    return v0
.end method

.method getNativeTabAndroidImpl()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    return v0
.end method

.method public getOpenerId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mOpenerTabId:I

    return v0
.end method

.method public getParentId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mParentId:I

    return v0
.end method

.method public getPreviousFindText()Ljava/lang/String;
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeGetPreviousFindText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProgress()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsLoading:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mChromeWebContentsDelegateAndroid:Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;->getMostRecentProgress()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x64

    goto :goto_0
.end method

.method public getRenderProcessPid()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isSavedAndViewDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, -0x80000000

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeGetRenderProcessPid(I)I

    move-result v0

    goto :goto_0
.end method

.method public getRenderProcessPrivateSizeKBytes()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isSavedAndViewDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeGetRenderProcessPrivateSizeKBytes(I)I

    move-result v0

    goto :goto_0
.end method

.method public getState()Ljava/lang/Object;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->isFrozen()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeGetStateAsByteArray(I)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedStateVersion:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/Tab$TabState;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/Tab$TabState;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    iput-object v1, v0, Lcom/google/android/apps/chrome/Tab$TabState;->state:[B

    iget-wide v1, p0, Lcom/google/android/apps/chrome/Tab;->mLastShownTimestamp:J

    iput-wide v1, v0, Lcom/google/android/apps/chrome/Tab$TabState;->lastShownTimestamp:J

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getParentId()I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/chrome/Tab$TabState;->parentId:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getAppAssociatedWith()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/Tab$TabState;->openerAppId:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/chrome/Tab;->mSavedStateVersion:I

    iput v1, v0, Lcom/google/android/apps/chrome/Tab$TabState;->savedStateVersion:I

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mTitle:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mTitle:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getUrl()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getView()Lorg/chromium/content/browser/ContentView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    return-object v0
.end method

.method public getViewClientForTesting()Lorg/chromium/content/browser/ContentViewClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public hasPrerenderedUrl(Ljava/lang/String;)Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/Tab;->nativeHasPrerenderedUrl(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method hide()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsHidden:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->onHide()V

    return-void
.end method

.method public initialize(I)V
    .locals 3

    sget-boolean v0, Lcom/google/android/apps/chrome/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->nativeInit()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->initHistoryThumbnailSize(Landroid/app/Activity;)V

    new-instance v0, Lcom/google/android/apps/chrome/ChromeDownloadListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/TabModelSelector;Lcom/google/android/apps/chrome/Tab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mDownloadListener:Lcom/google/android/apps/chrome/ChromeDownloadListener;

    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIncognito:Z

    invoke-static {v0}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(Z)I

    move-result p1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->createChromeWebContentsDelegateAndroid()Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mChromeWebContentsDelegateAndroid:Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->createContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/Tab;->initContentView(Landroid/app/Activity;I)V

    return-void
.end method

.method protected installBookmarkShortcut(Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public isClosing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsClosing:Z

    return v0
.end method

.method public isHidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsHidden:Z

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIncognito:Z

    return v0
.end method

.method public isInitialNavigation()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeIsInitialNavigation(I)Z

    move-result v0

    return v0
.end method

.method public isLoading()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoadingAndRenderingDone()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNTP()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "chrome://newtab/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isReady()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSavedAndViewDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowingInterstitialPage()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->isFrozen()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isShowingInterstitialPage()Z

    move-result v0

    goto :goto_0
.end method

.method public isShowingSnapshot()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ".mht"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTabBlockingPopups()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeGetPopupBlockedCount(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTextureViewAvailable()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadUrl(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 5

    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->removeSadTabIfNeeded()V

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v2, p1, p2, p3}, Lcom/google/android/apps/chrome/Tab;->nativeLoadUrl(ILjava/lang/String;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mPendingLoadUrl:Ljava/lang/String;

    :cond_0
    if-eq v3, v4, :cond_1

    const/4 v2, 0x2

    if-ne v3, v2, :cond_4

    :cond_1
    move v2, v0

    :goto_0
    if-ne v3, v4, :cond_5

    :goto_1
    if-eqz v2, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->didStartPageLoad(Ljava/lang/String;)V

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab;->didFinishPageLoad(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/Tab;->getLocationBar(Landroid/app/Activity;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar;->simulatePageLoadProgress()V

    :cond_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "tabId"

    iget v4, p0, Lcom/google/android/apps/chrome/Tab;->mId:I

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "fullyPrerendered"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/16 v0, 0x23

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    :cond_3
    return v3

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public onActivityResume()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->findOrCreateIfNeeded(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/ChromeActivity;Ljava/lang/String;)Lcom/google/android/apps/chrome/NewTabPageToolbar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->onActivityResume()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->reloadTabIfNecessary()V

    return-void
.end method

.method public onFaviconUpdated()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mCachedFavicon:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->notifyFaviconHasChanged()V

    return-void
.end method

.method public onFirstSearch()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/RevenueStats;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/RevenueStats;->onFirstSearch()V

    return-void
.end method

.method public onInfoBarDismissed(Lcom/google/android/apps/chrome/infobar/InfoBar;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mPopupInfoBar:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mPopupInfoBar:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    :cond_0
    return-void
.end method

.method public onInterstitialHidden()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->updateNTPToolbarUrl(Ljava/lang/String;)V

    return-void
.end method

.method public onInterstitialShown()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->notifyInterstitialShown()V

    return-void
.end method

.method public onLaunchBlockedPopups()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeLaunchBlockedPopups(I)V

    return-void
.end method

.method onNewTabPageReady()V
    .locals 1

    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->notifyPageLoad(I)V

    return-void
.end method

.method public onPopupBlockStateChanged()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isTabBlockingPopups()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mPopupInfoBar:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mPopupInfoBar:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mPopupInfoBar:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mPopupInfoBar:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->updateBlockedPopupText()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mPopupInfoBar:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mPopupInfoBar:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->removeInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mPopupInfoBar:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    goto :goto_0
.end method

.method public onReceivedHttpAuthRequest(Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/Tab;->tryHandleSpdyProxyAuthentication(Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/HttpAuthDialog;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/chrome/HttpAuthDialog;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;)V

    invoke-virtual {p1, v0}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->setAutofillObserver(Lorg/chromium/chrome/browser/ChromeHttpAuthHandler$AutofillObserver;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/HttpAuthDialog;->show()V

    goto :goto_0
.end method

.method public parentIsIncognito()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mParentIsIncognito:Z

    return v0
.end method

.method public popuplateContextMenu(Landroid/view/ContextMenu;)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isCustomMenu()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->getCustomItemSize()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->getCustomItemAt(I)Lcom/google/android/apps/chrome/ChromeContextMenuInfo$CustomMenuItem;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/chrome/ChromeContextMenuInfo$CustomMenuItem;->mLabel:Ljava/lang/String;

    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isAnchor:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isSelectedText:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isImage:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isEditable:Z

    if-eqz v0, :cond_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v2, 0x7f0e0000

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f0f00e6

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-boolean v2, v2, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isAnchor:Z

    invoke-interface {p1, v0, v2}, Landroid/view/ContextMenu;->setGroupVisible(IZ)V

    const v0, 0x7f0f00ec

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-boolean v2, v2, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isImage:Z

    invoke-interface {p1, v0, v2}, Landroid/view/ContextMenu;->setGroupVisible(IZ)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f0f00e8

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->linkText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0f00ea

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->linkUrl:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->linkUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->linkUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    :cond_6
    const v0, 0x7f0f00eb

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v1, v1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->linkUrl:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->isDownloadableScheme(Ljava/lang/String;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v0, 0x7f0f00ed

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContextMenuInfo:Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    iget-object v1, v1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->srcUrl:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->isDownloadableScheme(Ljava/lang/String;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method public prerenderUrl(Ljava/lang/String;I)V
    .locals 6

    iget v1, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getHeight()I

    move-result v5

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/Tab;->nativePrerenderUrl(ILjava/lang/String;III)V

    return-void
.end method

.method public purgeRenderProcessNativeMemory()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isSavedAndViewDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativePurgeRenderProcessNativeMemory(I)V

    :cond_0
    return-void
.end method

.method public reload()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->reload()V

    :cond_0
    return-void
.end method

.method public removeInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mInfoBarContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->removeInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    return-void
.end method

.method public requestFindMatchRects(I)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/Tab;->nativeRequestFindMatchRects(II)V

    return-void
.end method

.method public requestFocus(Z)V
    .locals 1

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/Tab;->getLocationBar(Landroid/app/Activity;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->requestFocus()Z

    goto :goto_0
.end method

.method public restoreState(Landroid/app/Activity;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;

    const-string v2, "Trying to restore tab from state but no state was previously saved."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mSavedStateVersion:I

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/Tab;->nativeRestoreStateFromByteArray([BI)I

    move-result v0

    :goto_0
    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIncognito:Z

    invoke-static {v0}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(Z)I

    move-result v0

    const/4 v1, 0x1

    :cond_1
    iput-object v3, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/Tab;->initContentView(Landroid/app/Activity;I)V

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "chrome://newtab/"

    :goto_1
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v3, v1}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mUrl:Ljava/lang/String;

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public saveStateAndDestroy()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsHidden:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->isFrozen()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->nativeGetStateAsByteArray(I)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedStateVersion:I

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->destroyInternal(Z)V

    :cond_1
    return-void
.end method

.method public setClosing()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mThumbnailRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsClosing:Z

    return-void
.end method

.method public setFindMatchRectsListener(Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;

    return-void
.end method

.method public setFindResultListener(Lcom/google/android/apps/chrome/Tab$FindResultListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mFindResultListener:Lcom/google/android/apps/chrome/Tab$FindResultListener;

    return-void
.end method

.method public setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    return-void
.end method

.method public setOpenerId(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/Tab;->mOpenerTabId:I

    return-void
.end method

.method public setParentIsIncognito()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mParentIsIncognito:Z

    return-void
.end method

.method public setSnapshotId(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createQueryStateIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public setViewClientForTesting(Lorg/chromium/content/browser/ContentViewClient;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->setContentViewClient(Lorg/chromium/content/browser/ContentViewClient;)V

    return-void
.end method

.method public setWindowId(I)V
    .locals 2

    iput p1, p0, Lcom/google/android/apps/chrome/Tab;->mWindowId:I

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    iget v1, p0, Lcom/google/android/apps/chrome/Tab;->mWindowId:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/Tab;->nativeSetWindowId(II)V

    return-void
.end method

.method public shouldTabStateBePersisted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsTabStateDirty:Z

    return v0
.end method

.method show(Landroid/app/Activity;)V
    .locals 3

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/Tab;->restoreState(Landroid/app/Activity;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSavedState:[B

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->showInternal()V

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenTopControlsOffsetY:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenContentOffsetY:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPositionsForTabToNonFullscreen()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->showControlsTransient()V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenTopControlsOffsetY:F

    iget v2, p0, Lcom/google/android/apps/chrome/Tab;->mPreviousFullscreenContentOffsetY:F

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPositionsForTab(FF)V

    goto :goto_0
.end method

.method public snapshotStateQueryResult(Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mSnapshotId:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/Tab$12;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotViewableState:[I

    invoke-virtual {p3}, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/Tab;->snapshotStateReady(Landroid/net/Uri;)V

    goto :goto_1

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->snapshotStateDownloading()V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->snapshotStateError()V

    goto :goto_1

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab;->snapshotStateNeverReady()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public startFinding(Ljava/lang/String;ZZ)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/apps/chrome/Tab;->nativeStartFinding(ILjava/lang/String;ZZ)V

    return-void
.end method

.method public stopFinding(I)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/Tab;->nativeStopFinding(II)V

    return-void
.end method

.method public stopLoading()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/Tab;->notifyPageLoad(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->stopLoading()V

    goto :goto_0
.end method

.method public tabStateWasPersisted()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Tab;->mIsTabStateDirty:Z

    return-void
.end method

.method public tryHandleSpdyProxyAuthentication(Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "SpdyProxy"

    invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSpdyProxyOrigin()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/apps/chrome/Tab;->mSpdyProxyAuthRequestTimestamp:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x1f4

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    iget-wide v4, p0, Lcom/google/android/apps/chrome/Tab;->mSpdyProxyAuthTokenInvalidationTimestamp:J

    sub-long v4, v2, v4

    const-wide/32 v6, 0x36ee80

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->invalidateToken()V

    iput-wide v2, p0, Lcom/google/android/apps/chrome/Tab;->mSpdyProxyAuthTokenInvalidationTimestamp:J

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/Tab;->mSpdyProxyAuthRequestTimestamp:J

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->getToken()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;

    const-string v3, "No existing spdyproxy token, refreshing..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    new-instance v3, Lcom/google/android/apps/chrome/Tab$11;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/chrome/Tab$11;-><init>(Lcom/google/android/apps/chrome/Tab;Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;)V

    invoke-virtual {v0, v2, p3, v3}, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->foregroundTokenRefresh(Landroid/app/Activity;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;

    const-string v2, "Interpreting frequent SpdyProxy auth requests as an authorization failure."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->cancel()V

    move v0, v1

    goto :goto_0

    :cond_3
    const-string v2, "fw-cookie"

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->proceed(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    sget-object v1, Lcom/google/android/apps/chrome/Tab;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SpdyProxy realm, but origin mismatch. Incoming: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " expected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public useTextureView()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->hasLargeOverlay()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
