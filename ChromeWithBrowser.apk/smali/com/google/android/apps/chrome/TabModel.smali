.class public interface abstract Lcom/google/android/apps/chrome/TabModel;
.super Ljava/lang/Object;


# static fields
.field public static final INVALID_TAB_ID:I = -0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final INVALID_TAB_INDEX:I = -0x1

.field public static final NTP_TAB_ID:I = -0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# virtual methods
.method public abstract bringToFrontOrLaunchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)V
.end method

.method public abstract closeAllTabs()V
.end method

.method public abstract closeCurrentTab()Z
.end method

.method public abstract closeTab(Lcom/google/android/apps/chrome/Tab;)Z
.end method

.method public abstract closeTab(Lcom/google/android/apps/chrome/Tab;Z)Z
.end method

.method public abstract closeTabById(I)Z
.end method

.method public abstract closeTabByIndex(I)Z
.end method

.method public abstract createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZ)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract createTabWithNativeContents(IILcom/google/android/apps/chrome/TabModel$TabLaunchType;IILjava/lang/String;)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract createTabWithNativeContents(IILjava/lang/String;)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract destroy()V
.end method

.method public abstract getCount()I
.end method

.method public abstract getCurrentTab()Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract getCurrentView()Lorg/chromium/content/browser/ContentView;
.end method

.method public abstract getNextTabIfClosed(I)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract getTab(I)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract getTabById(I)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract getTabFromView(Lorg/chromium/content/browser/ContentView;)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract getTabIndexById(I)I
.end method

.method public abstract getTabIndexByUrl(Ljava/lang/String;)I
.end method

.method public abstract getView(I)Lorg/chromium/content/browser/ContentView;
.end method

.method public abstract index()I
.end method

.method public abstract indexOf(Lcom/google/android/apps/chrome/Tab;)I
.end method

.method public abstract isIncognito()Z
.end method

.method public abstract isSessionRestoreInProgress()Z
.end method

.method public abstract launchNTP()Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract launchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract launchUrlFromExternalApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract moveTab(II)V
.end method

.method public abstract setIndex(I)V
.end method
