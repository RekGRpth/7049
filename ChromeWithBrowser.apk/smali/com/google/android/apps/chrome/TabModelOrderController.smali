.class Lcom/google/android/apps/chrome/TabModelOrderController;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NO_TAB:I = -0x1


# instance fields
.field private final mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/TabModelOrderController;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/TabModelOrderController;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelOrderController;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-void
.end method

.method private getIndexOfLastTabOpenedBy(II)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelOrderController;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-lt v0, p2, :cond_1

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getOpenerId()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static linkClicked(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LINK:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static sameModelType(Lcom/google/android/apps/chrome/TabModel;Lcom/google/android/apps/chrome/Tab;)Z
    .locals 2

    invoke-interface {p0}, Lcom/google/android/apps/chrome/TabModel;->isIncognito()Z

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method determineInsertionIndex(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;ILcom/google/android/apps/chrome/Tab;)I
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/chrome/TabModelOrderController;->linkClicked(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p3}, Lcom/google/android/apps/chrome/TabModelOrderController;->determineInsertionIndex(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Lcom/google/android/apps/chrome/Tab;)I

    move-result p2

    :cond_0
    invoke-virtual {p3}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/TabModelOrderController;->willOpenInForeground(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelOrderController;->forgetAllOpeners()V

    :cond_1
    return p2
.end method

.method determineInsertionIndex(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Lcom/google/android/apps/chrome/Tab;)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelOrderController;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/google/android/apps/chrome/TabModelOrderController;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->getTabIndexById(I)I

    move-result v2

    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/TabModelOrderController;->sameModelType(Lcom/google/android/apps/chrome/TabModel;Lcom/google/android/apps/chrome/Tab;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/TabModelOrderController;->willOpenInForeground(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/chrome/TabModelOrderController;->getIndexOfLastTabOpenedBy(II)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v2, 0x1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelOrderController;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method forgetAllOpeners()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelOrderController;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/Tab;->setOpenerId(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method willOpenInForeground(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelOrderController;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
