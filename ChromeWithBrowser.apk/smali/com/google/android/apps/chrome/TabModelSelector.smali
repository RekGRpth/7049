.class public interface abstract Lcom/google/android/apps/chrome/TabModelSelector;
.super Ljava/lang/Object;


# virtual methods
.method public abstract closeAllTabs()V
.end method

.method public abstract closeTab(Lcom/google/android/apps/chrome/Tab;)Z
.end method

.method public abstract closeTab(Lcom/google/android/apps/chrome/Tab;Z)Z
.end method

.method public abstract closeTabById(I)Z
.end method

.method public abstract destroy()V
.end method

.method public abstract getAllModels()[Lcom/google/android/apps/chrome/TabModel;
.end method

.method public abstract getCurrentModel()Lcom/google/android/apps/chrome/TabModel;
.end method

.method public abstract getCurrentModelIndex()I
.end method

.method public abstract getCurrentTab()Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract getCurrentView()Lorg/chromium/content/browser/ContentView;
.end method

.method public abstract getModel(Z)Lcom/google/android/apps/chrome/TabModel;
.end method

.method public abstract getRestoredTabCount()I
.end method

.method public abstract getTabById(I)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract getTabFromView(Lorg/chromium/content/browser/ContentView;)Lcom/google/android/apps/chrome/Tab;
.end method

.method public abstract getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
.end method

.method public abstract getTotalTabCount()I
.end method

.method public abstract isIncognitoSelected()Z
.end method

.method public abstract openNewTab(Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Lcom/google/android/apps/chrome/Tab;Z)V
.end method

.method public abstract registerChangeListener(Lcom/google/android/apps/chrome/TabModelSelector$ChangeListener;)V
.end method

.method public abstract selectModel(Z)V
.end method

.method public abstract unregisterChangeListener(Lcom/google/android/apps/chrome/TabModelSelector$ChangeListener;)V
.end method
