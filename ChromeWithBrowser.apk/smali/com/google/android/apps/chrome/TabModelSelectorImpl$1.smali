.class Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    const/4 v1, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$2200(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->handleOnOverviewModeHidden()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$2300(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->handleOnPageLoadStopped(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$2400(Lcom/google/android/apps/chrome/TabModelSelectorImpl;I)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->handleNewTabPageReady(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$2500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;I)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mSessionRestoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$2100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    sget-boolean v1, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mNativeChromeTabModel:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$2600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->nativeBroadcastSessionRestoreComplete(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$2700(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;I)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->invalidateIfChanged(Lcom/google/android/apps/chrome/TabThumbnailProvider;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "authToken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "success"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "result"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->handleAutoLoginResult(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    invoke-static {v4, v1, v2, v3, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$2800(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->handleAutoLoginDisabled()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$2900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_5
        0x10 -> :sswitch_4
        0x19 -> :sswitch_5
        0x1b -> :sswitch_2
        0x20 -> :sswitch_3
        0x39 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method
