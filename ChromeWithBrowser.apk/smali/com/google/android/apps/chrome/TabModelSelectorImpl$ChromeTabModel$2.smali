.class Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$1600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iget-object v0, v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mPreloadWebViewContainer:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Landroid/view/ViewGroup;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$1600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$1600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iget-object v0, v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mPreloadWebViewContainer:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iget-object v0, v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mPreloadWebViewContainer:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$1600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$1600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iget-object v1, v1, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->show(Landroid/app/Activity;)V

    goto :goto_0
.end method
