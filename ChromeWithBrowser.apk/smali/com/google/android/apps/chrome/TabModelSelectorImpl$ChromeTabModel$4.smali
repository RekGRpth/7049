.class Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

.field final synthetic val$tabId:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iput p2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;->val$tabId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iget v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;->val$tabId:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$1600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$1600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;->val$tabId:I

    if-ne v1, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$1600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->generateFullsizeBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iget-object v2, v2, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v2

    const/4 v3, -0x2

    new-instance v4, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;

    invoke-static {}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->defaultScale()F

    move-result v5

    invoke-direct {v4, v0, v5}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;-><init>(Landroid/graphics/Bitmap;F)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->putTexture(ILcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$1600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->clearCachedNtp()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$2000(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)V

    goto :goto_1

    :catch_0
    move-exception v0

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1900()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OutOfMemoryError thrown while caching NTP bitmap."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method
