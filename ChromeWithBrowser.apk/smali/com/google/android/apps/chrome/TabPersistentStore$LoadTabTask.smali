.class Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;
.super Landroid/os/AsyncTask;


# instance fields
.field private mId:I

.field private mTabState:Lcom/google/android/apps/chrome/Tab$TabState;

.field final synthetic this$0:Lcom/google/android/apps/chrome/TabPersistentStore;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/TabPersistentStore;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mTabState:Lcom/google/android/apps/chrome/Tab$TabState;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/TabPersistentStore;Lcom/google/android/apps/chrome/TabPersistentStore$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;-><init>(Lcom/google/android/apps/chrome/TabPersistentStore;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mId:I

    return v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->doInBackground([Ljava/lang/Integer;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/lang/Void;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mId:I

    iget v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mId:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$300(Lcom/google/android/apps/chrome/TabPersistentStore;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Tab;->readState(ILandroid/app/Activity;)Lcom/google/android/apps/chrome/Tab$TabState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mTabState:Lcom/google/android/apps/chrome/Tab$TabState;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 7

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mTabState:Lcom/google/android/apps/chrome/Tab$TabState;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mTabState:Lcom/google/android/apps/chrome/Tab$TabState;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/Tab$TabState;->isIncognito:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mCancelIncognitoTabLoads:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$1000(Lcom/google/android/apps/chrome/TabPersistentStore;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mTabState:Lcom/google/android/apps/chrome/Tab$TabState;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/Tab$TabState;->isIncognito:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mCancelNormalTabLoads:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$1100(Lcom/google/android/apps/chrome/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$1200(Lcom/google/android/apps/chrome/TabPersistentStore;)Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mTabState:Lcom/google/android/apps/chrome/Tab$TabState;

    iget-boolean v1, v1, Lcom/google/android/apps/chrome/Tab$TabState;->isIncognito:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mTabState:Lcom/google/android/apps/chrome/Tab$TabState;

    iget-object v1, v1, Lcom/google/android/apps/chrome/Tab$TabState;->state:[B

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mTabState:Lcom/google/android/apps/chrome/Tab$TabState;

    iget v2, v2, Lcom/google/android/apps/chrome/Tab$TabState;->savedStateVersion:I

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/Tab;->restoreState([BI)I

    move-result v2

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mSavedActiveId:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$1300(Lcom/google/android/apps/chrome/TabPersistentStore;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->indexOf(Lcom/google/android/apps/chrome/Tab;)I

    move-result v1

    :goto_1
    if-ltz v1, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mNextRestoredIndex:I
    invoke-static {v3}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$1400(Lcom/google/android/apps/chrome/TabPersistentStore;)I

    move-result v3

    iget-object v5, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mSavedActiveIndex:I
    invoke-static {v5}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$1500(Lcom/google/android/apps/chrome/TabPersistentStore;)I

    move-result v5

    if-ge v3, v5, :cond_4

    const/4 v3, 0x0

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    :cond_4
    iget v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mId:I

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_RESTORE:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    iget-object v5, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mTabState:Lcom/google/android/apps/chrome/Tab$TabState;

    iget v5, v5, Lcom/google/android/apps/chrome/Tab$TabState;->parentId:I

    iget-object v6, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mTabState:Lcom/google/android/apps/chrome/Tab$TabState;

    iget-object v6, v6, Lcom/google/android/apps/chrome/Tab$TabState;->openerAppId:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/TabModel;->createTabWithNativeContents(IILcom/google/android/apps/chrome/TabModel$TabLaunchType;IILjava/lang/String;)Lcom/google/android/apps/chrome/Tab;

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # operator++ for: Lcom/google/android/apps/chrome/TabPersistentStore;->mNextRestoredIndex:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$1408(Lcom/google/android/apps/chrome/TabPersistentStore;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mNextRestoredIndex:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$1400(Lcom/google/android/apps/chrome/TabPersistentStore;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mSavedActiveIndex:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$1500(Lcom/google/android/apps/chrome/TabPersistentStore;)I

    move-result v1

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # operator++ for: Lcom/google/android/apps/chrome/TabPersistentStore;->mNextRestoredIndex:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$1408(Lcom/google/android/apps/chrome/TabPersistentStore;)I

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # invokes: Lcom/google/android/apps/chrome/TabPersistentStore;->loadNextTab()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$1600(Lcom/google/android/apps/chrome/TabPersistentStore;)V

    goto/16 :goto_0

    :cond_7
    move v1, v4

    goto :goto_1
.end method
