.class Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;
.super Landroid/os/AsyncTask;


# instance fields
.field mListData:[B

.field mStateSaved:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/TabPersistentStore;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/TabPersistentStore;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->mStateSaved:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/TabPersistentStore;Lcom/google/android/apps/chrome/TabPersistentStore$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;-><init>(Lcom/google/android/apps/chrome/TabPersistentStore;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->mListData:[B

    if-nez v0, :cond_0

    :goto_0
    return-object v2

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->mListData:[B

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->mListData:[B

    # invokes: Lcom/google/android/apps/chrome/TabPersistentStore;->saveListToFile([B)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$700(Lcom/google/android/apps/chrome/TabPersistentStore;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    iput-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->mListData:[B

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->mStateSaved:Z

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$802(Lcom/google/android/apps/chrome/TabPersistentStore;Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;)Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # invokes: Lcom/google/android/apps/chrome/TabPersistentStore;->saveListToMemory()[B
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$600(Lcom/google/android/apps/chrome/TabPersistentStore;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->mListData:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->mListData:[B

    goto :goto_0
.end method
