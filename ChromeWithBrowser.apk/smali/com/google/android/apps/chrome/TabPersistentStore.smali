.class public Lcom/google/android/apps/chrome/TabPersistentStore;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final SAVED_STATE_FILE:Ljava/lang/String; = "tab_state"

.field private static final SAVED_STATE_VERSION:I = 0x3

.field private static final TAG:Ljava/lang/String; = "TabPersistentStore"


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mCancelIncognitoTabLoads:Z

.field private mCancelNormalTabLoads:Z

.field private mDestroyed:Z

.field private mLoadTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;

.field private mNextRestoredIndex:I

.field private mSaveListTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;

.field private mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

.field private mSavedActiveId:I

.field private mSavedActiveIndex:I

.field private final mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

.field private mTabsToRestore:Ljava/util/Deque;

.field private final mTabsToSave:Ljava/util/Deque;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/TabPersistentStore;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/TabPersistentStore;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/TabModelSelector;Landroid/app/Activity;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mCancelNormalTabLoads:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mCancelIncognitoTabLoads:Z

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    iput-object p2, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/TabPersistentStore;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mCancelIncognitoTabLoads:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/TabPersistentStore;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mCancelNormalTabLoads:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/TabPersistentStore;)Lcom/google/android/apps/chrome/TabModelSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/TabPersistentStore;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSavedActiveId:I

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/TabPersistentStore;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mNextRestoredIndex:I

    return v0
.end method

.method static synthetic access$1408(Lcom/google/android/apps/chrome/TabPersistentStore;)I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mNextRestoredIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mNextRestoredIndex:I

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/TabPersistentStore;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSavedActiveIndex:I

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/TabPersistentStore;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->loadNextTab()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/TabPersistentStore;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mDestroyed:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/TabPersistentStore;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/TabPersistentStore;Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;)Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/TabPersistentStore;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->saveNextTab()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/TabPersistentStore;)[B
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->saveListToMemory()[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/TabPersistentStore;[B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/TabPersistentStore;->saveListToFile([B)V

    return-void
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/TabPersistentStore;Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;)Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;

    return-object p1
.end method

.method private cleanupAllEncryptedPersistentData()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->fileList()[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    const-string v4, "cryptonito"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private cleanupPersistentData()V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->fileList()[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v6, v4, v3

    const-string v0, "not a number"

    const-string v1, "cryptonito"

    invoke-virtual {v6, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xa

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    :goto_1
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v7, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v7, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, v6}, Lcom/google/android/apps/chrome/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    const-string v1, "tab"

    invoke-virtual {v6, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x3

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move v1, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupPersistentData(Lcom/google/android/apps/chrome/TabModelSelector;)V

    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private cleanupPersistentData(IZ)V
    .locals 1

    invoke-static {p2, p1}, Lcom/google/android/apps/chrome/Tab;->getFilename(ZI)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V

    return-void
.end method

.method private cleanupPersistentDataAtAndAboveId(I)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->fileList()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    const-string v0, "cryptonito"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xa

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-lt v0, p1, :cond_0

    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const-string v0, "tab"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupPersistentDataAtAndAboveId(I)V

    :cond_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method private deleteFileAsync(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/TabPersistentStore$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/TabPersistentStore$1;-><init>(Lcom/google/android/apps/chrome/TabPersistentStore;Ljava/lang/String;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/TabPersistentStore$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private loadNextTab()V
    .locals 4

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mDestroyed:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->cleanupPersistentData()V

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v1, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;-><init>(Lcom/google/android/apps/chrome/TabPersistentStore;Lcom/google/android/apps/chrome/TabPersistentStore$1;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private loadStateInternal()I
    .locals 15

    sget-boolean v0, Lcom/google/android/apps/chrome/TabPersistentStore;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/TabPersistentStore;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v10

    const/4 v1, 0x0

    const/4 v7, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;

    const-string v2, "tab_state"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    :cond_2
    invoke-static {v10}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mNextRestoredIndex:I

    const/4 v7, 0x0

    :goto_0
    return v7

    :cond_3
    :try_start_1
    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;

    const-string v3, "tab_state"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v8, Ljava/io/DataInputStream;

    invoke-direct {v8, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_5

    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/io/DataInputStream;->close()V

    :cond_4
    invoke-static {v10}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mNextRestoredIndex:I

    const/4 v7, 0x0

    goto :goto_0

    :cond_5
    const/4 v0, -0x1

    :try_start_3
    iput v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSavedActiveId:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v11

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v12

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v13

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v14

    if-ltz v12, :cond_6

    if-ge v13, v12, :cond_6

    if-lt v14, v12, :cond_8

    :cond_6
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    :cond_7
    invoke-static {v10}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mNextRestoredIndex:I

    throw v0

    :cond_8
    const/4 v0, 0x0

    move v9, v0

    :goto_2
    if-ge v9, v12, :cond_e

    :try_start_4
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    iget v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSavedActiveId:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_d

    if-ne v9, v13, :cond_9

    if-nez v11, :cond_a

    :cond_9
    if-ne v9, v14, :cond_d

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/Tab;->readState(ILandroid/app/Activity;)Lcom/google/android/apps/chrome/Tab$TabState;

    move-result-object v6

    if-eqz v6, :cond_c

    iput v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSavedActiveId:I

    iput v9, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSavedActiveIndex:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    iget-boolean v2, v6, Lcom/google/android/apps/chrome/Tab$TabState;->isIncognito:Z

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    iget-object v2, v6, Lcom/google/android/apps/chrome/Tab$TabState;->state:[B

    iget v3, v6, Lcom/google/android/apps/chrome/Tab$TabState;->savedStateVersion:I

    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/Tab;->restoreState([BI)I

    move-result v2

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_RESTORE:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const/4 v4, -0x1

    iget v5, v6, Lcom/google/android/apps/chrome/Tab$TabState;->parentId:I

    iget-object v6, v6, Lcom/google/android/apps/chrome/Tab$TabState;->openerAppId:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/TabModel;->createTabWithNativeContents(IILcom/google/android/apps/chrome/TabModel$TabLaunchType;IILjava/lang/String;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    sget-boolean v3, Lcom/google/android/apps/chrome/TabPersistentStore;->$assertionsDisabled:Z

    if-nez v3, :cond_b

    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_b
    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->getTabIndexById(I)I

    move-result v3

    invoke-interface {v0, v3}, Lcom/google/android/apps/chrome/TabModel;->setIndex(I)V

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->requestFocus()Z

    :cond_c
    :goto_3
    if-lt v1, v7, :cond_10

    add-int/lit8 v0, v1, 0x1

    :goto_4
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    move v7, v0

    goto :goto_2

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    :cond_e
    if-eqz v8, :cond_f

    invoke-virtual {v8}, Ljava/io/DataInputStream;->close()V

    :cond_f
    invoke-static {v10}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mNextRestoredIndex:I

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    goto/16 :goto_1

    :cond_10
    move v0, v7

    goto :goto_4
.end method

.method private static logSaveException(Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "TabPersistentStore"

    const-string v1, "Error while saving tabs state; will attempt to continue..."

    invoke-static {v0, v1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method private onStateLoaded()V
    .locals 1

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    return-void
.end method

.method private declared-synchronized saveListToFile([B)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;

    const-string v1, "tab_state"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private saveListToMemory()[B
    .locals 7

    const/4 v1, 0x0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v4, 0x1

    invoke-interface {v0, v4}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v5

    invoke-interface {v4}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    invoke-interface {v5}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v6

    add-int/2addr v0, v6

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-interface {v4}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-interface {v5}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v0

    invoke-interface {v4}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v6

    add-int/2addr v0, v6

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v0, v1

    :goto_0
    invoke-interface {v4}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v6

    if-ge v0, v6, :cond_0

    invoke-interface {v4, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    invoke-interface {v5}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {v5, v1}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private saveNextTab()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    new-instance v1, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;-><init>(Lcom/google/android/apps/chrome/TabPersistentStore;Lcom/google/android/apps/chrome/Tab;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;-><init>(Lcom/google/android/apps/chrome/TabPersistentStore;Lcom/google/android/apps/chrome/TabPersistentStore$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method


# virtual methods
.method public addTabToSaveQueue(Lcom/google/android/apps/chrome/Tab;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->shouldTabStateBePersisted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->saveNextTab()V

    return-void
.end method

.method public cancelLoadingTabs(Z)V
    .locals 1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mCancelIncognitoTabLoads:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mCancelNormalTabLoads:Z

    goto :goto_0
.end method

.method public clearEncryptedState()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->cleanupAllEncryptedPersistentData()V

    return-void
.end method

.method public clearState()V
    .locals 1

    const-string v0, "tab_state"

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->cleanupPersistentData()V

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    return-void
.end method

.method public destroy()V
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mDestroyed:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->cancel(Z)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->cancel(Z)Z

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->cancel(Z)Z

    :cond_4
    return-void
.end method

.method getRestoredTabCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadState()I
    .locals 5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mCancelNormalTabLoads:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mCancelIncognitoTabLoads:Z

    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->loadStateInternal()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    const-string v1, "tab_state"

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->cleanupPersistentDataAtAndAboveId(I)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->loadNextTab()V

    return v0

    :catch_0
    move-exception v1

    const-string v2, "TabPersistentStore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "loadState exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public removeTabFromQueues(Lcom/google/android/apps/chrome/Tab;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->mId:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->access$000(Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;)I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;->cancel(Z)Z

    iput-object v3, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$LoadTabTask;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->loadNextTab()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    iget v0, v0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mId:I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->cancel(Z)Z

    iput-object v3, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->saveNextTab()V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->cleanupPersistentData(IZ)V

    return-void
.end method

.method public saveState()V
    .locals 8

    const/4 v7, 0x0

    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0, v7}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->shouldTabStateBePersisted()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->shouldTabStateBePersisted()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mStateSaved:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    iget-object v0, v0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mTab:Lcom/google/android/apps/chrome/Tab;

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->shouldTabStateBePersisted()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getState()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v6

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/chrome/Tab;->saveState(ILjava/lang/Object;Landroid/app/Activity;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->logSaveException(Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v3

    const-string v3, "TabPersistentStore"

    const-string v4, "Out of memory error while attempting to save tab state.  Erasing."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/chrome/Tab;->deleteStateFile(ILandroid/app/Activity;Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveListTask;->mStateSaved:Z

    if-nez v0, :cond_6

    :cond_5
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabPersistentStore;->saveListToMemory()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->saveListToFile([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_6
    :goto_1
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    return-void

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->logSaveException(Ljava/lang/Exception;)V

    goto :goto_1
.end method
