.class Lcom/google/android/apps/chrome/ToolbarPhone$5;
.super Lcom/google/android/apps/chrome/KeyboardNavigationListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ToolbarPhone;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ToolbarPhone;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$5;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/KeyboardNavigationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public getNextFocusBackward()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$5;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    const v1, 0x7f0f006e

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getNextFocusForward()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$5;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$100(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$5;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$100(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$5;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$100(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$5;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    goto :goto_0
.end method
