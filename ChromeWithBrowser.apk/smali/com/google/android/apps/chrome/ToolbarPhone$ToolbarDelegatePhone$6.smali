.class Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;
.super Landroid/animation/AnimatorListenerAdapter;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

.field final synthetic val$hasFocus:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->val$hasFocus:Z

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->setTranslationX(F)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->val$hasFocus:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v1, v1, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->getMeasuredWidth()I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/ToolbarPhone;->layoutLocationBar(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3000(Lcom/google/android/apps/chrome/ToolbarPhone;IZ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->requestLayout()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mDisableLocationBarRelayout:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3102(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2700(Lcom/google/android/apps/chrome/ToolbarPhone;)Lcom/google/android/apps/chrome/LocationBarPhone;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->val$hasFocus:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBarPhone;->finishUrlFocusChange(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusChangeInProgress:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2902(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2400(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusChangeInProgress:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2902(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->val$hasFocus:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v1, v1, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->getMeasuredWidth()I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/ToolbarPhone;->layoutLocationBar(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3000(Lcom/google/android/apps/chrome/ToolbarPhone;IZ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->requestLayout()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2400(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mDisableLocationBarRelayout:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3102(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z

    goto :goto_0
.end method
