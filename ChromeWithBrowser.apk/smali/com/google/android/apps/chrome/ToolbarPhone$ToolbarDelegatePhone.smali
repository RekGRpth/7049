.class Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;
.super Lcom/google/android/apps/chrome/ToolbarDelegate;


# instance fields
.field private mPreviousLocationBarLeftMargin:F

.field private mPreviousLocationBarWidth:I

.field private mVisualState:Lcom/google/android/apps/chrome/ToolbarPhone$VisualState;

.field final synthetic this$0:Lcom/google/android/apps/chrome/ToolbarPhone;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/ToolbarPhone;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ToolbarDelegate;-><init>(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->updateVisualsForToolbarState(ZZ)V

    return-void
.end method

.method private createEnterOverviewModeAnimation()Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 11

    new-instance v10, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v10}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getLinearInterpolator()Landroid/view/animation/LinearInterpolator;

    move-result-object v9

    new-instance v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$1;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModePercent:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/ToolbarPhone;)F

    move-result v3

    const/high16 v4, 0x3f800000

    const-wide/16 v5, 0xc8

    const-wide/16 v7, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$1;-><init>(Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;Lcom/google/android/apps/chrome/ToolbarPhone;FFJJLandroid/view/animation/Interpolator;)V

    invoke-virtual {v10, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    return-object v10
.end method

.method private createExitOverviewAnimation()Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 11

    new-instance v10, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$2;

    invoke-direct {v10, p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$2;-><init>(Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;)V

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getLinearInterpolator()Landroid/view/animation/LinearInterpolator;

    move-result-object v9

    new-instance v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$3;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModePercent:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/ToolbarPhone;)F

    move-result v3

    const/4 v4, 0x0

    const-wide/16 v5, 0xc8

    const-wide/16 v7, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$3;-><init>(Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;Lcom/google/android/apps/chrome/ToolbarPhone;FFJJLandroid/view/animation/Interpolator;)V

    invoke-virtual {v10, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    return-object v10
.end method

.method private updateVisualsForToolbarState(ZZ)V
    .locals 7

    const v2, 0x7f020093

    const/4 v4, 0x4

    const/4 v5, 0x0

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/ToolbarPhone$VisualState;->OVERVIEW_INCOGNITO:Lcom/google/android/apps/chrome/ToolbarPhone$VisualState;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mVisualState:Lcom/google/android/apps/chrome/ToolbarPhone$VisualState;

    if-ne v1, v0, :cond_3

    :goto_1
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/ToolbarPhone$VisualState;->OVERVIEW_NORMAL:Lcom/google/android/apps/chrome/ToolbarPhone$VisualState;

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/ToolbarPhone$VisualState;->INCOGNITO:Lcom/google/android/apps/chrome/ToolbarPhone$VisualState;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/ToolbarPhone$VisualState;->NORMAL:Lcom/google/android/apps/chrome/ToolbarPhone$VisualState;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuDarkBackground:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    const v3, 0x7f020045

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleStackDarkBackground:Landroid/graphics/drawable/Drawable;
    invoke-static {v3}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3400(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    # invokes: Lcom/google/android/apps/chrome/ToolbarPhone;->paddingSafeSetBackgroundDrawable(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3500(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuDarkBackground:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    const v6, 0x7f0200c0

    invoke-virtual {v3, v6}, Lcom/google/android/apps/chrome/ToolbarPhone;->setBackgroundResource(I)V

    const v3, 0x7f0a000d

    iget-object v6, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v6}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3600(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ProgressBar;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->shouldShowMenuButton()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v6, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mMenuButton:Landroid/widget/ImageButton;

    # invokes: Lcom/google/android/apps/chrome/ToolbarPhone;->paddingSafeSetBackgroundDrawable(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3500(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->getRootView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0f0080

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz p1, :cond_8

    move v1, v4

    :goto_3
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p2, :cond_9

    const v1, 0x7f07002a

    :goto_4
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mVisualState:Lcom/google/android/apps/chrome/ToolbarPhone$VisualState;

    goto/16 :goto_1

    :cond_6
    if-eqz p2, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    const v3, 0x7f020042

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleStackDarkBackground:Landroid/graphics/drawable/Drawable;
    invoke-static {v3}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3400(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    # invokes: Lcom/google/android/apps/chrome/ToolbarPhone;->paddingSafeSetBackgroundDrawable(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3500(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuDarkBackground:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    const v6, 0x7f0200be

    invoke-virtual {v3, v6}, Lcom/google/android/apps/chrome/ToolbarPhone;->setBackgroundResource(I)V

    const v3, 0x7f0a000c

    iget-object v6, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v6}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3600(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ProgressBar;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_2

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    const v2, 0x7f02003f

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleStackLightBackground:Landroid/graphics/drawable/Drawable;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3700(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    # invokes: Lcom/google/android/apps/chrome/ToolbarPhone;->paddingSafeSetBackgroundDrawable(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3500(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    const v2, 0x7f020092

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuLightBackground:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3800(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    const v6, 0x7f0200bc

    invoke-virtual {v3, v6}, Lcom/google/android/apps/chrome/ToolbarPhone;->setBackgroundResource(I)V

    const v3, 0x7f0a000b

    iget-object v6, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v6}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3600(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ProgressBar;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_2

    :cond_8
    move v1, v5

    goto/16 :goto_3

    :cond_9
    const v1, 0x7f070029

    goto/16 :goto_4
.end method


# virtual methods
.method protected handleFindToolbarStateChange(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initializeTabStackVisuals()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected isOverviewMode()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$400(Lcom/google/android/apps/chrome/ToolbarPhone;)Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onClick(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewListener:Landroid/view/View$OnClickListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1600(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewListener:Landroid/view/View$OnClickListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1600(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->toolbarPhoneTabStack()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/Button;

    move-result-object v0

    if-ne v0, p1, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabListener:Landroid/view/View$OnClickListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1700(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabListener:Landroid/view/View$OnClickListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1700(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->stackViewNewTab()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1800(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->stopOrReloadCurrentTab()V

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 7

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    const/4 v3, 0x2

    new-array v3, v3, [I

    invoke-virtual {p1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070020

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    const/16 v5, 0x35

    aget v0, v3, v0

    sub-int v0, v2, v0

    div-int/lit8 v2, v4, 0x2

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->getHeight()I

    move-result v2

    invoke-virtual {v1, v5, v0, v2}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onNativeLibraryReady()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onNativeLibraryReady()V

    return-void
.end method

.method public onStateRestored()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->updateTabCount()V

    return-void
.end method

.method protected onTabModelSelected(Z)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onTabModelSelected(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$400(Lcom/google/android/apps/chrome/ToolbarPhone;)Z

    move-result v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->updateVisualsForToolbarState(ZZ)V

    return-void
.end method

.method protected onUrlFocusChange(Z)V
    .locals 12

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v0, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlHasFocus:Z
    invoke-static {v2, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1902(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onUrlFocusChange(Z)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2000(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2000(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2000(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2002(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2002(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v3

    # invokes: Lcom/google/android/apps/chrome/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2100(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v2

    iget v3, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v4, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I
    invoke-static {v4}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/ToolbarPhone;)I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iput v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mPreviousLocationBarLeftMargin:F

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mPreviousLocationBarWidth:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mPreviousLocationBarWidth:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v3}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2000(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v3

    const/4 v4, 0x4

    new-array v4, v4, [Landroid/animation/Animator;

    iget-object v5, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    iget-object v6, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBgRightMarginProperty:Landroid/util/Property;
    invoke-static {v6}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2300(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/util/Property;

    move-result-object v6

    new-array v7, v9, [I

    iget-object v8, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;
    invoke-static {v8}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2400(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    aput v8, v7, v1

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/chrome/ToolbarPhone;->TRANSLATION_X_ANIM_PROPERTY:Landroid/util/Property;

    new-array v7, v10, [F

    iget v8, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mPreviousLocationBarLeftMargin:F

    aput v8, v7, v1

    aput v0, v7, v9

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v4, v9

    iget-object v5, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBar:Landroid/view/View;
    invoke-static {v5}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2500(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/chrome/ToolbarPhone;->TRANSLATION_X_ANIM_PROPERTY:Landroid/util/Property;

    new-array v7, v9, [F

    aput v0, v7, v1

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v4, v10

    iget-object v5, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;
    invoke-static {v5}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2600(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/chrome/ToolbarPhone;->TRANSLATION_X_ANIM_PROPERTY:Landroid/util/Property;

    new-array v7, v10, [F

    iget-object v8, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;
    invoke-static {v8}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2600(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getTranslationX()F

    move-result v8

    int-to-float v2, v2

    sub-float v2, v8, v2

    iget-object v8, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I
    invoke-static {v8}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/ToolbarPhone;)I

    move-result v8

    mul-int/lit8 v8, v8, 0x3

    int-to-float v8, v8

    add-float/2addr v2, v8

    aput v2, v7, v1

    aput v0, v7, v9

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v4, v11

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2000(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$4;-><init>(Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2000(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$6;-><init>(Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2000(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v0

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2000(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mPreviousLocationBarWidth:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v3}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2000(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v3

    const/4 v4, 0x5

    new-array v4, v4, [Landroid/animation/Animator;

    iget-object v5, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    iget-object v6, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBgRightMarginProperty:Landroid/util/Property;
    invoke-static {v6}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2300(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/util/Property;

    move-result-object v6

    new-array v7, v9, [I

    aput v1, v7, v1

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/chrome/ToolbarPhone;->TRANSLATION_X_ANIM_PROPERTY:Landroid/util/Property;

    new-array v7, v9, [F

    iget v8, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mPreviousLocationBarLeftMargin:F

    aput v8, v7, v1

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v4, v9

    iget-object v5, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBar:Landroid/view/View;
    invoke-static {v5}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2500(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/chrome/ToolbarPhone;->SCROLL_X_ANIM_PROPERTY:Landroid/util/Property;

    new-array v7, v9, [I

    aput v1, v7, v1

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v4, v10

    iget-object v5, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBar:Landroid/view/View;
    invoke-static {v5}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2500(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/chrome/ToolbarPhone;->TRANSLATION_X_ANIM_PROPERTY:Landroid/util/Property;

    new-array v7, v9, [F

    iget-object v8, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;
    invoke-static {v8}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2700(Lcom/google/android/apps/chrome/ToolbarPhone;)Lcom/google/android/apps/chrome/LocationBarPhone;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/LocationBarPhone;->isSecurityButtonShown()Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarUrlSecurityXTranslation:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2800(Lcom/google/android/apps/chrome/ToolbarPhone;)I

    move-result v0

    int-to-float v0, v0

    :cond_2
    aput v0, v7, v1

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v4, v11

    const/4 v5, 0x4

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2600(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/chrome/ToolbarPhone;->TRANSLATION_X_ANIM_PROPERTY:Landroid/util/Property;

    new-array v8, v9, [F

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2700(Lcom/google/android/apps/chrome/ToolbarPhone;)Lcom/google/android/apps/chrome/LocationBarPhone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBarPhone;->isSecurityButtonShown()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarUrlSecurityXTranslation:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2800(Lcom/google/android/apps/chrome/ToolbarPhone;)I

    move-result v0

    :goto_1
    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/ToolbarPhone;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v0, v2

    int-to-float v0, v0

    aput v0, v8, v1

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2000(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$5;-><init>(Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public setOnNewTabClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabListener:Landroid/view/View$OnClickListener;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1702(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setOnOverviewClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewListener:Landroid/view/View$OnClickListener;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1602(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    return-void
.end method

.method protected setOverviewMode(Z)V
    .locals 7

    const/16 v6, 0x11

    const/4 v2, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$400(Lcom/google/android/apps/chrome/ToolbarPhone;)Z

    move-result v0

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # invokes: Lcom/google/android/apps/chrome/ToolbarPhone;->isIncognitoMode()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$800(Lcom/google/android/apps/chrome/ToolbarPhone;)Z

    move-result v1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1000(Lcom/google/android/apps/chrome/ToolbarPhone;)Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    move-result-object v0

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->updateVisualsForToolbarState(ZZ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->createEnterOverviewModeAnimation()Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v2

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1102(Lcom/google/android/apps/chrome/ToolbarPhone;Lcom/google/android/apps/chrome/ChromeAnimation;)Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$600(Lcom/google/android/apps/chrome/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->updateButtonStatus()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->createExitOverviewAnimation()Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v2

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1102(Lcom/google/android/apps/chrome/ToolbarPhone;Lcom/google/android/apps/chrome/ChromeAnimation;)Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    if-eqz v1, :cond_5

    const v0, 0x7f0200be

    :goto_3
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;
    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1202(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/ToolbarPhone;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/ToolbarPhone;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    if-eqz v1, :cond_6

    const v0, 0x7f0a000c

    :goto_4
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTextOverlayColor:I
    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1302(Lcom/google/android/apps/chrome/ToolbarPhone;I)I

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    if-eqz v1, :cond_7

    const v0, 0x7f020043

    :goto_5
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1402(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v4}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->shouldShowMenuButton()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    if-eqz v1, :cond_8

    const v0, 0x7f020093

    :goto_6
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1502(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1500(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHeight()I

    move-result v2

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1500(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$402(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1100(Lcom/google/android/apps/chrome/ToolbarPhone;)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->invalidate()V

    goto/16 :goto_0

    :cond_5
    const v0, 0x7f0200bc

    goto/16 :goto_3

    :cond_6
    const v0, 0x7f0a000b

    goto/16 :goto_4

    :cond_7
    const v0, 0x7f020044

    goto/16 :goto_5

    :cond_8
    const v0, 0x7f020092

    goto :goto_6
.end method

.method protected shouldShowMenuButton()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateReloadButtonVisibility(Z)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateReloadButtonVisibility(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mIsReloading:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3900(Lcom/google/android/apps/chrome/ToolbarPhone;)Z

    move-result v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mIsReloading:Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3902(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1800(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_1

    const v0, 0x7f0701ff

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$1800(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    if-eqz p1, :cond_2

    const v0, 0x7f020028

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f0701fe

    goto :goto_1

    :cond_2
    const v0, 0x7f020025

    goto :goto_2
.end method

.method public updateTabCount()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabCount()V

    return-void
.end method

.method protected updateTabCountVisuals(I)V
    .locals 7

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v0, 0x7f070253

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v5, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v3, :cond_3

    const v0, 0x7f080065

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const v0, 0x7f080063

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    iget-object v6, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v6

    # invokes: Lcom/google/android/apps/chrome/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;
    invoke-static {v1, v6}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$2100(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_5

    const v0, 0x7f07002f

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    if-lez p1, :cond_4

    move v0, v3

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$3200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v0

    if-nez p1, :cond_2

    const/4 v4, 0x4

    :cond_2
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const v0, 0x7f080066

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const v0, 0x7f080064

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    goto :goto_1

    :cond_4
    move v0, v4

    goto :goto_3

    :cond_5
    move-object v0, v2

    goto :goto_2
.end method
