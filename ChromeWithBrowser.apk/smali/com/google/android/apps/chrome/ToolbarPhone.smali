.class public Lcom/google/android/apps/chrome/ToolbarPhone;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Lcom/google/android/apps/chrome/Toolbar;
.implements Lcom/google/android/apps/chrome/compositor/Invalidator$Client;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final ALPHA_ANIM_PROPERTY:Landroid/util/Property;

.field protected static final OVERVIEW_MODE_CHANGE_ANIMATION_DURATION_MS:I = 0xc8

.field public static final SCROLL_X_ANIM_PROPERTY:Landroid/util/Property;

.field public static final TRANSLATION_X_ANIM_PROPERTY:Landroid/util/Property;

.field protected static final URL_FOCUS_CHANGE_ANIMATION_DURATION_MS:I = 0x96


# instance fields
.field private final mBackgroundOverlayBounds:Landroid/graphics/Rect;

.field private final mBrowsingModeViews:Ljava/util/List;

.field private mDisableLocationBarRelayout:Z

.field private mIsReloading:Z

.field private mLocationBarBackground:Landroid/graphics/drawable/NinePatchDrawable;

.field private mLocationBarBgRightMargin:I

.field private final mLocationBarBgRightMarginProperty:Landroid/util/Property;

.field private final mLocationBarIconWidth:I

.field private mMenuButton:Landroid/widget/ImageView;

.field private mMenuDarkBackground:Landroid/graphics/drawable/Drawable;

.field private mMenuLightBackground:Landroid/graphics/drawable/Drawable;

.field private mNewTabButton:Landroid/widget/Button;

.field private mNewTabListener:Landroid/view/View$OnClickListener;

.field private mOptionsTabCountLabel:Landroid/widget/TextView;

.field private mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

.field private mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

.field private mOverviewAnimationTabStackDrawable:Landroid/graphics/drawable/Drawable;

.field private mOverviewAnimationTextOverlayColor:I

.field private mOverviewListener:Landroid/view/View$OnClickListener;

.field private mOverviewMode:Z

.field private mOverviewModeAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mOverviewModePercent:F

.field private final mOverviewModeViews:Ljava/util/List;

.field private mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mToggleStackDarkBackground:Landroid/graphics/drawable/Drawable;

.field private mToggleStackLightBackground:Landroid/graphics/drawable/Drawable;

.field private mToggleTabStackButton:Landroid/widget/ImageButton;

.field private mToolbarButtonsContainer:Landroid/view/View;

.field private mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

.field private final mToolbarHeightWithoutShadow:I

.field private final mToolbarSidePadding:I

.field private final mToolbarUrlSecurityXTranslation:I

.field private mUrlActionButton:Landroid/widget/ImageButton;

.field private mUrlActionsContainer:Landroid/view/View;

.field private final mUrlBackgroundPadding:Landroid/graphics/Rect;

.field private mUrlBar:Landroid/view/View;

.field private mUrlFocusChangeInProgress:Z

.field private mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;

.field private mUrlHasFocus:Z

.field private final mUrlViewportBounds:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ToolbarPhone;->$assertionsDisabled:Z

    new-instance v0, Lcom/google/android/apps/chrome/ToolbarPhone$2;

    const-class v1, Ljava/lang/Float;

    const-string v2, "alpha"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/ToolbarPhone$2;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/ToolbarPhone;->ALPHA_ANIM_PROPERTY:Landroid/util/Property;

    new-instance v0, Lcom/google/android/apps/chrome/ToolbarPhone$3;

    const-class v1, Ljava/lang/Float;

    const-string v2, "translationX"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/ToolbarPhone$3;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/ToolbarPhone;->TRANSLATION_X_ANIM_PROPERTY:Landroid/util/Property;

    new-instance v0, Lcom/google/android/apps/chrome/ToolbarPhone$4;

    const-class v1, Ljava/lang/Integer;

    const-string v2, "scrollX"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/ToolbarPhone$4;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/ToolbarPhone;->SCROLL_X_ANIM_PROPERTY:Landroid/util/Property;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeViews:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewMode:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlHasFocus:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModePercent:F

    iput v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBgRightMargin:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mBackgroundOverlayBounds:Landroid/graphics/Rect;

    new-instance v0, Lcom/google/android/apps/chrome/ToolbarPhone$1;

    const-class v1, Ljava/lang/Integer;

    const-string v2, ""

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/ToolbarPhone$1;-><init>(Lcom/google/android/apps/chrome/ToolbarPhone;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBgRightMarginProperty:Landroid/util/Property;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08006b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    const v1, 0x7f080067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarHeightWithoutShadow:I

    const v1, 0x7f08006c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarUrlSecurityXTranslation:I

    const v1, 0x7f080071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarIconWidth:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ToolbarPhone;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBgRightMargin:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/ToolbarPhone;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBgRightMargin:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/ToolbarPhone;)Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/ToolbarPhone;)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/chrome/ToolbarPhone;Lcom/google/android/apps/chrome/ChromeAnimation;)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/android/apps/chrome/ToolbarPhone;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTextOverlayColor:I

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewListener:Landroid/view/View$OnClickListener;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabListener:Landroid/view/View$OnClickListener;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlHasFocus:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/animation/AnimatorSet;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/ToolbarPhone;Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/ToolbarPhone;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    return v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/util/Property;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBgRightMarginProperty:Landroid/util/Property;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/google/android/apps/chrome/ToolbarPhone;)Lcom/google/android/apps/chrome/LocationBarPhone;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/apps/chrome/ToolbarPhone;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarUrlSecurityXTranslation:I

    return v0
.end method

.method static synthetic access$2902(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusChangeInProgress:Z

    return p1
.end method

.method static synthetic access$3000(Lcom/google/android/apps/chrome/ToolbarPhone;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/ToolbarPhone;->layoutLocationBar(IZ)V

    return-void
.end method

.method static synthetic access$3102(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mDisableLocationBarRelayout:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuDarkBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleStackDarkBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$3500(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->paddingSafeSetBackgroundDrawable(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method static synthetic access$3600(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleStackLightBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuLightBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/google/android/apps/chrome/ToolbarPhone;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mIsReloading:Z

    return v0
.end method

.method static synthetic access$3902(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mIsReloading:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ToolbarPhone;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewMode:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/ToolbarPhone;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewMode:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ToolbarPhone;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModePercent:F

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/ToolbarPhone;F)F
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModePercent:F

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ToolbarPhone;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeViews:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/ToolbarPhone;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/ToolbarPhone;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->isIncognitoMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/ToolbarPhone;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabButton:Landroid/widget/Button;

    return-object v0
.end method

.method private getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    return-object v0
.end method

.method private isIncognitoMode()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mIncognitoTab:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mIncognitoTab:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private layoutLocationBar(IZ)V
    .locals 12

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBar:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v7

    if-eqz p2, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, p1, v0

    iput v0, v5, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    iput v0, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarIconWidth:I

    iget v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    add-int/2addr v0, v1

    iput v0, v7, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBarPhone;->isSecurityButtonShown()Z

    move-result v2

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    mul-int/lit8 v1, v0, 0x2

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    if-eqz v2, :cond_1

    iget v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    const/4 v0, 0x0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int v2, p1, v2

    sub-int v8, v2, v1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    :goto_1
    iget-object v9, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/LocationBarPhone;->getChildCount()I

    move-result v9

    if-ge v1, v9, :cond_7

    iget-object v9, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v9, v1}, Lcom/google/android/apps/chrome/LocationBarPhone;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v10

    const/16 v11, 0x8

    if-eq v10, v11, :cond_4

    const v10, 0x7f0f006a

    invoke-virtual {p0, v10}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-ne v9, v10, :cond_2

    iget-object v10, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/LocationBarPhone;->isSecurityButtonShown()Z

    move-result v10

    if-nez v10, :cond_3

    :cond_2
    const v10, 0x7f0f006d

    invoke-virtual {p0, v10}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-eq v9, v10, :cond_3

    iget-object v10, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBar:Landroid/view/View;

    if-eq v9, v10, :cond_3

    iget-object v10, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;

    if-ne v9, v10, :cond_5

    :cond_3
    const/4 v2, 0x0

    :cond_4
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    if-eqz v2, :cond_6

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v4, v9

    goto :goto_2

    :cond_6
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v3, v9

    goto :goto_2

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBar:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    add-int v1, v8, v4

    add-int/2addr v1, v3

    iput v1, v5, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    sub-int/2addr v1, v4

    add-int/2addr v0, v1

    iput v0, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarIconWidth:I

    iput v0, v7, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    const/4 v0, 0x0

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_0
.end method

.method private static paddingSafeSetBackgroundDrawable(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    .locals 5

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method private static translateCanvasToView(Landroid/view/View;Landroid/view/View;Landroid/graphics/Canvas;)V
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/ToolbarPhone;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/ToolbarPhone;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object p1, v0

    :cond_2
    if-eq p1, p0, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "View \'to\' was not a desendent of \'from\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 10

    const/4 v9, 0x0

    const/16 v8, 0xff

    const/4 v7, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->update()Z

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBarPhone;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v9

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBarPhone;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getMeasuredWidth()I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBgRightMargin:I

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/LocationBarPhone;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusChangeInProgress:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->invalidate()V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v1, :cond_6

    const/high16 v1, 0x3f800000

    iget v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModePercent:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x437f0000

    mul-float/2addr v2, v1

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModePercent:F

    neg-float v3, v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mBackgroundOverlayBounds:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    invoke-virtual {p1, v9, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mBackgroundOverlayBounds:Landroid/graphics/Rect;

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/LocationBarPhone;->getAlpha()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/chrome/LocationBarPhone;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {p0, p1, v1, v4, v5}, Lcom/google/android/apps/chrome/ToolbarPhone;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/LocationBarPhone;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    invoke-static {p0, v1, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->translateCanvasToView(Landroid/view/View;Landroid/view/View;Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;

    invoke-static {v1, v3, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->translateCanvasToView(Landroid/view/View;Landroid/view/View;Landroid/graphics/Canvas;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;

    invoke-static {v3, v4, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->translateCanvasToView(Landroid/view/View;Landroid/view/View;Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;

    iget v4, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTextOverlayColor:I

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTextOverlayColor:I

    invoke-static {v5}, Landroid/graphics/Color;->green(I)I

    move-result v5

    iget v6, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTextOverlayColor:I

    invoke-static {v6}, Landroid/graphics/Color;->blue(I)I

    move-result v6

    invoke-static {v2, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    invoke-static {v1, v3, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->translateCanvasToView(Landroid/view/View;Landroid/view/View;Landroid/graphics/Canvas;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    if-eqz v0, :cond_5

    iput-object v7, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iput-object v7, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iput-object v7, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iput-object v7, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewMode:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->isIncognitoMode()Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->updateVisualsForToolbarState(ZZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->access$300(Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;ZZ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->invalidate()V

    :cond_6
    return-void
.end method

.method public doInvalidate()V
    .locals 0

    invoke-super {p0}, Landroid/widget/FrameLayout;->invalidate()V

    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 6

    const/16 v1, 0xff

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewMode:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeViews:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewMode:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    if-ne p2, v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/LocationBarPhone;->getAlpha()F

    move-result v2

    float-to-double v2, v2

    const-wide/high16 v4, 0x4008000000000000L

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    const-wide v4, 0x406fe00000000000L

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/NinePatchDrawable;->setAlpha(I)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-ge v2, v1, :cond_7

    :goto_0
    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBarPhone;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusChangeInProgress:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBarPhone;->isSecurityButtonShown()Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarSidePadding:I

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    :cond_4
    :goto_1
    const/4 v0, 0x1

    :cond_5
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v1

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_6
    return v1

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlFocusChangeInProgress:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabButton:Landroid/widget/Button;

    if-ne p2, v1, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    goto :goto_1

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    goto :goto_1
.end method

.method public getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public invalidate()V
    .locals 0

    invoke-static {p0}, Lcom/google/android/apps/chrome/compositor/Invalidator;->invalidate(Lcom/google/android/apps/chrome/compositor/Invalidator$Client;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/chrome/AndroidUtils;->insertAfter(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)I

    move-result v0

    sget-boolean v1, Lcom/google/android/apps/chrome/ToolbarPhone;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->onDetachedFromWindow()V

    return-void
.end method

.method public onFinishInflate()V
    .locals 7

    const/4 v4, 0x1

    const/high16 v6, -0x80000000

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    new-instance v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;-><init>(Lcom/google/android/apps/chrome/ToolbarPhone;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/LocationBarPhone;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    const v0, 0x7f0f00c9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    const v0, 0x7f0f006f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;

    new-instance v2, Lcom/google/android/apps/chrome/ToolbarPhone$5;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/ToolbarPhone$5;-><init>(Lcom/google/android/apps/chrome/ToolbarPhone;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleStackDarkBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v2, v4, [I

    const v3, 0x101030e

    aput v3, v2, v1

    invoke-virtual {v0, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToggleStackLightBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    const v0, 0x7f0f00bd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f00cb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOptionsTabCountLabel:Landroid/widget/TextView;

    const v0, 0x7f0f006e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBar:Landroid/view/View;

    const v0, 0x7f0f0074

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuDarkBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuLightBackground:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    new-instance v2, Lcom/google/android/apps/chrome/ToolbarPhone$6;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/ToolbarPhone$6;-><init>(Lcom/google/android/apps/chrome/ToolbarPhone;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v0, 0x7f0f0070

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;

    const v0, 0x7f0f0071

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlActionButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-boolean v4, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mIsReloading:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeViews:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabButton:Landroid/widget/Button;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200b7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/NinePatchDrawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/LocationBarPhone;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/apps/chrome/LocationBarPhone;->setPadding(IIII)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v2, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->mMenuButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->shouldShowMenuButton()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    iget v3, v2, Landroid/graphics/Point;->x:I

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mNewTabButton:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    const v0, 0x7f0f00bb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarHeightWithoutShadow:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    instance-of v2, v0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    const/high16 v2, 0x1020000

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mDisableLocationBarRelayout:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mUrlHasFocus:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/ToolbarPhone;->layoutLocationBar(IZ)V

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mBackgroundOverlayBounds:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mToolbarHeightWithoutShadow:I

    invoke-virtual {v0, v2, v2, p1, v1}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2, v2, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    return-void
.end method

.method protected setLoadProgress(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method
