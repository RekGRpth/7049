.class Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;
.super Lcom/google/android/apps/chrome/ToolbarDelegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ToolbarTablet;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/ToolbarTablet;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ToolbarDelegate;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onAccessibilityStatusChanged(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$500(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected onBookmarkUiVisibilityChange(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$400(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->updateButtonStatus()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onClick(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    if-ne v0, p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->back()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v1}, Lcom/google/android/apps/chrome/UmaRecordAction;->back(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    if-ne v0, p1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->forward()Z

    invoke-static {v1}, Lcom/google/android/apps/chrome/UmaRecordAction;->forward(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$300(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    if-ne v0, p1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->stopOrReloadCurrentTab()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$400(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    if-ne v0, p1, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkListener:Landroid/view/View$OnClickListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$600(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/view/View$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkListener:Landroid/view/View$OnClickListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$600(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$400(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->toolbarToggleBookmark()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$500(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mOverviewListener:Landroid/view/View$OnClickListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$700(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/view/View$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mOverviewListener:Landroid/view/View$OnClickListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$700(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$500(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setBookmarkClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # setter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkListener:Landroid/view/View$OnClickListener;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$602(Lcom/google/android/apps/chrome/ToolbarTablet;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setOnOverviewClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # setter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mOverviewListener:Landroid/view/View$OnClickListener;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$702(Lcom/google/android/apps/chrome/ToolbarTablet;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    return-void
.end method

.method protected setOverviewMode(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->shouldUseAccessibilityMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mInOverviewMode:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$202(Lcom/google/android/apps/chrome/ToolbarTablet;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$300(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->updateButtonStatus()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # setter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mInOverviewMode:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$202(Lcom/google/android/apps/chrome/ToolbarTablet;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/LocationBar;->setVisibility(I)V

    goto :goto_0
.end method

.method protected updateBackButtonVisibility(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mInOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$200(Lcom/google/android/apps/chrome/ToolbarTablet;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateBookmarkButtonVisibility(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$400(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$400(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f02000f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method protected updateForwardButtonVisibility(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mInOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$200(Lcom/google/android/apps/chrome/ToolbarTablet;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateReloadButtonVisibility(Z)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$300(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f020038

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$300(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0701ff

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$300(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mInOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$200(Lcom/google/android/apps/chrome/ToolbarTablet;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$300(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f020035

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$300(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0701fe

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
