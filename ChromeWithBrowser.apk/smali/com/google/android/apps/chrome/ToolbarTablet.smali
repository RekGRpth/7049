.class public Lcom/google/android/apps/chrome/ToolbarTablet;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lcom/google/android/apps/chrome/Toolbar;


# instance fields
.field private mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

.field private mBackButton:Landroid/widget/ImageButton;

.field private mBookmarkButton:Landroid/widget/ImageButton;

.field private mBookmarkListener:Landroid/view/View$OnClickListener;

.field private mForwardButton:Landroid/widget/ImageButton;

.field private mInOverviewMode:Z

.field private mOverviewListener:Landroid/view/View$OnClickListener;

.field private mReloadButton:Landroid/widget/ImageButton;

.field private mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mInOverviewMode:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ToolbarTablet;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mInOverviewMode:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/ToolbarTablet;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mInOverviewMode:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/ToolbarTablet;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkListener:Landroid/view/View$OnClickListener;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mOverviewListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/ToolbarTablet;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mOverviewListener:Landroid/view/View$OnClickListener;

    return-object p1
.end method


# virtual methods
.method public getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;->onDetachedFromWindow()V

    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    new-instance v0, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;-><init>(Lcom/google/android/apps/chrome/ToolbarTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;

    const v0, 0x7f0f00cc

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/ToolbarTablet$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ToolbarTablet$1;-><init>(Lcom/google/android/apps/chrome/ToolbarTablet;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v0, 0x7f0f00cd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/ToolbarTablet$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ToolbarTablet$2;-><init>(Lcom/google/android/apps/chrome/ToolbarTablet;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v0, 0x7f0f00ce

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/ToolbarTablet$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ToolbarTablet$3;-><init>(Lcom/google/android/apps/chrome/ToolbarTablet;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v0, 0x7f0f006f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->shouldUseAccessibilityMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    const v0, 0x7f0f0075

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0074

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/ToolbarTablet$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ToolbarTablet$4;-><init>(Lcom/google/android/apps/chrome/ToolbarTablet;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
