.class public Lcom/google/android/apps/chrome/UmaRecordAction;
.super Ljava/lang/Object;


# static fields
.field private static sDisableForTesting:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static back(Z)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeUmaRecordBack(Z)V

    goto :goto_0
.end method

.method public static beamCallbackSuccess()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordBeamCallbackSuccess()V

    goto :goto_0
.end method

.method public static beamInvalidAppState()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordBeamInvalidAppState()V

    goto :goto_0
.end method

.method public static crashUploadAttempt()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordCrashUploadAttempt()V

    goto :goto_0
.end method

.method public static disableForTesting()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    return-void
.end method

.method public static forward(Z)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeUmaRecordForward(Z)V

    goto :goto_0
.end method

.method public static freSignInAttempted()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordFreSignInAttempted()V

    goto :goto_0
.end method

.method public static freSignInSuccessful()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordFreSignInSuccessful()V

    goto :goto_0
.end method

.method public static freSkipSignIn()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordFreSkipSignIn()V

    goto :goto_0
.end method

.method public static menuAddToBookmarks()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuAddToBookmarks()V

    goto :goto_0
.end method

.method public static menuAllBookmarks()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuAllBookmarks()V

    goto :goto_0
.end method

.method public static menuCloseAllTabs()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuCloseAllTabs()V

    goto :goto_0
.end method

.method public static menuCloseTab()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuCloseTab()V

    goto :goto_0
.end method

.method public static menuFeedback()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuFeedback()V

    goto :goto_0
.end method

.method public static menuFindInPage()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuFindInPage()V

    goto :goto_0
.end method

.method public static menuFullscreen()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuFullscreen()V

    goto :goto_0
.end method

.method public static menuNewIncognitoTab()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuNewIncognitoTab()V

    goto :goto_0
.end method

.method public static menuNewTab()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuNewTab()V

    goto :goto_0
.end method

.method public static menuOpenTabs()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuOpenTabs()V

    goto :goto_0
.end method

.method public static menuSettings()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuSettings()V

    goto :goto_0
.end method

.method public static menuShare()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuShare()V

    goto :goto_0
.end method

.method public static menuShow()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordMenuShow()V

    goto :goto_0
.end method

.method private static native nativeRecordBeamCallbackSuccess()V
.end method

.method private static native nativeRecordBeamInvalidAppState()V
.end method

.method private static native nativeRecordContextMenuCopyLinkAddress()V
.end method

.method private static native nativeRecordContextMenuCopyLinkText()V
.end method

.method private static native nativeRecordContextMenuImage()V
.end method

.method private static native nativeRecordContextMenuLink()V
.end method

.method private static native nativeRecordContextMenuOpenImageInNewTab()V
.end method

.method private static native nativeRecordContextMenuOpenNewIncognitoTab()V
.end method

.method private static native nativeRecordContextMenuOpenNewTab()V
.end method

.method private static native nativeRecordContextMenuSaveImage()V
.end method

.method private static native nativeRecordContextMenuText()V
.end method

.method private static native nativeRecordContextMenuViewImage()V
.end method

.method private static native nativeRecordCrashUploadAttempt()V
.end method

.method private static native nativeRecordFreSignInAttempted()V
.end method

.method private static native nativeRecordFreSignInSuccessful()V
.end method

.method private static native nativeRecordFreSkipSignIn()V
.end method

.method private static native nativeRecordMenuAddToBookmarks()V
.end method

.method private static native nativeRecordMenuAllBookmarks()V
.end method

.method private static native nativeRecordMenuCloseAllTabs()V
.end method

.method private static native nativeRecordMenuCloseTab()V
.end method

.method private static native nativeRecordMenuFeedback()V
.end method

.method private static native nativeRecordMenuFindInPage()V
.end method

.method private static native nativeRecordMenuFullscreen()V
.end method

.method private static native nativeRecordMenuNewIncognitoTab()V
.end method

.method private static native nativeRecordMenuNewTab()V
.end method

.method private static native nativeRecordMenuOpenTabs()V
.end method

.method private static native nativeRecordMenuSettings()V
.end method

.method private static native nativeRecordMenuShare()V
.end method

.method private static native nativeRecordMenuShow()V
.end method

.method private static native nativeRecordNtpSectionBookmarks()V
.end method

.method private static native nativeRecordNtpSectionIncognito()V
.end method

.method private static native nativeRecordNtpSectionMostVisited()V
.end method

.method private static native nativeRecordNtpSectionOpenTabs()V
.end method

.method private static native nativeRecordOmniboxSearch()V
.end method

.method private static native nativeRecordOmniboxVoiceSearch()V
.end method

.method private static native nativeRecordPageLoaded(IIZ)V
.end method

.method private static native nativeRecordPageLoadedWithKeyboard()V
.end method

.method private static native nativeRecordReceivedExternalIntent()V
.end method

.method private static native nativeRecordShortcutAllBookmarks()V
.end method

.method private static native nativeRecordShortcutFindInPage()V
.end method

.method private static native nativeRecordShortcutNewIncognitoTab()V
.end method

.method private static native nativeRecordShortcutNewTab()V
.end method

.method private static native nativeRecordSideSwipeFinished()V
.end method

.method private static native nativeRecordStackViewCloseTab()V
.end method

.method private static native nativeRecordStackViewNewTab()V
.end method

.method private static native nativeRecordStackViewSwipeCloseTab()V
.end method

.method private static native nativeRecordTabClobbered()V
.end method

.method private static native nativeRecordTabStripCloseTab()V
.end method

.method private static native nativeRecordTabStripNewTab()V
.end method

.method private static native nativeRecordTabSwitched()V
.end method

.method private static native nativeRecordToolbarPhoneTabStack()V
.end method

.method private static native nativeRecordToolbarShowMenu()V
.end method

.method private static native nativeRecordToolbarToggleBookmark()V
.end method

.method private static native nativeUmaRecordBack(Z)V
.end method

.method private static native nativeUmaRecordForward(Z)V
.end method

.method private static native nativeUmaRecordReload(Z)V
.end method

.method private static native nativeUmaRecordSystemBack()V
.end method

.method private static native nativeUmaRecordSystemBackForNavigation()V
.end method

.method public static ntpSectionBookmarks()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordNtpSectionBookmarks()V

    goto :goto_0
.end method

.method public static ntpSectionIncognito()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordNtpSectionIncognito()V

    goto :goto_0
.end method

.method public static ntpSectionMostVisited()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordNtpSectionMostVisited()V

    goto :goto_0
.end method

.method public static ntpSectionOpenTabs()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordNtpSectionOpenTabs()V

    goto :goto_0
.end method

.method public static omniboxSearch()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordOmniboxSearch()V

    goto :goto_0
.end method

.method public static omniboxVoiceSearch()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordOmniboxVoiceSearch()V

    goto :goto_0
.end method

.method public static pageLoaded(IIZZ)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordPageLoaded(IIZ)V

    if-eqz p2, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordPageLoadedWithKeyboard()V

    goto :goto_0
.end method

.method public static receivedExternalIntent()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordReceivedExternalIntent()V

    goto :goto_0
.end method

.method public static recordContextMenuItemSelected(Landroid/view/MenuItem;)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordContextMenuOpenNewTab()V

    goto :goto_0

    :pswitch_2
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordContextMenuOpenNewIncognitoTab()V

    goto :goto_0

    :pswitch_3
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordContextMenuViewImage()V

    goto :goto_0

    :pswitch_4
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordContextMenuCopyLinkAddress()V

    goto :goto_0

    :pswitch_5
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordContextMenuCopyLinkText()V

    goto :goto_0

    :pswitch_6
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordContextMenuSaveImage()V

    goto :goto_0

    :pswitch_7
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordContextMenuOpenImageInNewTab()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0f00e7
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_7
    .end packed-switch
.end method

.method public static recordShowContextMenu(ZZZ)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordContextMenuLink()V

    goto :goto_0

    :cond_2
    if-eqz p0, :cond_3

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordContextMenuImage()V

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordContextMenuText()V

    goto :goto_0
.end method

.method public static reload(Z)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeUmaRecordReload(Z)V

    goto :goto_0
.end method

.method public static shortcutAllBookmarks()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordShortcutAllBookmarks()V

    goto :goto_0
.end method

.method public static shortcutFindInPage()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordShortcutFindInPage()V

    goto :goto_0
.end method

.method public static shortcutNewIncognitoTab()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordShortcutNewIncognitoTab()V

    goto :goto_0
.end method

.method public static shortcutNewTab()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordShortcutNewTab()V

    goto :goto_0
.end method

.method public static stackViewCloseTab()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordStackViewCloseTab()V

    goto :goto_0
.end method

.method public static stackViewNewTab()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordStackViewNewTab()V

    goto :goto_0
.end method

.method public static stackViewSwipeCloseTab()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordStackViewSwipeCloseTab()V

    goto :goto_0
.end method

.method public static systemBack()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeUmaRecordSystemBack()V

    goto :goto_0
.end method

.method public static systemBackForNavigation()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeUmaRecordSystemBackForNavigation()V

    goto :goto_0
.end method

.method public static tabClobbered()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordTabClobbered()V

    goto :goto_0
.end method

.method public static tabSideSwipeFinished()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordSideSwipeFinished()V

    goto :goto_0
.end method

.method public static tabStripCloseTab()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordTabStripCloseTab()V

    goto :goto_0
.end method

.method public static tabStripNewTab()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordTabStripNewTab()V

    goto :goto_0
.end method

.method public static tabSwitched()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordTabSwitched()V

    goto :goto_0
.end method

.method public static toolbarPhoneTabStack()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordToolbarPhoneTabStack()V

    goto :goto_0
.end method

.method public static toolbarShowMenu()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordToolbarShowMenu()V

    goto :goto_0
.end method

.method public static toolbarToggleBookmark()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaRecordAction;->sDisableForTesting:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->nativeRecordToolbarToggleBookmark()V

    goto :goto_0
.end method
