.class Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/UmaStatsOptInListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;->this$0:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;->this$0:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;->this$0:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    # getter for: Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mCount:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->access$000(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mCount:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->access$002(Lcom/google/android/apps/chrome/UmaStatsOptInListener;I)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;->this$0:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    # getter for: Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->access$100(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "Pages towards showing uma info bar"

    iget-object v2, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;->this$0:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    # getter for: Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mCount:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->access$000(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;->this$0:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    # getter for: Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mCount:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->access$000(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)I

    move-result v0

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;->this$0:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    # invokes: Lcom/google/android/apps/chrome/UmaStatsOptInListener;->showUmaStatsOptInInfoBar()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->access$200(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)V

    :cond_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method
