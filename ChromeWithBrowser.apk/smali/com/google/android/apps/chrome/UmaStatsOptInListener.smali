.class public Lcom/google/android/apps/chrome/UmaStatsOptInListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lorg/chromium/base/ActivityStatus$StateListener;


# static fields
.field private static final PAGE_LOADS_TO_SHOW:I = 0x14

.field public static final PAGE_LOADS_UMA_OPT_IN:Ljava/lang/String; = "Pages towards showing uma info bar"


# instance fields
.field private mCount:I

.field private mInfoBar:Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;

.field private mMain:Lcom/google/android/apps/chrome/Main;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/Main;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener$1;-><init>(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    iput-object p1, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mMain:Lcom/google/android/apps/chrome/Main;

    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mPrefs:Landroid/content/SharedPreferences;

    iput p2, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mCount:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isNeverUploadCrashDump()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->onUserResponse()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->handleNotifications(Z)V

    invoke-static {p0}, Lorg/chromium/base/ActivityStatus;->registerStateListener(Lorg/chromium/base/ActivityStatus$StateListener;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mCount:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/UmaStatsOptInListener;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mCount:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->showUmaStatsOptInInfoBar()V

    return-void
.end method

.method private handleNotifications(Z)V
    .locals 2

    const/16 v1, 0x9

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    goto :goto_0
.end method

.method private showUmaStatsOptInInfoBar()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mInfoBar:Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mInfoBar:Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;->getTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-eq v0, v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mInfoBar:Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mInfoBar:Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;->dismiss()V

    :cond_3
    new-instance v1, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;-><init>(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mInfoBar:Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mInfoBar:Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;->setExpireOnNavigation(Z)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mInfoBar:Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityStateChange(I)V
    .locals 2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->handleNotifications(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mMain:Lcom/google/android/apps/chrome/Main;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->setCrashUploadPreference(Landroid/content/Context;Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mInfoBar:Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;->dismiss()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->onUserResponse()V

    return-void
.end method

.method public onUserResponse()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "Pages towards showing uma info bar"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->handleNotifications(Z)V

    invoke-static {p0}, Lorg/chromium/base/ActivityStatus;->unregisterStateListener(Lorg/chromium/base/ActivityStatus$StateListener;)V

    return-void
.end method
