.class public Lcom/google/android/apps/chrome/UrlHandler;
.super Ljava/lang/Object;


# static fields
.field private static final SCHEME_WTAI:Ljava/lang/String; = "wtai://wp/"

.field private static final SCHEME_WTAI_AP:Ljava/lang/String; = "wtai://wp/ap;"

.field private static final SCHEME_WTAI_MC:Ljava/lang/String; = "wtai://wp/mc;"

.field private static final SCHEME_WTAI_SD:Ljava/lang/String; = "wtai://wp/sd;"


# instance fields
.field private mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/chrome/UrlHandler$ActivityEnvironment;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/UrlHandler$ActivityEnvironment;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/UrlHandler$Environment;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    return-void
.end method

.method private shouldOverrideInternal(Ljava/lang/String;Z)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "wtai://wp/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "wtai://wp/mc;"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tel:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v4, 0xd

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v2, v1}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "wtai://wp/sd;"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v2, "wtai://wp/ap;"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const-string v2, "about:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    const-string v2, "content:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/UrlHandler;->startActivityForUrl(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private startActivityForUrl(Ljava/lang/String;Z)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    invoke-static {p1, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v3, v2}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->canResolveActivity(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "market://search?q=pname:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "android.intent.category.BROWSABLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v2, v1}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v2, "Chrome"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bad URI "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const-string v3, "android.intent.category.BROWSABLE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v3, "com.android.browser.application_id"

    iget-object v4, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v4}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcom/google/android/apps/chrome/utilities/URLUtilities;->ACCEPTED_URI_SCHEMA:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v3, v2}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->isSpecializedHandlerAvailable(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v3, v2}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->willChromeHandleIntent(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v3, v2}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->startIncognitoIntent(Landroid/content/Intent;)V

    goto :goto_0

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->startActivityIfNeeded(Landroid/content/Intent;)Z
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public shouldOverrideNewTab(Ljava/lang/String;Z)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/UrlHandler;->shouldOverrideInternal(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public shouldOverrideUrlLoading(Ljava/lang/String;Ljava/lang/String;ZIZ)Z
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    and-int/lit16 v0, p4, 0xff

    if-nez v0, :cond_1

    move v8, v1

    :goto_0
    if-ne v0, v1, :cond_2

    move v7, v1

    :goto_1
    const/4 v3, 0x7

    if-ne v0, v3, :cond_3

    move v6, v1

    :goto_2
    const/high16 v0, 0x8000000

    and-int/2addr v0, p4

    if-eqz v0, :cond_4

    move v3, v1

    :goto_3
    const/high16 v0, 0x1000000

    and-int/2addr v0, p4

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    sget-object v4, Lcom/google/android/apps/chrome/utilities/URLUtilities;->ACCEPTED_URI_SCHEMA:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-nez v4, :cond_6

    move v5, v1

    :goto_5
    if-eqz v0, :cond_7

    :cond_0
    :goto_6
    return v2

    :cond_1
    move v8, v2

    goto :goto_0

    :cond_2
    move v7, v2

    goto :goto_1

    :cond_3
    move v6, v2

    goto :goto_2

    :cond_4
    move v3, v2

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    move v5, v2

    goto :goto_5

    :cond_7
    if-eqz v8, :cond_a

    if-nez v3, :cond_a

    move v4, v1

    :goto_7
    if-eqz v8, :cond_b

    if-eqz v3, :cond_b

    if-eqz p5, :cond_b

    move v3, v1

    :goto_8
    if-eqz v6, :cond_c

    if-eqz p5, :cond_c

    move v0, v1

    :goto_9
    if-eqz v7, :cond_d

    if-eqz p5, :cond_d

    if-eqz v5, :cond_d

    :goto_a
    if-nez v4, :cond_8

    if-nez v1, :cond_8

    if-nez v3, :cond_8

    if-eqz v0, :cond_0

    :cond_8
    if-eqz p2, :cond_9

    const-string v0, "chrome://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_9
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/chrome/UrlHandler;->shouldOverrideInternal(Ljava/lang/String;Z)Z

    move-result v2

    goto :goto_6

    :cond_a
    move v4, v2

    goto :goto_7

    :cond_b
    move v3, v2

    goto :goto_8

    :cond_c
    move v0, v2

    goto :goto_9

    :cond_d
    move v1, v2

    goto :goto_a
.end method
