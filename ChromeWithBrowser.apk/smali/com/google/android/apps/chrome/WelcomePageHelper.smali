.class Lcom/google/android/apps/chrome/WelcomePageHelper;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final NOTIFICATIONS:[I

.field private final mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mTab:Lcom/google/android/apps/chrome/Tab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/WelcomePageHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/WelcomePageHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/chrome/Tab;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->NOTIFICATIONS:[I

    new-instance v0, Lcom/google/android/apps/chrome/WelcomePageHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/WelcomePageHelper$1;-><init>(Lcom/google/android/apps/chrome/WelcomePageHelper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    iput-object p1, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mTab:Lcom/google/android/apps/chrome/Tab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->addSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x3
        0x9
        0x1b
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/WelcomePageHelper;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mTab:Lcom/google/android/apps/chrome/Tab;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/WelcomePageHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->maybeShowSettings()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/WelcomePageHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->sendSyncEnabled()V

    return-void
.end method

.method private getContext()Landroid/content/Context;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method

.method private isSyncEnabled()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method private maybeShowSettings()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "chrome://welcome/#settings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->goBack()V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x20020000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, ":android:show_fragment"

    const-class v3, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private sendSyncEnabled()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "chrome://welcome/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->isSyncEnabled()Z

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "document.getElementById(\'footer-container\').style.display = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_2

    const-string v0, "\'inline\'"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/chromium/content/browser/ContentView;->evaluateJavaScript(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/chrome/WelcomePageHelper;->TAG:Ljava/lang/String;

    const-string v1, "Can\'t send syncEnabled - ContentView was destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v0, "\'none\'"
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method protected destroy()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->removeSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mTab:Lcom/google/android/apps/chrome/Tab;

    goto :goto_0
.end method

.method protected isDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper;->mTab:Lcom/google/android/apps/chrome/Tab;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public syncStateChanged()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->sendSyncEnabled()V

    goto :goto_0
.end method
