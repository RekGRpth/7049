.class Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/CompositorContentView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/compositor/CompositorContentView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorContentView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorContentView;Lcom/google/android/apps/chrome/compositor/CompositorContentView$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorContentView;)V

    return-void
.end method


# virtual methods
.method public awakenScrollBars()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorContentView;

    # invokes: Landroid/view/View;->awakenScrollBars()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorContentView;->access$701(Lcom/google/android/apps/chrome/compositor/CompositorContentView;)Z

    move-result v0

    return v0
.end method

.method public drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "drawChild must not be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onScrollChanged(IIII)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorContentView;

    # invokes: Landroid/view/View;->onScrollChanged(IIII)V
    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/compositor/CompositorContentView;->access$601(Lcom/google/android/apps/chrome/compositor/CompositorContentView;IIII)V

    return-void
.end method

.method public super_awakenScrollBars(IZ)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorContentView;

    # invokes: Landroid/view/View;->awakenScrollBars(IZ)Z
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/chrome/compositor/CompositorContentView;->access$801(Lcom/google/android/apps/chrome/compositor/CompositorContentView;IZ)Z

    move-result v0

    return v0
.end method

.method public super_dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorContentView;

    # invokes: Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorContentView;->access$301(Lcom/google/android/apps/chrome/compositor/CompositorContentView;Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public super_dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorContentView;

    # invokes: Landroid/view/View;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorContentView;->access$201(Lcom/google/android/apps/chrome/compositor/CompositorContentView;Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public super_onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorContentView;

    # invokes: Landroid/view/View;->onConfigurationChanged(Landroid/content/res/Configuration;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorContentView;->access$501(Lcom/google/android/apps/chrome/compositor/CompositorContentView;Landroid/content/res/Configuration;)V

    return-void
.end method

.method public super_onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorContentView;

    # invokes: Landroid/view/View;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorContentView;->access$401(Lcom/google/android/apps/chrome/compositor/CompositorContentView;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public super_onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorContentView$InternalAccessAdapter;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorContentView;

    # invokes: Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/chrome/compositor/CompositorContentView;->access$101(Lcom/google/android/apps/chrome/compositor/CompositorContentView;ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method
