.class public Lcom/google/android/apps/chrome/compositor/CompositorView;
.super Landroid/view/SurfaceView;

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/google/android/apps/chrome/gl/GLResourceProvider;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mLastLayerCount:I

.field private mNativeCompositorView:I

.field private final mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->setVisibility(I)V

    return-void
.end method

.method private native nativeClearLayers(I)V
.end method

.method private native nativeCommitLayers(I)V
.end method

.method private native nativeCompositeToBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private native nativeCopyTextureToBitmap(IIIILandroid/graphics/Bitmap;)Z
.end method

.method private native nativeDeleteTexture(II)V
.end method

.method private native nativeDestroy(I)V
.end method

.method private native nativeGenerateCompressedTexture(IIIILjava/nio/ByteBuffer;)I
.end method

.method private native nativeGenerateTexture(ILandroid/graphics/Bitmap;)I
.end method

.method private native nativeGetContentTexture(IFLandroid/graphics/Rect;I)I
.end method

.method private native nativeInit()I
.end method

.method private native nativePutLayer(IIZIZFFFFFFFFFFFFFFFFFFFF)V
.end method

.method private native nativeSetLayoutViewport(IFFFF)V
.end method

.method private native nativeSetStaticLayer(IIZIFFFFFF)V
.end method

.method private native nativeSurfaceCreated(ILandroid/view/Surface;)V
.end method

.method private native nativeSurfaceDestroyed(I)V
.end method

.method private native nativeSurfaceSetSize(III)V
.end method

.method private onSwapBuffersCompleted()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->onSwapBuffersCompleted()V

    return-void
.end method

.method private requestRender()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->requestRender()V

    return-void
.end method


# virtual methods
.method public bindTexture(I)V
    .locals 0

    return-void
.end method

.method public copyTextureToBitmap(Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;)Landroid/graphics/Bitmap;
    .locals 7

    const/4 v6, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p1, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mTextureId:I

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mContentArea:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mContentArea:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_0
    move-object v5, v6

    :cond_1
    :goto_0
    return-object v5

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mContentArea:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p1, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mContentArea:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    iget v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    iget v2, p1, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mTextureId:I

    iget-object v0, p1, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mContentArea:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p1, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mContentArea:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeCopyTextureToBitmap(IIIILandroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    move-object v5, v6

    goto :goto_0
.end method

.method public deleteTexture(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeDeleteTexture(II)V

    :cond_0
    return-void
.end method

.method public generateCompressedTexture(Landroid/opengl/ETC1Util$ETC1Texture;)I
    .locals 6

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-virtual {p1}, Landroid/opengl/ETC1Util$ETC1Texture;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/opengl/ETC1Util$ETC1Texture;->getHeight()I

    move-result v3

    invoke-virtual {p1}, Landroid/opengl/ETC1Util$ETC1Texture;->getData()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-virtual {p1}, Landroid/opengl/ETC1Util$ETC1Texture;->getData()Ljava/nio/ByteBuffer;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeGenerateCompressedTexture(IIIILjava/nio/ByteBuffer;)I

    move-result v0

    goto :goto_0
.end method

.method public generateTexture(Landroid/graphics/Bitmap;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeGenerateTexture(ILandroid/graphics/Bitmap;)I

    move-result v0

    goto :goto_0
.end method

.method public getContentTexture(FLorg/chromium/content/browser/ContentViewCore;)Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;
    .locals 5

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-virtual {p2}, Lorg/chromium/content/browser/ContentViewCore;->getNativeContentViewCore()I

    move-result v2

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeGetContentTexture(IFLandroid/graphics/Rect;I)I

    move-result v2

    const/4 v0, 0x0

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->getVisibleViewport()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->top:I

    new-instance v0, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;-><init>(ILandroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public getLastLayerCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mLastLayerCount:I

    return v0
.end method

.method public getNativePointer()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    return v0
.end method

.method public initNativeCompositor()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeInit()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Surface created before native library loaded."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->setVisibility(I)V

    return-void
.end method

.method public layoutToLayers(Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;)V
    .locals 32

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getLayoutToRender()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v9

    if-eqz v9, :cond_0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v9}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->shouldDisplayTabDecoration()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    move-object/from16 v2, p0

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->update(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Landroid/view/View;)V

    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeClearLayers(I)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getViewport()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v5, v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getViewport()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v6, v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getViewport()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v7, v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getViewport()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v8, v3

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSetLayoutViewport(IFFFF)V

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v31

    if-eqz v31, :cond_4

    move-object/from16 v0, v31

    array-length v3, v0

    move/from16 v29, v3

    :goto_1
    instance-of v3, v9, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    move/from16 v0, v29

    if-ne v0, v3, :cond_6

    const/4 v3, 0x0

    aget-object v3, v31, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v5

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFallbackThumbnailId()I

    move-result v6

    const/4 v7, -0x2

    if-ne v6, v7, :cond_5

    const/4 v6, 0x1

    :goto_2
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBackgroundColor()I

    move-result v7

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getRenderX()F

    move-result v8

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getRenderY()F

    move-result v9

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v10

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v11

    const/4 v12, 0x0

    const/high16 v13, 0x3f800000

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v13}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSetStaticLayer(IIZIFFFFFF)V

    :cond_3
    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/chrome/compositor/CompositorView;->mLastLayerCount:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeCommitLayers(I)V

    goto/16 :goto_0

    :cond_4
    const/4 v3, 0x0

    move/from16 v29, v3

    goto :goto_1

    :cond_5
    const/4 v6, 0x0

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    move/from16 v30, v3

    :goto_3
    move/from16 v0, v30

    move/from16 v1, v29

    if-ge v0, v1, :cond_3

    aget-object v3, v31, v30

    sget-boolean v4, Lcom/google/android/apps/chrome/compositor/CompositorView;->$assertionsDisabled:Z

    if-nez v4, :cond_7

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isVisible()Z

    move-result v4

    if-nez v4, :cond_7

    new-instance v3, Ljava/lang/AssertionError;

    const-string v4, "LayoutTab in that list should be visible"

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    :cond_7
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getDrawDecoration()Z

    move-result v4

    if-eqz v4, :cond_8

    const/high16 v24, 0x3f800000

    :goto_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v5

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFallbackThumbnailId()I

    move-result v6

    const/4 v7, -0x2

    if-ne v6, v7, :cond_9

    const/4 v6, 0x1

    :goto_5
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBackgroundColor()I

    move-result v7

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isIncognito()Z

    move-result v8

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getRenderX()F

    move-result v9

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getRenderY()F

    move-result v10

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v11

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v12

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v13

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentHeight()F

    move-result v14

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getClippedWidth()F

    move-result v15

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v16

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(FF)F

    move-result v15

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getClippedHeight()F

    move-result v16

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v17

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(FF)F

    move-result v16

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltXPivotOffset()F

    move-result v17

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltYPivotOffset()F

    move-result v18

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltX()F

    move-result v19

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v20

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getRotation()F

    move-result v21

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getAlpha()F

    move-result v22

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBorderAlpha()F

    move-result v23

    mul-float v23, v23, v24

    const v25, 0x3ecccccd

    mul-float v25, v25, v24

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBorderCloseButtonAlpha()F

    move-result v26

    mul-float v26, v26, v24

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBorderScale()F

    move-result v27

    const/high16 v28, 0x3f800000

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v28}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativePutLayer(IIZIZFFFFFFFFFFFFFFFFFFFF)V

    add-int/lit8 v3, v30, 0x1

    move/from16 v30, v3

    goto/16 :goto_3

    :cond_8
    const/16 v24, 0x0

    goto/16 :goto_4

    :cond_9
    const/4 v6, 0x0

    goto :goto_5
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeCompositeToBitmap(ILandroid/graphics/Bitmap;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v3, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public shutDown()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeDestroy(I)V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-direct {p0, v0, p3, p4}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSurfaceSetSize(III)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSurfaceCreated(ILandroid/view/Surface;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->onSurfaceCreated()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSurfaceDestroyed(I)V

    return-void
.end method
