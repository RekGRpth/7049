.class Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$900(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$902(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$900(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->handleFindInPage(Z)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$900(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$902(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$900(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->handleFindInPage(Z)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # invokes: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->modelChanged()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$700(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x16 -> :sswitch_2
        0x2d -> :sswitch_0
        0x3b -> :sswitch_1
    .end sparse-switch
.end method
