.class public Lcom/google/android/apps/chrome/compositor/LayerTitleCache;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/tabs/TitleCache;


# instance fields
.field private mNativeLayerTitleCache:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorView;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getNativePointer()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->nativeInit(IF)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:I

    return-void
.end method

.method private native nativeClearExcept(II)V
.end method

.method private static native nativeDestroy(I)V
.end method

.method private native nativeInit(IF)I
.end method

.method private native nativeUpdateLayer(IILandroid/graphics/Bitmap;)V
.end method


# virtual methods
.method public clearExcept(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->nativeClearExcept(II)V

    return-void
.end method

.method public put(ILandroid/graphics/Bitmap;)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:I

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->nativeUpdateLayer(IILandroid/graphics/Bitmap;)V

    return-void
.end method

.method public remove(I)V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->nativeUpdateLayer(IILandroid/graphics/Bitmap;)V

    return-void
.end method

.method public shutDown()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:I

    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->nativeDestroy(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:I

    return-void
.end method
