.class public Lcom/google/android/apps/chrome/crash/MinidumpUploadService;
.super Landroid/app/IntentService;


# static fields
.field static final ACTION_FIND_ALL:Ljava/lang/String; = "com.google.android.apps.chrome.crash.ACTION_FIND_ALL"

.field private static final ACTION_FIND_LAST:Ljava/lang/String; = "com.google.android.apps.chrome.crash.ACTION_FIND_LAST"

.field static final ACTION_UPLOAD:Ljava/lang/String; = "com.google.android.apps.chrome.crash.ACTION_UPLOAD"

.field static final FILE_TO_UPLOAD_KEY:Ljava/lang/String; = "minidump_file"

.field static final MAX_TRIES_ALLOWED:I = 0x3

.field private static final PID_KEY:Ljava/lang/String; = "pid"

.field private static final TAG:Ljava/lang/String;

.field static final TRIES_KEY:Ljava/lang/String; = "tries"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->setIntentRedelivery(Z)V

    return-void
.end method

.method static createFindAndUploadAllCrashesIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.chrome.crash.ACTION_FIND_ALL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public static createFindAndUploadLastCrashIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.chrome.crash.ACTION_FIND_LAST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method static createUploadIntent(Landroid/content/Context;Ljava/io/File;)Landroid/content/Intent;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createUploadIntent(Landroid/content/Context;Ljava/io/File;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static createUploadIntent(Landroid/content/Context;Ljava/io/File;I)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.chrome.crash.ACTION_UPLOAD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "minidump_file"

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "tries"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method private handleFindAndUploadAllCrashes()V
    .locals 5

    new-instance v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/crash/CrashFileManager;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getAllMinidumpFiles()[Ljava/io/File;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->TAG:Ljava/lang/String;

    const-string v2, "Attempting to upload accumulated crash dumps."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createUploadIntent(Landroid/content/Context;Ljava/io/File;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private handleFindAndUploadLastCrash(Landroid/content/Intent;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/crash/CrashFileManager;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getAllMinidumpFilesSorted()[Ljava/io/File;

    move-result-object v0

    array-length v1, v0

    if-nez v1, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->TAG:Ljava/lang/String;

    const-string v1, "Could not find any crash dumps to upload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createUploadIntent(Landroid/content/Context;Ljava/io/File;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private handleUploadCrash(Landroid/content/Intent;)V
    .locals 5

    const/4 v3, -0x1

    const-string v0, "minidump_file"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->TAG:Ljava/lang/String;

    const-string v1, "Cannot upload crash data since minidump is absent."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->TAG:Ljava/lang/String;

    const-string v1, "Cannot upload crash data since specified minidump is not present."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v2, "tries"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v3, :cond_4

    const/4 v3, 0x3

    if-lt v2, v3, :cond_5

    :cond_4
    sget-object v1, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Giving up on trying to upload "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " after "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " attempts"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createMinidumpUploadCallable(Ljava/io/File;)Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->call()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    add-int/lit8 v2, v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createUploadIntent(Landroid/content/Context;Ljava/io/File;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public static tryUploadAllCrashDumps(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createFindAndUploadAllCrashesIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method createMinidumpUploadCallable(Ljava/io/File;)Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;-><init>(Ljava/io/File;Landroid/content/Context;)V

    return-object v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "com.google.android.apps.chrome.crash.ACTION_FIND_LAST"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->handleFindAndUploadLastCrash(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "com.google.android.apps.chrome.crash.ACTION_FIND_ALL"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->handleFindAndUploadAllCrashes()V

    goto :goto_0

    :cond_1
    const-string v0, "com.google.android.apps.chrome.crash.ACTION_UPLOAD"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->handleUploadCrash(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got unknown action from intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
