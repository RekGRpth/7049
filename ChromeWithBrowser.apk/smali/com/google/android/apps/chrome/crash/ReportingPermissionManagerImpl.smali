.class public Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/crash/ReportingPermissionManager;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public isUploadPermitted()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->allowUploadCrashDumpNow()Z

    move-result v0

    return v0
.end method
