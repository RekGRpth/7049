.class public Lcom/google/android/apps/chrome/eventfilter/BlackHoleEventFilter;
.super Lcom/google/android/apps/chrome/eventfilter/EventFilter;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/eventfilter/BlackHoleEventFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/eventfilter/BlackHoleEventFilter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;-><init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    return-void
.end method


# virtual methods
.method public onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onTouchEventInternal(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
