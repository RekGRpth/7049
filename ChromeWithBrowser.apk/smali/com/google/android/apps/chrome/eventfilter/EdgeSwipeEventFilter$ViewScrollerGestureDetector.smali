.class Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;-><init>(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$102(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$100(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$302(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    sub-float/2addr v2, v3

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->flingOccurred(FFFF)V
    invoke-static {v0, v1, v2, p3, p4}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1500(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;FFFF)V

    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$502(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$300(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    sub-float/2addr v2, v3

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->scrollUpdated(FF)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$600(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;FF)V

    :cond_0
    :goto_0
    return v7

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$700(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulateThreshold:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$800(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F

    move-result v0

    cmpg-float v0, v2, v0

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeRegion:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$900(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_2

    const-wide/16 v5, 0xc8

    cmp-long v0, v3, v5

    if-lez v0, :cond_0

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$700(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulateThreshold:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$800(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F

    move-result v0

    cmpg-float v0, v2, v0

    if-gez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulateThreshold:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$800(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z
    invoke-static {v0, v7}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$702(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->checkForFastScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1000(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_5

    float-to-double v2, v2

    float-to-double v0, v1

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->TAN_SIDE_SWIPE_ANGLE_THRESHOLD:D
    invoke-static {}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1100()D

    move-result-wide v4

    mul-double/2addr v0, v4

    cmpl-double v0, v2, v0

    if-lez v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    sget-object v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1202(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z
    invoke-static {v0, v7}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1302(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->scrollStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z
    invoke-static {v0, v7}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$302(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z
    invoke-static {v0, v8}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$702(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v0, v1, :cond_0

    const/16 v0, 0x11

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    goto/16 :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
