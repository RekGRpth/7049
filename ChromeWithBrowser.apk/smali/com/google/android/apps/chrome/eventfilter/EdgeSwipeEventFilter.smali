.class public Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;
.super Lcom/google/android/apps/chrome/eventfilter/EventFilter;


# static fields
.field private static final MAX_ACCUMULATE_DURATION_MS:J = 0xc8L

.field private static final NOTIFICATIONS:[I

.field private static final SIDE_SWIPE_ANGLE_THRESHOLD_DEGREES:D = 45.0

.field private static final TAB_SWIPING_ENABLED:Z = true

.field private static final TAG:Ljava/lang/String;

.field private static final TAN_SIDE_SWIPE_ANGLE_THRESHOLD:D


# instance fields
.field private final mAccumulateThreshold:F

.field private final mAccumulatedEvents:Ljava/util/ArrayList;

.field private mAccumulatingEvents:Z

.field private final mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

.field private mEdgeSwipeStarted:Z

.field private mEnableTabSwiping:Z

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private final mGutterDistance:F

.field private mInDoubleTap:Z

.field private mInLongPress:Z

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mPropagateEventsToHostView:Z

.field private mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

.field private mScrollStarted:Z

.field private final mSwipeRegion:F

.field private final mSwipeTimeConstant:D

.field private mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->TAG:Ljava/lang/String;

    const-wide v0, 0x4046800000000000L

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->TAN_SIDE_SWIPE_ANGLE_THRESHOLD:D

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->NOTIFICATIONS:[I

    return-void

    :array_0
    .array-data 4
        0x2d
        0x3b
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;-><init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$1;-><init>(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEnableTabSwiping:Z

    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGutterDistance:F

    const v1, 0x7f080018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeRegion:F

    const v1, 0x7f08001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulateThreshold:F

    const v1, 0x7f08001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeTimeConstant:D

    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;-><init>(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGestureDetector:Landroid/view/GestureDetector;

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, v3}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    iput-object p3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->checkForFastScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z

    return p1
.end method

.method static synthetic access$1100()D
    .locals 2

    sget-wide v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->TAN_SIDE_SWIPE_ANGLE_THRESHOLD:D

    return-wide v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->scrollStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;FFFF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->flingOccurred(FFFF)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z

    move-result v0

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;FF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->scrollUpdated(FF)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulateThreshold:F

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeRegion:F

    return v0
.end method

.method private checkForFastScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 10

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    sub-float/2addr v0, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v2, v2

    div-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    float-to-double v2, v2

    iget-wide v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeTimeConstant:D

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    const-wide/high16 v8, 0x3ff0000000000000L

    sub-double/2addr v6, v8

    mul-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-float v0, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    sub-float v0, v2, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->RIGHT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne v2, v3, :cond_1

    :goto_1
    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGutterDistance:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->LEFT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float v0, v2, v0

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGutterDistance:F

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private flingOccurred(FFFF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeStarted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeFlingOccurred(FFFF)V

    :cond_0
    return-void
.end method

.method private propagateAccumulatedEventsAndClear()Z
    .locals 6

    const/4 v4, 0x1

    const/4 v2, 0x0

    move v1, v2

    move v3, v4

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    invoke-interface {v5, v0}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    move v3, v4

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v3, v2

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z

    return v3
.end method

.method private scrollFinished()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeStarted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeFinished()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeStarted:Z

    return-void
.end method

.method private scrollStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->RIGHT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v2, v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeStarted(Z)V

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeStarted:Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private scrollUpdated(FF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeStarted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeUpdated(FF)V

    :cond_0
    return-void
.end method


# virtual methods
.method public enableTabSwiping(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEnableTabSwiping:Z

    return-void
.end method

.method protected isDownScrolling()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isSideScrolling()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->LEFT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->RIGHT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z

    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEnableTabSwiping:Z

    if-eqz v4, :cond_3

    if-nez p2, :cond_3

    if-nez v3, :cond_3

    if-lez v2, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v4, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v3}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-le v2, v1, :cond_3

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeRegion:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_4

    sget-object v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->RIGHT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v2, v3, :cond_3

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeRegion:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    sget-object v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->LEFT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    goto :goto_1
.end method

.method public onTouchEventInternal(Landroid/view/MotionEvent;)Z
    .locals 9

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    :cond_0
    :goto_0
    return v8

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z

    if-nez v1, :cond_2

    iput-boolean v8, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z

    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, v0}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    :cond_2
    if-ne v0, v8, :cond_4

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v1, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    iput-boolean v7, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z

    iput-boolean v7, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    if-eq v0, v8, :cond_6

    if-ne v0, v4, :cond_0

    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->scrollFinished()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v0, v1, :cond_7

    const/16 v0, 0x12

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    :cond_7
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    goto :goto_0
.end method

.method public registerForNotifications()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-void
.end method

.method public unregisterForNotifications()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method
