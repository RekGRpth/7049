.class public abstract Lcom/google/android/apps/chrome/eventfilter/EventFilter;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mGestureStarted:Z

.field protected final mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

.field private mSimulateIntercepting:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mSimulateIntercepting:Z

    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    return-void
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;Z)Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z

    move-result v0

    return v0
.end method

.method protected abstract onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onTouchEventInternal(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->onStartGesture()V

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    if-eqz v2, :cond_2

    if-eq v1, v3, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->onEndGesture()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    :cond_2
    return v0
.end method

.method protected abstract onTouchEventInternal(Landroid/view/MotionEvent;)Z
.end method

.method public simulateTouchEvent(Landroid/view/MotionEvent;Z)Z
    .locals 1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mSimulateIntercepting:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onInterceptTouchEvent(Landroid/view/MotionEvent;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mSimulateIntercepting:Z

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
