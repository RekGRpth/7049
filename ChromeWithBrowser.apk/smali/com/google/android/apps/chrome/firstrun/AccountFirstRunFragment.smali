.class public Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;


# static fields
.field private static final ADD_ACCOUNT_REQUEST:I = 0xc8


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->signOutIfNeeded()V

    return-void
.end method

.method private signOutIfNeeded()V
    .locals 2

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "force-fre"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->signOut()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateAccounts()V

    :cond_0
    return-void
.end method

.method public bridge synthetic onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f040010

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateAccounts()V

    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onStart()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setButtonsEnabled(Z)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    check-cast p1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    new-instance v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setListener(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;)V

    return-void
.end method
