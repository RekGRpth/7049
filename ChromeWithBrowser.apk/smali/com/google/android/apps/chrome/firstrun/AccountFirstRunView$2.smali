.class Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$2;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$2;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setButtonsEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$2;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mNoGoogleAccounts:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$400(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$2;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mListener:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$200(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$2;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$000(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;->onAccountSelected(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$2;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mListener:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$200(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;->onNewAccount()V

    goto :goto_0
.end method
