.class public Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;


# instance fields
.field private mAccountLayout:Landroid/widget/LinearLayout;

.field private mAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

.field private mAccountName:Ljava/lang/String;

.field private mAddAnotherAccount:Ljava/lang/String;

.field private mArrayAdapter:Landroid/widget/ArrayAdapter;

.field private mChromeInfo:Landroid/widget/TextView;

.field private mCurrentOrientation:I

.field private mListener:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;

.field private mNoGoogleAccounts:Z

.field private mSignInButton:Landroid/widget/Button;

.field private mSkipButton:Landroid/widget/Button;

.field private mSpinner:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mNoGoogleAccounts:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mCurrentOrientation:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAddAnotherAccount:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mListener:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mNoGoogleAccounts:Z

    return v0
.end method

.method private assignArrayAdapterText()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccountNames()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    invoke-virtual {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccountNames()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAddAnotherAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mNoGoogleAccounts:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mNoGoogleAccounts:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private orientationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    const v4, 0x7f0f0048

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mCurrentOrientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0f0047

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mCurrentOrientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic getPercentageTranslationX()F
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->getPercentageTranslationX()F

    move-result v0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->orientationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->onFinishInflate()V

    const v0, 0x7f0f0041

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0f0045

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSignInButton:Landroid/widget/Button;

    const v0, 0x7f0f0046

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAddAnotherAccount:Ljava/lang/String;

    const v0, 0x7f0f0042

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mChromeInfo:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mChromeInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$1;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSignInButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$2;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSkipButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$3;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0043

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040017

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->assignArrayAdapterText()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x7f040016

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->orientationChanged(Landroid/content/res/Configuration;)V

    :cond_0
    return-void
.end method

.method public setButtonsEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSignInButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public setCanCancel(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSkipButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mListener:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;

    return-void
.end method

.method public bridge synthetic setPercentageTranslationX(F)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->setPercentageTranslationX(F)V

    return-void
.end method

.method public updateAccounts()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setButtonsEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->assignArrayAdapterText()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;

    return-void
.end method
