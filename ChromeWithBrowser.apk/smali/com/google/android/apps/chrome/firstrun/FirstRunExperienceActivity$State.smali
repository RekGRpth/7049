.class public final enum Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

.field public static final enum CHOOSE_ACCOUNT:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

.field public static final enum FINISHED:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

.field public static final enum SIGNED:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

.field public static final enum SIGNING:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

.field public static final enum TOS_AND_UMA:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;


# instance fields
.field private mFragmentClass:Ljava/lang/Class;

.field private mNotificationIconResourceId:I

.field private mOnMainFlow:Z


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    const-string v1, "TOS_AND_UMA"

    const-class v3, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;-><init>(Ljava/lang/String;ILjava/lang/Class;ZI)V

    sput-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->TOS_AND_UMA:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    new-instance v5, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    const-string v6, "CHOOSE_ACCOUNT"

    const-class v8, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    move v7, v4

    move v9, v4

    move v10, v2

    invoke-direct/range {v5 .. v10}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;-><init>(Ljava/lang/String;ILjava/lang/Class;ZI)V

    sput-object v5, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->CHOOSE_ACCOUNT:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    new-instance v5, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    const-string v6, "SIGNING"

    const-class v8, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;

    move v7, v11

    move v9, v2

    move v10, v2

    invoke-direct/range {v5 .. v10}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;-><init>(Ljava/lang/String;ILjava/lang/Class;ZI)V

    sput-object v5, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->SIGNING:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    new-instance v5, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    const-string v6, "SIGNED"

    const-class v8, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;

    const v10, 0x7f020065

    move v7, v12

    move v9, v2

    invoke-direct/range {v5 .. v10}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;-><init>(Ljava/lang/String;ILjava/lang/Class;ZI)V

    sput-object v5, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->SIGNED:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v13}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->FINISHED:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    sget-object v1, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->TOS_AND_UMA:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->CHOOSE_ACCOUNT:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->SIGNING:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->SIGNED:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    aput-object v1, v0, v12

    sget-object v1, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->FINISHED:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    aput-object v1, v0, v13

    sput-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->$VALUES:[Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->mFragmentClass:Ljava/lang/Class;

    iput-boolean p4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->mOnMainFlow:Z

    iput p5, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->mNotificationIconResourceId:I

    return-void
.end method

.method public static getNumberOfMainStates()I
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->values()[Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    iget-boolean v4, v4, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->mOnMainFlow:Z

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->$VALUES:[Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    return-object v0
.end method


# virtual methods
.method public final getFragmentInstance(Landroid/os/Bundle;)Landroid/app/Fragment;
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->mFragmentClass:Ljava/lang/Class;

    if-nez v0, :cond_1

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->mFragmentClass:Ljava/lang/Class;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz p1, :cond_0

    :try_start_1
    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->setArguments(Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_4
    move-exception v1

    goto :goto_0

    :catch_5
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_6
    move-exception v1

    goto :goto_0

    :catch_7
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public final getMainFlowProgress()I
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->ordinal()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->ordinal()I

    move-result v0

    move v3, v0

    move v0, v1

    move v1, v3

    :goto_0
    if-ltz v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->values()[Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->isOnMainFlow()Z

    move-result v2

    if-nez v2, :cond_0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public final getNextState()Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->ordinal()I

    move-result v0

    invoke-static {}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->values()[Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->FINISHED:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->values()[Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->ordinal()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final getPreviousState()Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->ordinal()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->values()[Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->ordinal()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    aget-object p0, v0, v1

    goto :goto_0
.end method

.method public final getResourceId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->mNotificationIconResourceId:I

    return v0
.end method

.method public final isOnMainFlow()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->mOnMainFlow:Z

    return v0
.end method
