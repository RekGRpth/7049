.class public Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;
.super Lcom/google/android/apps/chrome/ChromeBaseActivity;

# interfaces
.implements Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;


# static fields
.field public static final CLOSE_APP:Ljava/lang/String; = "Close App"

.field private static final LAST_STATE:Ljava/lang/String; = "LastState"

.field public static final SIGNED_IN:Ljava/lang/String; = "Signed in to account"

.field private static final TAG:Ljava/lang/String; = "FirstRunExperienceActivity"


# instance fields
.field protected mCurrentBundle:Landroid/os/Bundle;

.field protected mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

.field protected mLoadingIcon:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeBaseActivity;-><init>()V

    sget-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->TOS_AND_UMA:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    return-void
.end method


# virtual methods
.method protected finishFirstRunExperience(Z)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "Signed in to account"

    iget-object v4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentBundle:Landroid/os/Bundle;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentBundle:Landroid/os/Bundle;

    const-string v5, "AccountName"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->finish()V

    const v0, 0x7f050005

    const v1, 0x7f050006

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->overridePendingTransition(II)V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "Close App"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->finish()V

    goto :goto_1
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public goBack()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    return-void
.end method

.method protected loadCurrentFragment(Landroid/os/Bundle;)V
    .locals 8

    const v7, 0x7f050003

    const v6, 0x7f050002

    const v5, 0x7f050001

    const v4, 0x7f050004

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->getFragmentInstance(Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    sget-object v2, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->FINISHED:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->finishFirstRunExperience(Z)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->ordinal()I

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2, v0, v4, v0, v0}, Landroid/app/FragmentTransaction;->setCustomAnimations(IIII)Landroid/app/FragmentTransaction;

    :cond_2
    :goto_1
    const v0, 0x7f0f004f

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->isOnMainFlow()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v2, v6, v4, v5, v7}, Landroid/app/FragmentTransaction;->setCustomAnimations(IIII)Landroid/app/FragmentTransaction;

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->getPreviousState()Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->isOnMainFlow()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2, v6, v4, v5, v7}, Landroid/app/FragmentTransaction;->setCustomAnimations(IIII)Landroid/app/FragmentTransaction;

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->ordinal()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->finishFirstRunExperience(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeBaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeBaseActivity;->onCreate(Landroid/os/Bundle;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Z)V
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->setupLayout()V

    if-eqz p1, :cond_0

    const-string v0, "LastState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LastState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    :cond_0
    const v0, 0x7f0f004c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mLoadingIcon:Landroid/widget/ProgressBar;

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "FirstRunExperienceActivity"

    const-string v2, "Failed to start browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->finish()V

    goto :goto_0
.end method

.method public onProgressFirstRunExperience(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->getNextState()Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    sget-object v1, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->FINISHED:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->finishFirstRunExperience(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->loadCurrentFragment(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onQuitFirstRunExperience(Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->finishFirstRunExperience(Z)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "LastState"

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setBundle(Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mCurrentBundle:Landroid/os/Bundle;

    return-void
.end method

.method protected setupLayout()V
    .locals 1

    const v0, 0x7f040012

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->setContentView(I)V

    return-void
.end method

.method public startLoadingAnim()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mLoadingIcon:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public stopLoadingAnim()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->mLoadingIcon:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method
