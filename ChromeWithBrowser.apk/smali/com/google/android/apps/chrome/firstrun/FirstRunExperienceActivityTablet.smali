.class public Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;

# interfaces
.implements Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;
.implements Lcom/google/android/apps/chrome/sync/SyncChooseAccountFragment$Listener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ADD_ACCOUNT_REQUEST:I = 0xc8

.field private static final FINISH_FIRST_RUN_DELAY_MS:I = 0x64

.field private static final PRIVACY_NOTICE_BASE_URL:Ljava/lang/String; = "http://www.google.com/chrome/intl/%s/privacy.html"

.field public static final SHOW_FRE_TOS:Ljava/lang/String; = "Show ToS string on first run"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isDestroyed:Z

.field private mAccount:Landroid/accounts/Account;

.field private mHandler:Landroid/os/Handler;

.field private mProgressRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet$1;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mProgressRunnable:Ljava/lang/Runnable;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->isDestroyed:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->isDestroyed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mProgressRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method private handleAccountSelection()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccountNames()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/sync/AccountAdder;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/AccountAdder;-><init>()V

    const/16 v1, 0xc8

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/sync/AccountAdder;->addAccount(Landroid/app/Activity;I)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mAccount:Landroid/accounts/Account;

    goto :goto_0
.end method


# virtual methods
.method protected finishFirstRunExperience(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "AccountName"

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->setBundle(Landroid/os/Bundle;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->finishFirstRunExperience(Z)V

    return-void
.end method

.method public onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->onHandleAccountSignIn()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0f0050

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "Show ToS string on first run"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0f0054

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->setFirstRunFlowListener(Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->onDestroy()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->isDestroyed:Z

    return-void
.end method

.method public onHandleAccountSignIn()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mAccount:Landroid/accounts/Account;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->handleAccountSelection()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mAccount:Landroid/accounts/Account;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f0f0050

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->setButtonsEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->startLoadingAnim()V

    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet$2;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet$2;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;Landroid/app/Activity;)V

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet$3;

    invoke-direct {v1, p0, p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet$3;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;Landroid/app/Activity;Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mAccount:Landroid/accounts/Account;

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->signInToSelectedAccount(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onSetCrashUploadPreference(Z)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->setCrashUploadPreference(Landroid/content/Context;Z)V

    return-void
.end method

.method public onShowToSAndPN(Z)V
    .locals 4

    const/4 v3, 0x1

    if-eqz p1, :cond_2

    invoke-static {}, Lcom/google/android/apps/chrome/BrowserVersion;->getTermsOfServiceHtml()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/Scanner;

    invoke-direct {v1, v0}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    const-string v2, "\\A"

    invoke-virtual {v1, v2}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object v1

    sget-boolean v2, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/util/Scanner;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v1}, Ljava/util/Scanner;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v0

    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "</head>"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v3

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :goto_0
    return-void

    :cond_2
    const-string v0, "http://www.google.com/chrome/intl/%s/privacy.html"

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {}, Lorg/chromium/base/LocaleUtils;->getDefaultLocale()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0701a2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->show(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setupLayout()V
    .locals 1

    const v0, 0x7f040013

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->setContentView(I)V

    return-void
.end method

.method public stopLoadingAnim()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityTablet;->mLoadingIcon:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method
