.class public interface abstract Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getBundle()Landroid/os/Bundle;
.end method

.method public abstract goBack()V
.end method

.method public abstract onProgressFirstRunExperience(Landroid/os/Bundle;)V
.end method

.method public abstract onQuitFirstRunExperience(Z)V
.end method

.method public abstract setBundle(Landroid/os/Bundle;)V
.end method

.method public abstract startLoadingAnim()V
.end method

.method public abstract stopLoadingAnim()V
.end method
