.class final Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;


# instance fields
.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$callback:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$appContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final tokenAvailable(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$appContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createSetEnabledIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$appContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->startServiceWithWakeLock(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$callback:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$callback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_1
    return-void
.end method
