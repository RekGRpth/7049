.class Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mSignInFinished:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->access$102(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;

    # getter for: Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->access$300(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;

    # getter for: Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->access$200(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;

    # getter for: Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->access$300(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;

    # getter for: Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->access$200(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    new-instance v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;

    # getter for: Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mAccount:Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->access$400(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)Landroid/accounts/Account;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;-><init>(Landroid/app/Activity;Landroid/accounts/Account;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->start()V

    goto :goto_0
.end method
