.class public Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;


# static fields
.field private static final SCREEN_DELAY_MS:I = 0x2ee


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAccountName:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private final mProgressRunnable:Ljava/lang/Runnable;

.field private mSignInFinished:Z

.field private mSigningText:Landroid/widget/TextView;

.field private mStateSaved:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$1;-><init>(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mSignInFinished:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mStateSaved:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mStateSaved:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mSignInFinished:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method private signOutIfNeeded()V
    .locals 2

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "force-fre"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->signOut()V

    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;->getBundle()Landroid/os/Bundle;

    move-result-object p3

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    invoke-interface {v0, p3}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;->setBundle(Landroid/os/Bundle;)V

    if-eqz p3, :cond_1

    const-string v0, "AccountName"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "AccountName"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mAccount:Landroid/accounts/Account;

    :goto_1
    const v0, 0x7f040015

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;->goBack()V

    goto :goto_1

    :cond_2
    move-object p3, v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;->stopLoadingAnim()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;->stopLoadingAnim()V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onResume()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mStateSaved:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;->startLoadingAnim()V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mSignInFinished:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mStateSaved:Z

    const-string v0, "AccountName"

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f0f0056

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mSigningText:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mSigningText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;->startLoadingAnim()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->signOutIfNeeded()V

    new-instance v0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$2;-><init>(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;)V

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$3;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment$3;-><init>(Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/SigningFirstRunFragment;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->signInToSelectedAccount(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V

    return-void
.end method
