.class public Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mButtonsPanel:Landroid/widget/LinearLayout;

.field private mChromeInfo:Landroid/widget/TextView;

.field private mCurrentOrientation:I

.field private mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;

.field private mSendReportCheckBox:Landroid/widget/CheckBox;

.field private mSignInButton:Landroid/widget/Button;

.field private mSkipButton:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mCurrentOrientation:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;)Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;

    return-object v0
.end method

.method private addToSAndPOLink(Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 10

    const/16 v9, 0x21

    const/4 v8, 0x0

    const-string v0, "<a href=\"$1\">"

    const-string v1, "</a>"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    sget-boolean v3, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Unable to find terms of service placeholder"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    if-gez v2, :cond_1

    new-instance v0, Landroid/text/SpannableString;

    const-string v1, "Error parsing terms of service"

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "<a href=\"$2\">"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    sget-boolean v6, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->$assertionsDisabled:Z

    if-nez v6, :cond_2

    if-gez v5, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Unable to find privacy link placeholder"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_2
    if-gez v5, :cond_3

    new-instance v0, Landroid/text/SpannableString;

    const-string v1, "Error parsing privacy notice"

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView$1;-><init>(Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;)V

    new-instance v6, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView$2;

    invoke-direct {v6, p0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView$2;-><init>(Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;)V

    invoke-virtual {v0, v1, v2, v3, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v0, v6, v5, v4, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0
.end method

.method private orientationChanged(Landroid/content/res/Configuration;)V
    .locals 7

    const v6, 0x7f0f0053

    const v5, 0x7f0f0051

    const v4, 0x7f0f0048

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mCurrentOrientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mCurrentOrientation:I

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mButtonsPanel:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mButtonsPanel:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mChromeInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v6}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2, v2, v0, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mButtonsPanel:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mButtonsPanel:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v6}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mChromeInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic getPercentageTranslationX()F
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->getPercentageTranslationX()F

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mSendReportCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;->onSetCrashUploadPreference(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mSignInButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->freSignInAttempted()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;->onHandleAccountSignIn()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mSkipButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->setButtonsEnabled(Z)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->freSkipSignIn()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;->onQuitFirstRunExperience(Z)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->orientationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->onFinishInflate()V

    const v0, 0x7f0f0045

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mSignInButton:Landroid/widget/Button;

    const v0, 0x7f0f0046

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mSkipButton:Landroid/widget/Button;

    const v0, 0x7f0f0047

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mButtonsPanel:Landroid/widget/LinearLayout;

    const v0, 0x7f0f0052

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mSendReportCheckBox:Landroid/widget/CheckBox;

    const v0, 0x7f0f0042

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mChromeInfo:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mChromeInfo:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08007b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mChromeInfo:Landroid/widget/TextView;

    invoke-virtual {v2, v1, v0, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    const v0, 0x7f0f0054

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/google/android/apps/chrome/BrowserVersion;->getTermsOfServiceString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->addToSAndPOLink(Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v2

    new-instance v3, Landroid/text/method/LinkMovementMethod;

    invoke-direct {v3}, Landroid/text/method/LinkMovementMethod;-><init>()V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mSignInButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->orientationChanged(Landroid/content/res/Configuration;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mSendReportCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isNeverUploadCrashDump()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public setButtonsEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mSignInButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public setFirstRunFlowListener(Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/TabletFirstRunView;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListenerTablet;

    return-void
.end method

.method public bridge synthetic setPercentageTranslationX(F)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->setPercentageTranslationX(F)V

    return-void
.end method
