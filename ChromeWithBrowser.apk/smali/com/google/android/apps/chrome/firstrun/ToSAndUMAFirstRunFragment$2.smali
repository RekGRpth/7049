.class Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;

    # getter for: Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mSendReportCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->access$000(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->setCrashUploadPreference(Landroid/content/Context;Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;

    # getter for: Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->access$200(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$2;->this$0:Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;

    # getter for: Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->access$100(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
