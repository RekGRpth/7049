.class public Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;
.super Ljava/lang/Object;


# instance fields
.field public final mContentArea:Landroid/graphics/Rect;

.field public final mTextureId:I


# direct methods
.method public constructor <init>(ILandroid/graphics/Rect;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mTextureId:I

    iput-object p2, p0, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mContentArea:Landroid/graphics/Rect;

    return-void
.end method
