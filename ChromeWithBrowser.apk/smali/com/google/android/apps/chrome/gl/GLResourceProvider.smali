.class public interface abstract Lcom/google/android/apps/chrome/gl/GLResourceProvider;
.super Ljava/lang/Object;


# virtual methods
.method public abstract bindTexture(I)V
.end method

.method public abstract copyTextureToBitmap(Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;)Landroid/graphics/Bitmap;
.end method

.method public abstract deleteTexture(I)V
.end method

.method public abstract generateCompressedTexture(Landroid/opengl/ETC1Util$ETC1Texture;)I
.end method

.method public abstract generateTexture(Landroid/graphics/Bitmap;)I
.end method

.method public abstract getContentTexture(FLorg/chromium/content/browser/ContentViewCore;)Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;
.end method
