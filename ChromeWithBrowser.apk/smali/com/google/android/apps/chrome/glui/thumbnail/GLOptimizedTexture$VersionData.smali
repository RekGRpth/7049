.class Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;
.super Ljava/lang/Object;


# instance fields
.field private scale:F

.field private version:I


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->scale:F

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;F)F
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->scale:F

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->version:I

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->version:I

    return p1
.end method
