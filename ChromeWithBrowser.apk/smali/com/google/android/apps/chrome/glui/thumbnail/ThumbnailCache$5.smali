.class Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$5;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$5;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$5;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->unregisterNotifications()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$2200(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$5;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->nativeSetSelectedTab(I)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$2300(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0xc -> :sswitch_1
        0x27 -> :sswitch_0
    .end sparse-switch
.end method
