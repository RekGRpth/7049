.class Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;
.super Landroid/os/AsyncTask;


# instance fields
.field private mErrorWhileCompressing:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->mErrorWhileCompressing:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;
    .locals 2

    const/4 v0, 0x0

    aget-object v0, p1, v0

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->compressTexture()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->mErrorWhileCompressing:Z

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->doInBackground([Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->mErrorWhileCompressing:Z

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$600()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Dropping texture "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because compression failed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->unpinTexture(I)V

    invoke-virtual {p1, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupCPUData(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->notifyListenersOfTextureChange(IZ)V

    const/16 v0, 0x31

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # setter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionTaskRunning:Z
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1902(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->compressNextTexture()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1500(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->aggressivelyRebuildTextures()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1100(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->rebuildGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;Z)V
    invoke-static {v0, p1, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1300(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;Z)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->writeTextureIfNecessary(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1800(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v1

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->notifyListenersOfTextureChange(IZ)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CompressTextureTask;->onPostExecute(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    return-void
.end method
