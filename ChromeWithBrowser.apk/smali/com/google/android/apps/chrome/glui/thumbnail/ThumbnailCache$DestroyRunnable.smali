.class final Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$DestroyRunnable;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final mNativeThumbnailCache:I


# direct methods
.method private constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$DestroyRunnable;->mNativeThumbnailCache:I

    return-void
.end method

.method synthetic constructor <init>(ILcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$DestroyRunnable;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$DestroyRunnable;->mNativeThumbnailCache:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$DestroyRunnable;->mNativeThumbnailCache:I

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->nativeDestroy(I)V
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$2100(I)V

    :cond_0
    return-void
.end method
