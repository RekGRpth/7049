.class Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$GLOptimizedApproximationTexture;
.super Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;


# direct methods
.method public constructor <init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;Landroid/graphics/Bitmap;F)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;-><init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;Landroid/graphics/Bitmap;F)V

    return-void
.end method


# virtual methods
.method public declared-synchronized canFreeSourceData()Z
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized compressionRequired()Z
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized dataWriteRequired()Z
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Approx - "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$GLOptimizedApproximationTexture;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
