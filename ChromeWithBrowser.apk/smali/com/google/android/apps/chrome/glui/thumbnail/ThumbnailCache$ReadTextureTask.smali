.class Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;
.super Landroid/os/AsyncTask;


# instance fields
.field private final mId:I

.field final synthetic this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput p2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;
    .locals 6

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getTextureInputStream(I)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    :try_start_1
    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v3, v3, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    invoke-static {v0, v3, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->buildTextureFromStream(ILcom/google/android/apps/chrome/gl/GLResourceProvider;Ljava/io/InputStream;)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_2
    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$600()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Deleting corrupt texture for tab "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget v4, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->deleteCorruptTextureSource(I)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :goto_0
    invoke-static {v2}, Lcom/google/android/apps/chrome/utilities/StreamUtils;->closeQuietly(Ljava/io/Closeable;)V

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v2, v1

    move-object v0, v1

    :goto_2
    :try_start_3
    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v3, "IO exception while reading texture for tab"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v2}, Lcom/google/android/apps/chrome/utilities/StreamUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw v0

    :catch_1
    move-exception v0

    move-object v2, v1

    :goto_4
    :try_start_4
    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$600()Ljava/lang/String;

    move-result-object v0

    const-string v3, "OutOfMemoryError thrown while trying to load tab thumbnail from disk."

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static {v2}, Lcom/google/android/apps/chrome/utilities/StreamUtils;->closeQuietly(Ljava/io/Closeable;)V

    move-object v0, v1

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v0, v1

    :goto_5
    invoke-static {v1}, Lcom/google/android/apps/chrome/utilities/StreamUtils;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    move-object v0, v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_5

    :catch_7
    move-exception v1

    move-object v1, v2

    goto :goto_5
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1000(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1000(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1100(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)Ljava/util/HashMap;

    move-result-object v0

    iget v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1100(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)Ljava/util/HashMap;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    invoke-virtual {v0, v1, v6}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->notifyListenersOfTextureChange(IZ)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->compressionRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1400(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->compressNextTexture()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1500(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # setter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadTaskRunning:Z
    invoke-static {v0, v5}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1602(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->readNextTexture()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    return-void

    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v1, v1, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    iget v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getRawData()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-boolean v2, v2, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mUseApproximationTextures:Z

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getScale()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->createApproximation(Landroid/graphics/Bitmap;F)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;

    move-result-object v1

    if-eqz v1, :cond_5

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$GLOptimizedApproximationTexture;

    iget v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v3, v3, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->getScale()F

    move-result v1

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$GLOptimizedApproximationTexture;-><init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;Landroid/graphics/Bitmap;F)V

    move-object v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->makeSpaceForNewItemIfNecessary(I)V
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1200(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;I)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    iget v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    iget v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->mId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->aggressivelyRebuildTextures()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->rebuildGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;Z)V
    invoke-static {v0, p1, v5}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$1300(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;Z)V

    goto/16 :goto_0

    :cond_4
    if-eqz p1, :cond_1

    invoke-virtual {p1, v6}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupCPUData(Z)V

    goto/16 :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ReadTextureTask;->onPostExecute(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    return-void
.end method
