.class public Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/identity/UniqueIdentificationGenerator;


# static fields
.field public static final GENERATOR_ID:Ljava/lang/String; = "GSERVICES_ANDROID_ID"

.field static final NOT_FOUND:I = -0x1

.field private static final TAG:Ljava/lang/String;

.field private static final UNIQUE_ID_KEY:Ljava/lang/String; = "com.google.android.apps.chrome.identity.UNIQUE_ID"


# instance fields
.field private final mApplicationContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->mApplicationContext:Landroid/content/Context;

    return-void
.end method

.method private getAndroidIdWithPackageName()Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->getAndroidIdFromGservices()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method getAndroidIdFromGservices()J
    .locals 6

    const-wide/16 v0, -0x1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    const-wide/16 v4, -0x1

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/c/a;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    return-wide v0

    :catch_0
    move-exception v2

    sget-object v3, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->TAG:Ljava/lang/String;

    const-string v4, "Couldn\'t get Android ID."

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method getUUID()Ljava/lang/String;
    .locals 1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUniqueId(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v0, "com.google.android.apps.chrome.identity.UNIQUE_ID"

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->getAndroidIdWithPackageName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->getUUID()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "com.google.android.apps.chrome.identity.UNIQUE_ID"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method
