.class public Lcom/google/android/apps/chrome/identity/SettingsSecureBasedIdentificationGenerator;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/identity/UniqueIdentificationGenerator;


# static fields
.field public static final GENERATOR_ID:Ljava/lang/String; = "SETTINGS_SECURE_ANDROID_ID"


# instance fields
.field private final mApplicationContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/identity/SettingsSecureBasedIdentificationGenerator;->mApplicationContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method getAndroidId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/identity/SettingsSecureBasedIdentificationGenerator;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUniqueId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/identity/SettingsSecureBasedIdentificationGenerator;->getAndroidId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/utilities/Hasher;->getMd5Hash(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0
.end method
