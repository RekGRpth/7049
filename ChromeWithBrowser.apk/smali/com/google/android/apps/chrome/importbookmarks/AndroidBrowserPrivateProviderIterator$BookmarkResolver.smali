.class Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;
.super Ljava/lang/Object;


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mDatabaseProvider:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mContentResolver:Landroid/content/ContentResolver;

    iput-object p1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;->close()V

    :cond_0
    return-void
.end method

.method public getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mContentResolver:Landroid/content/ContentResolver;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method
