.class public Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$BookmarkIterator;


# static fields
.field private static final BOOKMARKS_COLUMN_ACCOUNT_NAME:Ljava/lang/String; = "account_name"

.field private static final BOOKMARKS_COLUMN_ACCOUNT_TYPE:Ljava/lang/String; = "account_type"

.field private static final BOOKMARKS_COLUMN_CREATED:Ljava/lang/String; = "created"

.field private static final BOOKMARKS_COLUMN_DELETED:Ljava/lang/String; = "deleted"

.field private static final BOOKMARKS_COLUMN_FAVICON:Ljava/lang/String; = "favicon"

.field private static final BOOKMARKS_COLUMN_ID:Ljava/lang/String; = "_id"

.field private static final BOOKMARKS_COLUMN_IS_FOLDER:Ljava/lang/String; = "folder"

.field private static final BOOKMARKS_COLUMN_PARENT:Ljava/lang/String; = "parent"

.field private static final BOOKMARKS_COLUMN_TITLE:Ljava/lang/String; = "title"

.field private static final BOOKMARKS_COLUMN_URL:Ljava/lang/String; = "url"

.field private static final BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

.field public static final BOOKMARKS_PATH:Ljava/lang/String; = "bookmarks"

.field private static final BOOKMARKS_PROJECTION:[Ljava/lang/String;

.field private static final BOOKMARKS_SORT_ORDER:Ljava/lang/String; = "folder DESC, position ASC, _id ASC"

.field private static final BOOKMARK_CONTAINER_FOLDER_ID:J = 0x1L

.field private static final BOOKMARK_MATCHES:Ljava/lang/String; = "bookmark=1 AND url=? AND title=?"

.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final HISTORY_COLUMN_DATE_LAST_VISITED:Ljava/lang/String; = "date"

.field private static final HISTORY_COLUMN_URL:Ljava/lang/String; = "url"

.field private static final HISTORY_COLUMN_VISITS:Ljava/lang/String; = "visits"

.field private static final HISTORY_CONTENT_URI:Landroid/net/Uri;

.field private static final HISTORY_FOR_URL:Ljava/lang/String; = "url=?"

.field public static final HISTORY_PATH:Ljava/lang/String; = "history"

.field private static final HISTORY_PROJECTION:[Ljava/lang/String;

.field private static final NOT_SYNCED_OR_DELETED:Ljava/lang/String; = "deleted=0 AND account_name IS NULL AND account_type IS NULL AND _id!=1"

.field public static final PROVIDER_AUTHORITY:Ljava/lang/String; = "com.android.browser"

.field private static final TAG:Ljava/lang/String; = "AndroidBrowserPrivateProviderIterator"


# instance fields
.field private final mBookmarkResolver:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;

.field private final mCursor:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.android.browser"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "bookmarks"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "url"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "folder"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "parent"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "created"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "favicon"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->BOOKMARKS_PROJECTION:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "history"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->HISTORY_CONTENT_URI:Landroid/net/Uri;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "url"

    aput-object v1, v0, v3

    const-string v1, "date"

    aput-object v1, v0, v4

    const-string v1, "visits"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->HISTORY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mBookmarkResolver:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;

    iput-object p2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    return-void
.end method

.method public static createIfAvailable(Landroid/content/ContentResolver;)Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;
    .locals 6

    const/4 v4, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->BOOKMARKS_PROJECTION:[Ljava/lang/String;

    const-string v3, "deleted=0 AND account_name IS NULL AND account_type IS NULL AND _id!=1"

    const-string v5, "folder DESC, position ASC, _id ASC"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v4, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;

    new-instance v1, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v4, v1, v0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;-><init>(Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;Landroid/database/Cursor;)V

    :cond_0
    return-object v4
.end method

.method public static createIfAvailable(Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;)Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;
    .locals 6

    const/4 v4, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->BOOKMARKS_PROJECTION:[Ljava/lang/String;

    const-string v3, "deleted=0 AND account_name IS NULL AND account_type IS NULL AND _id!=1"

    const-string v5, "folder DESC, position ASC, _id ASC"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v4, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;

    new-instance v1, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;-><init>(Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;)V

    invoke-direct {v4, v1, v0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;-><init>(Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;Landroid/database/Cursor;)V

    :cond_0
    return-object v4
.end method

.method public static isProviderAvailable(Landroid/content/ContentResolver;)Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidInPublicAPI(Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;)Z
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-boolean v0, p1, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->isFolder:Z

    if-eqz v0, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mBookmarkResolver:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v6

    goto :goto_0

    :cond_1
    sget-object v1, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "url"

    aput-object v3, v2, v7

    const-string v3, "bookmark=1 AND url=? AND title=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    aput-object v5, v4, v7

    iget-object v5, p1, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->title:Ljava/lang/String;

    aput-object v5, v4, v6

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v6

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    if-lez v1, :cond_3

    move v0, v6

    goto :goto_0

    :cond_3
    move v0, v7

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mBookmarkResolver:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->close()V

    return-void
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;
    .locals 12

    const-wide/16 v10, 0x1

    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v9, -0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_1
    new-instance v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;

    invoke-direct {v6}, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;-><init>()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->id:J

    iget-wide v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->id:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_3

    :cond_2
    :goto_0
    return-object v5

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v2, "parent"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->parentId:J

    iget-wide v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->parentId:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_4

    const-wide/16 v0, 0x0

    iput-wide v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->parentId:J

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v2, "folder"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_b

    move v0, v4

    :goto_1
    iput-boolean v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->isFolder:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v2, "url"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v2, "title"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->title:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->isFolder:Z

    if-nez v0, :cond_5

    iget-object v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    if-eqz v0, :cond_2

    :cond_5
    iget-object v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->title:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v6}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->isValidInPublicAPI(Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v1, "created"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v9, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v1, "favicon"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v9, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->favicon:[B

    :cond_7
    iget-boolean v0, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->isFolder:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->mBookmarkResolver:Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;

    sget-object v1, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->HISTORY_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->HISTORY_PROJECTION:[Ljava/lang/String;

    const-string v3, "url=?"

    new-array v4, v4, [Ljava/lang/String;

    iget-object v8, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    aput-object v8, v4, v7

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_9

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "date"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v9, :cond_8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    :cond_8
    const-string v1, "visits"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v9, :cond_9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->visits:Ljava/lang/Long;

    :cond_9
    if-eqz v0, :cond_a

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_a
    move-object v5, v6

    goto/16 :goto_0

    :cond_b
    move v0, v7

    goto/16 :goto_1

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->next()Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
