.class public Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$BookmarkIterator;


# static fields
.field private static final SELECT_IS_BOOKMARK:Ljava/lang/String; = "bookmark=1"

.field private static final TAG:Ljava/lang/String; = "AndroidBrowserPublicProviderIterator"


# instance fields
.field private final mCursor:Landroid/database/Cursor;

.field private mNextId:J


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mNextId:J

    iput-object p1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    return-void
.end method

.method public static createIfAvailable(Landroid/content/ContentResolver;)Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;
    .locals 6

    const/4 v2, 0x0

    sget-object v1, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    const-string v3, "bookmark=1"

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;

    invoke-direct {v2, v0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;-><init>(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public static isProviderAvailable(Landroid/content/ContentResolver;)Z
    .locals 1

    sget-object v0, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;
    .locals 6

    const/4 v1, 0x0

    const/4 v5, -0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;-><init>()V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v4, "url"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v4, "title"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->title:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v2, v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->title:Ljava/lang/String;

    if-nez v2, :cond_3

    :cond_2
    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v2, "created"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v5, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v2, "date"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v5, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v2, "visits"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v5, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->visits:Ljava/lang/Long;

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v2, "favicon"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v5, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->favicon:[B

    :cond_7
    iget-wide v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mNextId:J

    const-wide/16 v3, 0x1

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->mNextId:J

    iput-wide v1, v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->id:J

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->parentId:J

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->isFolder:Z

    goto/16 :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->next()Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
