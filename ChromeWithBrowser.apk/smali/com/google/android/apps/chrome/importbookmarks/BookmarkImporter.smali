.class public abstract Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;
.super Ljava/lang/Object;


# static fields
.field private static final EXISTS_PROJECTION:[Ljava/lang/String;

.field private static final HAS_URL:Ljava/lang/String; = "url=?"

.field static final ROOT_FOLDER_ID:J = 0x0L

.field private static final SELECT_IS_BOOKMARK:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "BookmarkImporter"

.field private static final VALUE_IS_BOOKMARK:Ljava/lang/Integer;


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private mTask:Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportBookmarksTask;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->VALUE_IS_BOOKMARK:Ljava/lang/Integer;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "bookmark="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->VALUE_IS_BOOKMARK:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->SELECT_IS_BOOKMARK:Ljava/lang/String;

    new-array v0, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "url"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->EXISTS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/Integer;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->VALUE_IS_BOOKMARK:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->EXISTS_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->SELECT_IS_BOOKMARK:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected abstract availableBookmarks()[Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$BookmarkIterator;
.end method

.method public cancel()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->mTask:Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportBookmarksTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportBookmarksTask;->cancel(Z)Z

    return-void
.end method

.method public importBookmarks(Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$OnBookmarksImportedListener;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportBookmarksTask;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportBookmarksTask;-><init>(Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$OnBookmarksImportedListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->mTask:Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportBookmarksTask;

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->mTask:Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportBookmarksTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportBookmarksTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
