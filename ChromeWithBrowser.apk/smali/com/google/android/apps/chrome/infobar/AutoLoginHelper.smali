.class public Lcom/google/android/apps/chrome/infobar/AutoLoginHelper;
.super Ljava/lang/Object;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginHelper;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginHelper;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    return-void
.end method

.method public static shouldShowAutoLogin(Lorg/chromium/content/browser/ContentViewCore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    new-instance v1, Lcom/google/android/apps/chrome/AccountManagerContainer;

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/google/android/apps/chrome/AccountManagerContainer;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/AccountManagerContainer;->getAccount(Landroid/accounts/AccountManagerCallback;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method showAutoLoginInfoBar(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/infobar/NativeInfoBar;
    .locals 7

    new-instance v2, Lcom/google/android/apps/chrome/infobar/NativeInfoBar;

    invoke-direct {v2, p1}, Lcom/google/android/apps/chrome/infobar/NativeInfoBar;-><init>(I)V

    new-instance v0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginHelper;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    iget-object v3, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginHelper;->mActivity:Landroid/app/Activity;

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;Lcom/google/android/apps/chrome/infobar/NativeInfoBar;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "InfoBarContainer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "infoBar.hasAccount(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->hasAccount()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->hasAccount()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/infobar/NativeInfoBar;->setInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginHelper;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    goto :goto_0
.end method
