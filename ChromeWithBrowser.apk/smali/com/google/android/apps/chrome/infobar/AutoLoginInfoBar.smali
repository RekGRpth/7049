.class Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;
.super Lcom/google/android/apps/chrome/infobar/InfoBar;

# interfaces
.implements Landroid/accounts/AccountManagerCallback;
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private mAccountManagerContainer:Lcom/google/android/apps/chrome/AccountManagerContainer;

.field private final mActivity:Landroid/app/Activity;

.field private mCancelButton:Landroid/widget/Button;

.field private final mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

.field private mIsSingleLine:Z

.field private mLoginButton:Landroid/widget/Button;

.field private final mNativeInfoBar:Lcom/google/android/apps/chrome/infobar/NativeInfoBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;Lcom/google/android/apps/chrome/infobar/NativeInfoBar;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/infobar/InfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mNativeInfoBar:Lcom/google/android/apps/chrome/infobar/NativeInfoBar;

    iput-object p3, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mActivity:Landroid/app/Activity;

    new-instance v0, Lcom/google/android/apps/chrome/AccountManagerContainer;

    invoke-direct {v0, p3, p4, p5, p6}, Lcom/google/android/apps/chrome/AccountManagerContainer;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccountManagerContainer:Lcom/google/android/apps/chrome/AccountManagerContainer;

    invoke-static {}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->newExtraAccount()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccount:Landroid/accounts/Account;

    return-void

    :cond_0
    invoke-static {p3}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0
.end method

.method private static newExtraAccount()Landroid/accounts/Account;
    .locals 2

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "auto-login-extra-account"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->getSwitchValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected createContent(Landroid/content/Context;)Landroid/view/View;
    .locals 6

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04001e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0f0061

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f070135

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->getAccountName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0f0064

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mLoginButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mLoginButton:Landroid/widget/Button;

    const v2, 0x7f070136

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mLoginButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0063

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mCancelButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mCancelButton:Landroid/widget/Button;

    const v2, 0x7f070137

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p1}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mIsSingleLine:Z

    return-object v1
.end method

.method public exactMatch()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccountManagerContainer:Lcom/google/android/apps/chrome/AccountManagerContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/AccountManagerContainer;->exactMatch()Z

    move-result v0

    return v0
.end method

.method getAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccountManagerContainer:Lcom/google/android/apps/chrome/AccountManagerContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/AccountManagerContainer;->getAccountName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getAuthToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccountManagerContainer:Lcom/google/android/apps/chrome/AccountManagerContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/AccountManagerContainer;->getAuthToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getIconResourceId()Ljava/lang/Integer;
    .locals 1

    const v0, 0x7f020086

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public hasAccount()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method loginFailed()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->dismiss()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mActivity:Landroid/app/Activity;

    const v1, 0x7f070138

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->createWarningInfoBar(Ljava/lang/CharSequence;)Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    return-void
.end method

.method loginSucceeded(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mNativeInfoBar:Lcom/google/android/apps/chrome/infobar/NativeInfoBar;

    iget v0, v0, Lcom/google/android/apps/chrome/infobar/NativeInfoBar;->mNativeInfoBarPtr:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mNativeInfoBar:Lcom/google/android/apps/chrome/infobar/NativeInfoBar;

    iget v1, v1, Lcom/google/android/apps/chrome/infobar/NativeInfoBar;->mNativeInfoBarPtr:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->nativeOnButtonClicked(IILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->dismiss()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mLoginButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    const-string v1, "AutoLoginInfoBar"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "auto-login requested for "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0}, Landroid/accounts/Account;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const-string v0, "InfoBar"

    const-string v1, "auto-login failed because account is no longer valid"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->loginFailed()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v0, "?"

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccountManagerContainer:Lcom/google/android/apps/chrome/AccountManagerContainer;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/apps/chrome/AccountManagerContainer;->getAuthToken(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->setButtonsEnabled(Z)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->dismiss()V

    goto :goto_1

    :cond_5
    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 7

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "authtoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    :goto_0
    if-eqz v5, :cond_0

    const/4 v4, 0x1

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccountManagerContainer:Lcom/google/android/apps/chrome/AccountManagerContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/AccountManagerContainer;->getAuthToken()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mAccountManagerContainer:Lcom/google/android/apps/chrome/AccountManagerContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/AccountManagerContainer;->getAccountName()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mActivity:Landroid/app/Activity;

    new-instance v0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;-><init>(Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception v0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    goto :goto_1
.end method

.method setButtonsEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mLoginButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mLoginButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mCancelButton:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method protected shouldCenterIcon()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->mIsSingleLine:Z

    return v0
.end method
