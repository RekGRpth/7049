.class public Lcom/google/android/apps/chrome/infobar/ConfirmHelper;
.super Ljava/lang/Object;


# instance fields
.field private final mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/ConfirmHelper;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    return-void
.end method


# virtual methods
.method showConfirmInfoBar(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)Lcom/google/android/apps/chrome/infobar/NativeInfoBar;
    .locals 6

    new-instance v0, Lcom/google/android/apps/chrome/infobar/NativeConfirmInfoBar;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/infobar/NativeConfirmInfoBar;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/ConfirmHelper;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/NativeConfirmInfoBar;->getInfoBar()Lcom/google/android/apps/chrome/infobar/InfoBar;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    return-object v0
.end method
