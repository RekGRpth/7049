.class public abstract Lcom/google/android/apps/chrome/infobar/InfoBar;
.super Ljava/lang/Object;


# static fields
.field public static final ACTION_TYPE_AUTOLOGIN:I = 0x3

.field public static final ACTION_TYPE_CANCEL:I = 0x2

.field public static final ACTION_TYPE_NONE:I = 0x0

.field public static final ACTION_TYPE_OK:I = 0x1

.field public static final ACTION_TYPE_TRANSLATE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "InfoBar"

.field private static sIdCounter:I


# instance fields
.field private mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

.field private mContentView:Landroid/view/View;

.field private mExpireOnNavigation:Z

.field private final mId:I

.field private mIsDismissed:Z

.field private mListener:Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/chrome/infobar/InfoBar;->sIdCounter:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mExpireOnNavigation:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mIsDismissed:Z

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;

    invoke-static {}, Lcom/google/android/apps/chrome/infobar/InfoBar;->generateId()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mId:I

    return-void
.end method

.method private static generateId()I
    .locals 2

    sget v0, Lcom/google/android/apps/chrome/infobar/InfoBar;->sIdCounter:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/google/android/apps/chrome/infobar/InfoBar;->sIdCounter:I

    return v0
.end method


# virtual methods
.method protected abstract createContent(Landroid/content/Context;)Landroid/view/View;
.end method

.method public dismiss()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mIsDismissed:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mIsDismissed:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->hasBeenDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->removeInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;->onInfoBarDismissed(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    :cond_1
    return-void
.end method

.method protected getBackgroundType()Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->WARNING:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    return-object v0
.end method

.method public getCloseButton()Landroid/widget/ImageButton;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mContentView:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mContentView:Landroid/view/View;

    const v1, 0x7f0f0067

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    goto :goto_0
.end method

.method public getContent(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/infobar/InfoBar;->getContent(Landroid/content/Context;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method getContent(Landroid/content/Context;Z)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mContentView:Landroid/view/View;

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBar;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mContentView:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mContentView:Landroid/view/View;

    return-object v0
.end method

.method public getDismissedListener()Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;

    return-object v0
.end method

.method protected getIconBitmap()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getIconResourceId()Ljava/lang/Integer;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method getId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mId:I

    return v0
.end method

.method getInfoBarContainer()Lcom/google/android/apps/chrome/infobar/InfoBarContainer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    return-object v0
.end method

.method public getTab()Lcom/google/android/apps/chrome/Tab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    return-object v0
.end method

.method protected onDismissButtonClicked()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/InfoBar;->dismiss()V

    return-void
.end method

.method public setDismissedListener(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;

    return-void
.end method

.method public setExpireOnNavigation(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mExpireOnNavigation:Z

    return-void
.end method

.method setInfoBarContainer(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mContainer:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    return-void
.end method

.method protected shouldCenterIcon()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public shouldExpire(Ljava/lang/String;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mExpireOnNavigation:Z

    return v0
.end method

.method protected shouldShowCloseButton()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public willBeShown()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBar;->mIsDismissed:Z

    return-void
.end method
