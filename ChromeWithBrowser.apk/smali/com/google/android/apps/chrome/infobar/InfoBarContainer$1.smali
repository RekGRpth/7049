.class Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

.field final synthetic val$infoBar:Lcom/google/android/apps/chrome/infobar/InfoBar;

.field final synthetic val$infoBarView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;Lcom/google/android/apps/chrome/infobar/InfoBar;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->val$infoBar:Lcom/google/android/apps/chrome/infobar/InfoBar;

    iput-object p3, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->val$infoBarView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    # getter for: Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->access$000(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->val$infoBar:Lcom/google/android/apps/chrome/infobar/InfoBar;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->val$infoBarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    # getter for: Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsOnTop:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->access$100(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)Z

    move-result v1

    if-eqz v1, :cond_1

    neg-int v0, v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->val$infoBarView:Landroid/view/View;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->val$infoBarView:Landroid/view/View;

    const-string v1, "translationY"

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1$1;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->val$infoBarView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    # getter for: Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsOnTop:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->access$100(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;->val$infoBarView:Landroid/view/View;

    # setter for: Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mViewClippedToBounds:Landroid/view/View;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->access$202(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;Landroid/view/View;)Landroid/view/View;

    :cond_2
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method
