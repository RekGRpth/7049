.class Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

.field final synthetic val$infoBar:Lcom/google/android/apps/chrome/infobar/InfoBar;

.field final synthetic val$infoBarView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;Landroid/view/View;Lcom/google/android/apps/chrome/infobar/InfoBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->val$infoBarView:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->val$infoBar:Lcom/google/android/apps/chrome/infobar/InfoBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mViewClippedToBounds:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->access$202(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;Landroid/view/View;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->val$infoBarView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->val$infoBarView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->val$infoBar:Lcom/google/android/apps/chrome/infobar/InfoBar;

    # invokes: Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->broadcast(ILcom/google/android/apps/chrome/infobar/InfoBar;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->access$300(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;ILcom/google/android/apps/chrome/infobar/InfoBar;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    # invokes: Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->processPendingInfoBars()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->access$400(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)V

    return-void
.end method
