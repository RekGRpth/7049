.class public Lcom/google/android/apps/chrome/infobar/InfoBarContainer;
.super Landroid/widget/LinearLayout;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mAutoLoginHelper:Lcom/google/android/apps/chrome/infobar/AutoLoginHelper;

.field private final mConfirmHelper:Lcom/google/android/apps/chrome/infobar/ConfirmHelper;

.field private mDestroyed:Z

.field private final mInfoBars:Ljava/util/ArrayList;

.field private final mInfoBarsOnTop:Z

.field private final mInfoBarsToHide:Ljava/util/ArrayDeque;

.field private final mInfoBarsToShow:Ljava/util/ArrayDeque;

.field private mIsAnimatingInfoBar:Z

.field private mNativeInfoBarContainer:I

.field private final mTab:Lcom/google/android/apps/chrome/Tab;

.field private mViewClippedToBounds:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/Tab;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToShow:Ljava/util/ArrayDeque;

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToHide:Ljava/util/ArrayDeque;

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mIsAnimatingInfoBar:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mDestroyed:Z

    new-instance v0, Lcom/google/android/apps/chrome/infobar/AutoLoginHelper;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/chrome/infobar/AutoLoginHelper;-><init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mAutoLoginHelper:Lcom/google/android/apps/chrome/infobar/AutoLoginHelper;

    new-instance v0, Lcom/google/android/apps/chrome/infobar/ConfirmHelper;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/infobar/ConfirmHelper;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mConfirmHelper:Lcom/google/android/apps/chrome/infobar/ConfirmHelper;

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mTab:Lcom/google/android/apps/chrome/Tab;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->setOrientation(I)V

    invoke-static {p1}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsOnTop:Z

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsOnTop:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x30

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mAutoLoginHelper:Lcom/google/android/apps/chrome/infobar/AutoLoginHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mConfirmHelper:Lcom/google/android/apps/chrome/infobar/ConfirmHelper;

    invoke-direct {p0, p3, v0, v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->nativeInit(ILcom/google/android/apps/chrome/infobar/AutoLoginHelper;Lcom/google/android/apps/chrome/infobar/ConfirmHelper;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mNativeInfoBarContainer:I

    return-void

    :cond_0
    const/16 v0, 0x50

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsOnTop:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mViewClippedToBounds:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;ILcom/google/android/apps/chrome/infobar/InfoBar;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->broadcast(ILcom/google/android/apps/chrome/infobar/InfoBar;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->processPendingInfoBars()V

    return-void
.end method

.method private addInfoBarToViewHierarchy(Lcom/google/android/apps/chrome/infobar/InfoBar;Z)Landroid/view/View;
    .locals 4

    const/4 v3, -0x2

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/content/browser/ContentView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {v0, p0}, Lorg/chromium/content/browser/ContentView;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mActivity:Landroid/app/Activity;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/infobar/InfoBar;->getContent(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-object v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getChildCount()I

    move-result v0

    goto :goto_0
.end method

.method private broadcast(ILcom/google/android/apps/chrome/infobar/InfoBar;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "infoBarId"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/infobar/InfoBar;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private isOwnedByInstantTab()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mTab:Lcom/google/android/apps/chrome/Tab;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeDestroy(I)V
.end method

.method private native nativeInit(ILcom/google/android/apps/chrome/infobar/AutoLoginHelper;Lcom/google/android/apps/chrome/infobar/ConfirmHelper;)I
.end method

.method private processPendingInfoBars()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mIsAnimatingInfoBar:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToHide:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->removeAndFadeNextInfoBar()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToShow:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->showAndAnimateNextInfoBar()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mIsAnimatingInfoBar:Z

    goto :goto_0
.end method

.method private removeAndFadeNextInfoBar()V
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mIsAnimatingInfoBar:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToHide:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToHide:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/infobar/InfoBar;

    iget-object v3, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/chrome/infobar/InfoBar;->getContent(Landroid/content/Context;Z)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->indexOfChild(Landroid/view/View;)I

    move-result v4

    new-instance v5, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;

    invoke-direct {v5, p0, v3, v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$2;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;Landroid/view/View;Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    const/4 v0, -0x1

    if-eq v4, v0, :cond_2

    if-nez v3, :cond_5

    :cond_2
    sget-object v6, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v0, "Trying to fade infobar ("

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v3, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") with index "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->post(Ljava/lang/Runnable;)Z

    :goto_1
    return-void

    :cond_5
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsOnTop:Z

    if-eqz v0, :cond_9

    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_7

    if-ltz v4, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getChildCount()I

    move-result v0

    if-lt v4, v0, :cond_7

    :cond_6
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_7
    const-string v0, "translationY"

    new-array v7, v1, [F

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v8

    neg-int v8, v8

    int-to-float v8, v8

    aput v8, v7, v2

    invoke-static {v0, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    sget-boolean v8, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v8, :cond_8

    if-nez v4, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_8
    new-array v8, v1, [Landroid/animation/PropertyValuesHolder;

    aput-object v7, v8, v2

    invoke-static {v4, v8}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_9
    const-string v0, "translationY"

    new-array v7, v1, [F

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v8

    int-to-float v8, v8

    aput v8, v7, v2

    invoke-static {v0, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    move v0, v2

    :goto_3
    if-gt v0, v4, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getChildCount()I

    move-result v8

    if-ge v0, v8, :cond_b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    sget-boolean v9, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v9, :cond_a

    if-nez v8, :cond_a

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_a
    new-array v9, v1, [Landroid/animation/PropertyValuesHolder;

    aput-object v7, v9, v2

    invoke-static {v8, v9}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_b
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    new-instance v1, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$3;

    invoke-direct {v1, p0, v0, v5}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$3;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;Landroid/animation/AnimatorSet;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsOnTop:Z

    if-eqz v1, :cond_c

    iput-object v3, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mViewClippedToBounds:Landroid/view/View;

    :cond_c
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_1
.end method

.method private showAndAnimateNextInfoBar()V
    .locals 3

    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mIsAnimatingInfoBar:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToShow:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToShow:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/infobar/InfoBar;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsOnTop:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->addInfoBarToViewHierarchy(Lcom/google/android/apps/chrome/infobar/InfoBar;Z)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v2, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$1;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;Lcom/google/android/apps/chrome/infobar/InfoBar;Landroid/view/View;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mDestroyed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->TAG:Ljava/lang/String;

    const-string v1, "Trying to add an info bar that has already been added."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/infobar/InfoBar;->willBeShown()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, p0}, Lcom/google/android/apps/chrome/infobar/InfoBar;->setInfoBarContainer(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToShow:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->isOwnedByInstantTab()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mIsAnimatingInfoBar:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mIsAnimatingInfoBar:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->processPendingInfoBars()V

    :cond_2
    return-void
.end method

.method public destroy()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mDestroyed:Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->removeAllViews()V

    iget v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mNativeInfoBarContainer:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mNativeInfoBarContainer:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->nativeDestroy(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToShow:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToHide:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    return-void
.end method

.method public dismissAutoLogins()V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/infobar/InfoBar;

    instance-of v2, v0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/InfoBar;->dismiss()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public dismissAutoLogins(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/infobar/InfoBar;

    instance-of v2, v0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->getAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->getAuthToken()Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p3, :cond_1

    invoke-virtual {v0, p4}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->loginSucceeded(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->loginFailed()V

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;->setButtonsEnabled(Z)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsOnTop:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mViewClippedToBounds:Landroid/view/View;

    if-eq p2, v0, :cond_1

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public getInfoBarCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getInfoBars()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNative()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mNativeInfoBarContainer:I

    return v0
.end method

.method public getTab()Lcom/google/android/apps/chrome/Tab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mTab:Lcom/google/android/apps/chrome/Tab;

    return-object v0
.end method

.method public hasBeenDestroyed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mDestroyed:Z

    return v0
.end method

.method native nativeOnButtonClicked(IILjava/lang/String;)V
.end method

.method native nativeOnInfoBarClosed(I)V
.end method

.method public onContentViewChanged()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/content/browser/ContentView;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/apps/chrome/utilities/ViewUtilities;->isKeyboardShowing(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->setVisibility(I)V

    :cond_0
    :goto_1
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->setVisibility(I)V

    goto :goto_1
.end method

.method public onPageStarted(Ljava/lang/String;)V
    .locals 4

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/infobar/InfoBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/infobar/InfoBar;->shouldExpire(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/infobar/InfoBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/InfoBar;->dismiss()V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public removeInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mDestroyed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->TAG:Ljava/lang/String;

    const-string v1, "Trying to remove an info bar that is not in this container."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToShow:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->isOwnedByInstantTab()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mInfoBarsToHide:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mIsAnimatingInfoBar:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->mIsAnimatingInfoBar:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->processPendingInfoBars()V

    :cond_3
    return-void
.end method
