.class Lcom/google/android/apps/chrome/infobar/NativeConfirmInfoBar;
.super Lcom/google/android/apps/chrome/infobar/NativeInfoBar;

# interfaces
.implements Lcom/google/android/apps/chrome/infobar/ConfirmInfoBarListener;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 6

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/infobar/NativeInfoBar;-><init>(I)V

    new-instance v0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/ConfirmInfoBarListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/NativeConfirmInfoBar;->setInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    return-void
.end method


# virtual methods
.method public onConfirmInfoBarButtonClicked(Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;Z)V
    .locals 4

    iget v0, p0, Lcom/google/android/apps/chrome/infobar/NativeConfirmInfoBar;->mNativeInfoBarPtr:I

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->getInfoBarContainer()Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/chrome/infobar/NativeConfirmInfoBar;->mNativeInfoBarPtr:I

    const-string v3, ""

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->nativeOnButtonClicked(IILjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
