.class Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar$1;->this$0:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar$1;->this$0:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->dismiss()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar$1;->this$0:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    # getter for: Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->access$000(Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;)Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;->onLaunchBlockedPopups()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar$1;->this$0:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;

    # getter for: Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->access$100(Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getPattern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;->setAllowed(Ljava/lang/Boolean;)V

    :cond_0
    return-void
.end method
