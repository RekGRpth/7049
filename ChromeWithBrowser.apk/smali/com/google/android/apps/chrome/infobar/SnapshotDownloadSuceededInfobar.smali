.class public Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;
.super Lcom/google/android/apps/chrome/infobar/InfoBar;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mButton:Landroid/widget/Button;

.field private final mSnapshotUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->mSnapshotUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method protected createContent(Landroid/content/Context;)Landroid/view/View;
    .locals 4

    sget-boolean v0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->mButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04001b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0f005f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->mButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->mButton:Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07013d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->mButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method protected getBackgroundType()Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->INFO:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->mButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->dismiss()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->getTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->mSnapshotUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    return-void
.end method
