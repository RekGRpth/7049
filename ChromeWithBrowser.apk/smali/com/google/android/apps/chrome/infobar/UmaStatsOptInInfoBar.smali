.class public Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;
.super Lcom/google/android/apps/chrome/infobar/InfoBar;


# instance fields
.field private mAllowButton:Landroid/widget/Button;

.field private mListener:Lcom/google/android/apps/chrome/UmaStatsOptInListener;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/UmaStatsOptInListener;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;->mListener:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    return-void
.end method


# virtual methods
.method public createContent(Landroid/content/Context;)Landroid/view/View;
    .locals 4

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04001d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0f0061

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07019b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0f005f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;->mAllowButton:Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070171

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;->mAllowButton:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;->mAllowButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;->mListener:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method protected getBackgroundType()Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->INFO:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    return-object v0
.end method

.method protected getIconResourceId()Ljava/lang/Integer;
    .locals 1

    const v0, 0x7f02008d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onDismissButtonClicked()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/infobar/InfoBar;->onDismissButtonClicked()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/UmaStatsOptInInfoBar;->mListener:Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;->onUserResponse()V

    return-void
.end method
