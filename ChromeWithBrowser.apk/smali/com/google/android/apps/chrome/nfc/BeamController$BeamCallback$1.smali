.class Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$1;->this$0:Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$1;->this$0:Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;

    # getter for: Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->mChromeMain:Lcom/google/android/apps/chrome/Main;
    invoke-static {v0}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->access$300(Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;)Lcom/google/android/apps/chrome/Main;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->isOverlayVisable()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;

    const v1, 0x7f0701da

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;-><init>(Ljava/lang/Integer;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$1;->this$0:Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;

    # getter for: Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->mChromeMain:Lcom/google/android/apps/chrome/Main;
    invoke-static {v0}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->access$300(Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;)Lcom/google/android/apps/chrome/Main;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    # invokes: Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->getCurrentURL(Lcom/google/android/apps/chrome/TabModel;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->access$400(Lcom/google/android/apps/chrome/TabModel;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;

    const v1, 0x7f0701db

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;-><init>(Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$1;->call()Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;

    move-result-object v0

    return-object v0
.end method
