.class public Lcom/google/android/apps/chrome/omaha/VersionNumber;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "VersionNumber"


# instance fields
.field private mVersion:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/apps/chrome/omaha/VersionNumber;
    .locals 7

    const/4 v6, 0x4

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "\\."

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v1, v3

    if-ne v1, v6, :cond_0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/VersionNumber;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/omaha/VersionNumber;-><init>()V

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v6, :cond_2

    :try_start_0
    iget-object v4, v1, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget-object v5, v3, v2

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v4, v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public isSmallerThan(Lcom/google/android/apps/chrome/omaha/VersionNumber;)Z
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v2, v2, v0

    iget-object v3, p1, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v3, v3, v0

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v2, v2, v1

    iget-object v3, p1, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v3, v3, v1

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v2, v2, v4

    iget-object v3, p1, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v3, v3, v4

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v2, v2, v5

    iget-object v3, p1, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v3, v3, v5

    if-ge v2, v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "%d.%d.%d.%d"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v2, v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v2, v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/VersionNumber;->mVersion:[I

    aget v2, v2, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
