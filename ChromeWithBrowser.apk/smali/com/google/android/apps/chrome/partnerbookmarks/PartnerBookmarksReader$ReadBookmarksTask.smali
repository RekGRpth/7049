.class Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;
.super Landroid/os/AsyncTask;


# instance fields
.field private final mRootSync:Ljava/lang/Object;

.field final synthetic this$0:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->this$0:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->mRootSync:Ljava/lang/Object;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;-><init>(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->mRootSync:Ljava/lang/Object;

    return-object v0
.end method

.method private createRootBookmarksFolderBookmark()Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;
    .locals 5

    const-wide/16 v3, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;-><init>()V

    iput-wide v3, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->id:J

    const-string v1, "[IMPLIED_ROOT]"

    iput-object v1, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->title:Ljava/lang/String;

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->nativeId:J

    iput-wide v3, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parentId:J

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->isFolder:Z

    return-object v0
.end method

.method private readBookmarkHierarchy(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;Ljava/util/HashSet;)V
    .locals 9

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-wide v0, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->id:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    :try_start_0
    iget-object v8, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->mRootSync:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_2

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v8

    throw v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "PartnerBookmarksReader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error inserting bookmark "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    iget-wide v0, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->nativeId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    const-string v0, "PartnerBookmarksReader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error creating bookmark \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->this$0:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    iget-object v1, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->url:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->title:Ljava/lang/String;

    iget-boolean v3, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->isFolder:Z

    iget-wide v4, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parentId:J

    iget-object v6, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->favicon:[B

    iget-object v7, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->touchicon:[B

    # invokes: Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->onBookmarkPush(Ljava/lang/String;Ljava/lang/String;ZJ[B[B)J
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->access$200(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;Ljava/lang/String;Ljava/lang/String;ZJ[B[B)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->nativeId:J

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :cond_3
    iget-boolean v0, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->isFolder:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->entries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    iget-object v2, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parent:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    if-eq v2, p1, :cond_4

    const-string v0, "PartnerBookmarksReader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Hierarchy error in bookmark \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'. Skipping."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    iget-wide v2, p1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->nativeId:J

    iput-wide v2, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parentId:J

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->readBookmarkHierarchy(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;Ljava/util/HashSet;)V

    goto :goto_2
.end method

.method private recreateFolderHierarchy(Ljava/util/LinkedHashMap;)V
    .locals 9

    const-wide/16 v7, 0x0

    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    iget-wide v3, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->id:J

    cmp-long v1, v3, v7

    if-eqz v1, :cond_0

    iget-wide v3, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parentId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-wide v3, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parentId:J

    iget-wide v5, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->id:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_2

    :cond_1
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    iput-object v1, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parent:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    iget-object v1, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parent:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    iget-object v1, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->entries:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-wide v3, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parentId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    iput-object v1, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parent:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    iget-object v1, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parent:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    iget-object v1, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->entries:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 10

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->this$0:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->getAvailableBookmarks()Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$BookmarkIterator;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v9

    :cond_0
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->createRootBookmarksFolderBookmark()Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    move-result-object v4

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_1
    invoke-interface {v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$BookmarkIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$BookmarkIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    if-eqz v0, :cond_1

    iget-wide v5, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->id:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "PartnerBookmarksReader"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Duplicate bookmark id: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v7, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->id:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ". Dropping bookmark."

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    iget-boolean v5, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->isFolder:Z

    if-nez v5, :cond_3

    iget-object v5, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->url:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "PartnerBookmarksReader"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "More than one bookmark pointing to "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->url:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ". Keeping only the first one for consistency with Chromium."

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    iget-wide v5, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->id:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->url:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-interface {v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$BookmarkIterator;->close()V

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->recreateFolderHierarchy(Ljava/util/LinkedHashMap;)V

    iget-object v0, v4, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->entries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "PartnerBookmarksReader"

    const-string v1, "ATTENTION: not using partner bookmarks as none were provided"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    iget-object v0, v4, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->entries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const-string v0, "PartnerBookmarksReader"

    const-string v1, "ATTENTION: more than one top-level partner bookmarks, ignored"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->readBookmarkHierarchy(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;Ljava/util/HashSet;)V

    goto/16 :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->mRootSync:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->this$0:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->onBookmarksRead()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
