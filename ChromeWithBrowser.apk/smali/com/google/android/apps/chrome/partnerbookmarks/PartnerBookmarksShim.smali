.class public Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;
.super Ljava/lang/Object;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mReader:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public initialize(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->mContext:Landroid/content/Context;

    return-void
.end method

.method public kickOffReading()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->mReader:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->mReader:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->cancelReading()V

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    iget-object v1, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->mReader:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->mReader:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->onBookmarksRead()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->mReader:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->readBookmarks()V

    goto :goto_0
.end method
