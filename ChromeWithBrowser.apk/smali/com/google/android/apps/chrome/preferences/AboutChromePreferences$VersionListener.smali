.class Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$VersionListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private mOmahaIntentAlreadyFired:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$VersionListener;->this$0:Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$VersionListener;->mOmahaIntentAlreadyFired:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$VersionListener;-><init>(Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;)V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$VersionListener;->mOmahaIntentAlreadyFired:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$VersionListener;->this$0:Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createRegisterRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$VersionListener;->this$0:Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$VersionListener;->this$0:Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPrefOmahaForceUpdated(Z)V

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$VersionListener;->mOmahaIntentAlreadyFired:Z

    goto :goto_0
.end method
