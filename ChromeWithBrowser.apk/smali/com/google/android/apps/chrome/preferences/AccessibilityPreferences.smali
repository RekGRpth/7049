.class public Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field public static final FORCE_ENABLE_ZOOM_THRESHOLD_MULTIPLIER:F = 1.3f

.field private static final MAX_FSM:F = 1.3f

.field private static final MIN_FSM:F = 1.05f

.field private static final WIDTH_FOR_MAX_FSM:I = 0x320

.field private static final WIDTH_FOR_MIN_FSM:I = 0x140


# instance fields
.field mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

.field mFormat:Ljava/text/NumberFormat;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method public static getFontScaleMultiplier(Landroid/content/Context;)F
    .locals 3

    const v0, 0x3f866666

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v2, 0x140

    if-gt v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/16 v2, 0x320

    if-lt v1, v2, :cond_1

    const v0, 0x3fa66666

    goto :goto_0

    :cond_1
    add-int/lit16 v1, v1, -0x140

    int-to-float v1, v1

    const/high16 v2, 0x43f00000

    div-float/2addr v1, v2

    const/high16 v2, 0x3e800000

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public static getForceEnableZoomFontScaleThreshold(Landroid/content/Context;)F
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->getFontScaleMultiplier(Landroid/content/Context;)F

    move-result v0

    const v1, 0x3fa66666

    mul-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFormat:Ljava/text/NumberFormat;

    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->addPreferencesFromResource(I)V

    const-string v0, "text_scale"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "text_scale"

    const/high16 v3, 0x3f800000

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->updateTextScaleSummary(Landroid/preference/Preference;F)V

    const-string v1, "force_enable_zoom"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    iput-object v1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->setLinkedSeekBarPreference(Lcom/google/android/apps/chrome/preferences/SeekBarPreference;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getForceEnableZoom()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->setChecked(Z)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v3

    const-string v0, "text_scale"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p2

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->updateTextScaleSummary(Landroid/preference/Preference;F)V

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getFontScaleFactor()F

    move-result v0

    invoke-virtual {v3, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPreferenceChanged(Landroid/preference/Preference;Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getFontScaleFactor()F

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->getForceEnableZoomFontScaleThreshold(Landroid/content/Context;)F

    move-result v5

    cmpg-float v6, v0, v5

    if-gez v6, :cond_2

    cmpl-float v6, v4, v5

    if-ltz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->isChecked()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setForceEnableZoom(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->setChecked(Z)V

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPrefUserSetForceEnableZoom(Z)V

    :cond_1
    :goto_1
    move v0, v2

    goto :goto_0

    :cond_2
    cmpl-float v0, v0, v5

    if-ltz v0, :cond_1

    cmpg-float v0, v4, v5

    if-gez v0, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefUserSetForceEnableZoom()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setForceEnableZoom(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    :cond_3
    const-string v0, "force_enable_zoom"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPrefUserSetForceEnableZoom(Z)V

    invoke-virtual {v3, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPreferenceChanged(Landroid/preference/Preference;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method updateTextScaleSummary(Landroid/preference/Preference;F)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFormat:Ljava/text/NumberFormat;

    float-to-double v1, p2

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method
