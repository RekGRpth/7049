.class public Lcom/google/android/apps/chrome/preferences/AutofillEnabler;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mSwitch:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mSwitch:Landroid/widget/Switch;

    return-void
.end method


# virtual methods
.method public attach()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->updateSwitchValue()V

    return-void
.end method

.method public destroy()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mSwitch:Landroid/widget/Switch;

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setAutofillEnabled(Ljava/lang/Boolean;)V

    return-void
.end method

.method public setSwitch(Landroid/widget/Switch;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mSwitch:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->updateSwitchValue()V

    return-void
.end method

.method public updateSwitchValue()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefAutofillPreference()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mSwitch:Landroid/widget/Switch;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AutofillEnabler;->mContext:Landroid/content/Context;

    const v2, 0x7f0c0008

    invoke-virtual {v0, v1, v2}, Landroid/widget/Switch;->setSwitchTextAppearance(Landroid/content/Context;I)V

    return-void
.end method
