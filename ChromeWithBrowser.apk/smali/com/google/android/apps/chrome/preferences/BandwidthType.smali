.class public final enum Lcom/google/android/apps/chrome/preferences/BandwidthType;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/preferences/BandwidthType;

.field static final synthetic $assertionsDisabled:Z

.field public static final enum ALWAYS_PRERENDER:Lcom/google/android/apps/chrome/preferences/BandwidthType;

.field public static final DEFAULT:Lcom/google/android/apps/chrome/preferences/BandwidthType;

.field public static final enum NEVER_PRERENDER:Lcom/google/android/apps/chrome/preferences/BandwidthType;

.field public static final enum PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/BandwidthType;


# instance fields
.field private final index:I

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->$assertionsDisabled:Z

    new-instance v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;

    const-string v3, "NEVER_PRERENDER"

    const-string v4, "never_prefetch"

    invoke-direct {v0, v3, v2, v4, v2}, Lcom/google/android/apps/chrome/preferences/BandwidthType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->NEVER_PRERENDER:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    new-instance v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;

    const-string v3, "PRERENDER_ON_WIFI"

    const-string v4, "prefetch_on_wifi"

    invoke-direct {v0, v3, v1, v4, v1}, Lcom/google/android/apps/chrome/preferences/BandwidthType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    new-instance v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;

    const-string v3, "ALWAYS_PRERENDER"

    const-string v4, "always_prefetch"

    invoke-direct {v0, v3, v5, v4, v5}, Lcom/google/android/apps/chrome/preferences/BandwidthType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->ALWAYS_PRERENDER:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/chrome/preferences/BandwidthType;

    sget-object v3, Lcom/google/android/apps/chrome/preferences/BandwidthType;->NEVER_PRERENDER:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    aput-object v3, v0, v2

    sget-object v2, Lcom/google/android/apps/chrome/preferences/BandwidthType;->PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/chrome/preferences/BandwidthType;->ALWAYS_PRERENDER:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->$VALUES:[Lcom/google/android/apps/chrome/preferences/BandwidthType;

    sget-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    sput-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->DEFAULT:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->title:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->index:I

    return-void
.end method

.method static GetBandwidthFromTitle(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/BandwidthType;
    .locals 5

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/BandwidthType;->values()[Lcom/google/android/apps/chrome/preferences/BandwidthType;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget-object v4, v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->title:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->DEFAULT:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    goto :goto_1
.end method

.method static GetTypeFromIndex(I)Lcom/google/android/apps/chrome/preferences/BandwidthType;
    .locals 5

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/BandwidthType;->values()[Lcom/google/android/apps/chrome/preferences/BandwidthType;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget v4, v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->index:I

    if-ne v4, p0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->DEFAULT:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/BandwidthType;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/preferences/BandwidthType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->$VALUES:[Lcom/google/android/apps/chrome/preferences/BandwidthType;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/preferences/BandwidthType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/preferences/BandwidthType;

    return-object v0
.end method


# virtual methods
.method public final getDisplayTitle()I
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType$1;->$SwitchMap$com$google$android$apps$chrome$preferences$BandwidthType:[I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BandwidthType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    const v0, 0x7f070194

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f070195

    goto :goto_0

    :pswitch_2
    const v0, 0x7f070196

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final index()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->index:I

    return v0
.end method

.method public final title()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->title:Ljava/lang/String;

    return-object v0
.end method
