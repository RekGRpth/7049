.class public Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceActivity;
.super Landroid/preference/PreferenceActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method public static isTabletPreferencesUi(Landroid/app/Activity;)Z
    .locals 1

    instance-of v0, p0, Landroid/preference/PreferenceActivity;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/preference/PreferenceActivity;

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->flushPersistentData()V

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    return-void
.end method
