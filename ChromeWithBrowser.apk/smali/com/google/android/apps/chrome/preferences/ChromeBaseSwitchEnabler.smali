.class public abstract Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract attach()V
.end method

.method public abstract destroy()V
.end method

.method public abstract setSwitch(Landroid/widget/Switch;)V
.end method

.method public abstract updateSwitchValue()V
.end method
