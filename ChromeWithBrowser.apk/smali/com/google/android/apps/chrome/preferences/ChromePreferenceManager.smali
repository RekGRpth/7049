.class public Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
.super Ljava/lang/Object;


# static fields
.field public static final ALLOW_PRERENDER:Ljava/lang/String; = "allow_prefetch"

.field public static final FIRST_RUN_FLOW_COMPLETE:Ljava/lang/String; = "first_run_flow"

.field public static final PREF_ACCEPT_COOKIES:Ljava/lang/String; = "accept_cookies"

.field public static final PREF_APPLICATION_VERSION:Ljava/lang/String; = "application_version"

.field public static final PREF_AUTOFILL:Ljava/lang/String; = "autofill"

.field public static final PREF_BANDWIDTH:Ljava/lang/String; = "prefetch_bandwidth"

.field public static final PREF_BANDWIDTH_NO_CELLULAR:Ljava/lang/String; = "prefetch_bandwidth_no_cellular"

.field public static final PREF_BLOCK_POPUPS:Ljava/lang/String; = "block_popups"

.field public static final PREF_CRASH_DUMP_UPLOAD:Ljava/lang/String; = "crash_dump_upload"

.field public static final PREF_CRASH_DUMP_UPLOAD_NO_CELLULAR:Ljava/lang/String; = "crash_dump_upload_no_cellular"

.field public static final PREF_DO_NOT_TRACK:Ljava/lang/String; = "do_not_track"

.field public static final PREF_ENABLE_JAVASCRIPT:Ljava/lang/String; = "enable_javascript"

.field public static final PREF_ENABLE_LOCATION:Ljava/lang/String; = "enable_location"

.field public static final PREF_ENABLE_REMOTE_DEBUGGING:Ljava/lang/String; = "enable_remote_debugging"

.field public static final PREF_ENABLE_TILT_SCROLL:Ljava/lang/String; = "enable_tilt_scroll"

.field public static final PREF_ENABLE_WEB_ACCELERATION:Ljava/lang/String; = "enable_web_acceleration"

.field public static final PREF_EXECUTABLE_PATH:Ljava/lang/String; = "executable_path"

.field public static final PREF_FORCE_ENABLE_ZOOM:Ljava/lang/String; = "force_enable_zoom"

.field public static final PREF_GOOGLE_CHROME_VERSION:Ljava/lang/String; = "google_chrome_version"

.field public static final PREF_GOOGLE_LOCATION_SETTINGS:Ljava/lang/String; = "google_location_settings"

.field public static final PREF_HARDWARE_ACCELERATION:Ljava/lang/String; = "hardware_acceleration"

.field public static final PREF_HOME_PAGE:Ljava/lang/String; = "home_page"

.field public static final PREF_IMPORT_AUTOFILL_CONTACT:Ljava/lang/String; = "import_autofill_contact"

.field public static final PREF_JAVASCRIPT_VERSION:Ljava/lang/String; = "javascript_version"

.field public static final PREF_LEARN_PRIVACY_FEATURES:Ljava/lang/String; = "learn_privacy_features"

.field public static final PREF_LEARN_REMOTE_DEBUGGING:Ljava/lang/String; = "learn_remote_debugging"

.field public static final PREF_LEARN_WEB_ACCELERATION:Ljava/lang/String; = "learn_about_web_acceleration"

.field public static final PREF_MANAGE_POPUP_EXCEPTIONS:Ljava/lang/String; = "manage_popup_exceptions"

.field public static final PREF_NAVIGATION_ERROR:Ljava/lang/String; = "navigation_error"

.field public static final PREF_NETWORK_PREDICTIONS:Ljava/lang/String; = "network_predictions"

.field public static final PREF_OMAHA_FORCE_UPDATED:Ljava/lang/String; = "omaha_force_updated"

.field public static final PREF_OPEN_SOURCE_LICENSE:Ljava/lang/String; = "open_source_license"

.field public static final PREF_OS_VERSION:Ljava/lang/String; = "os_version"

.field public static final PREF_PROFILE_PATH:Ljava/lang/String; = "profile_path"

.field public static final PREF_RLZ_NOTIFIED:Ljava/lang/String; = "rlz_first_search_notified"

.field public static final PREF_SAVE_PASSWORDS:Ljava/lang/String; = "save_passwords"

.field public static final PREF_SEARCH_ENGINE:Ljava/lang/String; = "search_engine"

.field public static final PREF_SEARCH_SUGGESTIONS:Ljava/lang/String; = "search_suggestions"

.field public static final PREF_TERMS_OF_SERVICE:Ljava/lang/String; = "terms_of_service"

.field public static final PREF_TEXT_SCALE:Ljava/lang/String; = "text_scale"

.field public static final PREF_USER_SET_FORCE_ENABLE_ZOOM:Ljava/lang/String; = "user_set_force_enable_zoom"

.field public static final PREF_WEBKIT_VERSION:Ljava/lang/String; = "webkit_version"

.field private static sPrefs:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCrashUploadingEnabled:Z

.field private mSharedPreferences:Landroid/content/SharedPreferences;

.field private mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashUploadingEnabled:Z

    return-void
.end method

.method private allowUploadCrashDump()Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isMobileNetworkCapable()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "crash_dump_upload_no_cellular"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const v2, 0x7f070256

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const v3, 0x7f070258

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const v4, 0x7f070257

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, "crash_dump_upload"

    invoke-interface {v4, v5, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isWiFiOrEthernetNetwork()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static changeViewStyle(Landroid/view/View;Landroid/content/Context;)V
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    check-cast p0, Landroid/view/ViewGroup;

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->changeViewStyle(Landroid/view/View;Landroid/content/Context;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    check-cast p0, Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/widget/Switch;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p0, Landroid/widget/Switch;

    const v0, 0x7f0c0008

    invoke-virtual {p0, p1, v0}, Landroid/widget/Switch;->setSwitchTextAppearance(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method private getActiveNetworkInfo()Landroid/net/NetworkInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    .locals 2

    const-class v1, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->sPrefs:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->sPrefs:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->sPrefs:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public allowPrerender()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "allow_prefetch"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "allow_prefetch"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->checkAllowPrerender()Z

    move-result v0

    goto :goto_0
.end method

.method public allowUploadCrashDumpNow()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashUploadingEnabled:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->allowUploadCrashDump()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "force-dump-upload"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkAllowPrerender()Z
    .locals 4

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isMobileNetworkCapable()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "prefetch_bandwidth"

    sget-object v3, Lcom/google/android/apps/chrome/preferences/BandwidthType;->DEFAULT:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/preferences/BandwidthType;->title()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/BandwidthType;->GetBandwidthFromTitle(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/BandwidthType;

    move-result-object v0

    :goto_0
    sget-object v2, Lcom/google/android/apps/chrome/preferences/BandwidthType;->NEVER_PRERENDER:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/chrome/preferences/BandwidthType;->PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    if-ne v0, v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isWiFiOrEthernetNetwork()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "allow_prefetch"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "prefetch_bandwidth_no_cellular"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->ALWAYS_PRERENDER:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/preferences/BandwidthType;->NEVER_PRERENDER:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public disableCrashUploading()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashUploadingEnabled:Z

    return-void
.end method

.method public getFirstRunFlowComplete()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "first_run_flow"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getHomePage(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "home_page"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkType()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "Disconnected"

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPrefAutofillPreference()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "autofill"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefBandwidthPreference()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "prefetch_bandwidth"

    sget-object v2, Lcom/google/android/apps/chrome/preferences/BandwidthType;->PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/BandwidthType;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/BandwidthType;->title()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrefCrashDumpUploadPreference()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const v1, 0x7f070256

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "crash_dump_upload"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrefDoNotTrackPreference()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "do_not_track"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefEnableWebAccelerationPreference()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "enable_web_acceleration"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefOmahaForceUpdated()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "omaha_force_updated"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefSavePasswordsPreference()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "save_passwords"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefSearchEnginePreference()I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "search_engine"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getPrefUserSetForceEnableZoom()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "user_set_force_enable_zoom"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getRlzNotified()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "rlz_first_search_notified"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isFastNetwork()Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    if-eq v3, v1, :cond_2

    const/4 v4, 0x6

    if-eq v3, v4, :cond_2

    const/16 v4, 0x9

    if-ne v3, v4, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    if-nez v3, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public isMobileNetworkCapable()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isNetworkAvailable()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNeverUploadCrashDump()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isMobileNetworkCapable()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const v1, 0x7f070256

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "crash_dump_upload"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "crash_dump_upload_no_cellular"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isWiFiOrEthernetNetwork()Z
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-eq v2, v0, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openPopupExceptionsSettings()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method setAllowPrerendering(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "allow_prefetch"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setAutofillEnabled(Ljava/lang/Boolean;)V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setAutoFillEnabled(Z)V

    return-void
.end method

.method public setDoNotTrackEnabled(Ljava/lang/Boolean;)V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setDoNotTrackEnabled(Z)V

    return-void
.end method

.method public setFirstRunFlowComplete(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "first_run_flow"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setPrefBandwidthPreference(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "prefetch_bandwidth"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setPrefOmahaForceUpdated(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "omaha_force_updated"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setPrefUserSetForceEnableZoom(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "user_set_force_enable_zoom"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setPreferenceChanged(Landroid/preference/Preference;Ljava/lang/Object;)V
    .locals 4

    const-string v0, "accept_cookies"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setAllowCookiesEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "enable_location"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setAllowLocationEnabled(Z)V

    goto :goto_0

    :cond_1
    const-string v0, "enable_javascript"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setJavaScriptEnabled(Z)V

    goto :goto_0

    :cond_2
    const-string v0, "text_scale"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v3

    mul-float/2addr v2, v3

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->getFontScaleMultiplier(Landroid/content/Context;)F

    move-result v0

    mul-float/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setFontScaleFactor(F)V

    goto :goto_0

    :cond_3
    const-string v0, "force_enable_zoom"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setForceEnableZoom(Z)V

    goto :goto_0

    :cond_4
    const-string v0, "prefetch_bandwidth"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPrefBandwidthPreference(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string v0, "block_popups"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setAllowPopupsEnabled(Z)V

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    const-string v0, "search_suggestions"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setSearchSuggestEnabled(Z)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "network_predictions"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setNetworkPredictionEnabled(Z)V

    goto/16 :goto_0

    :cond_9
    const-string v0, "navigation_error"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setResolveNavigationErrorEnabled(Z)V

    goto/16 :goto_0

    :cond_a
    const-string v0, "enable_remote_debugging"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setRemoteDebuggingEnabled(Z)V

    goto/16 :goto_0

    :cond_b
    const-string v0, "enable_web_acceleration"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setSpdyProxyEnabled(Z)V

    goto/16 :goto_0

    :cond_c
    const-string v0, "do_not_track"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setDoNotTrackEnabled(Z)V

    goto/16 :goto_0

    :cond_d
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This should never be reached!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public setRememberPasswordsEnabled(Ljava/lang/Boolean;)V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setRememberPasswordsEnabled(Z)V

    return-void
.end method

.method public setRlzNotified(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "rlz_first_search_notified"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setSearchEngine(I)V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setSearchEngine(I)V

    const/16 v0, 0x2a

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    return-void
.end method

.method public setSharedPreferenceValueFromNative()V
    .locals 3

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "search_engine"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSearchEngine()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "accept_cookies"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAcceptCookiesEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "save_passwords"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isRememberPasswordsEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "autofill"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAutofillEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "do_not_track"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isDoNotTrackEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "enable_location"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAllowLocationEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "enable_javascript"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->javaScriptEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "block_popups"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->popupsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "search_suggestions"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isSearchSuggestEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "network_predictions"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isNetworkPredictionEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "navigation_error"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isResolveNavigationErrorEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "enable_remote_debugging"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isRemoteDebuggingEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "enable_web_acceleration"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isSpdyProxyEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "force_enable_zoom"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getForceEnableZoom()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSharedPreferenceValueFromNativeForSearchEngine()V
    .locals 3

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->updateSearchEngineFromNative()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "search_engine"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSearchEngine()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setWebAccelerationEnabled(Ljava/lang/Boolean;)V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setSpdyProxyEnabled(Z)V

    return-void
.end method
