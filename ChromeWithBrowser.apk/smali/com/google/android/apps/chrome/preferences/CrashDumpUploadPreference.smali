.class public Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseListPreference;

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefCrashDumpUploadPreference()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->getSummaryText(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->setSummary(I)V

    return-void
.end method


# virtual methods
.method public getSummaryText(Ljava/lang/String;)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f070257

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f07010e

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f070258

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f07010f

    goto :goto_0

    :cond_1
    const v0, 0x7f07010d

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->getSummaryText(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->setSummary(I)V

    const/4 v0, 0x1

    return v0
.end method
