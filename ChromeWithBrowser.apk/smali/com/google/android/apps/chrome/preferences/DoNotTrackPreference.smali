.class public Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mDoNotTrackEnabler:Lcom/google/android/apps/chrome/preferences/DoNotTrackEnabler;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f060008

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->addPreferencesFromResource(I)V

    new-instance v0, Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->mActionBarSwitch:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->addSwitchToActionBar(Landroid/widget/Switch;)V

    new-instance v0, Lcom/google/android/apps/chrome/preferences/DoNotTrackEnabler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/DoNotTrackEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->mDoNotTrackEnabler:Lcom/google/android/apps/chrome/preferences/DoNotTrackEnabler;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->mDoNotTrackEnabler:Lcom/google/android/apps/chrome/preferences/DoNotTrackEnabler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/DoNotTrackEnabler;->attach()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->mDoNotTrackEnabler:Lcom/google/android/apps/chrome/preferences/DoNotTrackEnabler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/DoNotTrackEnabler;->destroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setVisibility(I)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f0117

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f070112

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getDoNotTrackLearnMoreURL()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->show(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    const v0, 0x7f0f0117

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070112

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    return-void
.end method
