.class Lcom/google/android/apps/chrome/preferences/HomepagePreference$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreference;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/HomepagePreference;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreference$1;->this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreference$1;->this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreference;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/HomepagePreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreference$1;->this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreference;

    # getter for: Lcom/google/android/apps/chrome/preferences/HomepagePreference;->mCurrentPage:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/HomepagePreference;->access$000(Lcom/google/android/apps/chrome/preferences/HomepagePreference;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreference$1;->this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreference;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/HomepagePreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "chrome://newtab"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0f00da
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
