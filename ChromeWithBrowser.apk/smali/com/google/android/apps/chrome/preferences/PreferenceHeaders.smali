.class public Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceActivity;


# static fields
.field public static final FRAGMENT:Ljava/lang/String; = "fragment"

.field public static final RESULT_ACCOUNT:I = 0x1

.field private static final SETTINGS_HELP_URL:Ljava/lang/String; = "https://support.google.com/chrome/?p=mobile_settings"

.field private static final TAG:Ljava/lang/String; = "PreferenceHeaders"


# instance fields
.field private mFragments:Ljava/util/List;

.field private mHeaders:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceActivity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mFragments:Ljava/util/List;

    return-void
.end method

.method private addAccountHeader(Ljava/util/List;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->isTabletPreferencesUi(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v1}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, v1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    const-class v0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-interface {p1, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->invalidateOptionsMenu()V

    :cond_1
    return-void
.end method

.method private displayAccountPicker()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->hasGoogleAccounts()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncChooseAccountFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/SyncChooseAccountFragment;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/SyncChooseAccountFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/sync/AddGoogleAccountDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/AddGoogleAccountDialogFragment;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/AddGoogleAccountDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setDefaultSearchEngineSummary(Ljava/util/List;)V
    .locals 9

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-wide v1, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v5, 0x7f0f00d7

    cmp-long v1, v1, v5

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefSearchEnginePreference()I

    move-result v5

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getLocalizedSearchEngines()[Ljava/util/HashMap;

    move-result-object v6

    array-length v7, v6

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v7, :cond_0

    aget-object v8, v6, v2

    const-string v1, "searchEngineId"

    invoke-virtual {v8, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-ne v5, v1, :cond_2

    invoke-static {v3, v8}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getSearchEngineNameAndDomain(Landroid/content/Context;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method private setMenuVisibility(Landroid/view/Menu;I)V
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x7

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget v4, v2, v0

    if-ne v4, p2, :cond_0

    const/4 v5, 0x1

    invoke-interface {p1, v4, v5}, Landroid/view/Menu;->setGroupVisible(IZ)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {p1, v4, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_1

    :cond_1
    return-void

    nop

    :array_0
    .array-data 4
        0x7f0f010c
        0x7f0f010f
        0x7f0f0112
        0x7f0f0114
        0x7f0f0116
        0x7f0f0104
        0x7f0f0106
    .end array-data
.end method


# virtual methods
.method public getCurrentFragment()Landroid/app/Fragment;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mFragments:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mFragments:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mFragments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFragmentForTest(Ljava/lang/Class;)Landroid/app/Fragment;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeaders()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mHeaders:Ljava/util/List;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x1

    if-eq p1, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccountNames()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v2, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->get()Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceActivity;->onAttachFragment(Landroid/app/Fragment;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mFragments:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->invalidateOptionsMenu()V

    return-void
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 2

    const/4 v1, 0x4

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->isTabletPreferencesUi(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f060011

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->loadHeadersFromResource(ILjava/util/List;)V

    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->addAccountHeader(Ljava/util/List;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->setDefaultSearchEngineSummary(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mHeaders:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mHeaders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f060010

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->loadHeadersFromResource(ILjava/util/List;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Z)V
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setPathValuesForAboutChrome()V

    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessage(Landroid/nfc/NdefMessage;Landroid/app/Activity;[Landroid/app/Activity;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "PreferenceHeaders"

    const-string v2, "Failed to start browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->finish()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders$HeaderAdapter;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/SendGoogleFeedback;->setCurrentScreenshot(Landroid/graphics/Bitmap;)V

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceActivity;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8

    const v1, 0x7f0700c9

    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return v6

    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    :goto_1
    move v6, v7

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->finish()V

    goto :goto_1

    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->displayAccountPicker()V

    move v6, v7

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/SendGoogleFeedback;->launchGoogleFeedback(Landroid/content/Context;)V

    move v6, v7

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "https://support.google.com/chrome/?p=mobile_settings"

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->show(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "https://support.google.com/chrome/?p=mobile_settings"

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->show(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    goto :goto_0

    :sswitch_5
    const-class v0, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0700d6

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    move v6, v7

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksAlertDialog;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksAlertDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksAlertDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    move v6, v7

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f0105 -> :sswitch_4
        0x7f0f0107 -> :sswitch_1
        0x7f0f0108 -> :sswitch_2
        0x7f0f0109 -> :sswitch_3
        0x7f0f010a -> :sswitch_5
        0x7f0f010b -> :sswitch_6
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7

    const v2, 0x7f0f0106

    const v6, 0x7f0700c9

    const v5, 0x7f0f010b

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f0f010c

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->setMenuVisibility(Landroid/view/Menu;I)V

    :cond_0
    :goto_0
    const v0, 0x7f0f0107

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const v0, 0x7f0f0108

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const v0, 0x7f0f0109

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const v0, 0x7f0f0105

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const v0, 0x7f0f010a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    const v0, 0x7f0f0107

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v0, 0x7f0f0108

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->areBookmarksAccessible()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/UnderTheHoodPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const v0, 0x7f0f010a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    return v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0f010f

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f0f0112

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/DevToolsPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x7f0f0114

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f0f0116

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/UnderTheHoodPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_b

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    :cond_b
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->isTabletPreferencesUi(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f0104

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->updatePreferencesHeadersAndMenu()V

    return-void
.end method

.method public popFragmentList(Landroid/app/Fragment;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mFragments:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mFragments:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mHeaders:Ljava/util/List;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mHeaders:Ljava/util/List;

    invoke-interface {p1, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders$HeaderAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->mHeaders:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders$HeaderAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-super {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public updatePreferencesHeadersAndMenu()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->invalidateHeaders()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->invalidateOptionsMenu()V

    return-void
.end method
