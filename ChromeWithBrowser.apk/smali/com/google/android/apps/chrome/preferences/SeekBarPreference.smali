.class public Lcom/google/android/apps/chrome/preferences/SeekBarPreference;
.super Landroid/preference/Preference;

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field protected mMax:F

.field protected mMin:F

.field protected mStep:F

.field mSummary:Ljava/lang/CharSequence;

.field private mSummaryView:Landroid/widget/TextView;

.field private mTrackingTouch:Z

.field private mValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lcom/google/android/apps/chrome/R$styleable;->SeekBarPreference:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    const/4 v1, 0x1

    const/high16 v2, 0x42c80000

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    const/4 v1, 0x2

    const/high16 v2, 0x3f800000

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mStep:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->notifyChanged()V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    const v0, 0x7f04002b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->setLayoutResource(I)V

    return-void
.end method

.method private prefValueToSeekBarProgress(F)I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    sub-float v0, p1, v0

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mStep:F

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private seekBarProgressToPrefValue(I)F
    .locals 3

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    int-to-float v1, p1

    iget v2, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mStep:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private setValue(FZ)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iget p1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    iget p1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_2

    iput p1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->persistFloat(F)Z

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->notifyChanged()V

    :cond_2
    return-void
.end method


# virtual methods
.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummary:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    return v0
.end method

.method public isTrackingTouch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mTrackingTouch:Z

    return v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0f009e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->prefValueToSeekBarProgress(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->prefValueToSeekBarProgress(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummaryView:Landroid/widget/TextView;

    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0

    if-eqz p3, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->syncProgress(Landroid/widget/SeekBar;)V

    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_0
    check-cast p1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->min:F

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    iget v0, p1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->max:F

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    iget v0, p1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->step:F

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mStep:F

    iget v0, p1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->value:F

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->notifyChanged()V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Landroid/preference/Preference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->isPersistent()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    iput v0, v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->min:F

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    iput v0, v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->max:F

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mStep:F

    iput v0, v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->step:F

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    iput v0, v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->value:F

    move-object v0, v1

    goto :goto_0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->getPersistedFloat(F)F

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->setValue(F)V

    return-void

    :cond_0
    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mTrackingTouch:Z

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mTrackingTouch:Z

    return-void
.end method

.method public setSummary(Ljava/lang/CharSequence;)V
    .locals 2

    const/16 v1, 0x8

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummary:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    :cond_0
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummary:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummary:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummaryView:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummaryView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummaryView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummaryView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummaryView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummaryView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setValue(F)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->setValue(FZ)V

    return-void
.end method

.method syncProgress(Landroid/widget/SeekBar;)V
    .locals 2

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->seekBarProgressToPrefValue(I)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->setValue(FZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->prefValueToSeekBarProgress(F)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method
