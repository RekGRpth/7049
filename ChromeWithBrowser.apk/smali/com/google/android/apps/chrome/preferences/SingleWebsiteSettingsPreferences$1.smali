.class Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/preferences/Website$StoredDataClearedCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences$1;->this$0:Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStoredDataCleared()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences$1;->this$0:Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences$1;->this$0:Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string v2, "clear_data"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences$1;->this$0:Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->popBackIfNoSettings()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->access$000(Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;)V

    return-void
.end method
