.class public Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field public static final EXTRA_SITE:Ljava/lang/String; = "com.google.android.apps.chrome.preferences.site"

.field public static final PREF_CLEAR_DATA:Ljava/lang/String; = "clear_data"

.field public static final PREF_LOCATION_ACCESS:Ljava/lang/String; = "location_access"

.field public static final PREF_POPUP_PERMISSION:Ljava/lang/String; = "popup_permission"

.field public static final PREF_SITE_TITLE:Ljava/lang/String; = "site_title"


# instance fields
.field private mSite:Lcom/google/android/apps/chrome/preferences/Website;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->popBackIfNoSettings()V

    return-void
.end method

.method private popBackIfNoSettings()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-string v0, "site_title"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p0, v1, v2}, Landroid/preference/PreferenceActivity;->finishPreferencePanel(Landroid/app/Fragment;ILandroid/content/Intent;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.apps.chrome.preferences.site"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/Website;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    const v0, 0x7f060016

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_7

    invoke-interface {v3, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    const-string v4, "site_title"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/Website;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const-string v4, "clear_data"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/Website;->getTotalUsage()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_2

    invoke-virtual {v0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f07017f

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v4, v5}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->sizeValueToString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/google/android/apps/chrome/preferences/ClearWebsiteStorage;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/ClearWebsiteStorage;->setConfirmationListener(Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_3
    const-string v4, "location_access"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/Website;->isGeolocationAccessAllowed()Ljava/lang/Boolean;

    move-result-object v4

    if-eqz v4, :cond_4

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_5
    const-string v4, "popup_permission"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/Website;->isPopupExceptionAllowed()Ljava/lang/Boolean;

    move-result-object v4

    if-eqz v4, :cond_6

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    :cond_7
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    new-instance v1, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences$1;-><init>(Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/Website;->clearAllStoredData(Lcom/google/android/apps/chrome/preferences/Website$StoredDataClearedCallback;)V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    const-string v1, "location_access"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/Website;->setGeolocationInfoAllowed(Ljava/lang/Boolean;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/preferences/Website;->setGeolocationInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    :cond_2
    const-string v1, "popup_permission"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/Website;->setPopupExceptionInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/preferences/Website;->setPopupExceptionInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0
.end method
