.class Lcom/google/android/apps/chrome/preferences/Website$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfoClearedCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/Website;

.field final synthetic val$callback:Lcom/google/android/apps/chrome/preferences/Website$StoredDataClearedCallback;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/Website;Lcom/google/android/apps/chrome/preferences/Website$StoredDataClearedCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/Website$1;->this$0:Lcom/google/android/apps/chrome/preferences/Website;

    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/Website$1;->val$callback:Lcom/google/android/apps/chrome/preferences/Website$StoredDataClearedCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStorageInfoCleared()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website$1;->this$0:Lcom/google/android/apps/chrome/preferences/Website;

    # --operator for: Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfoCallbacksLeft:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/Website;->access$006(Lcom/google/android/apps/chrome/preferences/Website;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website$1;->val$callback:Lcom/google/android/apps/chrome/preferences/Website$StoredDataClearedCallback;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/Website$StoredDataClearedCallback;->onStoredDataCleared()V

    :cond_0
    return-void
.end method
