.class public Lcom/google/android/apps/chrome/preferences/Website;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private mAddress:Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

.field private mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;

.field private mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;

.field private mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;

.field private mStorageInfo:Ljava/util/List;

.field private mStorageInfoCallbacksLeft:I

.field private mSummary:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfo:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/Website;->mAddress:Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mTitle:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$006(Lcom/google/android/apps/chrome/preferences/Website;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfoCallbacksLeft:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfoCallbacksLeft:I

    return v0
.end method


# virtual methods
.method public addStorageInfo(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearAllStoredData(Lcom/google/android/apps/chrome/preferences/Website$StoredDataClearedCallback;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfoCallbacksLeft:I

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfoCallbacksLeft:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;

    new-instance v2, Lcom/google/android/apps/chrome/preferences/Website$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/chrome/preferences/Website$1;-><init>(Lcom/google/android/apps/chrome/preferences/Website;Lcom/google/android/apps/chrome/preferences/Website$StoredDataClearedCallback;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;->clear(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfoClearedCallback;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_1
    return-void

    :cond_2
    invoke-interface {p1}, Lcom/google/android/apps/chrome/preferences/Website$StoredDataClearedCallback;->onStoredDataCleared()V

    goto :goto_1
.end method

.method public compareByAddressTo(Lcom/google/android/apps/chrome/preferences/Website;)I
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mAddress:Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    iget-object v1, p1, Lcom/google/android/apps/chrome/preferences/Website;->mAddress:Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->compareTo(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)I

    move-result v0

    goto :goto_0
.end method

.method public getLocalStorageInfo()Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;

    return-object v0
.end method

.method public getStorageInfo()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfo:Ljava/util/List;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mSummary:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalUsage()J
    .locals 6

    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;->getSize()J

    move-result-wide v2

    add-long/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/Website;->mStorageInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;->getSize()J

    move-result-wide v4

    add-long v0, v1, v4

    move-wide v1, v0

    goto :goto_0

    :cond_1
    return-wide v1
.end method

.method public isGeolocationAccessAllowed()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->getAllowed()Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPopupExceptionAllowed()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;->getAllowed()Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGeolocationInfo(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/Website;->mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->getEmbedder()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mSummary:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setGeolocationInfoAllowed(Ljava/lang/Boolean;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->setAllowed(Ljava/lang/Boolean;)V

    :cond_0
    return-void
.end method

.method public setLocalStorageInfo(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;

    return-void
.end method

.method public setPopupExceptionInfo(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/Website;->mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;

    return-void
.end method

.method public setPopupExceptionInfoAllowed(Ljava/lang/Boolean;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Website;->mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;->setAllowed(Ljava/lang/Boolean;)V

    :cond_0
    return-void
.end method
