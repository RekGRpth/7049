.class Lcom/google/android/apps/chrome/preferences/WebsitePreference;
.super Landroid/preference/Preference;


# static fields
.field private static final LOCATION_ALLOWED:[I

.field private static final LOCATION_DENIED:[I

.field private static final POPUPS_ALLOWED:[I

.field private static final POPUPS_DENIED:[I


# instance fields
.field private mSite:Lcom/google/android/apps/chrome/preferences/Website;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x7f010002

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->LOCATION_ALLOWED:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->LOCATION_DENIED:[I

    new-array v0, v3, [I

    const v1, 0x7f010003

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->POPUPS_ALLOWED:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->POPUPS_DENIED:[I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/preferences/Website;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    const v0, 0x7f04003d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->setWidgetLayoutResource(I)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->refresh()V

    return-void
.end method

.method private refresh()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Website;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Website;->getSummary()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07017d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public compareTo(Landroid/preference/Preference;)I
    .locals 2

    instance-of v0, p1, Lcom/google/android/apps/chrome/preferences/WebsitePreference;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/chrome/preferences/WebsitePreference;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    iget-object v1, p1, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/Website;->compareByAddressTo(Lcom/google/android/apps/chrome/preferences/Website;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Landroid/preference/Preference;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->compareTo(Landroid/preference/Preference;)I

    move-result v0

    return v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 8

    const/4 v7, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0f00d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/Website;->getTotalUsage()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/Website;->getTotalUsage()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->getStorageUsageLevel(J)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageLevel(I)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    const v0, 0x7f0f00d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/Website;->isGeolocationAccessAllowed()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->LOCATION_ALLOWED:[I

    :goto_0
    invoke-virtual {v0, v1, v7}, Landroid/widget/ImageView;->setImageState([IZ)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    const v0, 0x7f0f00d3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/Website;->isPopupExceptionAllowed()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->POPUPS_ALLOWED:[I

    :goto_1
    invoke-virtual {v0, v1, v7}, Landroid/widget/ImageView;->setImageState([IZ)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    return-void

    :cond_3
    sget-object v1, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->LOCATION_DENIED:[I

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->POPUPS_DENIED:[I

    goto :goto_1
.end method

.method public putSiteIntoExtras(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v0, p1, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void
.end method
