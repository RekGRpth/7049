.class abstract Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;
.super Ljava/lang/Object;


# instance fields
.field private mNext:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;

.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)V

    return-void
.end method


# virtual methods
.method chain(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;->mNext:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;

    return-object p1
.end method

.method protected next()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;->mNext:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;->mNext:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;->run()V

    :cond_0
    return-void
.end method

.method abstract run()V
.end method
