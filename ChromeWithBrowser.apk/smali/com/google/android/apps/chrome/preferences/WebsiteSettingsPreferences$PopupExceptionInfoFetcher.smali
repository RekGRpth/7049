.class Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$PopupExceptionInfoFetcher;
.super Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$PopupExceptionInfoFetcher;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$PopupExceptionInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)V

    return-void
.end method


# virtual methods
.method run()V
    .locals 4

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->getPopupExceptionInfo()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;->getPattern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$PopupExceptionInfoFetcher;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->findOrCreateSitesByHost(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Ljava/util/Set;
    invoke-static {v3, v1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->access$700(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/Website;->setPopupExceptionInfo(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$PopupExceptionInfoFetcher;->next()V

    return-void
.end method
