.class public Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;


# instance fields
.field private mEmptyView:Landroid/widget/TextView;

.field private mSitesByHost:Ljava/util/Map;

.field private mSitesByOrigin:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByOrigin:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByHost:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByHost:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->displayEmptyScreenMessage()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Lcom/google/android/apps/chrome/preferences/Website;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->createSiteByOrigin(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Lcom/google/android/apps/chrome/preferences/Website;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Ljava/util/Set;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->findOrCreateSitesByHost(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Ljava/util/Set;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->findOrCreateSitesByOrigin(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByOrigin:Ljava/util/Map;

    return-object v0
.end method

.method private createSiteByOrigin(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Lcom/google/android/apps/chrome/preferences/Website;
    .locals 5

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getOrigin()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getHost()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/preferences/Website;

    invoke-direct {v2, p1}, Lcom/google/android/apps/chrome/preferences/Website;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByOrigin:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByOrigin:Ljava/util/Map;

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByOrigin:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByHost:Ljava/util/Map;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object v2
.end method

.method private displayEmptyScreenMessage()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mEmptyView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mEmptyView:Landroid/widget/TextView;

    const v1, 0x7f0701d8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void
.end method

.method private findOrCreateSitesByHost(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Ljava/util/Set;
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getHost()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByHost:Ljava/util/Map;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    new-instance v2, Lcom/google/android/apps/chrome/preferences/Website;

    invoke-direct {v2, p1}, Lcom/google/android/apps/chrome/preferences/Website;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)V

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method private findOrCreateSitesByOrigin(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Ljava/util/Set;
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getOrigin()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByOrigin:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->createSiteByOrigin(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Lcom/google/android/apps/chrome/preferences/Website;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByOrigin:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method private getInfoForOrigins()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByOrigin:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    new-instance v0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$GeolocationInfoFetcher;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$GeolocationInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$1;)V

    new-instance v1, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$LocalStorageInfoFetcher;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$LocalStorageInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;->chain(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;->chain(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$PopupExceptionInfoFetcher;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$PopupExceptionInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;->chain(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;->chain(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;->run()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    const v0, 0x7f06001a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mEmptyView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 2

    instance-of v0, p2, Lcom/google/android/apps/chrome/preferences/WebsitePreference;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;

    const-class v1, Lcom/google/android/apps/chrome/preferences/SingleWebsiteSettingsPreferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->setFragment(Ljava/lang/String;)V

    const-string v1, "com.google.android.apps.chrome.preferences.site"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/WebsitePreference;->putSiteIntoExtras(Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->getInfoForOrigins()V

    return-void
.end method
