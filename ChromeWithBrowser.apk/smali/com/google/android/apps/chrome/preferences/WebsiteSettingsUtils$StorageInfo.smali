.class public Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private mHost:Ljava/lang/String;

.field private mSize:J

.field private mType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clear(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfoClearedCallback;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;->mHost:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;->mType:I

    # invokes: Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->nativeClearStorageData(Ljava/lang/String;ILjava/lang/Object;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->access$300(Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;->mHost:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;->mSize:J

    return-wide v0
.end method

.method public setInfo(Ljava/lang/String;IJ)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;->mHost:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;->mType:I

    iput-wide p3, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;->mSize:J

    return-void
.end method
