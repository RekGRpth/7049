.class public abstract Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;
.super Ljava/lang/Object;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "WebsiteSettingsUtils"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1

    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->nativeGetGeolocationSettingForOrigin(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->nativeSetGeolocationSettingForOrigin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$200(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->nativeClearLocalStorageData(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->nativeClearStorageData(Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static fetchLocalStorageInfo(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfoReadyCallback;)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->nativeFetchLocalStorageInfo(Ljava/lang/Object;)V

    return-void
.end method

.method public static fetchStorageInfo(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfoReadyCallback;)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->nativeFetchStorageInfo(Ljava/lang/Object;)V

    return-void
.end method

.method public static getGeolocationInfo()Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->nativeGetGeolocationOrigins()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static getPopupExceptionInfo()Ljava/util/List;
    .locals 7

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getPopupExceptions()[Ljava/util/HashMap;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz v2, :cond_0

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v0, v2, v1

    new-instance v5, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;

    const-string v6, "displayPattern"

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v5, v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$PopupExceptionInfo;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public static getStorageUsageLevel(J)I
    .locals 5

    long-to-float v0, p0

    const/high16 v1, 0x49800000

    div-float/2addr v0, v1

    float-to-double v1, v0

    const-wide v3, 0x3fb999999999999aL

    cmpg-double v1, v1, v3

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/high16 v1, 0x40a00000

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private static native nativeClearLocalStorageData(Ljava/lang/String;)V
.end method

.method private static native nativeClearStorageData(Ljava/lang/String;ILjava/lang/Object;)V
.end method

.method private static native nativeFetchLocalStorageInfo(Ljava/lang/Object;)V
.end method

.method private static native nativeFetchStorageInfo(Ljava/lang/Object;)V
.end method

.method private static native nativeGetGeolocationOrigins()Ljava/util/ArrayList;
.end method

.method private static native nativeGetGeolocationSettingForOrigin(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;
.end method

.method private static native nativeSetGeolocationSettingForOrigin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
.end method

.method public static sizeValueToString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7

    const/high16 v5, 0x44800000

    const/4 v1, 0x0

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const v0, 0x7f070180

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    const/4 v0, 0x1

    const v3, 0x7f070181

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const v3, 0x7f070182

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const v3, 0x7f070183

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const v3, 0x7f070184

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const-wide/16 v3, 0x0

    cmp-long v0, p1, v3

    if-gtz v0, :cond_0

    const-string v0, "WebsiteSettingsUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sizeValueToString called with non-positive value: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "0"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    long-to-float v0, p1

    :goto_1
    array-length v3, v2

    if-ge v1, v3, :cond_1

    cmpg-float v3, v0, v5

    if-ltz v3, :cond_1

    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-eq v1, v3, :cond_1

    div-float/2addr v0, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    new-instance v3, Ljava/text/DecimalFormat;

    const-string v4, "#.##"

    invoke-direct {v3, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    float-to-double v5, v0

    invoke-virtual {v3, v5, v6}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
