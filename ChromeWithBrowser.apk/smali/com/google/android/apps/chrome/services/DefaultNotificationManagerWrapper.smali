.class public Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;


# instance fields
.field private mNotificationManager:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/app/NotificationManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;->mNotificationManager:Landroid/app/NotificationManager;

    return-void
.end method


# virtual methods
.method public cancel(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public notify(ILandroid/app/Notification;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method
