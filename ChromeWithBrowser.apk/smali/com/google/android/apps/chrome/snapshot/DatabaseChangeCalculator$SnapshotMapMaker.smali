.class abstract Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;
.super Ljava/lang/Object;


# instance fields
.field private mDocumentMap:Ljava/util/Map;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;->mDocumentMap:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;-><init>()V

    return-void
.end method


# virtual methods
.method protected createMap(Ljava/util/Collection;)Ljava/util/Map;
    .locals 4

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;->getValidId(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;->mDocumentMap:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;->mDocumentMap:Ljava/util/Map;

    return-object v0
.end method

.method protected abstract getValidId(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
.end method
