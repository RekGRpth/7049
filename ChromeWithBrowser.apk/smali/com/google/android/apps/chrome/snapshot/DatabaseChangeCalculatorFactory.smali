.class public Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculatorFactory;
.super Ljava/lang/Object;


# static fields
.field private static sDatabaseChangeCalculatorInstance:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;

.field private static final sDatabaseChangeCalculatorInstanceLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculatorFactory;->sDatabaseChangeCalculatorInstanceLock:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculatorFactory;->sDatabaseChangeCalculatorInstance:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;
    .locals 2

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculatorFactory;->sDatabaseChangeCalculatorInstanceLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculatorFactory;->sDatabaseChangeCalculatorInstance:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculatorFactory;->sDatabaseChangeCalculatorInstance:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculatorFactory;->sDatabaseChangeCalculatorInstance:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
