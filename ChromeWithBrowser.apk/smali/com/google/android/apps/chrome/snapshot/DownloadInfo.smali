.class public Lcom/google/android/apps/chrome/snapshot/DownloadInfo;
.super Ljava/lang/Object;


# instance fields
.field private final mDownloadId:J

.field private final mMediaType:Ljava/lang/String;

.field private final mReason:I

.field private final mStatus:I


# direct methods
.method public constructor <init>(JIILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mDownloadId:J

    iput p3, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mStatus:I

    iput p4, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mReason:I

    iput-object p5, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mMediaType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDownloadId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mDownloadId:J

    return-wide v0
.end method

.method public getMediaType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mMediaType:Ljava/lang/String;

    return-object v0
.end method

.method public getReason()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mReason:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mStatus:I

    return v0
.end method
