.class public Lcom/google/android/apps/chrome/snapshot/HeaderField;
.super Ljava/lang/Object;


# instance fields
.field private final mName:Ljava/lang/String;

.field private final mValue:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/HeaderField;->mName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/chrome/snapshot/HeaderField;->mValue:Ljava/lang/String;

    return-void
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/HeaderField;
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/HeaderField;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/snapshot/HeaderField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/HeaderField;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/HeaderField;->mValue:Ljava/lang/String;

    return-object v0
.end method
