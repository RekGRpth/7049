.class public Lcom/google/android/apps/chrome/snapshot/SlugGenerator;
.super Ljava/lang/Object;


# static fields
.field public static final DEFAULT_MAX_LENGTH:I = 0x32

.field private static final SINGLE_SPACE:Ljava/util/regex/Pattern;

.field public static final SINGLE_SPACE_REPLACEMENT:Ljava/lang/String; = " "

.field private static final SLUG_DASH:Ljava/util/regex/Pattern;

.field public static final SLUG_DASH_REPLACEMENT:Ljava/lang/String; = "-"

.field private static final VALID_CHARS:Ljava/util/regex/Pattern;

.field public static final VALID_CHARS_REPLACEMENT:Ljava/lang/String; = ""


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "[^a-z0-9\\s\\.-]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SlugGenerator;->VALID_CHARS:Ljava/util/regex/Pattern;

    const-string v0, "[\\s\\.-]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SlugGenerator;->SINGLE_SPACE:Ljava/util/regex/Pattern;

    const-string v0, "[\\s]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SlugGenerator;->SLUG_DASH:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generateSlug(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/16 v0, 0x32

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SlugGenerator;->generateSlug(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static generateSlug(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SlugGenerator;->VALID_CHARS:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SlugGenerator;->replace(Ljava/util/regex/Matcher;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SlugGenerator;->SINGLE_SPACE:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SlugGenerator;->replace(Ljava/util/regex/Matcher;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SlugGenerator;->SLUG_DASH:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, "-"

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SlugGenerator;->replace(Ljava/util/regex/Matcher;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static replace(Ljava/util/regex/Matcher;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :goto_0
    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0, p1}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
