.class Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;
.super Ljava/lang/Exception;


# instance fields
.field private final mResourceId:I


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput p2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;->mResourceId:I

    return-void
.end method


# virtual methods
.method public getResourceId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;->mResourceId:I

    return v0
.end method
