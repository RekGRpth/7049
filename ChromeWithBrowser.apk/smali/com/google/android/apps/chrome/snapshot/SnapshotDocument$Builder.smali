.class public Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
.super Ljava/lang/Object;


# instance fields
.field private mTempCreateTime:J

.field private mTempDownloadId:J

.field private mTempDownloadUri:Landroid/net/Uri;

.field private mTempId:I

.field private mTempJobId:Ljava/lang/String;

.field private mTempLocalUri:Landroid/net/Uri;

.field private mTempMimeType:Ljava/lang/String;

.field private mTempPageUrlDownloadId:J

.field private mTempPageUrlDownloadUri:Landroid/net/Uri;

.field private mTempPageUrlJobId:Ljava/lang/String;

.field private mTempPageUrlMimeType:Ljava/lang/String;

.field private mTempPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

.field private mTempPrinterId:Ljava/lang/String;

.field private mTempSnapshotId:Ljava/lang/String;

.field private mTempState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field private mTempTitle:Ljava/lang/String;

.field private mTempUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->this$0:Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$200(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempId:I

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$300(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$400(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempSnapshotId:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$500(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPrinterId:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$600(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempJobId:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$700(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlJobId:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$800(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempTitle:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$900(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempUri:Landroid/net/Uri;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1000(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadUri:Landroid/net/Uri;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1100(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadUri:Landroid/net/Uri;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1200(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempMimeType:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1300(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlMimeType:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1400(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempLocalUri:Landroid/net/Uri;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1500(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadId:J

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1600(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadId:J

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1700(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1800(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;-><init>(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;
    .locals 23

    new-instance v2, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempId:I

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempSnapshotId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPrinterId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempJobId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlJobId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempLocalUri:Landroid/net/Uri;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadId:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadId:J

    move-wide/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    move-object/from16 v22, v0

    invoke-direct/range {v2 .. v22}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;-><init>(IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)V

    return-object v2
.end method

.method public withCreateTimeIfNewer(J)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    :cond_0
    iput-wide p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    :cond_1
    return-object p0
.end method

.method public withDownloadId(J)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadId:J

    :cond_0
    return-object p0
.end method

.method public withDownloadUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadUri:Landroid/net/Uri;

    :cond_0
    return-object p0
.end method

.method public withId(I)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempId:I

    :cond_0
    return-object p0
.end method

.method public withJobId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempJobId:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public withLocalUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempLocalUri:Landroid/net/Uri;

    :cond_0
    return-object p0
.end method

.method public withMimeType(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempMimeType:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public withPageUrlDownloadId(J)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadId:J

    :cond_0
    return-object p0
.end method

.method public withPageUrlDownloadUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadUri:Landroid/net/Uri;

    :cond_0
    return-object p0
.end method

.method public withPageUrlJobId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlJobId:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public withPageUrlMimeType(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlMimeType:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public withPageUrlState(Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    :cond_0
    return-object p0
.end method

.method public withPageUrlStateIfNotSet(Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withPageUrlState(Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public withPrinterId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPrinterId:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public withSnapshotId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempSnapshotId:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public withState(Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    :cond_0
    return-object p0
.end method

.method public withStateIfNotSet(Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withState(Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public withTitle(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempTitle:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public withUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempUri:Landroid/net/Uri;

    :cond_0
    return-object p0
.end method
