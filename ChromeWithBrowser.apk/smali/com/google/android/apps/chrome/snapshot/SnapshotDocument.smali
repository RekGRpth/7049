.class public Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator; = null

.field private static final NO_CREATE_TIME:J = -0x1L

.field public static final NO_ID:I = -0x1


# instance fields
.field private mCreateTime:J

.field private mDownloadId:J

.field private mDownloadUri:Landroid/net/Uri;

.field private mId:I

.field private mJobId:Ljava/lang/String;

.field private mLocalUri:Landroid/net/Uri;

.field private mMimeType:Ljava/lang/String;

.field private mPageUrlDownloadId:J

.field private mPageUrlDownloadUri:Landroid/net/Uri;

.field private mPageUrlJobId:Ljava/lang/String;

.field private mPageUrlMimeType:Ljava/lang/String;

.field private mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

.field private mPrinterId:Ljava/lang/String;

.field private mSnapshotId:Ljava/lang/String;

.field private mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field private mTitle:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$1;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    iput-wide p2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    iput-object p4, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    iput-object p10, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    iput-object p11, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    iput-object p12, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    iput-object p13, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)V
    .locals 21

    const/4 v1, -0x1

    move-object/from16 v0, p0

    move-wide/from16 v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-wide/from16 v15, p14

    move-wide/from16 v17, p16

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    invoke-direct/range {v0 .. v20}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;-><init>(IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_4
    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    :goto_5
    iput-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    return-void

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    :cond_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    :cond_4
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->fromValue(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-result-object v0

    goto :goto_4

    :cond_5
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->fromValue(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    move-result-object v1

    goto :goto_5
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    return-wide v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    return-wide v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public static createBuilder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->builder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public builder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;-><init>(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$1;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCreateTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    return-wide v0
.end method

.method public getDownloadId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    return-wide v0
.end method

.method public getDownloadUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    return v0
.end method

.method public getJobId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getPageUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getPageUrlDownloadId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    return-wide v0
.end method

.method public getPageUrlDownloadUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getPageUrlJobId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    return-object v0
.end method

.method public getPageUrlMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getPageUrlState()Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    return-object v0
.end method

.method public getPrinterId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    return-object v0
.end method

.method public getSnapshotId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setId(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const/16 v3, 0x27

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SnapshotDocument{mId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCreateTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSnapshotId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPrinterId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mJobId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPageUrlJobId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTitle=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDownloadUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPageUrlDownloadUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMimeType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPageUrlMimeType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLocalUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDownloadId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPageUrlDownloadId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mState=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPageUrlState=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    if-nez v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->toValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->toValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_5
.end method
