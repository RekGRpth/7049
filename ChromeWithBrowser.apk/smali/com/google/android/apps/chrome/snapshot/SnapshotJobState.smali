.class public final enum Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field public static final enum DOWNLOADED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field public static final enum DOWNLOADED_AND_OPENED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field public static final enum DOWNLOADING:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field public static final enum DOWNLOAD_PENDING:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field public static final enum ERROR:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field public static final enum FINISHED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field public static final enum UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field public static final enum UNKNOWN:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const-string v1, "UNKNOWN"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNKNOWN:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const-string v1, "DOWNLOAD_PENDING"

    const-string v2, "DOWNLOAD_PENDING"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOAD_PENDING:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const-string v1, "UNABLE_TO_DOWNLOAD"

    const-string v2, "UNABLE_TO_DOWNLOAD"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const-string v1, "DOWNLOADING"

    const-string v2, "DOWNLOADING"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADING:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const-string v1, "DOWNLOADED"

    const-string v2, "DOWNLOADED"

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const-string v1, "DOWNLOADED_AND_OPENED"

    const/4 v2, 0x5

    const-string v3, "DOWNLOADED_AND_OPENED"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADED_AND_OPENED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const-string v1, "FINISHED"

    const/4 v2, 0x6

    const-string v3, "FINISHED"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->FINISHED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const-string v1, "ERROR"

    const/4 v2, 0x7

    const-string v3, "ERROR"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ERROR:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNKNOWN:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOAD_PENDING:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADING:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADED_AND_OPENED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->FINISHED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ERROR:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->$VALUES:[Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->value:Ljava/lang/String;

    return-void
.end method

.method public static fromValue(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;
    .locals 5

    if-eqz p0, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->values()[Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->$VALUES:[Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    return-object v0
.end method


# virtual methods
.method public final toValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->value:Ljava/lang/String;

    return-object v0
.end method
