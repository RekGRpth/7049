.class final Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->sListeners:Ljava/util/LinkedList;
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->access$000()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$Listener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$Listener;->snapshotStateChanged()V

    goto :goto_0

    :cond_0
    return-void
.end method
