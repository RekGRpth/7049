.class public Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sListeners:Ljava/util/LinkedList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->$assertionsDisabled:Z

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->sListeners:Ljava/util/LinkedList;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/LinkedList;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->sListeners:Ljava/util/LinkedList;

    return-object v0
.end method

.method public static addListener(Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$Listener;)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->sListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static alertListeners()V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$1;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$1;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static getSummaryResourceId(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->getAcquiringAuthToken(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f070091

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static removeListener(Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$Listener;)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->sListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
