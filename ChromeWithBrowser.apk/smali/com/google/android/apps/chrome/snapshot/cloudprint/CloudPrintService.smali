.class public Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;
.super Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;


# static fields
.field public static final ACTION_CONTROL:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_CONTROL"

.field private static final ACTION_DELETE_JOB:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_DELETE_JOB"

.field public static final ACTION_FETCH:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_FETCH"

.field public static final ACTION_SYNC_STATE_WITH_SERVER:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_SYNC_STATE_WITH_SERVER"

.field public static final EXTRA_C2DM_REGISTRATION_ID:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_C2DM_REGISTRATION_ID"

.field private static final EXTRA_JOB_ID:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_JOB_ID"

.field private static final EXTRA_STATUS:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_STATUS"

.field private static final TAG:Ljava/lang/String; = "CloudPrintService"

.field private static sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "CloudPrintService"

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->setIntentRedelivery(Z)V

    return-void
.end method

.method private clearNeedsUpdatingAndStoreApplicationVersionAndTimestamp()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setNeedsUpdating(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/base/BuildInfo;->getPackageVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setApplicationVersion(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->setLastUpdatedTimestamp(Landroid/content/Context;)V

    return-void
.end method

.method public static createControlIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_CONTROL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_JOB_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_STATUS"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->getStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public static createDeleteJobIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_DELETE_JOB"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_JOB_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public static createFetchIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_FETCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public static createSyncStateWithServerIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_SYNC_STATE_WITH_SERVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public static createSyncStateWithServerWithC2dmRegistrationIdIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->createSyncStateWithServerIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_C2DM_REGISTRATION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public static getDownloadRequest(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->createDownloadRequest(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;

    move-result-object v0

    return-object v0
.end method

.method private getLocalPrinterData(Landroid/content/Intent;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getGcmRegistrationId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->createDefaultPrinterData(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    const-string v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_C2DM_REGISTRATION_ID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_C2DM_REGISTRATION_ID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withC2dmRegistrationIdTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private handleControl(Landroid/content/Intent;)V
    .locals 5

    const-string v0, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_STATUS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    move-result-object v0

    const-string v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_JOB_ID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const-string v1, "CloudPrintService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleControl(): Unable to find jobid when trying to set print job status to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const-string v2, "CloudPrintService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "handleControl(): Trying to set print job status to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for print job = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->controlPrintJob(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createPrintJobStatusChangedIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v2, "CloudPrintService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to set job status to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " for jobid "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleDeleteJob(Landroid/content/Intent;)V
    .locals 4

    const-string v0, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_JOB_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string v1, "CloudPrintService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleDeleteJob(): Unable to find jobid when trying to delete print job = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const-string v1, "CloudPrintService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleDeleteJob(): Trying to delete print job = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->deletePrintJob(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createJobDeletedIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v1, "CloudPrintService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleDeleteJob(): Unable to delete print job "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleFetch()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "CloudPrintService"

    const-string v2, "Fetching external print jobs"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->fetchPrintJobs()Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createPrintJobFetchResultIntent(Landroid/content/Context;Ljava/util/Set;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    return-void
.end method

.method private handleIntent(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_SYNC_STATE_WITH_SERVER"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleSyncStateWithServer(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_FETCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleFetch()V

    goto :goto_0

    :cond_1
    const-string v0, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_CONTROL"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleControl(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v0, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_DELETE_JOB"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleDeleteJob(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    const-string v0, "CloudPrintService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got unknown action from intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleSyncStateWithServer(Landroid/content/Intent;)V
    .locals 4

    const-string v0, "CloudPrintService"

    const-string v1, "Syncing state with Cloud Print server"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getLocalPrinterData(Landroid/content/Intent;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getServerPrinterData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->isEnabled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->deletePrinter(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "CloudPrintService"

    const-string v1, "Successfully deleted printer registration."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->clearPrinterId(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->clearNeedsUpdatingAndStoreApplicationVersionAndTimestamp()V

    goto :goto_0

    :cond_3
    if-nez v0, :cond_6

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->hasC2dmRegistrationIdTag()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "CloudPrintService"

    const-string v1, "No C2DM registration ID found. Not registering printer."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->registerPrinter(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    if-nez v0, :cond_5

    const-string v0, "CloudPrintService"

    const-string v1, "Failed to register printer. Giving up for now."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    const-string v2, "CloudPrintService"

    const-string v3, "Successfully registered mobile device as printer."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    :cond_6
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->setStateSynchronizedAndFetchPrintJobs(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V

    goto :goto_0

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->updatePrinter(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->setStateSynchronizedAndFetchPrintJobs(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V

    goto :goto_0
.end method

.method public static setHttpClientFactoryForTest(Lcom/google/android/apps/chrome/utilities/HttpClientFactory;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    return-void
.end method

.method private setLastUpdatedTimestamp(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setLastUpdatedTimestamp(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private setStateSynchronizedAndFetchPrintJobs(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V
    .locals 2

    const-string v0, "CloudPrintService"

    const-string v1, "Synchronization of state with Cloud Print server complete."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setPrinterId(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->clearNeedsUpdatingAndStoreApplicationVersionAndTimestamp()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createFetchPrintJobsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method controlPrintJob(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Z
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->controlJob(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Z

    move-result v1

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    return v1
.end method

.method deletePrintJob(Ljava/lang/String;)Z
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->deleteJob(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Z

    move-result v1

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    return v1
.end method

.method deletePrinter(Ljava/lang/String;)Z
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->delete(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Z

    move-result v1

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    return v1
.end method

.method fetchPrintJobs()Ljava/util/Set;
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->fetchJobs(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    return-object v1
.end method

.method findPrinter(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->find(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    return-object v1
.end method

.method generateProxyId()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getDefaultProxy()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getGcmRegistrationId()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/android/b/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServerPrinterData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->hasPrinterId(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPrinterId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->findPrinter(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->generateProxyId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->listPrinters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    const-string v2, "CloudPrintService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Size of printers for Gservices ID proxy ID = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " too large: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ". Taking the first result."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method listPrinters(Ljava/lang/String;)Ljava/util/List;
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->list(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    return-object v1
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->onCreate()V

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    :cond_0
    const-string v0, "GSERVICES_ANDROID_ID"

    new-instance v1, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/identity/UniqueIdentificationGeneratorFactory;->registerGenerator(Ljava/lang/String;Lcom/google/android/apps/chrome/identity/UniqueIdentificationGenerator;Z)V

    return-void
.end method

.method protected onHandleIntentWithWakeLock(Landroid/content/Intent;)V
    .locals 3

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "CloudPrintService"

    const-string v2, "CloudPrint authentication failed:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->invalidateAndAcquireNewAuthToken(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleIntent(Landroid/content/Intent;)V
    :try_end_1
    .catch Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "CloudPrintService"

    const-string v2, "Hit Exception with new auth token: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_0
    const-string v0, "CloudPrintService"

    const-string v1, "Failed to get new auth token."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method registerPrinter(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->register(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    return-object v1
.end method

.method startServiceWithWakeLockInternal(Landroid/content/Intent;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->startServiceWithWakeLock(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method updatePrinter(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Z
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->update(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Z

    move-result v1

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    return v1
.end method
