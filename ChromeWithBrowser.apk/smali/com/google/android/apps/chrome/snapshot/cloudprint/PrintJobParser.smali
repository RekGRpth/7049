.class public Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobParser;
.super Ljava/lang/Object;


# static fields
.field private static final C2DM_JOB_DATA_TAG:Ljava/lang/String; = "__c2dm__job_data="

.field private static final JOB_CONTENT_TYPE:Ljava/lang/String; = "contentType"

.field private static final JOB_CREATE_TIME:Ljava/lang/String; = "createTime"

.field private static final JOB_DATA_SNAP_ID:Ljava/lang/String; = "snapID"

.field private static final JOB_DATA_TYPE:Ljava/lang/String; = "type"

.field private static final JOB_DATA_URL:Ljava/lang/String; = "url"

.field private static final JOB_FILE_URL:Ljava/lang/String; = "fileUrl"

.field private static final JOB_JOB_ID:Ljava/lang/String; = "id"

.field private static final JOB_TAGS:Ljava/lang/String; = "tags"

.field private static final JOB_TITLE:Ljava/lang/String; = "title"

.field private static final TAG:Ljava/lang/String; = "PrintJobParser"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static parseJobData(Lorg/json/JSONArray;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;
    .locals 3

    if-eqz p0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "__c2dm__job_data="

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x11

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobParser;->parsePrintJobJobData(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static parsePrintJob(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;
    .locals 8

    :try_start_0
    const-string v0, "createTime"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v0, "id"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "title"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "fileUrl"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const-string v0, "contentType"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "tags"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobParser;->parseJobData(Lorg/json/JSONArray;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v7

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "PrintJobParser"

    const-string v2, "Failed to parse print job"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parsePrintJobJobData(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;
    .locals 5

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "snapID"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "snapID"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    :goto_1
    const-string v1, "url"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "url"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    const-string v2, "type"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v1, :cond_3

    move-object v2, v0

    :goto_3
    new-instance v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    invoke-direct {v1, v3, v2, v4}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v3, v0

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_2

    :cond_3
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v2, v1

    goto :goto_3

    :catch_0
    move-exception v1

    const-string v2, "PrintJobParser"

    const-string v3, "Failed to parse print job"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
