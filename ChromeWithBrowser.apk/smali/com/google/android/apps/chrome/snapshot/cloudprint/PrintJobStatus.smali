.class public final enum Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

.field public static final enum DONE:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

.field public static final enum ERROR:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

.field public static final enum QUEUED:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

.field public static final enum SPOOLED:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;


# instance fields
.field private final mStatus:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    const-string v1, "QUEUED"

    const-string v2, "QUEUED"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->QUEUED:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    const-string v1, "SPOOLED"

    const-string v2, "SPOOLED"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->SPOOLED:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    const-string v1, "DONE"

    const-string v2, "DONE"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->DONE:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    const-string v1, "ERROR"

    const-string v2, "ERROR"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->ERROR:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->QUEUED:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->SPOOLED:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->DONE:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->ERROR:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->$VALUES:[Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->mStatus:Ljava/lang/String;

    return-void
.end method

.method public static parsePrintJobStatus(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;
    .locals 5

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->values()[Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->getStatus()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->$VALUES:[Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    return-object v0
.end method


# virtual methods
.method public final getStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->mStatus:Ljava/lang/String;

    return-object v0
.end method
