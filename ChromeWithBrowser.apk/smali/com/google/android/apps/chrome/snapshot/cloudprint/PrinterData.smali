.class public Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
.super Ljava/lang/Object;


# instance fields
.field private mApplicationVersionTag:Ljava/lang/String;

.field private mC2dmRegistrationIdTag:Ljava/lang/String;

.field private mCapabilities:Ljava/lang/String;

.field private mCapabilitiesHash:Ljava/lang/String;

.field private mDefaultValues:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mId:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mProtocolVersionTag:Ljava/lang/String;

.field private mProxy:Ljava/lang/String;

.field private mStatus:Ljava/lang/String;

.field private mType:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 13

    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, v1

    move-object v11, v1

    move-object v12, v1

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    iput-object p12, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    return-void
.end method

.method private build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 13

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    iget-object v12, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static create()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;-><init>()V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    if-nez v2, :cond_4

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    if-nez v2, :cond_7

    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    if-nez v2, :cond_a

    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    goto :goto_0

    :cond_e
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    if-nez v2, :cond_d

    :cond_f
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    goto :goto_0

    :cond_11
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    if-nez v2, :cond_10

    :cond_12
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    goto/16 :goto_0

    :cond_14
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    if-nez v2, :cond_13

    :cond_15
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    goto/16 :goto_0

    :cond_17
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    if-nez v2, :cond_16

    :cond_18
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    if-eqz v2, :cond_1a

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    goto/16 :goto_0

    :cond_1a
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    if-nez v2, :cond_19

    :cond_1b
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    :cond_1c
    move v0, v1

    goto/16 :goto_0

    :cond_1d
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    if-nez v2, :cond_1c

    :cond_1e
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    :cond_1f
    move v0, v1

    goto/16 :goto_0

    :cond_20
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    if-nez v2, :cond_1f

    :cond_21
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    if-eqz v2, :cond_23

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_24

    :cond_22
    move v0, v1

    goto/16 :goto_0

    :cond_23
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    if-nez v2, :cond_22

    :cond_24
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    if-eqz v2, :cond_25

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto/16 :goto_0

    :cond_25
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public getApplicationVersionTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    return-object v0
.end method

.method public getC2dmRegistrationIdTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    return-object v0
.end method

.method public getCapabilities()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    return-object v0
.end method

.method public getCapabilitiesHash()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultValues()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocolVersionTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    return-object v0
.end method

.method public getProxy()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public hasC2dmRegistrationIdTag()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasId()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto/16 :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_6

    :cond_8
    move v0, v1

    goto :goto_7

    :cond_9
    move v0, v1

    goto :goto_8

    :cond_a
    move v0, v1

    goto :goto_9

    :cond_b
    move v0, v1

    goto :goto_a
.end method

.method public withApplicationVersionTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withC2dmRegistrationIdTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withCapabilities(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withCapabilitiesHash(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withDefaultValues(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withDescription(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withName(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withProtocolVersionTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withProxy(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withStatus(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withType(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method
