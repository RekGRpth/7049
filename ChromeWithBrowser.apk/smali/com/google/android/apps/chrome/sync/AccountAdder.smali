.class public Lcom/google/android/apps/chrome/sync/AccountAdder;
.super Ljava/lang/Object;


# static fields
.field private static final EXTRA_ACCOUNT_TYPES:Ljava/lang/String; = "account_types"

.field private static final EXTRA_VALUE_GOOGLE_ACCOUNTS:Ljava/lang/String; = "com.google"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createAddAccountIntent()Landroid/content/Intent;
    .locals 5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "account_types"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "com.google"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public addAccount(Landroid/app/Activity;I)V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/sync/AccountAdder;->createAddAccountIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public addAccount(Landroid/app/Fragment;I)V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/sync/AccountAdder;->createAddAccountIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
