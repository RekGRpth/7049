.class final Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$1;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$ctx:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$1;->val$ctx:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$1;->val$ctx:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationController;->getContractAuthority()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$1;->val$account:Landroid/accounts/Account;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v1, v0, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    const/4 v0, 0x0

    return-object v0
.end method
