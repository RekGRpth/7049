.class public Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;
.super Landroid/app/Service;


# static fields
.field private static sSyncAdapter:Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;

.field private static final sSyncAdapterLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;->sSyncAdapterLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private getOrMakeSyncAdapter(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;
    .locals 3

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;->sSyncAdapterLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;->getApplication()Landroid/app/Application;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;-><init>(Landroid/content/Context;Landroid/app/Application;)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;->getOrMakeSyncAdapter(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
