.class final enum Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

.field public static final enum MSG_READY:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

.field public static final enum MSG_REGISTRATION_FAILURE:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

.field public static final enum MSG_REGISTRATION_STATUS:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

.field public static final enum MSG_REISSUE_REGISTRATIONS:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

.field public static final enum MSG_REQUEST_SYNC:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

.field public static final enum MSG_SHUTDOWN:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

.field public static final enum MSG_START:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

.field public static final enum MSG_STOP:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    const-string v1, "MSG_REGISTRATION_FAILURE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REGISTRATION_FAILURE:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    const-string v1, "MSG_REGISTRATION_STATUS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REGISTRATION_STATUS:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    const-string v1, "MSG_REQUEST_SYNC"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REQUEST_SYNC:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    const-string v1, "MSG_REISSUE_REGISTRATIONS"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REISSUE_REGISTRATIONS:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    const-string v1, "MSG_STOP"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_STOP:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    const-string v1, "MSG_READY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_READY:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    const-string v1, "MSG_START"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_START:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    const-string v1, "MSG_SHUTDOWN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_SHUTDOWN:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REGISTRATION_FAILURE:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REGISTRATION_STATUS:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REQUEST_SYNC:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REISSUE_REGISTRATIONS:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_STOP:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_READY:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_START:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_SHUTDOWN:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->$VALUES:[Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->$VALUES:[Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    return-object v0
.end method
