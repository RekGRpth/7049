.class public Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field static final ARG_IS_GAIA:Ljava/lang/String; = "is_gaia"

.field static final ARG_IS_UPDATE:Ljava/lang/String; = "is_update"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->handleOk()V

    return-void
.end method

.method private getListener()Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;

    goto :goto_0
.end method

.method private handleCancel()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "is_update"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "is_gaia"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getListener()Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;->onPassphraseCanceled(ZZ)V

    return-void
.end method

.method private handleOk()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f0f00b9

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f07009e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f0f00b1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "is_update"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "is_gaia"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getListener()Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;

    move-result-object v3

    invoke-interface {v3, v0, v2, v1}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;->onPassphraseEntered(Ljava/lang/String;ZZ)V

    return-void
.end method

.method public static newInstance(Landroid/app/Fragment;ZZ)Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;-><init>()V

    if-eqz p0, :cond_0

    const/4 v1, -0x1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "is_gaia"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "is_update"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public invalidPassphrase()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f0f00b9

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f07009d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->handleCancel()V

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040034

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "is_gaia"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    const v0, 0x7f0f00b8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f00b1

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v4

    if-eqz v3, :cond_0

    const v3, 0x7f070098

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f07009a

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setHint(I)V

    const v0, 0x7f070227

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    new-instance v0, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$1;-><init>(Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;)V

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v3, 0x3

    invoke-direct {v0, v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070073

    new-instance v2, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$2;-><init>(Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070072

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07007a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$3;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$3;-><init>(Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    return-object v0

    :cond_0
    invoke-static {v4}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->hasExplicitPassphraseTime()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getSyncDecryptionPassphraseType()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$4;->$SwitchMap$com$google$android$apps$chrome$sync$SyncDecryptionPassphraseType:[I

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    sget-object v3, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Found incorrect passphrase type "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". Falling back to default string."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getSyncEnterCustomPassphraseBodyText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const v0, 0x7f070096

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setHint(I)V

    const v0, 0x7f070228

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_0
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getSyncEnterGooglePassphraseBodyWithDateText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getSyncEnterCustomPassphraseBodyWithDateText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getSyncEnterCustomPassphraseBodyText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
