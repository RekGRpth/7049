.class Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;
.super Landroid/widget/ArrayAdapter;


# instance fields
.field private final mElementsContainer:Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;

.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;[Ljava/lang/String;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;->this$0:Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x109000f

    const v2, 0x1020014

    invoke-direct {p0, v0, v1, v2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;->mElementsContainer:Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;[Ljava/lang/String;Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;-><init>(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getItemId(I)J
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;->getType(I)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->internalValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getPositionForType(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;->mElementsContainer:Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->getElements()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;

    # getter for: Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPassphraseType:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->access$500(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v2

    if-ne v2, p1, :cond_0

    # getter for: Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPosition:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->access$000(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)I

    move-result v0

    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->INVALID:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->internalValue()I

    move-result v0

    goto :goto_0
.end method

.method public getType(I)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;->mElementsContainer:Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->getElements()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;

    # getter for: Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPassphraseType:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->access$500(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;->this$0:Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;

    # invokes: Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getCurrentTypeFromArguments()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    invoke-static {v1}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->access$600(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;->this$0:Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;

    # invokes: Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getAllowedTypesFromArguments()Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->access$700(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;->getPositionForType(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;)I

    move-result v1

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;->getType(I)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
