.class Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;
.super Ljava/lang/Object;


# instance fields
.field private final mDisplayName:Ljava/lang/String;

.field private final mPassphraseType:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

.field private mPosition:I


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPassphraseType:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mDisplayName:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->INVALID:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->internalValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPosition:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;Ljava/lang/String;Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;-><init>(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPosition:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPosition:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPassphraseType:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    return-object v0
.end method
