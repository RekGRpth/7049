.class Lcom/google/android/apps/chrome/sync/RegistrationManager;
.super Ljava/lang/Object;


# instance fields
.field protected mRegistrationStatuses:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Landroid/os/Handler;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/a/b/C;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-static {}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->values()[Lorg/chromium/sync/internal_api/pub/base/ModelType;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    new-instance v4, Lcom/google/android/apps/chrome/sync/RegistrationStatus;

    invoke-direct {v4, p1, p2, v3}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;-><init>(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Landroid/os/Handler;Lorg/chromium/sync/internal_api/pub/base/ModelType;)V

    iget-object v5, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method firePendingRegistrationsForTest()V
    .locals 5

    invoke-static {}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->values()[Lorg/chromium/sync/internal_api/pub/base/ModelType;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->firePendingRegistrationForTest()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method getPendingRegistrations()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->REGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/RegistrationManager;->getPendingRegistrations(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method getPendingRegistrations(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Ljava/util/Set;
    .locals 4

    invoke-static {}, Lcom/google/a/b/B;->b()Ljava/util/HashSet;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->isPending(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method getRegisteredTypes()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->REGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/RegistrationManager;->getRegisteredTypes(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method getRegisteredTypes(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Ljava/util/Set;
    .locals 6

    invoke-static {}, Lcom/google/a/b/B;->b()Ljava/util/HashSet;

    move-result-object v2

    invoke-static {}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->values()[Lorg/chromium/sync/internal_api/pub/base/ModelType;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->isRegistered(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method informRegistrationFailure(Lorg/chromium/sync/internal_api/pub/base/ModelType;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->informRegistrationFailure()V

    return-void
.end method

.method informRegistrationStatus(Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->informRegistrationStatus(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    return-void
.end method

.method reissueRegistration(Lorg/chromium/sync/internal_api/pub/base/ModelType;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->reissueRegistration()V

    return-void
.end method

.method reissueRegistrations()V
    .locals 4

    invoke-static {}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->values()[Lorg/chromium/sync/internal_api/pub/base/ModelType;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/sync/RegistrationManager;->reissueRegistration(Lorg/chromium/sync/internal_api/pub/base/ModelType;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method setRegisteredTypes(Ljava/util/Set;)V
    .locals 5

    invoke-static {}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->values()[Lorg/chromium/sync/internal_api/pub/base/ModelType;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->registerType()V

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->unRegisterType()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method stop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationManager;->mRegistrationStatuses:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->stop()V

    goto :goto_0

    :cond_0
    return-void
.end method
