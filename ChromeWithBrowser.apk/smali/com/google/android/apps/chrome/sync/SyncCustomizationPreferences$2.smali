.class Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getSelectedCheckBoxPreferences()Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$900(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mActionBarSwitch:Landroid/widget/Switch;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1000(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)Landroid/widget/Switch;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1100(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mAllTypes:[Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1200(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)[Landroid/preference/CheckBoxPreference;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->setPreferencesEnabled(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1300(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Z)V

    :cond_1
    return-void
.end method
