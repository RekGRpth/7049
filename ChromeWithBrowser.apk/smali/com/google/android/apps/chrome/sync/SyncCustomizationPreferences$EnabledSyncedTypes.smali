.class Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;
.super Ljava/lang/Object;


# instance fields
.field private mAutofillEnabled:Z

.field private mBookmarksEnabled:Z

.field private mOmniboxEnabled:Z

.field private mOpenTabsEnabled:Z

.field private mPasswordsEnabled:Z

.field private mSyncEverything:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mSyncEverything:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mSyncEverything:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mAutofillEnabled:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mAutofillEnabled:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mBookmarksEnabled:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mBookmarksEnabled:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mOmniboxEnabled:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mOmniboxEnabled:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mPasswordsEnabled:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mPasswordsEnabled:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mOpenTabsEnabled:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mOpenTabsEnabled:Z

    return p1
.end method
