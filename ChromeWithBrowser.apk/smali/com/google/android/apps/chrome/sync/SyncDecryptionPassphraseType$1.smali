.class final Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->fromInternalValue(I)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType$1;->newArray(I)[Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method
