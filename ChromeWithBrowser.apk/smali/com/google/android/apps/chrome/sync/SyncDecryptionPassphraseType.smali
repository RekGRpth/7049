.class public final enum Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
.super Ljava/lang/Enum;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

.field public static CREATOR:Landroid/os/Parcelable$Creator;

.field public static final enum CUSTOM_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

.field public static final enum FROZEN_IMPLICIT_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

.field public static final enum IMPLICIT_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

.field public static final enum INVALID:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

.field public static final enum KEYSTORE_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

.field public static final enum NONE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;


# instance fields
.field private final mNativeValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    const-string v1, "INVALID"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->INVALID:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    const-string v1, "NONE"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->NONE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    const-string v1, "IMPLICIT_PASSPHRASE"

    invoke-direct {v0, v1, v5, v3}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->IMPLICIT_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    const-string v1, "KEYSTORE_PASSPHRASE"

    invoke-direct {v0, v1, v6, v4}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->KEYSTORE_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    const-string v1, "FROZEN_IMPLICIT_PASSPHRASE"

    invoke-direct {v0, v1, v7, v5}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->FROZEN_IMPLICIT_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    const-string v1, "CUSTOM_PASSPHRASE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->CUSTOM_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    sget-object v1, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->INVALID:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->NONE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->IMPLICIT_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->KEYSTORE_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->FROZEN_IMPLICIT_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->CUSTOM_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->$VALUES:[Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType$1;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->mNativeValue:I

    return-void
.end method

.method public static fromInternalValue(I)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 5

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->values()[Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->internalValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->INVALID:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->$VALUES:[Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final internalValue()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->mNativeValue:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->mNativeValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
