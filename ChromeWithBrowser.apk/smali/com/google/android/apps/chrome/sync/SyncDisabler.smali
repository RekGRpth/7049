.class public Lcom/google/android/apps/chrome/sync/SyncDisabler;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "SyncDisabler"


# instance fields
.field private final mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

.field private final mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lorg/chromium/sync/notifier/SyncStatusHelper;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    return-void
.end method

.method private isSyncSuppressStart()Z
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->syncSuppressStart()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public syncStateChanged()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncDisabler;->isSyncSuppressStart()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v2, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v2

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    const-string v1, "SyncDisabler"

    const-string v2, "Disabling sync because it was disabled externally"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStates;->create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->build()Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    :cond_0
    return-void
.end method
