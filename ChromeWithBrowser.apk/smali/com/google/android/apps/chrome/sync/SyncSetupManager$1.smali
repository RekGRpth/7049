.class Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$states:Lcom/google/android/apps/chrome/sync/SyncStates;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->val$states:Lcom/google/android/apps/chrome/sync/SyncStates;

    iput-object p3, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->val$states:Lcom/google/android/apps/chrome/sync/SyncStates;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncStates;->hasSync()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->val$account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->val$states:Lcom/google/android/apps/chrome/sync/SyncStates;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setSyncState(Landroid/accounts/Account;Lcom/google/android/apps/chrome/sync/SyncStates;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->access$100(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Landroid/accounts/Account;Lcom/google/android/apps/chrome/sync/SyncStates;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->val$states:Lcom/google/android/apps/chrome/sync/SyncStates;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncStates;->hasSendToDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->val$states:Lcom/google/android/apps/chrome/sync/SyncStates;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncStates;->isSendToDeviceEnabled()Z

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setSendToDeviceState(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->access$200(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->val$states:Lcom/google/android/apps/chrome/sync/SyncStates;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncStates;->hasAutoLoginSet()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;->val$states:Lcom/google/android/apps/chrome/sync/SyncStates;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncStates;->isAutoLoginEnabled()Z

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setAutologinState(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->access$300(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Z)V

    :cond_2
    return-void
.end method
