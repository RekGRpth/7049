.class Lcom/google/android/apps/chrome/sync/SyncSetupManager$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->access$400(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "SyncSetupManager"

    const-string v1, "Ignoring pref change because user is not signed in to Chrome"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;
    invoke-static {v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->access$400(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabledForChrome(Landroid/accounts/Account;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;
    invoke-static {v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->access$400(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getPreviousMasterSyncState()Z
    invoke-static {v3}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->access$500(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getWantedSyncState(Landroid/accounts/Account;)Z
    invoke-static {v4, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->access$600(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Landroid/accounts/Account;)Z

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/sync/SyncStateCalculator;->calculateNewSyncStates(Landroid/accounts/Account;ZZZZ)Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$2;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    goto :goto_0
.end method
