.class Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

.field final synthetic val$invalidAuthToken:Ljava/lang/String;

.field final synthetic val$username:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lorg/chromium/sync/signin/AccountManagerHelper;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->val$accountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    iput-object p3, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->val$account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->val$invalidAuthToken:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->val$username:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->val$accountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->val$account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->val$invalidAuthToken:Ljava/lang/String;

    const-string v3, "chromiumsync"

    invoke-virtual {v0, v1, v2, v3}, Lorg/chromium/sync/signin/AccountManagerHelper;->getNewAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method public onPostExecute(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    const-string v0, "SyncSetupManager"

    const-string v1, "Auth token for sync was null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->access$700(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)I

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SyncSetupManager"

    const-string v1, "Successfully retrieved sync auth token."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->access$700(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->val$username:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeTokenAvailable(ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->access$800(Lcom/google/android/apps/chrome/sync/SyncSetupManager;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "SyncSetupManager"

    const-string v1, "Native sync setup manager not valid."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
