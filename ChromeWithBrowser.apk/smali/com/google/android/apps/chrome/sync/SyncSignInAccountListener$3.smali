.class Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->access$200(Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->updatePreferencesHeadersAndMenu()V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->addAccountHeader()V

    :cond_0
    return-void
.end method
