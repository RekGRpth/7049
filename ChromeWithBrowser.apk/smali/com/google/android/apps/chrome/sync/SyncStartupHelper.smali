.class public Lcom/google/android/apps/chrome/sync/SyncStartupHelper;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

.field private mDestroyed:Z

.field private final mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDestroyed:Z

    return v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->removeSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDestroyed:Z

    return-void
.end method

.method public startSync(Landroid/app/Activity;Landroid/accounts/Account;)V
    .locals 3

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDestroyed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;->startupComplete()V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;-><init>(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;Landroid/app/Activity;Landroid/accounts/Account;)V

    invoke-static {p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    const-string v2, "chromiumsync"

    invoke-virtual {v1, p1, p2, v2, v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenFromForeground(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    goto :goto_0
.end method

.method public syncStateChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;->startupComplete()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getAuthError()Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;->NONE:Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->hasUnrecoverableError()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;->startupFailed()V

    goto :goto_0
.end method
