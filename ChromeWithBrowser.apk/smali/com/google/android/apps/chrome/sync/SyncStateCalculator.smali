.class public Lcom/google/android/apps/chrome/sync/SyncStateCalculator;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calculateNewSyncStates(Landroid/accounts/Account;ZZZZ)Lcom/google/android/apps/chrome/sync/SyncStates;
    .locals 3

    invoke-static {p1, p2, p3, p4}, Lcom/google/android/apps/chrome/sync/SyncStateCalculator;->getNewSyncState(ZZZZ)Z

    move-result v0

    invoke-static {p1, p2, p3, p4}, Lcom/google/android/apps/chrome/sync/SyncStateCalculator;->getNewWantedSyncState(ZZZZ)Z

    move-result v1

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStates;->create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->masterSyncState(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->wantedSyncState(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->build()Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v0

    return-object v0
.end method

.method private static getNewSyncState(ZZZZ)Z
    .locals 1

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    if-nez p2, :cond_2

    if-eqz p3, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getNewWantedSyncState(ZZZZ)Z
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_2

    :cond_0
    move p3, v1

    :cond_1
    :goto_0
    return p3

    :cond_2
    move p3, v0

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_1

    if-nez p2, :cond_4

    if-eqz p3, :cond_4

    move p3, v1

    goto :goto_0

    :cond_4
    move p3, v0

    goto :goto_0
.end method
