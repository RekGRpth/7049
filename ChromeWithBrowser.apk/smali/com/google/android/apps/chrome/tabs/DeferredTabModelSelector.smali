.class public Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mCurrentTabModelIndex:I

.field private final mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

.field private final mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-direct {v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;-><init>()V

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-direct {v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;-><init>()V

    aput-object v2, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->init()V

    return-void
.end method

.method private removeIdsFromModel(Ljava/util/ArrayList;Z)V
    .locals 4

    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0, p2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v0}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v2, v0}, Lcom/google/android/apps/chrome/TabModel;->closeTab(Lcom/google/android/apps/chrome/Tab;)Z

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public closeAllTabs(Z)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getDeferredTabModel(Z)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->removeAllTabs()V

    return-void
.end method

.method public closeTab(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->removeTabById(II)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    rsub-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->removeTabById(II)Z

    :cond_0
    return-void
.end method

.method public createTab(IIZZ)V
    .locals 1

    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getDeferredTabModel(Z)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p4}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->addTab(IIZ)V

    return-void
.end method

.method public getCurrentModel()Lcom/google/android/apps/chrome/tabs/DeferredTabModel;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getCurrentModelIndex()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    return v0
.end method

.method public getCurrentTabId()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCurrentTabId()I

    move-result v0

    return v0
.end method

.method public getDeferredTabModel(I)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getDeferredTabModel(Z)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    aget-object v0, v1, v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getModelFromTabId(I)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    rsub-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    rsub-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTabId(I)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabId(I)I

    move-result v0

    return v0
.end method

.method public getTabId(IZ)I
    .locals 1

    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getDeferredTabModel(Z)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabId(I)I

    move-result v0

    return v0
.end method

.method public init()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModelIndex()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    aget-object v0, v0, v2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->init(Lcom/google/android/apps/chrome/TabModel;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    aget-object v0, v0, v3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v1, v3}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->init(Lcom/google/android/apps/chrome/TabModel;)V

    return-void
.end method

.method public isIncognitoSelected()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveTab(IIZ)V
    .locals 1

    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getDeferredTabModel(Z)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->moveTab(II)V

    return-void
.end method

.method public propagateAll()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->propagateModel()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->propagateCurrentTab()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->propagateTabClosing()V

    return-void
.end method

.method public propagateCurrentTab()V
    .locals 4

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCurrentTabId()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->isIncognitoSelected()Z

    move-result v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v3

    if-ne v0, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCurrentTabIndex()I

    move-result v0

    if-ne v0, v1, :cond_2

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :cond_2
    :goto_1
    if-eq v0, v1, :cond_0

    invoke-interface {v2, v0}, Lcom/google/android/apps/chrome/TabModel;->setIndex(I)V

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v0

    invoke-interface {v2, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->requestFocus(Z)V

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public propagateModel()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->isIncognitoSelected()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->selectModel(Z)V

    return-void
.end method

.method public propagateTabClosing()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->flushRemovedIds()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->removeIdsFromModel(Ljava/util/ArrayList;Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->flushRemovedIds()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->removeIdsFromModel(Ljava/util/ArrayList;Z)V

    :cond_1
    return-void
.end method

.method public selectModel(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCurrentTabId(I)I
    .locals 2

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentTabId()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->setCurrentTabId(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    rsub-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mDeferredTabModels:[Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->setCurrentTabId(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->mCurrentTabModelIndex:I

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentTabId()I

    move-result v0

    goto :goto_0
.end method
