.class public Lcom/google/android/apps/chrome/tabs/TabBorder;
.super Ljava/lang/Object;


# static fields
.field private static final HEADER_PERCENTAGE_WIDTH:F = 0.5f

.field public static final RES_BACK:I = 0x7f0a001c

.field public static final RES_BACK_LOGO:I = 0x7f020090

.field public static final RES_BORDER:I = 0x7f02001c

.field public static final RES_CLOSE:I = 0x7f020049

.field public static final RES_FADE:I = 0x7f020016

.field public static final RES_FRAME:I = 0x7f020018

.field public static final RES_INCOGNITO_BACK:I = 0x7f0a001d

.field public static final RES_INCOGNITO_BORDER:I = 0x7f02001a

.field public static final RES_INCOGNITO_CLOSE:I = 0x7f02004a

.field public static final RES_INCOGNITO_FADE:I = 0x7f020017

.field public static final RES_INCOGNITO_FRAME:I = 0x7f020019

.field public static final RES_INCOGNITO_TAB:I = 0x7f02001e

.field public static final RES_TAB:I = 0x7f02001d

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mBorderThickness:Landroid/graphics/RectF;

.field private final mCloseBitmapSize:Landroid/graphics/PointF;

.field private final mCloseButtonMargin:Landroid/graphics/RectF;

.field private final mClosePlacement:Landroid/graphics/RectF;

.field private final mContentPadding:Landroid/graphics/RectF;

.field private final mFrameA:Landroid/graphics/Rect;

.field private final mFrameB:Landroid/graphics/Rect;

.field private final mFrameMargin:Landroid/graphics/RectF;

.field private final mHeaderBitmapPlacement:Landroid/graphics/RectF;

.field private final mMinContentSizeToBeVisible:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/TabBorder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/TabBorder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseBitmapSize:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mClosePlacement:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02001c

    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v2, v2

    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020049

    invoke-static {v4, v5, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseBitmapSize:Landroid/graphics/PointF;

    iget v5, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v5, v5

    iput v5, v4, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseBitmapSize:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v1, v1

    iput v1, v4, Landroid/graphics/PointF;->y:F

    const v1, 0x7f080025

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const v4, 0x7f080026

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    const v5, 0x7f080027

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    const v6, 0x7f080028

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    new-instance v7, Landroid/graphics/RectF;

    sub-float/2addr v2, v5

    sub-float/2addr v3, v6

    invoke-direct {v7, v1, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v7, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mContentPadding:Landroid/graphics/RectF;

    const v1, 0x7f080029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    const v2, 0x7f08002a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    const v3, 0x7f08002b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    int-to-float v3, v3

    const v4, 0x7f08002c

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    int-to-float v4, v4

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mHeaderBitmapPlacement:Landroid/graphics/RectF;

    const v1, 0x7f08002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const v2, 0x7f08002f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const v3, 0x7f080030

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const v4, 0x7f080031

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mBorderThickness:Landroid/graphics/RectF;

    const v1, 0x7f080042

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    const v2, 0x7f080043

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    const v3, 0x7f080044

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    const v4, 0x7f080045

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mFrameA:Landroid/graphics/Rect;

    const v1, 0x7f080046

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    const v2, 0x7f080047

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    const v3, 0x7f080048

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    const v4, 0x7f080049

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mFrameB:Landroid/graphics/Rect;

    const v1, 0x7f08004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const v2, 0x7f08004b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const v3, 0x7f08004c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const v4, 0x7f08004d

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mFrameMargin:Landroid/graphics/RectF;

    const v1, 0x7f08004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const v2, 0x7f08004f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const v3, 0x7f080050

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const v4, 0x7f080051

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseButtonMargin:Landroid/graphics/RectF;

    const v1, 0x7f080052

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mMinContentSizeToBeVisible:F

    return-void
.end method

.method public static getBackColor(Landroid/content/Context;)I
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public static getHeaderWidth(F)F
    .locals 1

    const/high16 v0, 0x3f000000

    mul-float/2addr v0, p0

    return v0
.end method

.method public static getIncognitoBackColor(Landroid/content/Context;)I
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public static getMaxTitleWidth(Landroid/content/res/Resources;)I
    .locals 3

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    const/high16 v2, 0x3f000000

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public getBorderThickness()Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mBorderThickness:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getCloseButtonMargin()Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseButtonMargin:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getCloseRectangle(ZF)Landroid/graphics/RectF;
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mClosePlacement:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseButtonMargin:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float v1, p2, v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseBitmapSize:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mClosePlacement:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseButtonMargin:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float v1, p2, v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mClosePlacement:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseButtonMargin:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mClosePlacement:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseButtonMargin:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseBitmapSize:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mClosePlacement:Landroid/graphics/RectF;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mClosePlacement:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseButtonMargin:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mClosePlacement:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseButtonMargin:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseBitmapSize:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mClosePlacement:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseButtonMargin:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mClosePlacement:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseButtonMargin:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mCloseBitmapSize:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method public getFrameBitmapA()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mFrameA:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getFrameBitmapB()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mFrameB:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getFrameBitmapMargin()Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mFrameMargin:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getHeaderBitmapPlacement()Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mHeaderBitmapPlacement:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getMinContentSizeToBeVisible()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mMinContentSizeToBeVisible:F

    return v0
.end method

.method public getPadding()Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TabBorder;->mContentPadding:Landroid/graphics/RectF;

    return-object v0
.end method
