.class public interface abstract Lcom/google/android/apps/chrome/tabs/TabsView;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;
.end method

.method public abstract getView()Landroid/view/View;
.end method

.method public abstract isTabInteractive()Z
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public abstract onViewportSizeChanged(II)V
.end method

.method public abstract setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
.end method
