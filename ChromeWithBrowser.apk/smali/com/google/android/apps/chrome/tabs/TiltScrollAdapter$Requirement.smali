.class public final enum Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

.field public static final enum APP_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

.field public static final enum LAYOUT_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

.field public static final enum LAYOUT_VISIBLE:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

.field public static final enum NO_ANIMATION:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

.field public static final enum NO_TOUCH:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

.field public static final enum WINDOW_FOCUSED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    const-string v1, "APP_ENABLED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->APP_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    const-string v1, "WINDOW_FOCUSED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->WINDOW_FOCUSED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    const-string v1, "LAYOUT_ENABLED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    const-string v1, "LAYOUT_VISIBLE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_VISIBLE:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    const-string v1, "NO_TOUCH"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->NO_TOUCH:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    const-string v1, "NO_ANIMATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->NO_ANIMATION:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->APP_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->WINDOW_FOCUSED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_VISIBLE:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->NO_TOUCH:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->NO_ANIMATION:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->$VALUES:[Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->$VALUES:[Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    return-object v0
.end method
