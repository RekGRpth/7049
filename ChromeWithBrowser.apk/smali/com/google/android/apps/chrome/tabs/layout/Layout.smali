.class public abstract Lcom/google/android/apps/chrome/tabs/layout/Layout;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$TextureChangeListener;


# static fields
.field public static final NEED_TITLE:Z = true

.field public static final NO_CLOSE_BUTTON:Z = false

.field public static final NO_TITLE:Z = false

.field public static final RES_LOGO:I = 0x7f020091

.field public static final SHOW_CLOSE_BUTTON:Z = true

.field private static final TAG:Ljava/lang/String;

.field public static final UNSTALLED_ANIMATION_DURATION_MS:J = 0x1f4L


# instance fields
.field private final mBackgroundColor:I

.field private mCurrentOrientation:I

.field protected mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

.field private final mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

.field private mFreezeTextureSource:Z

.field protected mHeight:I

.field private mIsHiding:Z

.field private mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

.field protected mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field protected final mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

.field private mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

.field private final mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

.field protected mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/Layout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mWidth:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mHeight:I

    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    iput-object p3, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    iput-object p4, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mWidth:I

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mHeight:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mCurrentOrientation:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mBackgroundColor:I

    return-void
.end method

.method public static broadcastAnimationFinished(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x29

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->broadcastAnimationStateChanged(Ljava/lang/Class;Ljava/lang/String;I)V

    return-void
.end method

.method public static broadcastAnimationStarted(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x28

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->broadcastAnimationStateChanged(Ljava/lang/Class;Ljava/lang/String;I)V

    return-void
.end method

.method private static broadcastAnimationStateChanged(Ljava/lang/Class;Ljava/lang/String;I)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz p0, :cond_0

    const-string v1, "source"

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz p1, :cond_1

    const-string v1, "animation"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {p2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method protected addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V
    .locals 10

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-wide v5, p5

    move-wide/from16 v7, p7

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZ)V

    return-void
.end method

.method protected addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZ)V
    .locals 11

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getDecelerateInterpolator()Landroid/view/animation/DecelerateInterpolator;

    move-result-object v10

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move/from16 v9, p9

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    return-void
.end method

.method protected addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V
    .locals 1

    invoke-static/range {p1 .. p10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->createAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    return-void
.end method

.method protected addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationStarted()V

    new-instance v0, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->start()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->requestUpdate()V

    return-void
.end method

.method protected animationIsRunning()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bitmapLoaded(Lcom/google/android/apps/chrome/tabs/BitmapRequester;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/BitmapRequester;->loadRequestAbandoned()V

    invoke-interface {p1, p2}, Lcom/google/android/apps/chrome/tabs/BitmapRequester;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->requestRender()V

    return-void
.end method

.method protected cancelAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/ChromeAnimation;->cancel(Ljava/lang/Object;Ljava/lang/Enum;)V

    :cond_0
    return-void
.end method

.method public cleanupInstanceData(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpdateAnimation(JZ)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationFinished()V

    :cond_0
    return-void
.end method

.method public click(JFF)V
    .locals 0

    return-void
.end method

.method public contextChanged(Landroid/content/Context;)V
    .locals 0

    invoke-static {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->resetDimensionConstants(Landroid/content/Context;)V

    return-void
.end method

.method public createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->initLayoutTabFromHost(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)Z

    return-object v0
.end method

.method public doneHiding()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mIsHiding:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mFreezeTextureSource:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->doneHiding()V

    return-void
.end method

.method public drag(JFFFF)V
    .locals 0

    return-void
.end method

.method public fling(JFFFF)V
    .locals 0

    return-void
.end method

.method protected forceAnimationToFinish()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationFinished()V

    :cond_0
    return-void
.end method

.method public getBackgroundColor()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mBackgroundColor:I

    return v0
.end method

.method protected getCacheMaximumSize()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getMaximumCacheSize()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEventFilter()Lcom/google/android/apps/chrome/eventfilter/EventFilter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    return-object v0
.end method

.method public getFreezeTextureSource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mFreezeTextureSource:Z

    return v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mHeight:I

    return v0
.end method

.method protected getLayoutTab(I)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v1, v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getLayoutTabsDrawnCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->getLayoutTabsDrawnCount()I

    move-result v0

    return v0
.end method

.method public getLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    return-object v0
.end method

.method public getOrientation()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mCurrentOrientation:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mWidth:I

    return v0
.end method

.method public handlesCloseAll()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public handlesTabClosing()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public handlesTabCreating()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected initLayoutTabFromHost(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)Z
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isInitFromHostNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->markAsWaitingForInitFromHost()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->initLayoutTabFromHost(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHiding()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mIsHiding:Z

    return v0
.end method

.method public isListeningTiltScroll()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isTabInteractive()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected notifySizeChanged(III)V
    .locals 0

    return-void
.end method

.method protected onAnimationFinished()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x29

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->broadcastAnimationStateChanged(Ljava/lang/Class;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method protected onAnimationStarted()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x28

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->broadcastAnimationStateChanged(Ljava/lang/Class;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public onDown(JFF)V
    .locals 0

    return-void
.end method

.method public onLongPress(JFF)V
    .locals 0

    return-void
.end method

.method public onPinch(JFFFFZ)V
    .locals 0

    return-void
.end method

.method public onTextureChange(IZ)V
    .locals 0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->requestUpdate()V

    :cond_0
    return-void
.end method

.method public onTiltScroll(F)V
    .locals 0

    return-void
.end method

.method public onUpOrCancel(J)V
    .locals 0

    return-void
.end method

.method public final onUpdate(JJ)Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpdateAnimation(JZ)Z

    move-result v0

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->updateLayout(JJ)V

    return v0
.end method

.method protected onUpdateAnimation(JZ)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v1, :cond_2

    if-eqz p3, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    :goto_0
    if-nez v0, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationFinished()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->requestUpdate()V

    :cond_2
    return v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/ChromeAnimation;->update(J)Z

    move-result v0

    goto :goto_0
.end method

.method public propagateTabClosing()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->propagateTabClosing()V

    return-void
.end method

.method public propagateTabModel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->propagateModel()V

    return-void
.end method

.method public releaseTabLayout(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->releaseTabLayout(I)V

    return-void
.end method

.method public requestRender()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->requestRender()V

    return-void
.end method

.method public requestUpdate()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    return-void
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->removeTextureChangeListener(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$TextureChangeListener;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->addTextureChangeListener(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$TextureChangeListener;)V

    :cond_1
    return-void
.end method

.method public shouldDisplayContentOverlay()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public shouldDisplayTabDecoration()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public show(JZ)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mIsHiding:Z

    return-void
.end method

.method public final sizeChanged(III)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mWidth:I

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mHeight:I

    iput p3, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mCurrentOrientation:I

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->notifySizeChanged(III)V

    return-void
.end method

.method public startHiding(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getLayoutTab(I)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->startHiding()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mIsHiding:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->setCurrentTabId(I)I

    return-void
.end method

.method public supportsFullscreen()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public swipeFinished()V
    .locals 0

    return-void
.end method

.method public swipeFlingOccurred(FFFF)V
    .locals 0

    return-void
.end method

.method public swipeStarted(Z)V
    .locals 0

    return-void
.end method

.method public swipeUpdated(FF)V
    .locals 0

    return-void
.end method

.method public tabClosed(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->closeTab(II)V

    return-void
.end method

.method public tabClosing(JI)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    const/4 v1, -0x1

    invoke-virtual {v0, p3, v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->closeTab(II)V

    return-void
.end method

.method public tabCreated(JIIIZZ)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    if-nez p7, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p3, p4, p6, v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->createTab(IIZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tabCreating(I)V
    .locals 0

    return-void
.end method

.method public tabModelSwitched(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->selectModel(Z)V

    return-void
.end method

.method public tabMoved(IIZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->moveTab(IIZ)V

    return-void
.end method

.method public tabSelected(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->setCurrentTabId(I)I

    return-void
.end method

.method public tabSelecting(JI)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->setCurrentTabId(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->startHiding(I)V

    return-void
.end method

.method public tabsAllClosing(JZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->closeAllTabs(Z)V

    return-void
.end method

.method protected updateCacheVisibleIds(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->updateVisibleIds(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method protected abstract updateLayout(JJ)V
.end method
