.class public final enum Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

.field public static final enum FOREGROUND:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

.field public static final enum HIDDEN:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

.field public static final enum PREPARING_TO_HIDE:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

.field public static final enum TRYING_TO_HIDE:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    const-string v1, "FOREGROUND"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->FOREGROUND:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    const-string v1, "PREPARING_TO_HIDE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->PREPARING_TO_HIDE:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    const-string v1, "TRYING_TO_HIDE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->TRYING_TO_HIDE:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->HIDDEN:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->FOREGROUND:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->PREPARING_TO_HIDE:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->TRYING_TO_HIDE:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->HIDDEN:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    return-object v0
.end method
