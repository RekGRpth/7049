.class public Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;
.super Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

# interfaces
.implements Lcom/google/android/apps/chrome/OverviewBehavior;
.implements Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
.implements Lcom/google/android/apps/chrome/tabs/TiltScrollListener;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mEnableAnimations:Z

.field protected mGestureEventFilter:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

.field private mIncognitoTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

.field private final mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

.field private final mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

.field private mStandardTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

.field private final mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x28
        0x29
        0x30
        0x24
        0x2b
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    iput-boolean v8, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mEnableAnimations:Z

    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getLayoutRenderHost()Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    move-result-object v3

    new-instance v0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    invoke-direct {v0, v1, p2, p0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mGestureEventFilter:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mGestureEventFilter:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    invoke-direct {v0, v1, p0, v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mBlackHoleEventFilter:Lcom/google/android/apps/chrome/eventfilter/BlackHoleEventFilter;

    invoke-direct {v0, v1, p0, v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mEdgeSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-direct {v0, v1, p0, v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mEdgeSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    move-object v2, p0

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;ZZZ)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->setNextLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/TiltScrollListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStandardTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    invoke-direct {v0, v1, v8}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mIncognitoTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    return-void
.end method

.method private closeAllTabsRequest(Z)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->handlesCloseAll()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabsAllClosing(JZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->closeAllTabs(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->propagateTabClosing()V

    goto :goto_0
.end method

.method private getRotation()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private overviewNotify(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->overviewNotify(Lcom/google/android/apps/chrome/tabs/layout/Layout;I)V

    return-void
.end method

.method private overviewNotify(Lcom/google/android/apps/chrome/tabs/layout/Layout;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-ne p1, v0, :cond_0

    invoke-static {p2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    :cond_0
    return-void
.end method

.method private tabClosing(I)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-ne v1, v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cacheTabThumbnail(Lcom/google/android/apps/chrome/TabThumbnailProvider;)Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->pinTexture(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getVisibilityState()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->FOREGROUND:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->handlesTabClosing()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabClosing(JI)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mEnableAnimations:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabClosing(JI)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->finalizeShowing()V

    goto :goto_0
.end method

.method private tabCreating(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getVisibilityState()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->FOREGROUND:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->handlesTabCreating()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabCreating(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mEnableAnimations:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabCreating(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->finalizeShowing()V

    goto :goto_0
.end method


# virtual methods
.method public cleanupLayout()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->cleanupLayout()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->cleanupInstanceData(J)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->cleanupInstanceData(J)V

    return-void
.end method

.method public click(FF)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->click(JFF)V

    :cond_0
    return-void
.end method

.method public doneHiding()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->doneHiding()V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->overviewNotify(Lcom/google/android/apps/chrome/tabs/layout/Layout;I)V

    return-void
.end method

.method public drag(FFFF)V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->drag(JFFFF)V

    :cond_0
    return-void
.end method

.method protected finalizeShowing()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->finalizeShowing()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_VISIBLE:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->resume(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    :cond_0
    return-void
.end method

.method public fling(FFFF)V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->fling(JFFFF)V

    :cond_0
    return-void
.end method

.method protected getNotificationsToRegisterFor()[I
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->NOTIFICATIONS:[I

    return-object v0
.end method

.method public getOverviewBehavior()Lcom/google/android/apps/chrome/OverviewBehavior;
    .locals 0

    return-object p0
.end method

.method public getStackLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    return-object v0
.end method

.method public getTitleBitmap(Lcom/google/android/apps/chrome/Tab;)Landroid/graphics/Bitmap;
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mIncognitoTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->create(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStandardTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    goto :goto_0
.end method

.method protected handleNotificationMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->NO_ANIMATION:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->pause(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->NO_ANIMATION:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->resume(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_OVERVIEW:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_MENU:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LINK:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-ne v0, v1, :cond_0

    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "sourceTabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->tabCreating(I)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "animate"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->tabClosing(I)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "incognito"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->closeAllTabsRequest(Z)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x24 -> :sswitch_3
        0x28 -> :sswitch_0
        0x29 -> :sswitch_1
        0x2b -> :sswitch_4
        0x30 -> :sswitch_2
    .end sparse-switch
.end method

.method public hideOverview(Z)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getVisibilityState()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->FOREGROUND:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabSelecting(JI)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->startHiding()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->doneHiding()V

    goto :goto_0
.end method

.method public isListening()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->isListeningTiltScroll()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTiltScrollActive()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->isActive()Z

    move-result v0

    return v0
.end method

.method public onContextChanged(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->onContextChanged(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStandardTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mIncognitoTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    return-void
.end method

.method public onDown(FF)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onDown(JFF)V

    :cond_0
    return-void
.end method

.method public onEndGesture()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->NO_TOUCH:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->passiveResume(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    return-void
.end method

.method public onHostFocusChanged(Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->onHostFocusChanged(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->WINDOW_FOCUSED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->resume(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->WINDOW_FOCUSED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->pause(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    goto :goto_0
.end method

.method public onLongPress(FF)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onLongPress(JFF)V

    :cond_0
    return-void
.end method

.method public onPinch(FFFFZ)V
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onPinch(JFFFFZ)V

    :cond_0
    return-void
.end method

.method public onStartGesture()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->NO_TOUCH:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->passivePause(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    return-void
.end method

.method public onTiltScroll(F)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTiltScroll(F)V

    :cond_0
    return-void
.end method

.method public onUpOrCancel()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpOrCancel(J)V

    :cond_0
    return-void
.end method

.method public onViewportSizeChanged(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->onViewportSizeChanged(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getRotation()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->onRotationChange(I)V

    return-void
.end method

.method public overviewVisible()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getVisibilityState()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->FOREGROUND:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setChromeViewHolder(Lcom/google/android/apps/chrome/ContentViewHolder;)V
    .locals 0

    return-void
.end method

.method public setEnableAnimations(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mEnableAnimations:Z

    return-void
.end method

.method protected setLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->setLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    return-void
.end method

.method protected setLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->setLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->overviewNotify(I)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->isListeningTiltScroll()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->resume(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->pause(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    goto :goto_0
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->setTabModelSelector(Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->setTabModelSelector(Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    return-void
.end method

.method public setTiltControlsEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->APP_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->resume(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->APP_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->pause(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    goto :goto_0
.end method

.method public setTiltControlsLayoutEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->resume(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->pause(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    goto :goto_0
.end method

.method public showOverview(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->finalizeShowing()V

    return-void
.end method

.method public startHiding()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->startHiding()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTiltScrollAdapter:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_VISIBLE:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->pause(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V

    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->overviewNotify(I)V

    return-void
.end method

.method protected tabClosed(II)V
    .locals 3

    const/4 v1, 0x1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->layoutVisible()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-eq v1, v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->setNextLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabClosed(II)V

    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->requestFocus()Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mEnableAnimations:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->finalizeShowing()V

    goto :goto_1
.end method

.method protected tabCreated(IILcom/google/android/apps/chrome/TabModel$TabLaunchType;ZZ)V
    .locals 2

    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->tabCreated(IILcom/google/android/apps/chrome/TabModel$TabLaunchType;ZZ)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p3, p5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->shouldTabBeLaunchedInForeground(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0, p4}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->requestFocus()Z

    :cond_0
    return-void
.end method
