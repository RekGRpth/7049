.class public Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;
.super Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V
    .locals 8

    const/4 v5, 0x1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getLayoutRenderHost()Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    move-result-object v3

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->mEdgeSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-direct {v0, v1, p0, v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->mSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->mEdgeSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    move-object v2, p0

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;ZZZ)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->setNextLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    return-void
.end method


# virtual methods
.method public swipeStarted(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->swipeStarted(Z)V

    :cond_0
    return-void
.end method

.method public tabSelected(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->mSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-ne v0, v1, :cond_2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->tabSelected(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabSelected(I)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->finalizeShowing()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabSelecting(JI)V

    goto :goto_0
.end method
