.class public Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final ALPHA_THRESHOLD:F = 0.003921569f

.field private static final INIT_FROM_HOST_DONE:I = 0x2

.field private static final INIT_FROM_HOST_NEEDED:I = 0x0

.field private static final INIT_FROM_HOST_WAITING:I = 0x1

.field private static final SNAP_SPEED:F = 4.0f

.field private static final TAG:Ljava/lang/String;

.field private static sCloseButtonSlop:F


# instance fields
.field private mAlpha:F

.field private mBackgroundColor:I

.field private mBorderAlpha:F

.field private mBorderCloseButtonAlpha:F

.field private mBorderScale:F

.field private final mBounds:Landroid/graphics/RectF;

.field private mClippedHeight:F

.field private mClippedWidth:F

.field private mDrawDecoration:Z

.field private mFallbackThumbnailId:I

.field private final mId:I

.field private mInitFromHostStatus:I

.field private final mIsIncognito:Z

.field private mIsTitleNeeded:Z

.field private mOriginalContentHeight:F

.field private mOriginalContentWidth:F

.field private mRenderX:F

.field private mRenderY:F

.field private mRotation:F

.field private mScale:F

.field private final mTabBorder:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

.field private final mTabShadow:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

.field private mTiltX:F

.field private mTiltXPivotOffset:F

.field private mTiltY:F

.field private mTiltYPivotOffset:F

.field private mUseBorderClipMargin:Z

.field private mVisible:Z

.field private mX:F

.field private mY:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(IZLcom/google/android/apps/chrome/utilities/ObjectHandle;Lcom/google/android/apps/chrome/utilities/ObjectHandle;IIZZ)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mDrawDecoration:Z

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsTitleNeeded:Z

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInitFromHostStatus:I

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBackgroundColor:I

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mFallbackThumbnailId:I

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mId:I

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsIncognito:Z

    iput-object p3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTabBorder:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    iput-object p4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTabShadow:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    invoke-virtual {p0, p5, p6, p7, p8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->init(IIZZ)V

    return-void
.end method

.method public static getTouchSlop()F
    .locals 1

    sget v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sCloseButtonSlop:F

    return v0
.end method

.method public static resetDimensionConstants(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sCloseButtonSlop:F

    return-void
.end method

.method private updateSnap(FFF)F
    .locals 4

    sub-float v0, p2, p3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x3f800000

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    :goto_0
    return p3

    :cond_0
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, p3

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    mul-float/2addr v0, p1

    add-float/2addr v0, p2

    float-to-double v1, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-float v1, v1

    float-to-double v2, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-float v2, v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result p3

    goto :goto_0
.end method


# virtual methods
.method public checkCloseHitTest(FFZ)Z
    .locals 1

    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getCloseBounds(Z)Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkHitTest(FFFF)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getClickTargetBounds()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    return v0
.end method

.method public computeDistanceTo(FF)F
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getClickTargetBounds()Landroid/graphics/RectF;

    move-result-object v0

    iget v1, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, p1

    iget v2, v0, Landroid/graphics/RectF;->right:F

    sub-float v2, p1, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v2, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, p2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    sub-float v0, p2, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/4 v2, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public computeVisibleArea()F
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mVisible:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mAlpha:F

    const v1, 0x3b808081

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentHeight()F

    move-result v1

    mul-float/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAlpha()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mAlpha:F

    return v0
.end method

.method public getBackgroundColor()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBackgroundColor:I

    return v0
.end method

.method public getBorderAlpha()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderAlpha:F

    return v0
.end method

.method public getBorderCloseButtonAlpha()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderCloseButtonAlpha:F

    return v0
.end method

.method public getBorderScale()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    return v0
.end method

.method public getBorderThicknessTop()F
    .locals 2

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTabBorder:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/ObjectHandle;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/TabBorder;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getBorderThickness()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->top:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getClickTargetBounds()Landroid/graphics/RectF;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTabBorder:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/ObjectHandle;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/TabBorder;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getBorderThickness()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentHeight()F

    move-result v3

    add-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v3

    add-float/2addr v2, v3

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    iput v0, v1, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getClippedHeight()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedHeight:F

    return v0
.end method

.method public getClippedWidth()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedWidth:F

    return v0
.end method

.method public getCloseBounds(Z)Landroid/graphics/RectF;
    .locals 4

    const/4 v1, 0x0

    const/high16 v3, 0x3f000000

    const/high16 v2, 0x3f800000

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsTitleNeeded:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mVisible:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderCloseButtonAlpha:F

    cmpg-float v0, v0, v3

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderAlpha:F

    cmpg-float v0, v0, v3

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRotation:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltX:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltY:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTabBorder:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/ObjectHandle;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/TabBorder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v2

    invoke-virtual {v0, p1, v2}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getCloseRectangle(ZF)Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_2

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentHeight()F

    move-result v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_2

    iget v2, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    sget v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sCloseButtonSlop:F

    neg-float v1, v1

    sget v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sCloseButtonSlop:F

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    goto :goto_0
.end method

.method public getDrawDecoration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mDrawDecoration:Z

    return v0
.end method

.method public getFallbackThumbnailId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mFallbackThumbnailId:I

    return v0
.end method

.method public getFinalContentHeight()F
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedHeight:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public getFinalContentWidth()F
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedWidth:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mId:I

    return v0
.end method

.method public getOriginalContentHeight()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentHeight:F

    return v0
.end method

.method public getOriginalContentWidth()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentWidth:F

    return v0
.end method

.method public getPaddingLeft()F
    .locals 2

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTabBorder:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/ObjectHandle;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/TabBorder;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getPadding()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->left:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getPaddingTop()F
    .locals 2

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTabBorder:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/ObjectHandle;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/TabBorder;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getPadding()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->top:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getRenderX()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderX:F

    return v0
.end method

.method public getRenderY()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderY:F

    return v0
.end method

.method public getRotation()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRotation:F

    return v0
.end method

.method public getScale()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mScale:F

    return v0
.end method

.method public getScaledContentHeight()F
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentHeight:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mScale:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getScaledContentWidth()F
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentWidth:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mScale:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getTabBorder()Lcom/google/android/apps/chrome/tabs/TabBorder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTabBorder:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/ObjectHandle;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/TabBorder;

    return-object v0
.end method

.method public getTabShadow()Lcom/google/android/apps/chrome/tabs/TabShadow;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTabShadow:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/ObjectHandle;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/TabShadow;

    return-object v0
.end method

.method public getTiltX()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltX:F

    return v0
.end method

.method public getTiltXPivotOffset()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltXPivotOffset:F

    return v0
.end method

.method public getTiltY()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltY:F

    return v0
.end method

.method public getTiltYPivotOffset()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltYPivotOffset:F

    return v0
.end method

.method public getUseBorderClipMargin()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mUseBorderClipMargin:Z

    return v0
.end method

.method public getX()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    return v0
.end method

.method public getY()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    return v0
.end method

.method public init(IIZZ)V
    .locals 4

    const v3, 0x7f7fffff

    const/4 v2, 0x1

    const/high16 v1, 0x3f800000

    const/4 v0, 0x0

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mAlpha:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderAlpha:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderCloseButtonAlpha:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedWidth:F

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedHeight:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRotation:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mScale:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltX:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltY:F

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mVisible:Z

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderX:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderY:F

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mDrawDecoration:Z

    iput-boolean p4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsTitleNeeded:Z

    if-eqz p4, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInitFromHostStatus:I

    if-eq v0, v2, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInitFromHostStatus:I

    :cond_0
    int-to-float v0, p1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentWidth:F

    int-to-float v0, p2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentHeight:F

    return-void
.end method

.method public initFromHost(II)V
    .locals 1

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBackgroundColor:I

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mFallbackThumbnailId:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInitFromHostStatus:I

    return-void
.end method

.method public isIncognito()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsIncognito:Z

    return v0
.end method

.method public isInitFromHostNeeded()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInitFromHostStatus:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTitleNeeded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsTitleNeeded:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mVisible:Z

    return v0
.end method

.method public markAsWaitingForInitFromHost()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInitFromHostStatus:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInitFromHostStatus:I

    return-void
.end method

.method public setAlpha(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mAlpha:F

    return-void
.end method

.method public setBorderAlpha(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderAlpha:F

    return-void
.end method

.method public setBorderCloseButtonAlpha(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderCloseButtonAlpha:F

    return-void
.end method

.method public setBorderScale(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    return-void
.end method

.method public setClipSize(FF)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedWidth:F

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedHeight:F

    return-void
.end method

.method public setContentSize(FF)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentWidth:F

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentHeight:F

    return-void
.end method

.method public setDrawDecoration(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mDrawDecoration:Z

    return-void
.end method

.method public setOriginalContentHeight(I)V
    .locals 1

    int-to-float v0, p1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentHeight:F

    return-void
.end method

.method public setOriginalContentWidth(I)V
    .locals 1

    int-to-float v0, p1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentWidth:F

    return-void
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;F)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$1;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$LayoutTab$Property:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setAlpha(F)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setRotation(F)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    goto :goto_0

    :pswitch_5
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltXPivotOffset:F

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltX(FF)V

    goto :goto_0

    :pswitch_6
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltYPivotOffset:F

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;F)V

    return-void
.end method

.method public setRotation(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRotation:F

    return-void
.end method

.method public setScale(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mScale:F

    return-void
.end method

.method public setTiltX(FF)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltX:F

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltXPivotOffset:F

    return-void
.end method

.method public setTiltY(FF)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltY:F

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltYPivotOffset:F

    return-void
.end method

.method public setUseBorderClipMargin(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mUseBorderClipMargin:Z

    return-void
.end method

.method public setVisible(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mVisible:Z

    return-void
.end method

.method public setX(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    return-void
.end method

.method public setY(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateSnap(J)Z
    .locals 4

    long-to-float v0, p1

    const/high16 v1, 0x40800000

    mul-float/2addr v0, v1

    const/high16 v1, 0x447a0000

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderX:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(FFF)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderY:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(FFF)F

    move-result v2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderX:F

    cmpl-float v0, v1, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderY:F

    cmpl-float v0, v2, v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderX:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderY:F

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
