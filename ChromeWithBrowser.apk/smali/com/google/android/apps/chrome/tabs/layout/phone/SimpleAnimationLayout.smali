.class public Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;
.super Lcom/google/android/apps/chrome/tabs/layout/Layout;

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BACKGROUND_COVER_PCTG:F = 0.5f

.field private static final BACKGROUND_COVER_START_ANGLE:F = 45.0f

.field private static final BACKGROUND_COVER_START_PCTG:F = 0.75f

.field private static final BACKGROUND_STEP1_DURATION:J = 0x190L

.field private static final BACKGROUND_STEP2_DURATION:J = 0xc8L

.field private static final BACKGROUND_STEP3_DURATION:J = 0x12cL

.field private static final BACKGROUND_STEP3_START:J = 0x258L

.field protected static final FOREGROUND_ANIMATION_DURATION:I = 0x12c

.field protected static final TAB_CLOSED_ANIMATION_DURATION:I = 0x1f4

.field protected static final TAB_CLOSED_ROTATION_DEGREES:I = 0x2d


# instance fields
.field private mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    return-void
.end method

.method private ensureSourceTabCreated(I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getModelFromTabId(I)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->isIncognito()Z

    move-result v0

    invoke-virtual {p0, p1, v0, v2, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    new-array v1, v3, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v0, v1, v2

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    new-array v0, v3, [Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    goto :goto_0
.end method

.method private getDiscardRange()F
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mWidth:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mHeight:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3f333333

    mul-float/2addr v0, v1

    return v0
.end method

.method private reset()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    return-void
.end method

.method private setDiscardAmount(F)V
    .locals 7

    const/high16 v6, 0x3f800000

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getDiscardRange()F

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardScale(FF)F

    move-result v1

    sub-float v2, v6, v1

    const/high16 v3, 0x40000000

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v3

    mul-float/2addr v3, v2

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentHeight()F

    move-result v4

    mul-float/2addr v2, v4

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mWidth:I

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mHeight:I

    if-ge v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    add-float/2addr v3, p1

    invoke-virtual {v4, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-static {p1, v0, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardSideOffset(FFF)F

    move-result v4

    add-float/2addr v2, v4

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardAlpha(FF)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-static {p1, v0, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardAngle(FFF)F

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setRotation(F)V

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-static {p1, v0, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardSideOffset(FFF)F

    move-result v5

    add-float/2addr v3, v5

    invoke-virtual {v4, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    add-float/2addr v2, p1

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    goto :goto_0
.end method

.method private tabCreatedInBackground(IIZ)V
    .locals 28

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v15

    sget-boolean v3, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v3, v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v4, 0x0

    aget-object v4, v3, v4

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v5, 0x1

    aput-object v15, v3, v5

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v5, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->forceAnimationToFinish()V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mHeight:I

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    const v5, 0x3dccccd0

    mul-float/2addr v3, v5

    const/high16 v5, 0x40000000

    div-float v25, v3, v5

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getDecelerateInterpolator()Landroid/view/animation/DecelerateInterpolator;

    move-result-object v13

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getAccelerateInterpolator()Landroid/view/animation/AccelerateInterpolator;

    move-result-object v26

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getLinearInterpolator()Landroid/view/animation/LinearInterpolator;

    move-result-object v27

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v6, 0x3f800000

    const v7, 0x3f666666

    const-wide/16 v8, 0x190

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v6, 0x0

    const-wide/16 v8, 0x190

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    move-object/from16 v3, p0

    move/from16 v7, v25

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v6, 0x0

    const-wide/16 v8, 0x190

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    move-object/from16 v3, p0

    move/from16 v7, v25

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const v6, 0x3f8e38e4

    const/high16 v7, 0x3f800000

    const-wide/16 v8, 0x190

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000

    const-wide/16 v8, 0x190

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    const v3, 0x3f666666

    invoke-virtual {v15, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    sget-object v16, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/16 v17, 0x0

    const/high16 v18, 0x3f800000

    const-wide/16 v19, 0xc8

    const-wide/16 v21, 0x0

    const/16 v23, 0x0

    move-object/from16 v14, p0

    move-object/from16 v24, v13

    invoke-virtual/range {v14 .. v24}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getOrientation()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_1

    sget-object v16, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mWidth:I

    int-to-float v0, v3

    move/from16 v17, v0

    const-wide/16 v19, 0x190

    const-wide/16 v21, 0x0

    const/16 v23, 0x0

    move-object/from16 v14, p0

    move/from16 v18, v25

    move-object/from16 v24, v13

    invoke-virtual/range {v14 .. v24}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    sget-object v16, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v3, 0x3f400000

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mHeight:I

    int-to-float v5, v5

    mul-float v17, v3, v5

    const/high16 v3, 0x3f000000

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mHeight:I

    int-to-float v5, v5

    mul-float v18, v3, v5

    const-wide/16 v19, 0x190

    const-wide/16 v21, 0x0

    const/16 v23, 0x0

    move-object/from16 v14, p0

    move-object/from16 v24, v13

    invoke-virtual/range {v14 .. v24}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    :goto_0
    sget-object v16, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ROTATION:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v17, 0x42340000

    const/16 v18, 0x0

    const-wide/16 v19, 0x190

    const-wide/16 v21, 0x0

    const/16 v23, 0x0

    move-object/from16 v14, p0

    move-object/from16 v24, v13

    invoke-virtual/range {v14 .. v24}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const v6, 0x3f666666

    const/high16 v7, 0x3f800000

    const-wide/16 v8, 0x12c

    const-wide/16 v10, 0x258

    const/4 v12, 0x1

    move-object/from16 v3, p0

    move-object/from16 v13, v26

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v7, 0x0

    const-wide/16 v8, 0x12c

    const-wide/16 v10, 0x258

    const/4 v12, 0x1

    move-object/from16 v3, p0

    move/from16 v6, v25

    move-object/from16 v13, v26

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v7, 0x0

    const-wide/16 v8, 0x12c

    const-wide/16 v10, 0x258

    const/4 v12, 0x1

    move-object/from16 v3, p0

    move/from16 v6, v25

    move-object/from16 v13, v26

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v6, 0x3f800000

    const v7, 0x3f8e38e4

    const-wide/16 v8, 0x12c

    const-wide/16 v10, 0x258

    const/4 v12, 0x1

    move-object/from16 v3, p0

    move-object/from16 v13, v26

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v6, 0x3f800000

    const/4 v7, 0x0

    const-wide/16 v8, 0x12c

    const-wide/16 v10, 0x258

    const/4 v12, 0x1

    move-object/from16 v3, p0

    move-object/from16 v13, v26

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getOrientation()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    sget-object v16, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v3, 0x3f000000

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mHeight:I

    int-to-float v4, v4

    mul-float v17, v3, v4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mHeight:I

    int-to-float v3, v3

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getPaddingTop()F

    move-result v4

    add-float v18, v3, v4

    const-wide/16 v19, 0x12c

    const-wide/16 v21, 0x258

    const/16 v23, 0x1

    move-object/from16 v14, p0

    move-object/from16 v24, v27

    invoke-virtual/range {v14 .. v24}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->selectModel(Z)V

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->startHiding(I)V

    return-void

    :cond_1
    sget-object v16, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v3, 0x3f400000

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mWidth:I

    int-to-float v5, v5

    mul-float v17, v3, v5

    const/high16 v3, 0x3f000000

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mWidth:I

    int-to-float v5, v5

    mul-float v18, v3, v5

    const-wide/16 v19, 0x190

    const-wide/16 v21, 0x0

    const/16 v23, 0x0

    move-object/from16 v14, p0

    move-object/from16 v24, v13

    invoke-virtual/range {v14 .. v24}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    sget-object v16, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mHeight:I

    neg-int v3, v3

    int-to-float v0, v3

    move/from16 v17, v0

    const-wide/16 v19, 0x190

    const-wide/16 v21, 0x0

    const/16 v23, 0x0

    move-object/from16 v14, p0

    move/from16 v18, v25

    move-object/from16 v24, v13

    invoke-virtual/range {v14 .. v24}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    goto/16 :goto_0

    :cond_2
    sget-object v16, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v3, 0x3f000000

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mWidth:I

    int-to-float v4, v4

    mul-float v17, v3, v4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mWidth:I

    int-to-float v3, v3

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getPaddingLeft()F

    move-result v4

    add-float v18, v3, v4

    const-wide/16 v19, 0x12c

    const-wide/16 v21, 0x258

    const/16 v23, 0x1

    move-object/from16 v14, p0

    move-object/from16 v24, v27

    invoke-virtual/range {v14 .. v24}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    goto :goto_1
.end method

.method private tabCreatedInForeground(IIZ)V
    .locals 11

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v9, 0x0

    invoke-virtual {p0, p1, p3, v9, v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v2, v2

    if-nez v2, :cond_1

    :cond_0
    new-array v2, v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v2, v9

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    :goto_0
    new-array v2, v5, [Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->forceAnimationToFinish()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getDiscardRange()F

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getOrientation()I

    move-result v2

    if-ne v2, v0, :cond_2

    :goto_1
    int-to-float v0, v0

    mul-float v3, v1, v0

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;->DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    const-wide/16 v5, 0x12c

    const-wide/16 v7, 0x0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getAccelerateInterpolator()Landroid/view/animation/AccelerateInterpolator;

    move-result-object v10

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->selectModel(Z)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->startHiding(I)V

    return-void

    :cond_1
    new-array v2, v5, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v3, v3, v9

    aput-object v3, v2, v9

    aput-object v1, v2, v0

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public cleanupInstanceData(J)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->cleanupInstanceData(J)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->reset()V

    return-void
.end method

.method public handlesTabClosing()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public handlesTabCreating()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onUpdateAnimation(JZ)Z
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpdateAnimation(JZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;F)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$1;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$phone$SimpleAnimationLayout$Property:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->setDiscardAmount(F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;F)V

    return-void
.end method

.method public show(JZ)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->show(JZ)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->reset()V

    return-void
.end method

.method public tabClosed(II)V
    .locals 12

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v9, 0x0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabClosed(II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getModelFromTabId(I)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v11

    if-eqz v11, :cond_1

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->isIncognito()Z

    move-result v0

    invoke-virtual {p0, p2, v0, v9, v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setDrawDecoration(Z)V

    new-array v1, v3, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v0, v1, v9

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v0, v1, v2

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    new-array v0, v3, [Ljava/lang/Integer;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->forceAnimationToFinish()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;->DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getDiscardRange()F

    move-result v4

    const-wide/16 v5, 0x1f4

    const-wide/16 v7, 0x0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getAccelerateInterpolator()Landroid/view/animation/AccelerateInterpolator;

    move-result-object v10

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v11, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->isIncognito()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->selectModel(Z)V

    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->startHiding(I)V

    return-void

    :cond_1
    new-array v0, v2, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v9

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_0
.end method

.method public tabClosing(JI)V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->reset()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->forceAnimationToFinish()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getModelFromTabId(I)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->isIncognito()Z

    move-result v0

    invoke-virtual {p0, p3, v0, v2, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    new-array v0, v3, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    new-array v0, v3, [Ljava/lang/Integer;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabClosing(JI)V

    return-void

    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_0
.end method

.method public tabCreated(JIIIZZ)V
    .locals 1

    invoke-super/range {p0 .. p7}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabCreated(JIIIZZ)V

    invoke-direct {p0, p5}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->ensureSourceTabCreated(I)V

    if-eqz p7, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-lez v0, :cond_0

    invoke-direct {p0, p3, p5, p6}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->tabCreatedInBackground(IIZ)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p3, p5, p6}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->tabCreatedInForeground(IIZ)V

    goto :goto_0
.end method

.method public tabCreating(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabCreating(I)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->reset()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->forceAnimationToFinish()V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->ensureSourceTabCreated(I)V

    return-void
.end method

.method protected updateLayout(JJ)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v0, v1

    :goto_1
    if-ltz v2, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v3, v3, v2

    invoke-virtual {v3, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->requestUpdate()V

    goto :goto_0
.end method
