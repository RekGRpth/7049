.class Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;
.super Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;


# direct methods
.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;-><init>(II)V

    return-void
.end method


# virtual methods
.method protected addTiltScrollAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;FII)V
    .locals 9

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TILTY:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v3

    int-to-long v5, p4

    int-to-long v7, p5

    move-object v0, p1

    move-object v1, p2

    move v4, p3

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    return-void
.end method

.method protected createEnterStackAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIF)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 15

    new-instance v1, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    const/4 v2, 0x0

    move/from16 v0, p4

    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v4

    const/4 v2, 0x0

    move v14, v2

    :goto_0
    move-object/from16 v0, p1

    array-length v2, v0

    if-ge v14, v2, :cond_2

    aget-object v2, p1, v14

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->resetOffset()V

    const v3, 0x3f666666

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScale(F)V

    mul-int v3, v14, p3

    int-to-float v3, v3

    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v5

    move/from16 v0, p2

    if-ge v14, v0, :cond_0

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    const-wide/16 v6, 0x12c

    const-wide/16 v8, 0x0

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    :goto_1
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    goto :goto_0

    :cond_0
    move/from16 v0, p2

    if-le v14, v0, :cond_1

    invoke-virtual {v2, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->X_IN_STACK_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:I

    int-to-float v8, v3

    const/4 v9, 0x0

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x0

    move-object v5, v1

    move-object v6, v2

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    goto :goto_1

    :cond_1
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    const-wide/16 v6, 0x12c

    const-wide/16 v8, 0x0

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->X_IN_STACK_INFLUENCE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000

    const-wide/16 v10, 0xe1

    const-wide/16 v12, 0x0

    move-object v5, v1

    move-object v6, v2

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    const/high16 v8, 0x3f800000

    const v9, 0x3f666666

    const-wide/16 v10, 0xe1

    const-wide/16 v12, 0x0

    move-object v5, v1

    move-object v6, v2

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const v8, 0x3f8e38e4

    const/high16 v9, 0x3f800000

    const-wide/16 v10, 0xe1

    const-wide/16 v12, 0x0

    move-object v5, v1

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000

    const-wide/16 v10, 0xe1

    const-wide/16 v12, 0x0

    move-object v5, v1

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method protected createReachTopAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 11

    new-instance v0, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    const/4 v2, 0x0

    const/4 v1, 0x0

    move v9, v1

    move v10, v2

    :goto_0
    array-length v1, p1

    if-ge v9, v1, :cond_0

    aget-object v1, p1, v9

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v1

    cmpl-float v1, v10, v1

    if-gez v1, :cond_0

    aget-object v1, p1, v9

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    aget-object v3, p1, v9

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v3

    invoke-static {v10, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v4

    const-wide/16 v5, 0x190

    const-wide/16 v7, 0x0

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    aget-object v1, p1, v9

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v1

    add-float v2, v10, v1

    add-int/lit8 v1, v9, 0x1

    move v9, v1

    move v10, v2

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method protected createTabFocusedAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIF)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 15

    new-instance v2, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v2}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    const/4 v1, 0x0

    move v13, v1

    :goto_0
    move-object/from16 v0, p1

    array-length v1, v0

    if-ge v13, v1, :cond_2

    aget-object v14, p1, v13

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x190

    const/4 v6, 0x0

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->addTiltScrollAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;FII)V

    sget-object v6, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v7

    const/4 v8, 0x0

    const-wide/16 v9, 0x190

    const-wide/16 v11, 0x0

    move-object v4, v2

    move-object v5, v14

    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    move/from16 v0, p2

    if-ge v13, v0, :cond_0

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v5

    const/4 v1, 0x0

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v3

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:I

    int-to-float v6, v6

    sub-float/2addr v3, v6

    move/from16 v0, p3

    int-to-float v6, v0

    sub-float/2addr v3, v6

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v6

    const-wide/16 v7, 0x190

    const-wide/16 v9, 0x0

    move-object v3, v14

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    :goto_1
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto :goto_0

    :cond_0
    move/from16 v0, p2

    if-le v13, v0, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v1

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:I

    int-to-float v3, v3

    sub-float v1, v3, v1

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:I

    int-to-float v4, v4

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v1

    const/high16 v3, 0x42c80000

    mul-float/2addr v1, v3

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:I

    int-to-float v3, v3

    div-float/2addr v1, v3

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->X_IN_STACK_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackOffset()F

    move-result v5

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackOffset()F

    move-result v3

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:I

    int-to-float v6, v6

    add-float/2addr v6, v3

    const-wide/16 v7, 0x190

    float-to-long v9, v1

    sub-long/2addr v7, v9

    float-to-long v9, v1

    move-object v3, v14

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v14, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setXOutOfStack(F)V

    const/4 v1, 0x0

    invoke-virtual {v14, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYOutOfStack(F)V

    sget-object v6, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v7

    const/4 v1, 0x0

    move/from16 v0, p4

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v8

    const-wide/16 v9, 0x190

    const-wide/16 v11, 0x0

    move-object v4, v2

    move-object v5, v14

    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    sget-object v6, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScale()F

    move-result v7

    const/high16 v8, 0x3f800000

    const-wide/16 v9, 0x190

    const-wide/16 v11, 0x0

    move-object v4, v2

    move-object v5, v14

    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v5, 0x3f800000

    const/high16 v1, 0x3f800000

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScale()F

    move-result v6

    div-float v6, v1, v6

    const-wide/16 v7, 0x190

    const-wide/16 v9, 0x0

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    sget-object v6, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->X_IN_STACK_INFLUENCE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackInfluence()F

    move-result v7

    const/4 v8, 0x0

    const-wide/16 v9, 0x190

    const-wide/16 v11, 0x0

    move-object v4, v2

    move-object v5, v14

    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const-wide/16 v7, 0x12c

    const-wide/16 v9, 0x0

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    goto/16 :goto_1

    :cond_2
    return-object v2
.end method

.method protected createViewMoreAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 11

    new-instance v0, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    add-int/lit8 v1, p2, 0x1

    array-length v2, p1

    if-lt v1, v2, :cond_1

    :cond_0
    return-object v0

    :cond_1
    aget-object v1, p1, p2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v1

    add-int/lit8 v2, p2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v2

    sub-float/2addr v1, v2

    aget-object v2, p1, p2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v2

    const/high16 v3, 0x3f400000

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x43480000

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v10

    add-int/lit8 v1, p2, 0x1

    move v9, v1

    :goto_0
    array-length v1, p1

    if-ge v9, v1, :cond_0

    aget-object v1, p1, v9

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    aget-object v3, p1, v9

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v3

    aget-object v4, p1, v9

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v4

    add-float/2addr v4, v10

    const-wide/16 v5, 0x190

    const-wide/16 v7, 0x0

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto :goto_0
.end method

.method protected getScreenPositionInScrollDirection(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)F
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v0

    return v0
.end method

.method protected getScreenPositionInSideDirection(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)F
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v0

    return v0
.end method

.method protected getScreenSizeInScrollDirection()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:I

    int-to-float v0, v0

    return v0
.end method
