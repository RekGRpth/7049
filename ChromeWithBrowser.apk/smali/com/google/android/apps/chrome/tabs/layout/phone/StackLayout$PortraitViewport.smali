.class Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;
.super Ljava/lang/Object;


# instance fields
.field protected height:F

.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

.field protected width:F


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mWidth:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$400(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->width:F

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mHeight:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$500(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->height:F

    return-void
.end method


# virtual methods
.method getClampedRenderedScrollOffset()F
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$600(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingFromModelChange:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$700(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$800(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F

    move-result v1

    const/high16 v2, -0x40800000

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    :cond_1
    return v0
.end method

.method getHeight()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->height:F

    return v0
.end method

.method getInnerMargin()F
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInnerMarginPercent:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$900(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinMaxInnerMargin:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$1000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->width:F

    const v3, 0x3e2e147b

    mul-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method getStack0Left()F
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getClampedRenderedScrollOffset()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getFullScrollDistance()F
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$1100(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method getStack0ToStack1TranslationX()F
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->width:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getInnerMargin()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method getStack0ToStack1TranslationY()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method getStack0Top()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method getStackIndexAt(FF)I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0Left()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getWidth()F

    move-result v1

    add-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method getWidth()F
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->width:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getInnerMargin()F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method
