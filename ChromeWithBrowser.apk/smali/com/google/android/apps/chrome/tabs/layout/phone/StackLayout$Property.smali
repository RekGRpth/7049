.class public final enum Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

.field public static final enum INNER_MARGIN_PERCENT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

.field public static final enum STACK_SNAP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    const-string v1, "INNER_MARGIN_PERCENT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->INNER_MARGIN_PERCENT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    const-string v1, "STACK_SNAP"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->STACK_SNAP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->INNER_MARGIN_PERCENT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->STACK_SNAP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    return-object v0
.end method
