.class Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)I
    .locals 4

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getVisiblitySortingValue()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getVisiblitySortingValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    check-cast p2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;->compare(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)I

    move-result v0

    return v0
.end method
