.class Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;
.super Ljava/lang/Object;


# static fields
.field private static final BALLISTIC:I = 0x2

.field private static final CUBIC:I = 0x1

.field private static DECELERATION_RATE:F = 0.0f

.field private static final END_TENSION:F = 1.0f

.field private static final GRAVITY:F = 2000.0f

.field private static final INFLEXION:F = 0.35f

.field private static final NB_SAMPLES:I = 0x64

.field private static final P1:F = 0.175f

.field private static final P2:F = 0.35000002f

.field private static final SPLINE:I = 0x0

.field private static final SPLINE_POSITION:[F

.field private static final SPLINE_TIME:[F

.field private static final START_TENSION:F = 0.5f


# instance fields
.field private mCurrVelocity:F

.field private mCurrentPosition:I

.field private mDeceleration:F

.field private mDuration:I

.field private mFinal:I

.field private mFinished:Z

.field private mFlingFriction:F

.field private mOver:I

.field private final mPhysicalCoeff:F

.field private mSplineDistance:I

.field private mSplineDuration:I

.field private mStart:I

.field private mStartTime:J

.field private mState:I

.field private mVelocity:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const v14, 0x3e333333

    const/4 v4, 0x0

    const-wide v12, 0x3ee4f8b588e368f1L

    const/16 v11, 0x64

    const/high16 v1, 0x3f800000

    const-wide v2, 0x3fe8f5c28f5c28f6L

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    const-wide v5, 0x3feccccccccccccdL

    invoke-static {v5, v6}, Ljava/lang/Math;->log(D)D

    move-result-wide v5

    div-double/2addr v2, v5

    double-to-float v0, v2

    sput v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->DECELERATION_RATE:F

    const/16 v0, 0x65

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->SPLINE_POSITION:[F

    const/16 v0, 0x65

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->SPLINE_TIME:[F

    const/4 v0, 0x0

    move v5, v0

    move v2, v4

    :goto_0
    if-ge v5, v11, :cond_4

    int-to-float v0, v5

    const/high16 v3, 0x42c80000

    div-float v6, v0, v3

    move v0, v1

    move v3, v2

    :goto_1
    sub-float v2, v0, v3

    const/high16 v7, 0x40000000

    div-float/2addr v2, v7

    add-float/2addr v2, v3

    const/high16 v7, 0x40400000

    mul-float/2addr v7, v2

    sub-float v8, v1, v2

    mul-float/2addr v7, v8

    sub-float v8, v1, v2

    mul-float/2addr v8, v14

    const v9, 0x3eb33334

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    mul-float/2addr v8, v7

    mul-float v9, v2, v2

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    sub-float v9, v8, v6

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-double v9, v9

    cmpg-double v9, v9, v12

    if-ltz v9, :cond_1

    cmpl-float v7, v8, v6

    if-lez v7, :cond_0

    move v0, v2

    goto :goto_1

    :cond_0
    move v3, v2

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->SPLINE_POSITION:[F

    sub-float v8, v1, v2

    const/high16 v9, 0x3f000000

    mul-float/2addr v8, v9

    add-float/2addr v8, v2

    mul-float/2addr v7, v8

    mul-float v8, v2, v2

    mul-float/2addr v2, v8

    add-float/2addr v2, v7

    aput v2, v0, v5

    move v0, v1

    :goto_2
    sub-float v2, v0, v4

    const/high16 v7, 0x40000000

    div-float/2addr v2, v7

    add-float/2addr v2, v4

    const/high16 v7, 0x40400000

    mul-float/2addr v7, v2

    sub-float v8, v1, v2

    mul-float/2addr v7, v8

    sub-float v8, v1, v2

    const/high16 v9, 0x3f000000

    mul-float/2addr v8, v9

    add-float/2addr v8, v2

    mul-float/2addr v8, v7

    mul-float v9, v2, v2

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    sub-float v9, v8, v6

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-double v9, v9

    cmpg-double v9, v9, v12

    if-ltz v9, :cond_3

    cmpl-float v7, v8, v6

    if-lez v7, :cond_2

    move v0, v2

    goto :goto_2

    :cond_2
    move v4, v2

    goto :goto_2

    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->SPLINE_TIME:[F

    sub-float v6, v1, v2

    mul-float/2addr v6, v14

    const v8, 0x3eb33334

    mul-float/2addr v8, v2

    add-float/2addr v6, v8

    mul-float/2addr v6, v7

    mul-float v7, v2, v2

    mul-float/2addr v2, v7

    add-float/2addr v2, v6

    aput v2, v0, v5

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v2, v3

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->SPLINE_POSITION:[F

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->SPLINE_TIME:[F

    aput v1, v2, v11

    aput v1, v0, v11

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFlingFriction:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mState:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x43200000

    mul-float/2addr v0, v1

    const v1, 0x43c10b3d

    mul-float/2addr v0, v1

    const v1, 0x3f570a3d

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mPhysicalCoeff:F

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrentPosition:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mState:I

    return v0
.end method

.method private adjustDuration(III)V
    .locals 6

    const/high16 v4, 0x42c80000

    sub-int v0, p2, p1

    sub-int v1, p3, p1

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float v1, v4, v0

    float-to-int v1, v1

    const/16 v2, 0x64

    if-ge v1, v2, :cond_0

    int-to-float v2, v1

    div-float/2addr v2, v4

    add-int/lit8 v3, v1, 0x1

    int-to-float v3, v3

    div-float/2addr v3, v4

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->SPLINE_TIME:[F

    aget v4, v4, v1

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->SPLINE_TIME:[F

    add-int/lit8 v1, v1, 0x1

    aget v1, v5, v1

    sub-float/2addr v0, v2

    sub-float v2, v3, v2

    div-float/2addr v0, v2

    sub-float/2addr v1, v4

    mul-float/2addr v0, v1

    add-float/2addr v0, v4

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    :cond_0
    return-void
.end method

.method private fitOnBounceCurve(III)V
    .locals 6

    neg-int v0, p3

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    div-float/2addr v0, v1

    mul-int v1, p3, p3

    int-to-float v1, v1

    const/high16 v2, 0x40000000

    div-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v1, v2

    sub-int v2, p2, p1

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    const-wide/high16 v3, 0x4000000000000000L

    add-float/2addr v1, v2

    float-to-double v1, v1

    mul-double/2addr v1, v3

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-double v3, v3

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    double-to-float v1, v1

    iget-wide v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    const/high16 v4, 0x447a0000

    sub-float v0, v1, v0

    mul-float/2addr v0, v4

    float-to-int v0, v0

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    neg-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    return-void
.end method

.method private static getDeceleration(I)F
    .locals 1

    if-lez p0, :cond_0

    const/high16 v0, -0x3b060000

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x44fa0000

    goto :goto_0
.end method

.method private getSplineDeceleration(I)D
    .locals 3

    const v0, 0x3eb33333

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFlingFriction:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mPhysicalCoeff:F

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private getSplineFlingDistance(I)D
    .locals 8

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->getSplineDeceleration(I)D

    move-result-wide v0

    sget v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->DECELERATION_RATE:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L

    sub-double/2addr v2, v4

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFlingFriction:F

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mPhysicalCoeff:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    sget v6, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->DECELERATION_RATE:F

    float-to-double v6, v6

    div-double v2, v6, v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    return-wide v0
.end method

.method private getSplineFlingDuration(I)I
    .locals 6

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->getSplineDeceleration(I)D

    move-result-wide v0

    sget v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->DECELERATION_RATE:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L

    sub-double/2addr v2, v4

    const-wide v4, 0x408f400000000000L

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    return v0
.end method

.method private onEdgeReached()V
    .locals 4

    const/high16 v3, 0x40000000

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    mul-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v1, v3

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mOver:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    neg-float v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mOver:I

    int-to-float v1, v1

    mul-float/2addr v1, v3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mOver:I

    int-to-float v0, v0

    :cond_0
    float-to-int v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mOver:I

    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mState:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    if-lez v2, :cond_1

    :goto_0
    float-to-int v0, v0

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    const/high16 v0, 0x447a0000

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    neg-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    return-void

    :cond_1
    neg-float v0, v0

    goto :goto_0
.end method

.method private startAfterEdge(IIIIJ)V
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-le p1, p2, :cond_0

    if-ge p1, p3, :cond_0

    const-string v1, "StackScroller"

    const-string v2, "startAfterEdge called from a valid position"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    :goto_0
    return-void

    :cond_0
    if-le p1, p3, :cond_1

    move v4, v0

    :goto_1
    if-eqz v4, :cond_2

    move v2, p3

    :goto_2
    sub-int v3, p1, v2

    mul-int v5, v3, p4

    if-ltz v5, :cond_3

    :goto_3
    if-eqz v0, :cond_4

    invoke-direct {p0, p1, v2, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->startBounceAfterEdge(III)V

    goto :goto_0

    :cond_1
    move v4, v1

    goto :goto_1

    :cond_2
    move v2, p2

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    invoke-direct {p0, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->getSplineFlingDistance(I)D

    move-result-wide v0

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-double v5, v3

    cmpl-double v0, v0, v5

    if-lez v0, :cond_7

    if-eqz v4, :cond_5

    move v3, p2

    :goto_4
    if-eqz v4, :cond_6

    move v4, p1

    :goto_5
    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mOver:I

    move-object v0, p0

    move v1, p1

    move v2, p4

    move-wide v6, p5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->fling(IIIIIJ)V

    goto :goto_0

    :cond_5
    move v3, p1

    goto :goto_4

    :cond_6
    move v4, p3

    goto :goto_5

    :cond_7
    invoke-direct {p0, p1, v2, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->startSpringback(III)V

    goto :goto_0
.end method

.method private startBounceAfterEdge(III)V
    .locals 1

    if-nez p3, :cond_0

    sub-int v0, p1, p2

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->getDeceleration(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->fitOnBounceCurve(III)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->onEdgeReached()V

    return-void

    :cond_0
    move v0, p3

    goto :goto_0
.end method

.method private startSpringback(III)V
    .locals 7

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mState:I

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    sub-int v0, p1, p2

    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->getDeceleration(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    neg-int v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mOver:I

    const-wide v1, 0x408f400000000000L

    const-wide/high16 v3, -0x4000000000000000L

    int-to-double v5, v0

    mul-double/2addr v3, v5

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    float-to-double v5, v0

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    mul-double v0, v1, v3

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    return-void
.end method


# virtual methods
.method continueWhenFinished(J)Z
    .locals 5

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mState:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->update(J)Z

    const/4 v0, 0x1

    :cond_0
    :pswitch_0
    return v0

    :pswitch_1
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mSplineDuration:I

    if-ge v1, v2, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->getDeceleration(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->onEdgeReached()V

    goto :goto_0

    :pswitch_2
    iget-wide v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    int-to-long v3, v3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->startSpringback(III)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method extendDuration(JI)V
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    return-void
.end method

.method finish()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrentPosition:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    return-void
.end method

.method fling(IIIIIJ)V
    .locals 7

    const/4 v1, 0x0

    iput p5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mOver:I

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    int-to-float v0, p2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mSplineDuration:I

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    iput-wide p6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrentPosition:I

    if-gt p1, p4, :cond_0

    if-ge p1, p3, :cond_2

    :cond_0
    move-object v0, p0

    move v1, p1

    move v2, p3

    move v3, p4

    move v4, p2

    move-wide v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->startAfterEdge(IIIIJ)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mState:I

    const-wide/16 v0, 0x0

    if-eqz p2, :cond_3

    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->getSplineFlingDuration(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mSplineDuration:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->getSplineFlingDistance(I)D

    move-result-wide v0

    :cond_3
    int-to-float v2, p2

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mSplineDistance:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mSplineDistance:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    if-ge v0, p3, :cond_4

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    invoke-direct {p0, v0, v1, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->adjustDuration(III)V

    iput p3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    :cond_4
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    if-le v0, p4, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    invoke-direct {p0, v0, v1, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->adjustDuration(III)V

    iput p4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    goto :goto_0
.end method

.method notifyEdgeReached(IIIJ)V
    .locals 7

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mState:I

    if-nez v0, :cond_0

    iput p3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mOver:I

    iput-wide p4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F

    float-to-int v4, v0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p2

    move-wide v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->startAfterEdge(IIIIJ)V

    :cond_0
    return-void
.end method

.method setFinalPosition(I)V
    .locals 1

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    return-void
.end method

.method setFriction(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFlingFriction:F

    return-void
.end method

.method springback(IIIJ)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    iput-wide p4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    if-ge p1, p2, :cond_1

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->startSpringback(III)V

    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    if-nez v2, :cond_2

    :goto_1
    return v0

    :cond_1
    if-le p1, p3, :cond_0

    invoke-direct {p0, p1, p3, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->startSpringback(III)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method startScroll(IIJI)V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    add-int v0, p1, p2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    iput-wide p3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    iput p5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    return-void
.end method

.method update(J)Z
    .locals 8

    const/high16 v7, 0x447a0000

    const/high16 v6, 0x40000000

    const/high16 v5, 0x42c80000

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J

    sub-long v2, p1, v0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-wide/16 v0, 0x0

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mState:I

    packed-switch v4, :pswitch_data_0

    :goto_1
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrentPosition:I

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    long-to-float v0, v2

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mSplineDuration:I

    int-to-float v1, v1

    div-float v2, v0, v1

    mul-float v0, v5, v2

    float-to-int v3, v0

    const/high16 v1, 0x3f800000

    const/4 v0, 0x0

    const/16 v4, 0x64

    if-ge v3, v4, :cond_1

    int-to-float v0, v3

    div-float v1, v0, v5

    add-int/lit8 v0, v3, 0x1

    int-to-float v0, v0

    div-float/2addr v0, v5

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->SPLINE_POSITION:[F

    aget v4, v4, v3

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->SPLINE_POSITION:[F

    add-int/lit8 v3, v3, 0x1

    aget v3, v5, v3

    sub-float/2addr v3, v4

    sub-float/2addr v0, v1

    div-float v0, v3, v0

    sub-float v1, v2, v1

    mul-float/2addr v1, v0

    add-float/2addr v1, v4

    :cond_1
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mSplineDistance:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-double v1, v1

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mSplineDistance:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mSplineDuration:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    mul-float/2addr v0, v7

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F

    move-wide v0, v1

    goto :goto_1

    :pswitch_1
    long-to-float v0, v2

    div-float/2addr v0, v7

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDeceleration:F

    mul-float/2addr v2, v0

    mul-float/2addr v0, v2

    div-float/2addr v0, v6

    add-float/2addr v0, v1

    float-to-double v0, v0

    goto :goto_1

    :pswitch_2
    long-to-float v0, v2

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I

    int-to-float v1, v1

    div-float v2, v0, v1

    mul-float v3, v2, v2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mVelocity:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v4

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mOver:I

    int-to-float v0, v0

    mul-float/2addr v0, v4

    const/high16 v1, 0x40400000

    mul-float/2addr v1, v3

    mul-float v5, v2, v6

    mul-float/2addr v5, v3

    sub-float/2addr v1, v5

    mul-float/2addr v0, v1

    float-to-double v0, v0

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mOver:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const/high16 v5, 0x40c00000

    mul-float/2addr v4, v5

    neg-float v2, v2

    add-float/2addr v2, v3

    mul-float/2addr v2, v4

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method updateScroll(F)V
    .locals 3

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrentPosition:I

    return-void
.end method
