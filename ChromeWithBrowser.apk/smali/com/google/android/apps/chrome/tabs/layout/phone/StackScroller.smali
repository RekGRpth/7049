.class public Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;
.super Ljava/lang/Object;


# static fields
.field private static final DEFAULT_DURATION:I = 0xfa

.field private static final FLING_MODE:I = 0x1

.field private static final SCROLL_MODE:I

.field private static sViscousFluidNormalize:F

.field private static sViscousFluidScale:F


# instance fields
.field private final mFlywheel:Z

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mMode:I

.field private final mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

.field private final mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mFlywheel:Z

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->initContants()V

    return-void
.end method

.method private static initContants()V
    .locals 2

    const/high16 v1, 0x3f800000

    const/high16 v0, 0x41000000

    sput v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->sViscousFluidScale:F

    sput v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->sViscousFluidNormalize:F

    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->viscousFluid(F)F

    move-result v0

    div-float v0, v1, v0

    sput v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->sViscousFluidNormalize:F

    return-void
.end method

.method private static viscousFluid(F)F
    .locals 4

    const/high16 v3, 0x3f800000

    sget v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->sViscousFluidScale:F

    mul-float/2addr v0, p0

    cmpg-float v1, v0, v3

    if-gez v1, :cond_0

    neg-float v1, v0

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    move-result-wide v1

    double-to-float v1, v1

    sub-float v1, v3, v1

    sub-float/2addr v0, v1

    :goto_0
    sget v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->sViscousFluidNormalize:F

    mul-float/2addr v0, v1

    return v0

    :cond_0
    sub-float v0, v3, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    double-to-float v0, v0

    sub-float v0, v3, v0

    const v1, 0x3ebc5ab2

    const v2, 0x3f21d2a7

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public abortAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->finish()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->finish()V

    return-void
.end method

.method public computeScrollOffset(J)Z
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mMode:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$500(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)J

    move-result-wide v0

    sub-long v0, p1, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mDuration:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$600(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v2

    int-to-long v3, v2

    cmp-long v3, v0, v3

    if-gez v3, :cond_3

    long-to-float v0, v0

    int-to-float v1, v2

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    if-nez v1, :cond_2

    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->viscousFluid(F)F

    move-result v0

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->updateScroll(F)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->updateScroll(F)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->abortAnimation()V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->update(J)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->continueWhenFinished(J)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->finish()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->update(J)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->continueWhenFinished(J)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->finish()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public fling(IIIIIIIIIIJ)V
    .locals 8

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mFlywheel:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$200(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$200(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)F

    move-result v1

    int-to-float v2, p3

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    int-to-float v2, p4

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    int-to-float v2, p3

    add-float/2addr v0, v2

    float-to-int p3, v0

    int-to-float v0, p4

    add-float/2addr v0, v1

    float-to-int p4, v0

    move v2, p3

    :goto_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mMode:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    move v1, p1

    move v3, p5

    move v4, p6

    move/from16 v5, p9

    move-wide/from16 v6, p11

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->fling(IIIIIJ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    move v1, p2

    move v2, p4

    move v3, p7

    move/from16 v4, p8

    move/from16 v5, p10

    move-wide/from16 v6, p11

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->fling(IIIIIJ)V

    return-void

    :cond_0
    move v2, p3

    goto :goto_0
.end method

.method public fling(IIIIIIIIJ)V
    .locals 13

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-wide/from16 v11, p9

    invoke-virtual/range {v0 .. v12}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->fling(IIIIIIIIIIJ)V

    return-void
.end method

.method public final forceFinished(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # setter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z
    invoke-static {v1, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$002(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;Z)Z

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$002(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;Z)Z

    return-void
.end method

.method public getCurrVelocity()F
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$200(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$200(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)F

    move-result v1

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$200(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrVelocity:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$200(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method public final getCurrX()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrentPosition:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$100(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v0

    return v0
.end method

.method public final getCurrY()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mCurrentPosition:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$100(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v0

    return v0
.end method

.method public final getFinalX()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$400(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v0

    return v0
.end method

.method public final getFinalY()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$400(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v0

    return v0
.end method

.method public final getStartX()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$300(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v0

    return v0
.end method

.method public final getStartY()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$300(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v0

    return v0
.end method

.method public final isFinished()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOverScrolled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mState:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$700(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mState:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$700(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScrollingInDirection(FF)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$400(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$300(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mFinal:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$400(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStart:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$300(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Ljava/lang/Math;->signum(F)F

    move-result v2

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    cmpl-float v0, v2, v0

    if-nez v0, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result v0

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyHorizontalEdgeReached(IIIJ)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    move v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->notifyEdgeReached(IIIJ)V

    return-void
.end method

.method public notifyVerticalEdgeReached(IIIJ)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    move v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->notifyEdgeReached(IIIJ)V

    return-void
.end method

.method public final setFriction(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->setFriction(F)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->setFriction(F)V

    return-void
.end method

.method setInterpolator(Landroid/view/animation/Interpolator;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public springBack(IIIIIIJ)Z
    .locals 7

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mMode:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    move v1, p1

    move v2, p3

    move v3, p4

    move-wide v4, p7

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->springback(IIIJ)Z

    move-result v6

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    move v1, p2

    move v2, p5

    move v3, p6

    move-wide v4, p7

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->springback(IIIJ)Z

    move-result v0

    if-nez v6, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startScroll(IIIIJ)V
    .locals 8

    const/16 v7, 0xfa

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-wide v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->startScroll(IIIIJI)V

    return-void
.end method

.method public startScroll(IIIIJI)V
    .locals 6

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mMode:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    move v1, p1

    move v2, p3

    move-wide v3, p5

    move v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->startScroll(IIJI)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    move v1, p2

    move v2, p4

    move-wide v3, p5

    move v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->startScroll(IIJI)V

    return-void
.end method

.method public timePassed(J)I
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerX:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$500(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->mScrollerY:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->mStartTime:J
    invoke-static {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;->access$500(Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller$SplineStackScroller;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    sub-long v0, p1, v0

    long-to-int v0, v0

    return v0
.end method
