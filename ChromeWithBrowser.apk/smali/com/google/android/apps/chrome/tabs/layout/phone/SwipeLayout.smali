.class public Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;
.super Lcom/google/android/apps/chrome/tabs/layout/Layout;

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ANIMATION_SPEED_SCREEN:F = 500.0f

.field private static final ENTER_LAYOUT_ANIMATION_DURATION_MS:J = 0x12cL

.field private static final FLING_MAX_CONTRIBUTION:F = 0.5f

.field private static final FLING_TIME_STEP:F = 0.033333335f

.field private static final SIDE_TILT_SCALE:F = 10.0f

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mBorderThickness:F

.field private final mCommitDistanceFromEdge:F

.field private mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mOffset:F

.field private mOffsetStart:F

.field private mOffsetTarget:F

.field private mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private final mSpaceBetweenTabs:F

.field private mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mSpaceBetweenTabs:F

    const v1, 0x7f080016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mCommitDistanceFromEdge:F

    return-void
.end method

.method private init()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetStart:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffset:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetTarget:F

    return-void
.end method

.method private prepareLayoutTabForSwipe(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V
    .locals 3

    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBorderThicknessTop()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mBorderThickness:F

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    return-void
.end method

.method private smoothInput(FF)F
    .locals 2

    const/high16 v1, 0x41f00000

    sub-float v0, p2, v1

    add-float/2addr v1, p2

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    const v1, 0x3f4ccccd

    invoke-static {v0, p2, v1}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public cleanupInstanceData(J)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->cleanupInstanceData(J)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->init()V

    return-void
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout$Property;F)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout$1;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$phone$SwipeLayout$Property:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffset:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetTarget:F

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout$Property;F)V

    return-void
.end method

.method public show(JZ)V
    .locals 4

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->show(JZ)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->init()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCurrentTabId()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->isIncognito()Z

    move-result v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->prepareLayoutTabForSwipe(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    goto :goto_0
.end method

.method public swipeFinished()V
    .locals 9

    const-wide/16 v7, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mCommitDistanceFromEdge:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    div-int/lit8 v1, v1, 0x3

    int-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetTarget:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v0, v0

    add-float/2addr v4, v0

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eq v0, v1, :cond_3

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->tabSideSwipeFinished()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->startHiding(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->forceAnimationToFinish()V

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetTarget:F

    const/high16 v0, 0x43fa0000

    sub-float v1, v3, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-long v5, v0

    cmp-long v0, v5, v7

    if-lez v0, :cond_4

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout$Property;->OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout$Property;

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->requestRender()V

    goto :goto_0

    :cond_5
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetTarget:F

    neg-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v0, v0

    sub-float/2addr v4, v0

    goto :goto_1
.end method

.method public swipeFlingOccurred(FFFF)V
    .locals 5

    const/high16 v2, 0x3f000000

    const v4, 0x3d088889

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v0, v0

    mul-float/2addr v0, v2

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mHeight:I

    int-to-float v1, v1

    mul-float/2addr v1, v2

    mul-float v2, p3, v4

    neg-float v3, v0

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    mul-float v2, p4, v4

    neg-float v3, v1

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v1

    add-float/2addr v0, p1

    add-float/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->swipeUpdated(FF)V

    return-void
.end method

.method public swipeStarted(Z)V
    .locals 8

    const/4 v7, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->forceAnimationToFinish()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCurrentTabIndex()I

    move-result v3

    const/4 v0, -0x1

    if-eq v3, v0, :cond_0

    if-eqz p1, :cond_4

    add-int/lit8 v0, v3, -0x1

    :goto_1
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-ltz v4, :cond_2

    invoke-virtual {v2, v4}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabId(I)I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->isIncognito()Z

    move-result v4

    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->prepareLayoutTabForSwipe(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_3

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabId(I)I

    move-result v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->isIncognito()Z

    move-result v2

    invoke-virtual {p0, v0, v2, v5, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->prepareLayoutTabForSwipe(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    :cond_3
    iput-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz p1, :cond_5

    move v0, v1

    :goto_2
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetStart:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffset:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetTarget:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->requestUpdate()V

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v3, 0x1

    goto :goto_1

    :cond_5
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v0, v0

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_7

    new-array v0, v6, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_8

    new-array v0, v6, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_3

    :cond_8
    iput-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_3
.end method

.method public swipeUpdated(FF)V
    .locals 3

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetStart:F

    add-float/2addr v0, p1

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v2, v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetStart:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetTarget:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->requestUpdate()V

    return-void
.end method

.method protected updateLayout(JJ)V
    .locals 14

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    xor-int v4, v2, v3

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffset:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetTarget:F

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->smoothInput(FF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffset:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffset:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetTarget:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v3, 0x3dcccccd

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_7

    const/4 v5, 0x1

    :goto_3
    if-eqz v4, :cond_8

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffset:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/4 v3, 0x0

    const/high16 v6, 0x3f000000

    invoke-static {v2, v3, v6}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v2

    :goto_4
    const/high16 v3, 0x40000000

    mul-float/2addr v3, v2

    const/high16 v6, 0x3f800000

    sub-float/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    mul-float v6, v3, v3

    mul-float v7, v6, v6

    neg-float v6, v6

    const/high16 v8, 0x40000000

    mul-float/2addr v3, v8

    const/high16 v8, 0x40400000

    sub-float/2addr v3, v8

    mul-float/2addr v6, v3

    mul-float v3, v6, v6

    mul-float/2addr v3, v3

    mul-float/2addr v3, v3

    const/high16 v8, 0x3f800000

    const/4 v9, 0x0

    invoke-static {v8, v9, v3}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v8

    const/high16 v3, 0x3f800000

    const/high16 v9, 0x40000000

    iget v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mSpaceBetweenTabs:F

    const/high16 v11, 0x40000000

    iget v12, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mBorderThickness:F

    mul-float/2addr v11, v12

    sub-float/2addr v10, v11

    mul-float/2addr v9, v10

    iget v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mHeight:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    sub-float/2addr v3, v9

    const/high16 v9, 0x3f800000

    invoke-static {v3, v9, v7}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v9

    const/high16 v10, 0x3f800000

    const/high16 v11, 0x3f800000

    div-float v3, v11, v3

    invoke-static {v10, v3, v7}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v7

    const/4 v3, 0x0

    if-eqz v4, :cond_a

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffset:F

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    const/high16 v3, 0x41200000

    const/4 v4, 0x0

    invoke-static {v3, v4, v6}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v3

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x3f800000

    sub-float/2addr v4, v9

    mul-float/2addr v3, v4

    const/high16 v4, 0x40000000

    div-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v4, v4

    const/high16 v10, 0x3f800000

    sub-float/2addr v10, v9

    mul-float/2addr v4, v10

    invoke-static {v4, v3, v6}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v4

    const/4 v10, 0x0

    invoke-static {v10, v3, v6}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v3

    :goto_5
    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mHeight:I

    int-to-float v6, v6

    const/high16 v10, 0x3f800000

    sub-float/2addr v10, v9

    mul-float/2addr v6, v10

    const/high16 v10, 0x40000000

    div-float/2addr v6, v10

    iget-object v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v10, :cond_10

    iget-object v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v10, v9}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    iget-object v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v10, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    iget-object v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v10, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    iget-object v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v10, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    iget-object v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v10, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const v10, 0x3eaaaaab

    iget-object v11, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v11

    mul-float/2addr v10, v11

    invoke-virtual {v3, v2, v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v3

    if-nez v3, :cond_3

    if-eqz v5, :cond_d

    :cond_3
    const/4 v3, 0x1

    :goto_6
    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v5, :cond_f

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v5, v9}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v5, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v5, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const v5, 0x3f2aaaab

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v6

    mul-float/2addr v5, v6

    invoke-virtual {v4, v2, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-wide/from16 v0, p3

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v2

    if-nez v2, :cond_4

    if-eqz v3, :cond_e

    :cond_4
    const/4 v2, 0x1

    :goto_7
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->requestUpdate()V

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v5, 0x0

    goto/16 :goto_3

    :cond_8
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffset:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v3, v3

    div-float v3, v2, v3

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mOffsetStart:F

    const/4 v6, 0x0

    cmpl-float v2, v2, v6

    if-nez v2, :cond_9

    const/4 v2, 0x0

    :goto_8
    add-float/2addr v2, v3

    const/4 v3, 0x0

    const/high16 v6, 0x3f800000

    invoke-static {v2, v3, v6}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v2

    goto/16 :goto_4

    :cond_9
    const/high16 v2, 0x3f800000

    goto :goto_8

    :cond_a
    sget-boolean v4, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->$assertionsDisabled:Z

    if-nez v4, :cond_b

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v4, :cond_b

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_b
    sget-boolean v4, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->$assertionsDisabled:Z

    if-nez v4, :cond_c

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v4, :cond_c

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_c
    const/4 v4, 0x0

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v6, v6

    iget v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mSpaceBetweenTabs:F

    add-float/2addr v6, v10

    invoke-static {v4, v6, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v2

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mSpaceBetweenTabs:F

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    int-to-float v6, v6

    iget-object v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v10

    invoke-static {v6, v10}, Ljava/lang/Math;->min(FF)F

    move-result v6

    add-float/2addr v4, v6

    mul-float/2addr v4, v9

    sub-float v6, v2, v4

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mWidth:I

    div-int/lit8 v10, v4, 0x2

    int-to-float v4, v10

    iget-object v11, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v11

    const/high16 v12, 0x40000000

    div-float/2addr v11, v12

    sub-float/2addr v4, v11

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    int-to-float v2, v10

    iget-object v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v10

    const/high16 v11, 0x40000000

    div-float/2addr v10, v11

    sub-float/2addr v2, v10

    invoke-static {v2, v6}, Ljava/lang/Math;->min(FF)F

    move-result v2

    move v13, v3

    move v3, v2

    move v2, v13

    goto/16 :goto_5

    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_6

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_f
    move v2, v3

    goto/16 :goto_7

    :cond_10
    move v3, v5

    goto/16 :goto_6
.end method
