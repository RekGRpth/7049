.class public Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;
.super Lcom/google/android/apps/chrome/tabs/layout/Layout;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ALLOW_WRAPPING:Z = false

.field private static final FADE_IN_DURATION_MS:I = 0x64

.field private static final MOVE_WITH_DRAG_DIRECTION:Z


# instance fields
.field private mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mCurrentSideScrollingTabIndex:I

.field private mDragFromLeftEdge:Z

.field private mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mStartIndex:I

.field private final mTabSwitchDistance:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mStartIndex:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08009f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mTabSwitchDistance:F

    return-void
.end method

.method private broadcastTabPreSelection(I)V
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0x15

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private createAndStartSwitchAnimation(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V
    .locals 9

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->forceAnimationToFinish()V

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000

    const-wide/16 v5, 0x64

    const-wide/16 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    return-void
.end method

.method private updateCachedTabRequests()V
    .locals 9

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCount()I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    div-int/lit8 v1, v4, 0x2

    sub-int v6, v0, v1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    div-int/lit8 v1, v4, 0x2

    add-int/2addr v1, v0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    :goto_0
    if-gt v0, v1, :cond_1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mStartIndex:I

    if-eq v0, v2, :cond_0

    rem-int v2, v0, v4

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabId(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    add-int/lit8 v1, v0, -0x1

    :goto_1
    if-lt v1, v6, :cond_5

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, v1

    if-eqz v2, :cond_2

    add-int/lit8 v0, v0, -0x1

    :cond_2
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    if-ltz v1, :cond_3

    move v0, v1

    :goto_2
    iget v8, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mStartIndex:I

    if-ne v0, v8, :cond_4

    const/4 v0, 0x1

    :goto_3
    add-int/lit8 v1, v1, -0x1

    move v2, v0

    goto :goto_1

    :cond_3
    add-int v0, v1, v4

    goto :goto_2

    :cond_4
    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabId(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v7, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move v0, v2

    goto :goto_3

    :cond_5
    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    return-void
.end method

.method private updateCurrentlyShownTab(I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v3, v1}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(III)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    if-eq v2, v1, :cond_0

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabId(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->broadcastTabPreSelection(I)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->updateCachedTabRequests()V

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->releaseTabLayout(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->isIncognito()Z

    move-result v0

    invoke-virtual {p0, v1, v0, v3, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setDrawDecoration(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    :goto_1
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-ne v0, v1, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->createAndStartSwitchAnimation(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->requestRender()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_7

    new-array v0, v4, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_8

    new-array v0, v4, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_2

    :cond_8
    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_2
.end method


# virtual methods
.method public cleanupInstanceData(J)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->cleanupInstanceData(J)V

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mStartIndex:I

    return-void
.end method

.method public show(JZ)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->show(JZ)V

    return-void
.end method

.method public swipeFinished()V
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getTabId(I)I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentTabId()I

    move-result v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->startHiding(I)V

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->requestRender()V

    return-void
.end method

.method public swipeFlingOccurred(FFFF)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mStartIndex:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mCurrentSideScrollingTabIndex:I

    cmpg-float v1, p3, v2

    if-gez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mDragFromLeftEdge:Z

    if-nez v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    cmpl-float v1, p3, v2

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mDragFromLeftEdge:Z

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, -0x1

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->updateCurrentlyShownTab(I)V

    :cond_2
    return-void
.end method

.method public swipeStarted(Z)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mDragFromLeftEdge:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCurrentTabIndex()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mStartIndex:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mStartIndex:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->updateCurrentlyShownTab(I)V

    return-void
.end method

.method public swipeUpdated(FF)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mStartIndex:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mTabSwitchDistance:F

    div-float v1, p1, v1

    float-to-int v1, v1

    mul-int/lit8 v1, v1, -0x1

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->updateCurrentlyShownTab(I)V

    goto :goto_0
.end method

.method protected updateLayout(JJ)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mBackgroundTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->mShowingTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v3, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/SideSwitchLayout;->requestUpdate()V

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_0
.end method
