.class Lcom/google/android/apps/chrome/tests/Fling$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tests/Fling;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tests/Fling;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/tests/Fling$2;->this$0:Lcom/google/android/apps/chrome/tests/Fling;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Fling$2;->this$0:Lcom/google/android/apps/chrome/tests/Fling;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Fling;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v4, p0, Lcom/google/android/apps/chrome/tests/Fling$2;->this$0:Lcom/google/android/apps/chrome/tests/Fling;

    # getter for: Lcom/google/android/apps/chrome/tests/Fling;->mSpeed:I
    invoke-static {v4}, Lcom/google/android/apps/chrome/tests/Fling;->access$100(Lcom/google/android/apps/chrome/tests/Fling;)I

    move-result v4

    neg-int v6, v4

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v6}, Lorg/chromium/content/browser/ContentView;->fling(JIIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Fling$2;->this$0:Lcom/google/android/apps/chrome/tests/Fling;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Fling;->mContentView:Lorg/chromium/content/browser/ContentView;

    new-instance v1, Lcom/google/android/apps/chrome/tests/Fling$2$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tests/Fling$2$1;-><init>(Lcom/google/android/apps/chrome/tests/Fling$2;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Lorg/chromium/content/browser/ContentView;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Fling$2;->this$0:Lcom/google/android/apps/chrome/tests/Fling;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Fling;->mContentView:Lorg/chromium/content/browser/ContentView;

    new-instance v1, Lcom/google/android/apps/chrome/tests/Fling$2$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tests/Fling$2$2;-><init>(Lcom/google/android/apps/chrome/tests/Fling$2;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lorg/chromium/content/browser/ContentView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
