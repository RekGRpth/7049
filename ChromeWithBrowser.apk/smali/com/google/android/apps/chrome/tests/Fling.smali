.class public Lcom/google/android/apps/chrome/tests/Fling;
.super Lcom/google/android/apps/chrome/tests/ChromeTestActivity;


# instance fields
.field private mFling:Ljava/lang/Runnable;

.field private mSpeed:I

.field private mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tests/ChromeTestActivity;-><init>()V

    const/16 v0, 0xbb8

    iput v0, p0, Lcom/google/android/apps/chrome/tests/Fling;->mSpeed:I

    new-instance v0, Lcom/google/android/apps/chrome/tests/Fling$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tests/Fling$2;-><init>(Lcom/google/android/apps/chrome/tests/Fling;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tests/Fling;->mFling:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tests/Fling;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Fling;->mFling:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/tests/Fling;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tests/Fling;->mSpeed:I

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tests/ChromeTestActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/apps/chrome/tests/Fling$1;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tests/Fling;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/tests/Fling$1;-><init>(Lcom/google/android/apps/chrome/tests/Fling;Lorg/chromium/content/browser/ContentViewCore;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tests/Fling;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    const-string v0, "http://www.howtocreate.co.uk/browserSpeed.html"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tests/Fling;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tests/Fling;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "speed"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "speed"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tests/Fling;->mSpeed:I

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tests/Fling;->mContentView:Lorg/chromium/content/browser/ContentView;

    new-instance v2, Lorg/chromium/content/browser/LoadUrlParams;

    invoke-direct {v2, v0}, Lorg/chromium/content/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lorg/chromium/content/browser/ContentView;->loadUrl(Lorg/chromium/content/browser/LoadUrlParams;)V

    return-void
.end method
