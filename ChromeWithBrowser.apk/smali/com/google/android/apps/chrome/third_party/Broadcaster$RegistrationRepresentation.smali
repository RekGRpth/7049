.class public Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;
.super Ljava/lang/Object;


# instance fields
.field public senderWhat:I

.field public targetWhats:[I

.field public targets:[Landroid/os/Handler;

.field final synthetic this$0:Lcom/google/android/apps/chrome/third_party/Broadcaster;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/third_party/Broadcaster;Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;->this$0:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v0, p2, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;->senderWhat:I

    iget-object v0, p2, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    iget-object v1, p2, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;->targets:[Landroid/os/Handler;

    iget-object v0, p2, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    iget-object v1, p2, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;->targetWhats:[I

    return-void
.end method
