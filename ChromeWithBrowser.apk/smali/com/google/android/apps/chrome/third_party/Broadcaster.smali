.class public Lcom/google/android/apps/chrome/third_party/Broadcaster;
.super Ljava/lang/Object;


# instance fields
.field private mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public broadcast(Landroid/os/Message;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->broadcast(Landroid/os/Message;Z)V

    return-void
.end method

.method public broadcast(Landroid/os/Message;Z)V
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-nez v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget v2, p1, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    move-object v0, v1

    :cond_1
    iget v3, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-ge v3, v2, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-ne v0, v1, :cond_1

    :cond_2
    iget v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-ne v1, v2, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    iget-object v2, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    array-length v3, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_4

    aget-object v4, v1, v0

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    aget v6, v2, v0

    iput v6, v5, Landroid/os/Message;->what:I

    if-eqz p2, :cond_3

    invoke-virtual {v4}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v7

    if-ne v6, v7, :cond_3

    invoke-virtual {v4, v5}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public cancelRequest(ILandroid/os/Handler;I)V
    .locals 8

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-nez v2, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    move-object v1, v2

    :cond_1
    iget v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-ge v3, p1, :cond_2

    iget-object v1, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-ne v1, v2, :cond_1

    :cond_2
    iget v2, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-ne v2, p1, :cond_4

    iget-object v2, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    iget-object v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    array-length v4, v2

    :goto_1
    if-ge v0, v4, :cond_4

    aget-object v5, v2, v0

    if-ne v5, p2, :cond_5

    aget v5, v3, v0

    if-ne v5, p3, :cond_5

    add-int/lit8 v5, v4, -0x1

    new-array v5, v5, [Landroid/os/Handler;

    iput-object v5, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    add-int/lit8 v5, v4, -0x1

    new-array v5, v5, [I

    iput-object v5, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    if-lez v0, :cond_3

    const/4 v5, 0x0

    iget-object v6, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    const/4 v7, 0x0

    invoke-static {v2, v5, v6, v7, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v5, 0x0

    iget-object v6, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    const/4 v7, 0x0

    invoke-static {v3, v5, v6, v7, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    sub-int/2addr v4, v0

    add-int/lit8 v4, v4, -0x1

    if-eqz v4, :cond_4

    add-int/lit8 v5, v0, 0x1

    iget-object v6, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    invoke-static {v2, v5, v6, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v0, 0x1

    iget-object v1, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    invoke-static {v3, v2, v1, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public dumpRegistrations()V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->getRegistrationRepresentations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Broadcaster "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " { No registrations }"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Broadcaster "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " {"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    senderWhat="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;->senderWhat:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;->targets:[Landroid/os/Handler;

    array-length v3, v1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_1

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "        ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;->targetWhats:[I

    aget v6, v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;->targets:[Landroid/os/Handler;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getRegistrationRepresentations()Ljava/util/List;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-nez v2, :cond_0

    invoke-static {}, Lcom/google/a/b/B;->a()Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit p0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v2

    :cond_1
    new-instance v3, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;-><init>(Lcom/google/android/apps/chrome/third_party/Broadcaster;Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-ne v1, v2, :cond_1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public request(ILandroid/os/Handler;I)V
    .locals 8

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;-><init>(Lcom/google/android/apps/chrome/third_party/Broadcaster;Lcom/google/android/apps/chrome/third_party/Broadcaster$1;)V

    iput p1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/os/Handler;

    iput-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    const/4 v1, 0x1

    new-array v1, v1, [I

    iput-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    iget-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    iget-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    const/4 v2, 0x0

    aput p3, v1, v2

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    iput-object v0, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    iput-object v0, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->prev:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    :goto_0
    monitor-exit p0

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    move-object v1, v0

    :cond_1
    iget v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-ge v3, p1, :cond_2

    iget-object v1, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-ne v1, v0, :cond_1

    :cond_2
    iget v0, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-eq v0, p1, :cond_4

    new-instance v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;-><init>(Lcom/google/android/apps/chrome/third_party/Broadcaster;Lcom/google/android/apps/chrome/third_party/Broadcaster$1;)V

    iput p1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/os/Handler;

    iput-object v3, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    const/4 v3, 0x1

    new-array v3, v3, [I

    iput-object v3, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    iput-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    iget-object v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->prev:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    iput-object v3, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->prev:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    iget-object v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->prev:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    iput-object v0, v3, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    iput-object v0, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->prev:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    iget-object v3, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-ne v1, v3, :cond_3

    iget v1, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    iget v3, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-le v1, v3, :cond_3

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    :cond_3
    move v1, v2

    :goto_2
    iget-object v2, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    aput-object p2, v2, v1

    iget-object v0, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    aput p3, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_1
    iget-object v0, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    array-length v0, v0

    iget-object v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    iget-object v4, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    :goto_3
    if-ge v2, v0, :cond_6

    aget-object v5, v3, v2

    if-ne v5, p2, :cond_5

    aget v5, v4, v2

    if-ne v5, p3, :cond_5

    monitor-exit p0

    goto :goto_1

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    add-int/lit8 v2, v0, 0x1

    new-array v2, v2, [Landroid/os/Handler;

    iput-object v2, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    const/4 v2, 0x0

    iget-object v5, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-static {v3, v2, v5, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v0, 0x1

    new-array v2, v2, [I

    iput-object v2, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    const/4 v2, 0x0

    iget-object v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    const/4 v5, 0x0

    invoke-static {v4, v2, v3, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_2
.end method
