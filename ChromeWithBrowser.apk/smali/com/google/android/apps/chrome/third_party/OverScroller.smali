.class public Lcom/google/android/apps/chrome/third_party/OverScroller;
.super Ljava/lang/Object;


# static fields
.field private static final DEFAULT_DURATION:I = 0xfa

.field private static final FLING_MODE:I = 0x1

.field private static final SCROLL_MODE:I

.field private static sViscousFluidNormalize:F

.field private static sViscousFluidScale:F


# instance fields
.field private final mFlywheel:Z

.field private final mInterpolator:Landroid/view/animation/Interpolator;

.field private mMode:I

.field private final mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

.field private final mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/high16 v0, 0x41000000

    const/high16 v1, 0x3f800000

    sput v0, Lcom/google/android/apps/chrome/third_party/OverScroller;->sViscousFluidScale:F

    invoke-static {v1, v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;->viscousFluid(FFF)F

    move-result v0

    div-float v0, v1, v0

    sput v0, Lcom/google/android/apps/chrome/third_party/OverScroller;->sViscousFluidNormalize:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;FF)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;FFZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/apps/chrome/third_party/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    iput-boolean p3, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mFlywheel:Z

    new-instance v0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    new-instance v0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-static {p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->initFromContext(Landroid/content/Context;)V

    return-void
.end method

.method private getInterpolatedTime(JI)F
    .locals 2

    long-to-float v0, p1

    int-to-float v1, p3

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->viscousFluid(F)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_0
.end method

.method private isFinishedAtTime(J)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->isFinishedAtTime(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->isFinishedAtTime(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static viscousFluid(F)F
    .locals 2

    sget v0, Lcom/google/android/apps/chrome/third_party/OverScroller;->sViscousFluidScale:F

    sget v1, Lcom/google/android/apps/chrome/third_party/OverScroller;->sViscousFluidNormalize:F

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;->viscousFluid(FFF)F

    move-result v0

    return v0
.end method

.method private static viscousFluid(FFF)F
    .locals 4

    const/high16 v3, 0x3f800000

    mul-float v0, p0, p1

    cmpg-float v1, v0, v3

    if-gez v1, :cond_0

    neg-float v1, v0

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    move-result-wide v1

    double-to-float v1, v1

    sub-float v1, v3, v1

    sub-float/2addr v0, v1

    :goto_0
    mul-float/2addr v0, p2

    return v0

    :cond_0
    sub-float v0, v3, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    double-to-float v0, v0

    sub-float v0, v3, v0

    const v1, 0x3ebc5ab2

    const v2, 0x3f21d2a7

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public abortAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->finish()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->finish()V

    return-void
.end method

.method public computeScrollOffset()Z
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mMode:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J
    invoke-static {v2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$600(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$500(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v2

    int-to-long v3, v2

    cmp-long v3, v0, v3

    if-gez v3, :cond_2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/third_party/OverScroller;->getInterpolatedTime(JI)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->updateScroll(F)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->updateScroll(F)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->abortAnimation()V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$000(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->update()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->continueWhenFinished()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->finish()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$000(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->update()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->continueWhenFinished()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->finish()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public extendDuration(I)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->extendDuration(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->extendDuration(I)V

    return-void
.end method

.method public fling(IIIIIIII)V
    .locals 11

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/apps/chrome/third_party/OverScroller;->fling(IIIIIIIIII)V

    return-void
.end method

.method public fling(IIIIIIIIII)V
    .locals 6

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mFlywheel:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$200(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$200(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)F

    move-result v1

    int-to-float v2, p3

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    int-to-float v2, p4

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    int-to-float v2, p3

    add-float/2addr v0, v2

    float-to-int p3, v0

    int-to-float v0, p4

    add-float/2addr v0, v1

    float-to-int p4, v0

    move v2, p3

    :goto_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mMode:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    move v1, p1

    move v3, p5

    move v4, p6

    move v5, p9

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->fling(IIIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    move v1, p2

    move v2, p4

    move v3, p7

    move v4, p8

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->fling(IIIII)V

    return-void

    :cond_0
    move v2, p3

    goto :goto_0
.end method

.method public final forceFinished(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # setter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v1, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$002(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;Z)Z

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$002(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;Z)Z

    return-void
.end method

.method public getCurrVelocity()F
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$200(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$200(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)F

    move-result v1

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$200(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$200(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method public final getCurrX()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrentPosition:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$100(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    return v0
.end method

.method public final getCurrY()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrentPosition:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$100(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    return v0
.end method

.method public final getDuration()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$500(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$500(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final getFinalX()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$400(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    return v0
.end method

.method public final getFinalY()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$400(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    return v0
.end method

.method public getScrollOffsetAtTime(JLandroid/graphics/Point;)Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$600(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)J

    move-result-wide v0

    sub-long v0, p1, v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinishedAtTime(J)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mMode:I

    packed-switch v2, :pswitch_data_0

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$500(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/third_party/OverScroller;->getInterpolatedTime(JI)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->computeInterpolatedPosition(F)I

    move-result v1

    iput v1, p3, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->computeInterpolatedPosition(F)I

    move-result v0

    :goto_2
    iput v0, p3, Landroid/graphics/Point;->y:I

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # invokes: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->computeStateAtTime(J)Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;
    invoke-static {v2, v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$800(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;J)Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # invokes: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->computeStateAtTime(J)Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;
    invoke-static {v3, v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$800(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;J)Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;

    move-result-object v1

    if-eqz v2, :cond_1

    iget v0, v2, Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;->position:I

    :goto_3
    iput v0, p3, Landroid/graphics/Point;->x:I

    if-eqz v1, :cond_2

    iget v0, v1, Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;->position:I

    goto :goto_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->getFinalX()I

    move-result v0

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->getFinalY()I

    move-result v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getStartX()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$300(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    return v0
.end method

.method public final getStartY()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$300(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    return v0
.end method

.method public final isFinished()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$000(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$000(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOverScrolled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$000(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mState:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$700(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$000(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mState:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$700(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScrollingInDirection(FF)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$400(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$300(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$400(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$300(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Ljava/lang/Math;->signum(F)F

    move-result v2

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    cmpl-float v0, v2, v0

    if-nez v0, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result v0

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyHorizontalEdgeReached(III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->notifyEdgeReached(III)V

    return-void
.end method

.method public notifyVerticalEdgeReached(III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->notifyEdgeReached(III)V

    return-void
.end method

.method public setFinalX(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->setFinalPosition(I)V

    return-void
.end method

.method public setFinalY(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->setFinalPosition(I)V

    return-void
.end method

.method public final setFriction(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->setFriction(F)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->setFriction(F)V

    return-void
.end method

.method public springBack(IIIIII)Z
    .locals 3

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mMode:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v1, p1, p3, p4}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->springback(III)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v2, p2, p5, p6}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->springback(III)Z

    move-result v2

    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startScroll(IIII)V
    .locals 6

    const/16 v5, 0xfa

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/third_party/OverScroller;->startScroll(IIIII)V

    return-void
.end method

.method public startScroll(IIIII)V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mMode:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1, p3, p5}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->startScroll(III)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p2, p4, p5}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->startScroll(III)V

    return-void
.end method

.method public timePassed()I
    .locals 6

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J
    invoke-static {v2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$600(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J
    invoke-static {v4}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$600(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
