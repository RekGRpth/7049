.class public Lcom/google/android/apps/chrome/uma/UmaSessionStats;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/net/NetworkChangeNotifier$ConnectionTypeObserver;


# static fields
.field private static final NOTIFICATIONS:[I

.field private static final TAG:Ljava/lang/String;

.field private static mNativeUmaSessionStats:I


# instance fields
.field private mComponentCallbacks:Landroid/content/ComponentCallbacks;

.field private final mContext:Landroid/content/Context;

.field private mKeyboardConnected:Z

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mReportingPermissionManager:Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;

.field private mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const-class v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->TAG:Ljava/lang/String;

    sput v2, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mNativeUmaSessionStats:I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/16 v1, 0x9

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->NOTIFICATIONS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mKeyboardConnected:Z

    iput-object p1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats$NotificationHandler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats$NotificationHandler;-><init>(Lcom/google/android/apps/chrome/uma/UmaSessionStats;Lcom/google/android/apps/chrome/uma/UmaSessionStats$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    new-instance v0, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mReportingPermissionManager:Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;

    invoke-static {}, Lorg/chromium/net/NetworkChangeNotifier;->getInstance()Lorg/chromium/net/NetworkChangeNotifier;

    invoke-static {p0}, Lorg/chromium/net/NetworkChangeNotifier;->addConnectionTypeObserver(Lorg/chromium/net/NetworkChangeNotifier$ConnectionTypeObserver;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/uma/UmaSessionStats;)Lcom/google/android/apps/chrome/TabModelSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/uma/UmaSessionStats;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->recordPageLoadStats(I)V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/uma/UmaSessionStats;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mKeyboardConnected:Z

    return p1
.end method

.method private getTabCountFromModel(Lcom/google/android/apps/chrome/TabModel;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public static logRendererCrash()V
    .locals 1

    invoke-static {}, Lorg/chromium/base/ActivityStatus;->isPaused()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->nativeLogRendererCrash(Z)V

    return-void
.end method

.method private native nativeInit()I
.end method

.method private static native nativeLogRendererCrash(Z)V
.end method

.method private native nativeUmaEndSession(I)V
.end method

.method private native nativeUmaResumeSession(I)V
.end method

.method private native nativeUpdateMetricsServiceState(ZZ)V
.end method

.method private recordPageLoadStats(I)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/TabModelSelector;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v2, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->getTabCountFromModel(Lcom/google/android/apps/chrome/TabModel;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->getTabCountFromModel(Lcom/google/android/apps/chrome/TabModel;)I

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mKeyboardConnected:Z

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/chrome/UmaRecordAction;->pageLoaded(IIZZ)V

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getUseDesktopUserAgent()Z

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public logAndEndSession()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mComponentCallbacks:Landroid/content/ComponentCallbacks;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    sget v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mNativeUmaSessionStats:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->nativeUmaEndSession(I)V

    return-void
.end method

.method public onConnectionTypeChanged(I)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->updateMetricsServiceState()V

    return-void
.end method

.method public startNewSession(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 3

    const/4 v0, 0x1

    sget v1, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mNativeUmaSessionStats:I

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->nativeInit()I

    move-result v1

    sput v1, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mNativeUmaSessionStats:I

    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/uma/UmaSessionStats$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats$1;-><init>(Lcom/google/android/apps/chrome/uma/UmaSessionStats;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mComponentCallbacks:Landroid/content/ComponentCallbacks;

    iget-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mComponentCallbacks:Landroid/content/ComponentCallbacks;

    invoke-virtual {v1, v2}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->keyboard:I

    if-eq v1, v0, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mKeyboardConnected:Z

    iput-object p1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    sget-object v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    sget v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mNativeUmaSessionStats:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->nativeUmaResumeSession(I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateMetricsServiceState()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isNeverUploadCrashDump()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mReportingPermissionManager:Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;->isUploadPermitted()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->nativeUpdateMetricsServiceState(ZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
