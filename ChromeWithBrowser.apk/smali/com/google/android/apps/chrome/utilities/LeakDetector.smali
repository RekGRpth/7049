.class public Lcom/google/android/apps/chrome/utilities/LeakDetector;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private final mCollectedQueue:Ljava/lang/ref/ReferenceQueue;

.field private final mObjects:Ljava/util/HashMap;

.field private final mToRemove:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/utilities/LeakDetector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/utilities/LeakDetector;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mObjects:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mCollectedQueue:Ljava/lang/ref/ReferenceQueue;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mToRemove:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public addObject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/utilities/LeakDetector;->addObject(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;)V

    return-void
.end method

.method public addObject(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;)V
    .locals 8

    iget-object v6, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mObjects:Ljava/util/HashMap;

    new-instance v7, Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mCollectedQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v7, p1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    new-instance v0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const/4 v5, 0x0

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;-><init>(Lcom/google/android/apps/chrome/utilities/LeakDetector;Ljava/lang/Class;Ljava/lang/Object;Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;Lcom/google/android/apps/chrome/utilities/LeakDetector$1;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/utilities/LeakDetector;->lookForLeaks()V

    return-void
.end method

.method public lookForLeaks()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mToRemove:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mCollectedQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mObjects:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mObjects:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Leak detected: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/chrome/utilities/LeakDetector;->TAG:Ljava/lang/String;

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/google/android/apps/chrome/utilities/LeakDetector;->TAG:Ljava/lang/String;

    const-string v4, "Originally allocated here:"

    # getter for: Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mAllocationStack:Ljava/lang/Throwable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->access$100(Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;)Ljava/lang/Throwable;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    # getter for: Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mCleanupTask:Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;
    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->access$200(Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;)Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;

    move-result-object v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/apps/chrome/utilities/LeakDetector;->TAG:Ljava/lang/String;

    const-string v4, "running cleanup task"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mCleanupTask:Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;
    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->access$200(Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;)Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;

    move-result-object v3

    # getter for: Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mDescription:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->access$300(Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;->cleanup(Ljava/lang/Object;)V

    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mToRemove:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mToRemove:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mObjects:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mToRemove:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mToRemove:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public removeObject(Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mObjects:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector;->mObjects:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/utilities/LeakDetector;->lookForLeaks()V

    return-void
.end method
