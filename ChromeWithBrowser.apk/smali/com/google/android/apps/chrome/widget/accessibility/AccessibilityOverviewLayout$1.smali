.class Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$1;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$1;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    # invokes: Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->setStateBasedOnModel()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->access$200(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$1;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->overviewVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$1;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->hideOverview(Z)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0xc -> :sswitch_0
    .end sparse-switch
.end method
