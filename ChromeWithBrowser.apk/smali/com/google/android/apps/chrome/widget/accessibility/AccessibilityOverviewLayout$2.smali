.class Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$2;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public tabClosed(Lcom/google/android/apps/chrome/Tab;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$2;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    # getter for: Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->access$300(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;)Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/TabModel;->closeTab(Lcom/google/android/apps/chrome/Tab;)Z

    return-void
.end method

.method public tabSelected(Lcom/google/android/apps/chrome/Tab;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$2;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    # getter for: Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->access$300(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;)Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->getTabIndexById(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->setIndex(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$2;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->overviewVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$2;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->hideOverview(Z)V

    :cond_0
    return-void
.end method
