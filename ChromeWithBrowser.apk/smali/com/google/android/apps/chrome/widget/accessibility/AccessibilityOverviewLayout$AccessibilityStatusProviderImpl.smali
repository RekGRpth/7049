.class Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;-><init>()V

    return-void
.end method

.method private initAccessibilityManager(Landroid/content/Context;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public isAccessibilityModeEnabled(Landroid/content/Context;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->initAccessibilityManager(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAccessibilityStateChangeListener(Landroid/content/Context;Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)V
    .locals 2

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->initAccessibilityManager(Landroid/content/Context;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    if-nez v0, :cond_2

    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->removeAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    :cond_3
    if-eqz p2, :cond_1

    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;->mListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    goto :goto_0
.end method
