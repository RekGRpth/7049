.class public Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;
.implements Lcom/google/android/apps/chrome/OverviewBehavior;


# static fields
.field private static final INCOGNITO_TAB_INDICATOR:Ljava/lang/String; = "incognito"

.field private static final NORMAL_TAB_INDICATOR:Ljava/lang/String; = "normal"

.field private static final NOTIFICATIONS:[I

.field private static sAccessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

.field private static sAccessibilityStatusProvider:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;


# instance fields
.field private mAccessibilityContainer:Landroid/widget/TabHost;

.field private final mContainer:Landroid/view/ViewGroup;

.field private mIncognitoAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

.field private final mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

.field private mNormalAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProviderImpl;-><init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$1;)V

    sput-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStatusProvider:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xc
        0x2
    .end array-data
.end method

.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$1;-><init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    new-instance v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$2;-><init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->setStateBasedOnModel()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;)Lcom/google/android/apps/chrome/TabModelSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-object v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStateChangeListenerImpl;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStateChangeListenerImpl;-><init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$1;)V

    sput-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStatusProvider:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;

    sget-object v1, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;->setAccessibilityStateChangeListener(Landroid/content/Context;Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)V

    :cond_0
    return-void
.end method

.method public static onResume(Landroid/content/Context;)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    sget-object v1, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStatusProvider:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;

    invoke-interface {v1, p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;->onAccessibilityStateChanged(Z)V

    :cond_0
    return-void
.end method

.method public static setAccessibilityStatusProvider(Landroid/content/Context;Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStatusProvider:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;->setAccessibilityStateChangeListener(Landroid/content/Context;Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)V

    sput-object p1, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStatusProvider:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;->setAccessibilityStateChangeListener(Landroid/content/Context;Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)V

    return-void
.end method

.method private setStateBasedOnModel()V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v3}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v4}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    if-eqz v3, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public static shouldUseAccessibilityMode(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->init(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->sAccessibilityStatusProvider:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStatusProvider;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public clearOverviewMode()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mContainer:Landroid/view/ViewGroup;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    :cond_0
    return-void
.end method

.method public getAccessibilityContainer()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    return-object v0
.end method

.method public hideOverview(Z)V
    .locals 2

    const/16 v0, 0xf

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setVisibility(I)V

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-eqz v0, :cond_0

    const-string v0, "incognito"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/TabModelSelector;->selectModel(Z)V

    :cond_0
    return-void
.end method

.method public overviewVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerForNotifications()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method public setChromeViewHolder(Lcom/google/android/apps/chrome/ContentViewHolder;)V
    .locals 0

    return-void
.end method

.method public setEnableAnimations(Z)V
    .locals 0

    return-void
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mNormalAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mNormalAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->setTabModel(Lcom/google/android/apps/chrome/TabModel;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mIncognitoAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mIncognitoAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->setTabModel(Lcom/google/android/apps/chrome/TabModel;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->setStateBasedOnModel()V

    return-void
.end method

.method public setTiltControlsEnabled(Z)V
    .locals 0

    return-void
.end method

.method public showOverview(Z)V
    .locals 7

    const v6, 0x7f0f0001

    const/high16 v5, 0x7f0f0000

    const/4 v4, 0x0

    invoke-static {v4}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/high16 v1, 0x7f040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->setup()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f07023d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    const-string v2, "normal"

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    invoke-virtual {v1, v0}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f07023f

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    const-string v3, "incognito"

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    invoke-virtual {v2, v0}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v0, v2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v0, v5}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mNormalAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v0, v6}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mIncognitoAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mNormalAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v1, v4}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->setTabModel(Lcom/google/android/apps/chrome/TabModel;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mIncognitoAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->setTabModel(Lcom/google/android/apps/chrome/TabModel;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mNormalAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->setListener(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mIncognitoAccessibilityView:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->setListener(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->setStateBasedOnModel()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mAccessibilityContainer:Landroid/widget/TabHost;

    invoke-virtual {v0, v4}, Landroid/widget/TabHost;->setVisibility(I)V

    const/16 v0, 0xd

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    return-void
.end method

.method public unregisterForNotifications()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method
