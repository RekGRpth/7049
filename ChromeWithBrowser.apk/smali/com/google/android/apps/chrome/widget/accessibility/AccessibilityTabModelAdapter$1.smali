.class Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter$1;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public tabClosed(Lcom/google/android/apps/chrome/Tab;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter$1;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    # getter for: Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->access$000(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;)Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter$1;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    # getter for: Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->access$000(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;)Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;->tabClosed(Lcom/google/android/apps/chrome/Tab;)V

    :cond_0
    return-void
.end method

.method public tabSelected(Lcom/google/android/apps/chrome/Tab;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter$1;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    # getter for: Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->access$000(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;)Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter$1;->this$0:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    # getter for: Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->access$000(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;)Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;->tabSelected(Lcom/google/android/apps/chrome/Tab;)V

    :cond_0
    return-void
.end method
