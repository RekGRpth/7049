.class public Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

.field private mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mTab:Lcom/google/android/apps/chrome/Tab;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0xa

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->NOTIFICATIONS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$1;-><init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lcom/google/android/apps/chrome/Tab;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->updateTabTitle()V

    return-void
.end method

.method private updateTabTitle()V
    .locals 6

    const v5, 0x7f0701f8

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lcom/google/android/apps/chrome/Tab;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getTitle()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lcom/google/android/b/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070142

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {v1, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {v1, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    if-eqz v0, :cond_0

    if-ne p1, p0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;->tabSelected(Lcom/google/android/apps/chrome/Tab;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;->tabClosed(Lcom/google/android/apps/chrome/Tab;)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    const/4 v1, 0x1

    const v0, 0x7f0f0002

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->setClickable(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->setFocusable(Z)V

    const v0, 0x7f0f0003

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setListner(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    return-void
.end method

.method public setTabData(Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->updateTabTitle()V

    return-void
.end method
