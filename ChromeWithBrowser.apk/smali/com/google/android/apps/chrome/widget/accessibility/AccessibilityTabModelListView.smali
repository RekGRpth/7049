.class public Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;
.super Landroid/widget/ListView;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->mAdapter:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->mAdapter:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->registerForNotifications()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->mAdapter:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->unregisterForNotifications()V

    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/ListView;->onFinishInflate()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->mAdapter:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setTabModel(Lcom/google/android/apps/chrome/TabModel;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListView;->mAdapter:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->setTabModel(Lcom/google/android/apps/chrome/TabModel;)V

    return-void
.end method
