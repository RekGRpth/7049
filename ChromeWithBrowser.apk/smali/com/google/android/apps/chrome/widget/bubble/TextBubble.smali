.class public Lcom/google/android/apps/chrome/widget/bubble/TextBubble;
.super Landroid/widget/PopupWindow;

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Landroid/view/View$OnLayoutChangeListener;


# static fields
.field public static final ANIM_DURATION_MS:Ljava/lang/String; = "Animation_Duration"

.field public static final BACKGROUND_ID:Ljava/lang/String; = "Background_Id"

.field public static final CENTER:Ljava/lang/String; = "Center"

.field public static final LAYOUT_WIDTH_ID:Ljava/lang/String; = "Layout_Width_Id"

.field public static final TEXT_STYLE_ID:Ljava/lang/String; = "Text_Style_Id"

.field public static final TIP_ID:Ljava/lang/String; = "Tip_Id"

.field public static final TOOLTIP_MARGIN_ID:Ljava/lang/String; = "Tooltip_Margin"

.field public static final UP_DOWN:Ljava/lang/String; = "Up_Down"


# instance fields
.field private mAnchorBelow:Z

.field private mAnchorView:Landroid/view/View;

.field private mAnimator:Landroid/animation/AnimatorSet;

.field private final mBubbleTipXMargin:I

.field private mCenterView:Z

.field private final mTooltipEdgeMargin:I

.field private mTooltipText:Landroid/widget/TextView;

.field private final mTooltipTopMargin:I

.field private mXPosition:I

.field private mYOffset:I

.field private mYPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, -0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/widget/PopupWindow;-><init>()V

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorBelow:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mCenterView:Z

    const-string v0, "Up_Down"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Up_Down"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorBelow:Z

    const-string v0, "Center"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Center"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mCenterView:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080076

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipEdgeMargin:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v0, "Tooltip_Margin"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "Tooltip_Margin"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipTopMargin:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08008a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mBubbleTipXMargin:I

    new-instance v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/high16 v0, 0x1030000

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->setAnimationStyle(I)V

    const-string v0, "Animation_Duration"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v0, 0x7f050000

    invoke-static {p1, v0}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Landroid/animation/AnimatorSet;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnimator:Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p0}, Landroid/animation/AnimatorSet;->setTarget(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnimator:Landroid/animation/AnimatorSet;

    const-string v1, "Animation_Duration"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    :cond_1
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    const-string v0, "Text_Style_Id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Text_Style_Id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_2
    invoke-virtual {v1, p1, v0}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    const-string v0, "Layout_Width_Id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "Layout_Width_Id"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setWidth(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080086

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v0, v3, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0, v4, v4}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->setWindowLayoutMode(II)V

    return-void

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    const v0, 0x7f080077

    goto/16 :goto_1

    :cond_5
    const v0, 0x7f0c0005

    goto :goto_2
.end method

.method private showAtCalculatedPosition()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorBelow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x33

    iget v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    iget v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->showAtLocation(Landroid/view/View;III)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYOffset:I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x53

    iget v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    iget v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0
.end method

.method private updatePosition()Z
    .locals 4

    iget v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    iget v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->getBubbleArrowOffset()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->calculateNewPosition()V

    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    if-ne v1, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    if-ne v2, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->getBubbleArrowOffset()I

    move-result v0

    if-eq v3, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addShadowLayerToText(FFFI)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    return-void
.end method

.method protected calculateNewPosition()V
    .locals 8

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorBelow:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    move-object v3, v1

    move v1, v4

    :goto_0
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v3, v0

    move v0, v1

    move v1, v2

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_1
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mCenterView:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v3, v2, 0x2

    sub-int/2addr v0, v3

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget v5, v3, Landroid/graphics/Rect;->left:I

    iget v6, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int v3, v0, v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    add-int v5, v3, v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v6

    if-le v5, v6, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipEdgeMargin:I

    sub-int/2addr v0, v2

    move v2, v0

    :goto_2
    sub-int v0, v2, v3

    neg-int v0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    iget v6, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mBubbleTipXMargin:I

    sub-int/2addr v5, v6

    if-le v3, v5, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iget v5, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mBubbleTipXMargin:I

    sub-int/2addr v3, v5

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    float-to-int v0, v0

    mul-int/2addr v0, v3

    move v3, v0

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->setBubbleArrowXOffset(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorBelow:Z

    if-eqz v0, :cond_3

    iput v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipTopMargin:I

    sub-int v0, v1, v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    iput v4, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYOffset:I

    :goto_4
    return-void

    :cond_2
    if-gez v3, :cond_5

    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipEdgeMargin:I

    move v2, v0

    goto :goto_2

    :cond_3
    iput v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipTopMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    iput v4, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYOffset:I

    goto :goto_4

    :cond_4
    move v3, v0

    goto :goto_3

    :cond_5
    move v2, v3

    goto :goto_2

    :cond_6
    move v7, v0

    move v0, v1

    move v1, v7

    goto/16 :goto_1

    :cond_7
    move v0, v4

    move-object v3, v1

    move v1, v4

    goto/16 :goto_0
.end method

.method public dismiss()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->stopAnimation()V

    invoke-super {p0}, Landroid/widget/PopupWindow;->dismiss()V

    return-void
.end method

.method public getAnchorView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    return-object v0
.end method

.method public getOffsetY()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYOffset:I

    return v0
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->shouldDisappearOnLayoutChange()Z

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->updatePosition()Z

    move-result v1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->dismiss()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->dismiss()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->showAtCalculatedPosition()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->startAnimation()V

    goto :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->dismiss()V

    return-void
.end method

.method public setOffsetY(I)V
    .locals 3

    const/4 v2, -0x1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    iget v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    add-int/2addr v1, p1

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->update(IIII)V

    iput p1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYOffset:I

    return-void
.end method

.method protected shouldDisappearOnLayoutChange()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getAnchorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showTextBubble(Ljava/lang/String;Landroid/view/View;II)V
    .locals 3

    const/high16 v2, -0x80000000

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-static {p3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {p4, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->calculateNewPosition()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->showAtCalculatedPosition()V

    return-void
.end method

.method public startAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :cond_0
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    :cond_0
    return-void
.end method
