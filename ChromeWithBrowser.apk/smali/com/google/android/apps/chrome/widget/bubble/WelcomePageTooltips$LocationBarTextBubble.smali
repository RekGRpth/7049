.class Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$LocationBarTextBubble;
.super Lcom/google/android/apps/chrome/widget/bubble/TextBubble;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$LocationBarTextBubble;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method protected calculateNewPosition()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->calculateNewPosition()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$LocationBarTextBubble;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->setBubbleArrowXOffset(I)V

    return-void
.end method

.method protected shouldDisappearOnLayoutChange()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$LocationBarTextBubble;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # getter for: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$200(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Lcom/google/android/apps/chrome/Main;

    move-result-object v0

    const v1, 0x7f0f006f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
