.class Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field bottom:F

.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

.field top:F


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;FF)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->top:F

    iput p3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->bottom:F

    return-void
.end method


# virtual methods
.method centerY()F
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->top:F

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->bottom:F

    add-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    mul-float/2addr v0, v1

    return v0
.end method

.method public compareTo(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;)I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->centerY()F

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->centerY()F

    move-result v1

    cmpl-float v2, v0, v1

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->compareTo(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;)I

    move-result v0

    return v0
.end method

.method height()F
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->bottom:F

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->top:F

    sub-float/2addr v0, v1

    return v0
.end method

.method toRectF()Landroid/graphics/RectF;
    .locals 6

    const/high16 v5, 0x3f000000

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarDrawWidth:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->access$000(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;)I

    move-result v1

    sub-int/2addr v0, v1

    new-instance v1, Landroid/graphics/RectF;

    int-to-float v0, v0

    const/high16 v2, 0x40200000

    add-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->top:F

    add-float/2addr v2, v5

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3fc00000

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->bottom:F

    sub-float/2addr v4, v5

    invoke-direct {v1, v0, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v1
.end method
