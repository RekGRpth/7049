.class Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;
.super Landroid/view/View;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static mComparator:Ljava/util/Comparator;


# instance fields
.field private final mActiveBorderColor:I

.field private final mActiveColor:I

.field private mActiveMatch:Landroid/graphics/RectF;

.field private final mActiveMinHeight:I

.field private final mBackgroundBorderColor:I

.field private final mBackgroundColor:I

.field private final mBarDrawWidth:I

.field private mBarHeightForWhichTickmarksWereCached:I

.field private final mBarTouchWidth:I

.field private final mBarVerticalPadding:I

.field private mFillPaint:Landroid/graphics/Paint;

.field private mMatches:[Landroid/graphics/RectF;

.field private final mMinGapBetweenStacks:I

.field mRectsVersion:I

.field private final mResultBorderColor:I

.field private final mResultColor:I

.field private final mResultMinHeight:I

.field private final mStackedResultHeight:I

.field private mStrokePaint:Landroid/graphics/Paint;

.field private mTab:Lcom/google/android/apps/chrome/Tab;

.field private mTickmarks:Ljava/util/ArrayList;

.field mWaitingForActivateAck:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->$assertionsDisabled:Z

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$1;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mComparator:Ljava/util/Comparator;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/Tab;)V
    .locals 5

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, -0x1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mRectsVersion:I

    new-array v0, v1, [Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    iput v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarHeightForWhichTickmarksWereCached:I

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mWaitingForActivateAck:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBackgroundColor:I

    const v1, 0x7f0a000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBackgroundBorderColor:I

    const v1, 0x7f0a0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultColor:I

    const v1, 0x7f0a0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultBorderColor:I

    const v1, 0x7f0a0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveColor:I

    const v1, 0x7f0a0013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveBorderColor:I

    const v1, 0x7f080055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarTouchWidth:I

    const v1, 0x7f080056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f08005d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarDrawWidth:I

    const v1, 0x7f080057

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultMinHeight:I

    const v1, 0x7f080058

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMinHeight:I

    const v1, 0x7f080059

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarVerticalPadding:I

    const v1, 0x7f08005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMinGapBetweenStacks:I

    const v1, 0x7f08005b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStackedResultHeight:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTab:Lcom/google/android/apps/chrome/Tab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarTouchWidth:I

    const/4 v3, 0x5

    invoke-direct {v1, v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, p0, v1}, Lorg/chromium/content/browser/ContentView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarDrawWidth:I

    return v0
.end method

.method private calculateTickmarks()V
    .locals 14

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarHeightForWhichTickmarksWereCached:I

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    aget-object v0, v0, v3

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->tickmarkForRect(Landroid/graphics/RectF;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    move-result-object v1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMinGapBetweenStacks:I

    neg-int v0, v0

    int-to-float v0, v0

    move v2, v3

    move-object v13, v1

    move v1, v0

    move-object v0, v13

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v4, v4

    if-ge v2, v4, :cond_6

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v2, 0x1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v2, v2

    if-ge v4, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    aget-object v0, v0, v4

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->tickmarkForRect(Landroid/graphics/RectF;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    move-result-object v2

    iget v5, v2, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->top:F

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    iget v0, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->bottom:F

    iget v6, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMinGapBetweenStacks:I

    int-to-float v6, v6

    add-float/2addr v0, v6

    cmpg-float v0, v5, v0

    if-gtz v0, :cond_1

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    move-object v0, v2

    goto :goto_1

    :cond_0
    move-object v2, v0

    :cond_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMinGapBetweenStacks:I

    int-to-float v0, v0

    add-float v5, v1, v0

    add-int/lit8 v0, v9, -0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    iget v1, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->bottom:F

    add-int/lit8 v0, v9, -0x1

    iget v6, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStackedResultHeight:I

    mul-int/2addr v0, v6

    int-to-float v0, v0

    sub-float v0, v1, v0

    iget v6, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultMinHeight:I

    int-to-float v6, v6

    sub-float v6, v0, v6

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    iget v0, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->top:F

    invoke-static {v6, v5, v0}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v10, v0

    cmpl-float v0, v10, v6

    if-ltz v0, :cond_3

    const/high16 v0, 0x3f800000

    move v7, v0

    :goto_2
    const/4 v0, 0x1

    if-ne v9, v0, :cond_4

    const/4 v0, 0x0

    move v5, v0

    :goto_3
    move v6, v3

    :goto_4
    if-ge v6, v9, :cond_5

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    int-to-float v11, v6

    mul-float/2addr v11, v5

    add-float/2addr v11, v10

    iput v11, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->top:F

    add-int/lit8 v11, v9, -0x1

    if-eq v6, v11, :cond_2

    iget v11, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->top:F

    iget v12, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultMinHeight:I

    int-to-float v12, v12

    mul-float/2addr v12, v7

    add-float/2addr v11, v12

    iput v11, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->bottom:F

    :cond_2
    iget-object v11, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_4

    :cond_3
    sub-float v0, v1, v10

    sub-float v5, v1, v6

    div-float/2addr v0, v5

    move v7, v0

    goto :goto_2

    :cond_4
    sub-float v0, v1, v10

    iget v5, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultMinHeight:I

    int-to-float v5, v5

    mul-float/2addr v5, v7

    sub-float/2addr v0, v5

    add-int/lit8 v5, v9, -0x1

    int-to-float v5, v5

    div-float/2addr v0, v5

    move v5, v0

    goto :goto_3

    :cond_5
    move-object v0, v2

    move v2, v4

    goto/16 :goto_0

    :cond_6
    return-void
.end method

.method private expandTickmarkToMinHeight(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;
    .locals 5

    const/high16 v4, 0x40000000

    if-eqz p2, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMinHeight:I

    :goto_0
    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->height()F

    move-result v1

    sub-float v1, v0, v1

    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    iget v2, p1, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->top:F

    div-float v3, v1, v4

    sub-float/2addr v2, v3

    iget v3, p1, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->bottom:F

    div-float/2addr v1, v4

    add-float/2addr v1, v3

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;FF)V

    move-object p1, v0

    :cond_0
    return-object p1

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultMinHeight:I

    goto :goto_0
.end method

.method private tickmarkForRect(Landroid/graphics/RectF;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;
    .locals 4

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarHeightForWhichTickmarksWereCached:I

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarVerticalPadding:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    iget v2, p1, Landroid/graphics/RectF;->top:F

    mul-float/2addr v2, v0

    iget v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarVerticalPadding:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v0, v3

    iget v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarVerticalPadding:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;FF)V

    invoke-direct {p0, v1, p2}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->expandTickmarkToMinHeight(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clearMatchRects()V
    .locals 3

    const/4 v0, -0x1

    const/4 v1, 0x0

    new-array v1, v1, [Landroid/graphics/RectF;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->setMatchRects(I[Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    return-void
.end method

.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/content/browser/ContentView;->removeView(Landroid/view/View;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    const/4 v9, 0x1

    const/high16 v8, 0x3f000000

    const/4 v2, 0x0

    const/high16 v7, 0x40000000

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarDrawWidth:I

    sub-int v6, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBackgroundBorderColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v1, v6

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarDrawWidth:I

    add-int/2addr v0, v6

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    int-to-float v0, v6

    add-float v1, v0, v8

    int-to-float v0, v6

    add-float v3, v0, v8

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarHeightForWhichTickmarksWereCached:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->calculateTickmarks()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultBorderColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->toRectF()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v7, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v7, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMatch:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMatch:Landroid/graphics/RectF;

    sget-object v2, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mComparator:Ljava/util/Comparator;

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    if-ltz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    invoke-direct {p0, v0, v9}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->expandTickmarkToMinHeight(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->toRectF()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveBorderColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v7, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v7, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMatch:Landroid/graphics/RectF;

    invoke-direct {p0, v0, v9}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->tickmarkForRect(Landroid/graphics/RectF;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    move-result-object v0

    goto :goto_2
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTab:Lcom/google/android/apps/chrome/Tab;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mRectsVersion:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->requestFindMatchRects(I)V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v3, v3

    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mWaitingForActivateAck:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/AndroidUtils;->hideKeyboard(Landroid/view/View;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    new-instance v3, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;FF)V

    invoke-static {v0, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_4

    rsub-int/lit8 v3, v0, -0x1

    if-nez v3, :cond_1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mWaitingForActivateAck:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTab:Lcom/google/android/apps/chrome/Tab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    aget-object v2, v4, v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/chrome/Tab;->activateNearestFindResult(FF)V

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v3, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v0

    move v0, v1

    :goto_1
    sub-int/2addr v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->centerY()F

    move-result v0

    sub-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->centerY()F

    move-result v0

    sub-float v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v4, v0

    if-gtz v0, :cond_3

    move v0, v1

    move v2, v3

    goto :goto_1

    :cond_3
    move v0, v2

    move v2, v3

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_0
.end method

.method public setMatchRects(I[Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mRectsVersion:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mRectsVersion:I

    sget-boolean v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    sget-object v1, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mComparator:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarHeightForWhichTickmarksWereCached:I

    :cond_1
    iput-object p3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMatch:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->invalidate()V

    return-void
.end method
