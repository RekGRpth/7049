.class Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setPrevNextEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$300(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # setter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$502(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/apps/chrome/Tab;->startFinding(Ljava/lang/String;ZZ)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mLastUserSearch:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$702(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # invokes: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->clearResults()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$600(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/Tab;->stopFinding(I)V

    goto :goto_1
.end method
