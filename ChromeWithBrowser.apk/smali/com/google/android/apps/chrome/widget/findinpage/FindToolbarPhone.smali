.class public Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;
.super Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public activate(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->isViewAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->setVisibility(I)V

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->activate(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deactivate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->deactivate()V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->setVisibility(I)V

    return-void
.end method

.method protected updateVisualsForTabModel(Z)V
    .locals 2

    if-eqz p1, :cond_0

    const v0, 0x7f0200be

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mFindNextButton:Landroid/widget/ImageButton;

    const v1, 0x7f020054

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mFindPrevButton:Landroid/widget/ImageButton;

    const v1, 0x7f02005a

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mCloseFindButton:Landroid/widget/ImageButton;

    const v1, 0x7f020048

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f0200bc

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mFindNextButton:Landroid/widget/ImageButton;

    const v1, 0x7f020051

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mFindPrevButton:Landroid/widget/ImageButton;

    const v1, 0x7f020057

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mCloseFindButton:Landroid/widget/ImageButton;

    const v1, 0x7f020047

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method
