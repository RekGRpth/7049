.class public Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;
.super Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private final mCallback:Landroid/view/ActionMode$Callback;

.field private mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

.field private final mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2d
        0x2c
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/TabModelSelector;Landroid/view/ActionMode$Callback;Landroid/app/Activity;II)V
    .locals 0

    invoke-direct {p0, p3, p4, p5}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;-><init>(Landroid/app/Activity;II)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mCallback:Landroid/view/ActionMode$Callback;

    return-void
.end method


# virtual methods
.method protected getNotificationsToRegister()[I
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->NOTIFICATIONS:[I

    return-object v0
.end method

.method protected handleNotificationMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->inflateIfNecessary()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->activate(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setFindText(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->deactivate()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected shouldInflate()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic viewInflated(Landroid/view/View;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->viewInflated(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    return-void
.end method

.method protected viewInflated(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mCallback:Landroid/view/ActionMode$Callback;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setActionModeCallbackForTextEdit(Landroid/view/ActionMode$Callback;)V

    return-void
.end method
