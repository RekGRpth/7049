.class public abstract Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mInflatedView:Landroid/view/View;

.field private mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mParentView:Landroid/view/View;

.field private mPhoneResourceId:I

.field private mRegisteredForNotifications:Z

.field private mTabletResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p2}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;-><init>(Landroid/app/Activity;II)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;II)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mPhoneResourceId:I

    iput v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mTabletResourceId:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mRegisteredForNotifications:Z

    new-instance v0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader$1;-><init>(Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mActivity:Landroid/app/Activity;

    iput p2, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mPhoneResourceId:I

    iput p3, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mTabletResourceId:I

    return-void
.end method

.method public constructor <init>(Landroid/view/View;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p2}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;-><init>(Landroid/view/View;II)V

    return-void
.end method

.method public constructor <init>(Landroid/view/View;II)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mPhoneResourceId:I

    iput v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mTabletResourceId:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mRegisteredForNotifications:Z

    new-instance v0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader$1;-><init>(Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mParentView:Landroid/view/View;

    iput p2, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mPhoneResourceId:I

    iput p3, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mTabletResourceId:I

    return-void
.end method

.method private getDeviceSpecificResourceId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mActivity:Landroid/app/Activity;

    :goto_0
    invoke-static {v0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mTabletResourceId:I

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mParentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mPhoneResourceId:I

    goto :goto_1
.end method


# virtual methods
.method public getInflatedView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mInflatedView:Landroid/view/View;

    return-object v0
.end method

.method protected getNotificationsToRegister()[I
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected handleNotificationMessage(Landroid/os/Message;)V
    .locals 0

    return-void
.end method

.method protected inflateIfNecessary()Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->inflateIfNecessary(Z)Z

    move-result v0

    return v0
.end method

.method protected inflateIfNecessary(Z)Z
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mInflatedView:Landroid/view/View;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->shouldInflate()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->getDeviceSpecificResourceId()I

    move-result v2

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mParentView:Landroid/view/View;

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mParentView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    :cond_2
    :goto_1
    sget-boolean v2, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mActivity:Landroid/app/Activity;

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mInflatedView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mInflatedView:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/apps/chrome/widget/lazyloading/LazilyLoadable;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mInflatedView:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/chrome/widget/lazyloading/LazilyLoadable;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/widget/lazyloading/LazilyLoadable;->setLazyViewLoader(Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mInflatedView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->viewInflated(Landroid/view/View;)V

    move v0, v1

    goto :goto_0
.end method

.method public initialize()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->getNotificationsToRegister()[I

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mRegisteredForNotifications:Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    const/16 v0, 0x27

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    :cond_0
    return-void
.end method

.method protected abstract shouldInflate()Z
.end method

.method public uninitialize()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mRegisteredForNotifications:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->getNotificationsToRegister()[I

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mRegisteredForNotifications:Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    const/16 v0, 0x27

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    :cond_0
    return-void
.end method

.method protected viewInflated(Landroid/view/View;)V
    .locals 0

    return-void
.end method
