.class public Lcom/google/android/apps/chrome/widget/tabstrip/SlowedProgressBar;
.super Landroid/widget/ProgressBar;


# static fields
.field private static final MIN_MS_PER_FRAME:I = 0x42


# instance fields
.field private mLastDrawTimeMs:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/SlowedProgressBar;->mLastDrawTimeMs:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/SlowedProgressBar;->mLastDrawTimeMs:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/SlowedProgressBar;->mLastDrawTimeMs:J

    return-void
.end method


# virtual methods
.method public postInvalidateOnAnimation()V
    .locals 8

    const-wide/16 v6, 0x0

    const-wide/16 v2, 0x42

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/SlowedProgressBar;->getDrawingTime()J

    move-result-wide v4

    iget-wide v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/SlowedProgressBar;->mLastDrawTimeMs:J

    cmp-long v0, v0, v6

    if-gez v0, :cond_0

    move-wide v0, v2

    :goto_0
    iput-wide v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/SlowedProgressBar;->mLastDrawTimeMs:J

    cmp-long v4, v0, v2

    if-ltz v4, :cond_1

    invoke-super {p0}, Landroid/widget/ProgressBar;->postInvalidateOnAnimation()V

    :goto_1
    return-void

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/SlowedProgressBar;->mLastDrawTimeMs:J

    sub-long v0, v4, v0

    goto :goto_0

    :cond_1
    sub-long v0, v2, v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/widget/ProgressBar;->postInvalidateDelayed(J)V

    goto :goto_1
.end method
