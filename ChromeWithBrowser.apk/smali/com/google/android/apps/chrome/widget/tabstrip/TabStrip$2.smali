.class Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "willBeSelected"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabCreated(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabClosed(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V
    invoke-static {v0, v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabCrashed(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabLoadStarted(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabLoadFinished(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabTitleChanged(ILjava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;ILjava/lang/String;)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setBackgroundTabFading(Z)V
    invoke-static {v0, v6}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setBackgroundTabFading(Z)V
    invoke-static {v0, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "fromPosition"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "toPosition"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabMoved(IIIZ)V
    invoke-static {v0, v1, v2, v3, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IIIZ)V

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabFaviconChanged(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto/16 :goto_0

    :pswitch_c
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V
    invoke-static {v0, v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "tabId"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v2

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F
    invoke-static {v1, v2, v6, v6, v6}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F

    move-result v1

    float-to-int v1, v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startFastExpand(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabPageLoadStarted(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabPageLoadFinished(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto/16 :goto_0

    :pswitch_f
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isLoading()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabPageLoadStarted(I)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto/16 :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabPageLoadFinished(I)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_a
        :pswitch_2
        :pswitch_4
        :pswitch_f
        :pswitch_d
        :pswitch_e
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_e
    .end packed-switch
.end method
