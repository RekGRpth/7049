.class Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;->forceFinished(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$402(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$602(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)Z

    :cond_0
    return v2
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 12

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->getFinalX()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    float-to-int v1, v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$602(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$402(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/third_party/OverScroller;->forceFinished(Z)V

    :cond_0
    move v11, v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    float-to-int v1, v1

    const/4 v2, 0x0

    float-to-int v3, p3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F
    invoke-static {v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/apps/chrome/third_party/OverScroller;->fling(IIIIIIIIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;->getFinalX()I

    move-result v1

    add-int/2addr v1, v11

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;->setFinalX(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->invalidate()V

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startDrag(F)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;F)V

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F
    invoke-static {v0, v1, v5, v5, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->getFinalX()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, p3

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;

    move-result-object v1

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->setFinalX(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->invalidate()V

    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDownDragStartThresholdDip:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDownDragStartThresholdDip:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v3

    const/high16 v4, 0x40000000

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float v0, v1, v0

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->TAN_OF_TILT_FOR_DOWN_DRAG:F
    invoke-static {}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3600()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startDrag(F)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;F)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$402(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    return v5

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v1

    if-eqz v1, :cond_5

    cmpl-float v1, v0, v2

    if-eqz v1, :cond_5

    cmpl-float v1, v0, v2

    if-lez v1, :cond_3

    cmpg-float v1, p3, v2

    if-ltz v1, :cond_4

    :cond_3
    cmpg-float v1, v0, v2

    if-gez v1, :cond_0

    cmpl-float v1, p3, v2

    if-lez v1, :cond_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    sub-float/2addr v0, p3

    float-to-int v0, v0

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startFastExpand(I)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v0

    sub-float/2addr v0, p3

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setScrollOffsetPosition(FZZ)V
    invoke-static {v1, v0, v2, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;FZZ)V

    goto :goto_0
.end method
