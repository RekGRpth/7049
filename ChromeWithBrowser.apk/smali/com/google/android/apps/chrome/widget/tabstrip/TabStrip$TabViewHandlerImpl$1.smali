.class Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

.field final synthetic val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "TabStrip:TabClosed"

    invoke-static {v0}, Lorg/chromium/content/common/PerfTraceEvent;->begin(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->closeTabById(I)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->close()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v1, v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->getNextTabIfClosed(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v1, v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v1, v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v1, v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V
    invoke-static {v0, v1, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->invalidate()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->resetResizeTimeout(Z)V
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V

    goto :goto_1
.end method
