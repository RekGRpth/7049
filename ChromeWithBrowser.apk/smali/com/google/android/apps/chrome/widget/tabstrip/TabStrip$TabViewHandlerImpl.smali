.class Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V

    return-void
.end method


# virtual methods
.method public handleTabClosePress(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mChromeViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/ContentViewHolder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/ContentViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public handleTabPressDown(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$402(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$502(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$602(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getTexture(IZ)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    :cond_0
    return-void
.end method

.method public handleTabSelectPress(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 7

    const/high16 v6, 0x40000000

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabVisibleAmount()F

    move-result v0

    const v1, 0x3e99999a

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    const-string v0, "TabStrip:TabSelected"

    invoke-static {v0}, Lorg/chromium/content/common/PerfTraceEvent;->begin(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V
    invoke-static {v0, v1, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/TabModel;->indexOf(Lcom/google/android/apps/chrome/Tab;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->setIndex(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->indexOf(Lcom/google/android/apps/chrome/Tab;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    div-float/2addr v1, v6

    add-float/2addr v1, v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfNewTabButtonSpace:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    float-to-int v0, v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v3

    sub-float/2addr v2, v3

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfNewTabButtonSpace:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v6

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    neg-float v0, v0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    sub-float v2, v5, v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    float-to-int v2, v2

    float-to-int v0, v0

    invoke-virtual {v1, v2, v4, v0, v4}, Lcom/google/android/apps/chrome/third_party/OverScroller;->startScroll(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->invalidate()V

    goto/16 :goto_0
.end method
