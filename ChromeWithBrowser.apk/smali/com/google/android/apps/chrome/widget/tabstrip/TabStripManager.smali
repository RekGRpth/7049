.class public Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lcom/google/android/apps/chrome/OverviewBehavior;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mChromeViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

.field private mFaded:Z

.field private mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

.field private mIncognitoButtonWrapper:Landroid/view/View;

.field private mIncognitoIndicator:Landroid/view/View;

.field private mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

.field private mIsIncognito:Z

.field private mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xc
        0x11
        0x12
        0x2e
        0x2f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFaded:Z

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager$1;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040039

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->handleTabModelSelection(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->setFadeRequired(Z)V

    return-void
.end method

.method private createAllIncognitoAssetsIfNecessary()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    if-nez v0, :cond_2

    const v0, 0x7f0f00c3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setTabModel(Lcom/google/android/apps/chrome/TabModel;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mChromeViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setChromeViewHolder(Lcom/google/android/apps/chrome/ContentViewHolder;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoIndicator:Landroid/view/View;

    if-nez v0, :cond_3

    const v0, 0x7f0f00c0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoIndicator:Landroid/view/View;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    if-nez v0, :cond_0

    const v0, 0x7f0f00c5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButtonWrapper:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButtonWrapper:Landroid/view/View;

    const v1, 0x7f020007

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    const v0, 0x7f0f00c6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    goto :goto_0
.end method

.method private handleTabModelSelection(Z)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->createAllIncognitoAssetsIfNecessary()V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    if-eqz v0, :cond_4

    const v0, 0x7f020008

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->setBackgroundResource(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    :goto_2
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    :goto_3
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->refreshSelection()V

    if-eqz v0, :cond_3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setVisibility(I)V

    :cond_3
    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setVisibility(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->bringChildToFront(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoIndicator:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoIndicator:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_4
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    const v0, 0x7f0a0029

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_4
.end method

.method private setFadeRequired(Z)V
    .locals 3

    const/16 v2, 0x4b

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFaded:Z

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFaded:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoIndicator:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoIndicator:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFaded:Z

    if-eqz v1, :cond_3

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButtonWrapper:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButtonWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFaded:Z

    if-eqz v1, :cond_4

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    goto :goto_1

    :cond_4
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    goto :goto_0
.end method


# virtual methods
.method public hideOverview(Z)V
    .locals 0

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0f00c2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    const v0, 0x7f0a0029

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->setBackgroundResource(I)V

    return-void
.end method

.method public overviewVisible()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public setChromeViewHolder(Lcom/google/android/apps/chrome/ContentViewHolder;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mChromeViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mChromeViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setChromeViewHolder(Lcom/google/android/apps/chrome/ContentViewHolder;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mChromeViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setChromeViewHolder(Lcom/google/android/apps/chrome/ContentViewHolder;)V

    :cond_1
    return-void
.end method

.method public setEnableAnimations(Z)V
    .locals 0

    return-void
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setTabModel(Lcom/google/android/apps/chrome/TabModel;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModelSelector;->getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setTabModel(Lcom/google/android/apps/chrome/TabModel;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    :cond_2
    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->handleTabModelSelection(Z)V

    return-void
.end method

.method public setTiltControlsEnabled(Z)V
    .locals 0

    return-void
.end method

.method public showOverview(Z)V
    .locals 0

    return-void
.end method
