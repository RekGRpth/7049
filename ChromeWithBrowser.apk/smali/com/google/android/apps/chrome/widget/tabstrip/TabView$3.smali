.class Lcom/google/android/apps/chrome/widget/tabstrip/TabView$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->access$000(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->IDLE:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->access$100(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;->handleTabClosePress(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->tabStripCloseTab()V

    return-void
.end method
