.class Lcom/google/android/apps/chrome/widget/tabstrip/TabView$7;
.super Landroid/animation/AnimatorListenerAdapter;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$7;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$7;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->access$602(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$7;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->access$500(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->invalidate()V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$7;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->access$500(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void
.end method
