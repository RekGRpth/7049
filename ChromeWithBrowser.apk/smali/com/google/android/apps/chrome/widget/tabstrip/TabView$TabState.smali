.class public final enum Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

.field public static final enum CLOSED:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

.field public static final enum CLOSING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

.field public static final enum DYING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

.field public static final enum IDLE:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

.field public static final enum OPENING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    const-string v1, "OPENING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->OPENING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    const-string v1, "CLOSING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSED:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->IDLE:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    const-string v1, "DYING"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->DYING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->OPENING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSED:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->IDLE:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->DYING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->$VALUES:[Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->$VALUES:[Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    return-object v0
.end method
