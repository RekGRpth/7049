.class Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$ControlsOffsetProperty;
.super Landroid/util/Property;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$ControlsOffsetProperty;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    const-class v0, Ljava/lang/Float;

    const-string v1, "controlsOffset"

    invoke-direct {p0, v0, v1}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public get(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)Ljava/lang/Float;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$ControlsOffsetProperty;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainer:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->access$000(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$ControlsOffsetProperty;->get(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public set(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;Ljava/lang/Float;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$ControlsOffsetProperty;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mBrowserControlOffset:F
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->access$102(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;F)F

    # invokes: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->updateTopControlsOffset()V
    invoke-static {p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->access$200(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$ControlsOffsetProperty;->set(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;Ljava/lang/Float;)V

    return-void
.end method
