.class public final Lcom/google/ipc/invalidation/a/F;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

.field public static final b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field public static final c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

.field private static d:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    move-result-object v0

    invoke-static {v2, v3}, Lcom/google/android/b/c;->a(II)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/a/F;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v3, v1}, Lcom/google/android/b/c;->a(II)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-static {v2, v2}, Lcom/google/android/b/c;->a(II)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/a/F;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->a:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->a()I

    move-result v0

    sput v0, Lcom/google/ipc/invalidation/a/F;->d:I

    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->b:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->a()I

    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->a:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->a()I

    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->b:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->a()I

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v0

    sget v1, Lcom/google/ipc/invalidation/a/F;->d:I

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/a/F;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    return-void
.end method
