.class public Lcom/google/ipc/invalidation/a/G;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/a/a/h;


# instance fields
.field final synthetic a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;


# direct methods
.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/a/G;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/ByteString;->c()[B

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/b/c;->a(Lcom/google/ipc/invalidation/b/i;[B)Lcom/google/ipc/invalidation/b/i;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/ipc/invalidation/b/i;
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string v0, "(Inv: "

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/b/i;

    const-string v0, ", "

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "<"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->j()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/ipc/invalidation/b/i;->a(J)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ", P:"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->n()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;

    :cond_2
    const/16 v0, 0x29

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    goto :goto_0
.end method

.method public static a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/b/i;
    .locals 4

    if-nez p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string v0, "(Obj: %s, "

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;

    const/16 v0, 0x29

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    goto :goto_0
.end method

.method public static a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/a/H;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/a/H;-><init>(Lcom/google/protobuf/ByteString;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/ipc/invalidation/a/G;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/a/G;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)V

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/b/d;->a(Ljava/lang/Object;Lcom/google/a/a/h;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a([B)Ljava/lang/Object;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/a/I;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/a/I;-><init>([B)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/ipc/invalidation/b/i;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/G;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {p1, v0}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/b/i;

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {p0, p1}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/ipc/invalidation/b/i;)V

    return-void
.end method
