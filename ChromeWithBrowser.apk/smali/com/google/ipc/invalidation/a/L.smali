.class final Lcom/google/ipc/invalidation/a/L;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private a:Z

.field private synthetic b:Lcom/google/ipc/invalidation/a/K;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/a/K;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/a/L;->b:Lcom/google/ipc/invalidation/a/K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/a/L;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/a/L;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/a/L;->a:Z

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/L;->b:Lcom/google/ipc/invalidation/a/K;

    iget-object v0, v0, Lcom/google/ipc/invalidation/a/K;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not allowed"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
