.class public final Lcom/google/ipc/invalidation/a/O;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/ipc/invalidation/a/N;

.field private final b:Lcom/google/ipc/invalidation/a/P;

.field private final c:Lcom/google/ipc/invalidation/a/Q;


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/P;Lcom/google/ipc/invalidation/a/Q;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/ipc/invalidation/a/O;->a:Lcom/google/ipc/invalidation/a/N;

    iput-object p2, p0, Lcom/google/ipc/invalidation/a/O;->b:Lcom/google/ipc/invalidation/a/P;

    iput-object p3, p0, Lcom/google/ipc/invalidation/a/O;->c:Lcom/google/ipc/invalidation/a/Q;

    return-void
.end method

.method public static a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;
    .locals 3

    new-instance v0, Lcom/google/ipc/invalidation/a/O;

    sget-object v1, Lcom/google/ipc/invalidation/a/P;->a:Lcom/google/ipc/invalidation/a/P;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/ipc/invalidation/a/O;-><init>(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/P;Lcom/google/ipc/invalidation/a/Q;)V

    return-object v0
.end method

.method public static a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;
    .locals 3

    new-instance v1, Lcom/google/ipc/invalidation/a/O;

    sget-object v2, Lcom/google/ipc/invalidation/a/P;->a:Lcom/google/ipc/invalidation/a/P;

    const-string v0, "messageInfo cannot be null"

    invoke-static {p1, v0}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/a/Q;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/ipc/invalidation/a/O;-><init>(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/P;Lcom/google/ipc/invalidation/a/Q;)V

    return-object v1
.end method

.method public static b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;
    .locals 3

    new-instance v0, Lcom/google/ipc/invalidation/a/O;

    sget-object v1, Lcom/google/ipc/invalidation/a/P;->b:Lcom/google/ipc/invalidation/a/P;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/ipc/invalidation/a/O;-><init>(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/P;Lcom/google/ipc/invalidation/a/Q;)V

    return-object v0
.end method

.method public static b(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;
    .locals 3

    new-instance v1, Lcom/google/ipc/invalidation/a/O;

    sget-object v2, Lcom/google/ipc/invalidation/a/P;->b:Lcom/google/ipc/invalidation/a/P;

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/a/Q;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/ipc/invalidation/a/O;-><init>(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/P;Lcom/google/ipc/invalidation/a/Q;)V

    return-object v1
.end method


# virtual methods
.method public final a()Lcom/google/ipc/invalidation/a/N;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/O;->a:Lcom/google/ipc/invalidation/a/N;

    return-object v0
.end method

.method final b()Lcom/google/ipc/invalidation/a/P;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/O;->b:Lcom/google/ipc/invalidation/a/P;

    return-object v0
.end method

.method final c()Lcom/google/ipc/invalidation/a/Q;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/O;->c:Lcom/google/ipc/invalidation/a/Q;

    return-object v0
.end method

.method final d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/O;->c:Lcom/google/ipc/invalidation/a/Q;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
