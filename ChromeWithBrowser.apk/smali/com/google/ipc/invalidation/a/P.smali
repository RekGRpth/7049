.class final enum Lcom/google/ipc/invalidation/a/P;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/ipc/invalidation/a/P;

.field public static final enum b:Lcom/google/ipc/invalidation/a/P;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/ipc/invalidation/a/P;

    const-string v1, "REQUIRED"

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/P;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/a/P;->a:Lcom/google/ipc/invalidation/a/P;

    new-instance v0, Lcom/google/ipc/invalidation/a/P;

    const-string v1, "OPTIONAL"

    invoke-direct {v0, v1, v3}, Lcom/google/ipc/invalidation/a/P;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/a/P;->b:Lcom/google/ipc/invalidation/a/P;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/ipc/invalidation/a/P;

    sget-object v1, Lcom/google/ipc/invalidation/a/P;->a:Lcom/google/ipc/invalidation/a/P;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ipc/invalidation/a/P;->b:Lcom/google/ipc/invalidation/a/P;

    aput-object v1, v0, v3

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method
