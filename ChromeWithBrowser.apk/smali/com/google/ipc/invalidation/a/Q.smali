.class public Lcom/google/ipc/invalidation/a/Q;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/ipc/invalidation/a/M;

.field private final b:Ljava/util/Set;

.field private c:I


# direct methods
.method public varargs constructor <init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V
    .locals 10

    const/4 v9, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/Q;->b:Ljava/util/Set;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Lcom/google/ipc/invalidation/a/M;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iput-object p1, p0, Lcom/google/ipc/invalidation/a/Q;->a:Lcom/google/ipc/invalidation/a/M;

    array-length v3, p2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, p2, v0

    invoke-virtual {v4}, Lcom/google/ipc/invalidation/a/O;->a()Lcom/google/ipc/invalidation/a/N;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/ipc/invalidation/a/N;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v5

    const-string v6, "Bad field: %s"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/google/ipc/invalidation/a/O;->a()Lcom/google/ipc/invalidation/a/N;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/ipc/invalidation/a/N;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v5, v6, v7}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/ipc/invalidation/a/Q;->b:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4}, Lcom/google/ipc/invalidation/a/O;->b()Lcom/google/ipc/invalidation/a/P;

    move-result-object v4

    sget-object v5, Lcom/google/ipc/invalidation/a/P;->a:Lcom/google/ipc/invalidation/a/P;

    if-ne v4, v5, :cond_0

    iget v4, p0, Lcom/google/ipc/invalidation/a/Q;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/ipc/invalidation/a/Q;->c:I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const-string v3, "Not all fields specified in %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v1

    aput-object v2, v4, v9

    invoke-static {v0, v3, v4}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/M;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/Q;->a:Lcom/google/ipc/invalidation/a/M;

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/Q;->b:Ljava/util/Set;

    return-object v0
.end method

.method protected a(Lcom/google/protobuf/MessageLite;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
