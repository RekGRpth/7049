.class public final Lcom/google/ipc/invalidation/a/R;
.super Lcom/google/ipc/invalidation/a/J;


# instance fields
.field final b:Lcom/google/ipc/invalidation/a/X;

.field private final c:Lcom/google/ipc/invalidation/a/S;

.field private d:Lcom/google/ipc/invalidation/a/ad;


# direct methods
.method public constructor <init>(Lcom/google/ipc/invalidation/b/a;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/a/J;-><init>(Lcom/google/ipc/invalidation/b/a;)V

    new-instance v0, Lcom/google/ipc/invalidation/a/X;

    invoke-direct {v0, p0, v1}, Lcom/google/ipc/invalidation/a/X;-><init>(Lcom/google/ipc/invalidation/a/R;B)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/R;->b:Lcom/google/ipc/invalidation/a/X;

    new-instance v0, Lcom/google/ipc/invalidation/a/S;

    invoke-direct {v0, p0, v1}, Lcom/google/ipc/invalidation/a/S;-><init>(Lcom/google/ipc/invalidation/a/R;B)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/R;->c:Lcom/google/ipc/invalidation/a/S;

    new-instance v0, Lcom/google/ipc/invalidation/a/ad;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/a/ad;-><init>(Lcom/google/ipc/invalidation/a/R;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/R;->d:Lcom/google/ipc/invalidation/a/ad;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/R;->c:Lcom/google/ipc/invalidation/a/S;

    iget-object v0, v0, Lcom/google/ipc/invalidation/a/S;->a:Lcom/google/ipc/invalidation/a/Q;

    invoke-virtual {p0, p1, v0}, Lcom/google/ipc/invalidation/a/R;->a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/Q;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/R;->b:Lcom/google/ipc/invalidation/a/X;

    iget-object v0, v0, Lcom/google/ipc/invalidation/a/X;->c:Lcom/google/ipc/invalidation/a/Q;

    invoke-virtual {p0, p1, v0}, Lcom/google/ipc/invalidation/a/R;->a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/Q;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/R;->d:Lcom/google/ipc/invalidation/a/ad;

    iget-object v0, v0, Lcom/google/ipc/invalidation/a/ad;->a:Lcom/google/ipc/invalidation/a/Q;

    invoke-virtual {p0, p1, v0}, Lcom/google/ipc/invalidation/a/R;->a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/Q;)Z

    move-result v0

    return v0
.end method
