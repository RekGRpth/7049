.class final Lcom/google/ipc/invalidation/a/S;
.super Ljava/lang/Object;


# instance fields
.field final a:Lcom/google/ipc/invalidation/a/Q;

.field final synthetic b:Lcom/google/ipc/invalidation/a/R;

.field private c:Lcom/google/ipc/invalidation/a/Q;

.field private d:Lcom/google/ipc/invalidation/a/Q;

.field private e:Lcom/google/ipc/invalidation/a/Q;

.field private f:Lcom/google/ipc/invalidation/a/Q;

.field private g:Lcom/google/ipc/invalidation/a/Q;

.field private h:Lcom/google/ipc/invalidation/a/Q;

.field private i:Lcom/google/ipc/invalidation/a/Q;

.field private j:Lcom/google/ipc/invalidation/a/Q;


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/a/R;)V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iput-object p1, p0, Lcom/google/ipc/invalidation/a/S;->b:Lcom/google/ipc/invalidation/a/R;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/ipc/invalidation/a/T;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->c:Lcom/google/ipc/invalidation/a/d;

    const/4 v2, 0x7

    new-array v2, v2, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/d;->a:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->b:Lcom/google/ipc/invalidation/a/R;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/R;->b:Lcom/google/ipc/invalidation/a/X;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/X;->b:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v5

    sget-object v3, Lcom/google/ipc/invalidation/a/d;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/d;->c:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->b:Lcom/google/ipc/invalidation/a/R;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/R;->b:Lcom/google/ipc/invalidation/a/X;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/X;->f:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    sget-object v3, Lcom/google/ipc/invalidation/a/d;->d:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v8

    sget-object v3, Lcom/google/ipc/invalidation/a/d;->e:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/google/ipc/invalidation/a/d;->f:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/google/ipc/invalidation/a/d;->g:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, p0, v1, v2}, Lcom/google/ipc/invalidation/a/T;-><init>(Lcom/google/ipc/invalidation/a/S;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/S;->c:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/U;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->a:Lcom/google/ipc/invalidation/a/b;

    new-array v2, v7, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/b;->a:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v5

    sget-object v3, Lcom/google/ipc/invalidation/a/b;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-direct {v0, p0, v1, v2}, Lcom/google/ipc/invalidation/a/U;-><init>(Lcom/google/ipc/invalidation/a/S;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/S;->d:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/V;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->j:Lcom/google/ipc/invalidation/a/k;

    new-array v2, v9, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/k;->a:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v5

    sget-object v3, Lcom/google/ipc/invalidation/a/k;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/k;->d:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    sget-object v3, Lcom/google/ipc/invalidation/a/k;->c:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->d:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-direct {v0, p0, v1, v2}, Lcom/google/ipc/invalidation/a/V;-><init>(Lcom/google/ipc/invalidation/a/S;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/S;->e:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/Q;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->q:Lcom/google/ipc/invalidation/a/s;

    new-array v2, v6, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/s;->a:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->b:Lcom/google/ipc/invalidation/a/R;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/R;->b:Lcom/google/ipc/invalidation/a/X;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/X;->e:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/S;->f:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/Q;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->e:Lcom/google/ipc/invalidation/a/f;

    new-array v2, v9, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/f;->a:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->b:Lcom/google/ipc/invalidation/a/R;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/R;->b:Lcom/google/ipc/invalidation/a/X;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/X;->a:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v5

    sget-object v3, Lcom/google/ipc/invalidation/a/f;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/f;->c:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    sget-object v3, Lcom/google/ipc/invalidation/a/f;->d:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/S;->g:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/Q;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->h:Lcom/google/ipc/invalidation/a/i;

    const/4 v2, 0x5

    new-array v2, v2, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/i;->a:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->g:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v5

    sget-object v3, Lcom/google/ipc/invalidation/a/i;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/i;->c:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    sget-object v3, Lcom/google/ipc/invalidation/a/i;->e:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->b:Lcom/google/ipc/invalidation/a/R;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/R;->b:Lcom/google/ipc/invalidation/a/X;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/X;->g:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v8

    sget-object v3, Lcom/google/ipc/invalidation/a/i;->d:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/S;->h:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/Q;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->u:Lcom/google/ipc/invalidation/a/w;

    new-array v2, v6, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/w;->a:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/S;->i:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/Q;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->w:Lcom/google/ipc/invalidation/a/y;

    new-array v2, v6, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/y;->a:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->i:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/S;->j:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/W;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->d:Lcom/google/ipc/invalidation/a/e;

    const/4 v2, 0x6

    new-array v2, v2, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/e;->a:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->c:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v5

    sget-object v3, Lcom/google/ipc/invalidation/a/e;->f:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->h:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/e;->b:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->e:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    sget-object v3, Lcom/google/ipc/invalidation/a/e;->e:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->b:Lcom/google/ipc/invalidation/a/R;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/R;->b:Lcom/google/ipc/invalidation/a/X;

    iget-object v4, v4, Lcom/google/ipc/invalidation/a/X;->d:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v8

    sget-object v3, Lcom/google/ipc/invalidation/a/e;->c:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/S;->f:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/google/ipc/invalidation/a/e;->d:Lcom/google/ipc/invalidation/a/N;

    iget-object v5, p0, Lcom/google/ipc/invalidation/a/S;->j:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v4, v5}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, p0, v1, v2}, Lcom/google/ipc/invalidation/a/W;-><init>(Lcom/google/ipc/invalidation/a/S;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/S;->a:Lcom/google/ipc/invalidation/a/Q;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ipc/invalidation/a/R;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/a/S;-><init>(Lcom/google/ipc/invalidation/a/R;)V

    return-void
.end method
