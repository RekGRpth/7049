.class final Lcom/google/ipc/invalidation/a/T;
.super Lcom/google/ipc/invalidation/a/Q;


# instance fields
.field private synthetic a:Lcom/google/ipc/invalidation/a/S;


# direct methods
.method varargs constructor <init>(Lcom/google/ipc/invalidation/a/S;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/a/T;->a:Lcom/google/ipc/invalidation/a/S;

    invoke-direct {p0, p2, p3}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/MessageLite;)Z
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    check-cast p1, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->h()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/ByteString;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/ipc/invalidation/a/T;->a:Lcom/google/ipc/invalidation/a/S;

    iget-object v2, v2, Lcom/google/ipc/invalidation/a/S;->b:Lcom/google/ipc/invalidation/a/R;

    iget-object v2, v2, Lcom/google/ipc/invalidation/a/R;->a:Lcom/google/ipc/invalidation/b/a;

    const-string v3, "Client token was set but empty: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    invoke-interface {v2, v3, v1}, Lcom/google/ipc/invalidation/b/a;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->o()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/ipc/invalidation/a/T;->a:Lcom/google/ipc/invalidation/a/S;

    iget-object v2, v2, Lcom/google/ipc/invalidation/a/S;->b:Lcom/google/ipc/invalidation/a/R;

    iget-object v2, v2, Lcom/google/ipc/invalidation/a/R;->a:Lcom/google/ipc/invalidation/b/a;

    const-string v3, "Message id was set but empty: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    invoke-interface {v2, v3, v1}, Lcom/google/ipc/invalidation/b/a;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->l()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    iget-object v2, p0, Lcom/google/ipc/invalidation/a/T;->a:Lcom/google/ipc/invalidation/a/S;

    iget-object v2, v2, Lcom/google/ipc/invalidation/a/S;->b:Lcom/google/ipc/invalidation/a/R;

    iget-object v2, v2, Lcom/google/ipc/invalidation/a/R;->a:Lcom/google/ipc/invalidation/b/a;

    const-string v3, "Client time was negative: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    invoke-interface {v2, v3, v1}, Lcom/google/ipc/invalidation/b/a;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->n()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/a/T;->a:Lcom/google/ipc/invalidation/a/S;

    iget-object v2, v2, Lcom/google/ipc/invalidation/a/S;->b:Lcom/google/ipc/invalidation/a/R;

    iget-object v2, v2, Lcom/google/ipc/invalidation/a/R;->a:Lcom/google/ipc/invalidation/b/a;

    const-string v3, "Max known server time was negative: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    invoke-interface {v2, v3, v1}, Lcom/google/ipc/invalidation/b/a;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
