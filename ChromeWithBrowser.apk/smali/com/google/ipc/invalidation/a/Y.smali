.class final Lcom/google/ipc/invalidation/a/Y;
.super Lcom/google/ipc/invalidation/a/Q;


# instance fields
.field private synthetic a:Lcom/google/ipc/invalidation/a/X;


# direct methods
.method varargs constructor <init>(Lcom/google/ipc/invalidation/a/X;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/a/Y;->a:Lcom/google/ipc/invalidation/a/X;

    invoke-direct {p0, p2, p3}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/MessageLite;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    check-cast p1, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->f()I

    move-result v2

    if-ltz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->h()I

    move-result v2

    if-gez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/ipc/invalidation/a/Y;->a:Lcom/google/ipc/invalidation/a/X;

    iget-object v2, v2, Lcom/google/ipc/invalidation/a/X;->h:Lcom/google/ipc/invalidation/a/R;

    iget-object v2, v2, Lcom/google/ipc/invalidation/a/R;->a:Lcom/google/ipc/invalidation/b/a;

    const-string v3, "Invalid versions: %s"

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    invoke-interface {v2, v3, v0}, Lcom/google/ipc/invalidation/b/a;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    :cond_1
    return v0
.end method
