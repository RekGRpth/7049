.class public final Lcom/google/ipc/invalidation/a/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/a/M;


# static fields
.field public static final a:Lcom/google/ipc/invalidation/a/N;

.field public static final b:Lcom/google/ipc/invalidation/a/N;

.field public static final c:Lcom/google/ipc/invalidation/a/N;

.field public static final d:Lcom/google/ipc/invalidation/a/N;

.field public static final e:Lcom/google/ipc/invalidation/a/N;

.field private static final f:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "client_version"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "config_parameter"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "performance_counter"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "server_registration_summary_requested"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "client_config"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/i;->f:Ljava/util/Set;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "client_version"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/i;->a:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "config_parameter"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/i;->b:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "performance_counter"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/i;->c:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "server_registration_summary_requested"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/i;->d:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "client_config"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/i;->e:Lcom/google/ipc/invalidation/a/N;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/util/Collection;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/a/i;->f:Ljava/util/Set;

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/N;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    sget-object v2, Lcom/google/ipc/invalidation/a/i;->a:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v2, :cond_1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->e()Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v2, Lcom/google/ipc/invalidation/a/i;->b:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v2, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->h()I

    move-result v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/ipc/invalidation/a/i;->c:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v2, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->j()I

    move-result v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/ipc/invalidation/a/i;->d:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->k()Z

    move-result v0

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/ipc/invalidation/a/i;->e:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->m()Z

    move-result v0

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad descriptor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/N;)Ljava/lang/Object;
    .locals 3

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    sget-object v0, Lcom/google/ipc/invalidation/a/i;->a:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/a/i;->b:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/ipc/invalidation/a/i;->c:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->i()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/ipc/invalidation/a/i;->d:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/ipc/invalidation/a/i;->e:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->n()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad descriptor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
