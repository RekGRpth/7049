.class public Lcom/google/ipc/invalidation/b/d;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private final b:I

.field private c:I

.field private d:Z

.field private final e:Ljava/util/Random;


# direct methods
.method public constructor <init>(Ljava/util/Random;II)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lez p3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "max factor must be positive"

    invoke-static {v0, v3}, Lcom/google/a/a/a;->a(ZLjava/lang/Object;)V

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    iput-object v0, p0, Lcom/google/ipc/invalidation/b/d;->e:Ljava/util/Random;

    iput p3, p0, Lcom/google/ipc/invalidation/b/d;->b:I

    iput p2, p0, Lcom/google/ipc/invalidation/b/d;->a:I

    if-lez p2, :cond_1

    :goto_1
    const-string v0, "initial delay must be positive"

    invoke-static {v1, v0}, Lcom/google/a/a/a;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/b/d;->b()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method protected constructor <init>(Ljava/util/Random;IIIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/ipc/invalidation/b/d;-><init>(Ljava/util/Random;II)V

    iput p4, p0, Lcom/google/ipc/invalidation/b/d;->c:I

    iput-boolean p5, p0, Lcom/google/ipc/invalidation/b/d;->d:Z

    return-void
.end method

.method public static a(Ljava/lang/Object;Lcom/google/a/a/h;)Ljava/lang/Object;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/b/f;

    invoke-direct {v0, p1}, Lcom/google/ipc/invalidation/b/f;-><init>(Lcom/google/a/a/h;)V

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 1

    iget v0, p0, Lcom/google/ipc/invalidation/b/d;->a:I

    iput v0, p0, Lcom/google/ipc/invalidation/b/d;->c:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/b/d;->d:Z

    return-void
.end method

.method public final c()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/b/d;->d:Z

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/d;->e:Ljava/util/Random;

    iget v1, p0, Lcom/google/ipc/invalidation/b/d;->c:I

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/google/ipc/invalidation/b/d;->a:I

    iget v2, p0, Lcom/google/ipc/invalidation/b/d;->b:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/google/ipc/invalidation/b/d;->c:I

    if-gt v2, v1, :cond_0

    iget v2, p0, Lcom/google/ipc/invalidation/b/d;->c:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/ipc/invalidation/b/d;->c:I

    iget v2, p0, Lcom/google/ipc/invalidation/b/d;->c:I

    if-le v2, v1, :cond_0

    iput v1, p0, Lcom/google/ipc/invalidation/b/d;->c:I

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/b/d;->d:Z

    return v0
.end method

.method protected final d()I
    .locals 1

    iget v0, p0, Lcom/google/ipc/invalidation/b/d;->c:I

    return v0
.end method

.method protected final e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/b/d;->d:Z

    return v0
.end method
