.class public final Lcom/google/ipc/invalidation/b/h;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/Random;

.field private b:D


# direct methods
.method public constructor <init>(Ljava/util/Random;I)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p2, :cond_0

    const/16 v0, 0x64

    if-gt p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/a/a/a;->b(Z)V

    iput-object p1, p0, Lcom/google/ipc/invalidation/b/h;->a:Ljava/util/Random;

    int-to-double v0, p2

    const-wide/high16 v2, 0x4059000000000000L

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/ipc/invalidation/b/h;->b:D

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 6

    const-wide/high16 v0, 0x4000000000000000L

    iget-object v2, p0, Lcom/google/ipc/invalidation/b/h;->a:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x3ff0000000000000L

    sub-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/ipc/invalidation/b/h;->b:D

    mul-double/2addr v0, v2

    int-to-double v2, p1

    int-to-double v4, p1

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method
