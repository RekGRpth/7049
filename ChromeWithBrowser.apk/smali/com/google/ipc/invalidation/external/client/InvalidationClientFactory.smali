.class public Lcom/google/ipc/invalidation/external/client/InvalidationClientFactory;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Lcom/google/ipc/invalidation/external/client/SystemResources;I[BLjava/lang/String;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)Lcom/google/ipc/invalidation/external/client/InvalidationClient;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;-><init>(I[BLjava/lang/String;Z)V

    invoke-static {p0, v0, p4}, Lcom/google/ipc/invalidation/external/client/InvalidationClientFactory;->createClient(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    move-result-object v0

    return-object v0
.end method

.method public static createClient(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)Lcom/google/ipc/invalidation/external/client/InvalidationClient;
    .locals 8

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/l;->a()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    iget-boolean v1, p1, Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;->allowSuppression:Z

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v5

    new-instance v2, Ljava/util/Random;

    invoke-interface {p0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v0

    invoke-direct {v2, v0, v1}, Ljava/util/Random;-><init>(J)V

    new-instance v0, Lcom/google/ipc/invalidation/ticl/x;

    iget v3, p1, Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;->clientType:I

    iget-object v4, p1, Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;->clientName:[B

    iget-object v6, p1, Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;->applicationName:Ljava/lang/String;

    move-object v1, p0

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/ipc/invalidation/ticl/x;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)V

    return-object v0
.end method
