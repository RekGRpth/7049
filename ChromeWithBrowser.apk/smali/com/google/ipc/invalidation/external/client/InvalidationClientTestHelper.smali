.class public Lcom/google/ipc/invalidation/external/client/InvalidationClientTestHelper;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createForTest(Lcom/google/ipc/invalidation/external/client/SystemResources;I[BLjava/lang/String;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)Lcom/google/ipc/invalidation/external/client/InvalidationClient;
    .locals 8

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/l;->b()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v5

    new-instance v2, Ljava/util/Random;

    invoke-interface {p0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v0

    invoke-direct {v2, v0, v1}, Ljava/util/Random;-><init>(J)V

    new-instance v0, Lcom/google/ipc/invalidation/ticl/x;

    move-object v1, p0

    move v3, p1

    move-object v4, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/ipc/invalidation/ticl/x;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)V

    return-object v0
.end method
