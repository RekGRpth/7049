.class public Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;
.super Ljava/lang/Object;


# instance fields
.field private internalScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

.field private listenerScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

.field private logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

.field private network:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

.field private platform:Ljava/lang/String;

.field private sealed:Z

.field private storage:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;


# direct methods
.method public constructor <init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iput-object p2, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->internalScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    iput-object p3, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->listenerScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    iput-object p4, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->network:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

    iput-object p5, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->storage:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    return-void
.end method

.method public constructor <init>(Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iget-object v0, p1, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->internalScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->internalScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    iget-object v0, p1, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->listenerScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->listenerScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    iget-object v0, p1, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->network:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->network:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

    iget-object v0, p1, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->storage:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->storage:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->sealed:Z

    return-void
.end method


# virtual methods
.method public build()Lcom/google/ipc/invalidation/external/client/SystemResources;
    .locals 7

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->sealed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Builder\'s build method has already been called"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->seal()V

    new-instance v0, Lcom/google/ipc/invalidation/ticl/a;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->internalScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->listenerScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    iget-object v4, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->network:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

    iget-object v5, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->storage:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    iget-object v6, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->platform:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/ipc/invalidation/ticl/a;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;Ljava/lang/String;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->internalScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    return-object v0
.end method

.method public getListenerScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->listenerScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    return-object v0
.end method

.method public getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-object v0
.end method

.method public getNetwork()Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->network:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

    return-object v0
.end method

.method public getStorage()Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->storage:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    return-object v0
.end method

.method protected seal()V
    .locals 3

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->sealed:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Builder\'s already sealed"

    invoke-static {v0, v2}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->sealed:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setInternalScheduler(Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;)Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;
    .locals 2

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->sealed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Builder\'s build method has already been called"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->internalScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setListenerScheduler(Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;)Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;
    .locals 2

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->sealed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Builder\'s build method has already been called"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->listenerScheduler:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLogger(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;
    .locals 2

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->sealed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Builder\'s build method has already been called"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setNetwork(Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;)Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;
    .locals 2

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->sealed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Builder\'s build method has already been called"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->network:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPlatform(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;
    .locals 2

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->sealed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Builder\'s build method has already been called"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->platform:Ljava/lang/String;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setStorage(Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;)Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;
    .locals 2

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->sealed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Builder\'s build method has already been called"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->storage:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
