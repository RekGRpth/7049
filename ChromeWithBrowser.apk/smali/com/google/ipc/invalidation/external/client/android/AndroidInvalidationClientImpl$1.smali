.class Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$BoundWork;


# instance fields
.field final synthetic this$0:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;

.field final synthetic val$request:Lcom/google/ipc/invalidation/external/client/android/service/Request;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;Lcom/google/ipc/invalidation/external/client/android/service/Request;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl$1;->this$0:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;

    iput-object p2, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl$1;->val$request:Lcom/google/ipc/invalidation/external/client/android/service/Request;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService;)V
    .locals 7

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl$1;->val$request:Lcom/google/ipc/invalidation/external/client/android/service/Request;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {p1, v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService;->handleRequest(Landroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Response;

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Response;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response;->warnOnFailure()V

    return-void

    :catch_0
    move-exception v0

    # getter for: Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    invoke-static {}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->access$000()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    const-string v3, "Remote exeption executing request %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl$1;->val$request:Lcom/google/ipc/invalidation/external/client/android/service/Request;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic run(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService;

    invoke-virtual {p0, p1}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl$1;->run(Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService;)V

    return-void
.end method
