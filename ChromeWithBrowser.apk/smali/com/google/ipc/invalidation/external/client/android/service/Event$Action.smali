.class public final enum Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

.field public static final enum INFORM_ERROR:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

.field public static final enum INFORM_REGISTRATION_FAILURE:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

.field public static final enum INFORM_REGISTRATION_STATUS:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

.field public static final enum INVALIDATE:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

.field public static final enum INVALIDATE_ALL:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

.field public static final enum INVALIDATE_UNKNOWN:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

.field public static final enum READY:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

.field public static final enum REISSUE_REGISTRATIONS:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    const-string v1, "READY"

    invoke-direct {v0, v1, v3}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->READY:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    const-string v1, "INVALIDATE"

    invoke-direct {v0, v1, v4}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INVALIDATE:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    const-string v1, "INVALIDATE_UNKNOWN"

    invoke-direct {v0, v1, v5}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INVALIDATE_UNKNOWN:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    const-string v1, "INVALIDATE_ALL"

    invoke-direct {v0, v1, v6}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INVALIDATE_ALL:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    const-string v1, "INFORM_REGISTRATION_STATUS"

    invoke-direct {v0, v1, v7}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INFORM_REGISTRATION_STATUS:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    const-string v1, "INFORM_REGISTRATION_FAILURE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INFORM_REGISTRATION_FAILURE:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    const-string v1, "REISSUE_REGISTRATIONS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->REISSUE_REGISTRATIONS:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    const-string v1, "INFORM_ERROR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INFORM_ERROR:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->READY:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INVALIDATE:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INVALIDATE_UNKNOWN:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INVALIDATE_ALL:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INFORM_REGISTRATION_STATUS:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INFORM_REGISTRATION_FAILURE:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->REISSUE_REGISTRATIONS:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INFORM_ERROR:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->$VALUES:[Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;
    .locals 1

    const-class v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    return-object v0
.end method

.method public static values()[Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->$VALUES:[Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    invoke-virtual {v0}, [Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    return-object v0
.end method
