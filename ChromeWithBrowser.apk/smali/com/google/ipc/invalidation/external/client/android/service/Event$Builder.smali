.class public Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;
.super Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->ordinal()I

    move-result v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;-><init>(ILandroid/os/Bundle;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;Lcom/google/ipc/invalidation/external/client/android/service/Event$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;-><init>(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/ipc/invalidation/external/client/android/service/Event;
    .locals 2

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Event;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->bundle:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Event;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/ipc/invalidation/external/client/android/service/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Event;

    move-result-object v0

    return-object v0
.end method

.method public setErrorInfo(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "error"

    new-instance v2, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;

    invoke-direct {v2, p1}, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;-><init>(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object p0
.end method

.method public setInvalidation(Lcom/google/ipc/invalidation/external/client/types/Invalidation;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;
    .locals 4

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "invalidation"

    new-instance v2, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;

    const/4 v3, 0x1

    invoke-direct {v2, p1, v3}, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;-><init>(Lcom/google/ipc/invalidation/external/client/types/Invalidation;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object p0
.end method

.method public setIsTransient(Z)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;
    .locals 4

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "isTransient"

    const/4 v2, 0x1

    new-array v2, v2, [Z

    const/4 v3, 0x0

    aput-boolean p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    return-object p0
.end method

.method public setPrefix([BI)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "prefix"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "prefixLength"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method

.method public setRegistrationState(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "registrationState"

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method
