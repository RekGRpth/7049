.class public Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;
.super Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;


# static fields
.field private static serviceClassName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "com.google.ipc.invalidation.ticl.a.o"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;->serviceClassName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;->serviceClassName:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request;->SERVICE_INTENT:Landroid/content/Intent;

    const-class v1, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService;

    sget-object v2, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;->serviceClassName:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;-><init>(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected asInterface(Landroid/os/IBinder;)Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService;
    .locals 1

    invoke-static {p1}, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic asInterface(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;->asInterface(Landroid/os/IBinder;)Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService;

    move-result-object v0

    return-object v0
.end method
