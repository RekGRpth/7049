.class public Lcom/google/ipc/invalidation/external/client/android/service/ListenerBinder;
.super Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 1

    const-class v0, Lcom/google/ipc/invalidation/external/client/android/service/ListenerService;

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;-><init>(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected asInterface(Landroid/os/IBinder;)Lcom/google/ipc/invalidation/external/client/android/service/ListenerService;
    .locals 1

    invoke-static {p1}, Lcom/google/ipc/invalidation/external/client/android/service/ListenerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/ipc/invalidation/external/client/android/service/ListenerService;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic asInterface(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/ListenerBinder;->asInterface(Landroid/os/IBinder;)Lcom/google/ipc/invalidation/external/client/android/service/ListenerService;

    move-result-object v0

    return-object v0
.end method
