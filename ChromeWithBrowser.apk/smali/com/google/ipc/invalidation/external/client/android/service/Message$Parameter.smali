.class public Lcom/google/ipc/invalidation/external/client/android/service/Message$Parameter;
.super Ljava/lang/Object;


# static fields
.field public static final ACCOUNT:Ljava/lang/String; = "account"

.field public static final ACK_TOKEN:Ljava/lang/String; = "ackToken"

.field public static final ACTION:Ljava/lang/String; = "action"

.field public static final AUTH_TYPE:Ljava/lang/String; = "authType"

.field public static final CLIENT:Ljava/lang/String; = "client"

.field public static final ERROR:Ljava/lang/String; = "error"

.field public static final OBJECT_ID:Ljava/lang/String; = "objectId"


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
