.class public Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;
.super Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->ordinal()I

    move-result v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;-><init>(ILandroid/os/Bundle;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;Lcom/google/ipc/invalidation/external/client/android/service/Request$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;-><init>(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/ipc/invalidation/external/client/android/service/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Request;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/ipc/invalidation/external/client/android/service/Request;
    .locals 2

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Request;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->bundle:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public setClientType(I)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "clientType"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "intent"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object p0
.end method

.method public setObjectIds(Ljava/util/Collection;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    new-instance v3, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;

    invoke-direct {v3, v0}, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;-><init>(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->bundle:Landroid/os/Bundle;

    const-string v2, "objectIdList"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object p0
.end method
