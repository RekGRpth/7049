.class public final Lcom/google/ipc/invalidation/external/client/android/service/Request;
.super Lcom/google/ipc/invalidation/external/client/android/service/Message;


# static fields
.field public static final SERVICE_INTENT:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.ipc.invalidation.SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request;->SERVICE_INTENT:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Message;-><init>(Landroid/os/Bundle;)V

    return-void
.end method

.method public static newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;
    .locals 2

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;-><init>(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;Lcom/google/ipc/invalidation/external/client/android/service/Request$1;)V

    return-object v0
.end method


# virtual methods
.method public final getAction()Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;
    .locals 2

    invoke-static {}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->values()[Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getActionOrdinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final getClientType()I
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Request;->parameters:Landroid/os/Bundle;

    const-string v1, "clientType"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Request;->parameters:Landroid/os/Bundle;

    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    return-object v0
.end method

.method public final getObjectIds()Ljava/util/Collection;
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Request;->parameters:Landroid/os/Bundle;

    const-string v1, "objectIdList"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;

    iget-object v0, v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;->objectId:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
