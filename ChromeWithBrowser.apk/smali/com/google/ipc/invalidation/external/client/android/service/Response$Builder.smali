.class public Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;
.super Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;


# direct methods
.method private constructor <init>(ILandroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;-><init>(ILandroid/os/Bundle;)V

    return-void
.end method

.method synthetic constructor <init>(ILandroid/os/Bundle;Lcom/google/ipc/invalidation/external/client/android/service/Response$1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;-><init>(ILandroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/ipc/invalidation/external/client/android/service/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Response;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/ipc/invalidation/external/client/android/service/Response;
    .locals 2

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Response;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->bundle:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Response;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public setException(Ljava/lang/Exception;)V
    .locals 1

    instance-of v0, p1, Lcom/google/ipc/invalidation/external/client/android/service/AndroidClientException;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/ipc/invalidation/external/client/android/service/AndroidClientException;

    iget v0, p1, Lcom/google/ipc/invalidation/external/client/android/service/AndroidClientException;->status:I

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setStatus(I)Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;

    iget-object v0, p1, Lcom/google/ipc/invalidation/external/client/android/service/AndroidClientException;->message:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setError(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setStatus(I)Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setError(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    goto :goto_0
.end method

.method public setStatus(I)Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "status"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method
