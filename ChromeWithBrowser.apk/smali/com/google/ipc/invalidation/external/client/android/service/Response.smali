.class public final Lcom/google/ipc/invalidation/external/client/android/service/Response;
.super Lcom/google/ipc/invalidation/external/client/android/service/Message;


# static fields
.field private static final logger:Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "Response"

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forTag(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Response;->logger:Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Message;-><init>(Landroid/os/Bundle;)V

    return-void
.end method

.method public static newBuilder(ILandroid/os/Bundle;)Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;
    .locals 2

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;-><init>(ILandroid/os/Bundle;Lcom/google/ipc/invalidation/external/client/android/service/Response$1;)V

    return-object v0
.end method


# virtual methods
.method public final getStatus()I
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Response;->parameters:Landroid/os/Bundle;

    const-string v1, "status"

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final warnOnFailure()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/service/Response;->getStatus()I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Response;->parameters:Landroid/os/Bundle;

    const-string v2, "error"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected status value:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/service/Response;->logger:Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    const-string v2, "Error from AndroidInvalidationService: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    return-void
.end method
