.class Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static isValidAndroidListenerState(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isValidRegistrationCommand(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isValidStartCommand(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static newAndroidListenerState(Lcom/google/protobuf/ByteString;ILjava/util/Map;Ljava/lang/Iterable;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;
    .locals 4

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;->a(I)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;

    move-result-object v2

    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    invoke-static {v0}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/ak;

    invoke-static {v1, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newRetryRegistrationState(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/ticl/ak;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;

    move-result-object v0

    return-object v0
.end method

.method static newDelayedRegisterCommand(Lcom/google/protobuf/ByteString;Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newDelayedRegistrationCommand(Lcom/google/protobuf/ByteString;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    return-object v0
.end method

.method private static newDelayedRegistrationCommand(Lcom/google/protobuf/ByteString;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->b(Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    return-object v0
.end method

.method static newDelayedUnregisterCommand(Lcom/google/protobuf/ByteString;Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newDelayedRegistrationCommand(Lcom/google/protobuf/ByteString;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    return-object v0
.end method

.method static newRegisterCommand(Lcom/google/protobuf/ByteString;Ljava/lang/Iterable;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newRegistrationCommand(Lcom/google/protobuf/ByteString;Ljava/lang/Iterable;Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    return-object v0
.end method

.method private static newRegistrationCommand(Lcom/google/protobuf/ByteString;Ljava/lang/Iterable;Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->b(Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    invoke-static {v0}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    return-object v0
.end method

.method static newRetryRegistrationState(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/ticl/ak;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState$Builder;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/ak;->a()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState;

    move-result-object v0

    return-object v0
.end method

.method static newStartCommand(ILcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand$Builder;->a(I)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;

    move-result-object v0

    return-object v0
.end method

.method static newUnregisterCommand(Lcom/google/protobuf/ByteString;Ljava/lang/Iterable;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newRegistrationCommand(Lcom/google/protobuf/ByteString;Ljava/lang/Iterable;Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    return-object v0
.end method
