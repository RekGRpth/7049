.class final Lcom/google/ipc/invalidation/ticl/Q;
.super Lcom/google/ipc/invalidation/b/e;


# static fields
.field static final a:[B


# instance fields
.field private b:Lcom/google/ipc/invalidation/ticl/k;

.field private final c:Lcom/google/ipc/invalidation/ticl/ac;

.field private d:Lcom/google/ipc/invalidation/ticl/H;

.field private final e:Ljava/util/Map;

.field private final f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/ipc/invalidation/ticl/Q;->a:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/ac;Lcom/google/a/b/B;Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/e;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->e:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/Q;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/Q;->c:Lcom/google/ipc/invalidation/ticl/ac;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ab;

    invoke-direct {v0, p3}, Lcom/google/ipc/invalidation/ticl/ab;-><init>(Lcom/google/a/b/B;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    if-nez p4, :cond_1

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/Q;->b()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->d:Lcom/google/ipc/invalidation/ticl/H;

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p4}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;->g()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->d:Lcom/google/ipc/invalidation/ticl/H;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    invoke-virtual {p4}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/ticl/k;->a(Ljava/util/Collection;)Ljava/util/Collection;

    invoke-virtual {p4}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/Q;->e:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v3

    invoke-static {v3}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method final a([BI)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree$Builder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/ticl/k;->c()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree$Builder;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;

    move-result-object v0

    return-object v0
.end method

.method final a()Ljava/util/Collection;
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/ticl/k;->a()I

    move-result v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/Q;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/ticl/k;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-object v1
.end method

.method final a(Ljava/util/Collection;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)Ljava/util/Collection;
    .locals 3

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/Q;->e:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    invoke-interface {v2, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    invoke-interface {v0, p1}, Lcom/google/ipc/invalidation/ticl/k;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    invoke-interface {v0, p1}, Lcom/google/ipc/invalidation/ticl/k;->b(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_1
.end method

.method final a(Ljava/util/List;)Ljava/util/List;
    .locals 10

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v5

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/Q;->e:Ljava/util/Map;

    invoke-static {v5}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    invoke-interface {v6, v5}, Lcom/google/ipc/invalidation/ticl/k;->a(Ljava/lang/Object;)Z

    move-result v6

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    move-result-object v0

    sget-object v7, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    if-ne v0, v7, :cond_0

    move v0, v2

    :goto_1
    xor-int v7, v0, v6

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    invoke-interface {v7, v5}, Lcom/google/ipc/invalidation/ticl/k;->b(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/google/ipc/invalidation/ticl/Q;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v8, Lcom/google/ipc/invalidation/ticl/ad;->h:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v7, v8}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    iget-object v7, p0, Lcom/google/ipc/invalidation/ticl/Q;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v8, "Ticl discrepancy detected: registered = %s, requested = %s. Removing %s from requested"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v9, v1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v9, v2

    const/4 v0, 0x2

    invoke-static {v5}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v9, v0

    invoke-interface {v7, v8, v9}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    invoke-interface {v0, v5}, Lcom/google/ipc/invalidation/ticl/k;->b(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v6, "Removing %s from committed"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v5}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v7, v1

    invoke-interface {v0, v6, v7}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    goto :goto_2

    :cond_2
    return-object v3

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;)Ljava/util/Set;
    .locals 5

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->d:Lcom/google/ipc/invalidation/ticl/H;

    :cond_0
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/Q;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v2, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(I)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v1

    check-cast v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    if-ne v0, v4, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    move-object v0, v2

    :goto_2
    return-object v0

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Lcom/google/ipc/invalidation/b/i;)V
    .locals 4

    const-string v0, "Last known digest: %s, Requested regs: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/Q;->d:Lcom/google/ipc/invalidation/ticl/H;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/google/ipc/invalidation/b/i;

    return-void
.end method

.method final b()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/ticl/k;->a()I

    move-result v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    invoke-interface {v1}, Lcom/google/ipc/invalidation/ticl/k;->b()[B

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;

    move-result-object v0

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v0

    return-object v0
.end method

.method final c()Z
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->d:Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/Q;->b()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;
    .locals 5

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->d:Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;)Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->b:Lcom/google/ipc/invalidation/ticl/k;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/Q;->a:[B

    invoke-interface {v0}, Lcom/google/ipc/invalidation/ticl/k;->c()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;->a(Ljava/lang/Iterable;)Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/Q;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v1

    check-cast v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    if-ne v0, v4, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;)Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;->c()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    move-result-object v0

    return-object v0
.end method
