.class public final Lcom/google/ipc/invalidation/ticl/R;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

.field private b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/R;->b:Ljava/lang/Object;

    sget-object v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    return-void
.end method

.method constructor <init>(Lcom/google/protos/ipc/invalidation/Client$RunStateP;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/R;->b:Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->f()Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/R;->b:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    sget-object v4, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    if-ne v3, v4, :cond_0

    :goto_0
    const-string v1, "Cannot start: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    aput-object v5, v3, v4

    invoke-static {v0, v1, v3}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b()V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/R;->b:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    sget-object v4, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    if-ne v3, v4, :cond_0

    :goto_0
    const-string v1, "Cannot stop: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    aput-object v5, v3, v4

    invoke-static {v0, v1, v3}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->c:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final c()Z
    .locals 3

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/R;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    sget-object v2, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()Z
    .locals 3

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/R;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    sget-object v2, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->c:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()Lcom/google/protos/ipc/invalidation/Client$RunStateP;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->newBuilder()Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->c()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<RunState: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/R;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
