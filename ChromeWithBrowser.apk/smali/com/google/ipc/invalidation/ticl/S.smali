.class public final Lcom/google/ipc/invalidation/ticl/S;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;


# instance fields
.field private final a:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

.field private b:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/S;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    return-void
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/S;)Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/S;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    return-object v0
.end method


# virtual methods
.method public final deleteKey(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/S;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/X;

    invoke-direct {v1, p0, p2}, Lcom/google/ipc/invalidation/ticl/X;-><init>(Lcom/google/ipc/invalidation/ticl/S;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    invoke-interface {v0, p1, v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;->deleteKey(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    return-void
.end method

.method public final readAllKeys(Lcom/google/ipc/invalidation/external/client/types/Callback;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/S;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/Z;

    invoke-direct {v1, p0, p1}, Lcom/google/ipc/invalidation/ticl/Z;-><init>(Lcom/google/ipc/invalidation/ticl/S;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;->readAllKeys(Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    return-void
.end method

.method public final readKey(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/S;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/V;

    invoke-direct {v1, p0, p2}, Lcom/google/ipc/invalidation/ticl/V;-><init>(Lcom/google/ipc/invalidation/ticl/S;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    invoke-interface {v0, p1, v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;->readKey(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    return-void
.end method

.method public final setSystemResources(Lcom/google/ipc/invalidation/external/client/SystemResources;)V
    .locals 1

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/S;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    return-void
.end method

.method public final writeKey(Ljava/lang/String;[BLcom/google/ipc/invalidation/external/client/types/Callback;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/S;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/T;

    invoke-direct {v1, p0, p3}, Lcom/google/ipc/invalidation/ticl/T;-><init>(Lcom/google/ipc/invalidation/ticl/S;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    invoke-interface {v0, p1, p2, v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;->writeKey(Ljava/lang/String;[BLcom/google/ipc/invalidation/external/client/types/Callback;)V

    return-void
.end method
