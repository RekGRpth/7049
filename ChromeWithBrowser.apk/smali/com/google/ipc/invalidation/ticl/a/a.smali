.class public abstract Lcom/google/ipc/invalidation/ticl/a/a;
.super Landroid/app/Service;


# static fields
.field private static final b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;


# instance fields
.field final a:Ljava/lang/Object;

.field private final c:Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService$Stub;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "InvService"

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forTag(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/a;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/b;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/a/b;-><init>(Lcom/google/ipc/invalidation/ticl/a/a;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/a;->c:Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService$Stub;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/a;->a:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/a;->d:Z

    return-void
.end method

.method protected static a(Lcom/google/ipc/invalidation/external/client/android/service/ListenerService;Lcom/google/ipc/invalidation/external/client/android/service/Event;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    :try_start_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/a;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Sending %s event"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getAction()Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {p0, v1, v0}, Lcom/google/ipc/invalidation/external/client/android/service/ListenerService;->handleEvent(Landroid/os/Bundle;Landroid/os/Bundle;)V

    new-instance v1, Lcom/google/ipc/invalidation/external/client/android/service/Response;

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/external/client/android/service/Response;->warnOnFailure()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/a;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Unable to send event"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to send event"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected final a(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 9

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/a;->d:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/a;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Dropping bundle since not created: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Request;

    invoke-direct {v0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getActionOrdinal()I

    move-result v2

    invoke-static {v2, p2}, Lcom/google/ipc/invalidation/external/client/android/service/Response;->newBuilder(ILandroid/os/Bundle;)Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getAction()Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    move-result-object v3

    sget-object v4, Lcom/google/ipc/invalidation/ticl/a/a;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v5, "%s request from %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getClientKey()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-interface {v4, v5, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v4, Lcom/google/ipc/invalidation/ticl/a/c;->a:[I

    invoke-virtual {v3}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown action:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/a;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Client request error"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-interface {v3, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setStatus(I)Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setException(Ljava/lang/Exception;)V

    :goto_1
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_0
    :try_start_3
    invoke-virtual {p0, v0, v2}, Lcom/google/ipc/invalidation/ticl/a/a;->a(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0, v0, v2}, Lcom/google/ipc/invalidation/ticl/a/a;->b(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0, v0, v2}, Lcom/google/ipc/invalidation/ticl/a/a;->c(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0, v0, v2}, Lcom/google/ipc/invalidation/ticl/a/a;->d(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0, v0, v2}, Lcom/google/ipc/invalidation/ticl/a/a;->e(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V

    goto :goto_1

    :pswitch_5
    invoke-virtual {p0, v0, v2}, Lcom/google/ipc/invalidation/ticl/a/a;->f(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V

    goto :goto_1

    :pswitch_6
    invoke-virtual {p0, v0, v2}, Lcom/google/ipc/invalidation/ticl/a/a;->g(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V

    goto :goto_1

    :pswitch_7
    invoke-virtual {p0, v0, v2}, Lcom/google/ipc/invalidation/ticl/a/a;->h(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected abstract a(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
.end method

.method protected abstract b(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
.end method

.method protected abstract c(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
.end method

.method protected abstract d(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
.end method

.method protected abstract e(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
.end method

.method protected abstract f(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
.end method

.method protected abstract g(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
.end method

.method protected abstract h(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/a;->c:Lcom/google/ipc/invalidation/external/client/android/service/InvalidationService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/a;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "onCreate: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/a;->d:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onDestroy()V
    .locals 6

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/a;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "onDestroy: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/a;->d:Z

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
