.class final Lcom/google/ipc/invalidation/ticl/a/d;
.super Lcom/google/ipc/invalidation/ticl/a/j;

# interfaces
.implements Lcom/google/ipc/invalidation/ticl/aj;


# static fields
.field private static final a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

.field private static b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private static c:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static k:Z


# instance fields
.field private final d:Lcom/google/ipc/invalidation/ticl/a/l;

.field private final e:Landroid/content/Context;

.field private f:Lcom/google/ipc/invalidation/external/client/SystemResources;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

.field private j:Ljava/util/List;

.field private l:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "InvChannel"

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forTag(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/d;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;->a:Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;->a()I

    move-result v0

    invoke-static {v0, v1}, Lcom/google/android/b/c;->a(II)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/d;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/d;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    sput-boolean v1, Lcom/google/ipc/invalidation/ticl/a/d;->k:Z

    return-void
.end method

.method constructor <init>(Lcom/google/ipc/invalidation/ticl/a/l;Lorg/apache/http/client/HttpClient;Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/a/l;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/a/l;->c()Lcom/google/ipc/invalidation/ticl/a/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/o;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Lcom/google/ipc/invalidation/ticl/a/j;-><init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->j:Ljava/util/List;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->l:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/l;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->d:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-static {p3}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->e:Landroid/content/Context;

    return-void
.end method

.method static a(Landroid/content/Context;)Landroid/net/http/AndroidHttpClient;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/a/d;)Lcom/google/ipc/invalidation/external/client/SystemResources;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->f:Lcom/google/ipc/invalidation/external/client/SystemResources;

    return-object v0
.end method

.method static a(Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/b/d;Lcom/google/ipc/invalidation/ticl/a/h;)V
    .locals 4

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/d;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Running %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/i;

    invoke-direct {v0, p2, p1, p0}, Lcom/google/ipc/invalidation/ticl/a/i;-><init>(Lcom/google/ipc/invalidation/ticl/a/h;Lcom/google/ipc/invalidation/b/d;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;)V

    invoke-virtual {p2, v0}, Lcom/google/ipc/invalidation/ticl/a/h;->a(Lcom/google/ipc/invalidation/ticl/a/i;)V

    return-void
.end method

.method static synthetic e()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/d;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-object v0
.end method

.method private f()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->g:Ljava/lang/String;

    :goto_0
    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/b/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private declared-synchronized g()V
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/d;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/d;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Enabling network endpoint: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/d;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->i:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->i:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;->onOnlineStatusChange(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->j:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/ticl/a/d;->sendMessage([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->j:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    monitor-exit p0

    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->h:Ljava/lang/String;

    return-object v0
.end method

.method final declared-synchronized a(Lcom/google/ipc/invalidation/ticl/a/i;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->d:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->c()Lcom/google/ipc/invalidation/ticl/a/o;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/d;->d:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/l;->a()Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/d;->d:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/l;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-instance v4, Lcom/google/ipc/invalidation/ticl/a/f;

    invoke-direct {v4, p0, p1}, Lcom/google/ipc/invalidation/ticl/a/f;-><init>(Lcom/google/ipc/invalidation/ticl/a/d;Lcom/google/ipc/invalidation/ticl/a/i;)V

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/d;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Auth token request already pending"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/a/i;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Ljava/lang/String;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/d;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Auth token received fo %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/a/d;->d:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-virtual {v4}, Lcom/google/ipc/invalidation/ticl/a/l;->getClientKey()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/d;->h:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/d;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a([B)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    :try_start_0
    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->a([B)Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/d;->d:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/l;->getClientKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/ipc/invalidation/ticl/a/d;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Deliver to %s message %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v0, v4, v1

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/d;->i:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->c()[B

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;->onMessageReceived([B)V

    :goto_0
    return-void

    :cond_0
    sget-object v2, Lcom/google/ipc/invalidation/ticl/a/d;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Not delivering message due to key mismatch: %s vs %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/d;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/d;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Failed decoding AddressedAndroidMessage as C2DM payload"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final declared-synchronized b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/d;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final c()Ljava/lang/String;
    .locals 4

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/d;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/d;->d:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/l;->getClientKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/d;->d:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/l;->c()Lcom/google/ipc/invalidation/ticl/a/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/o;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/d;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId;->b()[B

    move-result-object v0

    const/16 v1, 0xb

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final d()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->f:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized sendMessage([B)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/d;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/a/d;->h:Ljava/lang/String;

    if-nez v3, :cond_5

    :cond_0
    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/a/d;->j:Ljava/util/List;

    if-nez v3, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/ipc/invalidation/ticl/a/d;->j:Ljava/util/List;

    :cond_1
    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/d;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Buffering outbound message: hasRegId: %s, hasAuthToken: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    if-eqz v2, :cond_2

    move v2, v0

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v2, 0x1

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/a/d;->h:Ljava/lang/String;

    if-eqz v6, :cond_3

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-interface {v3, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    monitor-exit p0

    return-void

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    :try_start_1
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/d;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Exceeded maximum number of buffered messages, dropping outbound message"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    :try_start_2
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/a/g;

    invoke-direct {v1, p0, p1}, Lcom/google/ipc/invalidation/ticl/a/g;-><init>(Lcom/google/ipc/invalidation/ticl/a/d;[B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public final setListener(Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;)V
    .locals 1

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/d;->i:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    return-void
.end method

.method public final setSystemResources(Lcom/google/ipc/invalidation/external/client/SystemResources;)V
    .locals 5

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/d;->f:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    new-instance v1, Lcom/google/ipc/invalidation/b/d;

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    const/16 v3, 0x3e8

    const v4, 0xa8c0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/ipc/invalidation/b/d;-><init>(Ljava/util/Random;II)V

    new-instance v2, Lcom/google/ipc/invalidation/ticl/a/h;

    invoke-direct {v2, p0}, Lcom/google/ipc/invalidation/ticl/a/h;-><init>(Lcom/google/ipc/invalidation/ticl/a/d;)V

    invoke-static {v0, v1, v2}, Lcom/google/ipc/invalidation/ticl/a/d;->a(Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/b/d;Lcom/google/ipc/invalidation/ticl/a/h;)V

    return-void
.end method
