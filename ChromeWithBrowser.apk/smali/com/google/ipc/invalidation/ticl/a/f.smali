.class final Lcom/google/ipc/invalidation/ticl/a/f;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field private synthetic a:Lcom/google/ipc/invalidation/ticl/a/i;

.field private synthetic b:Lcom/google/ipc/invalidation/ticl/a/d;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/a/d;Lcom/google/ipc/invalidation/ticl/a/i;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->b:Lcom/google/ipc/invalidation/ticl/a/d;

    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/f;->a:Lcom/google/ipc/invalidation/ticl/a/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run(Landroid/accounts/AccountManagerFuture;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/a/d;->e()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string v2, "Token acquisition requires user login"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->a:Lcom/google/ipc/invalidation/ticl/a/i;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/i;->a()V

    :cond_0
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->b:Lcom/google/ipc/invalidation/ticl/a/d;

    const-string v2, "authtoken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/ipc/invalidation/ticl/a/d;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/a/d;->e()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string v2, "Auth cancelled"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/a/d;->e()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string v2, "Auth error acquiring token"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/f;->a:Lcom/google/ipc/invalidation/ticl/a/i;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/i;->b()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/a/d;->e()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string v2, "IO Exception acquiring token"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/f;->a:Lcom/google/ipc/invalidation/ticl/a/i;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/i;->b()V

    goto :goto_0
.end method
