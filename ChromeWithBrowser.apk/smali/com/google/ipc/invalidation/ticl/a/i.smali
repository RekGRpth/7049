.class Lcom/google/ipc/invalidation/ticl/a/i;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/google/ipc/invalidation/ticl/a/h;

.field final synthetic b:Lcom/google/ipc/invalidation/b/d;

.field final synthetic c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/a/h;Lcom/google/ipc/invalidation/b/d;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/i;->a:Lcom/google/ipc/invalidation/ticl/a/h;

    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/i;->b:Lcom/google/ipc/invalidation/b/d;

    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/a/i;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/a/d;->e()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "%s succeeded"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/a/i;->a:Lcom/google/ipc/invalidation/ticl/a/h;

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public b()V
    .locals 6

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/i;->b:Lcom/google/ipc/invalidation/b/d;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/d;->c()I

    move-result v0

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/a/d;->e()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string v2, "%s failed, retrying after %s ms"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/i;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    new-instance v2, Lcom/google/ipc/invalidation/ticl/a/e;

    invoke-direct {v2, p0}, Lcom/google/ipc/invalidation/ticl/a/e;-><init>(Lcom/google/ipc/invalidation/ticl/a/i;)V

    invoke-interface {v1, v0, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->schedule(ILjava/lang/Runnable;)V

    return-void
.end method
