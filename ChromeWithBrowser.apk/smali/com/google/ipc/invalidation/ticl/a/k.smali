.class public final Lcom/google/ipc/invalidation/ticl/a/k;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

.field private static b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;


# instance fields
.field private final c:Lcom/google/ipc/invalidation/ticl/a/o;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "InvClientManager"

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forTag(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/k;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/l;->a()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/k;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    return-void
.end method

.method constructor <init>(Lcom/google/ipc/invalidation/ticl/a/o;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/k;->d:Ljava/util/Map;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/k;->e:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/k;->c:Lcom/google/ipc/invalidation/ticl/a/o;

    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/r;
    .locals 2

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/k;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/r;

    invoke-direct {v0, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/r;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;
    .locals 6

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/k;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/k;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/l;

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/k;->c:Lcom/google/ipc/invalidation/ticl/a/o;

    invoke-direct {p0, v2, p1}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/r;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/r;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/k;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Client %s loaded from disk"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-interface {v0, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/l;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/a/k;->c:Lcom/google/ipc/invalidation/ticl/a/o;

    sget-object v4, Lcom/google/ipc/invalidation/ticl/a/k;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-direct {v0, v3, v2, v4}, Lcom/google/ipc/invalidation/ticl/a/l;-><init>(Lcom/google/ipc/invalidation/ticl/a/o;Lcom/google/ipc/invalidation/ticl/a/r;Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)V

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/k;->d:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method final a()I
    .locals 2

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/k;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/k;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;
    .locals 2

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/k;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/ticl/a/k;->c(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Ljava/lang/String;ILandroid/accounts/Account;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/ipc/invalidation/ticl/a/l;
    .locals 6

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/k;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/ticl/a/k;->c(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/AndroidClientException;

    const/4 v2, 0x1

    const-string v3, "Account does not match existing client"

    invoke-direct {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidClientException;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1

    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/k;->c:Lcom/google/ipc/invalidation/ticl/a/o;

    invoke-direct {p0, v0, p1}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/r;

    move-result-object v2

    invoke-virtual {v2, p2, p3, p4, p5}, Lcom/google/ipc/invalidation/ticl/a/r;->a(ILandroid/accounts/Account;Ljava/lang/String;Landroid/content/Intent;)V

    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/l;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/a/k;->c:Lcom/google/ipc/invalidation/ticl/a/o;

    sget-object v4, Lcom/google/ipc/invalidation/ticl/a/k;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-direct {v0, v3, v2, v4}, Lcom/google/ipc/invalidation/ticl/a/l;-><init>(Lcom/google/ipc/invalidation/ticl/a/o;Lcom/google/ipc/invalidation/ticl/a/r;Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)V

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/k;->d:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lcom/google/ipc/invalidation/ticl/a/k;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Client %s created"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method final b()V
    .locals 3

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/k;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/k;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->d()Lcom/google/ipc/invalidation/ticl/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/d;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 2

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/k;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/k;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/l;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->release()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final c()V
    .locals 3

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/k;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/k;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/k;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final d()Z
    .locals 3

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/k;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/k;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
