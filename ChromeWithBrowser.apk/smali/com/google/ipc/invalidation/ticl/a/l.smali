.class final Lcom/google/ipc/invalidation/ticl/a/l;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;


# static fields
.field private static final a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;


# instance fields
.field private final b:Lcom/google/ipc/invalidation/ticl/a/o;

.field private final c:Ljava/lang/String;

.field private d:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

.field private final e:Lcom/google/ipc/invalidation/ticl/a/m;

.field private final f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

.field private final g:Lcom/google/ipc/invalidation/ticl/a/d;

.field private final h:Lcom/google/ipc/invalidation/external/client/SystemResources;

.field private final i:Landroid/net/http/AndroidHttpClient;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "InvClientProxy"

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forTag(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/l;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-void
.end method

.method constructor <init>(Lcom/google/ipc/invalidation/ticl/a/o;Lcom/google/ipc/invalidation/ticl/a/r;Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)V
    .locals 10

    const/4 v8, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/l;->b:Lcom/google/ipc/invalidation/ticl/a/o;

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/ticl/a/r;->a()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->c:Ljava/lang/String;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/m;

    invoke-direct {v0, p0, v8}, Lcom/google/ipc/invalidation/ticl/a/m;-><init>(Lcom/google/ipc/invalidation/ticl/a/l;B)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->e:Lcom/google/ipc/invalidation/ticl/a/m;

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/a/d;->a(Landroid/content/Context;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->i:Landroid/net/http/AndroidHttpClient;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/d;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/l;->i:Landroid/net/http/AndroidHttpClient;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/ipc/invalidation/ticl/a/d;-><init>(Lcom/google/ipc/invalidation/ticl/a/l;Lorg/apache/http/client/HttpClient;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->g:Lcom/google/ipc/invalidation/ticl/a/d;

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/a/l;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/a/l;->g:Lcom/google/ipc/invalidation/ticl/a/d;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;

    invoke-static {v5}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forPrefix(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v1

    new-instance v2, Lcom/google/ipc/invalidation/ticl/a/p;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "ticl"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v8}, Lcom/google/ipc/invalidation/ticl/a/p;-><init>(Ljava/lang/String;B)V

    new-instance v3, Lcom/google/ipc/invalidation/ticl/a/p;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ticl-listener"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5, v8}, Lcom/google/ipc/invalidation/ticl/a/p;-><init>(Ljava/lang/String;B)V

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Android-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->setPlatform(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/SystemResourcesBuilder;->build()Lcom/google/ipc/invalidation/external/client/SystemResources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->h:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/ticl/a/r;->a()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/ipc/invalidation/ticl/a/l;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/l;->h:Lcom/google/ipc/invalidation/external/client/SystemResources;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->j()I

    move-result v3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    iget-object v7, p0, Lcom/google/ipc/invalidation/ticl/a/l;->e:Lcom/google/ipc/invalidation/ticl/a/m;

    if-nez p3, :cond_0

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/l;->a()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    :goto_0
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v5

    new-instance v2, Ljava/util/Random;

    invoke-interface {v1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v8

    invoke-direct {v2, v8, v9}, Ljava/util/Random;-><init>(J)V

    new-instance v0, Lcom/google/ipc/invalidation/ticl/x;

    invoke-direct/range {v0 .. v7}, Lcom/google/ipc/invalidation/ticl/x;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->d:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    return-void

    :cond_0
    invoke-static {p3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/a/l;)Lcom/google/ipc/invalidation/ticl/a/o;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->b:Lcom/google/ipc/invalidation/ticl/a/o;

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x0

    const-string v1, "unknown"

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/ipc/invalidation/ticl/a/l;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Cannot retrieve current application version: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/ipc/invalidation/ticl/a/l;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    return-object v0
.end method

.method static synthetic c(Lcom/google/ipc/invalidation/ticl/a/l;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/l;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 3

    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/l;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/l;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->n()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final acknowledge(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->d:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-interface {v0, p1}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->acknowledge(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final c()Lcom/google/ipc/invalidation/ticl/a/o;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->b:Lcom/google/ipc/invalidation/ticl/a/o;

    return-object v0
.end method

.method final d()Lcom/google/ipc/invalidation/ticl/a/d;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->g:Lcom/google/ipc/invalidation/ticl/a/d;

    return-object v0
.end method

.method public final destroy()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/l;->stop()V

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->h:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getStorage()Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/r;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/r;->c()V

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/a/o;->a()Lcom/google/ipc/invalidation/ticl/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/l;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/k;->b(Ljava/lang/String;)V

    return-void
.end method

.method final e()Lcom/google/ipc/invalidation/ticl/a/r;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->h:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getStorage()Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/r;

    return-object v0
.end method

.method final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->j:Z

    return v0
.end method

.method public final getClientKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final register(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->d:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-interface {v0, p1}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->register(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    return-void
.end method

.method public final register(Ljava/util/Collection;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->d:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-interface {v0, p1}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->register(Ljava/util/Collection;)V

    return-void
.end method

.method public final release()V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->e:Lcom/google/ipc/invalidation/ticl/a/m;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/m;->a()V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->h:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->h:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->stop()V

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->i:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    return-void
.end method

.method public final start()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->j:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/l;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Not starting Ticl since already started"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->h:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->start()V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->d:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->j:Z

    goto :goto_0
.end method

.method public final stop()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->j:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/l;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Not stopping Ticl since already stopped"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->j:Z

    invoke-static {v0}, Lcom/google/a/a/a;->b(Z)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->d:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->stop()V

    iput-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/l;->j:Z

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->h:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->stop()V

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/a/o;->a()Lcom/google/ipc/invalidation/ticl/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/l;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/k;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final unregister(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->d:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-interface {v0, p1}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->unregister(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    return-void
.end method

.method public final unregister(Ljava/util/Collection;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/l;->d:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-interface {v0, p1}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->unregister(Ljava/util/Collection;)V

    return-void
.end method
