.class final Lcom/google/ipc/invalidation/ticl/a/m;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/InvalidationListener;


# instance fields
.field final synthetic a:Lcom/google/ipc/invalidation/ticl/a/l;

.field private b:Lcom/google/ipc/invalidation/external/client/android/service/ListenerBinder;


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/a/l;)V
    .locals 4

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/m;->a:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/ListenerBinder;

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/a/l;->a(Lcom/google/ipc/invalidation/ticl/a/l;)Lcom/google/ipc/invalidation/ticl/a/o;

    move-result-object v1

    sget-object v2, Lcom/google/ipc/invalidation/external/client/android/service/Event;->LISTENER_INTENT:Landroid/content/Intent;

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/a/l;->b(Lcom/google/ipc/invalidation/ticl/a/l;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->t()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/android/service/ListenerBinder;-><init>(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/m;->b:Lcom/google/ipc/invalidation/external/client/android/service/ListenerBinder;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ipc/invalidation/ticl/a/l;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/ticl/a/m;-><init>(Lcom/google/ipc/invalidation/ticl/a/l;)V

    return-void
.end method

.method private a(Lcom/google/ipc/invalidation/external/client/android/service/Event;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/m;->b:Lcom/google/ipc/invalidation/external/client/android/service/ListenerBinder;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/a/n;

    invoke-direct {v1, p0, p1}, Lcom/google/ipc/invalidation/ticl/a/n;-><init>(Lcom/google/ipc/invalidation/ticl/a/m;Lcom/google/ipc/invalidation/external/client/android/service/Event;)V

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/ListenerBinder;->runWhenBound(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$BoundWork;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/m;->b:Lcom/google/ipc/invalidation/external/client/android/service/ListenerBinder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/ListenerBinder;->release()V

    return-void
.end method

.method public final informError(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INFORM_ERROR:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/m;->a:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/l;->c(Lcom/google/ipc/invalidation/ticl/a/l;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0, p2}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setErrorInfo(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Event;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/a/m;->a(Lcom/google/ipc/invalidation/external/client/android/service/Event;)V

    return-void
.end method

.method public final informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INFORM_REGISTRATION_FAILURE:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/m;->a:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/l;->c(Lcom/google/ipc/invalidation/ticl/a/l;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0, p2}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setObjectId(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0, p3}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setIsTransient(Z)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setError(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Event;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/a/m;->a(Lcom/google/ipc/invalidation/external/client/android/service/Event;)V

    return-void
.end method

.method public final informRegistrationStatus(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INFORM_REGISTRATION_STATUS:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/m;->a:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/l;->c(Lcom/google/ipc/invalidation/ticl/a/l;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0, p2}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setObjectId(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0, p3}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setRegistrationState(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Event;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/a/m;->a(Lcom/google/ipc/invalidation/external/client/android/service/Event;)V

    return-void
.end method

.method public final invalidate(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/Invalidation;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INVALIDATE:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/m;->a:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/l;->c(Lcom/google/ipc/invalidation/ticl/a/l;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0, p2}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setInvalidation(Lcom/google/ipc/invalidation/external/client/types/Invalidation;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setAckHandle(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Event;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/a/m;->a(Lcom/google/ipc/invalidation/external/client/android/service/Event;)V

    return-void
.end method

.method public final invalidateAll(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INVALIDATE_ALL:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/m;->a:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/l;->c(Lcom/google/ipc/invalidation/ticl/a/l;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0, p2}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setAckHandle(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Event;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/a/m;->a(Lcom/google/ipc/invalidation/external/client/android/service/Event;)V

    return-void
.end method

.method public final invalidateUnknownVersion(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->INVALIDATE_UNKNOWN:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/m;->a:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/l;->c(Lcom/google/ipc/invalidation/ticl/a/l;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0, p2}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setObjectId(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0, p3}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setAckHandle(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Event;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/a/m;->a(Lcom/google/ipc/invalidation/external/client/android/service/Event;)V

    return-void
.end method

.method public final ready(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->READY:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/m;->a:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/l;->c(Lcom/google/ipc/invalidation/ticl/a/l;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Event;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/a/m;->a(Lcom/google/ipc/invalidation/external/client/android/service/Event;)V

    return-void
.end method

.method public final reissueRegistrations(Lcom/google/ipc/invalidation/external/client/InvalidationClient;[BI)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->REISSUE_REGISTRATIONS:Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/m;->a:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/l;->c(Lcom/google/ipc/invalidation/ticl/a/l;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    invoke-virtual {v0, p2, p3}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->setPrefix([BI)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Event;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/a/m;->a(Lcom/google/ipc/invalidation/external/client/android/service/Event;)V

    return-void
.end method
