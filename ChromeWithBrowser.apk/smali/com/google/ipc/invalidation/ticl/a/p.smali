.class final Lcom/google/ipc/invalidation/ticl/a/p;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;


# instance fields
.field private a:Lcom/google/ipc/invalidation/external/client/SystemResources;

.field private final b:Ljava/util/concurrent/ScheduledExecutorService;

.field private c:Ljava/lang/Thread;

.field private final d:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/p;->b:Ljava/util/concurrent/ScheduledExecutorService;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/p;->c:Ljava/lang/Thread;

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/p;->d:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/ticl/a/p;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/a/p;)Ljava/lang/Thread;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/p;->c:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/a/p;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/p;->c:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic b(Lcom/google/ipc/invalidation/ticl/a/p;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/p;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/ipc/invalidation/ticl/a/p;)Lcom/google/ipc/invalidation/external/client/SystemResources;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/p;->a:Lcom/google/ipc/invalidation/external/client/SystemResources;

    return-object v0
.end method


# virtual methods
.method public final getCurrentTimeMs()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public final isRunningOnThread()Z
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/p;->c:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/p;->c:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final schedule(ILjava/lang/Runnable;)V
    .locals 5

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/p;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/a/q;

    const-string v2, "AndroidScheduler"

    invoke-direct {v1, p0, v2, p2, p1}, Lcom/google/ipc/invalidation/ticl/a/q;-><init>(Lcom/google/ipc/invalidation/ticl/a/p;Ljava/lang/String;Ljava/lang/Runnable;I)V

    int-to-long v2, p1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    return-void
.end method

.method public final setSystemResources(Lcom/google/ipc/invalidation/external/client/SystemResources;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/p;->a:Lcom/google/ipc/invalidation/external/client/SystemResources;

    return-void
.end method
