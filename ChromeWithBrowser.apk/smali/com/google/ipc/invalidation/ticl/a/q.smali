.class final Lcom/google/ipc/invalidation/ticl/a/q;
.super Lcom/google/ipc/invalidation/b/g;


# instance fields
.field private synthetic a:Ljava/lang/Runnable;

.field private synthetic b:I

.field private synthetic c:Lcom/google/ipc/invalidation/ticl/a/p;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/a/p;Ljava/lang/String;Ljava/lang/Runnable;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/q;->c:Lcom/google/ipc/invalidation/ticl/a/p;

    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/a/q;->a:Ljava/lang/Runnable;

    iput p4, p0, Lcom/google/ipc/invalidation/ticl/a/q;->b:I

    invoke-direct {p0, p2}, Lcom/google/ipc/invalidation/b/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/q;->c:Lcom/google/ipc/invalidation/ticl/a/p;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/p;->a(Lcom/google/ipc/invalidation/ticl/a/p;)Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/q;->c:Lcom/google/ipc/invalidation/ticl/a/p;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/p;->a(Lcom/google/ipc/invalidation/ticl/a/p;Ljava/lang/Thread;)Ljava/lang/Thread;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/q;->c:Lcom/google/ipc/invalidation/ticl/a/p;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/p;->a(Lcom/google/ipc/invalidation/ticl/a/p;)Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/q;->c:Lcom/google/ipc/invalidation/ticl/a/p;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/p;->b(Lcom/google/ipc/invalidation/ticl/a/p;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/q;->c:Lcom/google/ipc/invalidation/ticl/a/p;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/p;->c(Lcom/google/ipc/invalidation/ticl/a/p;)Lcom/google/ipc/invalidation/external/client/SystemResources;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/q;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/q;->c:Lcom/google/ipc/invalidation/ticl/a/p;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/p;->c(Lcom/google/ipc/invalidation/ticl/a/p;)Lcom/google/ipc/invalidation/external/client/SystemResources;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "Not running on internal thread since resources not started %s, %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/ipc/invalidation/ticl/a/q;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/a/q;->a:Ljava/lang/Runnable;

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
