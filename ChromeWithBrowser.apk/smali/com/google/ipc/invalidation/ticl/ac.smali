.class public final Lcom/google/ipc/invalidation/ticl/ac;
.super Lcom/google/ipc/invalidation/b/e;


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Ljava/util/Map;

.field private final c:Ljava/util/Map;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/e;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->c:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->d:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->e:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->a:Ljava/util/Map;

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/ah;->a()[Lcom/google/ipc/invalidation/ticl/ah;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->b:Ljava/util/Map;

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/ag;->a()[Lcom/google/ipc/invalidation/ticl/ag;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->c:Ljava/util/Map;

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/ae;->a()[Lcom/google/ipc/invalidation/ticl/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->d:Ljava/util/Map;

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/af;->a()[Lcom/google/ipc/invalidation/ticl/af;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->e:Ljava/util/Map;

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/ad;->a()[Lcom/google/ipc/invalidation/ticl/ad;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;[Ljava/lang/Object;)V

    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/lang/Object;)V
    .locals 1

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/util/List;)V
    .locals 5

    const/4 v2, 0x0

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_2

    if-nez v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/ipc/invalidation/external/client/types/SimplePair;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    move-object v2, v1

    goto :goto_0

    :cond_0
    return-void

    :cond_1
    move-object v1, v2

    goto :goto_1

    :cond_2
    move-object v1, v2

    goto :goto_2
.end method

.method private static a(Ljava/util/Map;[Ljava/lang/Object;)V
    .locals 5

    const/4 v1, 0x0

    array-length v2, p1

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/ipc/invalidation/b/i;)V
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/List;)V

    const-string v1, "Client Statistics: %s\n"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/google/ipc/invalidation/b/i;

    return-void
.end method

.method public final a(Lcom/google/ipc/invalidation/ticl/ad;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->e:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/ipc/invalidation/ticl/ae;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->c:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/ipc/invalidation/ticl/af;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->d:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/ipc/invalidation/ticl/ag;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->b:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/ipc/invalidation/ticl/ah;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->b:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->c:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->d:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ac;->e:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/Map;Ljava/util/List;)V

    return-void
.end method
