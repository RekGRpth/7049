.class public final enum Lcom/google/ipc/invalidation/ticl/ae;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/ipc/invalidation/ticl/ae;

.field public static final enum b:Lcom/google/ipc/invalidation/ticl/ae;

.field public static final enum c:Lcom/google/ipc/invalidation/ticl/ae;

.field private static final synthetic d:[Lcom/google/ipc/invalidation/ticl/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ae;

    const-string v1, "ACKNOWLEDGE"

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/ticl/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/ae;->a:Lcom/google/ipc/invalidation/ticl/ae;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ae;

    const-string v1, "REGISTRATION"

    invoke-direct {v0, v1, v3}, Lcom/google/ipc/invalidation/ticl/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/ae;->b:Lcom/google/ipc/invalidation/ticl/ae;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ae;

    const-string v1, "UNREGISTRATION"

    invoke-direct {v0, v1, v4}, Lcom/google/ipc/invalidation/ticl/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/ae;->c:Lcom/google/ipc/invalidation/ticl/ae;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/ipc/invalidation/ticl/ae;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ae;->a:Lcom/google/ipc/invalidation/ticl/ae;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ae;->b:Lcom/google/ipc/invalidation/ticl/ae;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ae;->c:Lcom/google/ipc/invalidation/ticl/ae;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/ipc/invalidation/ticl/ae;->d:[Lcom/google/ipc/invalidation/ticl/ae;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/ae;
    .locals 1

    const-class v0, Lcom/google/ipc/invalidation/ticl/ae;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/ae;

    return-object v0
.end method

.method public static a()[Lcom/google/ipc/invalidation/ticl/ae;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/ae;->d:[Lcom/google/ipc/invalidation/ticl/ae;

    invoke-virtual {v0}, [Lcom/google/ipc/invalidation/ticl/ae;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ipc/invalidation/ticl/ae;

    return-object v0
.end method
