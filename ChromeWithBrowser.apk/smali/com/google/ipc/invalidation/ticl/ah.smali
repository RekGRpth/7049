.class public final enum Lcom/google/ipc/invalidation/ticl/ah;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/ipc/invalidation/ticl/ah;

.field public static final enum b:Lcom/google/ipc/invalidation/ticl/ah;

.field public static final enum c:Lcom/google/ipc/invalidation/ticl/ah;

.field public static final enum d:Lcom/google/ipc/invalidation/ticl/ah;

.field public static final enum e:Lcom/google/ipc/invalidation/ticl/ah;

.field public static final enum f:Lcom/google/ipc/invalidation/ticl/ah;

.field private static final synthetic g:[Lcom/google/ipc/invalidation/ticl/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ah;

    const-string v1, "INFO"

    invoke-direct {v0, v1, v3}, Lcom/google/ipc/invalidation/ticl/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/ah;->a:Lcom/google/ipc/invalidation/ticl/ah;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ah;

    const-string v1, "INITIALIZE"

    invoke-direct {v0, v1, v4}, Lcom/google/ipc/invalidation/ticl/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/ah;->b:Lcom/google/ipc/invalidation/ticl/ah;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ah;

    const-string v1, "INVALIDATION_ACK"

    invoke-direct {v0, v1, v5}, Lcom/google/ipc/invalidation/ticl/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/ah;->c:Lcom/google/ipc/invalidation/ticl/ah;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ah;

    const-string v1, "REGISTRATION"

    invoke-direct {v0, v1, v6}, Lcom/google/ipc/invalidation/ticl/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/ah;->d:Lcom/google/ipc/invalidation/ticl/ah;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ah;

    const-string v1, "REGISTRATION_SYNC"

    invoke-direct {v0, v1, v7}, Lcom/google/ipc/invalidation/ticl/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/ah;->e:Lcom/google/ipc/invalidation/ticl/ah;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ah;

    const-string v1, "TOTAL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/ticl/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/ah;->f:Lcom/google/ipc/invalidation/ticl/ah;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/ipc/invalidation/ticl/ah;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ah;->a:Lcom/google/ipc/invalidation/ticl/ah;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ah;->b:Lcom/google/ipc/invalidation/ticl/ah;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ah;->c:Lcom/google/ipc/invalidation/ticl/ah;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ah;->d:Lcom/google/ipc/invalidation/ticl/ah;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ah;->e:Lcom/google/ipc/invalidation/ticl/ah;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/ipc/invalidation/ticl/ah;->f:Lcom/google/ipc/invalidation/ticl/ah;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/ipc/invalidation/ticl/ah;->g:[Lcom/google/ipc/invalidation/ticl/ah;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/ah;
    .locals 1

    const-class v0, Lcom/google/ipc/invalidation/ticl/ah;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/ah;

    return-object v0
.end method

.method public static a()[Lcom/google/ipc/invalidation/ticl/ah;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/ah;->g:[Lcom/google/ipc/invalidation/ticl/ah;

    invoke-virtual {v0}, [Lcom/google/ipc/invalidation/ticl/ah;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ipc/invalidation/ticl/ah;

    return-object v0
.end method
