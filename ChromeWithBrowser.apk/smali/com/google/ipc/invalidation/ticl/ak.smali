.class public final Lcom/google/ipc/invalidation/ticl/ak;
.super Lcom/google/ipc/invalidation/b/d;


# direct methods
.method public constructor <init>(Ljava/util/Random;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/ipc/invalidation/b/d;-><init>(Ljava/util/Random;II)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Random;IILcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)V
    .locals 6

    invoke-virtual {p4}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->f()I

    move-result v4

    invoke-virtual {p4}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->h()Z

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/b/d;-><init>(Ljava/util/Random;IIIZ)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->newBuilder()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/ak;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a(I)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/ak;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->c()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    return-object v0
.end method
