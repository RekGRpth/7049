.class public final Lcom/google/ipc/invalidation/ticl/android2/Q;
.super Lcom/google/ipc/invalidation/ticl/a;


# instance fields
.field private a:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/ipc/invalidation/ticl/android2/a;Ljava/lang/String;)V
    .locals 7

    invoke-static {p3}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forPrefix(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v1

    new-instance v2, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;

    invoke-direct {v2, p1, p2}, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;-><init>(Landroid/content/Context;Lcom/google/ipc/invalidation/ticl/android2/a;)V

    new-instance v3, Lcom/google/ipc/invalidation/ticl/android2/R;

    const/4 v0, 0x0

    invoke-direct {v3, v0}, Lcom/google/ipc/invalidation/ticl/android2/R;-><init>(B)V

    new-instance v4, Lcom/google/ipc/invalidation/ticl/android2/channel/c;

    invoke-direct {v4, p1}, Lcom/google/ipc/invalidation/ticl/android2/channel/c;-><init>(Landroid/content/Context;)V

    new-instance v5, Lcom/google/ipc/invalidation/ticl/android2/M;

    invoke-direct {v5, p1}, Lcom/google/ipc/invalidation/ticl/android2/M;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "Android-"

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/ipc/invalidation/ticl/a;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/ipc/invalidation/ticl/android2/a;Ljava/lang/String;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/ipc/invalidation/ticl/android2/Q;-><init>(Landroid/content/Context;Lcom/google/ipc/invalidation/ticl/android2/a;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method final a()Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/Q;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    const-string v1, "network listener not yet set"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/Q;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Listener already set: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/Q;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method
