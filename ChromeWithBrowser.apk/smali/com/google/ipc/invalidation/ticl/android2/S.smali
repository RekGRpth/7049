.class final Lcom/google/ipc/invalidation/ticl/android2/S;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/types/Callback;


# instance fields
.field final synthetic a:Lcom/google/ipc/invalidation/ticl/android2/TiclService;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/android2/S;->a:Lcom/google/ipc/invalidation/ticl/android2/TiclService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic accept(Ljava/lang/Object;)V
    .locals 5

    const/4 v4, 0x0

    check-cast p1, Lcom/google/ipc/invalidation/external/client/types/SimplePair;

    iget-object v0, p1, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->second:Ljava/lang/Object;

    check-cast v0, [B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/S;->a:Lcom/google/ipc/invalidation/ticl/android2/TiclService;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)Lcom/google/ipc/invalidation/ticl/android2/Q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "No persistent state found for client; not rewriting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/S;->a:Lcom/google/ipc/invalidation/ticl/android2/TiclService;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)Lcom/google/ipc/invalidation/ticl/android2/Q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/S;->a:Lcom/google/ipc/invalidation/ticl/android2/TiclService;

    invoke-static {v2}, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->b(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)Lcom/google/a/b/B;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;[BLcom/google/a/b/B;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/S;->a:Lcom/google/ipc/invalidation/ticl/android2/TiclService;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)Lcom/google/ipc/invalidation/ticl/android2/Q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string v2, "Ignoring invalid Ticl state: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->a(J)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->c()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/S;->a:Lcom/google/ipc/invalidation/ticl/android2/TiclService;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->b(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)Lcom/google/a/b/B;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;Lcom/google/a/b/B;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/S;->a:Lcom/google/ipc/invalidation/ticl/android2/TiclService;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)Lcom/google/ipc/invalidation/ticl/android2/Q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getStorage()Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    move-result-object v1

    const-string v2, "ClientToken"

    new-instance v3, Lcom/google/ipc/invalidation/ticl/android2/T;

    invoke-direct {v3, p0}, Lcom/google/ipc/invalidation/ticl/android2/T;-><init>(Lcom/google/ipc/invalidation/ticl/android2/S;)V

    invoke-interface {v1, v2, v0, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;->writeKey(Ljava/lang/String;[BLcom/google/ipc/invalidation/external/client/types/Callback;)V

    goto :goto_0
.end method
