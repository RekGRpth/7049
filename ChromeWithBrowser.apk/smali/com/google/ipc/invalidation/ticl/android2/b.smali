.class public final Lcom/google/ipc/invalidation/ticl/android2/b;
.super Lcom/google/ipc/invalidation/a/J;


# static fields
.field static final b:Lcom/google/ipc/invalidation/a/Q;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/c;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->C:Lcom/google/ipc/invalidation/a/E;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/ipc/invalidation/a/O;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/ipc/invalidation/a/E;->a:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/ipc/invalidation/a/E;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/ticl/android2/c;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/b;->b:Lcom/google/ipc/invalidation/a/Q;

    return-void
.end method

.method public constructor <init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/a/J;-><init>(Lcom/google/ipc/invalidation/b/a;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest;)Z
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/i;->b:Lcom/google/ipc/invalidation/a/Q;

    invoke-virtual {p0, p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/Q;)Z

    move-result v0

    return v0
.end method

.method final a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;)Z
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/a/Q;

    invoke-virtual {p0, p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/Q;)Z

    move-result v0

    return v0
.end method

.method final a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;)Z
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/i;->c:Lcom/google/ipc/invalidation/a/Q;

    invoke-virtual {p0, p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/Q;)Z

    move-result v0

    return v0
.end method

.method final a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;)Z
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/d;->a:Lcom/google/ipc/invalidation/a/Q;

    invoke-virtual {p0, p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/Q;)Z

    move-result v0

    return v0
.end method

.method final a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;)Z
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/g;->a:Lcom/google/ipc/invalidation/a/Q;

    invoke-virtual {p0, p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/Q;)Z

    move-result v0

    return v0
.end method

.method final a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;)Z
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/j;->a:Lcom/google/ipc/invalidation/a/Q;

    invoke-virtual {p0, p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/Q;)Z

    move-result v0

    return v0
.end method
