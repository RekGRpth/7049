.class public final Lcom/google/ipc/invalidation/ticl/android2/channel/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/ticl/aj;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/google/ipc/invalidation/ticl/android2/Q;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/channel/c;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final sendMessage([B)V
    .locals 3

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/android2/P;->a([B)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/channel/c;->a:Landroid/content/Context;

    const-class v2, Lcom/google/ipc/invalidation/ticl/android2/channel/AndroidMessageSenderService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/channel/c;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public final setListener(Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/channel/c;->b:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->a(Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;)V

    return-void
.end method

.method public final setSystemResources(Lcom/google/ipc/invalidation/external/client/SystemResources;)V
    .locals 1

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/android2/Q;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/channel/c;->b:Lcom/google/ipc/invalidation/ticl/android2/Q;

    return-void
.end method
