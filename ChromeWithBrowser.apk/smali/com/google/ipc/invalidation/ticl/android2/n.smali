.class final Lcom/google/ipc/invalidation/ticl/android2/n;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/InvalidationListener;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/n;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-void
.end method

.method static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/O;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/android2/O;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/O;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 5

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/n;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Dropping call to %s; could not parse ack handle data %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->getHandleData()[B

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final informError(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public final informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v1

    invoke-static {v1, p3, p4}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public final informRegistrationStatus(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V
    .locals 2

    invoke-static {p2}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->REGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    if-ne p3, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Landroid/content/Intent;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final invalidate(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/Invalidation;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 9

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p3}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->getHandleData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->a([B)Lcom/google/protos/ipc/invalidation/Client$AckHandleP;

    move-result-object v2

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getVersion()J

    move-result-wide v5

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getIsTrickleRestartForInternalUse()Z

    move-result v1

    invoke-static {v1}, Lcom/google/ipc/invalidation/a/ag;->a(Z)Lcom/google/ipc/invalidation/a/ag;

    move-result-object v7

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getPayload()[B

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v4

    const/4 v8, 0x1

    invoke-virtual {v4, v8}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v4

    invoke-virtual {v4, v5, v6}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(J)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v4

    sget-object v5, Lcom/google/ipc/invalidation/a/ag;->a:Lcom/google/ipc/invalidation/a/ag;

    if-ne v7, v5, :cond_2

    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    :cond_0
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Lcom/google/protos/ipc/invalidation/Client$AckHandleP;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Landroid/content/Intent;)V

    :goto_2
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getPayload()[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "invalidate"

    invoke-direct {p0, v0, p3}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto :goto_2
.end method

.method public final invalidateAll(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 2

    :try_start_0
    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->getHandleData()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->a([B)Lcom/google/protos/ipc/invalidation/Client$AckHandleP;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->b(Lcom/google/protos/ipc/invalidation/Client$AckHandleP;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "invalidateAll"

    invoke-direct {p0, v0, p2}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto :goto_0
.end method

.method public final invalidateUnknownVersion(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 3

    :try_start_0
    invoke-virtual {p3}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->getHandleData()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->a([B)Lcom/google/protos/ipc/invalidation/Client$AckHandleP;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Lcom/google/protos/ipc/invalidation/Client$AckHandleP;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "invalidateUnknownVersion"

    invoke-direct {p0, v0, p3}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto :goto_0
.end method

.method public final ready(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Landroid/content/Context;

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->d()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public final reissueRegistrations(Lcom/google/ipc/invalidation/external/client/InvalidationClient;[BI)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Landroid/content/Context;

    invoke-static {p2, p3}, Lcom/google/ipc/invalidation/ticl/android2/a;->a([BI)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method
