.class public final Lcom/google/ipc/invalidation/ticl/android2/w;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/a/M;


# static fields
.field public static final a:Lcom/google/ipc/invalidation/ticl/android2/y;

.field public static final b:Lcom/google/ipc/invalidation/ticl/android2/x;

.field public static final c:Lcom/google/ipc/invalidation/a/N;

.field public static final d:Lcom/google/ipc/invalidation/a/N;

.field public static final e:Lcom/google/ipc/invalidation/a/N;

.field public static final f:Lcom/google/ipc/invalidation/a/N;

.field public static final g:Lcom/google/ipc/invalidation/a/N;

.field public static final h:Lcom/google/ipc/invalidation/a/N;

.field private static final i:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/y;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/android2/y;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->a:Lcom/google/ipc/invalidation/ticl/android2/y;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/x;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/android2/x;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->b:Lcom/google/ipc/invalidation/ticl/android2/x;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/A;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/android2/A;-><init>()V

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/z;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/android2/z;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "serial"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "version"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "start"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "stop"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "ack"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "registrations"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->i:Ljava/util/Set;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "serial"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->c:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "version"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->d:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "start"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->e:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "stop"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->f:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "ack"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->g:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "registrations"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->h:Lcom/google/ipc/invalidation/a/N;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/util/Collection;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->i:Ljava/util/Set;

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/N;)Z
    .locals 3

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->c:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->e()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->d:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->g()Z

    move-result v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->e:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->i()Z

    move-result v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->f:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->k()Z

    move-result v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->g:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->m()Z

    move-result v0

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->h:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->o()Z

    move-result v0

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad descriptor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/N;)Ljava/lang/Object;
    .locals 3

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->c:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->d:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->e:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->j()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->f:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->l()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->g:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->n()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    move-result-object v0

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/w;->h:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->p()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v0

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad descriptor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
