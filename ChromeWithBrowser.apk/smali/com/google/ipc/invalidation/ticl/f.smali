.class final Lcom/google/ipc/invalidation/ticl/f;
.super Lcom/google/ipc/invalidation/b/g;


# instance fields
.field private synthetic a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

.field private synthetic b:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

.field private synthetic c:Z

.field private synthetic d:Ljava/lang/String;

.field private synthetic e:Lcom/google/ipc/invalidation/ticl/b;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/b;Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/f;->e:Lcom/google/ipc/invalidation/ticl/b;

    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/f;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    iput-object p4, p0, Lcom/google/ipc/invalidation/ticl/f;->b:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    iput-boolean p5, p0, Lcom/google/ipc/invalidation/ticl/f;->c:Z

    iput-object p6, p0, Lcom/google/ipc/invalidation/ticl/f;->d:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/google/ipc/invalidation/b/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/f;->e:Lcom/google/ipc/invalidation/ticl/b;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/b;->a(Lcom/google/ipc/invalidation/ticl/b;)Lcom/google/ipc/invalidation/ticl/ac;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/af;->b:Lcom/google/ipc/invalidation/ticl/af;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/af;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/f;->e:Lcom/google/ipc/invalidation/ticl/b;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/b;->b(Lcom/google/ipc/invalidation/ticl/b;)Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/f;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/f;->b:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    iget-boolean v3, p0, Lcom/google/ipc/invalidation/ticl/f;->c:Z

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/f;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V

    return-void
.end method
