.class public abstract Lcom/google/ipc/invalidation/ticl/l;
.super Lcom/google/ipc/invalidation/b/e;

# interfaces
.implements Lcom/google/ipc/invalidation/ticl/M;
.implements Lcom/google/ipc/invalidation/ticl/ai;


# instance fields
.field a:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

.field final b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

.field final c:Lcom/google/ipc/invalidation/ticl/ac;

.field private final d:Lcom/google/ipc/invalidation/external/client/SystemResources;

.field private final e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

.field private final f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

.field private g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

.field private final h:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

.field private final i:Lcom/google/ipc/invalidation/ticl/Q;

.field private final j:Lcom/google/ipc/invalidation/ticl/I;

.field private final k:Lcom/google/ipc/invalidation/a/R;

.field private final l:Lcom/google/a/b/B;

.field private final m:Lcom/google/ipc/invalidation/ticl/R;

.field private final n:Lcom/google/ipc/invalidation/b/h;

.field private o:Lcom/google/protobuf/ByteString;

.field private p:Lcom/google/protobuf/ByteString;

.field private q:Z

.field private r:Z

.field private final s:Ljava/util/Random;

.field private t:J

.field private u:Lcom/google/ipc/invalidation/ticl/p;

.field private v:Lcom/google/ipc/invalidation/ticl/v;

.field private w:Lcom/google/ipc/invalidation/ticl/t;

.field private x:Lcom/google/ipc/invalidation/ticl/r;

.field private y:Lcom/google/ipc/invalidation/ticl/q;

.field private z:Lcom/google/ipc/invalidation/ticl/s;


# direct methods
.method public constructor <init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)V
    .locals 12

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v11, p7

    invoke-direct/range {v0 .. v11}, Lcom/google/ipc/invalidation/ticl/l;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Ljava/lang/String;Lcom/google/protos/ipc/invalidation/Client$RunStateP;Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)V

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/external/client/SystemResources;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Created client: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Ljava/lang/String;Lcom/google/protos/ipc/invalidation/Client$RunStateP;Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)V
    .locals 9

    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/e;-><init>()V

    new-instance v1, Lcom/google/a/b/B;

    invoke-direct {v1}, Lcom/google/a/b/B;-><init>()V

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->l:Lcom/google/a/b/B;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/l;->r:Z

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/google/ipc/invalidation/ticl/l;->t:J

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ipc/invalidation/external/client/SystemResources;

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->d:Lcom/google/ipc/invalidation/external/client/SystemResources;

    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/l;->s:Ljava/util/Random;

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    invoke-static {v1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v1

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getStorage()Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    move-result-object v1

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    iput-object p5, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    if-nez p7, :cond_1

    new-instance v1, Lcom/google/ipc/invalidation/ticl/R;

    invoke-direct {v1}, Lcom/google/ipc/invalidation/ticl/R;-><init>()V

    :goto_0
    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->m:Lcom/google/ipc/invalidation/ticl/R;

    new-instance v1, Lcom/google/ipc/invalidation/b/h;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->r()I

    move-result v2

    invoke-direct {v1, p2, v2}, Lcom/google/ipc/invalidation/b/h;-><init>(Ljava/util/Random;I)V

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->n:Lcom/google/ipc/invalidation/b/h;

    invoke-static {p4}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    move-result-object v1

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    new-instance v1, Lcom/google/ipc/invalidation/a/R;

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/ipc/invalidation/a/R;-><init>(Lcom/google/ipc/invalidation/b/a;)V

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->k:Lcom/google/ipc/invalidation/a/R;

    if-eqz p10, :cond_9

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v3

    invoke-virtual/range {p10 .. p10}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->e()Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/google/ipc/invalidation/ticl/ac;

    invoke-direct {v2}, Lcom/google/ipc/invalidation/ticl/ac;-><init>()V

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->f()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\\."

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    const/4 v8, 0x2

    if-eq v7, v8, :cond_2

    const-string v1, "Perf counter name must of form: class.value, skipping: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-interface {v3, v1, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    new-instance v1, Lcom/google/ipc/invalidation/ticl/R;

    move-object/from16 v0, p7

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/ticl/R;-><init>(Lcom/google/protos/ipc/invalidation/Client$RunStateP;)V

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    aget-object v5, v6, v5

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->h()I

    move-result v7

    const-string v1, "SentMessageType"

    invoke-static {v5, v1}, Lcom/google/android/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v6}, Lcom/google/ipc/invalidation/ticl/ah;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/ah;

    move-result-object v5

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v7, :cond_0

    invoke-virtual {v2, v5}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ah;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    const-string v1, "IncomingOperationType"

    invoke-static {v5, v1}, Lcom/google/android/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {v6}, Lcom/google/ipc/invalidation/ticl/ae;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/ae;

    move-result-object v5

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v7, :cond_0

    invoke-virtual {v2, v5}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ae;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    const-string v1, "ReceivedMessageType"

    invoke-static {v5, v1}, Lcom/google/android/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {v6}, Lcom/google/ipc/invalidation/ticl/ag;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/ag;

    move-result-object v5

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v7, :cond_0

    invoke-virtual {v2, v5}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ag;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_5
    const-string v1, "ListenerEventType"

    invoke-static {v5, v1}, Lcom/google/android/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {v6}, Lcom/google/ipc/invalidation/ticl/af;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/af;

    move-result-object v5

    const/4 v1, 0x0

    :goto_5
    if-ge v1, v7, :cond_0

    invoke-virtual {v2, v5}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/af;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_6
    const-string v1, "ClientErrorType"

    invoke-static {v5, v1}, Lcom/google/android/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {v6}, Lcom/google/ipc/invalidation/ticl/ad;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/ad;

    move-result-object v5

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v7, :cond_0

    invoke-virtual {v2, v5}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_7
    const-string v1, "Skipping unknown enum class name %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-interface {v3, v1, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_8
    move-object v1, v2

    :goto_7
    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/Q;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/l;->l:Lcom/google/a/b/B;

    move-object/from16 v0, p8

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/ipc/invalidation/ticl/Q;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/ac;Lcom/google/a/b/B;Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;)V

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->i:Lcom/google/ipc/invalidation/ticl/Q;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/I;

    invoke-virtual {p5}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->x()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->n:Lcom/google/ipc/invalidation/b/h;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    iget-object v7, p0, Lcom/google/ipc/invalidation/ticl/l;->k:Lcom/google/ipc/invalidation/a/R;

    move-object v2, p1

    move v4, p3

    move-object v5, p6

    move-object v6, p0

    move-object/from16 v8, p9

    invoke-direct/range {v1 .. v8}, Lcom/google/ipc/invalidation/ticl/I;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/ac;ILjava/lang/String;Lcom/google/ipc/invalidation/ticl/M;Lcom/google/ipc/invalidation/a/R;Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)V

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->j:Lcom/google/ipc/invalidation/ticl/I;

    return-void

    :cond_9
    new-instance v1, Lcom/google/ipc/invalidation/ticl/ac;

    invoke-direct {v1}, Lcom/google/ipc/invalidation/ticl/ac;-><init>()V

    goto :goto_7
.end method

.method public constructor <init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Ljava/lang/String;Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)V
    .locals 13

    invoke-virtual/range {p7 .. p7}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->f()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v8

    invoke-virtual/range {p7 .. p7}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->t()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    move-result-object v9

    invoke-virtual/range {p7 .. p7}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->r()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v10

    invoke-virtual/range {p7 .. p7}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->H()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    move-result-object v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v12, p8

    invoke-direct/range {v1 .. v12}, Lcom/google/ipc/invalidation/ticl/l;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Ljava/lang/String;Lcom/google/protos/ipc/invalidation/Client$RunStateP;Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)V

    invoke-virtual/range {p7 .. p7}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p7 .. p7}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->h()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    :cond_0
    invoke-virtual/range {p7 .. p7}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual/range {p7 .. p7}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->j()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    :cond_1
    invoke-virtual/range {p7 .. p7}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->l()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/l;->q:Z

    invoke-virtual/range {p7 .. p7}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->n()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/ipc/invalidation/ticl/l;->t:J

    invoke-virtual/range {p7 .. p7}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->p()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/l;->r:Z

    move-object/from16 v0, p7

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)V

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/external/client/SystemResources;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Created client: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->REGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->UNREGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    return-object v0
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/l;ILcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/ipc/invalidation/ticl/ak;
    .locals 3

    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ak;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->s:Ljava/util/Random;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->p()I

    move-result v2

    invoke-direct {v0, v1, p1, v2, p2}, Lcom/google/ipc/invalidation/ticl/ak;-><init>(Ljava/util/Random;IILcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/ak;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->s:Ljava/util/Random;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->p()I

    move-result v2

    invoke-direct {v0, v1, p1, v2}, Lcom/google/ipc/invalidation/ticl/ak;-><init>(Ljava/util/Random;II)V

    goto :goto_0
.end method

.method public static a()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 5

    const/4 v4, 0x3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v4, v1}, Lcom/google/android/b/c;->a(II)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;

    move-result-object v2

    const/16 v3, 0x1388

    invoke-virtual {v2, v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;->b(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/ipc/invalidation/external/client/SystemResources;)V
    .locals 2

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getNetwork()Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

    move-result-object v0

    new-instance v1, Lcom/google/ipc/invalidation/ticl/m;

    invoke-direct {v1, p0}, Lcom/google/ipc/invalidation/ticl/m;-><init>(Lcom/google/ipc/invalidation/ticl/l;)V

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;->setListener(Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/l;Lcom/google/protobuf/ByteString;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/protobuf/ByteString;)V

    return-void
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/l;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/ipc/invalidation/ticl/l;->a(ZZ)V

    return-void
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/l;[B)V
    .locals 11

    const/4 v1, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v2, "Not on internal thread"

    invoke-static {v0, v2}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    if-nez p1, :cond_1

    move-object v0, v1

    :goto_0
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v3, Lcom/google/ipc/invalidation/ticl/ad;->d:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Failed deserializing persistent state: %s"

    new-array v4, v10, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/ipc/invalidation/a/G;->a([B)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Restarting from persistent state: %s"

    new-array v4, v10, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->f()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v5}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/protobuf/ByteString;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->f()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/ipc/invalidation/ticl/l;->b(Lcom/google/protobuf/ByteString;)V

    iput-boolean v9, p0, Lcom/google/ipc/invalidation/ticl/l;->q:Z

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->d:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->h()J

    move-result-wide v3

    invoke-interface {v2}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v5

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->z()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->v()I

    move-result v0

    :goto_1
    invoke-interface {v2}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    const-string v3, "Computed heartbeat delay %s from: offline-delivery = %s, initial-persistent-delay = %s, heartbeat-interval = %s, nowMs = %s"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v9

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->z()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v4, v10

    const/4 v7, 0x2

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->v()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v7

    const/4 v7, 0x3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->l()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v7

    const/4 v1, 0x4

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Lcom/google/ipc/invalidation/ticl/s;

    invoke-direct {v1, p0, v0}, Lcom/google/ipc/invalidation/ticl/s;-><init>(Lcom/google/ipc/invalidation/ticl/l;I)V

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->z:Lcom/google/ipc/invalidation/ticl/s;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->z:Lcom/google/ipc/invalidation/ticl/s;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/s;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->x:Lcom/google/ipc/invalidation/ticl/r;

    const-string v1, "Startup-after-persistence"

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/r;->a(Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->l:Lcom/google/a/b/B;

    invoke-static {v0, p1, v2}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;[BLcom/google/a/b/B;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    cmp-long v0, v3, v5

    if-gtz v0, :cond_3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->l()I

    move-result v0

    int-to-long v7, v0

    add-long/2addr v7, v3

    cmp-long v0, v7, v5

    if-gez v0, :cond_4

    :cond_3
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->v()I

    move-result v0

    goto :goto_1

    :cond_4
    sub-long v3, v5, v3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->l()I

    move-result v0

    int-to-long v7, v0

    sub-long v3, v7, v3

    long-to-int v0, v3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->v()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Starting with no previous state"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v10, p0, Lcom/google/ipc/invalidation/ticl/l;->q:Z

    const-string v0, "Startup"

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private a(Lcom/google/protobuf/ByteString;)V
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Tried to set nonce with existing token %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    aput-object v4, v2, v1

    invoke-static {v0, v3, v2}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)V
    .locals 5

    if-nez p1, :cond_1

    new-instance v0, Lcom/google/ipc/invalidation/ticl/p;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/p;-><init>(Lcom/google/ipc/invalidation/ticl/l;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->u:Lcom/google/ipc/invalidation/ticl/p;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/r;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/r;-><init>(Lcom/google/ipc/invalidation/ticl/l;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->x:Lcom/google/ipc/invalidation/ticl/r;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/v;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/v;-><init>(Lcom/google/ipc/invalidation/ticl/l;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->v:Lcom/google/ipc/invalidation/ticl/v;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/t;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/t;-><init>(Lcom/google/ipc/invalidation/ticl/l;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->w:Lcom/google/ipc/invalidation/ticl/t;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/q;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->j:Lcom/google/ipc/invalidation/ticl/I;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->d:Lcom/google/ipc/invalidation/external/client/SystemResources;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->n:Lcom/google/ipc/invalidation/b/h;

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-virtual {v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->x()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->f()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/ipc/invalidation/ticl/q;-><init>(Lcom/google/ipc/invalidation/ticl/I;Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/b/h;I)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->y:Lcom/google/ipc/invalidation/ticl/q;

    :cond_0
    :goto_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/s;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/ipc/invalidation/ticl/s;-><init>(Lcom/google/ipc/invalidation/ticl/l;I)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->z:Lcom/google/ipc/invalidation/ticl/s;

    return-void

    :cond_1
    new-instance v0, Lcom/google/ipc/invalidation/ticl/p;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->v()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/ipc/invalidation/ticl/p;-><init>(Lcom/google/ipc/invalidation/ticl/l;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->u:Lcom/google/ipc/invalidation/ticl/p;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/r;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->B()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/ipc/invalidation/ticl/r;-><init>(Lcom/google/ipc/invalidation/ticl/l;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->x:Lcom/google/ipc/invalidation/ticl/r;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/v;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->x()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/ipc/invalidation/ticl/v;-><init>(Lcom/google/ipc/invalidation/ticl/l;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->v:Lcom/google/ipc/invalidation/ticl/v;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/t;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->z()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/ipc/invalidation/ticl/t;-><init>(Lcom/google/ipc/invalidation/ticl/l;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->w:Lcom/google/ipc/invalidation/ticl/t;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/q;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->j:Lcom/google/ipc/invalidation/ticl/I;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->d:Lcom/google/ipc/invalidation/external/client/SystemResources;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->n:Lcom/google/ipc/invalidation/b/h;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->D()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/ipc/invalidation/ticl/q;-><init>(Lcom/google/ipc/invalidation/ticl/I;Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/b/h;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->y:Lcom/google/ipc/invalidation/ticl/q;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->w:Lcom/google/ipc/invalidation/ticl/t;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/t;->a(Lcom/google/ipc/invalidation/ticl/t;)Lcom/google/ipc/invalidation/b/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->F()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/b;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/l;->b(Lcom/google/protobuf/ByteString;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->u:Lcom/google/ipc/invalidation/ticl/p;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/ticl/p;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/util/Collection;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)V
    .locals 10

    const/4 v9, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Must specify some object id"

    invoke-static {v0, v3}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    const-string v0, "Must specify (un)registration"

    invoke-static {p2, v0}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v3, "Not running on internal thread"

    invoke-static {v0, v3}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->m:Lcom/google/ipc/invalidation/ticl/R;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/R;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Ticl stopped: register (%s) of %s ignored."

    new-array v4, v9, [Ljava/lang/Object;

    aput-object p2, v4, v2

    aput-object p1, v4, v1

    invoke-interface {v0, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->m:Lcom/google/ipc/invalidation/ticl/R;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/R;->c()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Ticl is not yet started; failing registration call; client = %s, objects = %s, op = %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v2

    aput-object p1, v4, v1

    aput-object p2, v4, v9

    invoke-interface {v0, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    const-string v4, "Client not yet ready"

    invoke-interface {v3, p0, v0, v1, v4}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V

    goto :goto_2

    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    const-string v5, "Must specify object id"

    invoke-static {v0, v5}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v5

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    if-ne p2, v0, :cond_4

    sget-object v0, Lcom/google/ipc/invalidation/ticl/ae;->b:Lcom/google/ipc/invalidation/ticl/ae;

    :goto_4
    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    invoke-virtual {v6, v0}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ae;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v6, "Register %s, %s"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v5}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v7, v2

    aput-object p2, v7, v1

    invoke-interface {v0, v6, v7}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    sget-object v0, Lcom/google/ipc/invalidation/ticl/ae;->c:Lcom/google/ipc/invalidation/ticl/ae;

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->i:Lcom/google/ipc/invalidation/ticl/Q;

    invoke-virtual {v0, v3, p2}, Lcom/google/ipc/invalidation/ticl/Q;->a(Ljava/util/Collection;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)Ljava/util/Collection;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/l;->q:Z

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->j:Lcom/google/ipc/invalidation/ticl/I;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->y:Lcom/google/ipc/invalidation/ticl/q;

    invoke-virtual {v1, v0, p2, v2}, Lcom/google/ipc/invalidation/ticl/I;->a(Ljava/util/Collection;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;Lcom/google/ipc/invalidation/ticl/q;)V

    :cond_6
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->v:Lcom/google/ipc/invalidation/ticl/v;

    const-string v1, "performRegister"

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/v;->a(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private a(ZZ)V
    .locals 5

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Sending info message to server; request server summary = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    :cond_0
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->j:Lcom/google/ipc/invalidation/ticl/I;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->y:Lcom/google/ipc/invalidation/ticl/q;

    invoke-virtual {v2, v1, v0, p2, v3}, Lcom/google/ipc/invalidation/ticl/I;->a(Ljava/util/List;Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;ZLcom/google/ipc/invalidation/ticl/q;)V

    return-void
.end method

.method static synthetic b(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-object v0
.end method

.method public static b()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/b/c;->a(II)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v1

    const/16 v2, 0xc8

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/protobuf/ByteString;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Tried to set token with existing nonce %s"

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->m:Lcom/google/ipc/invalidation/ticl/R;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/R;->c()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    move v0, v2

    :goto_1
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->m:Lcom/google/ipc/invalidation/ticl/R;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/R;->c()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    invoke-static {v0}, Lcom/google/a/a/a;->b(Z)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->m:Lcom/google/ipc/invalidation/ticl/R;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/R;->a()V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    invoke-interface {v0, p0}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->ready(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    sget-object v3, Lcom/google/ipc/invalidation/ticl/Q;->a:[B

    invoke-interface {v0, p0, v3, v1}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->reissueRegistrations(Lcom/google/ipc/invalidation/external/client/InvalidationClient;[BI)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Ticl started: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v1

    invoke-interface {v0, v3, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method static synthetic c(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/b/h;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->n:Lcom/google/ipc/invalidation/b/h;

    return-object v0
.end method

.method static synthetic d(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    return-object v0
.end method

.method static synthetic e(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method static synthetic f(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    return-object v0
.end method

.method static synthetic g(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method static synthetic h(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/ticl/q;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->y:Lcom/google/ipc/invalidation/ticl/q;

    return-object v0
.end method

.method static synthetic i(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/ticl/I;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->j:Lcom/google/ipc/invalidation/ticl/I;

    return-object v0
.end method

.method static synthetic j(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/ticl/Q;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->i:Lcom/google/ipc/invalidation/ticl/Q;

    return-object v0
.end method

.method static synthetic k(Lcom/google/ipc/invalidation/ticl/l;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/l;->t:J

    return-wide v0
.end method

.method static synthetic l(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/a/b/B;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->l:Lcom/google/a/b/B;

    return-object v0
.end method

.method static synthetic m(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->d:Lcom/google/ipc/invalidation/external/client/SystemResources;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/ipc/invalidation/b/i;)V
    .locals 4

    const-string v0, "Client: %s, %s, %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->m:Lcom/google/ipc/invalidation/ticl/R;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/google/ipc/invalidation/b/i;

    return-void
.end method

.method a(Z)V
    .locals 10

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v2

    const-string v3, "Not on internal thread"

    invoke-static {v2, v3}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/l;->r:Z

    iput-boolean p1, p0, Lcom/google/ipc/invalidation/ticl/l;->r:Z

    if-eqz p1, :cond_0

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/ipc/invalidation/ticl/l;->t:J

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-virtual {v6}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->B()I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v4, "Sending heartbeat after reconnection, previous send was %s ms ago"

    new-array v5, v0, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/ipc/invalidation/ticl/l;->t:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-interface {v2, v3, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->i:Lcom/google/ipc/invalidation/ticl/Q;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/Q;->c()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/ipc/invalidation/ticl/l;->a(ZZ)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method a([B)V
    .locals 11

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ag;->h:Lcom/google/ipc/invalidation/ticl/ag;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ag;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->j:Lcom/google/ipc/invalidation/ticl/I;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/ticl/I;->a([B)Lcom/google/ipc/invalidation/ticl/L;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    iget-object v1, v3, Lcom/google/ipc/invalidation/ticl/L;->a:Lcom/google/ipc/invalidation/ticl/N;

    iget-object v1, v1, Lcom/google/ipc/invalidation/ticl/N;->a:Lcom/google/protobuf/ByteString;

    invoke-static {v0, v1}, Lcom/google/android/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Incoming message has bad token: server = %s, client = %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v3, Lcom/google/ipc/invalidation/ticl/L;->a:Lcom/google/ipc/invalidation/ticl/N;

    iget-object v5, v5, Lcom/google/ipc/invalidation/ticl/N;->a:Lcom/google/protobuf/ByteString;

    invoke-static {v5}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    invoke-static {v5}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ad;->j:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ag;->e:Lcom/google/ipc/invalidation/ticl/ag;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ag;)V

    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->a:Lcom/google/ipc/invalidation/ticl/N;

    iget-object v1, v0, Lcom/google/ipc/invalidation/ticl/N;->a:Lcom/google/protobuf/ByteString;

    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->f()Lcom/google/protobuf/ByteString;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v2

    const-string v4, "Not on internal thread"

    invoke-static {v2, v4}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    if-eqz v0, :cond_b

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    invoke-static {v1, v2}, Lcom/google/android/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    invoke-static {v1, v4}, Lcom/google/android/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v2, :cond_2

    if-eqz v1, :cond_9

    :cond_2
    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Ignoring new token; %s does not match nonce = %s or existing token = %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    aput-object v5, v4, v0

    const/4 v0, 0x2

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    aput-object v5, v4, v0

    invoke-interface {v1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    if-eqz v0, :cond_0

    iget-object v1, v3, Lcom/google/ipc/invalidation/ticl/L;->a:Lcom/google/ipc/invalidation/ticl/N;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v2, "Not on internal thread"

    invoke-static {v0, v2}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_4
    const-string v2, "Cannot process server header with non-null nonce (have %s): %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v0, v2, v4}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, v1, Lcom/google/ipc/invalidation/ticl/N;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/l;->q:Z

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->i:Lcom/google/ipc/invalidation/ticl/Q;

    iget-object v2, v1, Lcom/google/ipc/invalidation/ticl/N;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/ticl/Q;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;)Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Receivced new server registration summary (%s); will make %s upcalls"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v1, v1, Lcom/google/ipc/invalidation/ticl/N;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    aput-object v1, v5, v6

    const/4 v1, 0x1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-interface {v2, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    move-result-object v0

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    invoke-interface {v4, p0, v2, v0}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->informRegistrationStatus(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    goto :goto_5

    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    iget-object v1, v3, Lcom/google/ipc/invalidation/ticl/L;->a:Lcom/google/ipc/invalidation/ticl/N;

    iget-object v1, v1, Lcom/google/ipc/invalidation/ticl/N;->a:Lcom/google/protobuf/ByteString;

    invoke-static {v0, v1}, Lcom/google/android/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ad;->i:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Rejecting server message with mismatched nonce: Client = %s, Server = %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    invoke-static {v5}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    iget-object v5, v3, Lcom/google/ipc/invalidation/ticl/L;->a:Lcom/google/ipc/invalidation/ticl/N;

    iget-object v5, v5, Lcom/google/ipc/invalidation/ticl/N;->a:Lcom/google/protobuf/ByteString;

    invoke-static {v5}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Accepting server message with matching nonce: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    invoke-static {v5}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Neither token nor nonce was set in validateToken: %s, %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    aput-object v5, v2, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    aput-object v5, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_a
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "New token being assigned at client: %s, Old = %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    invoke-static {v6}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-interface {v1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->x:Lcom/google/ipc/invalidation/ticl/r;

    const-string v2, "Heartbeat-after-new-token"

    invoke-virtual {v1, v2}, Lcom/google/ipc/invalidation/ticl/r;->a(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/protobuf/ByteString;)V

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/l;->b(Lcom/google/protobuf/ByteString;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->w:Lcom/google/ipc/invalidation/ticl/t;

    const-string v1, "Write-after-new-token"

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/t;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_b
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Destroying existing token: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    invoke-static {v5}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "Destroy"

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_d
    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ag;->b:Lcom/google/ipc/invalidation/ticl/ag;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ag;)V

    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->e()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v1

    const-string v2, "Not on internal thread"

    invoke-static {v1, v2}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->newBuilder()Lcom/google/protos/ipc/invalidation/Client$AckHandleP$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/Client$AckHandleP$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP$Builder;->c()Lcom/google/protos/ipc/invalidation/Client$AckHandleP;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->b()[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->newInstance([B)Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v1

    if-eqz v1, :cond_e

    sget-object v5, Lcom/google/ipc/invalidation/a/F;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-virtual {v5}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->f()I

    move-result v5

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->f()I

    move-result v6

    if-ne v5, v6, :cond_e

    sget-object v5, Lcom/google/ipc/invalidation/a/F;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-virtual {v5}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->h()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->h()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    const/4 v1, 0x1

    :goto_7
    if-eqz v1, :cond_f

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Issuing invalidate all"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-interface {v0, v1, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    invoke-interface {v0, p0, v4}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->invalidateAll(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto :goto_6

    :cond_e
    const/4 v1, 0x0

    goto :goto_7

    :cond_f
    invoke-static {v0}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->l()Z

    move-result v5

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v7, "Issuing invalidate (known-version = %s, is-trickle-restart = %s): %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->h()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    aput-object v1, v8, v9

    invoke-interface {v6, v7, v8}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->h()Z

    move-result v0

    if-eqz v0, :cond_11

    if-eqz v5, :cond_10

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->D()Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    invoke-interface {v0, p0, v1, v4}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->invalidate(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/Invalidation;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto/16 :goto_6

    :cond_11
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v1

    invoke-interface {v0, p0, v1, v4}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->invalidateUnknownVersion(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto/16 :goto_6

    :cond_12
    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ag;->c:Lcom/google/ipc/invalidation/ticl/ag;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ag;)V

    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;->e()Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->i:Lcom/google/ipc/invalidation/ticl/Q;

    invoke-virtual {v0, v4}, Lcom/google/ipc/invalidation/ticl/Q;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_13

    const/4 v0, 0x1

    :goto_8
    const-string v1, "Not all registration statuses were processed"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    const/4 v0, 0x0

    move v2, v0

    :goto_9
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_18

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v7, "Process reg status: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    invoke-interface {v6, v7, v8}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v6

    if-eqz v1, :cond_14

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    invoke-interface {v1, p0, v6, v0}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->informRegistrationStatus(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    :goto_a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    :cond_13
    const/4 v0, 0x0

    goto :goto_8

    :cond_14
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;)Z

    move-result v1

    if-eqz v1, :cond_15

    const-string v1, "Registration discrepancy detected"

    :goto_b
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    move-result-object v0

    sget-object v7, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    if-ne v0, v7, :cond_16

    const/4 v0, 0x1

    :goto_c
    iget-object v7, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    if-nez v0, :cond_17

    const/4 v0, 0x1

    :goto_d
    invoke-interface {v7, p0, v6, v0, v1}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V

    goto :goto_a

    :cond_15
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->h()Ljava/lang/String;

    move-result-object v1

    goto :goto_b

    :cond_16
    const/4 v0, 0x0

    goto :goto_c

    :cond_17
    const/4 v0, 0x0

    goto :goto_d

    :cond_18
    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ag;->d:Lcom/google/ipc/invalidation/ticl/ag;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ag;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->i:Lcom/google/ipc/invalidation/ticl/Q;

    sget-object v1, Lcom/google/ipc/invalidation/b/c;->a:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->a()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/ipc/invalidation/ticl/Q;->a([BI)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->j:Lcom/google/ipc/invalidation/ticl/I;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->y:Lcom/google/ipc/invalidation/ticl/q;

    invoke-virtual {v1, v0, v2}, Lcom/google/ipc/invalidation/ticl/I;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;Lcom/google/ipc/invalidation/ticl/q;)V

    :cond_19
    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ag;->a:Lcom/google/ipc/invalidation/ticl/ag;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ag;)V

    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->e()Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v2, "Not on internal thread"

    invoke-static {v0, v2}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    const/4 v0, 0x0

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;

    sget-object v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;

    if-ne v0, v2, :cond_1d

    const/4 v0, 0x1

    :goto_e
    if-eqz v0, :cond_1a

    :cond_1b
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->i:Lcom/google/ipc/invalidation/ticl/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/Q;->c()Z

    move-result v1

    if-nez v1, :cond_1e

    const/4 v1, 0x1

    :goto_f
    invoke-direct {p0, v0, v1}, Lcom/google/ipc/invalidation/ticl/l;->a(ZZ)V

    :cond_1c
    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ag;->f:Lcom/google/ipc/invalidation/ticl/ag;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ag;)V

    iget-object v0, v3, Lcom/google/ipc/invalidation/ticl/L;->a:Lcom/google/ipc/invalidation/ticl/N;

    iget-object v1, v3, Lcom/google/ipc/invalidation/ticl/L;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    move-result-object v1

    iget-object v2, v3, Lcom/google/ipc/invalidation/ticl/L;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;->h()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v3

    const-string v4, "Not on internal thread"

    invoke-static {v3, v4}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Received error message: %s, %s, %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v1, v5, v0

    const/4 v0, 0x2

    aput-object v2, v5, v0

    invoke-interface {v3, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/ipc/invalidation/ticl/o;->a:[I

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    const/4 v0, -0x1

    :goto_10
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v3, v2, v4}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->newInstance(IZLjava/lang/String;Lcom/google/ipc/invalidation/external/client/types/ErrorContext;)Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    move-result-object v0

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    invoke-interface {v3, p0, v0}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->informError(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->i:Lcom/google/ipc/invalidation/ticl/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/Q;->a()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Issuing failure for %s objects"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-interface {v1, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->b:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    invoke-static {v0}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v0

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Auth error: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, p0, v0, v4, v5}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V

    goto :goto_11

    :cond_1d
    const/4 v0, 0x0

    goto/16 :goto_e

    :cond_1e
    const/4 v1, 0x0

    goto/16 :goto_f

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_10

    :pswitch_1
    const/4 v0, -0x1

    goto :goto_10

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public acknowledge(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not running on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    :try_start_0
    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->getHandleData()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->a([B)Lcom/google/protos/ipc/invalidation/Client$AckHandleP;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->k:Lcom/google/ipc/invalidation/a/R;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/ipc/invalidation/a/R;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Incorrect ack handle data: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ad;->a:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Bad ack handle : %s"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->getHandleData()[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/G;->a([B)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ad;->a:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->m()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->r()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    :cond_2
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v2, Lcom/google/ipc/invalidation/ticl/ae;->a:Lcom/google/ipc/invalidation/ticl/ae;

    invoke-virtual {v1, v2}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ae;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->j:Lcom/google/ipc/invalidation/ticl/I;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->y:Lcom/google/ipc/invalidation/ticl/q;

    invoke-virtual {v1, v0, v2}, Lcom/google/ipc/invalidation/ticl/I;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Lcom/google/ipc/invalidation/ticl/q;)V

    goto :goto_0
.end method

.method protected c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    return-object v0
.end method

.method protected d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    return-object v0
.end method

.method public final e()Lcom/google/ipc/invalidation/external/client/SystemResources;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->d:Lcom/google/ipc/invalidation/external/client/SystemResources;

    return-object v0
.end method

.method public final f()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/a/a/a;->b(Z)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->d:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/l;->t:J

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->w:Lcom/google/ipc/invalidation/ticl/t;

    const-string v1, "sent-message"

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/t;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final h()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->i:Lcom/google/ipc/invalidation/ticl/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/Q;->b()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v0

    return-object v0
.end method

.method protected final i()Ljava/util/Map;
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    const-string v1, "AcquireToken"

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->u:Lcom/google/ipc/invalidation/ticl/p;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/p;->c()Lcom/google/ipc/invalidation/b/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "RegSyncHeartbeat"

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->v:Lcom/google/ipc/invalidation/ticl/v;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/v;->c()Lcom/google/ipc/invalidation/b/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "PersistentWrite"

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->w:Lcom/google/ipc/invalidation/ticl/t;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/t;->c()Lcom/google/ipc/invalidation/b/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Heartbeat"

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->x:Lcom/google/ipc/invalidation/ticl/r;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/r;->c()Lcom/google/ipc/invalidation/b/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Batching"

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->y:Lcom/google/ipc/invalidation/ticl/q;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/q;->c()Lcom/google/ipc/invalidation/b/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "InitialPersistentHeartbeat"

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/l;->z:Lcom/google/ipc/invalidation/ticl/s;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/s;->c()Lcom/google/ipc/invalidation/b/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public final j()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;
    .locals 6

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not running on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    :cond_0
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/l;->t:J

    invoke-virtual {v2, v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(J)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->p:Lcom/google/protobuf/ByteString;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->c(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->j:Lcom/google/ipc/invalidation/ticl/I;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/I;->b()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->i:Lcom/google/ipc/invalidation/ticl/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/Q;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/l;->q:Z

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->m:Lcom/google/ipc/invalidation/ticl/R;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/R;->e()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$RunStateP;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/l;->r:Z

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b(Z)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->u:Lcom/google/ipc/invalidation/ticl/p;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/p;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->w:Lcom/google/ipc/invalidation/ticl/t;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/t;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->c(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->v:Lcom/google/ipc/invalidation/ticl/v;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/v;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->x:Lcom/google/ipc/invalidation/ticl/r;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/r;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->d(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/l;->y:Lcom/google/ipc/invalidation/ticl/q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/q;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->e(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;

    move-result-object v4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Ljava/util/List;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/ipc/invalidation/external/client/types/SimplePair;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->getSecond()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/b/c;->a(Ljava/lang/String;I)Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;)Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;->c()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->o:Lcom/google/protobuf/ByteString;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    :cond_3
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->w:Lcom/google/ipc/invalidation/ticl/t;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/t;->a(Lcom/google/ipc/invalidation/ticl/t;)Lcom/google/ipc/invalidation/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->w:Lcom/google/ipc/invalidation/ticl/t;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/t;->a(Lcom/google/ipc/invalidation/ticl/t;)Lcom/google/ipc/invalidation/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    :cond_4
    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->c()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v0

    return-object v0
.end method

.method public register(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    invoke-direct {p0, v0, v1}, Lcom/google/ipc/invalidation/ticl/l;->a(Ljava/util/Collection;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)V

    return-void
.end method

.method public register(Ljava/util/Collection;)V
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    invoke-direct {p0, p1, v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Ljava/util/Collection;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)V

    return-void
.end method

.method public start()V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->d:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->isStarted()Z

    move-result v0

    const-string v1, "Resources must be started before starting the Ticl"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->m:Lcom/google/ipc/invalidation/ticl/R;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/R;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Ignoring start call since already started: client = %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->a(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/protobuf/ByteString;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Starting with Java config: %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/l;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    const-string v1, "ClientToken"

    new-instance v2, Lcom/google/ipc/invalidation/ticl/n;

    invoke-direct {v2, p0}, Lcom/google/ipc/invalidation/ticl/n;-><init>(Lcom/google/ipc/invalidation/ticl/l;)V

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;->readKey(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->f:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Ticl being stopped: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->m:Lcom/google/ipc/invalidation/ticl/R;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/R;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/l;->m:Lcom/google/ipc/invalidation/ticl/R;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/R;->b()V

    :cond_0
    return-void
.end method

.method public unregister(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    invoke-direct {p0, v0, v1}, Lcom/google/ipc/invalidation/ticl/l;->a(Ljava/util/Collection;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)V

    return-void
.end method

.method public unregister(Ljava/util/Collection;)V
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    invoke-direct {p0, p1, v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Ljava/util/Collection;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)V

    return-void
.end method
