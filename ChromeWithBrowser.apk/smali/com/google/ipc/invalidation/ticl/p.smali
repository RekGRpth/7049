.class final Lcom/google/ipc/invalidation/ticl/p;
.super Lcom/google/ipc/invalidation/ticl/w;


# instance fields
.field private synthetic a:Lcom/google/ipc/invalidation/ticl/l;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/l;)V
    .locals 6

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/p;->a:Lcom/google/ipc/invalidation/ticl/l;

    const-string v2, "AcquireToken"

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->d(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->h()I

    move-result v4

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/w;-><init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;IIZ)V

    return-void
.end method

.method constructor <init>(Lcom/google/ipc/invalidation/ticl/l;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V
    .locals 6

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/p;->a:Lcom/google/ipc/invalidation/ticl/l;

    const-string v2, "AcquireToken"

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->d(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->h()I

    move-result v3

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/w;-><init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;ILcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;B)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/p;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->e(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/p;->a:Lcom/google/ipc/invalidation/ticl/l;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/p;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->a(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;Lcom/google/protobuf/ByteString;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/p;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->i(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/ticl/I;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/p;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/l;->f(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    move-result-object v1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/p;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v2}, Lcom/google/ipc/invalidation/ticl/l;->g(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protobuf/ByteString;

    move-result-object v2

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/p;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v3}, Lcom/google/ipc/invalidation/ticl/l;->h(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/ticl/q;

    move-result-object v3

    const-string v4, "AcquireToken"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/ipc/invalidation/ticl/I;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;Lcom/google/protobuf/ByteString;Lcom/google/ipc/invalidation/ticl/q;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
