.class final Lcom/google/ipc/invalidation/ticl/t;
.super Lcom/google/ipc/invalidation/ticl/w;


# instance fields
.field final synthetic a:Lcom/google/ipc/invalidation/ticl/l;

.field private final b:Lcom/google/ipc/invalidation/b/b;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/l;)V
    .locals 6

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/t;->a:Lcom/google/ipc/invalidation/ticl/l;

    const-string v2, "PersistentWrite"

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->d(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->j()I

    move-result v4

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/w;-><init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;IIZ)V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->d()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/b/b;->a(Ljava/lang/Object;)Lcom/google/ipc/invalidation/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/t;->b:Lcom/google/ipc/invalidation/b/b;

    return-void
.end method

.method constructor <init>(Lcom/google/ipc/invalidation/ticl/l;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V
    .locals 6

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/t;->a:Lcom/google/ipc/invalidation/ticl/l;

    const-string v2, "PersistentWrite"

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->d(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->j()I

    move-result v3

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/w;-><init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;ILcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;B)V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->d()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/b/b;->a(Ljava/lang/Object;)Lcom/google/ipc/invalidation/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/t;->b:Lcom/google/ipc/invalidation/b/b;

    return-void
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/t;)Lcom/google/ipc/invalidation/b/b;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/t;->b:Lcom/google/ipc/invalidation/b/b;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/t;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->e(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/t;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->e(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/t;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v2}, Lcom/google/ipc/invalidation/ticl/l;->k(Lcom/google/ipc/invalidation/ticl/l;)J

    move-result-wide v2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->newBuilder()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->a(J)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->c()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/t;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v3}, Lcom/google/ipc/invalidation/ticl/l;->l(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/a/b/B;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;Lcom/google/a/b/B;)[B

    move-result-object v3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/t;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->d(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/t;->b:Lcom/google/ipc/invalidation/b/b;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/ticl/H;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->f()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/t;->b:Lcom/google/ipc/invalidation/b/b;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->f()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/t;->a:Lcom/google/ipc/invalidation/ticl/l;

    iget-object v0, v0, Lcom/google/ipc/invalidation/ticl/l;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    const-string v1, "ClientToken"

    new-instance v4, Lcom/google/ipc/invalidation/ticl/u;

    invoke-direct {v4, p0, v2}, Lcom/google/ipc/invalidation/ticl/u;-><init>(Lcom/google/ipc/invalidation/ticl/t;Lcom/google/ipc/invalidation/ticl/H;)V

    invoke-interface {v0, v1, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;->writeKey(Ljava/lang/String;[BLcom/google/ipc/invalidation/external/client/types/Callback;)V

    const/4 v0, 0x1

    goto :goto_0
.end method
