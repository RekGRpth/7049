.class final Lcom/google/ipc/invalidation/ticl/u;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/types/Callback;


# instance fields
.field private synthetic a:Lcom/google/ipc/invalidation/ticl/H;

.field private synthetic b:Lcom/google/ipc/invalidation/ticl/t;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/t;Lcom/google/ipc/invalidation/ticl/H;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/u;->b:Lcom/google/ipc/invalidation/ticl/t;

    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/u;->a:Lcom/google/ipc/invalidation/ticl/H;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic accept(Ljava/lang/Object;)V
    .locals 5

    check-cast p1, Lcom/google/ipc/invalidation/external/client/types/Status;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/u;->b:Lcom/google/ipc/invalidation/ticl/t;

    iget-object v0, v0, Lcom/google/ipc/invalidation/ticl/t;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->b(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "Write state completed: %s for %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/u;->a:Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v4}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/u;->b:Lcom/google/ipc/invalidation/ticl/t;

    iget-object v0, v0, Lcom/google/ipc/invalidation/ticl/t;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->m(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/a/a/a;->b(Z)V

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/Status;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/u;->b:Lcom/google/ipc/invalidation/ticl/t;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/t;->a(Lcom/google/ipc/invalidation/ticl/t;)Lcom/google/ipc/invalidation/b/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/u;->a:Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/b;->b(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/u;->b:Lcom/google/ipc/invalidation/ticl/t;

    iget-object v0, v0, Lcom/google/ipc/invalidation/ticl/t;->a:Lcom/google/ipc/invalidation/ticl/l;

    iget-object v0, v0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ad;->f:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    goto :goto_0
.end method
