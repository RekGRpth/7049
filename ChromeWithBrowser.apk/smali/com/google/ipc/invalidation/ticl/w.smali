.class abstract Lcom/google/ipc/invalidation/ticl/w;
.super Lcom/google/ipc/invalidation/ticl/O;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;IIZ)V
    .locals 8

    const/4 v5, 0x0

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v2

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->b(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v3

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->c(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/b/h;

    move-result-object v4

    if-eqz p5, :cond_0

    invoke-static {p1, p4, v5}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;ILcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/ipc/invalidation/ticl/ak;

    move-result-object v5

    :cond_0
    move-object v0, p0

    move-object v1, p2

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/ipc/invalidation/ticl/O;-><init>(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/b/h;Lcom/google/ipc/invalidation/ticl/ak;II)V

    return-void
.end method

.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;ILcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V
    .locals 7

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v2

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->b(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v3

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->c(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/b/h;

    move-result-object v4

    invoke-virtual {p4}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->l()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    invoke-static {p1, p3, v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;ILcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/ipc/invalidation/ticl/ak;

    move-result-object v5

    move-object v0, p0

    move-object v1, p2

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/ipc/invalidation/ticl/O;-><init>(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/b/h;Lcom/google/ipc/invalidation/ticl/ak;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;ILcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/ipc/invalidation/ticl/w;-><init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;ILcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V
    .locals 7

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v2

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->b(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v3

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->c(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/b/h;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/ipc/invalidation/ticl/O;-><init>(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/b/h;Lcom/google/ipc/invalidation/ticl/ak;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/ipc/invalidation/ticl/w;-><init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V

    return-void
.end method
