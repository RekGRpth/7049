.class public final Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatchOrBuilder;


# instance fields
.field private a:I

.field private b:Ljava/util/List;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->b:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;)Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;->e()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method static synthetic c()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;-><init>()V

    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->f()V

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method private d()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;
    .locals 3

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;->d()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;

    move-result-object v2

    if-eq v1, v2, :cond_0

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;->a(Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;->a(Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->b:Ljava/util/List;

    iget v1, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->a:I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->f()V

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->b:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;->a(Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;
    .locals 3

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;-><init>(Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;B)V

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->a:I

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;->a(Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch;Ljava/util/List;)Ljava/util/List;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->b:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->a:I

    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageBatch$Builder;

    move-result-object v0

    return-object v0
.end method
