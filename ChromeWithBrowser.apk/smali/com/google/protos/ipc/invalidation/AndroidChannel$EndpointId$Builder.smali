.class public final Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointIdOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private g:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->c:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->d:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->e:Ljava/lang/Object;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->g:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->q()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->c:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->d:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->e:Ljava/lang/Object;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    :cond_4
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_2

    :sswitch_6
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->g:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;
    .locals 5

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->d()Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;

    move-result-object v2

    if-eq v1, v2, :cond_7

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v4

    if-eq v3, v4, :cond_3

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    :goto_0
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    :cond_0
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;

    :cond_1
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->b(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;

    :cond_2
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->k()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->l()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    goto :goto_0

    :cond_4
    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->e:Ljava/lang/Object;

    :cond_5
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->m()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->n()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_8

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v4

    if-eq v3, v4, :cond_8

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    :goto_1
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    :cond_6
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->o()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->c(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;

    :cond_7
    return-object v0

    :cond_8
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    goto :goto_1
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;-><init>(Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->a(Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->a(Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->b(Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->c(Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->a(Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->g:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->d(Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->a(Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;I)I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->c:Ljava/lang/Object;

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->d:Ljava/lang/Object;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->g:Ljava/lang/Object;

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId;->q()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidChannel$EndpointId$Builder;

    move-result-object v0

    return-object v0
.end method
