.class public final Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerStateOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;


# instance fields
.field private b:I

.field private c:Ljava/util/List;

.field private d:Ljava/util/List;

.field private e:Lcom/google/protobuf/ByteString;

.field private f:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->a:Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->c:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->d:Ljava/util/List;

    sget-object v1, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->e:Lcom/google/protobuf/ByteString;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->f:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->g:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->h:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->g:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->h:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;-><init>(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->f:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->e:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method public static a([B)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;->a([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->c:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->b:I

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->d:Ljava/util/List;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->a:Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$Builder;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->c()I

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->e:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_3
    return-void
.end method

.method public final c()I
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->h:I

    const/4 v0, -0x1

    if-eq v3, v0, :cond_0

    :goto_0
    return v3

    :cond_0
    move v1, v2

    move v3, v2

    :goto_1
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_3

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->e:Lcom/google/protobuf/ByteString;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v3, v0

    :cond_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_4

    const/4 v0, 0x4

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->f:I

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v0

    add-int/2addr v3, v0

    :cond_4
    iput v3, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->h:I

    goto :goto_0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->c:Ljava/util/List;

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->d:Ljava/util/List;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->e:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->f:I

    return v0
.end method

.method public final k()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->g:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->g:B

    goto :goto_0
.end method
