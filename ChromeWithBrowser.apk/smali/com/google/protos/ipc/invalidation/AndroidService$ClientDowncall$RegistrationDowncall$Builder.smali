.class public final Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncallOrBuilder;


# instance fields
.field private a:I

.field private b:Ljava/util/List;

.field private c:Ljava/util/List;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->i()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->g()V

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->h()V

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->b(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->b(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    :cond_2
    :goto_1
    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->c(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->c(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->g()V

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b:Ljava/util/List;

    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->b(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->h()V

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c:Ljava/util/List;

    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->c(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->g()V

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Iterable;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->h()V

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->i()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;
    .locals 3

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;B)V

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;Ljava/util/List;)Ljava/util/List;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->b(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;Ljava/util/List;)Ljava/util/List;

    return-object v0
.end method
