.class public final Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncallOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

.field private f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

.field private g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

.field private h:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->a:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->c:J

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->i:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->j:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->i:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->j:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    return-object p1
.end method

.method public static a([B)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->a:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IJ)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_5
    return-void
.end method

.method public final c()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->c:J

    invoke-static {v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->j:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()J
    .locals 2

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->c:J

    return-wide v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    return-object v0
.end method

.method public final q()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->i:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->i:B

    goto :goto_0
.end method
