.class public final Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncallOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private c:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

.field private d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

.field private e:Z

.field private f:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->o()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;

    :cond_3
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_7

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    :cond_6
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    move-result-object v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    goto/16 :goto_0

    :sswitch_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->e:Z

    goto/16 :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_a

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    :cond_9
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;
    .locals 5

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    move-result-object v2

    if-eq v1, v2, :cond_4

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v4

    if-eq v3, v4, :cond_5

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    :goto_0
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    :cond_0
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->h()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    move-result-object v4

    if-eq v3, v4, :cond_6

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    :goto_1
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    :cond_1
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->j()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_7

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    move-result-object v4

    if-eq v3, v4, :cond_7

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    :goto_2
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    :cond_2
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->k()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->l()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    :cond_3
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->m()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->n()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v1

    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_8

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v3

    if-eq v2, v3, :cond_8

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    invoke-static {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    :goto_3
    iget v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    :cond_4
    return-object v0

    :cond_5
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    goto/16 :goto_0

    :cond_6
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    goto :goto_1

    :cond_7
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    goto :goto_2

    :cond_8
    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    goto :goto_3
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->e:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;Z)Z

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;I)I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    return-object p0
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->e:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->o()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v0

    return-object v0
.end method
