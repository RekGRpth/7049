.class public final Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcallOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;


# instance fields
.field private b:I

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Z

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->a:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->c:I

    const-string v1, ""

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->d:Ljava/lang/Object;

    iput-boolean v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->e:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->f:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->g:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->f:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->g:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->c:I

    return p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->b:I

    return p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->a:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    return-object v0
.end method

.method private l()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->a(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->d:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->l()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_2
    return-void
.end method

.method public final c()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->c:I

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->l()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->e:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->g:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->c:I

    return v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->a(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->e:Z

    return v0
.end method

.method public final k()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->f:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->f:B

    goto :goto_0
.end method
