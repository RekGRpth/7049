.class public final Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcallOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protobuf/ByteString;

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

.field private e:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->b:Lcom/google/protobuf/ByteString;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->m()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->b:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :sswitch_3
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    :cond_3
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_2

    :sswitch_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->e:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->f()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v2

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v2

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    :goto_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->l()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    goto :goto_0

    :cond_5
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    goto :goto_1

    :cond_6
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    goto :goto_2
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    return-object p0
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->e:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->b:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->m()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->b:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->e:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;Z)Z

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;I)I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
