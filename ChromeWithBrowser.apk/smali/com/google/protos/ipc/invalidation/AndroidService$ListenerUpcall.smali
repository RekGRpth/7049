.class public final Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcallOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

.field private f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

.field private g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

.field private h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

.field private i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

.field private j:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->c:J

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->j:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->k:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->l:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->k:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->l:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->j:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    return-object p1
.end method

.method public static a([B)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IJ)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_6
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->j:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_7
    return-void
.end method

.method public final c()I
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->l:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->c:J

    invoke-static {v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->j:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->l:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()J
    .locals 2

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->c:J

    return-wide v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    return-object v0
.end method

.method public final q()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    return-object v0
.end method

.method public final s()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->j:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    return-object v0
.end method

.method public final u()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->k:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->k:B

    goto :goto_0
.end method
