.class public final Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadataOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->c:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->e:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->f:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->g:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->h:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->i:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->u()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 3

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    :cond_1
    invoke-virtual {p1, v2, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->c:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->d:I

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->e:Ljava/lang/Object;

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->f:Ljava/lang/Object;

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->g:Ljava/lang/Object;

    goto :goto_0

    :sswitch_7
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->h:Ljava/lang/Object;

    goto :goto_0

    :sswitch_8
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->i:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->d:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->d()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    if-eq v1, v2, :cond_9

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->j()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a(I)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->b(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->c(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->o()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->d(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->q()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->e(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->f(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    goto/16 :goto_0

    :cond_9
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    goto :goto_1
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->c:Ljava/lang/Object;

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->e:Ljava/lang/Object;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->f:Ljava/lang/Object;

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->u()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->g:Ljava/lang/Object;

    return-object p0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;-><init>(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->d:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;I)I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->b(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->f:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->c(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->g:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->d(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->h:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->e(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->i:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->f(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->b(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;I)I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->h:Ljava/lang/Object;

    return-object p0
.end method

.method public final f(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->i:Ljava/lang/Object;

    return-object p0
.end method
