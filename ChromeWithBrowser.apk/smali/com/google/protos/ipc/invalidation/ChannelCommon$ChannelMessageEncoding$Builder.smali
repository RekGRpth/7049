.class public final Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncodingOrBuilder;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;)Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;->e()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->e()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method static synthetic c()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;-><init>()V

    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;
    .locals 1

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :pswitch_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private d()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;->e()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->d()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;-><init>(Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;B)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;->d()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;->d()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;->d()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;

    move-result-object v0

    return-object v0
.end method
