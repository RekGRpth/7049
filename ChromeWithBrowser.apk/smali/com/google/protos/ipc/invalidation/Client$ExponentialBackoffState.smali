.class public final Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffStateOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;


# instance fields
.field private b:I

.field private c:I

.field private d:Z

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->a:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    iput v1, v0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->c:I

    iput-boolean v1, v0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->d:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->e:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->f:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->e:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->f:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;-><init>(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->c:I

    return p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->newBuilder()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->b:I

    return p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->a:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->e()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->newBuilder()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->d:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_1
    return-void
.end method

.method public final c()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->c:I

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->d:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->f:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->c:I

    return v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->d:Z

    return v0
.end method

.method public final i()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->e:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->e:B

    goto :goto_0
.end method
