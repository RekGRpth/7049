.class public final enum Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# static fields
.field public static final enum a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

.field public static final enum b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

.field public static final enum c:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->c:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    new-array v0, v5, [Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    sget-object v1, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->c:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    aput-object v1, v0, v3

    new-instance v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State$1;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State$1;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->d:I

    return-void
.end method

.method public static a(I)Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->c:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->d:I

    return v0
.end method
