.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigPOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Z

.field private k:I

.field private l:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

.field private m:Z

.field private n:I

.field private o:Z

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const v3, 0xea60

    const/4 v2, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    iput v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d:I

    const/16 v1, 0x2710

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->e:I

    const v1, 0x124f80

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->f:I

    const v1, 0x1499700

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->g:I

    const/16 v1, 0x1f4

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->h:I

    const/16 v1, 0x14

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->i:I

    iput-boolean v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->j:Z

    const/16 v1, 0x7d0

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->k:I

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->l:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    iput-boolean v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->m:Z

    iput v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->n:I

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->o:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->p:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->q:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->p:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->q:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d:I

    return p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->l:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->j:Z

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->e:I

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->m:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->f:I

    return p1
.end method

.method static synthetic c(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->o:Z

    return p1
.end method

.method static synthetic d(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->g:I

    return p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    return-object v0
.end method

.method static synthetic e(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->h:I

    return p1
.end method

.method static synthetic f(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->i:I

    return p1
.end method

.method static synthetic g(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->k:I

    return p1
.end method

.method static synthetic h(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->n:I

    return p1
.end method

.method static synthetic i(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    return p1
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final B()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->n:I

    return v0
.end method

.method public final C()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final D()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->o:Z

    return v0
.end method

.method public final E()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->p:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->p:B

    goto :goto_0
.end method

.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->f:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_6
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->j:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_7
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    const/16 v0, 0x9

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_8
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->l:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_9
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_a
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    const/16 v0, 0xc

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_b
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_c
    return-void
.end method

.method public final c()I
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->q:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->f:I

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->j:Z

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->k:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->l:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->m:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    const/16 v1, 0xc

    iget v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->n:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_d

    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->o:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->q:I

    goto/16 :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d:I

    return v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->e:I

    return v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->f:I

    return v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->g:I

    return v0
.end method

.method public final o()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->h:I

    return v0
.end method

.method public final q()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->i:I

    return v0
.end method

.method public final s()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->j:Z

    return v0
.end method

.method public final u()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->k:I

    return v0
.end method

.method public final w()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->l:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    return-object v0
.end method

.method public final y()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->m:Z

    return v0
.end method
