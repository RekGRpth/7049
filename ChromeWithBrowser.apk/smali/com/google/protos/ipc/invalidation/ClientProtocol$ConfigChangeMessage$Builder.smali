.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessageOrBuilder;


# instance fields
.field private a:I

.field private b:J


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->g()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->b:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->f()J

    move-result-wide v0

    iget v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->a:I

    iput-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->b:J

    goto :goto_0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-wide v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->b:J

    invoke-static {v2, v3, v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;J)J

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;I)I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v0

    return-object v0
.end method
