.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessageOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

.field private c:Ljava/util/List;

.field private d:Ljava/util/List;

.field private e:Z

.field private f:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d:Ljava/util/List;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->o()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->g()V

    iget-object v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->e:Z

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    :cond_4
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    move-result-object v2

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    :cond_2
    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    :cond_3
    :goto_2
    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->c(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->c(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    :cond_4
    :goto_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->l()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->n()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_9

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v2

    if-eq v1, v2, :cond_9

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    :goto_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    goto/16 :goto_0

    :cond_6
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    goto/16 :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->g()V

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c:Ljava/util/List;

    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_8
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->h()V

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d:Ljava/util/List;

    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->c(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_9
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    goto :goto_4
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->h()V

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->e:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->o()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;Ljava/util/List;)Ljava/util/List;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;Ljava/util/List;)Ljava/util/List;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->e:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;Z)Z

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;I)I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method
