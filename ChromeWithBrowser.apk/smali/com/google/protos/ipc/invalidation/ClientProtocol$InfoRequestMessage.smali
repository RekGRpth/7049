.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessageOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;


# instance fields
.field private b:Ljava/util/List;

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->b:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->c:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->d:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->c:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->d:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;)V

    return-void
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->b:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->b:Ljava/util/List;

    return-object v0
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->c()I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;->a()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(II)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c()I
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->d:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v1, v0

    move v2, v0

    :goto_1
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;->a()I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->b(I)I

    move-result v0

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x0

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->d:I

    goto :goto_0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->b:Ljava/util/List;

    return-object v0
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->c:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->c:B

    goto :goto_0
.end method
