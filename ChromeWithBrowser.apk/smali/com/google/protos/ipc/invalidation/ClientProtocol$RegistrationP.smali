.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationPOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->e:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->f:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->e:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->f:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    return-object p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->b()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(II)V

    :cond_1
    return-void
.end method

.method public final c()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->b()I

    move-result v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->f:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    return-object v0
.end method

.method public final i()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->e:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->e:B

    goto :goto_0
.end method
