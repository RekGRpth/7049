.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->i()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    :cond_4
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;
    .locals 5

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;

    move-result-object v2

    if-eq v1, v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v4

    if-eq v3, v4, :cond_2

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    :goto_0
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    :cond_0
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v1

    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v3

    if-eq v2, v3, :cond_3

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    invoke-static {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    :goto_1
    iget v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    :cond_1
    return-object v0

    :cond_2
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    goto :goto_0

    :cond_3
    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;I)I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;

    move-result-object v0

    return-object v0
.end method
