.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->e:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->f:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->e:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->f:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    return-void
.end method

.method public final c()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->f:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    return-object v0
.end method

.method public final i()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->e:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatus;->e:B

    goto :goto_0
.end method
