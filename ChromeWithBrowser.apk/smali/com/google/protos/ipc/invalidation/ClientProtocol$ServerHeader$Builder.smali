.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeaderOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

.field private c:Lcom/google/protobuf/ByteString;

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

.field private e:J

.field private f:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    sget-object v0, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->c:Lcom/google/protobuf/ByteString;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->f:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->o()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->c:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;

    :cond_4
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->b()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->e:J

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->f:Ljava/lang/Object;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v2

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    goto :goto_1

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->c:Lcom/google/protobuf/ByteString;

    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->i()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v2

    if-eq v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    :goto_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    :cond_6
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->k()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->l()J

    move-result-wide v0

    iget v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    iput-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->e:J

    :cond_7
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->n()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    goto :goto_2

    :cond_9
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->f:Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->c:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->e:J

    invoke-static {v2, v4, v5}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;J)J

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->f:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;I)I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    move-result-object v0

    return-object v0
.end method
