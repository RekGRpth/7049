.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessageOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

.field private e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

.field private f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

.field private g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

.field private h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

.field private i:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->u()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method static synthetic c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;-><init>()V

    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    :cond_4
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_8

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;

    :cond_7
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v0

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v0, v2

    goto :goto_3

    :cond_9
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    goto/16 :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_b

    move v0, v1

    :goto_4
    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage$Builder;

    :cond_a
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    move-result-object v0

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    move v0, v2

    goto :goto_4

    :cond_c
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    goto/16 :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_e

    move v0, v1

    :goto_5
    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    :cond_d
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    move-result-object v0

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    move v0, v2

    goto :goto_5

    :cond_f
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    goto/16 :goto_0

    :sswitch_6
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_11

    move v0, v1

    :goto_6
    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    :cond_10
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    move-result-object v0

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    move v0, v2

    goto :goto_6

    :cond_12
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    goto/16 :goto_0

    :sswitch_7
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_14

    move v0, v1

    :goto_7
    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;

    :cond_13
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    move-result-object v0

    if-nez v0, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_14
    move v0, v2

    goto :goto_7

    :cond_15
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    goto/16 :goto_0

    :sswitch_8
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_17

    move v0, v1

    :goto_8
    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Builder;

    :cond_16
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    move-result-object v0

    if-nez v0, :cond_18

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_17
    move v0, v2

    goto :goto_8

    :cond_18
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method private d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;
    .locals 5

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;

    move-result-object v2

    if-eq v1, v2, :cond_7

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_8

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v4

    if-eq v3, v4, :cond_8

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    :goto_0
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    :cond_0
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_9

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    move-result-object v4

    if-eq v3, v4, :cond_9

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    :goto_1
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    :cond_1
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_a

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v4

    if-eq v3, v4, :cond_a

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    :goto_2
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    :cond_2
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->k()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->l()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_b

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    move-result-object v4

    if-eq v3, v4, :cond_b

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    :goto_3
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    :cond_3
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->m()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->n()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_c

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    move-result-object v4

    if-eq v3, v4, :cond_c

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    :goto_4
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    :cond_4
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->o()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->p()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_d

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    move-result-object v4

    if-eq v3, v4, :cond_d

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    :goto_5
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    :cond_5
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->q()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->r()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_e

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    move-result-object v4

    if-eq v3, v4, :cond_e

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    :goto_6
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    :cond_6
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->s()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->t()Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    move-result-object v1

    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_f

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    move-result-object v3

    if-eq v2, v3, :cond_f

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    invoke-static {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    :goto_7
    iget v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    :cond_7
    return-object v0

    :cond_8
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    goto/16 :goto_0

    :cond_9
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    goto/16 :goto_1

    :cond_a
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    goto/16 :goto_2

    :cond_b
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    goto/16 :goto_3

    :cond_c
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    goto/16 :goto_4

    :cond_d
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    goto :goto_5

    :cond_e
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    goto :goto_6

    :cond_f
    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    goto :goto_7
.end method

.method private e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;I)I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    move-result-object v0

    return-object v0
.end method
