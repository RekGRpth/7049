.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessageOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

.field private e:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

.field private f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

.field private g:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

.field private h:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

.field private i:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

.field private j:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->j:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->k:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->l:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->k:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->l:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->j:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    return-object p1
.end method

.method public static a([B)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_6
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->j:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_7
    return-void
.end method

.method public final c()I
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->l:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->j:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->l:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    return-object v0
.end method

.method public final q()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->i:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    return-object v0
.end method

.method public final s()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->j:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    return-object v0
.end method

.method public final u()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->k:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->k:B

    goto :goto_0
.end method
