.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusPOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

.field private c:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->c:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->i()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->f()I

    move-result v0

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a:I

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->c:Ljava/lang/Object;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a:I

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a:I

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->c:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;I)I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v0

    return-object v0
.end method
