.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusPOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

.field private d:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->d:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->e:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->f:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->e:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->f:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->b:I

    return p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->d:Ljava/lang/Object;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    return-object v0
.end method

.method private j()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->a(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->d:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;->a()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->b(II)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->j()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_1
    return-void
.end method

.method public final c()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;->a()I

    move-result v0

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->j()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->f:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP$Code;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->a(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final i()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->e:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$StatusP;->e:B

    goto :goto_0
.end method
