.class public final Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientStateOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

.field private c:Lcom/google/protobuf/ByteString;

.field private d:Lcom/google/protobuf/ByteString;

.field private e:Z

.field private f:J

.field private g:Z

.field private h:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

.field private i:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

.field private j:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

.field private k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

.field private l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

.field private m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

.field private n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

.field private o:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

.field private p:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->d()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    sget-object v0, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->c:Lcom/google/protobuf/ByteString;

    sget-object v0, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->d:Lcom/google/protobuf/ByteString;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->h:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->i:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->d()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->o:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->p:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->I()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->newBuilder()Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$RunStateP;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$RunStateP;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->c:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->d:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->e:Z

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->b()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->f:J

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->g:Z

    goto :goto_0

    :sswitch_7
    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->h:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    :cond_3
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_2

    :sswitch_8
    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->i:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;)Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;

    :cond_5
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_3

    :sswitch_9
    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    :cond_7
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto :goto_4

    :sswitch_a
    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    :cond_9
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto :goto_5

    :sswitch_b
    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_c

    move v0, v1

    :goto_6
    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    :cond_b
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->c(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto :goto_6

    :sswitch_c
    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v4, 0x800

    if-ne v0, v4, :cond_e

    move v0, v1

    :goto_7
    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    :cond_d
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->d(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    goto/16 :goto_0

    :cond_e
    move v0, v2

    goto :goto_7

    :sswitch_d
    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v0, v4, :cond_10

    move v0, v1

    :goto_8
    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    :cond_f
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->e(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    goto/16 :goto_0

    :cond_10
    move v0, v2

    goto :goto_8

    :sswitch_e
    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->newBuilder()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v0, v4, :cond_12

    move v0, v1

    :goto_9
    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->o:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    :cond_11
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    goto/16 :goto_0

    :cond_12
    move v0, v2

    goto :goto_9

    :sswitch_f
    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v0, v4, :cond_14

    move v0, v1

    :goto_a
    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->p:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;)Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;

    :cond_13
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    goto/16 :goto_0

    :cond_14
    move v0, v2

    goto :goto_a

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->f()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->f:J

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->o:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/Client$RunStateP;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->f()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_10

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->d()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v2

    if-eq v1, v2, :cond_10

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->a(Lcom/google/protos/ipc/invalidation/Client$RunStateP;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$RunStateP;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->j()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->c(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->l()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->n()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(J)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->o()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->p()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b(Z)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->q()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->r()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_11

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->h:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v2

    if-eq v1, v2, :cond_11

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->h:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->h:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    :goto_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    :cond_8
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->s()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->t()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_12

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->i:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    move-result-object v2

    if-eq v1, v2, :cond_12

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->i:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;)Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;)Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->i:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    :goto_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    :cond_9
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->u()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->v()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_13

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v2

    if-eq v1, v2, :cond_13

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    :goto_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    :cond_a
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->w()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->x()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_14

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v2

    if-eq v1, v2, :cond_14

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    :goto_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    :cond_b
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->y()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->z()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_15

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v2

    if-eq v1, v2, :cond_15

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    :goto_6
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    :cond_c
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->A()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->B()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_16

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v2

    if-eq v1, v2, :cond_16

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    :goto_7
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    :cond_d
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->C()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->D()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_17

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v2

    if-eq v1, v2, :cond_17

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    :goto_8
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    :cond_e
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->E()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->F()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_18

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->o:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->d()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v2

    if-eq v1, v2, :cond_18

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->o:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->o:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    :goto_9
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    :cond_f
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->H()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_19

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->p:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    move-result-object v2

    if-eq v1, v2, :cond_19

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->p:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;)Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;)Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->p:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    :goto_a
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    goto/16 :goto_0

    :cond_10
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    goto/16 :goto_1

    :cond_11
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->h:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    goto/16 :goto_2

    :cond_12
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->i:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    goto/16 :goto_3

    :cond_13
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    goto/16 :goto_4

    :cond_14
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    goto/16 :goto_5

    :cond_15
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    goto/16 :goto_6

    :cond_16
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    goto/16 :goto_7

    :cond_17
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    goto/16 :goto_8

    :cond_18
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->o:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    goto :goto_9

    :cond_19
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->p:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    goto :goto_a
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->h:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->i:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->p:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    return-object p0
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->e:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->f()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->c:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final b(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    return-object p0
.end method

.method public final b(Z)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->g:Z

    return-object p0
.end method

.method public final c(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->d:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final c(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->I()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->f()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    return-object p0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;-><init>(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_e

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/Client$RunStateP;)Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->c:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->d:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->e:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Z)Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-wide v4, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->f:J

    invoke-static {v2, v4, v5}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;J)J

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->g:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Z)Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->h:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->i:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;)Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->c(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->d(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    and-int/lit16 v1, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->e(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    and-int/lit16 v1, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v1, v4, :cond_c

    or-int/lit16 v0, v0, 0x2000

    :cond_c
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->o:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    and-int/lit16 v1, v3, 0x4000

    const/16 v3, 0x4000

    if-ne v1, v3, :cond_d

    or-int/lit16 v0, v0, 0x4000

    :cond_d
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->p:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;)Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;I)I

    return-object v2

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public final e(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a:I

    return-object p0
.end method
