.class public final Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskStateOrBuilder;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Z

.field private e:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->d()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->e:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->m()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;
    .locals 3

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->b:I

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->c:I

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d:Z

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->newBuilder()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->e:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->f()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->b:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->e:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(I)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->b(I)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->j()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->l()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->e:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->d()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v2

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->e:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->e:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    goto :goto_0

    :cond_5
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->e:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    goto :goto_1
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->f()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->c:I

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->m()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->f()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;-><init>(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->b:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;I)I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->c:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;I)I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->d:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;Z)Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->e:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->c(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;I)I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
