.class public final Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskStateOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:Z

.field private f:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->a:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    iput v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->c:I

    iput v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d:I

    iput-boolean v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->e:Z

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->d()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->f:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->g:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->h:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->g:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->h:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;-><init>(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->c:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->f:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    return-object p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d:I

    return p1
.end method

.method static synthetic c(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    return p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->a:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->e()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->f:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    return-void
.end method

.method public final c()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->c:I

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->e:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->f:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->h:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->c:I

    return v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d:I

    return v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->e:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->f:Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    return-object v0
.end method

.method public final m()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->g:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->g:B

    goto :goto_0
.end method
