.class public final Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsStateOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;


# instance fields
.field private b:Ljava/util/List;

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->a:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->b:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->c:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->d:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->c:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->d:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;-><init>(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;)V

    return-void
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;)Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;)Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->b:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->b:Ljava/util/List;

    return-object v0
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->a:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;->e()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState$Builder;)Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->c()I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c()I
    .locals 4

    const/4 v0, 0x0

    iget v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->d:I

    const/4 v1, -0x1

    if-eq v2, v1, :cond_0

    :goto_0
    return v2

    :cond_0
    move v1, v0

    move v2, v0

    :goto_1
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iput v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->d:I

    goto :goto_0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->b:Ljava/util/List;

    return-object v0
.end method

.method public final f()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->c:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->c:B

    goto :goto_0
.end method
