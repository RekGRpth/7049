.class public final enum Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# static fields
.field public static final enum a:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

.field public static final enum b:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

.field public static final enum c:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

.field private static enum d:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

.field private static enum e:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    const-string v1, "INTERNAL"

    invoke-direct {v0, v1, v6, v3}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->a:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    const-string v1, "TEST"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->b:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    const-string v1, "DEMO"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->d:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    const-string v1, "CHROME_SYNC"

    const/16 v2, 0x3ec

    invoke-direct {v0, v1, v7, v2}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->e:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    const-string v1, "CHROME_SYNC_ANDROID"

    const/16 v2, 0x3fa

    invoke-direct {v0, v1, v5, v2}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->c:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->a:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->b:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->d:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->e:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->c:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    aput-object v1, v0, v5

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type$1;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type$1;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->f:I

    return-void
.end method

.method public static a(I)Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->a:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    goto :goto_0

    :sswitch_1
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->b:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    goto :goto_0

    :sswitch_2
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->d:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    goto :goto_0

    :sswitch_3
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->e:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    goto :goto_0

    :sswitch_4
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->c:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x3ec -> :sswitch_3
        0x3fa -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->f:I

    return v0
.end method
