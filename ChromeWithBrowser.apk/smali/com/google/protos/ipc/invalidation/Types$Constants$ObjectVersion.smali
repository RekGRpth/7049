.class public final enum Lcom/google/protos/ipc/invalidation/Types$Constants$ObjectVersion;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# static fields
.field private static enum a:Lcom/google/protos/ipc/invalidation/Types$Constants$ObjectVersion;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$Constants$ObjectVersion;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/protos/ipc/invalidation/Types$Constants$ObjectVersion;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$Constants$ObjectVersion;->a:Lcom/google/protos/ipc/invalidation/Types$Constants$ObjectVersion;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/protos/ipc/invalidation/Types$Constants$ObjectVersion;

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$Constants$ObjectVersion;->a:Lcom/google/protos/ipc/invalidation/Types$Constants$ObjectVersion;

    aput-object v1, v0, v2

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$Constants$ObjectVersion$1;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Types$Constants$ObjectVersion$1;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method
