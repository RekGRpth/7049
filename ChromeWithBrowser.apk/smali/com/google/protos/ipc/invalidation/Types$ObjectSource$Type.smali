.class public final enum Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# static fields
.field public static final enum a:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

.field public static final enum b:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

.field public static final enum c:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

.field private static enum d:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

.field private static enum e:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

.field private static enum f:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

.field private static enum g:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;


# instance fields
.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    const-string v1, "INTERNAL"

    invoke-direct {v0, v1, v7, v4}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->a:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    const-string v1, "TEST"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->b:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    const-string v1, "DEMO"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->d:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    const-string v1, "CHROME_SYNC"

    const/16 v2, 0x3ec

    invoke-direct {v0, v1, v8, v2}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->c:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    const-string v1, "COSMO_CHANGELOG"

    const/16 v2, 0x3f6

    invoke-direct {v0, v1, v6, v2}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->e:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    const-string v1, "CHROME_COMPONENTS"

    const/4 v2, 0x5

    const/16 v3, 0x401

    invoke-direct {v0, v1, v2, v3}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->f:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    const-string v1, "CHROME_PUSH_MESSAGING"

    const/4 v2, 0x6

    const/16 v3, 0x406

    invoke-direct {v0, v1, v2, v3}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->g:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->a:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->b:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->d:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->c:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->e:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    sget-object v2, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->f:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->g:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    aput-object v2, v0, v1

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type$1;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type$1;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->h:I

    return-void
.end method

.method public static a(I)Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->a:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    goto :goto_0

    :sswitch_1
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->b:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    goto :goto_0

    :sswitch_2
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->d:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    goto :goto_0

    :sswitch_3
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->c:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    goto :goto_0

    :sswitch_4
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->e:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    goto :goto_0

    :sswitch_5
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->f:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    goto :goto_0

    :sswitch_6
    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->g:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x3ec -> :sswitch_3
        0x3f6 -> :sswitch_4
        0x401 -> :sswitch_5
        0x406 -> :sswitch_6
    .end sparse-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->h:I

    return v0
.end method
