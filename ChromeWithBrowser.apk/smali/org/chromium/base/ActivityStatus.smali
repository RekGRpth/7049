.class public Lorg/chromium/base/ActivityStatus;
.super Ljava/lang/Object;


# static fields
.field public static final CREATED:I = 0x1

.field public static final DESTROYED:I = 0x6

.field public static final PAUSED:I = 0x4

.field public static final RESUMED:I = 0x3

.field public static final STARTED:I = 0x2

.field public static final STOPPED:I = 0x5

.field private static sActivity:Landroid/app/Activity;

.field private static sActivityState:I

.field private static final sStateListeners:Ljava/util/concurrent/CopyOnWriteArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lorg/chromium/base/ActivityStatus;->sStateListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getActivity()Landroid/app/Activity;
    .locals 1

    sget-object v0, Lorg/chromium/base/ActivityStatus;->sActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public static getState()I
    .locals 1

    sget v0, Lorg/chromium/base/ActivityStatus;->sActivityState:I

    return v0
.end method

.method public static isPaused()Z
    .locals 2

    sget v0, Lorg/chromium/base/ActivityStatus;->sActivityState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static onStateChange(Landroid/app/Activity;I)V
    .locals 2

    sget-object v0, Lorg/chromium/base/ActivityStatus;->sActivity:Landroid/app/Activity;

    if-eq v0, p0, :cond_0

    sput-object p0, Lorg/chromium/base/ActivityStatus;->sActivity:Landroid/app/Activity;

    :cond_0
    sput p1, Lorg/chromium/base/ActivityStatus;->sActivityState:I

    sget-object v0, Lorg/chromium/base/ActivityStatus;->sStateListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/base/ActivityStatus$StateListener;

    invoke-interface {v0, p1}, Lorg/chromium/base/ActivityStatus$StateListener;->onActivityStateChange(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x6

    if-ne p1, v0, :cond_2

    const/4 v0, 0x0

    sput-object v0, Lorg/chromium/base/ActivityStatus;->sActivity:Landroid/app/Activity;

    :cond_2
    return-void
.end method

.method public static registerStateListener(Lorg/chromium/base/ActivityStatus$StateListener;)V
    .locals 1

    sget-object v0, Lorg/chromium/base/ActivityStatus;->sStateListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static unregisterStateListener(Lorg/chromium/base/ActivityStatus$StateListener;)V
    .locals 1

    sget-object v0, Lorg/chromium/base/ActivityStatus;->sStateListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
