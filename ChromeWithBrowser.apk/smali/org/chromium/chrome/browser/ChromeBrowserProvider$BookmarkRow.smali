.class Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;
.super Ljava/lang/Object;


# instance fields
.field created:Ljava/lang/Long;

.field date:Ljava/lang/Long;

.field favicon:[B

.field isBookmark:Ljava/lang/Boolean;

.field parentId:J

.field title:Ljava/lang/String;

.field url:Ljava/lang/String;

.field visits:Ljava/lang/Integer;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static fromContentValues(Landroid/content/ContentValues;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;
    .locals 3

    const/4 v1, 0x0

    new-instance v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;

    invoke-direct {v2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;-><init>()V

    const-string v0, "url"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "url"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->url:Ljava/lang/String;

    :cond_0
    const-string v0, "bookmark"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "bookmark"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->isBookmark:Ljava/lang/Boolean;

    :cond_1
    const-string v0, "created"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "created"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->created:Ljava/lang/Long;

    :cond_2
    const-string v0, "date"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "date"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->date:Ljava/lang/Long;

    :cond_3
    const-string v0, "favicon"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "favicon"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->favicon:[B

    iget-object v0, v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->favicon:[B

    if-nez v0, :cond_4

    new-array v0, v1, [B

    iput-object v0, v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->favicon:[B

    :cond_4
    const-string v0, "title"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "title"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->title:Ljava/lang/String;

    :cond_5
    const-string v0, "visits"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "visits"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->visits:Ljava/lang/Integer;

    :cond_6
    const-string v0, "parentId"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "parentId"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->parentId:J

    :cond_7
    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method
