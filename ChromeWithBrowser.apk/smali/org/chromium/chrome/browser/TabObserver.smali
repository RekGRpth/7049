.class public interface abstract Lorg/chromium/chrome/browser/TabObserver;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onCloseTab(Lorg/chromium/chrome/browser/TabBase;)V
.end method

.method public abstract onLoadProgressChanged(Lorg/chromium/chrome/browser/TabBase;I)V
.end method

.method public abstract onUpdateUrl(Lorg/chromium/chrome/browser/TabBase;Ljava/lang/String;)V
.end method
