.class public Lorg/chromium/chrome/browser/autofill/AutofillPopup;
.super Landroid/widget/ListPopupWindow;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final ITEM_ID_AUTOCOMPLETE_ENTRY:I = 0x0

.field private static final ITEM_ID_DATA_LIST_ENTRY:I = -0x6

.field private static final ITEM_ID_PASSWORD_ENTRY:I = -0x2

.field private static final TEXT_PADDING_DP:I = 0x1e


# instance fields
.field private mAnchorRect:Landroid/graphics/Rect;

.field private mAnchorView:Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;

.field private final mAutofillCallback:Lorg/chromium/chrome/browser/autofill/AutofillPopup$AutofillPopupDelegate;

.field private final mContainerViewDelegate:Lorg/chromium/content/browser/ContainerViewDelegate;

.field private mLabelViewPaint:Landroid/graphics/Paint;

.field private mNameViewPaint:Landroid/graphics/Paint;

.field private final mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;


# direct methods
.method public constructor <init>(Lorg/chromium/ui/gfx/NativeWindow;Lorg/chromium/content/browser/ContainerViewDelegate;Lorg/chromium/chrome/browser/autofill/AutofillPopup$AutofillPopupDelegate;)V
    .locals 2

    invoke-virtual {p1}, Lorg/chromium/ui/gfx/NativeWindow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    iput-object p2, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mContainerViewDelegate:Lorg/chromium/content/browser/ContainerViewDelegate;

    iput-object p3, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mAutofillCallback:Lorg/chromium/chrome/browser/autofill/AutofillPopup$AutofillPopupDelegate;

    invoke-virtual {p0, p0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    invoke-virtual {v1}, Lorg/chromium/ui/gfx/NativeWindow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/autofill/AutofillPopup;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mAnchorView:Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mContainerViewDelegate:Lorg/chromium/content/browser/ContainerViewDelegate;

    iget-object v1, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mAnchorView:Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;

    invoke-interface {v0, v1}, Lorg/chromium/content/browser/ContainerViewDelegate;->addViewToContainerView(Landroid/view/View;)V

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mAnchorView:Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->setAnchorView(Landroid/view/View;)V

    return-void
.end method

.method private getDesiredWidth([Lorg/chromium/chrome/browser/autofill/AutofillSuggestion;)I
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mNameViewPaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mLabelViewPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    invoke-virtual {v0}, Lorg/chromium/ui/gfx/NativeWindow;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v2, Lorg/chromium/chrome/R$layout;->autofill_text:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v0, Lorg/chromium/chrome/R$id;->autofill_name:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mNameViewPaint:Landroid/graphics/Paint;

    sget v0, Lorg/chromium/chrome/R$id;->autofill_label:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mLabelViewPaint:Landroid/graphics/Paint;

    :cond_1
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    move v0, v1

    move v2, v1

    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_2

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    aget-object v4, p1, v0

    iget-object v4, v4, Lorg/chromium/chrome/browser/autofill/AutofillSuggestion;->mName:Ljava/lang/String;

    iget-object v5, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mNameViewPaint:Landroid/graphics/Paint;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v4, v1, v6, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    aget-object v5, p1, v0

    iget-object v5, v5, Lorg/chromium/chrome/browser/autofill/AutofillSuggestion;->mLabel:Ljava/lang/String;

    iget-object v6, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mLabelViewPaint:Landroid/graphics/Paint;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v5, v1, v7, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/high16 v0, 0x41f00000

    iget-object v1, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    invoke-virtual {v1}, Lorg/chromium/ui/gfx/NativeWindow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    add-int/2addr v0, v2

    return v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mAutofillCallback:Lorg/chromium/chrome/browser/autofill/AutofillPopup$AutofillPopupDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AutofillPopupDelegate;->requestHide()V

    return-void
.end method

.method public hide()V
    .locals 2

    invoke-super {p0}, Landroid/widget/ListPopupWindow;->dismiss()V

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mContainerViewDelegate:Lorg/chromium/content/browser/ContainerViewDelegate;

    iget-object v1, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mAnchorView:Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;

    invoke-interface {v0, v1}, Lorg/chromium/content/browser/ContainerViewDelegate;->removeViewFromContainerView(Landroid/view/View;)V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mAutofillCallback:Lorg/chromium/chrome/browser/autofill/AutofillPopup$AutofillPopupDelegate;

    invoke-interface {v0, p3}, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AutofillPopupDelegate;->suggestionSelected(I)V

    return-void
.end method

.method public setAnchorRect(FFFF)V
    .locals 6

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    invoke-virtual {v0}, Lorg/chromium/ui/gfx/NativeWindow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/gfx/DeviceDisplayInfo;->create(Landroid/content/Context;)Lorg/chromium/ui/gfx/DeviceDisplayInfo;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/ui/gfx/DeviceDisplayInfo;->getDIPScale()D

    move-result-wide v0

    double-to-float v0, v0

    new-instance v1, Landroid/graphics/Rect;

    mul-float v2, p1, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    mul-float v3, p2, v0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-float v4, p1, p3

    mul-float/2addr v4, v0

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    add-float v5, p2, p4

    mul-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mAnchorRect:Landroid/graphics/Rect;

    return-void
.end method

.method public show([Lorg/chromium/chrome/browser/autofill/AutofillSuggestion;)V
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    aget-object v2, p1, v0

    iget v2, v2, Lorg/chromium/chrome/browser/autofill/AutofillSuggestion;->mUniqueId:I

    if-gtz v2, :cond_0

    if-eqz v2, :cond_0

    const/4 v3, -0x2

    if-eq v2, v3, :cond_0

    const/4 v3, -0x6

    if-ne v2, v3, :cond_1

    :cond_0
    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v0, Lorg/chromium/chrome/browser/autofill/AutofillListAdapter;

    iget-object v2, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    invoke-virtual {v2}, Lorg/chromium/ui/gfx/NativeWindow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lorg/chromium/chrome/browser/autofill/AutofillListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mAnchorView:Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->mAnchorRect:Landroid/graphics/Rect;

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->getDesiredWidth([Lorg/chromium/chrome/browser/autofill/AutofillSuggestion;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;->setSize(Landroid/graphics/Rect;I)V

    return-void
.end method
