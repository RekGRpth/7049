.class public Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/chrome/browser/autofill/AutofillPopup$AutofillPopupDelegate;


# instance fields
.field private final mAutofillPopup:Lorg/chromium/chrome/browser/autofill/AutofillPopup;

.field private final mNativeAutofillPopup:I


# direct methods
.method public constructor <init>(ILorg/chromium/ui/gfx/NativeWindow;Lorg/chromium/content/browser/ContainerViewDelegate;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;->mNativeAutofillPopup:I

    new-instance v0, Lorg/chromium/chrome/browser/autofill/AutofillPopup;

    invoke-direct {v0, p2, p3, p0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup;-><init>(Lorg/chromium/ui/gfx/NativeWindow;Lorg/chromium/content/browser/ContainerViewDelegate;Lorg/chromium/chrome/browser/autofill/AutofillPopup$AutofillPopupDelegate;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;->mAutofillPopup:Lorg/chromium/chrome/browser/autofill/AutofillPopup;

    return-void
.end method

.method private static create(ILorg/chromium/ui/gfx/NativeWindow;Lorg/chromium/content/browser/ContainerViewDelegate;)Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;
    .locals 1

    new-instance v0, Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;

    invoke-direct {v0, p0, p1, p2}, Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;-><init>(ILorg/chromium/ui/gfx/NativeWindow;Lorg/chromium/content/browser/ContainerViewDelegate;)V

    return-object v0
.end method

.method private static createAutofillSuggestion(Ljava/lang/String;Ljava/lang/String;I)Lorg/chromium/chrome/browser/autofill/AutofillSuggestion;
    .locals 1

    new-instance v0, Lorg/chromium/chrome/browser/autofill/AutofillSuggestion;

    invoke-direct {v0, p0, p1, p2}, Lorg/chromium/chrome/browser/autofill/AutofillSuggestion;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method private hide()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;->mAutofillPopup:Lorg/chromium/chrome/browser/autofill/AutofillPopup;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->hide()V

    return-void
.end method

.method private native nativeRequestHide(I)V
.end method

.method private native nativeSuggestionSelected(II)V
.end method

.method private setAnchorRect(FFFF)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;->mAutofillPopup:Lorg/chromium/chrome/browser/autofill/AutofillPopup;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->setAnchorRect(FFFF)V

    return-void
.end method

.method private show([Lorg/chromium/chrome/browser/autofill/AutofillSuggestion;)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;->mAutofillPopup:Lorg/chromium/chrome/browser/autofill/AutofillPopup;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->show([Lorg/chromium/chrome/browser/autofill/AutofillSuggestion;)V

    return-void
.end method


# virtual methods
.method public requestHide()V
    .locals 1

    iget v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;->mNativeAutofillPopup:I

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;->nativeRequestHide(I)V

    return-void
.end method

.method public suggestionSelected(I)V
    .locals 1

    iget v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;->mNativeAutofillPopup:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/chrome/browser/autofill/AutofillPopupGlue;->nativeSuggestionSelected(II)V

    return-void
.end method
