.class Lorg/chromium/content/app/SandboxedProcessService$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lorg/chromium/content/app/SandboxedProcessService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/content/app/SandboxedProcessService;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/app/SandboxedProcessService$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/chromium/content/app/SandboxedProcessService;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$100(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/Thread;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mNativeLibraryName:Ljava/lang/String;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$700(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$100(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    move-exception v0

    const-string v1, "SandboxedProcessService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SandboxedProcessMain startup failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mNativeLibraryName:Ljava/lang/String;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$700(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/app/LibraryLoader;->setLibraryToLoad(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_4 .. :try_end_4} :catch_1

    :try_start_5
    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->loadNow()V
    :try_end_5
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    :try_start_6
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$100(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/Thread;

    move-result-object v1

    monitor-enter v1
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_6 .. :try_end_6} :catch_1

    :goto_2
    :try_start_7
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mCommandLineParams:[Ljava/lang/String;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$200(Lorg/chromium/content/app/SandboxedProcessService;)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$100(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit v1

    throw v0
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_8 .. :try_end_8} :catch_1

    :catch_1
    move-exception v0

    const-string v1, "SandboxedProcessService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SandboxedProcessMain startup failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_9
    const-string v1, "SandboxedProcessService"

    const-string v2, "Failed to load native library, exiting sandboxed process"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_1

    :cond_1
    :try_start_a
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mCommandLineParams:[Ljava/lang/String;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$200(Lorg/chromium/content/app/SandboxedProcessService;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/app/LibraryLoader;->initializeOnMainThread([Ljava/lang/String;)V

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$100(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/Thread;

    move-result-object v1

    monitor-enter v1
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_b .. :try_end_b} :catch_1

    :try_start_c
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    const/4 v2, 0x1

    # setter for: Lorg/chromium/content/app/SandboxedProcessService;->mLibraryInitialized:Z
    invoke-static {v0, v2}, Lorg/chromium/content/app/SandboxedProcessService;->access$802(Lorg/chromium/content/app/SandboxedProcessService;Z)Z

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$100(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_3
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$500(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$100(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_3

    :catchall_2
    move-exception v0

    :try_start_d
    monitor-exit v1

    throw v0
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_d .. :try_end_d} :catch_1

    :cond_2
    :try_start_e
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :try_start_f
    sget-boolean v0, Lorg/chromium/content/app/SandboxedProcessService$2;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$500(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileFds:Ljava/util/ArrayList;
    invoke-static {v1}, Lorg/chromium/content/app/SandboxedProcessService;->access$600(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$500(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [I

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileFds:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$600(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v3, v0, [I

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$500(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$500(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v1

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileFds:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$600(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->detachFd()I

    move-result v0

    aput v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_4
    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->sContext:Landroid/content/Context;
    invoke-static {}, Lorg/chromium/content/app/SandboxedProcessService;->access$900()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/app/ContentMain;->initApplicationContext(Landroid/content/Context;)V

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->sContext:Landroid/content/Context;
    invoke-static {}, Lorg/chromium/content/app/SandboxedProcessService;->access$900()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    iget-object v4, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mCpuCount:I
    invoke-static {v4}, Lorg/chromium/content/app/SandboxedProcessService;->access$300(Lorg/chromium/content/app/SandboxedProcessService;)I

    move-result v4

    iget-object v5, p0, Lorg/chromium/content/app/SandboxedProcessService$2;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mCpuFeatures:J
    invoke-static {v5}, Lorg/chromium/content/app/SandboxedProcessService;->access$400(Lorg/chromium/content/app/SandboxedProcessService;)J

    move-result-wide v5

    # invokes: Lorg/chromium/content/app/SandboxedProcessService;->nativeInitSandboxedProcess(Landroid/content/Context;Lorg/chromium/content/app/SandboxedProcessService;[I[IIJ)V
    invoke-static/range {v0 .. v6}, Lorg/chromium/content/app/SandboxedProcessService;->access$1000(Landroid/content/Context;Lorg/chromium/content/app/SandboxedProcessService;[I[IIJ)V

    invoke-static {}, Lorg/chromium/content/app/ContentMain;->start()I

    # invokes: Lorg/chromium/content/app/SandboxedProcessService;->nativeExitSandboxedProcess()V
    invoke-static {}, Lorg/chromium/content/app/SandboxedProcessService;->access$1100()V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_f .. :try_end_f} :catch_1

    goto/16 :goto_1
.end method
