.class Lorg/chromium/content/browser/BrowserProcessSurfaceTexture;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "content"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static establishSurfaceTexturePeer(IILandroid/graphics/SurfaceTexture;II)V
    .locals 1

    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p2}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-static {p0, p1, v0, p3, p4}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->establishSurfacePeer(IILandroid/view/Surface;II)V

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    return-void
.end method
