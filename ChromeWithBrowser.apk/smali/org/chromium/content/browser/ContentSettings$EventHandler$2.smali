.class Lorg/chromium/content/browser/ContentSettings$EventHandler$2;
.super Landroid/os/Handler;


# instance fields
.field final synthetic this$1:Lorg/chromium/content/browser/ContentSettings$EventHandler;

.field final synthetic val$this$0:Lorg/chromium/content/browser/ContentSettings;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/ContentSettings$EventHandler;Landroid/os/Looper;Lorg/chromium/content/browser/ContentSettings;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings$EventHandler$2;->this$1:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    iput-object p3, p0, Lorg/chromium/content/browser/ContentSettings$EventHandler$2;->val$this$0:Lorg/chromium/content/browser/ContentSettings;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings$EventHandler$2;->this$1:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    iget-object v0, v0, Lorg/chromium/content/browser/ContentSettings$EventHandler;->this$0:Lorg/chromium/content/browser/ContentSettings;

    # getter for: Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings;->access$200(Lorg/chromium/content/browser/ContentSettings;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings$EventHandler$2;->this$1:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    iget-object v0, v0, Lorg/chromium/content/browser/ContentSettings$EventHandler;->this$0:Lorg/chromium/content/browser/ContentSettings;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentSettings;->syncFromNativeOnUiThread()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings$EventHandler$2;->this$1:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    iget-object v0, v0, Lorg/chromium/content/browser/ContentSettings$EventHandler;->this$0:Lorg/chromium/content/browser/ContentSettings;

    const/4 v2, 0x0

    # setter for: Lorg/chromium/content/browser/ContentSettings;->mIsSyncMessagePending:Z
    invoke-static {v0, v2}, Lorg/chromium/content/browser/ContentSettings;->access$302(Lorg/chromium/content/browser/ContentSettings;Z)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
