.class Lorg/chromium/content/browser/ContentViewCore$10;
.super Lorg/chromium/content/browser/InsertionHandleController;


# static fields
.field private static final AVERAGE_LINE_HEIGHT:I = 0xe


# instance fields
.field final synthetic this$0:Lorg/chromium/content/browser/ContentViewCore;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/ContentViewCore;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore$10;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/InsertionHandleController;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getLineHeight()I
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$10;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$000(Lorg/chromium/content/browser/ContentViewCore;)F

    move-result v0

    const/high16 v1, 0x41600000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public paste()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$10;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$1800(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ImeAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->paste()Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$10;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # invokes: Lorg/chromium/content/browser/ContentViewCore;->hideHandles()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$1900(Lorg/chromium/content/browser/ContentViewCore;)V

    return-void
.end method

.method public setCursorPosition(II)V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$10;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$1000(Lorg/chromium/content/browser/ContentViewCore;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$10;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore$10;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewCore;->access$1000(Lorg/chromium/content/browser/ContentViewCore;)I

    move-result v1

    # invokes: Lorg/chromium/content/browser/ContentViewCore;->nativeMoveCaret(III)V
    invoke-static {v0, v1, p1, p2}, Lorg/chromium/content/browser/ContentViewCore;->access$1700(Lorg/chromium/content/browser/ContentViewCore;III)V

    :cond_0
    return-void
.end method

.method public showHandle()V
    .locals 0

    invoke-super {p0}, Lorg/chromium/content/browser/InsertionHandleController;->showHandle()V

    return-void
.end method
