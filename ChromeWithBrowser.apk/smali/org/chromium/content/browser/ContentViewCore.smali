.class public Lorg/chromium/content/browser/ContentViewCore;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
.implements Lorg/chromium/content/browser/NavigationClient;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "content"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final IS_LONG_PRESS:I = 0x1

.field private static final IS_LONG_TAP:I = 0x2

.field public static final PERSONALITY_CHROME:I = 0x1

.field public static final PERSONALITY_VIEW:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field private static final TEXT_HANDLE_FADE_IN_DELAY:I = 0x12c

.field private static final ZOOM_CONTROLS_EPSILON:F = 0.007f


# instance fields
.field private mAccessibilityInjector:Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

.field private mActionMode:Landroid/view/ActionMode;

.field private mAdapterInputConnectionFactory:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnectionFactory;

.field private mAttachedToWindow:Z

.field private mContainerView:Landroid/view/ViewGroup;

.field private mContainerViewInternals:Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

.field private mContentHeight:I

.field private mContentSettings:Lorg/chromium/content/browser/ContentSettings;

.field private mContentSizeChangeListener:Lorg/chromium/content/browser/ContentViewCore$ContentSizeChangeListener;

.field private mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

.field private mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;

.field private mContentWidth:I

.field private final mContext:Landroid/content/Context;

.field private mDeferredHandleFadeInRunnable:Ljava/lang/Runnable;

.field private mDownloadDelegate:Lorg/chromium/content/browser/ContentViewDownloadDelegate;

.field private final mEndHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

.field private mFakeMouseMoveRunnable:Ljava/lang/Runnable;

.field private mFocusPreOSKViewportRect:Landroid/graphics/Rect;

.field private mHardwareAccelerated:Z

.field private mHasSelection:Z

.field private mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

.field private mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

.field private mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

.field private final mInsertionHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

.field private mJavaScriptInterfaces:Ljava/util/Map;

.field private mLastSelectedText:Ljava/lang/String;

.field private mNativeContentViewCore:I

.field private mNativeMaximumScale:F

.field private mNativeMinimumScale:F

.field private mNativePageScaleFactor:F

.field private mNativeScrollX:I

.field private mNativeScrollY:I

.field private mNeedUpdateOrientationChanged:Z

.field private final mPersonality:I

.field private mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

.field private mRetainedJavaScriptObjects:Ljava/util/HashSet;

.field private mScrolledAndZoomedFocusedEditableNode:Z

.field private mSelectionEditable:Z

.field private mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

.field private final mStartHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

.field private mUnfocusOnNextSizeChanged:Z

.field private mUnselectAllOnActionModeDismiss:Z

.field private mViewportHeight:I

.field private mViewportWidth:I

.field private mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

.field private mZoomManager:Lorg/chromium/content/browser/ZoomManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/browser/ContentViewCore;->$assertionsDisabled:Z

    const-class v0, Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/chromium/content/browser/ContentViewCore;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4

    const/high16 v3, 0x3f800000

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mJavaScriptInterfaces:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mRetainedJavaScriptObjects:Ljava/util/HashSet;

    iput v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mAttachedToWindow:Z

    iput v3, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    iput v3, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeMinimumScale:F

    iput v3, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeMaximumScale:F

    iput-object v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mFakeMouseMoveRunnable:Ljava/lang/Runnable;

    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    invoke-direct {v0, p0, v2}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;-><init>(Lorg/chromium/content/browser/ContentViewCore;Lorg/chromium/content/browser/ContentViewCore$1;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mStartHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    invoke-direct {v0, p0, v2}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;-><init>(Lorg/chromium/content/browser/ContentViewCore;Lorg/chromium/content/browser/ContentViewCore$1;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mEndHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    invoke-direct {v0, p0, v2}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;-><init>(Lorg/chromium/content/browser/ContentViewCore;Lorg/chromium/content/browser/ContentViewCore$1;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mUnfocusOnNextSizeChanged:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mScrolledAndZoomedFocusedEditableNode:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mHardwareAccelerated:Z

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lorg/chromium/base/WeakContext;->initializeWeakContext(Landroid/content/Context;)V

    iput p2, p0, Lorg/chromium/content/browser/ContentViewCore;->mPersonality:I

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/browser/HeapStatsLogger;->init(Landroid/content/Context;)V

    new-instance v0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnectionFactory;

    invoke-direct {v0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnectionFactory;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAdapterInputConnectionFactory:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnectionFactory;

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/content/browser/ContentViewCore;)F
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    return v0
.end method

.method static synthetic access$100(Lorg/chromium/content/browser/ContentViewCore;)I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I

    return v0
.end method

.method static synthetic access$1000(Lorg/chromium/content/browser/ContentViewCore;)I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    return v0
.end method

.method static synthetic access$1100(Lorg/chromium/content/browser/ContentViewCore;IJIIZ)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lorg/chromium/content/browser/ContentViewCore;->nativeSingleTap(IJIIZ)V

    return-void
.end method

.method static synthetic access$1200(Lorg/chromium/content/browser/ContentViewCore;IJIIZ)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lorg/chromium/content/browser/ContentViewCore;->nativeLongPress(IJIIZ)V

    return-void
.end method

.method static synthetic access$1300(Lorg/chromium/content/browser/ContentViewCore;I)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewCore;->nativeScrollFocusedEditableNodeIntoView(I)V

    return-void
.end method

.method static synthetic access$1400(Lorg/chromium/content/browser/ContentViewCore;I)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewCore;->nativeUndoScrollFocusedEditableNodeIntoView(I)V

    return-void
.end method

.method static synthetic access$1500(Lorg/chromium/content/browser/ContentViewCore;IIIII)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lorg/chromium/content/browser/ContentViewCore;->nativeSelectBetweenCoordinates(IIIII)V

    return-void
.end method

.method static synthetic access$1600(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->showSelectActionBar()V

    return-void
.end method

.method static synthetic access$1700(Lorg/chromium/content/browser/ContentViewCore;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/content/browser/ContentViewCore;->nativeMoveCaret(III)V

    return-void
.end method

.method static synthetic access$1800(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ImeAdapter;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    return-object v0
.end method

.method static synthetic access$1900(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->hideHandles()V

    return-void
.end method

.method static synthetic access$200(Lorg/chromium/content/browser/ContentViewCore;)I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I

    return v0
.end method

.method static synthetic access$2000(Lorg/chromium/content/browser/ContentViewCore;)Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionEditable:Z

    return v0
.end method

.method static synthetic access$2102(Lorg/chromium/content/browser/ContentViewCore;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$2200(Lorg/chromium/content/browser/ContentViewCore;)Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mUnselectAllOnActionModeDismiss:Z

    return v0
.end method

.method static synthetic access$2300(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewGestureHandler;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;

    return-object v0
.end method

.method static synthetic access$2400(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->scheduleTextHandleFadeIn()V

    return-void
.end method

.method static synthetic access$2500(Lorg/chromium/content/browser/ContentViewCore;)Z
    .locals 1

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/SelectionHandleController;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    return-object v0
.end method

.method static synthetic access$2700(Lorg/chromium/content/browser/ContentViewCore;)Z
    .locals 1

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2800(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/InsertionHandleController;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    return-object v0
.end method

.method static synthetic access$400(Lorg/chromium/content/browser/ContentViewCore;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$500(Lorg/chromium/content/browser/ContentViewCore;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewCore;->undoScrollFocusedEditableNodeIntoViewIfNeeded(Z)V

    return-void
.end method

.method static synthetic access$600(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->scrollFocusedEditableNodeIntoView()V

    return-void
.end method

.method static synthetic access$700(Lorg/chromium/content/browser/ContentViewCore;)Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$800(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->hidePopupDialog()V

    return-void
.end method

.method static synthetic access$900(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->resetGestureHandlers()V

    return-void
.end method

.method private addToNavigationHistory(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 7

    new-instance v0, Lorg/chromium/content/browser/NavigationEntry;

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/NavigationEntry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    check-cast p1, Lorg/chromium/content/browser/NavigationHistory;

    invoke-virtual {p1, v0}, Lorg/chromium/content/browser/NavigationHistory;->addEntry(Lorg/chromium/content/browser/NavigationEntry;)V

    return-void
.end method

.method private confirmTouchEvent(I)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->confirmTouchEvent(I)V

    return-void
.end method

.method private createImeAdapter(Landroid/content/Context;)Lorg/chromium/content/browser/ImeAdapter;
    .locals 4

    new-instance v0, Lorg/chromium/content/browser/ImeAdapter;

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->getSelectionHandleController()Lorg/chromium/content/browser/SelectionHandleController;

    move-result-object v1

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lorg/chromium/content/browser/InsertionHandleController;

    move-result-object v2

    new-instance v3, Lorg/chromium/content/browser/ContentViewCore$2;

    invoke-direct {v3, p0}, Lorg/chromium/content/browser/ContentViewCore$2;-><init>(Lorg/chromium/content/browser/ContentViewCore;)V

    invoke-direct {v0, p1, v1, v2, v3}, Lorg/chromium/content/browser/ImeAdapter;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/SelectionHandleController;Lorg/chromium/content/browser/InsertionHandleController;Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;)V

    return-object v0
.end method

.method private getInsertionHandleController()Lorg/chromium/content/browser/InsertionHandleController;
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    if-nez v0, :cond_0

    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$10;

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/chromium/content/browser/ContentViewCore$10;-><init>(Lorg/chromium/content/browser/ContentViewCore;Landroid/view/View;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->hideAndDisallowAutomaticShowing()V

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    return-object v0
.end method

.method private getSelectionHandleController()Lorg/chromium/content/browser/SelectionHandleController;
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    if-nez v0, :cond_0

    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$9;

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/chromium/content/browser/ContentViewCore$9;-><init>(Lorg/chromium/content/browser/ContentViewCore;Landroid/view/View;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    invoke-virtual {v0}, Lorg/chromium/content/browser/SelectionHandleController;->hideAndDisallowAutomaticShowing()V

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    return-object v0
.end method

.method private handleTapOrPress(JIIIZ)V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestFocus()Z

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    invoke-virtual {v0}, Lorg/chromium/content/browser/PopupZoomer;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    int-to-float v1, p3

    int-to-float v2, p4

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/PopupZoomer;->setLastTouch(FF)V

    :cond_1
    const/4 v0, 0x1

    if-ne p5, v0, :cond_3

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lorg/chromium/content/browser/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->allowAutomaticShowing()V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->getSelectionHandleController()Lorg/chromium/content/browser/SelectionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/SelectionHandleController;->allowAutomaticShowing()V

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_2

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->nativeLongPress(IJIIZ)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v0, 0x2

    if-ne p5, v0, :cond_4

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lorg/chromium/content/browser/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->allowAutomaticShowing()V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->getSelectionHandleController()Lorg/chromium/content/browser/SelectionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/SelectionHandleController;->allowAutomaticShowing()V

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_2

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->nativeLongTap(IJIIZ)V

    goto :goto_0

    :cond_4
    if-nez p6, :cond_5

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_5

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->nativeShowPressState(IJII)V

    :cond_5
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionEditable:Z

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lorg/chromium/content/browser/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->allowAutomaticShowing()V

    :cond_6
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_2

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->nativeSingleTap(IJIIZ)V

    goto :goto_0
.end method

.method public static hasHardwareAcceleration(Landroid/app/Activity;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, 0x1000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ActivityInfo;->flags:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit16 v2, v2, 0x200

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "Chrome"

    const-string v2, "getActivityInfo(self) should not fail"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static hasHardwareAcceleration(Landroid/content/Context;)Z
    .locals 1

    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/app/Activity;

    invoke-static {p0}, Lorg/chromium/content/browser/ContentViewCore;->hasHardwareAcceleration(Landroid/app/Activity;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasTouchEventHandlers(Z)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->hasTouchEventHandlers(Z)V

    return-void
.end method

.method private hideHandles()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    invoke-virtual {v0}, Lorg/chromium/content/browser/SelectionHandleController;->hideAndDisallowAutomaticShowing()V

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->hideAndDisallowAutomaticShowing()V

    :cond_1
    return-void
.end method

.method private hidePopupDialog()V
    .locals 0

    invoke-static {p0}, Lorg/chromium/content/browser/SelectPopupDialog;->hide(Lorg/chromium/content/browser/ContentViewCore;)V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->hideHandles()V

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->hideSelectActionBar()V

    return-void
.end method

.method private imeUpdateAdapter(IILjava/lang/String;IIIIZ)V
    .locals 6

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    const/16 v0, 0xa0

    const/16 v1, 0x20

    invoke-virtual {p3, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    sget v0, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeNone:I

    if-eq p2, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionEditable:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0, p1, p2, v1, p8}, Lorg/chromium/content/browser/ImeAdapter;->attachAndShowIfNeeded(IILjava/lang/String;Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    if-eqz v0, :cond_1

    if-ne p4, p6, :cond_3

    if-ne p5, p7, :cond_3

    move v2, p5

    :goto_1
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    move v3, p5

    move v4, p6

    move v5, p7

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->setEditableText(Ljava/lang/String;IIII)V

    :cond_1
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v2, p4

    goto :goto_1
.end method

.method private initPopupZoomer(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Lorg/chromium/content/browser/PopupZoomer;

    invoke-direct {v0, p1}, Lorg/chromium/content/browser/PopupZoomer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    new-instance v1, Lorg/chromium/content/browser/ContentViewCore$4;

    invoke-direct {v1, p0}, Lorg/chromium/content/browser/ContentViewCore$4;-><init>(Lorg/chromium/content/browser/ContentViewCore;)V

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/PopupZoomer;->setOnVisibilityChangedListener(Lorg/chromium/content/browser/PopupZoomer$OnVisibilityChangedListener;)V

    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$5;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewCore$5;-><init>(Lorg/chromium/content/browser/ContentViewCore;)V

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    invoke-virtual {v1, v0}, Lorg/chromium/content/browser/PopupZoomer;->setOnTapListener(Lorg/chromium/content/browser/PopupZoomer$OnTapListener;)V

    return-void
.end method

.method private initializeContainerView(Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setWillNotDraw(Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFocusable(Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClickable(Z)V

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mPersonality:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getScrollBarStyle()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setHorizontalScrollBarEnabled(Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVerticalScrollBarEnabled(Z)V

    :cond_0
    new-instance v0, Lorg/chromium/content/browser/ZoomManager;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lorg/chromium/content/browser/ZoomManager;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/ContentViewCore;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager;->updateMultiTouchSupport()V

    new-instance v0, Lorg/chromium/content/browser/ContentViewGestureHandler;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-direct {v0, v1, p0, v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;Lorg/chromium/content/browser/ZoomManager;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;

    iput v3, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I

    iput v3, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I

    const/high16 v0, 0x3f800000

    iput v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->initPopupZoomer(Landroid/content/Context;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->createImeAdapter(Landroid/content/Context;)Lorg/chromium/content/browser/ImeAdapter;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void
.end method

.method private isInsertionHandleShowing()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSelectionHandleShowing()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    invoke-virtual {v0}, Lorg/chromium/content/browser/SelectionHandleController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeAddJavascriptInterface(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/util/HashSet;)V
.end method

.method private native nativeCanGoBack(I)Z
.end method

.method private native nativeCanGoForward(I)Z
.end method

.method private native nativeCanGoToOffset(II)Z
.end method

.method private native nativeCancelPendingReload(I)V
.end method

.method private native nativeClearHistory(I)V
.end method

.method private native nativeClearSslPreferences(I)V
.end method

.method private native nativeConsumePendingRendererFrame(I)Z
.end method

.method private native nativeContinuePendingReload(I)V
.end method

.method private native nativeCrashed(I)Z
.end method

.method private native nativeDoubleTap(IJII)V
.end method

.method private native nativeEvaluateJavaScript(ILjava/lang/String;Lorg/chromium/content/browser/ContentViewCore$JavaScriptCallback;)V
.end method

.method private native nativeExitFullscreen(I)V
.end method

.method private native nativeFlingCancel(IJ)V
.end method

.method private native nativeFlingStart(IJIIII)V
.end method

.method private native nativeGetBackgroundColor(I)I
.end method

.method private native nativeGetCurrentRenderProcessId(I)I
.end method

.method private native nativeGetDirectedNavigationHistory(ILjava/lang/Object;ZI)V
.end method

.method private native nativeGetNativeImeAdapter(I)I
.end method

.method private native nativeGetNavigationHistory(ILjava/lang/Object;)I
.end method

.method private native nativeGetTitle(I)Ljava/lang/String;
.end method

.method private native nativeGetURL(I)Ljava/lang/String;
.end method

.method private native nativeGetUseDesktopUserAgent(I)Z
.end method

.method private native nativeGoBack(I)V
.end method

.method private native nativeGoForward(I)V
.end method

.method private native nativeGoToNavigationIndex(II)V
.end method

.method private native nativeGoToOffset(II)V
.end method

.method private native nativeInit(ZZII)I
.end method

.method private native nativeIsIncognito(I)Z
.end method

.method private native nativeIsRenderWidgetHostViewReady(I)Z
.end method

.method private native nativeIsShowingInterstitialPage(I)Z
.end method

.method private native nativeLoadUrl(ILjava/lang/String;IIILjava/lang/String;[BLjava/lang/String;Ljava/lang/String;Z)V
.end method

.method private native nativeLongPress(IJIIZ)V
.end method

.method private native nativeLongTap(IJIIZ)V
.end method

.method private native nativeMoveCaret(III)V
.end method

.method private native nativeNeedsReload(I)Z
.end method

.method private native nativeOnHide(I)V
.end method

.method private native nativeOnJavaContentViewCoreDestroyed(I)V
.end method

.method private native nativeOnShow(I)V
.end method

.method private native nativePinchBegin(IJII)V
.end method

.method private native nativePinchBy(IJIIF)V
.end method

.method private native nativePinchEnd(IJ)V
.end method

.method private native nativePopulateBitmapFromCompositor(ILandroid/graphics/Bitmap;)Z
.end method

.method private native nativeReload(I)V
.end method

.method private native nativeRemoveJavascriptInterface(ILjava/lang/String;)V
.end method

.method private native nativeScrollBegin(IJII)V
.end method

.method private native nativeScrollBy(IJIIII)V
.end method

.method private native nativeScrollEnd(IJ)V
.end method

.method private native nativeScrollFocusedEditableNodeIntoView(I)V
.end method

.method private native nativeSelectBetweenCoordinates(IIIII)V
.end method

.method private native nativeSelectPopupMenuItems(I[I)V
.end method

.method private native nativeSendMouseMoveEvent(IJII)I
.end method

.method private native nativeSendMouseWheelEvent(IJIIF)I
.end method

.method private native nativeSendOrientationChangeEvent(II)V
.end method

.method private native nativeSendTouchEvent(IJI[Lorg/chromium/content/browser/TouchPoint;)Z
.end method

.method private native nativeSetAllUserAgentOverridesInHistory(ILjava/lang/String;)V
.end method

.method private native nativeSetBackgroundColor(II)V
.end method

.method private native nativeSetFocus(IZ)V
.end method

.method private native nativeSetSize(III)V
.end method

.method private native nativeSetUseDesktopUserAgent(IZZ)V
.end method

.method private native nativeShowInterstitialPage(ILjava/lang/String;I)V
.end method

.method private native nativeShowPressCancel(IJII)V
.end method

.method private native nativeShowPressState(IJII)V
.end method

.method private native nativeSingleTap(IJIIZ)V
.end method

.method private native nativeStopLoading(I)V
.end method

.method private native nativeUndoScrollFocusedEditableNodeIntoView(I)V
.end method

.method private native nativeUpdateVSyncParameters(IJJ)V
.end method

.method private onBackgroundColorChanged(I)V
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewClient;->onBackgroundColorChanged(I)V

    return-void
.end method

.method private static onEvaluateJavaScriptResult(Ljava/lang/String;Lorg/chromium/content/browser/ContentViewCore$JavaScriptCallback;)V
    .locals 0

    invoke-interface {p1, p0}, Lorg/chromium/content/browser/ContentViewCore$JavaScriptCallback;->handleJavaScriptResult(Ljava/lang/String;)V

    return-void
.end method

.method private onRenderProcessSwap(II)V
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAttachedToWindow:Z

    if-eqz v0, :cond_1

    if-eq p1, p2, :cond_1

    if-lez p1, :cond_0

    invoke-static {p1}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->unbindAsHighPriority(I)V

    :cond_0
    if-lez p2, :cond_1

    invoke-static {p2}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->bindAsHighPriority(I)V

    :cond_1
    return-void
.end method

.method private onSelectionBoundsChanged(Landroid/graphics/Rect;ILandroid/graphics/Rect;IZ)V
    .locals 7

    const/4 v6, 0x0

    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget v2, p3, Landroid/graphics/Rect;->left:I

    iget v3, p3, Landroid/graphics/Rect;->bottom:I

    if-ne v0, v2, :cond_0

    if-ne v1, v3, :cond_0

    iget-object v4, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    invoke-virtual {v4}, Lorg/chromium/content/browser/SelectionHandleController;->isDragging()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_0
    iget-object v4, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-virtual {v4}, Lorg/chromium/content/browser/InsertionHandleController;->hide()V

    :cond_1
    if-eqz p5, :cond_2

    iget-object v4, p0, Lorg/chromium/content/browser/ContentViewCore;->mStartHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v4, v0, v1}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->setWindow(FF)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mEndHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    int-to-float v1, v2

    int-to-float v2, v3

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->setWindow(FF)V

    :goto_0
    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->getSelectionHandleController()Lorg/chromium/content/browser/SelectionHandleController;

    move-result-object v0

    invoke-virtual {v0, p2, p4}, Lorg/chromium/content/browser/SelectionHandleController;->onSelectionChanged(II)V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->updateHandleScreenPositions()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mHasSelection:Z

    :goto_1
    return-void

    :cond_2
    iget-object v4, p0, Lorg/chromium/content/browser/ContentViewCore;->mStartHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    int-to-float v2, v2

    int-to-float v3, v3

    invoke-virtual {v4, v2, v3}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->setWindow(FF)V

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mEndHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v2, v0, v1}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->setWindow(FF)V

    goto :goto_0

    :cond_3
    iput-boolean v6, p0, Lorg/chromium/content/browser/ContentViewCore;->mUnselectAllOnActionModeDismiss:Z

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->hideSelectActionBar()V

    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    iget-boolean v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionEditable:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    invoke-virtual {v2}, Lorg/chromium/content/browser/SelectionHandleController;->hide()V

    :cond_4
    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v2, v0, v1}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->setWindow(FF)V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lorg/chromium/content/browser/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->onCursorPositionChanged()V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->updateHandleScreenPositions()V

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isWatchingCursor(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    iget-object v5, v1, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->screen:Landroid/graphics/PointF;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget v2, v5, Landroid/graphics/PointF;->x:F

    float-to-int v2, v2

    iget v3, v5, Landroid/graphics/PointF;->y:F

    float-to-int v3, v3

    iget v4, v5, Landroid/graphics/PointF;->x:F

    float-to-int v4, v4

    iget v5, v5, Landroid/graphics/PointF;->y:F

    float-to-int v5, v5

    invoke-virtual/range {v0 .. v5}, Landroid/view/inputmethod/InputMethodManager;->updateCursor(Landroid/view/View;IIII)V

    :cond_5
    :goto_2
    iput-boolean v6, p0, Lorg/chromium/content/browser/ContentViewCore;->mHasSelection:Z

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    invoke-virtual {v0}, Lorg/chromium/content/browser/SelectionHandleController;->hideAndDisallowAutomaticShowing()V

    :cond_7
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->hideAndDisallowAutomaticShowing()V

    goto :goto_2
.end method

.method private onSelectionChanged(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mLastSelectedText:Ljava/lang/String;

    return-void
.end method

.method private onTabCrash()V
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewClient;->onTabCrash()V

    return-void
.end method

.method private onWebContentsConnected()V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->isNativeImeAdapterAttached()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v1}, Lorg/chromium/content/browser/ContentViewCore;->nativeGetNativeImeAdapter(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ImeAdapter;->attach(I)V

    :cond_0
    return-void
.end method

.method private onWebPreferencesUpdated()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentSettings:Lorg/chromium/content/browser/ContentSettings;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentSettings;->syncSettings()V

    return-void
.end method

.method private processImeBatchStateAck(Z)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->setIgnoreTextInputStateUpdates(Z)V

    goto :goto_0
.end method

.method private resetGestureDetectors()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->resetGestureHandlers()V

    return-void
.end method

.method private scheduleTextHandleFadeIn()V
    .locals 4

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mDeferredHandleFadeInRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$12;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewCore$12;-><init>(Lorg/chromium/content/browser/ContentViewCore;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mDeferredHandleFadeInRunnable:Ljava/lang/Runnable;

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mDeferredHandleFadeInRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mDeferredHandleFadeInRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private scrollFocusedEditableNodeIntoView()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$6;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewCore$6;-><init>(Lorg/chromium/content/browser/ContentViewCore;)V

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mScrolledAndZoomedFocusedEditableNode:Z

    :cond_0
    return-void
.end method

.method private sendOrientationChangeEvent()V
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lorg/chromium/content/browser/ContentViewCore;->TAG:Ljava/lang/String;

    const-string v1, "Unknown rotation!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_0
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    const/16 v1, 0x5a

    invoke-direct {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->nativeSendOrientationChangeEvent(II)V

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    const/16 v1, 0xb4

    invoke-direct {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->nativeSendOrientationChangeEvent(II)V

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    const/16 v1, -0x5a

    invoke-direct {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->nativeSendOrientationChangeEvent(II)V

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->nativeSendOrientationChangeEvent(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setTitle(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewClient;->onUpdateTitle(Ljava/lang/String;)V

    return-void
.end method

.method private showDisambiguationPopup(Landroid/graphics/Rect;Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    invoke-virtual {v0, p2}, Lorg/chromium/content/browser/PopupZoomer;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/PopupZoomer;->show(Landroid/graphics/Rect;)V

    return-void
.end method

.method private showPastePopup(II)V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->setWindow(FF)V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lorg/chromium/content/browser/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->showHandle()V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->updateHandleScreenPositions()V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lorg/chromium/content/browser/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->showHandleWithPastePopup()V

    return-void
.end method

.method private showSelectActionBar()V
    .locals 5

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$11;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewCore$11;-><init>(Lorg/chromium/content/browser/ContentViewCore;)V

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v2

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v4}, Lorg/chromium/content/browser/ContentViewCore;->nativeIsIncognito(I)Z

    move-result v4

    invoke-virtual {v2, v3, v0, v4}, Lorg/chromium/content/browser/ContentViewClient;->getSelectActionModeCallback(Landroid/content/Context;Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)Landroid/view/ActionMode$Callback;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mUnselectAllOnActionModeDismiss:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->unselect()Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewClient;->onContextualActionBarShown()V

    goto :goto_0
.end method

.method private showSelectPopup([Ljava/lang/String;[IZ[I)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lorg/chromium/content/browser/SelectPopupDialog;->show(Lorg/chromium/content/browser/ContentViewCore;[Ljava/lang/String;[IZ[I)V

    return-void
.end method

.method private startContentIntent(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lorg/chromium/content/browser/ContentViewClient;->onStartContentIntent(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private temporarilyHideTextHandles()V
    .locals 2

    const/4 v1, 0x4

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/SelectionHandleController;->setHandleVisibility(I)V

    :cond_0
    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/InsertionHandleController;->setHandleVisibility(I)V

    :cond_1
    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->scheduleTextHandleFadeIn()V

    return-void
.end method

.method private undoScrollFocusedEditableNodeIntoViewIfNeeded(Z)V
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mScrolledAndZoomedFocusedEditableNode:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$7;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewCore$7;-><init>(Lorg/chromium/content/browser/ContentViewCore;)V

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mScrolledAndZoomedFocusedEditableNode:Z

    return-void
.end method

.method private updateContentSize(II)V
    .locals 4

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getWidth()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getHeight()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentWidth:I

    if-ne v2, v0, :cond_0

    iget v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentHeight:I

    if-eq v2, v1, :cond_1

    :cond_0
    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/chromium/content/browser/PopupZoomer;->hide(Z)V

    iput v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentWidth:I

    iput v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentHeight:I

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentWidth:I

    iget v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentHeight:I

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewClient;->onContentSizeChanged(II)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentSizeChangeListener:Lorg/chromium/content/browser/ContentViewCore$ContentSizeChangeListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentSizeChangeListener:Lorg/chromium/content/browser/ContentViewCore$ContentSizeChangeListener;

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentWidth()I

    move-result v1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentHeight()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewCore$ContentSizeChangeListener;->onContentSizeChanged(II)V

    :cond_1
    return-void
.end method

.method private updateHandleScreenPositions()V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mStartHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->updateWindowFromDocument()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mEndHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->updateWindowFromDocument()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->updateWindowFromDocument()V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mStartHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    iget-object v1, v1, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->screen:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/SelectionHandleController;->setStartHandlePosition(Landroid/graphics/PointF;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mEndHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    iget-object v1, v1, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->screen:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/SelectionHandleController;->setEndHandlePosition(Landroid/graphics/PointF;)V

    :cond_0
    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;

    iget-object v1, v1, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->screen:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/InsertionHandleController;->setHandlePosition(Landroid/graphics/PointF;)V

    :cond_1
    return-void
.end method

.method private updateOffsetsForFullscreen(FF)V
    .locals 3

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v0

    mul-float/2addr v0, p2

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lorg/chromium/content/browser/ContentViewClient;->onOffsetsForFullscreenChanged(FF)V

    return-void
.end method

.method private updatePageScaleLimits(FF)V
    .locals 1

    iput p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeMinimumScale:F

    iput p2, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeMaximumScale:F

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager;->updateZoomControls()V

    return-void
.end method

.method private updateScrollOffsetAndPageScaleFactor(IIF)V
    .locals 3

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I

    iget v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I

    invoke-interface {v0, p1, p2, v1, v2}, Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->onScrollChanged(IIII)V

    iput p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I

    iput p2, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    cmpl-float v0, v0, p3

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    invoke-virtual {v0, v1, p3}, Lorg/chromium/content/browser/ContentViewClient;->onScaleChanged(FF)V

    :cond_1
    iput p3, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/PopupZoomer;->hide(Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager;->updateZoomControls()V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->temporarilyHideTextHandles()V

    goto :goto_0
.end method

.method private updateTextHandlesForGesture(I)V
    .locals 0

    sparse-switch p1, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->temporarilyHideTextHandles()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_0
        0x8 -> :sswitch_0
        0xa -> :sswitch_0
    .end sparse-switch
.end method

.method private zoomByDelta(F)Z
    .locals 6

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getWidth()I

    move-result v0

    div-int/lit8 v3, v0, 0x2

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getHeight()I

    move-result v0

    div-int/lit8 v4, v0, 0x2

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewGestureHandler()Lorg/chromium/content/browser/ContentViewGestureHandler;

    move-result-object v0

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/chromium/content/browser/ContentViewGestureHandler;->pinchBegin(JII)V

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewGestureHandler()Lorg/chromium/content/browser/ContentViewGestureHandler;

    move-result-object v0

    move v5, p1

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewGestureHandler;->pinchBy(JIIF)V

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewGestureHandler()Lorg/chromium/content/browser/ContentViewGestureHandler;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->pinchEnd(J)V

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    const-class v0, Lorg/chromium/content/browser/JavascriptInterface;

    invoke-virtual {p0, p1, p2, v0}, Lorg/chromium/content/browser/ContentViewCore;->addPossiblyUnsafeJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)V

    return-void
.end method

.method public addPossiblyUnsafeJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 6

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mJavaScriptInterfaces:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    iget-object v5, p0, Lorg/chromium/content/browser/ContentViewCore;->mRetainedJavaScriptObjects:Ljava/util/HashSet;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->nativeAddJavascriptInterface(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/util/HashSet;)V

    :cond_0
    return-void
.end method

.method public awakenScrollBars(IZ)Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getScrollBarStyle()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v0, p1, p2}, Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_awakenScrollBars(IZ)Z

    move-result v0

    goto :goto_0
.end method

.method public canGoBack()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeCanGoBack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canGoForward()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeCanGoForward(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canGoToOffset(I)Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/content/browser/ContentViewCore;->nativeCanGoToOffset(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canZoomIn()Z
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeMaximumScale:F

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    sub-float/2addr v0, v1

    const v1, 0x3be56042

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canZoomOut()Z
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeMinimumScale:F

    sub-float/2addr v0, v1

    const v1, 0x3be56042

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelPendingReload()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeCancelPendingReload(I)V

    :cond_0
    return-void
.end method

.method checkIsAlive()V
    .locals 2

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ContentView used after destroy() was called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public clearHistory()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeClearHistory(I)V

    :cond_0
    return-void
.end method

.method public clearSslPreferences()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeClearSslPreferences(I)V

    return-void
.end method

.method public computeHorizontalScrollExtent()I
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getWidth()I

    move-result v0

    return v0
.end method

.method public computeHorizontalScrollOffset()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I

    return v0
.end method

.method public computeHorizontalScrollRange()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentWidth:I

    return v0
.end method

.method public computeVerticalScrollExtent()I
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getHeight()I

    move-result v0

    return v0
.end method

.method public computeVerticalScrollOffset()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I

    return v0
.end method

.method public computeVerticalScrollRange()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentHeight:I

    return v0
.end method

.method public consumePendingRendererFrame()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeConsumePendingRendererFrame(I)Z

    move-result v0

    goto :goto_0
.end method

.method public continuePendingReload()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeContinuePendingReload(I)V

    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeOnJavaContentViewCoreDestroyed(I)V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentSettings:Lorg/chromium/content/browser/ContentSettings;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mJavaScriptInterfaces:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mRetainedJavaScriptObjects:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    return-void
.end method

.method public didUIStealScroll(FF)Z
    .locals 3

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->computeHorizontalScrollOffset()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->computeVerticalScrollOffset()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, p1, p2, v1, v2}, Lorg/chromium/content/browser/ContentViewClient;->shouldOverrideScroll(FFFF)Z

    move-result v0

    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewClient;->shouldOverrideKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v0, p1}, Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ImeAdapter;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v0, p1}, Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 2

    :try_start_0
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mUnfocusOnNextSizeChanged:Z

    :goto_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v0, p1}, Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->undoScrollFocusedEditableNodeIntoViewIfNeeded(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    throw v0
.end method

.method public evaluateJavaScript(Ljava/lang/String;Lorg/chromium/content/browser/ContentViewCore$JavaScriptCallback;)V
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->checkIsAlive()V

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p1, p2}, Lorg/chromium/content/browser/ContentViewCore;->nativeEvaluateJavaScript(ILjava/lang/String;Lorg/chromium/content/browser/ContentViewCore$JavaScriptCallback;)V

    return-void
.end method

.method public exitFullscreen()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeExitFullscreen(I)V

    return-void
.end method

.method public flingScroll(II)V
    .locals 7

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const v3, -0x7fffffff

    const/high16 v4, -0x80000000

    neg-int v5, p1

    neg-int v6, p2

    invoke-virtual/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler;->fling(JIIII)V

    return-void
.end method

.method public getBackgroundColor()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeGetBackgroundColor(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 2

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap(II)Landroid/graphics/Bitmap;
    .locals 5

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getHeight()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v2, :cond_3

    iget v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v2, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativePopulateBitmapFromCompositor(ILandroid/graphics/Bitmap;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    int-to-float v2, p1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    int-to-float v3, p2

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public getContainerView()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getContainerViewDelegate()Lorg/chromium/content/browser/ContainerViewDelegate;
    .locals 1

    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$1;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewCore$1;-><init>(Lorg/chromium/content/browser/ContentViewCore;)V

    return-object v0
.end method

.method public getContentHeight()I
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentHeight:I

    int-to-float v0, v0

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getContentSettings()Lorg/chromium/content/browser/ContentSettings;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentSettings:Lorg/chromium/content/browser/ContentSettings;

    return-object v0
.end method

.method getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    if-nez v0, :cond_0

    new-instance v0, Lorg/chromium/content/browser/ContentViewClient;

    invoke-direct {v0}, Lorg/chromium/content/browser/ContentViewClient;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    return-object v0
.end method

.method getContentViewGestureHandler()Lorg/chromium/content/browser/ContentViewGestureHandler;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;

    return-object v0
.end method

.method public getContentWidth()I
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentWidth:I

    int-to-float v0, v0

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDirectedNavigationHistory(ZI)Lorg/chromium/content/browser/NavigationHistory;
    .locals 2

    new-instance v0, Lorg/chromium/content/browser/NavigationHistory;

    invoke-direct {v0}, Lorg/chromium/content/browser/NavigationHistory;-><init>()V

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v1, v0, p1, p2}, Lorg/chromium/content/browser/ContentViewCore;->nativeGetDirectedNavigationHistory(ILjava/lang/Object;ZI)V

    return-object v0
.end method

.method getDownloadDelegate()Lorg/chromium/content/browser/ContentViewDownloadDelegate;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mDownloadDelegate:Lorg/chromium/content/browser/ContentViewDownloadDelegate;

    return-object v0
.end method

.method public getEditableForTest()Landroid/text/Editable;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mViewportHeight:I

    return v0
.end method

.method protected getImeAdapterForTest()Lorg/chromium/content/browser/ImeAdapter;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    return-object v0
.end method

.method protected getInputConnectionForTest()Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    return-object v0
.end method

.method public getInsertionHandleControllerForTest()Lorg/chromium/content/browser/InsertionHandleController;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    return-object v0
.end method

.method public getNativeContentViewCore()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    return v0
.end method

.method public getNativeScrollXForTest()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I

    return v0
.end method

.method public getNativeScrollYForTest()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I

    return v0
.end method

.method public getNavigationHistory()Lorg/chromium/content/browser/NavigationHistory;
    .locals 2

    new-instance v0, Lorg/chromium/content/browser/NavigationHistory;

    invoke-direct {v0}, Lorg/chromium/content/browser/NavigationHistory;-><init>()V

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v1, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeGetNavigationHistory(ILjava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/NavigationHistory;->setCurrentEntryIndex(I)V

    return-object v0
.end method

.method public getScale()F
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    return v0
.end method

.method public getScaledPerformanceOptimizedBitmap(II)Landroid/util/Pair;
    .locals 3

    const/high16 v0, 0x3f800000

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    :cond_0
    int-to-float v1, p1

    div-float/2addr v1, v0

    float-to-int v1, v1

    int-to-float v2, p2

    div-float/2addr v2, v0

    float-to-int v2, v2

    invoke-virtual {p0, v1, v2}, Lorg/chromium/content/browser/ContentViewCore;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method getSelectedText()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mHasSelection:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mLastSelectedText:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeGetTitle(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeGetURL(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUseDesktopUserAgent()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeGetUseDesktopUserAgent(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mViewportWidth:I

    return v0
.end method

.method public getZoomControlsForTest()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager;->getZoomControlsViewForTest()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public goBack()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeGoBack(I)V

    :cond_0
    return-void
.end method

.method public goForward()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeGoForward(I)V

    :cond_0
    return-void
.end method

.method public goToNavigationIndex(I)V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/content/browser/ContentViewCore;->nativeGoToNavigationIndex(II)V

    :cond_0
    return-void
.end method

.method public goToOffset(I)V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/content/browser/ContentViewCore;->nativeGoToOffset(II)V

    :cond_0
    return-void
.end method

.method public hasFixedPageScale()Z
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeMinimumScale:F

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeMaximumScale:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFocus()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->hasFocus()Z

    move-result v0

    return v0
.end method

.method public hasLargeOverlay()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method hideSelectActionBar()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_0
    return-void
.end method

.method public initialize(Landroid/view/ViewGroup;Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;ILorg/chromium/ui/gfx/NativeWindow;Z)V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->hasHardwareAcceleration(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mHardwareAccelerated:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-boolean v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mHardwareAccelerated:Z

    invoke-virtual {p4}, Lorg/chromium/ui/gfx/NativeWindow;->getNativePointer()I

    move-result v2

    invoke-direct {p0, v1, v0, p3, v2}, Lorg/chromium/content/browser/ContentViewCore;->nativeInit(ZZII)I

    move-result v0

    iput v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    new-instance v0, Lorg/chromium/content/browser/ContentSettings;

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {v0, p0, v1, p5}, Lorg/chromium/content/browser/ContentSettings;-><init>(Lorg/chromium/content/browser/ContentViewCore;IZ)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentSettings:Lorg/chromium/content/browser/ContentSettings;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/ContentViewCore;->initializeContainerView(Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;)V

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mPersonality:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->setAllUserAgentOverridesInHistory()V

    :cond_0
    invoke-static {p0}, Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->newInstance(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0}, Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->addOrRemoveAccessibilityApisIfNecessary()V

    const-string v0, "Web View"

    sget v1, Lorg/chromium/content/R$string;->accessibility_content_view:I

    if-nez v1, :cond_2

    sget-object v1, Lorg/chromium/content/browser/ContentViewCore;->TAG:Ljava/lang/String;

    const-string v2, "Setting contentDescription to \'Web View\' as no value was specified."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v0, Lorg/chromium/content/browser/ContentViewCore$3;

    invoke-direct {v0, p0, p0}, Lorg/chromium/content/browser/ContentViewCore$3;-><init>(Lorg/chromium/content/browser/ContentViewCore;Lorg/chromium/content/browser/ContentViewCore;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lorg/chromium/content/R$string;->accessibility_content_view:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public invokeZoomPicker()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentSettings:Lorg/chromium/content/browser/ContentSettings;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentSettings:Lorg/chromium/content/browser/ContentSettings;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentSettings;->supportZoom()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager;->invokeZoomPicker()V

    :cond_0
    return-void
.end method

.method public isAlive()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAvailable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isCrashed()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeCrashed(I)Z

    move-result v0

    goto :goto_0
.end method

.method public isInjectingAccessibilityScript()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0}, Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->accessibilityIsAvailable()Z

    move-result v0

    return v0
.end method

.method public isMultiTouchZoomSupported()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager;->isMultiTouchZoomSupported()Z

    move-result v0

    return v0
.end method

.method isPersonalityView()Z
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mPersonality:I

    packed-switch v1, :pswitch_data_0

    sget-object v1, Lorg/chromium/content/browser/ContentViewCore;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown ContentView personality: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lorg/chromium/content/browser/ContentViewCore;->mPersonality:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isReady()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeIsRenderWidgetHostViewReady(I)Z

    move-result v0

    return v0
.end method

.method public isShowingInterstitialPage()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeIsShowingInterstitialPage(I)Z

    move-result v0

    goto :goto_0
.end method

.method public loadUrl(Lorg/chromium/content/browser/LoadUrlParams;)V
    .locals 11

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->isPersonalityView()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lorg/chromium/content/browser/LoadUrlParams;->UA_OVERRIDE_TRUE:I

    invoke-virtual {p1, v0}, Lorg/chromium/content/browser/LoadUrlParams;->setOverrideUserAgent(I)V

    :cond_1
    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    iget-object v2, p1, Lorg/chromium/content/browser/LoadUrlParams;->mUrl:Ljava/lang/String;

    iget v3, p1, Lorg/chromium/content/browser/LoadUrlParams;->mLoadUrlType:I

    iget v4, p1, Lorg/chromium/content/browser/LoadUrlParams;->mTransitionType:I

    iget v5, p1, Lorg/chromium/content/browser/LoadUrlParams;->mUaOverrideOption:I

    invoke-virtual {p1}, Lorg/chromium/content/browser/LoadUrlParams;->getExtraHeadersString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p1, Lorg/chromium/content/browser/LoadUrlParams;->mPostData:[B

    iget-object v8, p1, Lorg/chromium/content/browser/LoadUrlParams;->mBaseUrlForDataUrl:Ljava/lang/String;

    iget-object v9, p1, Lorg/chromium/content/browser/LoadUrlParams;->mVirtualUrlForDataUrl:Ljava/lang/String;

    iget-boolean v10, p1, Lorg/chromium/content/browser/LoadUrlParams;->mCanLoadLocalResources:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lorg/chromium/content/browser/ContentViewCore;->nativeLoadUrl(ILjava/lang/String;IIILjava/lang/String;[BLjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public needsReload()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeNeedsReload(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityPause()V
    .locals 1

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->hidePopupDialog()V

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeOnHide(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->setAccessibilityState(Z)V

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void
.end method

.method public onActivityResume()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeOnShow(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->setAccessibilityState(Z)V

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mAttachedToWindow:Z

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeGetCurrentRenderProcessId(I)I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {v0}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->bindAsHighPriority(I)V

    :cond_0
    invoke-virtual {p0, v1}, Lorg/chromium/content/browser/ContentViewCore;->setAccessibilityState(Z)V

    return-void
.end method

.method public onCheckIsTextEditor()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->hasTextInputType()Z

    move-result v0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    const/4 v3, 0x1

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    iget v0, p1, Landroid/content/res/Configuration;->keyboard:I

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v1}, Lorg/chromium/content/browser/ContentViewCore;->nativeGetNativeImeAdapter(I)I

    move-result v1

    sget v2, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeNone:I

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ImeAdapter;->attach(II)V

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v0, p1}, Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_onConfigurationChanged(Landroid/content/res/Configuration;)V

    iput-boolean v3, p0, Lorg/chromium/content/browser/ContentViewCore;->mNeedUpdateOrientationChanged:Z

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->hasTextInputType()Z

    move-result v0

    if-nez v0, :cond_0

    const/high16 v0, 0x2000000

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAdapterInputConnectionFactory:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnectionFactory;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0, v1, v2, p1}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnectionFactory;->get(Landroid/view/View;Lorg/chromium/content/browser/ImeAdapter;Landroid/view/inputmethod/EditorInfo;)Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    return-object v0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mAttachedToWindow:Z

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeGetCurrentRenderProcessId(I)I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {v0}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->unbindAsHighPriority(I)V

    :cond_0
    invoke-virtual {p0, v1}, Lorg/chromium/content/browser/ContentViewCore;->setAccessibilityState(Z)V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->hidePopupDialog()V

    return-void
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewClient;->onImeStateChangeRequested(Z)V

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/content/browser/ContentViewCore;->nativeSetFocus(IZ)V

    :cond_1
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v0, p1}, Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->nativeSendMouseWheelEvent(IJIIF)I

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mFakeMouseMoveRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    new-instance v1, Lorg/chromium/content/browser/ContentViewCore$8;

    invoke-direct {v1, p0, v0}, Lorg/chromium/content/browser/ContentViewCore$8;-><init>(Lorg/chromium/content/browser/ContentViewCore;Landroid/view/MotionEvent;)V

    iput-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mFakeMouseMoveRunnable:Ljava/lang/Runnable;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mFakeMouseMoveRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onHide()V
    .locals 1

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->hidePopupDialog()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->setAccessibilityState(Z)V

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeOnHide(I)V

    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const-string v0, "onHoverEvent"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->begin(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mFakeMouseMoveRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->nativeSendMouseMoveEvent(IJII)I

    :cond_0
    const-string v0, "onHoverEvent"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setScrollX(I)V

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setScrollY(I)V

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentWidth:I

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentHeight:I

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-gtz v1, :cond_0

    if-lez v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-lt v0, v3, :cond_2

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollX(I)V

    invoke-virtual {p1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollY(I)V

    :cond_2
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    invoke-virtual {v1}, Lorg/chromium/content/browser/PopupZoomer;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    invoke-virtual {v1, v0}, Lorg/chromium/content/browser/PopupZoomer;->hide(Z)V

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v0, p1, p2}, Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method onNativeContentViewCoreDestroyed(I)V
    .locals 1

    sget-boolean v0, Lorg/chromium/content/browser/ContentViewCore;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    return-void
.end method

.method public onShow()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeOnShow(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->setAccessibilityState(Z)V

    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lorg/chromium/content/browser/PopupZoomer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/PopupZoomer;->hide(Z)V

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentWidth:I

    if-ge v0, p1, :cond_0

    iput p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentWidth:I

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentHeight:I

    if-ge v0, p2, :cond_1

    iput p2, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentHeight:I

    :cond_1
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mViewportWidth:I

    if-ne v0, p1, :cond_2

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mViewportHeight:I

    if-eq v0, p2, :cond_3

    :cond_2
    iput p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mViewportWidth:I

    iput p2, p0, Lorg/chromium/content/browser/ContentViewCore;->mViewportHeight:I

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_3

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mViewportWidth:I

    iget v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mViewportHeight:I

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/content/browser/ContentViewCore;->nativeSetSize(III)V

    :cond_3
    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->updateAfterSizeChanged()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->undoScrollFocusedEditableNodeIntoViewIfNeeded(Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentSettings:Lorg/chromium/content/browser/ContentSettings;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentSettings;->supportZoom()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager;->dismissZoomPicker()V

    :cond_0
    return-void
.end method

.method public pageDown(Z)Z
    .locals 6

    const/16 v5, 0x5d

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->computeVerticalScrollOffset()I

    move-result v2

    iget v3, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentHeight:I

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    if-lt v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->computeHorizontalScrollOffset()I

    move-result v0

    iget v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentHeight:I

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0, v0, v2}, Lorg/chromium/content/browser/ContentViewCore;->scrollTo(II)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v0, v5}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p0, v2}, Lorg/chromium/content/browser/ContentViewCore;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    new-instance v0, Landroid/view/KeyEvent;

    invoke-direct {v0, v1, v5}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    goto :goto_1
.end method

.method public pageUp(Z)Z
    .locals 4

    const/16 v3, 0x5c

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->computeVerticalScrollOffset()I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->computeHorizontalScrollOffset()I

    move-result v2

    invoke-virtual {p0, v2, v0}, Lorg/chromium/content/browser/ContentViewCore;->scrollTo(II)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v0, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p0, v2}, Lorg/chromium/content/browser/ContentViewCore;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    new-instance v0, Landroid/view/KeyEvent;

    invoke-direct {v0, v1, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    goto :goto_1
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->supportsAccessibilityAction(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reload()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0}, Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->addOrRemoveAccessibilityApisIfNecessary()V

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeReload(I)V

    :cond_0
    return-void
.end method

.method public removeJavascriptInterface(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mJavaScriptInterfaces:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/content/browser/ContentViewCore;->nativeRemoveJavascriptInterface(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public scrollBy(II)V
    .locals 8

    const/4 v4, 0x0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object v0, p0

    move v5, v4

    move v6, p1

    move v7, p2

    invoke-direct/range {v0 .. v7}, Lorg/chromium/content/browser/ContentViewCore;->nativeScrollBy(IJIIII)V

    :cond_0
    return-void
.end method

.method public scrollTo(II)V
    .locals 8

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I

    sub-int v6, p1, v0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I

    sub-int v7, p2, v0

    if-nez v6, :cond_2

    if-eqz v7, :cond_0

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    iget v4, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I

    iget v5, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->nativeScrollBegin(IJII)V

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    iget v4, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I

    iget v5, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lorg/chromium/content/browser/ContentViewCore;->nativeScrollBy(IJIIII)V

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, v2, v3}, Lorg/chromium/content/browser/ContentViewCore;->nativeScrollEnd(IJ)V

    goto :goto_0
.end method

.method selectPopupMenuItems([I)V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/content/browser/ContentViewCore;->nativeSelectPopupMenuItems(I[I)V

    :cond_0
    return-void
.end method

.method public sendGesture(IJIILandroid/os/Bundle;)Z
    .locals 8

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewCore;->updateTextHandlesForGesture(I)V

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->nativeShowPressState(IJII)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->nativeShowPressCancel(IJII)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->nativeDoubleTap(IJII)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->nativeSingleTap(IJIIZ)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_4
    const/4 v5, 0x0

    const-string v0, "ShowPress"

    const/4 v1, 0x0

    invoke-virtual {p6, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    move-object v0, p0

    move-wide v1, p2

    move v3, p4

    move v4, p5

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->handleTapOrPress(JIIIZ)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_5
    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p2

    move v3, p4

    move v4, p5

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->handleTapOrPress(JIIIZ)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_6
    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p2

    move v3, p4

    move v4, p5

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->handleTapOrPress(JIIIZ)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_7
    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->nativeScrollBegin(IJII)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_8
    const-string v0, "Distance X"

    invoke-virtual {p6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v0, "Distance Y"

    invoke-virtual {p6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v7}, Lorg/chromium/content/browser/ContentViewCore;->nativeScrollBy(IJIIII)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_9
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p2, p3}, Lorg/chromium/content/browser/ContentViewCore;->nativeScrollEnd(IJ)V

    const/4 v0, 0x1

    goto/16 :goto_0

    :pswitch_a
    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    const-string v0, "Velocity X"

    const/4 v2, 0x0

    invoke-virtual {p6, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    const-string v0, "Velocity Y"

    const/4 v2, 0x0

    invoke-virtual {p6, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v7}, Lorg/chromium/content/browser/ContentViewCore;->nativeFlingStart(IJIIII)V

    const/4 v0, 0x1

    goto/16 :goto_0

    :pswitch_b
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p2, p3}, Lorg/chromium/content/browser/ContentViewCore;->nativeFlingCancel(IJ)V

    const/4 v0, 0x1

    goto/16 :goto_0

    :pswitch_c
    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->nativePinchBegin(IJII)V

    const/4 v0, 0x1

    goto/16 :goto_0

    :pswitch_d
    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    const-string v0, "Delta"

    const/4 v2, 0x0

    invoke-virtual {p6, v0, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v6

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->nativePinchBy(IJIIF)V

    const/4 v0, 0x1

    goto/16 :goto_0

    :pswitch_e
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p2, p3}, Lorg/chromium/content/browser/ContentViewCore;->nativePinchEnd(IJ)V

    const/4 v0, 0x1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_1
        :pswitch_6
    .end packed-switch
.end method

.method public sendTouchEvent(JI[Lorg/chromium/content/browser/TouchPoint;)Z
    .locals 6

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->nativeSendTouchEvent(IJI[Lorg/chromium/content/browser/TouchPoint;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAccessibilityState(Z)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->setScriptEnabled(Z)V

    return-void
.end method

.method protected setAdapterInputConnectionFactory(Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnectionFactory;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mAdapterInputConnectionFactory:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnectionFactory;

    return-void
.end method

.method setAllUserAgentOverridesInHistory()V
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentSettings:Lorg/chromium/content/browser/ContentSettings;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->nativeSetAllUserAgentOverridesInHistory(ILjava/lang/String;)V

    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getBackgroundColor()I

    move-result v0

    if-eq v0, p1, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/content/browser/ContentViewCore;->nativeSetBackgroundColor(II)V

    :cond_0
    return-void
.end method

.method public setContentSizeChangeListener(Lorg/chromium/content/browser/ContentViewCore$ContentSizeChangeListener;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentSizeChangeListener:Lorg/chromium/content/browser/ContentViewCore$ContentSizeChangeListener;

    return-void
.end method

.method public setContentViewClient(Lorg/chromium/content/browser/ContentViewClient;)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The client can\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    return-void
.end method

.method public setDownloadDelegate(Lorg/chromium/content/browser/ContentViewDownloadDelegate;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore;->mDownloadDelegate:Lorg/chromium/content/browser/ContentViewDownloadDelegate;

    return-void
.end method

.method public setUseDesktopUserAgent(ZZ)V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0, p1, p2}, Lorg/chromium/content/browser/ContentViewCore;->nativeSetUseDesktopUserAgent(IZZ)V

    :cond_0
    return-void
.end method

.method public showInterstitialPage(Ljava/lang/String;Lorg/chromium/content/browser/InterstitialPageDelegateAndroid;)V
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-virtual {p2}, Lorg/chromium/content/browser/InterstitialPageDelegateAndroid;->getNative()I

    move-result v1

    invoke-direct {p0, v0, p1, v1}, Lorg/chromium/content/browser/ContentViewCore;->nativeShowInterstitialPage(ILjava/lang/String;I)V

    goto :goto_0
.end method

.method public stopCurrentAccessibilityNotifications()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0}, Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->onPageLostFocus()V

    return-void
.end method

.method public stopLoading()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->nativeStopLoading(I)V

    :cond_0
    return-void
.end method

.method public supportsAccessibilityAction(I)Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lorg/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->supportsAccessibilityAction(I)Z

    move-result v0

    return v0
.end method

.method public updateAfterSizeChanged()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->scrollFocusedEditableNodeIntoView()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNeedUpdateOrientationChanged:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewCore;->sendOrientationChangeEvent()V

    iput-boolean v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mNeedUpdateOrientationChanged:Z

    :cond_1
    return-void

    :cond_2
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mUnfocusOnNextSizeChanged:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->undoScrollFocusedEditableNodeIntoViewIfNeeded(Z)V

    iput-boolean v2, p0, Lorg/chromium/content/browser/ContentViewCore;->mUnfocusOnNextSizeChanged:Z

    goto :goto_0
.end method

.method updateMultiTouchZoomSupport()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager;->updateMultiTouchSupport()V

    return-void
.end method

.method public updateVSync(JJ)V
    .locals 6

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    if-eqz v0, :cond_0

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->nativeUpdateVSyncParameters(IJJ)V

    :cond_0
    return-void
.end method

.method public zoomIn()Z
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->canZoomIn()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3fa00000

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->zoomByDelta(F)Z

    move-result v0

    goto :goto_0
.end method

.method public zoomOut()Z
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->canZoomOut()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const v0, 0x3f4ccccd

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->zoomByDelta(F)Z

    move-result v0

    goto :goto_0
.end method

.method public zoomReset()Z
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeMinimumScale:F

    sub-float/2addr v0, v1

    const v1, 0x3be56042

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativeMinimumScale:F

    iget v1, p0, Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F

    div-float/2addr v0, v1

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->zoomByDelta(F)Z

    move-result v0

    goto :goto_0
.end method
