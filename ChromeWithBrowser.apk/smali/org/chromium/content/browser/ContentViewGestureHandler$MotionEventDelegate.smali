.class public interface abstract Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
.super Ljava/lang/Object;


# virtual methods
.method public abstract didUIStealScroll(FF)Z
.end method

.method public abstract hasFixedPageScale()Z
.end method

.method public abstract invokeZoomPicker()V
.end method

.method public abstract sendGesture(IJIILandroid/os/Bundle;)Z
.end method

.method public abstract sendTouchEvent(JI[Lorg/chromium/content/browser/TouchPoint;)Z
.end method
