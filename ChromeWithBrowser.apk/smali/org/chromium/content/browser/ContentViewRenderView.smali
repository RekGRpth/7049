.class public Lorg/chromium/content/browser/ContentViewRenderView;
.super Landroid/widget/FrameLayout;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "content"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mNativeContentViewRenderView:I

.field private mSurfaceView:Landroid/view/SurfaceView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/content/browser/ContentViewRenderView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/browser/ContentViewRenderView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, -0x1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:I

    invoke-static {}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeInit()I

    move-result v0

    iput v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:I

    sget-boolean v0, Lorg/chromium/content/browser/ContentViewRenderView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    new-instance v0, Landroid/view/SurfaceView;

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewRenderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    new-instance v1, Lorg/chromium/content/browser/ContentViewRenderView$1;

    invoke-direct {v1, p0}, Lorg/chromium/content/browser/ContentViewRenderView$1;-><init>(Lorg/chromium/content/browser/ContentViewRenderView;)V

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewRenderView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/content/browser/ContentViewRenderView;)I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:I

    return v0
.end method

.method static synthetic access$100(Lorg/chromium/content/browser/ContentViewRenderView;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSurfaceSetSize(III)V

    return-void
.end method

.method static synthetic access$200(Lorg/chromium/content/browser/ContentViewRenderView;ILandroid/view/Surface;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSurfaceCreated(ILandroid/view/Surface;)V

    return-void
.end method

.method static synthetic access$300(Lorg/chromium/content/browser/ContentViewRenderView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSurfaceDestroyed(I)V

    return-void
.end method

.method private native nativeDestroy(I)V
.end method

.method private static native nativeInit()I
.end method

.method private native nativeSetCurrentContentView(II)V
.end method

.method private native nativeSurfaceCreated(ILandroid/view/Surface;)V
.end method

.method private native nativeSurfaceDestroyed(I)V
.end method

.method private native nativeSurfaceSetSize(III)V
.end method


# virtual methods
.method public destroy()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeDestroy(I)V

    return-void
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onReadyToRender()V
    .locals 0

    return-void
.end method

.method public setCurrentContentView(Lorg/chromium/content/browser/ContentView;)V
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:I

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getNativeContentViewCore()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSetCurrentContentView(II)V

    return-void
.end method
