.class interface abstract Lorg/chromium/content/browser/CursorController;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;


# virtual methods
.method public abstract beforeStartUpdatingPosition(Lorg/chromium/content/browser/HandleView;)V
.end method

.method public abstract hide()V
.end method

.method public abstract isShowing()Z
.end method

.method public abstract onDetached()V
.end method

.method public abstract updatePosition(Lorg/chromium/content/browser/HandleView;II)V
.end method
