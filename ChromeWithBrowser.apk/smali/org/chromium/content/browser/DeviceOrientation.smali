.class Lorg/chromium/content/browser/DeviceOrientation;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "content"
.end annotation


# instance fields
.field private mGravityVector:[F

.field private mHandler:Landroid/os/Handler;

.field private mHandlerLock:Ljava/lang/Object;

.field private mMagneticFieldVector:[F

.field private mNativePtr:I

.field private mNativePtrLock:Ljava/lang/Object;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mThread:Ljava/lang/Thread;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mHandlerLock:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mNativePtrLock:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/content/browser/DeviceOrientation;Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/DeviceOrientation;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method private static create()Lorg/chromium/content/browser/DeviceOrientation;
    .locals 1

    new-instance v0, Lorg/chromium/content/browser/DeviceOrientation;

    invoke-direct {v0}, Lorg/chromium/content/browser/DeviceOrientation;-><init>()V

    return-object v0
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 3

    iget-object v1, p0, Lorg/chromium/content/browser/DeviceOrientation;->mHandlerLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lorg/chromium/content/browser/DeviceOrientation$1;

    invoke-direct {v2, p0}, Lorg/chromium/content/browser/DeviceOrientation$1;-><init>(Lorg/chromium/content/browser/DeviceOrientation;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mThread:Ljava/lang/Thread;

    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mHandlerLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :try_start_2
    monitor-exit v1

    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mHandler:Landroid/os/Handler;

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getSensorManager()Landroid/hardware/SensorManager;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mSensorManager:Landroid/hardware/SensorManager;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "sensor"

    invoke-static {v0}, Lorg/chromium/base/WeakContext;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mSensorManager:Landroid/hardware/SensorManager;

    goto :goto_0
.end method

.method private native nativeGotOrientation(IDDD)V
.end method

.method private setHandler(Landroid/os/Handler;)V
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/DeviceOrientation;->mHandlerLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lorg/chromium/content/browser/DeviceOrientation;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mHandlerLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method getOrientationUsingGetRotationMatrix()V
    .locals 11

    const-wide v9, 0x4076800000000000L

    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mGravityVector:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mMagneticFieldVector:[F

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x9

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/chromium/content/browser/DeviceOrientation;->mGravityVector:[F

    iget-object v3, p0, Lorg/chromium/content/browser/DeviceOrientation;->mMagneticFieldVector:[F

    invoke-static {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->getRotationMatrix([F[F[F[F)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    new-array v5, v1, [F

    invoke-static {v0, v5}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    const/4 v0, 0x0

    aget v0, v5, v0

    neg-float v0, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v1

    :goto_1
    const-wide/16 v3, 0x0

    cmpg-double v0, v1, v3

    if-gez v0, :cond_2

    add-double/2addr v1, v9

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    aget v0, v5, v0

    neg-float v0, v0

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v3

    :goto_2
    const-wide v6, -0x3f99800000000000L

    cmpg-double v0, v3, v6

    if-gez v0, :cond_3

    add-double/2addr v3, v9

    goto :goto_2

    :cond_3
    const/4 v0, 0x2

    aget v0, v5, v0

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v5

    :goto_3
    const-wide v7, -0x3fa9800000000000L

    cmpg-double v0, v5, v7

    if-gez v0, :cond_4

    add-double/2addr v5, v9

    goto :goto_3

    :cond_4
    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lorg/chromium/content/browser/DeviceOrientation;->gotOrientation(DDD)V

    goto :goto_0
.end method

.method gotOrientation(DDD)V
    .locals 9

    iget-object v8, p0, Lorg/chromium/content/browser/DeviceOrientation;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    iget v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mNativePtr:I

    if-eqz v0, :cond_0

    iget v1, p0, Lorg/chromium/content/browser/DeviceOrientation;->mNativePtr:I

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lorg/chromium/content/browser/DeviceOrientation;->nativeGotOrientation(IDDD)V

    :cond_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4

    const/4 v1, 0x3

    const/4 v3, 0x0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mGravityVector:[F

    if-nez v0, :cond_0

    new-array v0, v1, [F

    iput-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mGravityVector:[F

    :cond_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    iget-object v1, p0, Lorg/chromium/content/browser/DeviceOrientation;->mGravityVector:[F

    iget-object v2, p0, Lorg/chromium/content/browser/DeviceOrientation;->mGravityVector:[F

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_1
    invoke-virtual {p0}, Lorg/chromium/content/browser/DeviceOrientation;->getOrientationUsingGetRotationMatrix()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mMagneticFieldVector:[F

    if-nez v0, :cond_1

    new-array v0, v1, [F

    iput-object v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mMagneticFieldVector:[F

    :cond_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    iget-object v1, p0, Lorg/chromium/content/browser/DeviceOrientation;->mMagneticFieldVector:[F

    iget-object v2, p0, Lorg/chromium/content/browser/DeviceOrientation;->mMagneticFieldVector:[F

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method registerForSensorType(II)Z
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/chromium/content/browser/DeviceOrientation;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1, p1}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    mul-int/lit16 v3, p2, 0x3e8

    div-int/lit8 v3, v3, 0x2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Sensor;

    invoke-direct {p0}, Lorg/chromium/content/browser/DeviceOrientation;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v1, p0, v0, v3, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    move-result v0

    goto :goto_0
.end method

.method registerForSensors(I)Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lorg/chromium/content/browser/DeviceOrientation;->registerForSensorType(II)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p0, v1, p1}, Lorg/chromium/content/browser/DeviceOrientation;->registerForSensorType(II)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/chromium/content/browser/DeviceOrientation;->unregisterForSensors()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public start(II)Z
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/DeviceOrientation;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lorg/chromium/content/browser/DeviceOrientation;->stop()V

    invoke-virtual {p0, p2}, Lorg/chromium/content/browser/DeviceOrientation;->registerForSensors(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iput p1, p0, Lorg/chromium/content/browser/DeviceOrientation;->mNativePtr:I

    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public stop()V
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/DeviceOrientation;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mNativePtr:I

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/DeviceOrientation;->mNativePtr:I

    invoke-virtual {p0}, Lorg/chromium/content/browser/DeviceOrientation;->unregisterForSensors()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method unregisterForSensors()V
    .locals 1

    invoke-direct {p0}, Lorg/chromium/content/browser/DeviceOrientation;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_0
.end method
