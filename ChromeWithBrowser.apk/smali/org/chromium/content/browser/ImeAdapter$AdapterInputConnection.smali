.class public Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;
.super Landroid/view/inputmethod/BaseInputConnection;


# instance fields
.field private mIgnoreTextInputStateUpdates:Z

.field private mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

.field private mInternalView:Landroid/view/View;

.field private mNumNestedBatchEdits:I

.field private mSingleLine:Z


# direct methods
.method protected constructor <init>(Landroid/view/View;Lorg/chromium/content/browser/ImeAdapter;Landroid/view/inputmethod/EditorInfo;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    iput v2, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mNumNestedBatchEdits:I

    iput-boolean v2, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mIgnoreTextInputStateUpdates:Z

    iput-object p1, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mInternalView:Landroid/view/View;

    iput-object p2, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-static {v0, p0}, Lorg/chromium/content/browser/ImeAdapter;->access$900(Lorg/chromium/content/browser/ImeAdapter;Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;)V

    iput-boolean v1, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mSingleLine:Z

    const/high16 v0, 0x2000000

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/16 v0, 0xa1

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->inputType:I

    # getter for: Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I
    invoke-static {p2}, Lorg/chromium/content/browser/ImeAdapter;->access$1000(Lorg/chromium/content/browser/ImeAdapter;)I

    move-result v0

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeText:I

    if-ne v0, v1, :cond_3

    iget v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    if-ltz v1, :cond_1

    if-gez v0, :cond_2

    :cond_1
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    move v1, v0

    :cond_2
    iput v1, p3, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    return-void

    :cond_3
    # getter for: Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I
    invoke-static {p2}, Lorg/chromium/content/browser/ImeAdapter;->access$1000(Lorg/chromium/content/browser/ImeAdapter;)I

    move-result v0

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeTextArea:I

    if-eq v0, v1, :cond_4

    # getter for: Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I
    invoke-static {p2}, Lorg/chromium/content/browser/ImeAdapter;->access$1000(Lorg/chromium/content/browser/ImeAdapter;)I

    move-result v0

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeContentEditable:I

    if-ne v0, v1, :cond_5

    :cond_4
    iget v0, p3, Landroid/view/inputmethod/EditorInfo;->inputType:I

    const v1, 0x2c000

    or-int/2addr v0, v1

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->inputType:I

    iget v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    iput-boolean v2, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mSingleLine:Z

    goto :goto_0

    :cond_5
    # getter for: Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I
    invoke-static {p2}, Lorg/chromium/content/browser/ImeAdapter;->access$1000(Lorg/chromium/content/browser/ImeAdapter;)I

    move-result v0

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypePassword:I

    if-ne v0, v1, :cond_6

    const/16 v0, 0xe1

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->inputType:I

    iget v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto :goto_0

    :cond_6
    # getter for: Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I
    invoke-static {p2}, Lorg/chromium/content/browser/ImeAdapter;->access$1000(Lorg/chromium/content/browser/ImeAdapter;)I

    move-result v0

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeSearch:I

    if-ne v0, v1, :cond_7

    iget v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x3

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto :goto_0

    :cond_7
    # getter for: Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I
    invoke-static {p2}, Lorg/chromium/content/browser/ImeAdapter;->access$1000(Lorg/chromium/content/browser/ImeAdapter;)I

    move-result v0

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeUrl:I

    if-ne v0, v1, :cond_8

    iget v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto :goto_0

    :cond_8
    # getter for: Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I
    invoke-static {p2}, Lorg/chromium/content/browser/ImeAdapter;->access$1000(Lorg/chromium/content/browser/ImeAdapter;)I

    move-result v0

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeEmail:I

    if-ne v0, v1, :cond_9

    const/16 v0, 0xd1

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->inputType:I

    iget v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto :goto_0

    :cond_9
    # getter for: Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I
    invoke-static {p2}, Lorg/chromium/content/browser/ImeAdapter;->access$1000(Lorg/chromium/content/browser/ImeAdapter;)I

    move-result v0

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeTel:I

    if-ne v0, v1, :cond_a

    const/4 v0, 0x3

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->inputType:I

    iget v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x5

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto/16 :goto_0

    :cond_a
    # getter for: Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I
    invoke-static {p2}, Lorg/chromium/content/browser/ImeAdapter;->access$1000(Lorg/chromium/content/browser/ImeAdapter;)I

    move-result v0

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeNumber:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->inputType:I

    iget v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x5

    iput v0, p3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto/16 :goto_0
.end method

.method private getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mInternalView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method


# virtual methods
.method public beginBatchEdit()Z
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mNumNestedBatchEdits:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    const/4 v1, 0x1

    # invokes: Lorg/chromium/content/browser/ImeAdapter;->batchStateChanged(Z)V
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ImeAdapter;->access$300(Lorg/chromium/content/browser/ImeAdapter;Z)V

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mNumNestedBatchEdits:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mNumNestedBatchEdits:I

    const/4 v0, 0x0

    return v0
.end method

.method public commitText(Ljava/lang/CharSequence;I)Z
    .locals 3

    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, p2, v0}, Lorg/chromium/content/browser/ImeAdapter;->checkCompositionQueueAndCallNative(Ljava/lang/String;IZ)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deleteSurroundingText(II)Z
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->deleteSurroundingText(II)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    # invokes: Lorg/chromium/content/browser/ImeAdapter;->deleteSurroundingText(II)Z
    invoke-static {v0, p1, p2}, Lorg/chromium/content/browser/ImeAdapter;->access$400(Lorg/chromium/content/browser/ImeAdapter;II)Z

    move-result v0

    goto :goto_0
.end method

.method public endBatchEdit()Z
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mNumNestedBatchEdits:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mNumNestedBatchEdits:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mNumNestedBatchEdits:I

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mNumNestedBatchEdits:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    # invokes: Lorg/chromium/content/browser/ImeAdapter;->batchStateChanged(Z)V
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ImeAdapter;->access$300(Lorg/chromium/content/browser/ImeAdapter;Z)V

    goto :goto_0
.end method

.method public finishComposingText()Z
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v2

    invoke-static {v1}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v1

    if-ne v2, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/view/inputmethod/BaseInputConnection;->finishComposingText()Z

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    const-string v2, ""

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lorg/chromium/content/browser/ImeAdapter;->checkCompositionQueueAndCallNative(Ljava/lang/String;IZ)Z

    move-result v0

    goto :goto_0
.end method

.method public getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;
    .locals 3

    new-instance v1, Landroid/view/inputmethod/ExtractedText;

    invoke-direct {v1}, Landroid/view/inputmethod/ExtractedText;-><init>()V

    invoke-virtual {p0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    iput v2, v1, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    iput v2, v1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, v1, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    iget-boolean v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mSingleLine:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, v1, Landroid/view/inputmethod/ExtractedText;->flags:I

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isActive()Z
    .locals 1

    invoke-direct {p0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isActive()Z

    move-result v0

    return v0
.end method

.method protected isIgnoringTextInputStateUpdates()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mIgnoreTextInputStateUpdates:Z

    return v0
.end method

.method public performContextMenuAction(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->selectAll()Z

    move-result v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->cut()Z

    move-result v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->copy()Z

    move-result v0

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->paste()Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102001f
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public performEditorAction(I)Z
    .locals 6

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->restartInput()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sEventTypeRawKeyDown:I

    const/16 v4, 0x3d

    const/4 v5, 0x0

    # invokes: Lorg/chromium/content/browser/ImeAdapter;->sendSyntheticKeyEvent(IJII)Z
    invoke-static/range {v0 .. v5}, Lorg/chromium/content/browser/ImeAdapter;->access$100(Lorg/chromium/content/browser/ImeAdapter;IJII)Z

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    const/16 v1, 0x42

    const/16 v2, 0x16

    # invokes: Lorg/chromium/content/browser/ImeAdapter;->sendKeyEventWithKeyCode(II)V
    invoke-static {v0, v1, v2}, Lorg/chromium/content/browser/ImeAdapter;->access$200(Lorg/chromium/content/browser/ImeAdapter;II)V

    goto :goto_0
.end method

.method restartInput()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mInternalView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    iput-boolean v2, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mIgnoreTextInputStateUpdates:Z

    iput v2, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mNumNestedBatchEdits:I

    return-void
.end method

.method public sendKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 6

    const/4 v2, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    # getter for: Lorg/chromium/content/browser/ImeAdapter;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;
    invoke-static {v0}, Lorg/chromium/content/browser/ImeAdapter;->access$500(Lorg/chromium/content/browser/ImeAdapter;)Lorg/chromium/content/browser/SelectionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/SelectionHandleController;->hideAndDisallowAutomaticShowing()V

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    # getter for: Lorg/chromium/content/browser/ImeAdapter;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;
    invoke-static {v0}, Lorg/chromium/content/browser/ImeAdapter;->access$600(Lorg/chromium/content/browser/ImeAdapter;)Lorg/chromium/content/browser/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->hideAndDisallowAutomaticShowing()V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v4, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x43

    if-ne v0, v1, :cond_1

    invoke-super {p0, v4, v2}, Landroid/view/inputmethod/BaseInputConnection;->deleteSurroundingText(II)Z

    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    # invokes: Lorg/chromium/content/browser/ImeAdapter;->translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z
    invoke-static {v0, p1}, Lorg/chromium/content/browser/ImeAdapter;->access$700(Lorg/chromium/content/browser/ImeAdapter;Landroid/view/KeyEvent;)Z

    return v4

    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x70

    if-ne v0, v1, :cond_2

    invoke-super {p0, v2, v4}, Landroid/view/inputmethod/BaseInputConnection;->deleteSurroundingText(II)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-static {v3}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    if-le v0, v1, :cond_3

    :goto_1
    int-to-char v2, v2

    invoke-static {v2}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v1, v0, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0

    :cond_3
    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_1
.end method

.method public setComposingRegion(II)Z
    .locals 3

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/view/inputmethod/BaseInputConnection;->setComposingRegion(II)Z

    iget-object v2, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    # invokes: Lorg/chromium/content/browser/ImeAdapter;->setComposingRegion(II)Z
    invoke-static {v2, v0, v1}, Lorg/chromium/content/browser/ImeAdapter;->access$800(Lorg/chromium/content/browser/ImeAdapter;II)Z

    move-result v0

    return v0
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)Z
    .locals 3

    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Lorg/chromium/content/browser/ImeAdapter;->checkCompositionQueueAndCallNative(Ljava/lang/String;IZ)Z

    move-result v0

    return v0
.end method

.method public setEditableText(Ljava/lang/String;IIII)V
    .locals 12

    invoke-virtual {p0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    invoke-static {v1}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v4

    invoke-static {v1}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v5

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {p2, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    invoke-static {p3, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    move/from16 v0, p4

    invoke-static {v0, v9}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    move/from16 v0, p5

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v10

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const/4 v6, 0x0

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v11

    invoke-interface {v1, v6, v11, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    :cond_0
    if-ne v2, v7, :cond_2

    if-ne v3, v8, :cond_2

    if-ne v4, v9, :cond_2

    if-ne v5, v10, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {v1, v7, v8}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    invoke-super {p0, v9, v10}, Landroid/view/inputmethod/BaseInputConnection;->setComposingRegion(II)Z

    iget-boolean v1, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mIgnoreTextInputStateUpdates:Z

    if-nez v1, :cond_1

    invoke-virtual {p0, v7, v8, v9, v10}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->updateSelection(IIII)V

    goto :goto_0
.end method

.method setIgnoreTextInputStateUpdates(Z)V
    .locals 4

    iput-boolean p1, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mIgnoreTextInputStateUpdates:Z

    if-eqz p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-static {v0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v3

    invoke-static {v0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v0

    invoke-virtual {p0, v1, v2, v3, v0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->updateSelection(IIII)V

    goto :goto_0
.end method

.method public setSelection(II)Z
    .locals 1

    if-ltz p1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setSelection(II)Z

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/content/browser/ImeAdapter;->setEditableSelectionOffsets(II)Z

    move-result v0

    goto :goto_0
.end method

.method protected updateSelection(IIII)V
    .locals 6

    invoke-direct {p0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->mInternalView:Landroid/view/View;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/view/inputmethod/InputMethodManager;->updateSelection(Landroid/view/View;IIII)V

    return-void
.end method
