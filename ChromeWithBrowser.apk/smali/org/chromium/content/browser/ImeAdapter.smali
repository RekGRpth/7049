.class Lorg/chromium/content/browser/ImeAdapter;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "content"
.end annotation


# static fields
.field static final COMPOSITION_KEY_CODE:I = 0xe5

.field private static final INPUT_DISMISS_DELAY:I = 0x96

.field static sEventTypeChar:I

.field static sEventTypeKeyUp:I

.field static sEventTypeRawKeyDown:I

.field static sModifierAlt:I

.field static sModifierCapsLockOn:I

.field static sModifierCtrl:I

.field static sModifierNumLockOn:I

.field static sModifierShift:I

.field static sTextInputTypeContentEditable:I

.field static sTextInputTypeEmail:I

.field static sTextInputTypeNone:I

.field static sTextInputTypeNumber:I

.field static sTextInputTypePassword:I

.field static sTextInputTypeSearch:I

.field static sTextInputTypeTel:I

.field static sTextInputTypeText:I

.field static sTextInputTypeTextArea:I

.field static sTextInputTypeUrl:I

.field static sTextInputTypeWeek:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDismissInput:Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;

.field private mHandler:Landroid/os/Handler;

.field private mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

.field private mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

.field protected mIsShowWithoutHideOutstanding:Z

.field private mNativeImeAdapterAndroid:I

.field private mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

.field private mTextInputType:I

.field private mViewEmbedder:Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;


# direct methods
.method constructor <init>(Landroid/content/Context;Lorg/chromium/content/browser/SelectionHandleController;Lorg/chromium/content/browser/InsertionHandleController;Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mDismissInput:Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mIsShowWithoutHideOutstanding:Z

    iput-object p1, p0, Lorg/chromium/content/browser/ImeAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lorg/chromium/content/browser/ImeAdapter;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    iput-object p3, p0, Lorg/chromium/content/browser/ImeAdapter;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    iput-object p4, p0, Lorg/chromium/content/browser/ImeAdapter;->mViewEmbedder:Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/content/browser/ImeAdapter;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ImeAdapter;->dismissInput(Z)V

    return-void
.end method

.method static synthetic access$100(Lorg/chromium/content/browser/ImeAdapter;IJII)Z
    .locals 1

    invoke-direct/range {p0 .. p5}, Lorg/chromium/content/browser/ImeAdapter;->sendSyntheticKeyEvent(IJII)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lorg/chromium/content/browser/ImeAdapter;)I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I

    return v0
.end method

.method static synthetic access$200(Lorg/chromium/content/browser/ImeAdapter;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/chromium/content/browser/ImeAdapter;->sendKeyEventWithKeyCode(II)V

    return-void
.end method

.method static synthetic access$300(Lorg/chromium/content/browser/ImeAdapter;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ImeAdapter;->batchStateChanged(Z)V

    return-void
.end method

.method static synthetic access$400(Lorg/chromium/content/browser/ImeAdapter;II)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lorg/chromium/content/browser/ImeAdapter;->deleteSurroundingText(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lorg/chromium/content/browser/ImeAdapter;)Lorg/chromium/content/browser/SelectionHandleController;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    return-object v0
.end method

.method static synthetic access$600(Lorg/chromium/content/browser/ImeAdapter;)Lorg/chromium/content/browser/InsertionHandleController;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    return-object v0
.end method

.method static synthetic access$700(Lorg/chromium/content/browser/ImeAdapter;Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ImeAdapter;->translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lorg/chromium/content/browser/ImeAdapter;II)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lorg/chromium/content/browser/ImeAdapter;->setComposingRegion(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lorg/chromium/content/browser/ImeAdapter;Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ImeAdapter;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    return-void
.end method

.method private batchStateChanged(Z)V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/content/browser/ImeAdapter;->nativeImeBatchStateChanged(IZ)V

    goto :goto_0
.end method

.method private cancelComposition()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->restartInput()V

    :cond_0
    return-void
.end method

.method private deleteSurroundingText(II)Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0, p1, p2}, Lorg/chromium/content/browser/ImeAdapter;->nativeDeleteSurroundingText(III)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private dismissInput(Z)V
    .locals 1

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ImeAdapter;->hideKeyboard(Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mViewEmbedder:Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;

    invoke-interface {v0}, Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;->onDismissInput()V

    return-void
.end method

.method private static getModifiers(I)I
    .locals 2

    const/4 v0, 0x0

    and-int/lit8 v1, p0, 0x1

    if-eqz v1, :cond_0

    sget v0, Lorg/chromium/content/browser/ImeAdapter;->sModifierShift:I

    or-int/lit8 v0, v0, 0x0

    :cond_0
    and-int/lit8 v1, p0, 0x2

    if-eqz v1, :cond_1

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sModifierAlt:I

    or-int/2addr v0, v1

    :cond_1
    and-int/lit16 v1, p0, 0x1000

    if-eqz v1, :cond_2

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sModifierCtrl:I

    or-int/2addr v0, v1

    :cond_2
    const/high16 v1, 0x100000

    and-int/2addr v1, p0

    if-eqz v1, :cond_3

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sModifierCapsLockOn:I

    or-int/2addr v0, v1

    :cond_3
    const/high16 v1, 0x200000

    and-int/2addr v1, p0

    if-eqz v1, :cond_4

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sModifierNumLockOn:I

    or-int/2addr v0, v1

    :cond_4
    return v0
.end method

.method private hideKeyboard(Z)V
    .locals 4

    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/chromium/content/browser/ImeAdapter;->mIsShowWithoutHideOutstanding:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mContext:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mViewEmbedder:Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;

    invoke-interface {v1}, Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;->getAttachedView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-eqz p1, :cond_1

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mViewEmbedder:Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;

    invoke-interface {v1}, Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;->getNewShowKeyboardReceiver()Landroid/os/ResultReceiver;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v2, v3, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static initializeTextInputTypes(IIIIIIIIIIIIIIII)V
    .locals 0

    sput p0, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeNone:I

    sput p1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeText:I

    sput p2, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeTextArea:I

    sput p3, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypePassword:I

    sput p4, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeSearch:I

    sput p5, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeUrl:I

    sput p6, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeEmail:I

    sput p7, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeTel:I

    sput p8, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeNumber:I

    sput p14, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeWeek:I

    sput p15, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeContentEditable:I

    return-void
.end method

.method static initializeWebInputEvents(IIIIIIII)V
    .locals 0

    sput p0, Lorg/chromium/content/browser/ImeAdapter;->sEventTypeRawKeyDown:I

    sput p1, Lorg/chromium/content/browser/ImeAdapter;->sEventTypeKeyUp:I

    sput p2, Lorg/chromium/content/browser/ImeAdapter;->sEventTypeChar:I

    sput p3, Lorg/chromium/content/browser/ImeAdapter;->sModifierShift:I

    sput p4, Lorg/chromium/content/browser/ImeAdapter;->sModifierAlt:I

    sput p5, Lorg/chromium/content/browser/ImeAdapter;->sModifierCtrl:I

    sput p6, Lorg/chromium/content/browser/ImeAdapter;->sModifierCapsLockOn:I

    sput p7, Lorg/chromium/content/browser/ImeAdapter;->sModifierNumLockOn:I

    return-void
.end method

.method static isTextInputType(I)Z
    .locals 1

    sget v0, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeNone:I

    if-eq p0, v0, :cond_0

    invoke-static {p0}, Lorg/chromium/content/browser/InputDialogContainer;->isDialogInputType(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeAttachImeAdapter(I)V
.end method

.method private native nativeCommitText(ILjava/lang/String;)V
.end method

.method private native nativeCopy(I)V
.end method

.method private native nativeCut(I)V
.end method

.method private native nativeDeleteSurroundingText(III)V
.end method

.method private native nativeImeBatchStateChanged(IZ)V
.end method

.method private native nativePaste(I)V
.end method

.method private native nativeSelectAll(I)V
.end method

.method private native nativeSendKeyEvent(ILandroid/view/KeyEvent;IIJIZI)Z
.end method

.method private native nativeSendSyntheticKeyEvent(IIJII)Z
.end method

.method private native nativeSetComposingRegion(III)V
.end method

.method private native nativeSetComposingText(ILjava/lang/String;I)V
.end method

.method private native nativeSetEditableSelectionOffsets(III)V
.end method

.method private native nativeUnselect(I)V
.end method

.method private sendKeyEventWithKeyCode(II)V
    .locals 15

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    new-instance v0, Landroid/view/KeyEvent;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x0

    move-wide v3, v1

    move/from16 v6, p1

    move/from16 v11, p2

    invoke-direct/range {v0 .. v11}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ImeAdapter;->translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z

    new-instance v3, Landroid/view/KeyEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, -0x1

    const/4 v13, 0x0

    move-wide v6, v1

    move/from16 v9, p1

    move/from16 v14, p2

    invoke-direct/range {v3 .. v14}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    invoke-direct {p0, v3}, Lorg/chromium/content/browser/ImeAdapter;->translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z

    return-void
.end method

.method private sendSyntheticKeyEvent(IJII)Z
    .locals 7

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    move-object v0, p0

    move v2, p1

    move-wide v3, p2

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ImeAdapter;->nativeSendSyntheticKeyEvent(IIJII)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setComposingRegion(II)Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0, p1, p2}, Lorg/chromium/content/browser/ImeAdapter;->nativeSetComposingRegion(III)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setInputConnection(Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ImeAdapter;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    return-void
.end method

.method private shouldSendKeyEventWithKeyCode(Ljava/lang/String;)I
    .locals 3

    const/16 v0, 0xe5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "\n"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x42

    goto :goto_0

    :cond_2
    const-string v1, "\t"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x3d

    goto :goto_0
.end method

.method private showKeyboard()V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mIsShowWithoutHideOutstanding:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mContext:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mViewEmbedder:Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;

    invoke-interface {v1}, Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;->getAttachedView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/chromium/content/browser/ImeAdapter;->mViewEmbedder:Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;

    invoke-interface {v3}, Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;->getNewShowKeyboardReceiver()Landroid/os/ResultReceiver;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    return-void
.end method

.method private translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z
    .locals 10

    const/4 v0, 0x0

    iget v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-eqz v1, :cond_2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    :cond_2
    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mViewEmbedder:Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;

    invoke-interface {v1, v0}, Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;->onImeEvent(Z)V

    iget v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v0

    invoke-static {v0}, Lorg/chromium/content/browser/ImeAdapter;->getModifiers(I)I

    move-result v4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v5

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v7

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v8

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v9

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Lorg/chromium/content/browser/ImeAdapter;->nativeSendKeyEvent(ILandroid/view/KeyEvent;IIJIZI)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method attach(I)V
    .locals 1

    iput p1, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-eqz p1, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ImeAdapter;->nativeAttachImeAdapter(I)V

    :cond_0
    return-void
.end method

.method attach(II)V
    .locals 1

    iput p1, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    iput p2, p0, Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ImeAdapter;->nativeAttachImeAdapter(I)V

    return-void
.end method

.method attachAndShowIfNeeded(IILjava/lang/String;Z)V
    .locals 4

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mDismissInput:Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeNone:I

    if-ne v0, v1, :cond_1

    if-nez p4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1, p2}, Lorg/chromium/content/browser/ImeAdapter;->isFor(II)Z

    move-result v0

    if-nez v0, :cond_3

    sget v0, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeNone:I

    if-ne p2, v0, :cond_2

    new-instance v0, Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;

    invoke-direct {v0, p0, p1}, Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;-><init>(Lorg/chromium/content/browser/ImeAdapter;I)V

    iput-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mDismissInput:Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mDismissInput:Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I

    invoke-virtual {p0, p1, p2}, Lorg/chromium/content/browser/ImeAdapter;->attach(II)V

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mContext:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mViewEmbedder:Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;

    invoke-interface {v1}, Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;->getAttachedView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    if-eqz p4, :cond_0

    invoke-direct {p0}, Lorg/chromium/content/browser/ImeAdapter;->showKeyboard()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lorg/chromium/content/browser/ImeAdapter;->hasInputType()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    invoke-direct {p0}, Lorg/chromium/content/browser/ImeAdapter;->showKeyboard()V

    goto :goto_0
.end method

.method checkCompositionQueueAndCallNative(Ljava/lang/String;IZ)Z
    .locals 7

    const/4 v6, 0x0

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v0, :cond_0

    :goto_0
    return v6

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;

    invoke-virtual {v1}, Lorg/chromium/content/browser/SelectionHandleController;->hideAndDisallowAutomaticShowing()V

    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-virtual {v1}, Lorg/chromium/content/browser/InsertionHandleController;->hideAndDisallowAutomaticShowing()V

    :cond_1
    iget-object v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mViewEmbedder:Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;

    invoke-interface {v1, v0}, Lorg/chromium/content/browser/ImeAdapter$ViewEmbedder;->onImeEvent(Z)V

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ImeAdapter;->shouldSendKeyEventWithKeyCode(Ljava/lang/String;)I

    move-result v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/16 v0, 0xe5

    if-eq v5, v0, :cond_2

    const/4 v0, 0x6

    invoke-direct {p0, v5, v0}, Lorg/chromium/content/browser/ImeAdapter;->sendKeyEventWithKeyCode(II)V

    :goto_1
    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    iget v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    sget v2, Lorg/chromium/content/browser/ImeAdapter;->sEventTypeRawKeyDown:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ImeAdapter;->nativeSendSyntheticKeyEvent(IIJII)Z

    if-eqz p3, :cond_3

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/content/browser/ImeAdapter;->nativeCommitText(ILjava/lang/String;)V

    :goto_2
    iget v1, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    sget v2, Lorg/chromium/content/browser/ImeAdapter;->sEventTypeKeyUp:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lorg/chromium/content/browser/ImeAdapter;->nativeSendSyntheticKeyEvent(IIJII)Z

    goto :goto_1

    :cond_3
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0, p1, p2}, Lorg/chromium/content/browser/ImeAdapter;->nativeSetComposingText(ILjava/lang/String;I)V

    goto :goto_2
.end method

.method commitText()V
    .locals 2

    invoke-direct {p0}, Lorg/chromium/content/browser/ImeAdapter;->cancelComposition()V

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lorg/chromium/content/browser/ImeAdapter;->nativeCommitText(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method copy()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ImeAdapter;->nativeCopy(I)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method cut()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ImeAdapter;->nativeCut(I)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method detach()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    iput v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I

    return-void
.end method

.method dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ImeAdapter;->translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method hasInputType()Z
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I

    sget v1, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeNone:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method hasTextInputType()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I

    invoke-static {v0}, Lorg/chromium/content/browser/ImeAdapter;->isTextInputType(I)Z

    move-result v0

    return v0
.end method

.method isActive()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mInputConnection:Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter$AdapterInputConnection;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isFor(II)Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mTextInputType:I

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isNativeImeAdapterAttached()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method paste()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ImeAdapter;->nativePaste(I)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method selectAll()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ImeAdapter;->nativeSelectAll(I)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected setEditableSelectionOffsets(II)Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0, p1, p2}, Lorg/chromium/content/browser/ImeAdapter;->nativeSetEditableSelectionOffsets(III)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method unselect()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ImeAdapter;->mNativeImeAdapterAndroid:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ImeAdapter;->nativeUnselect(I)V

    const/4 v0, 0x1

    goto :goto_0
.end method
