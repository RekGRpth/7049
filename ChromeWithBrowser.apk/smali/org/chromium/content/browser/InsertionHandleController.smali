.class abstract Lorg/chromium/content/browser/InsertionHandleController;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/content/browser/CursorController;


# instance fields
.field private mAllowAutomaticShowing:Z

.field private mContext:Landroid/content/Context;

.field private mHandle:Lorg/chromium/content/browser/HandleView;

.field private mIsShowing:Z

.field private mParent:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/chromium/content/browser/InsertionHandleController;->mParent:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/content/browser/InsertionHandleController;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/content/browser/InsertionHandleController;)Lorg/chromium/content/browser/HandleView;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/content/browser/InsertionHandleController;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mParent:Landroid/view/View;

    return-object v0
.end method

.method private createHandleIfNeeded()V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    if-nez v0, :cond_0

    new-instance v0, Lorg/chromium/content/browser/HandleView;

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/chromium/content/browser/InsertionHandleController;->mParent:Landroid/view/View;

    invoke-direct {v0, p0, v1, v2}, Lorg/chromium/content/browser/HandleView;-><init>(Lorg/chromium/content/browser/CursorController;ILandroid/view/View;)V

    iput-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    :cond_0
    return-void
.end method

.method private showHandleIfNeeded()V
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mIsShowing:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mIsShowing:Z

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->show()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/InsertionHandleController;->setHandleVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method allowAutomaticShowing()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mAllowAutomaticShowing:Z

    return-void
.end method

.method public beforeStartUpdatingPosition(Lorg/chromium/content/browser/HandleView;)V
    .locals 0

    return-void
.end method

.method beginHandleFadeIn()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->beginFadeIn()V

    return-void
.end method

.method canPaste()Z
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mContext:Landroid/content/Context;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v0

    return v0
.end method

.method public getHandleViewForTest()Lorg/chromium/content/browser/HandleView;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    return-object v0
.end method

.method getHandleX()I
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->getAdjustedPositionX()I

    move-result v0

    return v0
.end method

.method getHandleY()I
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->getAdjustedPositionY()I

    move-result v0

    return v0
.end method

.method protected abstract getLineHeight()I
.end method

.method public hide()V
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mIsShowing:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->hide()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mIsShowing:Z

    :cond_1
    return-void
.end method

.method hideAndDisallowAutomaticShowing()V
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/InsertionHandleController;->hide()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mAllowAutomaticShowing:Z

    return-void
.end method

.method public isShowing()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mIsShowing:Z

    return v0
.end method

.method onCursorPositionChanged()V
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mAllowAutomaticShowing:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/InsertionHandleController;->showHandle()V

    :cond_0
    return-void
.end method

.method public onDetached()V
    .locals 0

    return-void
.end method

.method public onTouchModeChanged(Z)V
    .locals 0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/InsertionHandleController;->hide()V

    :cond_0
    return-void
.end method

.method protected abstract paste()V
.end method

.method protected abstract setCursorPosition(II)V
.end method

.method setHandlePosition(II)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/content/browser/HandleView;->positionAt(II)V

    return-void
.end method

.method setHandlePosition(Landroid/graphics/PointF;)V
    .locals 2

    iget v0, p1, Landroid/graphics/PointF;->x:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lorg/chromium/content/browser/InsertionHandleController;->setHandlePosition(II)V

    return-void
.end method

.method setHandleVisibility(I)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/HandleView;->setVisibility(I)V

    return-void
.end method

.method showHandle()V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/content/browser/InsertionHandleController;->createHandleIfNeeded()V

    invoke-direct {p0}, Lorg/chromium/content/browser/InsertionHandleController;->showHandleIfNeeded()V

    return-void
.end method

.method showHandleWithPastePopup()V
    .locals 0

    invoke-virtual {p0}, Lorg/chromium/content/browser/InsertionHandleController;->showHandle()V

    invoke-virtual {p0}, Lorg/chromium/content/browser/InsertionHandleController;->showPastePopup()V

    return-void
.end method

.method showPastePopup()V
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mIsShowing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->showPastePopupWindow()V

    :cond_0
    return-void
.end method

.method public updatePosition(Lorg/chromium/content/browser/HandleView;II)V
    .locals 0

    invoke-virtual {p0, p2, p3}, Lorg/chromium/content/browser/InsertionHandleController;->setCursorPosition(II)V

    return-void
.end method
