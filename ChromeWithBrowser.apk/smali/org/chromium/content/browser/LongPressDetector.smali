.class Lorg/chromium/content/browser/LongPressDetector;
.super Ljava/lang/Object;


# static fields
.field private static final LONGPRESS_TIMEOUT:I

.field private static final LONG_PRESS:I = 0x2

.field private static final TAP_TIMEOUT:I


# instance fields
.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mInLongPress:Z

.field private final mLongPressDelegate:Lorg/chromium/content/browser/LongPressDetector$LongPressDelegate;

.field private final mLongPressHandler:Landroid/os/Handler;

.field private mMoveConfirmed:Z

.field private mTouchInitialX:I

.field private mTouchInitialY:I

.field private final mTouchSlopSquare:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Lorg/chromium/content/browser/LongPressDetector;->LONGPRESS_TIMEOUT:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Lorg/chromium/content/browser/LongPressDetector;->TAP_TIMEOUT:I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lorg/chromium/content/browser/LongPressDetector$LongPressDelegate;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lorg/chromium/content/browser/LongPressDetector;->mLongPressDelegate:Lorg/chromium/content/browser/LongPressDetector$LongPressDelegate;

    new-instance v0, Lorg/chromium/content/browser/LongPressDetector$LongPressHandler;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/LongPressDetector$LongPressHandler;-><init>(Lorg/chromium/content/browser/LongPressDetector;)V

    iput-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mLongPressHandler:Landroid/os/Handler;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/2addr v0, v0

    iput v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mTouchSlopSquare:I

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/content/browser/LongPressDetector;)V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/content/browser/LongPressDetector;->dispatchLongPress()V

    return-void
.end method

.method private canHandle(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private dispatchLongPress()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mInLongPress:Z

    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mLongPressDelegate:Lorg/chromium/content/browser/LongPressDetector$LongPressDelegate;

    iget-object v1, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Lorg/chromium/content/browser/LongPressDetector$LongPressDelegate;->onLongPress(Landroid/view/MotionEvent;)V

    return-void
.end method


# virtual methods
.method cancelLongPress()V
    .locals 2

    const/4 v1, 0x2

    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mLongPressHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mLongPressHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    return-void
.end method

.method cancelLongPressIfNeeded(Landroid/view/MotionEvent;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    invoke-virtual {p0}, Lorg/chromium/content/browser/LongPressDetector;->hasPendingMessage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v0

    iget-object v2, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v0

    sget v2, Lorg/chromium/content/browser/LongPressDetector;->TAP_TIMEOUT:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    sget v2, Lorg/chromium/content/browser/LongPressDetector;->LONGPRESS_TIMEOUT:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iput-boolean v4, p0, Lorg/chromium/content/browser/LongPressDetector;->mInLongPress:Z

    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mLongPressHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    sub-float v0, v2, v0

    float-to-int v0, v0

    iget-object v2, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    mul-int/2addr v0, v0

    mul-int/2addr v1, v1

    add-int/2addr v0, v1

    iget v1, p0, Lorg/chromium/content/browser/LongPressDetector;->mTouchSlopSquare:I

    if-le v0, v1, :cond_0

    iput-boolean v4, p0, Lorg/chromium/content/browser/LongPressDetector;->mInLongPress:Z

    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mLongPressHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method cancelLongPressIfNeeded(Ljava/util/Iterator;)V
    .locals 5

    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v3

    cmp-long v3, v3, v1

    if-nez v3, :cond_0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/LongPressDetector;->cancelLongPressIfNeeded(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method confirmOfferMoveEventToJavaScript(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-boolean v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mMoveConfirmed:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget v1, p0, Lorg/chromium/content/browser/LongPressDetector;->mTouchInitialX:I

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget v2, p0, Lorg/chromium/content/browser/LongPressDetector;->mTouchInitialY:I

    sub-int/2addr v1, v2

    mul-int/2addr v0, v0

    mul-int/2addr v1, v1

    add-int/2addr v0, v1

    iget v1, p0, Lorg/chromium/content/browser/LongPressDetector;->mTouchSlopSquare:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mMoveConfirmed:Z

    :cond_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mMoveConfirmed:Z

    return v0
.end method

.method hasPendingMessage()Z
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mLongPressHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    return v0
.end method

.method isInLongPress()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mInLongPress:Z

    return v0
.end method

.method onOfferTouchEventToJavaScript(Landroid/view/MotionEvent;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mMoveConfirmed:Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mTouchInitialX:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mTouchInitialY:I

    :cond_0
    return-void
.end method

.method sendLongPressGestureForTest()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lorg/chromium/content/browser/LongPressDetector;->dispatchLongPress()V

    goto :goto_0
.end method

.method startLongPressTimerIfNeeded(Landroid/view/MotionEvent;)V
    .locals 6

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/LongPressDetector;->canHandle(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    :cond_1
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    iget-object v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mLongPressHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/chromium/content/browser/LongPressDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v4, Lorg/chromium/content/browser/LongPressDetector;->TAP_TIMEOUT:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    sget v4, Lorg/chromium/content/browser/LongPressDetector;->LONGPRESS_TIMEOUT:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/LongPressDetector;->mInLongPress:Z

    goto :goto_0
.end method
