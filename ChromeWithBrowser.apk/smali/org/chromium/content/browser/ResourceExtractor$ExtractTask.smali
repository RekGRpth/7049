.class Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;
.super Landroid/os/AsyncTask;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BUFFER_SIZE:I = 0x4000


# instance fields
.field final synthetic this$0:Lorg/chromium/content/browser/ResourceExtractor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/content/browser/ResourceExtractor;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/content/browser/ResourceExtractor;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private checkPakTimestamp()Ljava/lang/String;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/content/browser/ResourceExtractor;->access$200(Lorg/chromium/content/browser/ResourceExtractor;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lorg/chromium/content/browser/ResourceExtractor;->access$200(Lorg/chromium/content/browser/ResourceExtractor;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "pak_timestamp-"

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "pak_timestamp-"

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pak_timestamp-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mOutputDir:Ljava/io/File;
    invoke-static {v1}, Lorg/chromium/content/browser/ResourceExtractor;->access$100(Lorg/chromium/content/browser/ResourceExtractor;)Ljava/io/File;

    move-result-object v1

    new-instance v2, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask$1;

    invoke-direct {v2, p0}, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask$1;-><init>(Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;)V

    invoke-virtual {v1, v2}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 18

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->sMandatoryPaks:[Ljava/lang/String;
    invoke-static {}, Lorg/chromium/content/browser/ResourceExtractor;->access$000()[Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    sget-boolean v1, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "No pak files specified.  Call setMandatoryPaksToExtract before beginning the resource extractions"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mOutputDir:Ljava/io/File;
    invoke-static {v1}, Lorg/chromium/content/browser/ResourceExtractor;->access$100(Lorg/chromium/content/browser/ResourceExtractor;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mOutputDir:Ljava/io/File;
    invoke-static {v1}, Lorg/chromium/content/browser/ResourceExtractor;->access$100(Lorg/chromium/content/browser/ResourceExtractor;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "ResourceExtractor"

    const-string v2, "Unable to create pak resources directory!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    invoke-direct/range {p0 .. p0}, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->checkPakTimestamp()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lorg/chromium/content/browser/ResourceExtractor;->access$200(Lorg/chromium/content/browser/ResourceExtractor;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/content/browser/ResourceExtractor;->deleteFiles(Landroid/content/Context;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lorg/chromium/content/browser/ResourceExtractor;->access$200(Lorg/chromium/content/browser/ResourceExtractor;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v1, "Pak filenames"

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v8, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/util/HashSet;

    invoke-static {}, Lorg/chromium/base/LocaleUtils;->getDefaultLocale()Ljava/lang/String;

    move-result-object v2

    const-string v3, "-"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v4, v2, v3

    const-string v2, "Last language"

    const-string v3, ""

    invoke-interface {v8, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v2

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->sMandatoryPaks:[Ljava/lang/String;
    invoke-static {}, Lorg/chromium/content/browser/ResourceExtractor;->access$000()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-lt v2, v3, :cond_5

    const/4 v3, 0x1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mOutputDir:Ljava/io/File;
    invoke-static {v9}, Lorg/chromium/content/browser/ResourceExtractor;->access$100(Lorg/chromium/content/browser/ResourceExtractor;)Ljava/io/File;

    move-result-object v9

    invoke-direct {v6, v9, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_6

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_5
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "Last language"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->sMandatoryPaks:[Ljava/lang/String;
    invoke-static {}, Lorg/chromium/content/browser/ResourceExtractor;->access$000()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v6, :cond_8

    aget-object v9, v5, v2

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-lez v10, :cond_7

    const/16 v10, 0x7c

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_7
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "\\Q"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\\E"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_8
    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->sExtractImplicitLocalePak:Z
    invoke-static {}, Lorg/chromium/content/browser/ResourceExtractor;->access$300()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_9

    const/16 v2, 0x7c

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "(-\\w+)?\\.pak"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lorg/chromium/content/browser/ResourceExtractor;->access$200(Lorg/chromium/content/browser/ResourceExtractor;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v10

    const/4 v3, 0x0

    :try_start_0
    const-string v2, ""

    invoke-virtual {v10, v2}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    array-length v12, v11

    const/4 v2, 0x0

    move v6, v2

    move-object v2, v3

    :goto_3
    if-ge v6, v12, :cond_13

    aget-object v13, v11, v6

    invoke-virtual {v9, v13}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_11

    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mOutputDir:Ljava/io/File;
    invoke-static {v3}, Lorg/chromium/content/browser/ResourceExtractor;->access$100(Lorg/chromium/content/browser/ResourceExtractor;)Ljava/io/File;

    move-result-object v3

    invoke-direct {v14, v3, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_11

    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {v10, v13}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v5

    :try_start_2
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    :try_start_3
    const-string v4, "ResourceExtractor"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Extracting resource "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v4, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v2, :cond_b

    const/16 v2, 0x4000

    new-array v2, v2, [B

    :cond_b
    :goto_4
    const/4 v4, 0x0

    const/16 v15, 0x4000

    invoke-virtual {v5, v2, v4, v15}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    const/4 v15, -0x1

    if-eq v4, v15, :cond_e

    const/4 v15, 0x0

    invoke-virtual {v3, v2, v15, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v1

    move-object v2, v3

    move-object v3, v5

    :goto_5
    if-eqz v3, :cond_c

    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_c
    if-eqz v2, :cond_d

    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    :cond_d
    throw v1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :catch_0
    move-exception v1

    const-string v2, "ResourceExtractor"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception unpacking required pak resources: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lorg/chromium/content/browser/ResourceExtractor;->access$200(Lorg/chromium/content/browser/ResourceExtractor;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/content/browser/ResourceExtractor;->deleteFiles(Landroid/content/Context;)V

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_e
    :try_start_6
    invoke-virtual {v3}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v14}, Ljava/io/File;->length()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v4, v14, v16

    if-nez v4, :cond_f

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " extracted with 0 length!"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_f
    invoke-virtual {v1, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v5, :cond_10

    :try_start_7
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_10
    :try_start_8
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    :cond_11
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto/16 :goto_3

    :catchall_1
    move-exception v1

    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    throw v1

    :catchall_2
    move-exception v1

    if-eqz v2, :cond_12

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    :cond_12
    throw v1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    :cond_13
    if-eqz v7, :cond_14

    :try_start_9
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/chromium/content/browser/ResourceExtractor$ExtractTask;->this$0:Lorg/chromium/content/browser/ResourceExtractor;

    # getter for: Lorg/chromium/content/browser/ResourceExtractor;->mOutputDir:Ljava/io/File;
    invoke-static {v3}, Lorg/chromium/content/browser/ResourceExtractor;->access$100(Lorg/chromium/content/browser/ResourceExtractor;)Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    :cond_14
    :goto_6
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "Pak filenames"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "Pak filenames"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 v1, 0x0

    goto/16 :goto_0

    :catch_1
    move-exception v2

    const-string v2, "ResourceExtractor"

    const-string v3, "Failed to write resource pak timestamp!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :catchall_3
    move-exception v1

    move-object v2, v4

    goto/16 :goto_5

    :catchall_4
    move-exception v1

    move-object v2, v4

    move-object v3, v5

    goto/16 :goto_5

    :cond_15
    move v2, v3

    goto/16 :goto_1
.end method
