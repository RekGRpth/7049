.class public Lorg/chromium/content/browser/SandboxedProcessConnection;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EXTRA_COMMAND_LINE:Ljava/lang/String; = "com.google.android.apps.chrome.extra.sandbox_command_line"

.field public static final EXTRA_CPU_COUNT:Ljava/lang/String; = "com.google.android.apps.chrome.extra.cpu_count"

.field public static final EXTRA_CPU_FEATURES:Ljava/lang/String; = "com.google.android.apps.chrome.extra.cpu_features"

.field public static final EXTRA_FILES_FD_SUFFIX:Ljava/lang/String; = "_fd"

.field public static final EXTRA_FILES_ID_SUFFIX:Ljava/lang/String; = "_id"

.field public static final EXTRA_FILES_PREFIX:Ljava/lang/String; = "com.google.android.apps.chrome.extra.sandbox_extraFile_"

.field public static final EXTRA_NATIVE_LIBRARY_NAME:Ljava/lang/String; = "com.google.android.apps.chrome.extra.sandbox_native_library_name"

.field private static final TAG:Ljava/lang/String; = "SandboxedProcessConnection"


# instance fields
.field private mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

.field private final mContext:Landroid/content/Context;

.field private final mDeathCallback:Lorg/chromium/content/browser/SandboxedProcessConnection$DeathCallback;

.field private mHighPriorityConnection:Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;

.field private mHighPriorityConnectionCount:I

.field private mIsBound:Z

.field private mPID:I

.field private mService:Lorg/chromium/content/common/ISandboxedProcessService;

.field private mServiceConnectComplete:Z

.field private final mServiceNumber:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/content/browser/SandboxedProcessConnection;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/browser/SandboxedProcessConnection;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;ILorg/chromium/content/browser/SandboxedProcessConnection$DeathCallback;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mService:Lorg/chromium/content/common/ISandboxedProcessService;

    iput-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mServiceConnectComplete:Z

    iput v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mPID:I

    iput-object v1, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnection:Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;

    iput v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnectionCount:I

    iput-object p1, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mContext:Landroid/content/Context;

    iput p2, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mServiceNumber:I

    iput-object p3, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mDeathCallback:Lorg/chromium/content/browser/SandboxedProcessConnection$DeathCallback;

    return-void
.end method

.method static synthetic access$100(Lorg/chromium/content/browser/SandboxedProcessConnection;)Landroid/content/Intent;
    .locals 1

    invoke-direct {p0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->createServiceBindIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/content/browser/SandboxedProcessConnection;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private createServiceBindIntent()Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lorg/chromium/content/app/SandboxedProcessService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mContext:Landroid/content/Context;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mServiceNumber:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private doConnectionSetup()V
    .locals 10

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    sget-boolean v0, Lorg/chromium/content/browser/SandboxedProcessConnection;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mServiceConnectComplete:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    iget-object v0, v0, Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;->mOnConnectionCallback:Ljava/lang/Runnable;

    move-object v2, v0

    :goto_0
    if-nez v2, :cond_5

    invoke-virtual {p0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->unbind()V

    :cond_2
    :goto_1
    iput-object v3, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    :cond_3
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    :goto_2
    return-void

    :cond_4
    move-object v2, v3

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mService:Lorg/chromium/content/common/ISandboxedProcessService;

    if-eqz v0, :cond_2

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v0, "com.google.android.apps.chrome.extra.sandbox_command_line"

    iget-object v5, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    iget-object v5, v5, Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;->mCommandLine:[Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    iget-object v5, v0, Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;->mFilesToBeMapped:[Lorg/chromium/content/browser/FileDescriptorInfo;

    array-length v0, v5

    new-array v6, v0, [Landroid/os/ParcelFileDescriptor;

    move v0, v1

    :goto_3
    array-length v7, v5

    if-ge v0, v7, :cond_8

    aget-object v7, v5, v0

    iget v7, v7, Lorg/chromium/content/browser/FileDescriptorInfo;->mFd:I

    const/4 v8, -0x1

    if-ne v7, v8, :cond_6

    const-string v1, "SandboxedProcessConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid FD (id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v0, v5, v0

    iget v0, v0, Lorg/chromium/content/browser/FileDescriptorInfo;->mId:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") for process connection, aborting connection."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "com.google.android.apps.chrome.extra.sandbox_extraFile_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "com.google.android.apps.chrome.extra.sandbox_extraFile_"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_fd"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aget-object v9, v5, v0

    iget-boolean v9, v9, Lorg/chromium/content/browser/FileDescriptorInfo;->mAutoClose:Z

    if-eqz v9, :cond_7

    aget-object v9, v5, v0

    iget v9, v9, Lorg/chromium/content/browser/FileDescriptorInfo;->mFd:I

    invoke-static {v9}, Landroid/os/ParcelFileDescriptor;->adoptFd(I)Landroid/os/ParcelFileDescriptor;

    move-result-object v9

    aput-object v9, v6, v0

    :goto_4
    aget-object v9, v6, v0

    invoke-virtual {v4, v8, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    aget-object v8, v5, v0

    iget v8, v8, Lorg/chromium/content/browser/FileDescriptorInfo;->mId:I

    invoke-virtual {v4, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    :try_start_0
    aget-object v9, v5, v0

    iget v9, v9, Lorg/chromium/content/browser/FileDescriptorInfo;->mFd:I

    invoke-static {v9}, Landroid/os/ParcelFileDescriptor;->fromFd(I)Landroid/os/ParcelFileDescriptor;

    move-result-object v9

    aput-object v9, v6, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    const-string v1, "SandboxedProcessConnection"

    const-string v2, "Invalid FD provided for process connection, aborting connection."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    :cond_8
    const-string v0, "com.google.android.apps.chrome.extra.cpu_count"

    invoke-static {}, Lorg/chromium/base/CpuFeatures;->getCount()I

    move-result v5

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "com.google.android.apps.chrome.extra.cpu_features"

    invoke-static {}, Lorg/chromium/base/CpuFeatures;->getMask()J

    move-result-wide v7

    invoke-virtual {v4, v0, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :try_start_1
    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mService:Lorg/chromium/content/common/ISandboxedProcessService;

    iget-object v5, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    iget-object v5, v5, Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;->mCallback:Lorg/chromium/content/common/ISandboxedProcessCallback;

    invoke-interface {v0, v4, v5}, Lorg/chromium/content/common/ISandboxedProcessService;->setupConnection(Landroid/os/Bundle;Lorg/chromium/content/common/ISandboxedProcessCallback;)I

    move-result v0

    iput v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mPID:I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_5
    :try_start_2
    array-length v0, v6

    :goto_6
    if-ge v1, v0, :cond_2

    aget-object v4, v6, v1

    if-eqz v4, :cond_9

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :catch_1
    move-exception v0

    const-string v4, "SandboxedProcessConnection"

    const-string v5, "Failed to setup connection."

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    :catch_2
    move-exception v0

    const-string v1, "SandboxedProcessConnection"

    const-string v4, "Failed to close FD."

    invoke-static {v1, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1
.end method

.method private onBindFailed()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mServiceConnectComplete:Z

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->doConnectionSetup()V

    :cond_0
    return-void
.end method


# virtual methods
.method declared-synchronized bind(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    sget-boolean v0, Lorg/chromium/content/browser/SandboxedProcessConnection;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->createServiceBindIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.apps.chrome.extra.sandbox_native_library_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p2, :cond_1

    const-string v1, "com.google.android.apps.chrome.extra.sandbox_command_line"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    iget-object v1, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mIsBound:Z

    iget-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mIsBound:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->onBindFailed()V

    :cond_2
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method declared-synchronized bindHighPriority()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mService:Lorg/chromium/content/common/ISandboxedProcessService;

    if-nez v0, :cond_0

    const-string v0, "SandboxedProcessConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The connection is not bound for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mPID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnection:Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;

    if-nez v0, :cond_1

    new-instance v0, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;-><init>(Lorg/chromium/content/browser/SandboxedProcessConnection;Lorg/chromium/content/browser/SandboxedProcessConnection$1;)V

    iput-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnection:Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnection:Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;

    invoke-virtual {v0}, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;->bind()V

    :cond_1
    iget v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnectionCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnectionCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPid()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mPID:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getService()Lorg/chromium/content/common/ISandboxedProcessService;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mService:Lorg/chromium/content/common/ISandboxedProcessService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getServiceNumber()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mServiceNumber:I

    return v0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mServiceConnectComplete:Z

    invoke-static {p2}, Lorg/chromium/content/common/ISandboxedProcessService$Stub;->asInterface(Landroid/os/IBinder;)Lorg/chromium/content/common/ISandboxedProcessService;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mService:Lorg/chromium/content/common/ISandboxedProcessService;

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->doConnectionSetup()V

    :cond_0
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 5

    iget v1, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mPID:I

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    iget-object v0, v0, Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;->mOnConnectionCallback:Ljava/lang/Runnable;

    :goto_0
    const-string v2, "SandboxedProcessConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onServiceDisconnected (crash?): pid="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->unbind()V

    if-eqz v1, :cond_0

    iget-object v2, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mDeathCallback:Lorg/chromium/content/browser/SandboxedProcessConnection$DeathCallback;

    invoke-interface {v2, v1}, Lorg/chromium/content/browser/SandboxedProcessConnection$DeathCallback;->onSandboxedProcessDied(I)V

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method declared-synchronized setupConnection([Ljava/lang/String;[Lorg/chromium/content/browser/FileDescriptorInfo;Lorg/chromium/content/common/ISandboxedProcessCallback;Ljava/lang/Runnable;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    sget-boolean v0, Lorg/chromium/content/browser/SandboxedProcessConnection;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    new-instance v0, Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    invoke-direct {v0, p1, p2, p3, p4}, Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;-><init>([Ljava/lang/String;[Lorg/chromium/content/browser/FileDescriptorInfo;Lorg/chromium/content/common/ISandboxedProcessCallback;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    iget-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mServiceConnectComplete:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->doConnectionSetup()V

    :cond_1
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method declared-synchronized unbind()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mIsBound:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mIsBound:Z

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mService:Lorg/chromium/content/common/ISandboxedProcessService;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnection:Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->unbindHighPriority(Z)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mService:Lorg/chromium/content/common/ISandboxedProcessService;

    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mPID:I

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mConnectionParams:Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mServiceConnectComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized unbindHighPriority(Z)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mService:Lorg/chromium/content/common/ISandboxedProcessService;

    if-nez v0, :cond_1

    const-string v0, "SandboxedProcessConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The connection is not bound for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mPID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnectionCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnectionCount:I

    if-nez p1, :cond_2

    iget v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnectionCount:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnection:Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnection:Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;

    invoke-virtual {v0}, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;->unbind()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection;->mHighPriorityConnection:Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
