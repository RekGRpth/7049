.class final Lorg/chromium/content/browser/SandboxedProcessLauncher$3;
.super Lorg/chromium/content/common/ISandboxedProcessCallback$Stub;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/content/common/ISandboxedProcessCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final establishSurfacePeer(IILandroid/view/Surface;II)V
    .locals 0

    invoke-static {p1, p2, p3, p4, p5}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->establishSurfacePeer(IILandroid/view/Surface;II)V

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/view/Surface;->release()V

    :cond_0
    return-void
.end method
