.class public interface abstract Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;
.super Ljava/lang/Object;


# virtual methods
.method public abstract copy()Z
.end method

.method public abstract cut()Z
.end method

.method public abstract getSelectedText()Ljava/lang/String;
.end method

.method public abstract isSelectionEditable()Z
.end method

.method public abstract onDestroyActionMode()V
.end method

.method public abstract paste()Z
.end method

.method public abstract selectAll()Z
.end method
