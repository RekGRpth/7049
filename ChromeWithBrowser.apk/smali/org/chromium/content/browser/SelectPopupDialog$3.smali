.class Lorg/chromium/content/browser/SelectPopupDialog$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic this$0:Lorg/chromium/content/browser/SelectPopupDialog;

.field final synthetic val$listView:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/SelectPopupDialog;Landroid/widget/ListView;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/SelectPopupDialog$3;->this$0:Lorg/chromium/content/browser/SelectPopupDialog;

    iput-object p2, p0, Lorg/chromium/content/browser/SelectPopupDialog$3;->val$listView:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/SelectPopupDialog$3;->this$0:Lorg/chromium/content/browser/SelectPopupDialog;

    # getter for: Lorg/chromium/content/browser/SelectPopupDialog;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lorg/chromium/content/browser/SelectPopupDialog;->access$000(Lorg/chromium/content/browser/SelectPopupDialog;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/content/browser/SelectPopupDialog$3;->this$0:Lorg/chromium/content/browser/SelectPopupDialog;

    iget-object v2, p0, Lorg/chromium/content/browser/SelectPopupDialog$3;->val$listView:Landroid/widget/ListView;

    # invokes: Lorg/chromium/content/browser/SelectPopupDialog;->getSelectedIndices(Landroid/widget/ListView;)[I
    invoke-static {v1, v2}, Lorg/chromium/content/browser/SelectPopupDialog;->access$100(Lorg/chromium/content/browser/SelectPopupDialog;Landroid/widget/ListView;)[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->selectPopupMenuItems([I)V

    iget-object v0, p0, Lorg/chromium/content/browser/SelectPopupDialog$3;->this$0:Lorg/chromium/content/browser/SelectPopupDialog;

    # getter for: Lorg/chromium/content/browser/SelectPopupDialog;->mListBoxPopup:Landroid/app/AlertDialog;
    invoke-static {v0}, Lorg/chromium/content/browser/SelectPopupDialog;->access$200(Lorg/chromium/content/browser/SelectPopupDialog;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method
