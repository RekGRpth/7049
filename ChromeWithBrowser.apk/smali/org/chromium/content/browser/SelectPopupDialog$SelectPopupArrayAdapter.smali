.class Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;
.super Landroid/widget/ArrayAdapter;


# static fields
.field static final POPUP_ITEM_TYPE_DISABLED:I = 0x1

.field static final POPUP_ITEM_TYPE_ENABLED:I = 0x2

.field static final POPUP_ITEM_TYPE_GROUP:I


# instance fields
.field private mAreAllItemsEnabled:Z

.field private mItemEnabled:[I

.field final synthetic this$0:Lorg/chromium/content/browser/SelectPopupDialog;


# direct methods
.method public constructor <init>(Lorg/chromium/content/browser/SelectPopupDialog;[Ljava/lang/String;[IZ)V
    .locals 6

    const/4 v1, 0x0

    iput-object p1, p0, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;->this$0:Lorg/chromium/content/browser/SelectPopupDialog;

    # getter for: Lorg/chromium/content/browser/SelectPopupDialog;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {p1}, Lorg/chromium/content/browser/SelectPopupDialog;->access$000(Lorg/chromium/content/browser/SelectPopupDialog;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p4, :cond_1

    const v0, 0x1090013

    :goto_0
    invoke-direct {p0, v2, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object p3, p0, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;->mItemEnabled:[I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;->mAreAllItemsEnabled:Z

    iget-object v2, p0, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;->mItemEnabled:[I

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    const/4 v5, 0x2

    if-eq v4, v5, :cond_2

    iput-boolean v1, p0, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;->mAreAllItemsEnabled:Z

    :cond_0
    return-void

    :cond_1
    const v0, 0x1090012

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;->mAreAllItemsEnabled:Z

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v2, 0x0

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_2

    :cond_0
    move-object v1, v2

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    invoke-super {p0, p1, v2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;->mItemEnabled:[I

    aget v0, v0, p1

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;->mItemEnabled:[I

    aget v0, v0, p1

    if-nez v0, :cond_3

    move-object v0, v1

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 3

    const/4 v0, 0x0

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;->getCount()I

    move-result v1

    if-lt p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;->mItemEnabled:[I

    aget v1, v1, p1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
