.class abstract Lorg/chromium/content/browser/SelectionHandleController;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/content/browser/CursorController;


# static fields
.field private static final TEXT_DIRECTION_DEFAULT:I = 0x0

.field private static final TEXT_DIRECTION_LTR:I = 0x1

.field private static final TEXT_DIRECTION_RTL:I = 0x2


# instance fields
.field private mAllowAutomaticShowing:Z

.field private mEndHandle:Lorg/chromium/content/browser/HandleView;

.field private mFixedHandleX:I

.field private mFixedHandleY:I

.field private mIsShowing:Z

.field private mParent:Landroid/view/View;

.field private mStartHandle:Lorg/chromium/content/browser/HandleView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mAllowAutomaticShowing:Z

    iput-object p1, p0, Lorg/chromium/content/browser/SelectionHandleController;->mParent:Landroid/view/View;

    return-void
.end method

.method private createHandlesIfNeeded(II)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x2

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    if-nez v0, :cond_0

    new-instance v3, Lorg/chromium/content/browser/HandleView;

    if-ne p1, v1, :cond_2

    move v0, v1

    :goto_0
    iget-object v4, p0, Lorg/chromium/content/browser/SelectionHandleController;->mParent:Landroid/view/View;

    invoke-direct {v3, p0, v0, v4}, Lorg/chromium/content/browser/HandleView;-><init>(Lorg/chromium/content/browser/CursorController;ILandroid/view/View;)V

    iput-object v3, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mEndHandle:Lorg/chromium/content/browser/HandleView;

    if-nez v0, :cond_1

    new-instance v0, Lorg/chromium/content/browser/HandleView;

    if-ne p2, v1, :cond_3

    :goto_1
    iget-object v1, p0, Lorg/chromium/content/browser/SelectionHandleController;->mParent:Landroid/view/View;

    invoke-direct {v0, p0, v2, v1}, Lorg/chromium/content/browser/HandleView;-><init>(Lorg/chromium/content/browser/CursorController;ILandroid/view/View;)V

    iput-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mEndHandle:Lorg/chromium/content/browser/HandleView;

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method private showHandlesIfNeeded()V
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mIsShowing:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mIsShowing:Z

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->show()V

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mEndHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->show()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/SelectionHandleController;->setHandleVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method allowAutomaticShowing()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mAllowAutomaticShowing:Z

    return-void
.end method

.method public beforeStartUpdatingPosition(Lorg/chromium/content/browser/HandleView;)V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mEndHandle:Lorg/chromium/content/browser/HandleView;

    :goto_0
    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->getAdjustedPositionX()I

    move-result v1

    iput v1, p0, Lorg/chromium/content/browser/SelectionHandleController;->mFixedHandleX:I

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->getLineAdjustedPositionY()I

    move-result v0

    iput v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mFixedHandleY:I

    return-void

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    goto :goto_0
.end method

.method beginHandleFadeIn()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->beginFadeIn()V

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mEndHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->beginFadeIn()V

    return-void
.end method

.method cancelFadeOutAnimation()V
    .locals 0

    invoke-virtual {p0}, Lorg/chromium/content/browser/SelectionHandleController;->hide()V

    return-void
.end method

.method public hide()V
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mIsShowing:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->hide()V

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mEndHandle:Lorg/chromium/content/browser/HandleView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mEndHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->hide()V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mIsShowing:Z

    :cond_2
    return-void
.end method

.method hideAndDisallowAutomaticShowing()V
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/SelectionHandleController;->hide()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mAllowAutomaticShowing:Z

    return-void
.end method

.method isDragging()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->isDragging()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mEndHandle:Lorg/chromium/content/browser/HandleView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mEndHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->isDragging()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isSelectionStartDragged()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/HandleView;->isDragging()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mIsShowing:Z

    return v0
.end method

.method public onDetached()V
    .locals 0

    return-void
.end method

.method onSelectionChanged(II)V
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mAllowAutomaticShowing:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lorg/chromium/content/browser/SelectionHandleController;->showHandles(II)V

    :cond_0
    return-void
.end method

.method public onTouchModeChanged(Z)V
    .locals 0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/SelectionHandleController;->hide()V

    :cond_0
    return-void
.end method

.method protected abstract selectBetweenCoordinates(IIII)V
.end method

.method setEndHandlePosition(II)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mEndHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/content/browser/HandleView;->positionAt(II)V

    return-void
.end method

.method setEndHandlePosition(Landroid/graphics/PointF;)V
    .locals 2

    iget v0, p1, Landroid/graphics/PointF;->x:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lorg/chromium/content/browser/SelectionHandleController;->setEndHandlePosition(II)V

    return-void
.end method

.method setHandleVisibility(I)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/HandleView;->setVisibility(I)V

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mEndHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/HandleView;->setVisibility(I)V

    return-void
.end method

.method setStartHandlePosition(II)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mStartHandle:Lorg/chromium/content/browser/HandleView;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/content/browser/HandleView;->positionAt(II)V

    return-void
.end method

.method setStartHandlePosition(Landroid/graphics/PointF;)V
    .locals 2

    iget v0, p1, Landroid/graphics/PointF;->x:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lorg/chromium/content/browser/SelectionHandleController;->setStartHandlePosition(II)V

    return-void
.end method

.method showHandles(II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/chromium/content/browser/SelectionHandleController;->createHandlesIfNeeded(II)V

    invoke-direct {p0}, Lorg/chromium/content/browser/SelectionHandleController;->showHandlesIfNeeded()V

    return-void
.end method

.method public updatePosition(Lorg/chromium/content/browser/HandleView;II)V
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/SelectionHandleController;->mFixedHandleX:I

    iget v1, p0, Lorg/chromium/content/browser/SelectionHandleController;->mFixedHandleY:I

    invoke-virtual {p0, v0, v1, p2, p3}, Lorg/chromium/content/browser/SelectionHandleController;->selectBetweenCoordinates(IIII)V

    return-void
.end method
