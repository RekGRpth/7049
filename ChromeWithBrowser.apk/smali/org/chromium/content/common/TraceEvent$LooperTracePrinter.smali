.class Lorg/chromium/content/common/TraceEvent$LooperTracePrinter;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/util/Printer;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NAME:Ljava/lang/String; = "Looper.dispatchMessage"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/content/common/TraceEvent;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/common/TraceEvent$LooperTracePrinter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/content/common/TraceEvent$1;)V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/content/common/TraceEvent$LooperTracePrinter;-><init>()V

    return-void
.end method


# virtual methods
.method public println(Ljava/lang/String;)V
    .locals 1

    const-string v0, ">>>>>"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Looper.dispatchMessage"

    invoke-static {v0, p1}, Lorg/chromium/content/common/TraceEvent;->begin(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lorg/chromium/content/common/TraceEvent$LooperTracePrinter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    const-string v0, "<<<<<"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    const-string v0, "Looper.dispatchMessage"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    goto :goto_0
.end method
