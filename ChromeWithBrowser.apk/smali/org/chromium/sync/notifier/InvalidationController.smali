.class public Lorg/chromium/sync/notifier/InvalidationController;
.super Ljava/lang/Object;


# static fields
.field private static final IMPLEMENTING_CLASS_MANIFEST_PROPERTY:Ljava/lang/String; = "org.chromium.sync.notifier.IMPLEMENTING_CLASS_NAME"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/sync/notifier/InvalidationController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/chromium/sync/notifier/InvalidationController;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lorg/chromium/sync/notifier/InvalidationController;->mContext:Landroid/content/Context;

    return-void
.end method

.method static getDestinationClassName(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_1

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v2, "org.chromium.sync.notifier.IMPLEMENTING_CLASS_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    sget-object v2, Lorg/chromium/sync/notifier/InvalidationController;->TAG:Ljava/lang/String;

    const-string v3, "No value for org.chromium.sync.notifier.IMPLEMENTING_CLASS_NAME in manifest; sync notifications will not work"

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    sget-object v2, Lorg/chromium/sync/notifier/InvalidationController;->TAG:Ljava/lang/String;

    const-string v3, "Cannot read own application info"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;
    .locals 1

    new-instance v0, Lorg/chromium/sync/notifier/InvalidationController;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationController;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private setDestinationClassName(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    iget-object v0, p0, Lorg/chromium/sync/notifier/InvalidationController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/InvalidationController;->getDestinationClassName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/chromium/sync/notifier/InvalidationController;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object p1
.end method


# virtual methods
.method public getContractAuthority()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/chromium/sync/notifier/InvalidationController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getModelTypeResolver()Lorg/chromium/sync/notifier/ModelTypeResolver;
    .locals 1

    new-instance v0, Lorg/chromium/sync/notifier/ModelTypeResolverImpl;

    invoke-direct {v0}, Lorg/chromium/sync/notifier/ModelTypeResolverImpl;-><init>()V

    return-object v0
.end method

.method public refreshRegisteredTypes()V
    .locals 4

    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    iget-object v1, p0, Lorg/chromium/sync/notifier/InvalidationController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedSyncedTypes()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedSyncedAccount()Landroid/accounts/Account;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v0, "ALL_TYPES"

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :goto_1
    invoke-virtual {p0, v3, v1, v0}, Lorg/chromium/sync/notifier/InvalidationController;->setRegisteredTypes(Landroid/accounts/Account;ZLjava/util/Set;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->syncTypesToModelTypes(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    goto :goto_1
.end method

.method public setRegisteredTypes(Landroid/accounts/Account;ZLjava/util/Set;)V
    .locals 2

    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationController;->getModelTypeResolver()Lorg/chromium/sync/notifier/ModelTypeResolver;

    move-result-object v0

    invoke-interface {v0, p3}, Lorg/chromium/sync/notifier/ModelTypeResolver;->resolveModelTypes(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lorg/chromium/sync/notifier/InvalidationController$IntentProtocol;->createRegisterIntent(Landroid/accounts/Account;ZLjava/util/Set;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/chromium/sync/notifier/InvalidationController;->setDestinationClassName(Landroid/content/Intent;)Landroid/content/Intent;

    iget-object v1, p0, Lorg/chromium/sync/notifier/InvalidationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public start()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-direct {p0, v0}, Lorg/chromium/sync/notifier/InvalidationController;->setDestinationClassName(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/sync/notifier/InvalidationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public stop()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-direct {p0, v0}, Lorg/chromium/sync/notifier/InvalidationController;->setDestinationClassName(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "stop"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lorg/chromium/sync/notifier/InvalidationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
