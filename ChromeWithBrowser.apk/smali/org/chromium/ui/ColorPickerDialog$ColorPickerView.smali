.class Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;
.super Landroid/view/View;


# static fields
.field private static final BOUNDING_BOX_EDGE:I = 0x64

.field private static final CENTER_RADIUS:I = 0x20

.field private static final DIALOG_HEIGHT:I = 0xc8

.field private static final PI:F = 3.1415925f


# instance fields
.field private center_x:I

.field private center_y:I

.field private final mCenterPaint:Landroid/graphics/Paint;

.field private final mColors:[I

.field private mHighlightCenter:Z

.field private final mListener:Lorg/chromium/ui/ColorPickerDialog$OnColorChangedListener;

.field private final mPaint:Landroid/graphics/Paint;

.field private mTrackingCenter:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lorg/chromium/ui/ColorPickerDialog$OnColorChangedListener;I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v0, -0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->center_x:I

    iput v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->center_y:I

    iput-object p2, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mListener:Lorg/chromium/ui/ColorPickerDialog$OnColorChangedListener;

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mColors:[I

    new-instance v0, Landroid/graphics/SweepGradient;

    iget-object v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mColors:[I

    const/4 v2, 0x0

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/SweepGradient;-><init>(FF[I[F)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x42000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void

    :array_0
    .array-data 4
        -0x10000
        -0xff01
        -0xffff01
        -0xff0001
        -0xff0100
        -0x100
        -0x10000
    .end array-data
.end method

.method private static interpolate(IIF)I
    .locals 1

    sub-int v0, p1, p0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, p0

    return v0
.end method

.method static interpolateColor([IFF)I
    .locals 7

    const/high16 v5, 0x3f800000

    const/4 v4, 0x0

    float-to-double v0, p2

    float-to-double v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    double-to-float v0, v0

    const v1, 0x40c90fda

    div-float/2addr v0, v1

    cmpg-float v1, v0, v4

    if-gez v1, :cond_0

    add-float/2addr v0, v5

    :cond_0
    cmpg-float v1, v0, v4

    if-gtz v1, :cond_1

    const/4 v0, 0x0

    aget v0, p0, v0

    :goto_0
    return v0

    :cond_1
    cmpl-float v1, v0, v5

    if-ltz v1, :cond_2

    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    aget v0, p0, v0

    goto :goto_0

    :cond_2
    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v1, v0

    int-to-float v2, v1

    sub-float/2addr v0, v2

    aget v2, p0, v1

    add-int/lit8 v1, v1, 0x1

    aget v1, p0, v1

    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-static {v3, v4, v0}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->interpolate(IIF)I

    move-result v3

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v4

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v5

    invoke-static {v4, v5, v0}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->interpolate(IIF)I

    move-result v4

    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    move-result v5

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v6

    invoke-static {v5, v6, v0}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->interpolate(IIF)I

    move-result v5

    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    invoke-static {v2, v1, v0}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->interpolate(IIF)I

    move-result v0

    invoke-static {v3, v4, v5, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    const/4 v1, -0x1

    const/high16 v5, 0x42000000

    const/4 v4, 0x0

    iget v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->center_x:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->center_x:I

    :cond_0
    iget v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->center_y:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->center_y:I

    :cond_1
    const/high16 v0, 0x42c80000

    iget-object v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    const/high16 v2, 0x3f000000

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->center_x:I

    int-to-float v1, v1

    iget v2, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->center_y:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    new-instance v1, Landroid/graphics/RectF;

    neg-float v2, v0

    neg-float v3, v0

    invoke-direct {v1, v2, v3, v0, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v4, v5, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-boolean v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mTrackingCenter:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    iget-object v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-boolean v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mHighlightCenter:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    :goto_0
    iget-object v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    add-float/2addr v1, v5

    iget-object v2, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v4, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    :cond_2
    return-void

    :cond_3
    iget-object v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 1

    const/16 v0, 0xc8

    invoke-virtual {p0, p1, v0}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->setMeasuredDimension(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v3, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->center_x:I

    int-to-float v3, v3

    sub-float v3, v0, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v4, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->center_y:I

    int-to-float v4, v4

    sub-float v4, v0, v4

    mul-float v0, v3, v3

    mul-float v5, v4, v4

    add-float/2addr v0, v5

    const/high16 v5, 0x44800000

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :pswitch_0
    iput-boolean v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mTrackingCenter:Z

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mHighlightCenter:Z

    invoke-virtual {p0}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->invalidate()V

    goto :goto_1

    :cond_2
    :pswitch_1
    iget-boolean v2, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mTrackingCenter:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mHighlightCenter:Z

    if-eq v2, v0, :cond_0

    iput-boolean v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mHighlightCenter:Z

    invoke-virtual {p0}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->invalidate()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mColors:[I

    invoke-static {v2, v3, v4}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->interpolateColor([IFF)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->invalidate()V

    goto :goto_1

    :pswitch_2
    iget-boolean v3, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mTrackingCenter:Z

    if-eqz v3, :cond_0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mListener:Lorg/chromium/ui/ColorPickerDialog$OnColorChangedListener;

    iget-object v3, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    invoke-interface {v0, v3}, Lorg/chromium/ui/ColorPickerDialog$OnColorChangedListener;->colorChanged(I)V

    :cond_4
    iput-boolean v2, p0, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->mTrackingCenter:Z

    invoke-virtual {p0}, Lorg/chromium/ui/ColorPickerDialog$ColorPickerView;->invalidate()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
