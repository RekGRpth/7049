.class Lorg/chromium/ui/SelectFileDialog;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/ui/gfx/NativeWindow$IntentCallback;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "ui"
.end annotation


# static fields
.field private static final ALL_AUDIO_TYPES:Ljava/lang/String; = "audio/*"

.field private static final ALL_IMAGE_TYPES:Ljava/lang/String; = "image/*"

.field private static final ALL_VIDEO_TYPES:Ljava/lang/String; = "video/*"

.field private static final ANY_TYPES:Ljava/lang/String; = "*/*"

.field private static final AUDIO_TYPE:Ljava/lang/String; = "audio/"

.field private static final CAPTURE_CAMCORDER:Ljava/lang/String; = "camcorder"

.field private static final CAPTURE_CAMERA:Ljava/lang/String; = "camera"

.field private static final CAPTURE_FILESYSTEM:Ljava/lang/String; = "filesystem"

.field private static final CAPTURE_IMAGE_DIRECTORY:Ljava/lang/String; = "browser-photos"

.field private static final CAPTURE_MICROPHONE:Ljava/lang/String; = "microphone"

.field private static final IMAGE_TYPE:Ljava/lang/String; = "image/"

.field private static final VIDEO_TYPE:Ljava/lang/String; = "video/"


# instance fields
.field private mCameraOutputUri:Landroid/net/Uri;

.field private mCapture:Ljava/lang/String;

.field private mFileTypes:Ljava/util/List;

.field private final mNativeSelectFileDialog:I


# direct methods
.method private constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lorg/chromium/ui/SelectFileDialog;->mNativeSelectFileDialog:I

    return-void
.end method

.method private acceptSpecificType(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lorg/chromium/ui/SelectFileDialog;->mFileTypes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private captureCamcorder()Z
    .locals 2

    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->shouldShowVideoTypes()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/ui/SelectFileDialog;->mCapture:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/ui/SelectFileDialog;->mCapture:Ljava/lang/String;

    const-string v1, "camcorder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private captureCamera()Z
    .locals 2

    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->shouldShowImageTypes()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/ui/SelectFileDialog;->mCapture:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/ui/SelectFileDialog;->mCapture:Ljava/lang/String;

    const-string v1, "camera"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private captureFilesystem()Z
    .locals 2

    iget-object v0, p0, Lorg/chromium/ui/SelectFileDialog;->mCapture:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/ui/SelectFileDialog;->mCapture:Ljava/lang/String;

    const-string v1, "filesystem"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private captureMicrophone()Z
    .locals 2

    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->shouldShowAudioTypes()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/ui/SelectFileDialog;->mCapture:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/ui/SelectFileDialog;->mCapture:Ljava/lang/String;

    const-string v1, "microphone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static create(I)Lorg/chromium/ui/SelectFileDialog;
    .locals 1

    new-instance v0, Lorg/chromium/ui/SelectFileDialog;

    invoke-direct {v0, p0}, Lorg/chromium/ui/SelectFileDialog;-><init>(I)V

    return-object v0
.end method

.method private getFileForImageCapture()Ljava/io/File;
    .locals 4

    sget-object v0, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "browser-photos"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private native nativeOnFileNotSelected(I)V
.end method

.method private native nativeOnFileSelected(ILjava/lang/String;)V
.end method

.method private noSpecificType()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lorg/chromium/ui/SelectFileDialog;->mFileTypes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lorg/chromium/ui/SelectFileDialog;->mFileTypes:Ljava/util/List;

    const-string v2, "*/*"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onFileNotSelected()V
    .locals 1

    iget v0, p0, Lorg/chromium/ui/SelectFileDialog;->mNativeSelectFileDialog:I

    invoke-direct {p0, v0}, Lorg/chromium/ui/SelectFileDialog;->nativeOnFileNotSelected(I)V

    return-void
.end method

.method private selectFile([Ljava/lang/String;Ljava/lang/String;Lorg/chromium/ui/gfx/NativeWindow;)V
    .locals 8

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lorg/chromium/ui/SelectFileDialog;->mFileTypes:Ljava/util/List;

    iput-object p2, p0, Lorg/chromium/ui/SelectFileDialog;->mCapture:Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.CHOOSER"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->getFileForImageCapture()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lorg/chromium/ui/SelectFileDialog;->mCameraOutputUri:Landroid/net/Uri;

    const-string v2, "output"

    iget-object v3, p0, Lorg/chromium/ui/SelectFileDialog;->mCameraOutputUri:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.media.action.VIDEO_CAPTURE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.provider.MediaStore.RECORD_SOUND"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lorg/chromium/ui/gfx/NativeWindow;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lorg/chromium/ui/R$string;->low_memory_error:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->captureCamera()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p3, v0, p0, v4}, Lorg/chromium/ui/gfx/NativeWindow;->showIntent(Landroid/content/Intent;Lorg/chromium/ui/gfx/NativeWindow$IntentCallback;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->captureCamcorder()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {p3, v2, p0, v4}, Lorg/chromium/ui/gfx/NativeWindow;->showIntent(Landroid/content/Intent;Lorg/chromium/ui/gfx/NativeWindow$IntentCallback;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_2
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.GET_CONTENT"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "android.intent.category.OPENABLE"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->noSpecificType()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->shouldShowImageTypes()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v7, "image/*"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "*/*"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    const-string v2, "android.intent.extra.INITIAL_INTENTS"

    const/4 v0, 0x0

    new-array v0, v0, [Landroid/content/Intent;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "android.intent.extra.INTENT"

    invoke-virtual {v1, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p3, v1, p0, v4}, Lorg/chromium/ui/gfx/NativeWindow;->showIntent(Landroid/content/Intent;Lorg/chromium/ui/gfx/NativeWindow$IntentCallback;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->onFileNotSelected()V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->captureMicrophone()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p3, v3, p0, v4}, Lorg/chromium/ui/gfx/NativeWindow;->showIntent(Landroid/content/Intent;Lorg/chromium/ui/gfx/NativeWindow$IntentCallback;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_0

    :cond_6
    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->shouldShowVideoTypes()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v7, "video/*"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->shouldShowAudioTypes()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v7, "audio/*"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method private shouldShowAudioTypes()Z
    .locals 2

    const-string v0, "audio/*"

    const-string v1, "audio/"

    invoke-direct {p0, v0, v1}, Lorg/chromium/ui/SelectFileDialog;->shouldShowTypes(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private shouldShowImageTypes()Z
    .locals 2

    const-string v0, "image/*"

    const-string v1, "image/"

    invoke-direct {p0, v0, v1}, Lorg/chromium/ui/SelectFileDialog;->shouldShowTypes(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private shouldShowTypes(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->noSpecificType()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/ui/SelectFileDialog;->mFileTypes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p2}, Lorg/chromium/ui/SelectFileDialog;->acceptSpecificType(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private shouldShowVideoTypes()Z
    .locals 2

    const-string v0, "video/*"

    const-string v1, "video/"

    invoke-direct {p0, v0, v1}, Lorg/chromium/ui/SelectFileDialog;->shouldShowTypes(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onIntentCompleted(Lorg/chromium/ui/gfx/NativeWindow;ILandroid/content/ContentResolver;Landroid/content/Intent;)V
    .locals 8

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->onFileNotSelected()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p4, :cond_2

    iget v0, p0, Lorg/chromium/ui/SelectFileDialog;->mNativeSelectFileDialog:I

    iget-object v1, p0, Lorg/chromium/ui/SelectFileDialog;->mCameraOutputUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/chromium/ui/SelectFileDialog;->nativeOnFileSelected(ILjava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    iget-object v2, p0, Lorg/chromium/ui/SelectFileDialog;->mCameraOutputUri:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p1, v0}, Lorg/chromium/ui/gfx/NativeWindow;->sendBroadcast(Landroid/content/Intent;)V

    :goto_1
    if-nez v6, :cond_0

    invoke-direct {p0}, Lorg/chromium/ui/SelectFileDialog;->onFileNotSelected()V

    invoke-virtual {p1}, Lorg/chromium/ui/gfx/NativeWindow;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lorg/chromium/ui/R$string;->opening_file_error:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/chromium/ui/gfx/NativeWindow;->showError(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v7

    move-object v0, p3

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v6, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget v2, p0, Lorg/chromium/ui/SelectFileDialog;->mNativeSelectFileDialog:I

    invoke-direct {p0, v2, v0}, Lorg/chromium/ui/SelectFileDialog;->nativeOnFileSelected(ILjava/lang/String;)V

    move v0, v6

    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v6, v0

    goto :goto_1

    :cond_3
    move v0, v7

    goto :goto_2

    :cond_4
    move v6, v7

    goto :goto_1
.end method
