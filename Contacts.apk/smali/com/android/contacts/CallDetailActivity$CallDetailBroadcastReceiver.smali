.class Lcom/android/contacts/CallDetailActivity$CallDetailBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CallDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/CallDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CallDetailBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/CallDetailActivity;


# direct methods
.method private constructor <init>(Lcom/android/contacts/CallDetailActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/CallDetailActivity$CallDetailBroadcastReceiver;->this$0:Lcom/android/contacts/CallDetailActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/contacts/CallDetailActivity;Lcom/android/contacts/CallDetailActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/contacts/CallDetailActivity;
    .param p2    # Lcom/android/contacts/CallDetailActivity$1;

    invoke-direct {p0, p1}, Lcom/android/contacts/CallDetailActivity$CallDetailBroadcastReceiver;-><init>(Lcom/android/contacts/CallDetailActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/CallDetailActivity$CallDetailBroadcastReceiver;->this$0:Lcom/android/contacts/CallDetailActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallDetailBroadcastReceiver, onReceive action = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/contacts/CallDetailActivity;->access$2400(Lcom/android/contacts/CallDetailActivity;Ljava/lang/String;)V

    const-string v1, "android.intent.action.VOICE_CALL_DEFAULT_SIM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/contacts/CallDetailActivity$CallDetailBroadcastReceiver;->this$0:Lcom/android/contacts/CallDetailActivity;

    invoke-static {v1}, Lcom/android/contacts/CallDetailActivity;->access$2500(Lcom/android/contacts/CallDetailActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/contacts/CallDetailActivity$CallDetailBroadcastReceiver;->this$0:Lcom/android/contacts/CallDetailActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/contacts/CallDetailActivity;->setSimIndicatorVisibility(Z)V

    :cond_0
    return-void
.end method
