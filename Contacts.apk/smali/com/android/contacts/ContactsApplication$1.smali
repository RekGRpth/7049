.class Lcom/android/contacts/ContactsApplication$1;
.super Ljava/lang/Object;
.source "ContactsApplication.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/contacts/ContactsApplication;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/ContactsApplication;


# direct methods
.method constructor <init>(Lcom/android/contacts/ContactsApplication;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/ContactsApplication$1;->this$0:Lcom/android/contacts/ContactsApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Lcom/mediatek/contacts/HyphonManager;->getInstance()Lcom/mediatek/contacts/HyphonManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/contacts/ContactsApplication$1;->this$0:Lcom/android/contacts/ContactsApplication;

    iget-object v3, v3, Lcom/android/contacts/ContactsApplication;->TEST_NUMBER:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/mediatek/contacts/HyphonManager;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "ContactsPerf"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Thread HyphonManager formatNumber() use time :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
