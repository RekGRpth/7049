.class public final Lcom/android/contacts/R$bool;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "bool"
.end annotation


# static fields
.field public static final config_allow_export_to_sdcard:I = 0x7f0b0003

.field public static final config_allow_import_from_sdcard:I = 0x7f0b0000

.field public static final config_allow_share_visible_contacts:I = 0x7f0b0004

.field public static final config_allow_sim_import:I = 0x7f0b0012

.field public static final config_allow_users_select_all_vcard_import:I = 0x7f0b0002

.field public static final config_browse_list_show_images:I = 0x7f0b0006

.field public static final config_default_display_order_primary:I = 0x7f0b000b

.field public static final config_default_sort_order_primary:I = 0x7f0b0009

.field public static final config_display_order_user_changeable:I = 0x7f0b000a

.field public static final config_editor_field_order_primary:I = 0x7f0b000c

.field public static final config_editor_include_phonetic_name:I = 0x7f0b000d

.field public static final config_enable_dialer_key_vibration:I = 0x7f0b0005

.field public static final config_import_all_vcard_from_sdcard_automatically:I = 0x7f0b0001

.field public static final config_show_group_action_in_action_bar:I = 0x7f0b0011

.field public static final config_show_onscreen_dial_button:I = 0x7f0b0007

.field public static final config_sort_order_user_changeable:I = 0x7f0b0008

.field public static final config_use_two_panes:I = 0x7f0b000e

.field public static final config_use_two_panes_in_favorites:I = 0x7f0b000f

.field public static final show_home_icon:I = 0x7f0b0010


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
