.class public final Lcom/android/contacts/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final RCS:I = 0x7f07006e

.field public static final RCSIcon:I = 0x7f0700e9

.field public static final RCS_action:I = 0x7f07006f

.field public static final RCS_container:I = 0x7f07006d

.field public static final RCS_divider:I = 0x7f070071

.field public static final RCS_icon:I = 0x7f070072

.field public static final RCS_text:I = 0x7f070070

.field public static final RelativeLayout01:I = 0x7f07013d

.field public static final about_fragment_container:I = 0x7f0700a9

.field public static final account:I = 0x7f070121

.field public static final accountType:I = 0x7f0700e2

.field public static final accountUserName:I = 0x7f0700e3

.field public static final account_container:I = 0x7f070120

.field public static final account_filter_header:I = 0x7f070030

.field public static final account_filter_header_container:I = 0x7f07002f

.field public static final account_header:I = 0x7f070127

.field public static final account_icon:I = 0x7f070124

.field public static final account_list:I = 0x7f0700d2

.field public static final account_name:I = 0x7f070123

.field public static final account_type:I = 0x7f070122

.field public static final actions_view_container:I = 0x7f0700b8

.field public static final addToContact:I = 0x7f07002e

.field public static final add_account_button:I = 0x7f0700d3

.field public static final add_accounts:I = 0x7f070130

.field public static final add_connection_icon:I = 0x7f0700ad

.field public static final add_connection_label:I = 0x7f0700ae

.field public static final add_contact:I = 0x7f0701c9

.field public static final add_field_footer:I = 0x7f070146

.field public static final add_member_field:I = 0x7f070138

.field public static final add_organization_button:I = 0x7f070155

.field public static final add_text:I = 0x7f07011a

.field public static final addicon:I = 0x7f0700fb

.field public static final additionalData:I = 0x7f07019b

.field public static final aggregation_suggestion_data:I = 0x7f070035

.field public static final aggregation_suggestion_name:I = 0x7f070034

.field public static final aggregation_suggestion_photo:I = 0x7f070036

.field public static final aggregation_suggestions:I = 0x7f070033

.field public static final all_fragment:I = 0x7f07015c

.field public static final all_tab_text:I = 0x7f07007b

.field public static final alpha_overlay:I = 0x7f07009f

.field public static final anchor_view:I = 0x7f07011e

.field public static final association_buttons:I = 0x7f07003c

.field public static final association_layout:I = 0x7f070174

.field public static final association_list:I = 0x7f070039

.field public static final association_sim_icon:I = 0x7f0700bd

.field public static final association_sim_text:I = 0x7f0700be

.field public static final association_view:I = 0x7f070037

.field public static final backdrop:I = 0x7f070167

.field public static final badge:I = 0x7f070129

.field public static final blank:I = 0x7f070052

.field public static final blue_separator:I = 0x7f070046

.field public static final body:I = 0x7f070179

.field public static final border:I = 0x7f070190

.field public static final browse_view:I = 0x7f07015b

.field public static final btn_action:I = 0x7f070141

.field public static final btn_back:I = 0x7f070140

.field public static final btn_cancel:I = 0x7f0700a8

.field public static final btn_discard:I = 0x7f07003a

.field public static final btn_divider1:I = 0x7f0700da

.field public static final btn_done:I = 0x7f07003b

.field public static final btn_ok:I = 0x7f0700d9

.field public static final btn_type_filter_all:I = 0x7f070079

.field public static final btn_type_filter_incoming:I = 0x7f07007c

.field public static final btn_type_filter_incoming_icon:I = 0x7f07007e

.field public static final btn_type_filter_missed:I = 0x7f070082

.field public static final btn_type_filter_missed_icon:I = 0x7f070083

.field public static final btn_type_filter_outgoing:I = 0x7f07007f

.field public static final btn_type_filter_outgoing_icon:I = 0x7f070081

.field public static final button_add_field:I = 0x7f07017e

.field public static final button_edit_externally:I = 0x7f070180

.field public static final buttonbar_layout:I = 0x7f0700d8

.field public static final buttons_linear_layout:I = 0x7f070168

.field public static final call:I = 0x7f070108

.field public static final callType:I = 0x7f070106

.field public static final call_and_sms:I = 0x7f07004c

.field public static final call_and_sms_divider:I = 0x7f070050

.field public static final call_and_sms_icon:I = 0x7f070051

.field public static final call_and_sms_label:I = 0x7f07004f

.field public static final call_and_sms_main_action:I = 0x7f07004d

.field public static final call_and_sms_text:I = 0x7f07004e

.field public static final call_and_sms_text_action:I = 0x7f070060

.field public static final call_count:I = 0x7f070092

.field public static final call_count_and_date:I = 0x7f07008f

.field public static final call_date:I = 0x7f070093

.field public static final call_detail:I = 0x7f070040

.field public static final call_log_auto_rejected_label:I = 0x7f070077

.field public static final call_log_date:I = 0x7f070074

.field public static final call_log_divider:I = 0x7f070075

.field public static final call_log_fragment:I = 0x7f070073

.field public static final call_log_header:I = 0x7f070091

.field public static final call_log_unavailable_container:I = 0x7f070115

.field public static final call_log_unavailable_view:I = 0x7f070114

.field public static final call_number_geocode:I = 0x7f070063

.field public static final call_type:I = 0x7f07008d

.field public static final call_type_icon:I = 0x7f070055

.field public static final call_type_icons:I = 0x7f07008e

.field public static final call_type_text:I = 0x7f070056

.field public static final calllog_auto_rejected_cluster:I = 0x7f070076

.field public static final calllog_search_button_cluster:I = 0x7f070078

.field public static final calllog_search_result:I = 0x7f070084

.field public static final calllog_search_results_for:I = 0x7f070085

.field public static final calllog_search_results_found:I = 0x7f070086

.field public static final card1_photo:I = 0x7f07014a

.field public static final card2_photo:I = 0x7f07014b

.field public static final choose_resources:I = 0x7f0701c6

.field public static final company:I = 0x7f070103

.field public static final contact_background:I = 0x7f070045

.field public static final contact_background_sizer:I = 0x7f070044

.field public static final contact_detail:I = 0x7f0700b6

.field public static final contact_detail_about_fragment:I = 0x7f0700b4

.field public static final contact_detail_container:I = 0x7f0700ac

.field public static final contact_detail_list_padding:I = 0x7f0700c7

.field public static final contact_detail_loader_fragment:I = 0x7f07015f

.field public static final contact_detail_updates_fragment:I = 0x7f0700d0

.field public static final contact_detail_view:I = 0x7f0700aa

.field public static final contact_details_view:I = 0x7f07015e

.field public static final contact_editor_fragment:I = 0x7f0700d6

.field public static final contact_list:I = 0x7f070151

.field public static final contact_list_empty:I = 0x7f070152

.field public static final contact_list_filter_loader:I = 0x7f070019

.field public static final contact_photo:I = 0x7f070038

.field public static final contact_quick_fix:I = 0x7f0700b5

.field public static final contact_tile_frequent_phone:I = 0x7f0700ed

.field public static final contact_tile_horizontal_divider:I = 0x7f0700ec

.field public static final contact_tile_image:I = 0x7f0700f3

.field public static final contact_tile_layout:I = 0x7f0700f6

.field public static final contact_tile_list:I = 0x7f0700f0

.field public static final contact_tile_list_empty:I = 0x7f0700f1

.field public static final contact_tile_name:I = 0x7f0700ea

.field public static final contact_tile_phone_number:I = 0x7f0700ee

.field public static final contact_tile_phone_type:I = 0x7f0700ef

.field public static final contact_tile_push_state:I = 0x7f0700f4

.field public static final contact_tile_quick:I = 0x7f0700e8

.field public static final contact_tile_secondary_button:I = 0x7f0700f5

.field public static final contact_tile_status:I = 0x7f0700eb

.field public static final contacts:I = 0x7f070193

.field public static final contacts_count:I = 0x7f0701ac

.field public static final contacts_unavailable_container:I = 0x7f070159

.field public static final contacts_unavailable_view:I = 0x7f070158

.field public static final container:I = 0x7f070156

.field public static final controls:I = 0x7f070042

.field public static final copy_phone_number:I = 0x7f0701b0

.field public static final count:I = 0x7f070119

.field public static final create_contact_button:I = 0x7f0700f7

.field public static final create_new_contact:I = 0x7f0701bb

.field public static final custom_dialog_content:I = 0x7f07001b

.field public static final data:I = 0x7f070002

.field public static final date:I = 0x7f070057

.field public static final datePicker:I = 0x7f070102

.field public static final date_view:I = 0x7f07011b

.field public static final day:I = 0x7f070100

.field public static final deleteButton:I = 0x7f07010c

.field public static final delete_all:I = 0x7f0701b9

.field public static final delete_button:I = 0x7f07011c

.field public static final delete_button_container:I = 0x7f070126

.field public static final delete_select_box:I = 0x7f070096

.field public static final dialButton:I = 0x7f07002b

.field public static final dialer_search_item_view:I = 0x7f070104

.field public static final dialog_cache_vcard:I = 0x7f07000b

.field public static final dialog_cancel_confirmation:I = 0x7f07000e

.field public static final dialog_cancel_failed:I = 0x7f07000f

.field public static final dialog_delete_contact_confirmation:I = 0x7f070010

.field public static final dialog_delete_contact_loader_id:I = 0x7f070011

.field public static final dialog_error_with_message:I = 0x7f07000d

.field public static final dialog_event_date_picker:I = 0x7f07001a

.field public static final dialog_export_confirmation:I = 0x7f070012

.field public static final dialog_exporting_vcard:I = 0x7f070013

.field public static final dialog_fail_to_export_with_reason:I = 0x7f070014

.field public static final dialog_io_exception:I = 0x7f07000c

.field public static final dialog_manager_id_1:I = 0x7f070017

.field public static final dialog_manager_id_2:I = 0x7f070018

.field public static final dialog_phone_number_call_disambiguation:I = 0x7f070015

.field public static final dialog_phone_number_message_disambiguation:I = 0x7f070016

.field public static final dialog_sdcard_not_found:I = 0x7f070006

.field public static final dialog_searching_vcard:I = 0x7f070005

.field public static final dialog_select_import_type:I = 0x7f070008

.field public static final dialog_select_multiple_vcard:I = 0x7f07000a

.field public static final dialog_select_one_vcard:I = 0x7f070009

.field public static final dialog_sync_add:I = 0x7f070004

.field public static final dialog_vcard_not_found:I = 0x7f070007

.field public static final dialpad:I = 0x7f070109

.field public static final dialpadAdditionalButtons:I = 0x7f07010a

.field public static final dialpadButton:I = 0x7f07002a

.field public static final dialpadChooser:I = 0x7f070110

.field public static final dialpadDivider:I = 0x7f070111

.field public static final dialtacts_frame:I = 0x7f070113

.field public static final digits:I = 0x7f07010f

.field public static final digits_container:I = 0x7f070112

.field public static final directory_header:I = 0x7f070117

.field public static final displayName:I = 0x7f07018a

.field public static final display_name:I = 0x7f070118

.field public static final divider:I = 0x7f070090

.field public static final done:I = 0x7f07012a

.field public static final done_menu_item:I = 0x7f070094

.field public static final downText:I = 0x7f0700de

.field public static final duration:I = 0x7f070058

.field public static final edit_name:I = 0x7f07017a

.field public static final edit_phonetic_name:I = 0x7f07017b

.field public static final edit_photo:I = 0x7f07017c

.field public static final editor_container:I = 0x7f0700a7

.field public static final editors:I = 0x7f0700d7

.field public static final eight:I = 0x7f070025

.field public static final empty:I = 0x7f07012f

.field public static final emptyText:I = 0x7f0700b3

.field public static final empty_right_menu_item:I = 0x7f0701ca

.field public static final expansion_view:I = 0x7f07011d

.field public static final expansion_view_container:I = 0x7f070165

.field public static final extra_info:I = 0x7f0700a4

.field public static final favorites_fragment:I = 0x7f070162

.field public static final favorites_view:I = 0x7f070161

.field public static final filter_option:I = 0x7f0701c8

.field public static final first_divider:I = 0x7f07007a

.field public static final five:I = 0x7f070022

.field public static final floating_layout:I = 0x7f070171

.field public static final footer:I = 0x7f0700c0

.field public static final footer_stub:I = 0x7f0700e1

.field public static final four:I = 0x7f070021

.field public static final fragment:I = 0x7f07012c

.field public static final fragment_carousel:I = 0x7f0700b1

.field public static final frame:I = 0x7f070147

.field public static final frequent_fragment:I = 0x7f070163

.field public static final group_detail:I = 0x7f070133

.field public static final group_detail_fragment:I = 0x7f070132

.field public static final group_details_view:I = 0x7f070160

.field public static final group_editor_fragment:I = 0x7f070137

.field public static final group_label:I = 0x7f07013b

.field public static final group_list:I = 0x7f070143

.field public static final group_list_header:I = 0x7f070131

.field public static final group_members:I = 0x7f070139

.field public static final group_membership_view:I = 0x7f070142

.field public static final group_menu_item:I = 0x7f070154

.field public static final group_name:I = 0x7f070128

.field public static final group_size:I = 0x7f070136

.field public static final group_source:I = 0x7f07013c

.field public static final group_source_view_container:I = 0x7f070134

.field public static final group_title:I = 0x7f070135

.field public static final groups_fragment:I = 0x7f07015d

.field public static final header_RCS_container:I = 0x7f07005e

.field public static final header_call_and_sms_container:I = 0x7f070054

.field public static final header_container:I = 0x7f0700f2

.field public static final header_extra_top_padding:I = 0x7f07012d

.field public static final header_ip_call_container:I = 0x7f07005c

.field public static final header_phones:I = 0x7f070003

.field public static final header_text:I = 0x7f070049

.field public static final header_video_call_container:I = 0x7f07005a

.field public static final header_voicemail_container:I = 0x7f070053

.field public static final history:I = 0x7f070041

.field public static final horizontal_divider:I = 0x7f0701a5

.field public static final icon:I = 0x7f070032

.field public static final image:I = 0x7f07018f

.field public static final import_contacts_button:I = 0x7f0700f8

.field public static final import_failure_retry_button:I = 0x7f0700fa

.field public static final import_failure_uninstall_button:I = 0x7f0700f9

.field public static final internetIcon:I = 0x7f07018d

.field public static final ip_call:I = 0x7f07006a

.field public static final ip_call_action:I = 0x7f070066

.field public static final ip_call_container:I = 0x7f070069

.field public static final ip_call_label:I = 0x7f07006c

.field public static final ip_call_text:I = 0x7f07006b

.field public static final item_list_pager:I = 0x7f070173

.field public static final join_contact_blurb:I = 0x7f07014e

.field public static final kind:I = 0x7f0700c6

.field public static final kind_editors:I = 0x7f070145

.field public static final kind_title:I = 0x7f07011f

.field public static final kind_title_layout:I = 0x7f070144

.field public static final labe_and_geocode_text:I = 0x7f070062

.field public static final label:I = 0x7f07008c

.field public static final labelAndNumber:I = 0x7f070105

.field public static final label_background:I = 0x7f07009e

.field public static final left:I = 0x7f070000

.field public static final left_button:I = 0x7f0700d4

.field public static final line_after_track:I = 0x7f070172

.field public static final list:I = 0x7f07012e

.field public static final list_container:I = 0x7f0700e6

.field public static final list_fragment:I = 0x7f0700db

.field public static final list_view:I = 0x7f07010e

.field public static final loader_fragment:I = 0x7f0700ab

.field public static final loading_contact:I = 0x7f0701af

.field public static final loading_container:I = 0x7f0701ad

.field public static final main_action:I = 0x7f070048

.field public static final main_action_push_layer:I = 0x7f07004a

.field public static final main_view:I = 0x7f07015a

.field public static final menu_2s_pause:I = 0x7f0701c1

.field public static final menu_accounts:I = 0x7f0701da

.field public static final menu_add:I = 0x7f0701b5

.field public static final menu_add_contact:I = 0x7f0701d4

.field public static final menu_add_contacts:I = 0x7f0701c0

.field public static final menu_add_group:I = 0x7f0701d5

.field public static final menu_add_wait:I = 0x7f0701c2

.field public static final menu_association_sim:I = 0x7f0701e2

.field public static final menu_block_video_incoming_call:I = 0x7f0701e4

.field public static final menu_block_voice_incoming_call:I = 0x7f0701e3

.field public static final menu_call_settings:I = 0x7f0701c7

.field public static final menu_call_settings_dialpad:I = 0x7f0701c4

.field public static final menu_clear_frequents:I = 0x7f0701d9

.field public static final menu_clear_select:I = 0x7f0701d1

.field public static final menu_contacts_filter:I = 0x7f0701d7

.field public static final menu_create_contact_shortcut:I = 0x7f0701e5

.field public static final menu_delete:I = 0x7f0701b6

.field public static final menu_delete_contact:I = 0x7f0701d6

.field public static final menu_delete_group:I = 0x7f0701e8

.field public static final menu_discard:I = 0x7f0701ce

.field public static final menu_done:I = 0x7f0701cb

.field public static final menu_edit:I = 0x7f0701df

.field public static final menu_edit_group:I = 0x7f0701e7

.field public static final menu_edit_number_before_call:I = 0x7f0701b2

.field public static final menu_email_group:I = 0x7f0701eb

.field public static final menu_group_source:I = 0x7f0701d0

.field public static final menu_help:I = 0x7f0701cf

.field public static final menu_import_export:I = 0x7f0701d8

.field public static final menu_ip_dial:I = 0x7f0701bd

.field public static final menu_join:I = 0x7f0701cd

.field public static final menu_look_simstorage:I = 0x7f0701dd

.field public static final menu_message_group:I = 0x7f0701ea

.field public static final menu_move_group:I = 0x7f0701e9

.field public static final menu_option:I = 0x7f0701d2

.field public static final menu_people:I = 0x7f0701c3

.field public static final menu_print:I = 0x7f0701e6

.field public static final menu_remove_from_call_log:I = 0x7f0701b1

.field public static final menu_search:I = 0x7f0701d3

.field public static final menu_select_all:I = 0x7f0701b3

.field public static final menu_send_message:I = 0x7f0701bf

.field public static final menu_set_ringtone:I = 0x7f0701e1

.field public static final menu_settings:I = 0x7f0701db

.field public static final menu_share:I = 0x7f0701e0

.field public static final menu_share_visible_contacts:I = 0x7f0701dc

.field public static final menu_speed_dial:I = 0x7f0701be

.field public static final menu_split:I = 0x7f0701cc

.field public static final menu_star:I = 0x7f0701de

.field public static final menu_unselect_all:I = 0x7f0701b4

.field public static final menu_video_call:I = 0x7f0701bc

.field public static final message:I = 0x7f070097

.field public static final month:I = 0x7f0700ff

.field public static final more_icon:I = 0x7f07003f

.field public static final multichoice_confirm_dialog:I = 0x7f07001c

.field public static final multichoice_contact_list_layout:I = 0x7f070150

.field public static final multichoice_report_dialog:I = 0x7f07001d

.field public static final name:I = 0x7f07008a

.field public static final name_and_snippet:I = 0x7f070192

.field public static final name_and_snippet_container:I = 0x7f070191

.field public static final network_icon:I = 0x7f0700c8

.field public static final network_title:I = 0x7f0700c9

.field public static final new_contact:I = 0x7f0700e7

.field public static final next:I = 0x7f0700dc

.field public static final nine:I = 0x7f070026

.field public static final number:I = 0x7f07008b

.field public static final one:I = 0x7f07001e

.field public static final open_details_button:I = 0x7f0700a3

.field public static final open_details_push_layer:I = 0x7f0700a5

.field public static final operator:I = 0x7f070107

.field public static final overflow_menu:I = 0x7f07002c

.field public static final pager:I = 0x7f0700af

.field public static final parent:I = 0x7f0700fe

.field public static final phoneNumber:I = 0x7f07018b

.field public static final photo:I = 0x7f07009c

.field public static final photo_container:I = 0x7f070175

.field public static final photo_overlay:I = 0x7f07009d

.field public static final photo_text_bar:I = 0x7f070047

.field public static final photo_touch_intercept_overlay:I = 0x7f070166

.field public static final photo_triangle_affordance:I = 0x7f070148

.field public static final photo_triangle_affordance_temp:I = 0x7f070149

.field public static final pinned_header_list_layout:I = 0x7f0700df

.field public static final playback_position_text:I = 0x7f07016d

.field public static final playback_seek:I = 0x7f07016c

.field public static final playback_speakerphone:I = 0x7f07016a

.field public static final playback_speed_text:I = 0x7f07016e

.field public static final playback_start_stop:I = 0x7f070169

.field public static final pound:I = 0x7f070029

.field public static final presence_icon:I = 0x7f0700ba

.field public static final primary_action_view:I = 0x7f070087

.field public static final primary_icon:I = 0x7f0700bf

.field public static final primary_indicator:I = 0x7f0700bc

.field public static final profile_title:I = 0x7f0701ab

.field public static final progress:I = 0x7f070099

.field public static final progress_loading_contact:I = 0x7f0701ae

.field public static final progress_spinner:I = 0x7f070164

.field public static final push_layer:I = 0x7f0701a6

.field public static final quick_contact_photo:I = 0x7f070089

.field public static final radioButton:I = 0x7f0700e4

.field public static final rate_decrease_button:I = 0x7f07016f

.field public static final rate_increase_button:I = 0x7f070170

.field public static final rcs_icon:I = 0x7f070031

.field public static final read_only_name:I = 0x7f07017f

.field public static final read_only_warning:I = 0x7f0700a6

.field public static final revert:I = 0x7f07012b

.field public static final right:I = 0x7f070001

.field public static final right_button:I = 0x7f0700d5

.field public static final root_view:I = 0x7f0700a2

.field public static final save_menu_item:I = 0x7f070125

.field public static final sd_index:I = 0x7f070194

.field public static final sd_label:I = 0x7f070196

.field public static final sd_name:I = 0x7f070199

.field public static final sd_number:I = 0x7f070198

.field public static final sd_photo:I = 0x7f070195

.field public static final sd_remove:I = 0x7f070197

.field public static final searchButton:I = 0x7f07010b

.field public static final search_menu_item:I = 0x7f070153

.field public static final search_on_action_bar:I = 0x7f0701c5

.field public static final search_option:I = 0x7f070116

.field public static final search_progress:I = 0x7f0700e0

.field public static final search_view:I = 0x7f0700e5

.field public static final second_image_container:I = 0x7f0701a8

.field public static final secondary_action_button:I = 0x7f0700c5

.field public static final secondary_action_icon:I = 0x7f070088

.field public static final secondary_action_view_container:I = 0x7f0700b9

.field public static final secondary_divider:I = 0x7f07007d

.field public static final secondary_message:I = 0x7f070098

.field public static final sect_fields:I = 0x7f07017d

.field public static final sect_general:I = 0x7f070181

.field public static final seek_container:I = 0x7f07016b

.field public static final select:I = 0x7f07018c

.field public static final select_items:I = 0x7f070095

.field public static final selected_tab_rectangle:I = 0x7f070178

.field public static final separator01:I = 0x7f070059

.field public static final separator02:I = 0x7f07005b

.field public static final separator03:I = 0x7f07005d

.field public static final setPrimary:I = 0x7f070184

.field public static final seven:I = 0x7f070024

.field public static final shadow:I = 0x7f0700cf

.field public static final shortPhoneNumber:I = 0x7f070188

.field public static final show_all_calls:I = 0x7f0701b8

.field public static final show_auto_rejected_calls:I = 0x7f0701ba

.field public static final show_voicemails_only:I = 0x7f0701b7

.field public static final simIcon:I = 0x7f070185

.field public static final simSignal:I = 0x7f070187

.field public static final simStatus:I = 0x7f070186

.field public static final sim_name:I = 0x7f07005f

.field public static final sim_photo:I = 0x7f07014c

.field public static final six:I = 0x7f070023

.field public static final sourceIcon:I = 0x7f07019a

.field public static final spacer:I = 0x7f07013a

.field public static final star:I = 0x7f070028

.field public static final static_photo_container:I = 0x7f0700b7

.field public static final status:I = 0x7f0700a1

.field public static final status_description:I = 0x7f07019e

.field public static final status_icon:I = 0x7f07019c

.field public static final status_photo:I = 0x7f0700a0

.field public static final status_progress_bar:I = 0x7f07019f

.field public static final status_progress_text:I = 0x7f07019d

.field public static final stream_item_attribution:I = 0x7f0701a3

.field public static final stream_item_comments:I = 0x7f0701a4

.field public static final stream_item_content:I = 0x7f0701a0

.field public static final stream_item_first_image:I = 0x7f0701a7

.field public static final stream_item_html:I = 0x7f0701a2

.field public static final stream_item_image_rows:I = 0x7f0701a1

.field public static final stream_item_second_image:I = 0x7f0701a9

.field public static final stub_photo:I = 0x7f070182

.field public static final sub_title:I = 0x7f07014f

.field public static final suggested:I = 0x7f070189

.field public static final tab_about:I = 0x7f0700cc

.field public static final tab_and_shadow_container:I = 0x7f0700ca

.field public static final tab_carousel:I = 0x7f0700b0

.field public static final tab_container:I = 0x7f0700cb

.field public static final tab_divider:I = 0x7f0700cd

.field public static final tab_pager:I = 0x7f070157

.field public static final tab_update:I = 0x7f0700ce

.field public static final text:I = 0x7f0700d1

.field public static final text1:I = 0x7f07003d

.field public static final text2:I = 0x7f07003e

.field public static final third_divider:I = 0x7f070080

.field public static final three:I = 0x7f070020

.field public static final tips:I = 0x7f07013f

.field public static final title:I = 0x7f0700fc

.field public static final top:I = 0x7f07010d

.field public static final topview_layout:I = 0x7f07013e

.field public static final totalContactsText:I = 0x7f070183

.field public static final track:I = 0x7f070177

.field public static final track_scroller:I = 0x7f070176

.field public static final two:I = 0x7f07001f

.field public static final type:I = 0x7f0700bb

.field public static final upText:I = 0x7f0700dd

.field public static final updates_fragment_container:I = 0x7f0700b2

.field public static final user_profile_header:I = 0x7f0701aa

.field public static final usim_photo:I = 0x7f07014d

.field public static final vertical_divider:I = 0x7f0700c4

.field public static final vertical_divider_vtcall:I = 0x7f0700c1

.field public static final videoDialButton:I = 0x7f07002d

.field public static final video_call:I = 0x7f070065

.field public static final video_call_action:I = 0x7f070061

.field public static final video_call_container:I = 0x7f070064

.field public static final video_call_label:I = 0x7f070068

.field public static final video_call_text:I = 0x7f070067

.field public static final voicemail_container:I = 0x7f07004b

.field public static final voicemail_status:I = 0x7f070043

.field public static final voicemail_status_action:I = 0x7f07009b

.field public static final voicemail_status_message:I = 0x7f07009a

.field public static final vtcall_action_button:I = 0x7f0700c3

.field public static final vtcall_action_view_container:I = 0x7f0700c2

.field public static final widget_container:I = 0x7f07018e

.field public static final year:I = 0x7f070101

.field public static final yearToggle:I = 0x7f0700fd

.field public static final zero:I = 0x7f070027


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
