.class public final Lcom/android/contacts/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CallLog:[I

.field public static final CallLog_call_log_header_color:I = 0x4

.field public static final CallLog_call_log_primary_background_color:I = 0x1

.field public static final CallLog_call_log_primary_text_color:I = 0x0

.field public static final CallLog_call_log_secondary_background_color:I = 0x3

.field public static final CallLog_call_log_secondary_text_color:I = 0x2

.field public static final ContactBrowser:[I

.field public static final ContactBrowser_contact_browser_background:I = 0x2

.field public static final ContactBrowser_contact_browser_list_padding_left:I = 0x0

.field public static final ContactBrowser_contact_browser_list_padding_right:I = 0x1

.field public static final ContactListItemView:[I

.field public static final ContactListItemView_activated_background:I = 0x2

.field public static final ContactListItemView_list_item_call_button_padding:I = 0xb

.field public static final ContactListItemView_list_item_contacts_count_text_color:I = 0x18

.field public static final ContactListItemView_list_item_contacts_count_text_size:I = 0x1a

.field public static final ContactListItemView_list_item_data_width_weight:I = 0x1b

.field public static final ContactListItemView_list_item_divider:I = 0x4

.field public static final ContactListItemView_list_item_gap_between_image_and_text:I = 0x9

.field public static final ContactListItemView_list_item_gap_between_label_and_data:I = 0xa

.field public static final ContactListItemView_list_item_header_height:I = 0x15

.field public static final ContactListItemView_list_item_header_text_color:I = 0x13

.field public static final ContactListItemView_list_item_header_text_indent:I = 0x12

.field public static final ContactListItemView_list_item_header_text_size:I = 0x14

.field public static final ContactListItemView_list_item_header_underline_color:I = 0x17

.field public static final ContactListItemView_list_item_header_underline_height:I = 0x16

.field public static final ContactListItemView_list_item_height:I = 0x0

.field public static final ContactListItemView_list_item_label_width_weight:I = 0x1c

.field public static final ContactListItemView_list_item_padding_bottom:I = 0x7

.field public static final ContactListItemView_list_item_padding_left:I = 0x8

.field public static final ContactListItemView_list_item_padding_right:I = 0x6

.field public static final ContactListItemView_list_item_padding_top:I = 0x5

.field public static final ContactListItemView_list_item_photo_size:I = 0xf

.field public static final ContactListItemView_list_item_prefix_highlight_color:I = 0x11

.field public static final ContactListItemView_list_item_presence_icon_margin:I = 0xd

.field public static final ContactListItemView_list_item_presence_icon_size:I = 0xe

.field public static final ContactListItemView_list_item_profile_photo_size:I = 0x10

.field public static final ContactListItemView_list_item_text_indent:I = 0x19

.field public static final ContactListItemView_list_item_vertical_divider_margin:I = 0xc

.field public static final ContactListItemView_list_section_header_height:I = 0x1

.field public static final ContactListItemView_section_header_background:I = 0x3

.field public static final EdgeTriggerView:[I

.field public static final EdgeTriggerView_edgeWidth:I = 0x0

.field public static final EdgeTriggerView_listenEdges:I = 0x1

.field public static final Favorites:[I

.field public static final Favorites_favorites_padding_bottom:I = 0x0

.field public static final InterpolatingLayout_Layout:[I

.field public static final InterpolatingLayout_Layout_layout_narrowMarginLeft:I = 0x2

.field public static final InterpolatingLayout_Layout_layout_narrowMarginRight:I = 0x3

.field public static final InterpolatingLayout_Layout_layout_narrowPaddingLeft:I = 0x4

.field public static final InterpolatingLayout_Layout_layout_narrowPaddingRight:I = 0x5

.field public static final InterpolatingLayout_Layout_layout_narrowParentWidth:I = 0x0

.field public static final InterpolatingLayout_Layout_layout_narrowWidth:I = 0x1

.field public static final InterpolatingLayout_Layout_layout_wideMarginLeft:I = 0x8

.field public static final InterpolatingLayout_Layout_layout_wideMarginRight:I = 0x9

.field public static final InterpolatingLayout_Layout_layout_widePaddingLeft:I = 0xa

.field public static final InterpolatingLayout_Layout_layout_widePaddingRight:I = 0xb

.field public static final InterpolatingLayout_Layout_layout_wideParentWidth:I = 0x6

.field public static final InterpolatingLayout_Layout_layout_wideWidth:I = 0x7

.field public static final Mapping:[I

.field public static final Mapping_detailColumn:I = 0x4

.field public static final Mapping_icon:I = 0x2

.field public static final Mapping_mimeType:I = 0x0

.field public static final Mapping_remoteViews:I = 0x1

.field public static final Mapping_summaryColumn:I = 0x3

.field public static final ProportionalLayout:[I

.field public static final ProportionalLayout_direction:I = 0x0

.field public static final ProportionalLayout_ratio:I = 0x1

.field public static final VoicemailStatus:[I

.field public static final VoicemailStatus_call_log_voicemail_status_action_text_color:I = 0x3

.field public static final VoicemailStatus_call_log_voicemail_status_background_color:I = 0x1

.field public static final VoicemailStatus_call_log_voicemail_status_height:I = 0x0

.field public static final VoicemailStatus_call_log_voicemail_status_text_color:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x5

    const/4 v3, 0x2

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/contacts/R$styleable;->CallLog:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/contacts/R$styleable;->ContactBrowser:[I

    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/contacts/R$styleable;->ContactListItemView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/contacts/R$styleable;->EdgeTriggerView:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f01003e

    aput v2, v0, v1

    sput-object v0, Lcom/android/contacts/R$styleable;->Favorites:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/contacts/R$styleable;->InterpolatingLayout_Layout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/contacts/R$styleable;->Mapping:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/android/contacts/R$styleable;->ProportionalLayout:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/android/contacts/R$styleable;->VoicemailStatus:[I

    return-void

    :array_0
    .array-data 4
        0x7f010035
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
    .end array-data

    :array_1
    .array-data 4
        0x7f010015
        0x7f010016
        0x7f010017
    .end array-data

    :array_2
    .array-data 4
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
        0x7f010033
        0x7f010034
    .end array-data

    :array_3
    .array-data 4
        0x7f010005
        0x7f010006
    .end array-data

    :array_4
    .array-data 4
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
    .end array-data

    :array_5
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
    .end array-data

    :array_6
    .array-data 4
        0x7f010013
        0x7f010014
    .end array-data

    :array_7
    .array-data 4
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
