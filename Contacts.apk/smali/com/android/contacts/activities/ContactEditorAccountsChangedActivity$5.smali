.class Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;
.super Ljava/lang/Object;
.source "ContactEditorAccountsChangedActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;


# direct methods
.method constructor <init>(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    invoke-static {}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$600()Ljava/lang/String;

    move-result-object v2

    const-string v3, "serviceComplete run"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    invoke-static {v2}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$500(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)Lcom/mediatek/CellConnService/CellConnMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/CellConnService/CellConnMgr;->getResult()I

    move-result v0

    invoke-static {}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$600()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "serviceComplete result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/mediatek/CellConnService/CellConnMgr;->resultToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    invoke-static {v2}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$500(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)Lcom/mediatek/CellConnService/CellConnMgr;

    const/4 v2, 0x2

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    iget-object v3, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    invoke-static {v3}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$400(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)I

    move-result v3

    invoke-static {v2, v3}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    iget-wide v3, v1, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    invoke-static {v2, v3, v4}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$902(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;J)J

    :cond_1
    invoke-static {}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$600()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSimSelectionDialog mSimId is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    invoke-static {v4}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$900(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$1002(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;Z)Z

    iget-object v2, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    iget-object v3, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    invoke-static {v3}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$000(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)Lcom/android/contacts/util/AccountsListAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    invoke-static {v4}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$1100(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/contacts/util/AccountsListAdapter;->getItem(I)Lcom/android/contacts/model/AccountWithDataSet;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$700(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;Lcom/android/contacts/model/AccountWithDataSet;)V

    goto :goto_0
.end method
