.class Lcom/android/contacts/activities/DialtactsActivity$DialtactsBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DialtactsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/activities/DialtactsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DialtactsBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/activities/DialtactsActivity;


# direct methods
.method private constructor <init>(Lcom/android/contacts/activities/DialtactsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/activities/DialtactsActivity$DialtactsBroadcastReceiver;->this$0:Lcom/android/contacts/activities/DialtactsActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/contacts/activities/DialtactsActivity;Lcom/android/contacts/activities/DialtactsActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/contacts/activities/DialtactsActivity;
    .param p2    # Lcom/android/contacts/activities/DialtactsActivity$1;

    invoke-direct {p0, p1}, Lcom/android/contacts/activities/DialtactsActivity$DialtactsBroadcastReceiver;-><init>(Lcom/android/contacts/activities/DialtactsActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/activities/DialtactsActivity$DialtactsBroadcastReceiver;->this$0:Lcom/android/contacts/activities/DialtactsActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DialtactsBroadcastReceiver, onReceive action = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/contacts/activities/DialtactsActivity;->log(Ljava/lang/String;)V

    const-string v1, "android.intent.action.VOICE_CALL_DEFAULT_SIM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/contacts/activities/DialtactsActivity$DialtactsBroadcastReceiver;->this$0:Lcom/android/contacts/activities/DialtactsActivity;

    invoke-static {v1}, Lcom/android/contacts/activities/DialtactsActivity;->access$2200(Lcom/android/contacts/activities/DialtactsActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/contacts/activities/DialtactsActivity$DialtactsBroadcastReceiver;->this$0:Lcom/android/contacts/activities/DialtactsActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/contacts/activities/DialtactsActivity;->setSimIndicatorVisibility(Z)V

    :cond_0
    const-string v1, "com.android.contacts.ACTION_PHB_LOAD_FINISHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/contacts/activities/DialtactsActivity$DialtactsBroadcastReceiver;->this$0:Lcom/android/contacts/activities/DialtactsActivity;

    invoke-static {v1}, Lcom/android/contacts/activities/DialtactsActivity;->access$000(Lcom/android/contacts/activities/DialtactsActivity;)Lcom/android/contacts/dialpad/DialpadFragment;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/contacts/activities/DialtactsActivity$DialtactsBroadcastReceiver;->this$0:Lcom/android/contacts/activities/DialtactsActivity;

    invoke-static {v1}, Lcom/android/contacts/activities/DialtactsActivity;->access$000(Lcom/android/contacts/activities/DialtactsActivity;)Lcom/android/contacts/dialpad/DialpadFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/contacts/dialpad/DialpadFragment;->updateDialerSearch()V

    :cond_1
    return-void
.end method
