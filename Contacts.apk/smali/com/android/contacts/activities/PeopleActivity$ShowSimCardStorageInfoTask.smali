.class Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;
.super Landroid/os/AsyncTask;
.source "PeopleActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/activities/PeopleActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShowSimCardStorageInfoTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static sInstance:Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDlgContent:Ljava/lang/String;

.field private mIsCancelled:Z

.field private mIsException:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->sInstance:Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-boolean v0, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mIsCancelled:Z

    iput-boolean v0, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mIsException:Z

    iput-object v1, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mDlgContent:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mContext:Landroid/content/Context;

    const-string v0, "PeopleActivity"

    const-string v1, "[ShowSimCardStorageInfoTask] onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static showSimCardStorageInfo(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    const-string v0, "PeopleActivity"

    const-string v1, "[ShowSimCardStorageInfoTask]_beg"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->sInstance:Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->sInstance:Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;

    invoke-virtual {v0}, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->cancel()V

    const/4 v0, 0x0

    sput-object v0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->sInstance:Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;

    :cond_0
    new-instance v0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;

    invoke-direct {v0, p0}, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->sInstance:Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;

    sget-object v0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->sInstance:Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const-string v0, "PeopleActivity"

    const-string v1, "[ShowSimCardStorageInfoTask]_end"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    const/4 v0, 0x1

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->cancel(Z)Z

    iput-boolean v0, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mIsCancelled:Z

    const-string v0, "PeopleActivity"

    const-string v1, "[ShowSimCardStorageInfoTask]: mIsCancelled = true"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 13
    .param p1    # [Ljava/lang/Void;

    const-string v8, "PeopleActivity"

    const-string v9, "[ShowSimCardStorageInfoTask]: doInBackground_beg"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mContext:Landroid/content/Context;

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/android/contacts/activities/ContactDetailActivity;->getInsertedSimCardInfoList(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v6

    const-string v8, "PeopleActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[ShowSimCardStorageInfoTask]: simInfos.size = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v8, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mIsCancelled:Z

    if-nez v8, :cond_4

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/provider/Telephony$SIMInfo;

    if-lez v2, :cond_1

    const-string v8, "\n\n"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    const/4 v7, 0x0

    const-string v8, "PeopleActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[ShowSimCardStorageInfoTask] simName = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v5, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; simSlot = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v5, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; simId = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, v5, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, v5, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ":\n"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :try_start_0
    const-string v8, "phone"

    invoke-static {v8}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v4

    iget-boolean v8, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mIsCancelled:Z

    if-nez v8, :cond_2

    if-eqz v4, :cond_2

    iget v8, v5, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-interface {v4, v8}, Lcom/android/internal/telephony/ITelephony;->getAdnStorageInfo(I)[I

    move-result-object v7

    const-string v8, "PeopleActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[ShowSimCardStorageInfoTask] infos: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v8, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c00de

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x1

    aget v12, v7, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const/4 v12, 0x0

    aget v12, v7, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v8, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mIsCancelled:Z

    if-eqz v8, :cond_0

    const/4 v8, 0x0

    :goto_0
    return-object v8

    :cond_2
    :try_start_1
    const-string v8, "PeopleActivity"

    const-string v9, "[ShowSimCardStorageInfoTask]: phone = null"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mIsException:Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v8, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v8, "PeopleActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[ShowSimCardStorageInfoTask]_exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mIsException:Z

    const/4 v8, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mDlgContent:Ljava/lang/String;

    :cond_4
    const-string v8, "PeopleActivity"

    const-string v9, "[ShowSimCardStorageInfoTask]: doInBackground_end"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1    # Ljava/lang/Void;

    const/4 v2, 0x0

    sput-object v2, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->sInstance:Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;

    iget-boolean v0, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mIsCancelled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mIsException:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0200b0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c00dd

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/activities/PeopleActivity$ShowSimCardStorageInfoTask;->mDlgContent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    return-void
.end method
