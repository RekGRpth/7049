.class Lcom/android/contacts/calllog/CallLogFragment$1;
.super Landroid/os/Handler;
.source "CallLogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/calllog/CallLogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/calllog/CallLogFragment;


# direct methods
.method constructor <init>(Lcom/android/contacts/calllog/CallLogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/calllog/CallLogFragment$1;->this$0:Lcom/android/contacts/calllog/CallLogFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment$1;->this$0:Lcom/android/contacts/calllog/CallLogFragment;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage msg = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/contacts/calllog/CallLogFragment;->access$200(Lcom/android/contacts/calllog/CallLogFragment;Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment$1;->this$0:Lcom/android/contacts/calllog/CallLogFragment;

    invoke-static {v0}, Lcom/android/contacts/calllog/CallLogFragment;->access$300(Lcom/android/contacts/calllog/CallLogFragment;)Lcom/android/contacts/calllog/CallLogAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment$1;->this$0:Lcom/android/contacts/calllog/CallLogFragment;

    invoke-static {v0}, Lcom/android/contacts/calllog/CallLogFragment;->access$300(Lcom/android/contacts/calllog/CallLogFragment;)Lcom/android/contacts/calllog/CallLogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :sswitch_1
    const-string v2, "CallLogFragment"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start WAIT_CURSOR_START !isFinished : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment$1;->this$0:Lcom/android/contacts/calllog/CallLogFragment;

    invoke-static {v0}, Lcom/android/contacts/calllog/CallLogFragment;->access$400(Lcom/android/contacts/calllog/CallLogFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment$1;->this$0:Lcom/android/contacts/calllog/CallLogFragment;

    invoke-static {v0}, Lcom/android/contacts/calllog/CallLogFragment;->access$400(Lcom/android/contacts/calllog/CallLogFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment$1;->this$0:Lcom/android/contacts/calllog/CallLogFragment;

    invoke-static {v0}, Lcom/android/contacts/calllog/CallLogFragment;->access$500(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/widget/TextView;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment$1;->this$0:Lcom/android/contacts/calllog/CallLogFragment;

    invoke-static {v0}, Lcom/android/contacts/calllog/CallLogFragment;->access$600(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment$1;->this$0:Lcom/android/contacts/calllog/CallLogFragment;

    invoke-static {v0}, Lcom/android/contacts/calllog/CallLogFragment;->access$700(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment$1;->this$0:Lcom/android/contacts/calllog/CallLogFragment;

    invoke-static {v0}, Lcom/android/contacts/calllog/CallLogFragment;->access$800(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x4ce -> :sswitch_1
    .end sparse-switch
.end method
