.class Lcom/android/contacts/calllog/CallLogListItemHelper;
.super Ljava/lang/Object;
.source "CallLogListItemHelper.java"


# instance fields
.field private final mPhoneCallDetailsHelper:Lcom/android/contacts/PhoneCallDetailsHelper;

.field private final mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/android/contacts/PhoneCallDetailsHelper;Lcom/android/contacts/calllog/PhoneNumberHelper;Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # Lcom/android/contacts/PhoneCallDetailsHelper;
    .param p2    # Lcom/android/contacts/calllog/PhoneNumberHelper;
    .param p3    # Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/contacts/calllog/CallLogListItemHelper;->mPhoneCallDetailsHelper:Lcom/android/contacts/PhoneCallDetailsHelper;

    iput-object p2, p0, Lcom/android/contacts/calllog/CallLogListItemHelper;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    iput-object p3, p0, Lcom/android/contacts/calllog/CallLogListItemHelper;->mResources:Landroid/content/res/Resources;

    return-void
.end method
