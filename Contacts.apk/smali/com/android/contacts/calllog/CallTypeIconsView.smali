.class public Lcom/android/contacts/calllog/CallTypeIconsView;
.super Landroid/view/View;
.source "CallTypeIconsView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/contacts/calllog/CallTypeIconsView$Resources;
    }
.end annotation


# instance fields
.field private mCallType:I

.field private mHeight:I

.field private mResources:Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

.field private mVTCall:I

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/contacts/calllog/CallTypeIconsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

    invoke-direct {v0, p1}, Lcom/android/contacts/calllog/CallTypeIconsView$Resources;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mResources:Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

    return-void
.end method

.method private getCallTypeDrawable(II)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid call type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mResources:Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

    iget-object v0, v0, Lcom/android/contacts/calllog/CallTypeIconsView$Resources;->vtincoming:Landroid/graphics/drawable/Drawable;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mResources:Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

    iget-object v0, v0, Lcom/android/contacts/calllog/CallTypeIconsView$Resources;->incoming:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_1
    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mResources:Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

    iget-object v0, v0, Lcom/android/contacts/calllog/CallTypeIconsView$Resources;->vtoutgoing:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mResources:Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

    iget-object v0, v0, Lcom/android/contacts/calllog/CallTypeIconsView$Resources;->outgoing:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_2
    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mResources:Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

    iget-object v0, v0, Lcom/android/contacts/calllog/CallTypeIconsView$Resources;->vtmissed:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mResources:Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

    iget-object v0, v0, Lcom/android/contacts/calllog/CallTypeIconsView$Resources;->missed:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mResources:Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

    iget-object v0, v0, Lcom/android/contacts/calllog/CallTypeIconsView$Resources;->voicemail:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_4
    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mResources:Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

    iget-object v0, v0, Lcom/android/contacts/calllog/CallTypeIconsView$Resources;->vtautorejected:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mResources:Lcom/android/contacts/calllog/CallTypeIconsView$Resources;

    iget-object v0, v0, Lcom/android/contacts/calllog/CallTypeIconsView$Resources;->autorejected:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public clear()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mCallType:I

    iput v1, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mWidth:I

    iput v1, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mHeight:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public getCallType()I
    .locals 1

    iget v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mCallType:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    const/4 v3, 0x0

    iget v1, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mCallType:I

    iget v2, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mVTCall:I

    invoke-direct {p0, v1, v2}, Lcom/android/contacts/calllog/CallTypeIconsView;->getCallTypeDrawable(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mWidth:I

    iget v1, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mHeight:I

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public set(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mCallType:I

    iput p2, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mVTCall:I

    invoke-direct {p0, p1, p2}, Lcom/android/contacts/calllog/CallTypeIconsView;->getCallTypeDrawable(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mWidth:I

    iget v1, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mHeight:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/android/contacts/calllog/CallTypeIconsView;->mHeight:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto :goto_0
.end method
