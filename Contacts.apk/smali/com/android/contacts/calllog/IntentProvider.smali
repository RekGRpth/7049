.class public abstract Lcom/android/contacts/calllog/IntentProvider;
.super Ljava/lang/Object;
.source "IntentProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCallDetailIntentProvider(Lcom/android/contacts/calllog/CallLogAdapter;IJI)Lcom/android/contacts/calllog/IntentProvider;
    .locals 6
    .param p0    # Lcom/android/contacts/calllog/CallLogAdapter;
    .param p1    # I
    .param p2    # J
    .param p4    # I

    new-instance v0, Lcom/android/contacts/calllog/IntentProvider$1;

    move-object v1, p0

    move v2, p1

    move v3, p4

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/contacts/calllog/IntentProvider$1;-><init>(Lcom/android/contacts/calllog/CallLogAdapter;IIJ)V

    return-object v0
.end method

.method public static getReturnCallIntentProvider(Ljava/lang/String;J)Lcom/android/contacts/calllog/IntentProvider;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # J

    new-instance v0, Lcom/android/contacts/calllog/IntentProvider$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/contacts/calllog/IntentProvider$2;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method


# virtual methods
.method public abstract getIntent(Landroid/content/Context;)Landroid/content/Intent;
.end method
