.class interface abstract Lcom/android/contacts/detail/ContactDetailLayoutController$LayoutMode;
.super Ljava/lang/Object;
.source "ContactDetailLayoutController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/detail/ContactDetailLayoutController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "LayoutMode"
.end annotation


# static fields
.field public static final FRAGMENT_CAROUSEL:I = 0x2

.field public static final TWO_COLUMN:I = 0x0

.field public static final TWO_COLUMN_FRAGMENT_CAROUSEL:I = 0x3

.field public static final VIEW_PAGER_AND_TAB_CAROUSEL:I = 0x1
