.class public Lcom/android/contacts/dialpad/DialpadImageButton;
.super Landroid/widget/ImageButton;
.source "DialpadImageButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/contacts/dialpad/DialpadImageButton$OnPressedListener;
    }
.end annotation


# instance fields
.field private mOnPressedListener:Lcom/android/contacts/dialpad/DialpadImageButton$OnPressedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public setOnPressedListener(Lcom/android/contacts/dialpad/DialpadImageButton$OnPressedListener;)V
    .locals 0
    .param p1    # Lcom/android/contacts/dialpad/DialpadImageButton$OnPressedListener;

    iput-object p1, p0, Lcom/android/contacts/dialpad/DialpadImageButton;->mOnPressedListener:Lcom/android/contacts/dialpad/DialpadImageButton$OnPressedListener;

    return-void
.end method

.method public setPressed(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/view/View;->setPressed(Z)V

    iget-object v0, p0, Lcom/android/contacts/dialpad/DialpadImageButton;->mOnPressedListener:Lcom/android/contacts/dialpad/DialpadImageButton$OnPressedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/contacts/dialpad/DialpadImageButton;->mOnPressedListener:Lcom/android/contacts/dialpad/DialpadImageButton$OnPressedListener;

    invoke-interface {v0, p0, p1}, Lcom/android/contacts/dialpad/DialpadImageButton$OnPressedListener;->onPressed(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method
