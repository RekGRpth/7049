.class Lcom/android/contacts/editor/SelectAccountDialogFragment$2;
.super Ljava/lang/Object;
.source "SelectAccountDialogFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/editor/SelectAccountDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/contacts/editor/SelectAccountDialogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$2;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x2

    const-string v1, "SelectAccountDialogFragment"

    const-string v2, "serviceComplete run"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$2;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    invoke-static {v1}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$200(Lcom/android/contacts/editor/SelectAccountDialogFragment;)Lcom/mediatek/CellConnService/CellConnMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/CellConnService/CellConnMgr;->getResult()I

    move-result v0

    const-string v1, "SelectAccountDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serviceComplete result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/mediatek/CellConnService/CellConnMgr;->resultToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SelectAccountDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCellMgr.RESULT_ABORT = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$2;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    invoke-static {v3}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$200(Lcom/android/contacts/editor/SelectAccountDialogFragment;)Lcom/mediatek/CellConnService/CellConnMgr;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SelectAccountDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nRet = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$2;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    invoke-static {v1}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$200(Lcom/android/contacts/editor/SelectAccountDialogFragment;)Lcom/mediatek/CellConnService/CellConnMgr;

    if-ne v4, v0, :cond_0

    iget-object v1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$2;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    iget-object v1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$2;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    invoke-virtual {v1}, Landroid/app/DialogFragment;->dismiss()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$2;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    iget-object v2, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$2;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    invoke-static {v2}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$000(Lcom/android/contacts/editor/SelectAccountDialogFragment;)Lcom/android/contacts/model/AccountWithDataSet;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$300(Lcom/android/contacts/editor/SelectAccountDialogFragment;Lcom/android/contacts/model/AccountWithDataSet;)V

    goto :goto_0
.end method
