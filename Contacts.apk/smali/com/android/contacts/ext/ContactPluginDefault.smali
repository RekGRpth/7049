.class public Lcom/android/contacts/ext/ContactPluginDefault;
.super Ljava/lang/Object;
.source "ContactPluginDefault.java"

# interfaces
.implements Lcom/android/contacts/ext/IOPContactPlugin;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createCallDetailExtension()Lcom/android/contacts/ext/CallDetailExtension;
    .locals 1

    new-instance v0, Lcom/android/contacts/ext/CallDetailExtension;

    invoke-direct {v0}, Lcom/android/contacts/ext/CallDetailExtension;-><init>()V

    return-object v0
.end method

.method public createCallListExtension()Lcom/android/contacts/ext/CallListExtension;
    .locals 1

    new-instance v0, Lcom/android/contacts/ext/CallListExtension;

    invoke-direct {v0}, Lcom/android/contacts/ext/CallListExtension;-><init>()V

    return-object v0
.end method

.method public createContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;
    .locals 1

    new-instance v0, Lcom/android/contacts/ext/ContactAccountExtension;

    invoke-direct {v0}, Lcom/android/contacts/ext/ContactAccountExtension;-><init>()V

    return-object v0
.end method

.method public createContactDetailExtension()Lcom/android/contacts/ext/ContactDetailExtension;
    .locals 1

    new-instance v0, Lcom/android/contacts/ext/ContactDetailExtension;

    invoke-direct {v0}, Lcom/android/contacts/ext/ContactDetailExtension;-><init>()V

    return-object v0
.end method

.method public createContactListExtension()Lcom/android/contacts/ext/ContactListExtension;
    .locals 1

    new-instance v0, Lcom/android/contacts/ext/ContactListExtension;

    invoke-direct {v0}, Lcom/android/contacts/ext/ContactListExtension;-><init>()V

    return-object v0
.end method

.method public createDialPadExtension()Lcom/android/contacts/ext/DialPadExtension;
    .locals 1

    new-instance v0, Lcom/android/contacts/ext/DialPadExtension;

    invoke-direct {v0}, Lcom/android/contacts/ext/DialPadExtension;-><init>()V

    return-object v0
.end method

.method public createDialtactsExtension()Lcom/android/contacts/ext/DialtactsExtension;
    .locals 1

    new-instance v0, Lcom/android/contacts/ext/DialtactsExtension;

    invoke-direct {v0}, Lcom/android/contacts/ext/DialtactsExtension;-><init>()V

    return-object v0
.end method

.method public createQuickContactExtension()Lcom/android/contacts/ext/QuickContactExtension;
    .locals 1

    new-instance v0, Lcom/android/contacts/ext/QuickContactExtension;

    invoke-direct {v0}, Lcom/android/contacts/ext/QuickContactExtension;-><init>()V

    return-object v0
.end method

.method public createSimPickExtension()Lcom/android/contacts/ext/SimPickExtension;
    .locals 1

    new-instance v0, Lcom/android/contacts/ext/SimPickExtension;

    invoke-direct {v0}, Lcom/android/contacts/ext/SimPickExtension;-><init>()V

    return-object v0
.end method

.method public createSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;
    .locals 1

    new-instance v0, Lcom/android/contacts/ext/SpeedDialExtension;

    invoke-direct {v0}, Lcom/android/contacts/ext/SpeedDialExtension;-><init>()V

    return-object v0
.end method
