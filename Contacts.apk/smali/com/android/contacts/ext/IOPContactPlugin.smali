.class public interface abstract Lcom/android/contacts/ext/IOPContactPlugin;
.super Ljava/lang/Object;
.source "IOPContactPlugin.java"


# virtual methods
.method public abstract createCallDetailExtension()Lcom/android/contacts/ext/CallDetailExtension;
.end method

.method public abstract createCallListExtension()Lcom/android/contacts/ext/CallListExtension;
.end method

.method public abstract createContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;
.end method

.method public abstract createContactDetailExtension()Lcom/android/contacts/ext/ContactDetailExtension;
.end method

.method public abstract createContactListExtension()Lcom/android/contacts/ext/ContactListExtension;
.end method

.method public abstract createDialPadExtension()Lcom/android/contacts/ext/DialPadExtension;
.end method

.method public abstract createDialtactsExtension()Lcom/android/contacts/ext/DialtactsExtension;
.end method

.method public abstract createQuickContactExtension()Lcom/android/contacts/ext/QuickContactExtension;
.end method

.method public abstract createSimPickExtension()Lcom/android/contacts/ext/SimPickExtension;
.end method

.method public abstract createSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;
.end method
