.class public Lcom/android/contacts/ext/SpeedDialExtension;
.super Ljava/lang/Object;
.source "SpeedDialExtension.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SpeedDialExtension"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearPrefStateIfNecessary()Z
    .locals 2

    const-string v0, "SpeedDialExtension"

    const-string v1, "SpeedDialManageActivity: [clearPrefStateIfNecessary]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public needClearPreState()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public needClearSharedPreferences()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public setAddPosition(IZ)I
    .locals 0
    .param p1    # I
    .param p2    # Z

    return p1
.end method

.method public setView(Landroid/view/View;IZI)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Z
    .param p4    # I

    return-void
.end method

.method public showSpeedInputDialog()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
