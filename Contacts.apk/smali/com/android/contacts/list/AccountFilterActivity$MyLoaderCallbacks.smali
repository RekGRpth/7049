.class Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;
.super Ljava/lang/Object;
.source "AccountFilterActivity.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/list/AccountFilterActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/android/contacts/list/ContactListFilter;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/list/AccountFilterActivity;


# direct methods
.method private constructor <init>(Lcom/android/contacts/list/AccountFilterActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/contacts/list/AccountFilterActivity;Lcom/android/contacts/list/AccountFilterActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/contacts/list/AccountFilterActivity;
    .param p2    # Lcom/android/contacts/list/AccountFilterActivity$1;

    invoke-direct {p0, p1}, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;-><init>(Lcom/android/contacts/list/AccountFilterActivity;)V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/android/contacts/list/ContactListFilter;",
            ">;>;"
        }
    .end annotation

    invoke-static {}, Lcom/android/contacts/list/AccountFilterActivity;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onCreateLoader"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/contacts/list/AccountFilterActivity;->isFinished:Z

    iget-object v0, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    invoke-static {v0}, Lcom/android/contacts/list/AccountFilterActivity;->access$300(Lcom/android/contacts/list/AccountFilterActivity;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    invoke-static {v0}, Lcom/android/contacts/list/AccountFilterActivity;->access$400(Lcom/android/contacts/list/AccountFilterActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    invoke-static {v1}, Lcom/android/contacts/list/AccountFilterActivity;->access$400(Lcom/android/contacts/list/AccountFilterActivity;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x4ce

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    new-instance v0, Lcom/android/contacts/list/AccountFilterActivity$FilterLoader;

    iget-object v1, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    invoke-direct {v0, v1}, Lcom/android/contacts/list/AccountFilterActivity$FilterLoader;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->onLoadFinished(Landroid/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/android/contacts/list/ContactListFilter;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lcom/android/contacts/list/ContactListFilter;",
            ">;)V"
        }
    .end annotation

    const/16 v3, 0x8

    invoke-static {}, Lcom/android/contacts/list/AccountFilterActivity;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onLoadFinished"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/contacts/list/AccountFilterActivity;->isFinished:Z

    iget-object v0, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    invoke-static {v0}, Lcom/android/contacts/list/AccountFilterActivity;->access$300(Lcom/android/contacts/list/AccountFilterActivity;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    const v2, 0x10a0001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    invoke-static {v0}, Lcom/android/contacts/list/AccountFilterActivity;->access$300(Lcom/android/contacts/list/AccountFilterActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    invoke-static {v0}, Lcom/android/contacts/list/AccountFilterActivity;->access$500(Lcom/android/contacts/list/AccountFilterActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    invoke-static {v0}, Lcom/android/contacts/list/AccountFilterActivity;->access$600(Lcom/android/contacts/list/AccountFilterActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    if-nez p2, :cond_0

    invoke-static {}, Lcom/android/contacts/list/AccountFilterActivity;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to load filters"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    invoke-static {v0}, Lcom/android/contacts/list/AccountFilterActivity;->access$800(Lcom/android/contacts/list/AccountFilterActivity;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/android/contacts/list/AccountFilterActivity$FilterListAdapter;

    iget-object v2, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    iget-object v3, p0, Lcom/android/contacts/list/AccountFilterActivity$MyLoaderCallbacks;->this$0:Lcom/android/contacts/list/AccountFilterActivity;

    invoke-static {v3}, Lcom/android/contacts/list/AccountFilterActivity;->access$700(Lcom/android/contacts/list/AccountFilterActivity;)Lcom/android/contacts/list/ContactListFilter;

    move-result-object v3

    invoke-direct {v1, v2, p2, v3}, Lcom/android/contacts/list/AccountFilterActivity$FilterListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/android/contacts/list/ContactListFilter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/android/contacts/list/ContactListFilter;",
            ">;>;)V"
        }
    .end annotation

    return-void
.end method
