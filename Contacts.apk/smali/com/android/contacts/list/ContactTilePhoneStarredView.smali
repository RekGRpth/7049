.class public Lcom/android/contacts/list/ContactTilePhoneStarredView;
.super Lcom/android/contacts/list/ContactTileView;
.source "ContactTilePhoneStarredView.java"


# instance fields
.field private mSecondaryButton:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/contacts/list/ContactTileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected getApproximateImageSize()I
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/list/ContactTileView;->mListener:Lcom/android/contacts/list/ContactTileView$Listener;

    invoke-interface {v0}, Lcom/android/contacts/list/ContactTileView$Listener;->getApproximateTileWidth()I

    move-result v0

    return v0
.end method

.method protected isDarkTheme()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Lcom/android/contacts/list/ContactTileView;->onFinishInflate()V

    const v0, 0x7f0700f5

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/contacts/list/ContactTilePhoneStarredView;->mSecondaryButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/contacts/list/ContactTilePhoneStarredView;->mSecondaryButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/contacts/list/ContactTilePhoneStarredView$1;

    invoke-direct {v1, p0}, Lcom/android/contacts/list/ContactTilePhoneStarredView$1;-><init>(Lcom/android/contacts/list/ContactTilePhoneStarredView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
