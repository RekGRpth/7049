.class Lcom/android/contacts/list/PhoneFavoriteFragment$ScrollListener;
.super Ljava/lang/Object;
.source "PhoneFavoriteFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/list/PhoneFavoriteFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScrollListener"
.end annotation


# instance fields
.field private mShouldShowFastScroller:Z

.field final synthetic this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;


# direct methods
.method private constructor <init>(Lcom/android/contacts/list/PhoneFavoriteFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ScrollListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/contacts/list/PhoneFavoriteFragment;Lcom/android/contacts/list/PhoneFavoriteFragment$1;)V
    .locals 0
    .param p1    # Lcom/android/contacts/list/PhoneFavoriteFragment;
    .param p2    # Lcom/android/contacts/list/PhoneFavoriteFragment$1;

    invoke-direct {p0, p1}, Lcom/android/contacts/list/PhoneFavoriteFragment$ScrollListener;-><init>(Lcom/android/contacts/list/PhoneFavoriteFragment;)V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ScrollListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v1}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1500(Lcom/android/contacts/list/PhoneFavoriteFragment;)Lcom/android/contacts/list/PhoneFavoriteMergedAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/android/contacts/list/PhoneFavoriteMergedAdapter;->shouldShowFirstScroller(I)Z

    move-result v0

    iget-boolean v1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ScrollListener;->mShouldShowFastScroller:Z

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ScrollListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v1}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1600(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVerticalScrollBarEnabled(Z)V

    iget-object v1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ScrollListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v1}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1600(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    iget-object v1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ScrollListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v1}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1600(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->setFastScrollAlwaysVisible(Z)V

    iput-boolean v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ScrollListener;->mShouldShowFastScroller:Z

    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    return-void
.end method
