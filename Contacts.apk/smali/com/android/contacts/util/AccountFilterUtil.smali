.class public Lcom/android/contacts/util/AccountFilterUtil;
.super Ljava/lang/Object;
.source "AccountFilterUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/contacts/util/AccountFilterUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/contacts/util/AccountFilterUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAccountDisplayNameByAccount(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move-object v1, v0

    :goto_0
    return-object v1

    :cond_1
    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v4

    if-nez v4, :cond_2

    move-object v1, v0

    goto :goto_0

    :cond_2
    invoke-static {v4}, Lcom/android/contacts/model/AccountTypeManager;->getInstance(Landroid/content/Context;)Lcom/android/contacts/model/AccountTypeManager;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/android/contacts/model/AccountTypeManager;->getAccounts(Z)Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_3

    move-object v1, v0

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/contacts/model/AccountWithDataSet;

    instance-of v6, v3, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    if-eqz v6, :cond_4

    iget-object v6, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {p0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    check-cast v3, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v3}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method public static handleAccountFilterResult(Lcom/android/contacts/list/ContactListFilterController;ILandroid/content/Intent;)V
    .locals 3
    .param p0    # Lcom/android/contacts/list/ContactListFilterController;
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    const-string v1, "contactListFilter"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/contacts/list/ContactListFilter;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, v0, Lcom/android/contacts/list/ContactListFilter;->filterType:I

    const/4 v2, -0x3

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactListFilterController;->selectCustomFilter()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/contacts/list/ContactListFilterController;->setContactListFilter(Lcom/android/contacts/list/ContactListFilter;Z)V

    goto :goto_0
.end method

.method public static startAccountFilterActivityForResult(Landroid/app/Activity;ILcom/android/contacts/list/ContactListFilter;)V
    .locals 2
    .param p0    # Landroid/app/Activity;
    .param p1    # I
    .param p2    # Lcom/android/contacts/list/ContactListFilter;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/contacts/list/AccountFilterActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "currentFilter"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public static startAccountFilterActivityForResult(Landroid/app/Fragment;ILcom/android/contacts/list/ContactListFilter;)V
    .locals 4
    .param p0    # Landroid/app/Fragment;
    .param p1    # I
    .param p2    # Lcom/android/contacts/list/ContactListFilter;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/contacts/list/AccountFilterActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "currentFilter"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v1, p1}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    sget-object v2, Lcom/android/contacts/util/AccountFilterUtil;->TAG:Ljava/lang/String;

    const-string v3, "getActivity() returned null. Ignored"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static updateAccountFilterTitle(Landroid/view/View;Lcom/android/contacts/list/ContactListFilter;ZZ)Z
    .locals 9
    .param p0    # Landroid/view/View;
    .param p1    # Lcom/android/contacts/list/ContactListFilter;
    .param p2    # Z
    .param p3    # Z

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v6, 0x7f070030

    invoke-virtual {p0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    const/4 v5, 0x0

    if-eqz p1, :cond_b

    const/4 v1, 0x0

    iget-object v6, p1, Lcom/android/contacts/list/ContactListFilter;->accountType:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/contacts/list/ContactListFilter;->accountName:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/android/contacts/util/AccountFilterUtil;->getAccountDisplayNameByAccount(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/android/contacts/list/ContactListFilter;->accountName:Ljava/lang/String;

    :cond_1
    if-eqz p3, :cond_6

    iget v6, p1, Lcom/android/contacts/list/ContactListFilter;->filterType:I

    const/4 v7, -0x2

    if-ne v6, v7, :cond_3

    if-eqz p2, :cond_2

    const v6, 0x7f0c0281

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(I)V

    const/4 v5, 0x1

    :cond_2
    :goto_0
    return v5

    :cond_3
    iget v6, p1, Lcom/android/contacts/list/ContactListFilter;->filterType:I

    if-nez v6, :cond_4

    const v6, 0x7f0c0152

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v5, 0x1

    goto :goto_0

    :cond_4
    iget v6, p1, Lcom/android/contacts/list/ContactListFilter;->filterType:I

    const/4 v7, -0x3

    if-ne v6, v7, :cond_5

    const v6, 0x7f0c0154

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(I)V

    const/4 v5, 0x1

    goto :goto_0

    :cond_5
    sget-object v6, Lcom/android/contacts/util/AccountFilterUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Filter type \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Lcom/android/contacts/list/ContactListFilter;->filterType:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\" isn\'t expected."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_6
    iget v6, p1, Lcom/android/contacts/list/ContactListFilter;->filterType:I

    const/4 v7, -0x2

    if-ne v6, v7, :cond_7

    if-eqz p2, :cond_2

    const v6, 0x7f0c027d

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(I)V

    const/4 v5, 0x1

    goto :goto_0

    :cond_7
    iget v6, p1, Lcom/android/contacts/list/ContactListFilter;->filterType:I

    if-nez v6, :cond_8

    const v6, 0x7f0c0152

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v5, 0x1

    goto :goto_0

    :cond_8
    iget v6, p1, Lcom/android/contacts/list/ContactListFilter;->filterType:I

    const/4 v7, -0x3

    if-ne v6, v7, :cond_9

    const v6, 0x7f0c0154

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(I)V

    const/4 v5, 0x1

    goto :goto_0

    :cond_9
    iget v6, p1, Lcom/android/contacts/list/ContactListFilter;->filterType:I

    const/4 v7, -0x6

    if-ne v6, v7, :cond_a

    const v6, 0x7f0c0153

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(I)V

    const/4 v5, 0x1

    goto/16 :goto_0

    :cond_a
    sget-object v6, Lcom/android/contacts/util/AccountFilterUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Filter type \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Lcom/android/contacts/list/ContactListFilter;->filterType:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\" isn\'t expected."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_b
    sget-object v6, Lcom/android/contacts/util/AccountFilterUtil;->TAG:Ljava/lang/String;

    const-string v7, "Filter is null."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static updateAccountFilterTitleForPeople(Landroid/view/View;Lcom/android/contacts/list/ContactListFilter;Z)Z
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # Lcom/android/contacts/list/ContactListFilter;
    .param p2    # Z

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/android/contacts/util/AccountFilterUtil;->updateAccountFilterTitle(Landroid/view/View;Lcom/android/contacts/list/ContactListFilter;ZZ)Z

    move-result v0

    return v0
.end method

.method public static updateAccountFilterTitleForPhone(Landroid/view/View;Lcom/android/contacts/list/ContactListFilter;Z)Z
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # Lcom/android/contacts/list/ContactListFilter;
    .param p2    # Z

    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/android/contacts/util/AccountFilterUtil;->updateAccountFilterTitle(Landroid/view/View;Lcom/android/contacts/list/ContactListFilter;ZZ)Z

    move-result v0

    return v0
.end method
