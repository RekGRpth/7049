.class public Lcom/mediatek/contacts/CallOptionHandler;
.super Ljava/lang/Object;
.source "CallOptionHandler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Lcom/mediatek/contacts/CallOptionHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/CallOptionHandler$OnHandleCallOption;
    }
.end annotation


# static fields
.field private static final OUTGOING_CALL_RECEIVER:Ljava/lang/String; = "com.mediatek.phone.OutgoingCallReceiver"

.field private static final PACKAGE:Ljava/lang/String; = "com.android.phone"

.field private static final TAG:Ljava/lang/String; = "CallOptionHandler"


# instance fields
.field protected mApp:Lcom/android/contacts/ContactsApplication;

.field protected mAssociateSimMissingArgs:Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;

.field protected mAssociateSimMissingClicked:Z

.field protected mCallOptionHelper:Lcom/mediatek/contacts/CallOptionHelper;

.field protected mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

.field protected mClicked:Z

.field protected mContext:Landroid/content/Context;

.field protected mDialogs:[Landroid/app/Dialog;

.field protected mIntent:Landroid/content/Intent;

.field protected mNumber:Ljava/lang/String;

.field protected mOnHandleCallOption:Lcom/mediatek/contacts/CallOptionHandler$OnHandleCallOption;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field protected mReason:I

.field private mRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [Landroid/app/Dialog;

    iput-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mReason:I

    new-instance v0, Lcom/mediatek/contacts/CallOptionHandler$4;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/CallOptionHandler$4;-><init>(Lcom/mediatek/contacts/CallOptionHandler;)V

    iput-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/mediatek/contacts/CallOptionHelper;->getInstance(Landroid/content/Context;)Lcom/mediatek/contacts/CallOptionHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mCallOptionHelper:Lcom/mediatek/contacts/CallOptionHelper;

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mCallOptionHelper:Lcom/mediatek/contacts/CallOptionHelper;

    invoke-virtual {v0, p0}, Lcom/mediatek/contacts/CallOptionHelper;->setCallback(Lcom/mediatek/contacts/CallOptionHelper$Callback;)V

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mApp:Lcom/android/contacts/ContactsApplication;

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mApp:Lcom/android/contacts/ContactsApplication;

    iget-object v0, v0, Lcom/android/contacts/ContactsApplication;->cellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    iput-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/CallOptionHandler;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/CallOptionHandler;

    invoke-direct {p0}, Lcom/mediatek/contacts/CallOptionHandler;->dismissProgressIndication()V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/CallOptionHandler;II)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/CallOptionHandler;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/contacts/CallOptionHandler;->afterCheckSIMStatus(II)Z

    move-result v0

    return v0
.end method

.method private afterCheckSIMStatus(II)Z
    .locals 15
    .param p1    # I
    .param p2    # I

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "afterCheckSIMStatus, result = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " slot = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    const/4 v11, 0x4

    move/from16 v0, p1

    if-eq v0, v11, :cond_0

    const/4 v11, 0x1

    :goto_0
    return v11

    :cond_0
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v11

    invoke-virtual {v11}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getInsertedSimCount()I

    move-result v11

    if-nez v11, :cond_1

    const/4 v5, 0x1

    :goto_1
    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v12, "com.android.phone.extra.video"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    if-nez v11, :cond_3

    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v12, "com.android.phone.extra.ip"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    if-eqz v11, :cond_3

    if-nez v5, :cond_3

    move/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/CallOptionHandler;->queryIPPrefix(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    new-instance v3, Landroid/content/Intent;

    const-string v11, "com.android.phone.MAIN"

    invoke-direct {v3, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v11, "com.android.phone"

    const-string v12, "com.mediatek.settings.CallSettings"

    invoke-virtual {v3, v11, v12}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v11

    move/from16 v0, p2

    invoke-virtual {v11, v0}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimInfoBySlot(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v7

    const-string v11, "simId"

    iget-wide v12, v7, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    invoke-virtual {v3, v11, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v11, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    const v12, 0x7f0c0041

    const/4 v13, 0x0

    invoke-static {v11, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V

    const/4 v11, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    :cond_2
    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mNumber:Ljava/lang/String;

    invoke-virtual {v11, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v12, "android.phone.extra.ACTUAL_NUMBER_TO_DIAL"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/mediatek/contacts/CallOptionHandler;->mNumber:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    const-string v11, "voicemail:"

    iget-object v12, p0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    invoke-virtual {v12}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    const-string v12, "phone"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/TelephonyManager;

    move/from16 v0, p2

    invoke-virtual {v8, v0}, Landroid/telephony/TelephonyManager;->getVoiceMailNumberGemini(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    new-instance v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;

    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mCallOptionHelper:Lcom/mediatek/contacts/CallOptionHelper;

    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v2, v11}, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;-><init>(Lcom/mediatek/contacts/CallOptionHelper;)V

    const/4 v11, 0x7

    iput v11, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    const/4 v11, 0x2

    iput v11, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->type:I

    move/from16 v0, p2

    int-to-long v11, v0

    iput-wide v11, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->id:J

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/CallOptionHandler;->onMakeCall(Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;)V

    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_4
    move/from16 v0, p2

    invoke-virtual {v8, v0}, Landroid/telephony/TelephonyManager;->getVoiceMailNumberGemini(I)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v12, "android.phone.extra.ACTUAL_NUMBER_TO_DIAL"

    invoke-virtual {v11, v12, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_5
    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0, v11}, Lcom/mediatek/contacts/CallOptionHandler;->getInitialNumber(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v6

    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v12, "com.android.phone.extra.video"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    if-eqz v11, :cond_6

    const/4 v9, 0x1

    :goto_2
    move/from16 v0, p2

    invoke-direct {p0, v6, v9, v0}, Lcom/mediatek/contacts/CallOptionHandler;->newCallBroadcastIntent(Ljava/lang/String;II)Landroid/content/Intent;

    move-result-object v1

    iget-object v11, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v11, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v11, 0x1

    goto/16 :goto_0

    :cond_6
    const/4 v9, 0x2

    goto :goto_2
.end method

.method private dismissProgressIndication()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method private isRoamingNeeded(I)Z
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isRoamingNeeded slot = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isRoamingNeeded = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "gsm.roaming.indicator.needed.2"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    const-string v0, "gsm.roaming.indicator.needed.2"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isRoamingNeeded = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "gsm.roaming.indicator.needed"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    const-string v0, "gsm.roaming.indicator.needed"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private needToCheckSIMStatus(I)Z
    .locals 6
    .param p1    # I

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v4, "phone"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-ltz p1, :cond_0

    :try_start_0
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    const-string v4, "the sim not insert, bail out!"

    invoke-virtual {p0, v4}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return v2

    :cond_2
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_3

    move v2, v3

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    :cond_3
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_4

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/CallOptionHandler;->roamingRequest(I)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_4
    move v2, v3

    goto :goto_0
.end method

.method private newCallBroadcastIntent(Ljava/lang/String;II)Landroid/content/Intent;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.phone.OutgoingCallReceiver"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.phone"

    const-string v2, "com.mediatek.phone.OutgoingCallReceiver"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-ne p2, v3, :cond_0

    const-string v1, "com.android.phone.extra.video"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    if-nez p2, :cond_1

    const-string v1, "sip"

    invoke-static {v1, p1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :goto_0
    const-string v1, "com.android.phone.extra.slot"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0

    :cond_1
    const-string v1, "tel"

    invoke-static {v1, p1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private queryIPPrefix(I)Ljava/lang/String;
    .locals 6
    .param p1    # I

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimInfoBySlot(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ipprefix"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, v3, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "queryIPPrefix, ipPrefix = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    return-object v1
.end method

.method private roamingRequest(I)Z
    .locals 6
    .param p1    # I

    const/4 v2, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "roamingRequest slot = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/telephony/TelephonyManager;->isNetworkRoamingGemini(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "roamingRequest slot = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is roaming"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "roaming_reminder_mode_setting"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/CallOptionHandler;->isRoamingNeeded(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v2, "roamingRequest reminder once and need to indicate"

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "roamingRequest slot = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is not roaming"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    move v1, v2

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "roaming_reminder_mode_setting"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v1, v3, :cond_2

    const-string v2, "roamingRequest reminder always"

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v1, "roamingRequest result = false"

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    move v1, v2

    goto :goto_0
.end method

.method private showProgressIndication()V
    .locals 3

    const-string v0, "showProgressIndication(searching network message )"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/contacts/CallOptionHandler;->dismissProgressIndication()V

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method


# virtual methods
.method public getInitialNumber(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/Intent;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getInitialNumber(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    const-string v2, "android.phone.extra.ACTUAL_NUMBER_TO_DIAL"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "android.phone.extra.ACTUAL_NUMBER_TO_DIAL"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-static {p1, v2}, Landroid/telephony/PhoneNumberUtils;->getNumberFromIntent(Landroid/content/Intent;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected handleCallOptionComplete()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mOnHandleCallOption:Lcom/mediatek/contacts/CallOptionHandler$OnHandleCallOption;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mOnHandleCallOption:Lcom/mediatek/contacts/CallOptionHandler$OnHandleCallOption;

    iget v1, p0, Lcom/mediatek/contacts/CallOptionHandler;->mReason:I

    invoke-interface {v0, v1}, Lcom/mediatek/contacts/CallOptionHandler$OnHandleCallOption;->onHandleCallOption(I)V

    :cond_0
    return-void
.end method

.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "CallOptionHandler"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const-string v7, "+CallOptionHandler.onClick"

    invoke-static {v7}, Lcom/mediatek/contacts/Profiler;->trace(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onClick, dialog = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " which = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    const/4 v8, 0x5

    aget-object v7, v7, v8

    if-ne p1, v7, :cond_6

    move-object v0, p1

    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    invoke-interface {v4, p2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onClick, slot = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    const/4 v7, -0x2

    if-ne v6, v7, :cond_3

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v8, "com.android.phone.extra.ip"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    const v8, 0x7f0c0042

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    :goto_0
    invoke-virtual {p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    :cond_0
    :goto_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mClicked:Z

    :cond_1
    :goto_2
    const-string v7, "-CallOptionHandler.onClick"

    invoke-static {v7}, Lcom/mediatek/contacts/Profiler;->trace(Ljava/lang/String;)V

    return-void

    :cond_2
    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mNumber:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/contacts/CallOptionHandler;->newCallBroadcastIntent(Ljava/lang/String;II)Landroid/content/Intent;

    move-result-object v3

    new-instance v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mCallOptionHelper:Lcom/mediatek/contacts/CallOptionHelper;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v2, v7}, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;-><init>(Lcom/mediatek/contacts/CallOptionHelper;)V

    const/4 v7, 0x0

    iput v7, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    const/4 v7, 0x0

    iput v7, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->type:I

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/CallOptionHandler;->onMakeCall(Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;)V

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v8, "com.android.phone.extra.slot"

    invoke-virtual {v7, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-direct {p0, v6}, Lcom/mediatek/contacts/CallOptionHandler;->needToCheckSIMStatus(I)Z

    move-result v7

    if-eqz v7, :cond_5

    if-ltz v6, :cond_4

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    const/16 v8, 0x132

    iget-object v9, p0, Lcom/mediatek/contacts/CallOptionHandler;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v7, v6, v8, v9}, Lcom/mediatek/CellConnService/CellConnMgr;->handleCellConn(IILjava/lang/Runnable;)I

    move-result v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    const/4 v7, 0x1

    if-ne v5, v7, :cond_0

    invoke-direct {p0}, Lcom/mediatek/contacts/CallOptionHandler;->showProgressIndication()V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_1

    :cond_5
    const/4 v7, 0x4

    invoke-direct {p0, v7, v6}, Lcom/mediatek/contacts/CallOptionHandler;->afterCheckSIMStatus(II)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_1

    :cond_6
    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    const/4 v8, 0x6

    aget-object v7, v7, v8

    if-ne p1, v7, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/app/AlertDialog;

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;

    if-eqz v7, :cond_8

    const/4 v7, -0x1

    if-ne p2, v7, :cond_c

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;

    iget-object v7, v7, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->viaSimInfo:Landroid/provider/Telephony$SIMInfo;

    if-eqz v7, :cond_b

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;

    iget-object v7, v7, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->viaSimInfo:Landroid/provider/Telephony$SIMInfo;

    iget v6, v7, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v8, "com.android.phone.extra.slot"

    invoke-virtual {v7, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-direct {p0, v6}, Lcom/mediatek/contacts/CallOptionHandler;->needToCheckSIMStatus(I)Z

    move-result v7

    if-eqz v7, :cond_a

    if-ltz v6, :cond_9

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    const/16 v8, 0x132

    iget-object v9, p0, Lcom/mediatek/contacts/CallOptionHandler;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v7, v6, v8, v9}, Lcom/mediatek/CellConnService/CellConnMgr;->handleCellConn(IILjava/lang/Runnable;)I

    move-result v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    const/4 v7, 0x1

    if-ne v5, v7, :cond_7

    invoke-direct {p0}, Lcom/mediatek/contacts/CallOptionHandler;->showProgressIndication()V

    :cond_7
    :goto_3
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mAssociateSimMissingClicked:Z

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;

    :cond_8
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto/16 :goto_2

    :cond_9
    invoke-virtual {p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_3

    :cond_a
    const/4 v7, 0x4

    invoke-direct {p0, v7, v6}, Lcom/mediatek/contacts/CallOptionHandler;->afterCheckSIMStatus(II)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_3

    :cond_b
    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mNumber:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {p0, v7, v8, v9}, Lcom/mediatek/contacts/CallOptionHandler;->newCallBroadcastIntent(Ljava/lang/String;II)Landroid/content/Intent;

    move-result-object v3

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_3

    :cond_c
    const/4 v7, -0x2

    if-ne p2, v7, :cond_7

    new-instance v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mCallOptionHelper:Lcom/mediatek/contacts/CallOptionHelper;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v2, v7}, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;-><init>(Lcom/mediatek/contacts/CallOptionHelper;)V

    iget-object v7, p0, Lcom/mediatek/contacts/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;

    iget-wide v7, v7, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->suggested:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    const/4 v7, 0x5

    iput v7, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/CallOptionHandler;->onMakeCall(Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;)V

    goto :goto_3
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;

    const/4 v2, 0x5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDismiss, mClicked = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mediatek/contacts/CallOptionHandler;->mClicked:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    aget-object v0, v0, v2

    if-ne p1, v0, :cond_2

    iget-boolean v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mClicked:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    const/4 v1, 0x0

    aput-object v1, v0, v2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_3

    iget-boolean v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mAssociateSimMissingClicked:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_0
.end method

.method public onMakeCall(Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;)V
    .locals 34
    .param p1    # Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onMakeCall, reason = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " args = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget v2, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mReason:I

    move-object/from16 v0, p1

    iget v2, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/16 v32, -0x1

    move-object/from16 v0, p1

    iget v2, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->type:I

    if-nez v2, :cond_2

    const-string v2, "voicemail:"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v4, "com.android.phone.extra.ip"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    const v4, 0x7f0c0042

    const/4 v5, 0x0

    invoke-static {v2, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mNumber:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v5}, Lcom/mediatek/contacts/CallOptionHandler;->newCallBroadcastIntent(Ljava/lang/String;II)Landroid/content/Intent;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v15}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    :cond_2
    move-object/from16 v0, p1

    iget v2, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->type:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_4

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->id:J

    long-to-int v0, v4

    move/from16 v32, v0

    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v4, "com.android.phone.extra.slot"

    move/from16 v0, v32

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/CallOptionHandler;->needToCheckSIMStatus(I)Z

    move-result v2

    if-eqz v2, :cond_6

    if-ltz v32, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    const/16 v4, 0x132

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/CallOptionHandler;->mRunnable:Ljava/lang/Runnable;

    move/from16 v0, v32

    invoke-virtual {v2, v0, v4, v5}, Lcom/mediatek/CellConnService/CellConnMgr;->handleCellConn(IILjava/lang/Runnable;)I

    move-result v31

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    const/4 v2, 0x1

    move/from16 v0, v31

    if-ne v0, v2, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/contacts/CallOptionHandler;->showProgressIndication()V

    goto/16 :goto_0

    :cond_4
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v2

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->id:J

    long-to-int v4, v4

    invoke-virtual {v2, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v32

    const/4 v2, -0x1

    move/from16 v0, v32

    if-ne v0, v2, :cond_3

    const/16 v32, 0x0

    goto :goto_2

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-direct {v0, v2, v1}, Lcom/mediatek/contacts/CallOptionHandler;->afterCheckSIMStatus(II)Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    goto/16 :goto_0

    :pswitch_2
    new-instance v16, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v13, 0x1

    const-string v2, "phone"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v28

    if-eqz v28, :cond_7

    const/4 v2, 0x0

    :try_start_0
    move-object/from16 v0, v28

    invoke-interface {v0, v2}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x1

    move-object/from16 v0, v28

    invoke-interface {v0, v2}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_7

    const/4 v13, 0x0

    :cond_7
    :goto_3
    if-nez v13, :cond_8

    const v2, 0x7f0c00c9

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x7f0c0040

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    const/4 v2, 0x0

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :goto_4
    invoke-virtual/range {v16 .. v16}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    aput-object v20, v2, v4

    invoke-virtual/range {v20 .. v20}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :catch_0
    move-exception v25

    const/4 v13, 0x0

    goto :goto_3

    :cond_8
    const v2, 0x7f0c00c9

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x7f0c003c

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1040009

    const/4 v2, 0x0

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x1040013

    new-instance v5, Lcom/mediatek/contacts/CallOptionHandler$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/mediatek/contacts/CallOptionHandler$1;-><init>(Lcom/mediatek/contacts/CallOptionHandler;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_4

    :pswitch_3
    new-instance v18, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0c00c9

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x7f0c00ca

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1040009

    const/4 v2, 0x0

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x1040013

    new-instance v5, Lcom/mediatek/contacts/CallOptionHandler$2;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/mediatek/contacts/CallOptionHandler$2;-><init>(Lcom/mediatek/contacts/CallOptionHandler;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual/range {v18 .. v18}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    aput-object v24, v2, v4

    invoke-virtual/range {v24 .. v24}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :pswitch_4
    const-string v2, "voicemail:"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_9

    const/4 v6, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v29

    if-eqz v26, :cond_a

    const v2, 0x7f0c016d

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    aget-object v2, v2, v4

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "original SIM selection is showing, bail out..."

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const/4 v6, 0x0

    goto :goto_5

    :cond_a
    const v2, 0x7f0c003f

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_6

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v7, p0

    invoke-static/range {v2 .. v7}, Lcom/mediatek/contacts/widget/SimPickerDialog;->create(Landroid/content/Context;Ljava/lang/String;JZLandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    aput-object v22, v2, v4

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mClicked:Z

    invoke-virtual/range {v22 .. v22}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    check-cast v11, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;

    new-instance v19, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->id:J

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v2

    long-to-int v4, v8

    invoke-virtual {v2, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimInfoById(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v10

    const-string v12, ""

    if-eqz v10, :cond_c

    iget-object v12, v10, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    :cond_c
    iget-object v2, v11, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->viaSimInfo:Landroid/provider/Telephony$SIMInfo;

    if-eqz v2, :cond_e

    iget-object v2, v11, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->viaSimInfo:Landroid/provider/Telephony$SIMInfo;

    iget-object v0, v2, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    move-object/from16 v33, v0

    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0c003d

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v12, v5, v7

    const/4 v7, 0x1

    aput-object v33, v5, v7

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->number:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x1040013

    move-object/from16 v0, p0

    invoke-virtual {v2, v4, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget v2, v11, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->type:I

    if-nez v2, :cond_f

    const/high16 v2, 0x1040000

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_d
    :goto_8
    invoke-virtual/range {v19 .. v19}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    aput-object v23, v2, v4

    invoke-virtual/range {v23 .. v23}, Landroid/app/Dialog;->show()V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/mediatek/contacts/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mAssociateSimMissingClicked:Z

    goto/16 :goto_0

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0c0131

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v33

    goto :goto_7

    :cond_f
    iget v2, v11, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->type:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_d

    const v2, 0x7f0c003e

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_8

    :pswitch_6
    new-instance v17, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0c00c6

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x7f0c00c7

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1040009

    const/4 v2, 0x0

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x7f0c00c8

    new-instance v5, Lcom/mediatek/contacts/CallOptionHandler$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v5, v0, v1}, Lcom/mediatek/contacts/CallOptionHandler$3;-><init>(Lcom/mediatek/contacts/CallOptionHandler;Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    aput-object v21, v2, v4

    invoke-virtual/range {v21 .. v21}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onStop()V
    .locals 4

    iget-object v0, p0, Lcom/mediatek/contacts/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/contacts/CallOptionHandler;->dismissProgressIndication()V

    return-void
.end method

.method public setOnHandleCallOption(Lcom/mediatek/contacts/CallOptionHandler$OnHandleCallOption;)V
    .locals 0
    .param p1    # Lcom/mediatek/contacts/CallOptionHandler$OnHandleCallOption;

    iput-object p1, p0, Lcom/mediatek/contacts/CallOptionHandler;->mOnHandleCallOption:Lcom/mediatek/contacts/CallOptionHandler$OnHandleCallOption;

    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/4 v4, -0x1

    const-string v2, "+CallOptionHandler.StartActivity"

    invoke-static {v2}, Lcom/mediatek/contacts/Profiler;->trace(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startActivity, intent = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mediatek/contacts/CallOptionHandler;->mIntent:Landroid/content/Intent;

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-static {p1, v2}, Landroid/telephony/PhoneNumberUtils;->getNumberFromIntent(Landroid/content/Intent;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/CallOptionHandler;->mNumber:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/contacts/CallOptionHandler;->mNumber:Ljava/lang/String;

    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.android.phone.extra.slot"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v4, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/mediatek/contacts/CallOptionHandler;->mNumber:Ljava/lang/String;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/mediatek/contacts/CallOptionHandler;->newCallBroadcastIntent(Ljava/lang/String;II)Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/contacts/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/mediatek/contacts/CallOptionHandler;->mOnHandleCallOption:Lcom/mediatek/contacts/CallOptionHandler$OnHandleCallOption;

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/contacts/CallOptionHandler;->handleCallOptionComplete()V

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/CallOptionHandler;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/contacts/CallOptionHandler;->mCallOptionHelper:Lcom/mediatek/contacts/CallOptionHelper;

    invoke-virtual {v2, p1}, Lcom/mediatek/contacts/CallOptionHelper;->makeCall(Landroid/content/Intent;)V

    const-string v2, "-CallOptionHandler.StartActivity"

    invoke-static {v2}, Lcom/mediatek/contacts/Profiler;->trace(Ljava/lang/String;)V

    goto :goto_1
.end method
