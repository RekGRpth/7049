.class public Lcom/mediatek/contacts/CallOptionHelper;
.super Ljava/lang/Object;
.source "CallOptionHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/CallOptionHelper$Callback;,
        Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;,
        Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;
    }
.end annotation


# static fields
.field public static final DIAL_TYPE_SIP:I = 0x0

.field public static final DIAL_TYPE_VIDEO:I = 0x1

.field public static final DIAL_TYPE_VOICE:I = 0x2

.field public static final MAKE_CALL_REASON_3G_SERVICE_OFF:I = 0x1

.field public static final MAKE_CALL_REASON_ASK:I = 0x5

.field public static final MAKE_CALL_REASON_ASSOCIATE_MISSING:I = 0x6

.field public static final MAKE_CALL_REASON_MAX:I = 0x7

.field public static final MAKE_CALL_REASON_MISSING_VOICE_MAIL_NUMBER:I = 0x7

.field public static final MAKE_CALL_REASON_OK:I = 0x0

.field public static final MAKE_CALL_REASON_SIP_DISABLED:I = 0x2

.field public static final MAKE_CALL_REASON_SIP_NO_INTERNET:I = 0x3

.field public static final MAKE_CALL_REASON_SIP_START_SETTINGS:I = 0x4

.field private static final TAG:Ljava/lang/String; = "CallOptionHelper"


# instance fields
.field protected mCallback:Lcom/mediatek/contacts/CallOptionHelper$Callback;

.field protected mContext:Landroid/content/Context;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/contacts/CallOptionHelper;->mContext:Landroid/content/Context;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/mediatek/contacts/CallOptionHelper;
    .locals 1
    .param p0    # Landroid/content/Context;

    new-instance v0, Lcom/mediatek/contacts/CallOptionHelper;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/CallOptionHelper;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "CallOptionHelper"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public makeCall(Landroid/content/Intent;)V
    .locals 17
    .param p1    # Landroid/content/Intent;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "makeCall, intent = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " uri = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/mediatek/contacts/CallOptionHelper;->log(Ljava/lang/String;)V

    const/4 v11, 0x2

    const-string v13, "com.android.phone.extra.video"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_0

    const/4 v11, 0x1

    :cond_0
    const-string v13, "follow_sim_management"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    const-string v13, "sip"

    invoke-virtual {v9, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    if-nez v4, :cond_1

    const/4 v11, 0x0

    :cond_1
    const-string v13, "voicemail:"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v14

    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v11, 0x2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/contacts/CallOptionHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "voice_call_sim_setting"

    const-wide/16 v15, -0x5

    invoke-static/range {v13 .. v16}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v1

    const-wide/16 v13, 0x0

    cmp-long v13, v1, v13

    if-lez v13, :cond_2

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v10

    long-to-int v13, v1

    invoke-virtual {v10, v13}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimInfoById(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v5

    if-eqz v5, :cond_2

    const-string v13, "simId"

    iget v14, v5, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_2
    const-string v6, ""

    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/contacts/CallOptionHelper;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Landroid/telephony/PhoneNumberUtils;->getNumberFromIntent(Landroid/content/Intent;Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    :goto_0
    const-string v13, "com.android.phone.extra.original"

    const-wide/16 v14, -0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14, v15}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v11, v7, v8}, Lcom/mediatek/contacts/CallOptionHelper;->makeCall(Ljava/lang/String;IJ)V

    return-void

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/mediatek/contacts/CallOptionHelper;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected makeCall(Ljava/lang/String;IJ)V
    .locals 14
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # J

    const/4 v11, 0x0

    new-instance v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;

    const/4 v4, 0x0

    const/4 v5, 0x2

    const-wide/16 v6, 0x0

    const/4 v9, 0x0

    move-object v3, p0

    move-object v8, p1

    invoke-direct/range {v2 .. v9}, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;-><init>(Lcom/mediatek/contacts/CallOptionHelper;IIJLjava/lang/String;Ljava/lang/Object;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "makeCall, number = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " originalSim = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/CallOptionHelper;->log(Ljava/lang/String;)V

    packed-switch p2, :pswitch_data_0

    :goto_0
    iget-object v3, p0, Lcom/mediatek/contacts/CallOptionHelper;->mCallback:Lcom/mediatek/contacts/CallOptionHelper$Callback;

    invoke-interface {v3, v2}, Lcom/mediatek/contacts/CallOptionHelper$Callback;->onMakeCall(Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;)V

    return-void

    :pswitch_0
    iget-object v3, p0, Lcom/mediatek/contacts/CallOptionHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "enable_internet_call_value"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    const/4 v3, 0x1

    if-ne v10, v3, :cond_0

    const/4 v3, 0x0

    iput v3, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->type:I

    goto :goto_0

    :cond_0
    const/4 v3, 0x2

    iput v3, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    goto :goto_0

    :pswitch_1
    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v3, -0x1

    if-ne v13, v3, :cond_1

    const/4 v3, 0x1

    iput v3, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    goto :goto_0

    :cond_1
    int-to-long v3, v13

    iput-wide v3, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->id:J

    const/4 v3, 0x1

    iput v3, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->type:I

    goto :goto_0

    :pswitch_2
    move-object v3, p0

    move-object v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p3

    move-object v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/mediatek/contacts/CallOptionHelper;->makeVoiceCall(Ljava/lang/String;IJLcom/mediatek/contacts/CallOptionHelper$CallbackArgs;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected makeVoiceCall(Ljava/lang/String;IJLcom/mediatek/contacts/CallOptionHelper$CallbackArgs;)V
    .locals 30
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # J
    .param p5    # Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;

    const-string v26, "+CallOptionHelper.makeVoiceCall"

    invoke-static/range {v26 .. v26}, Lcom/mediatek/contacts/Profiler;->trace(Ljava/lang/String;)V

    const-wide/16 v21, -0x5

    const-wide/16 v3, -0x5

    const/4 v5, 0x0

    const/16 v17, 0x0

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/CallOptionHelper;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v26

    const-string v27, "voice_call_sim_setting"

    const-wide/16 v28, -0x5

    invoke-static/range {v26 .. v29}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v9

    const-string v26, "phone"

    invoke-static/range {v26 .. v26}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v18

    invoke-static {}, Lcom/mediatek/contacts/SimAssociateHandler;->getInstance()Lcom/mediatek/contacts/SimAssociateHandler;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/SimAssociateHandler;->query(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v26

    if-lez v26, :cond_1

    const/4 v13, 0x1

    :goto_0
    if-eqz v13, :cond_2

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Integer;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v23

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v20

    if-ltz v20, :cond_0

    :try_start_0
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v26

    if-eqz v26, :cond_0

    add-int/lit8 v5, v5, 0x1

    move/from16 v0, v23

    int-to-long v3, v0

    goto :goto_1

    :cond_1
    const/4 v13, 0x0

    goto :goto_0

    :catch_0
    move-exception v11

    invoke-virtual {v11}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/CallOptionHelper;->log(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual/range {v19 .. v19}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getInsertedSimCount()I

    move-result v26

    if-nez v26, :cond_6

    const-wide/16 v26, -0x2

    cmp-long v26, v9, v26

    if-eqz v26, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/CallOptionHelper;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v26

    const-string v27, "enable_internet_call_value"

    const/16 v28, 0x0

    invoke-static/range {v26 .. v28}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    const/4 v15, 0x1

    :goto_2
    if-nez v15, :cond_5

    const/16 v26, 0x2

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->type:I

    const-wide/16 v26, 0x0

    move-wide/from16 v0, v26

    move-object/from16 v2, p5

    iput-wide v0, v2, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->id:J

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    :cond_3
    :goto_3
    return-void

    :cond_4
    const/4 v15, 0x0

    goto :goto_2

    :cond_5
    const/16 v26, 0x5

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    const-wide/16 v26, -0x2

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    goto :goto_3

    :cond_6
    const-wide/16 v26, -0x5

    cmp-long v26, v9, v26

    if-eqz v26, :cond_3

    const-wide/16 v26, -0x5

    cmp-long v26, p3, v26

    if-eqz v26, :cond_7

    move-wide/from16 v0, p3

    long-to-int v0, v0

    move/from16 v26, v0

    move-object/from16 v0, v19

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v20

    if-ltz v20, :cond_8

    :try_start_1
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v26

    if-eqz v26, :cond_8

    const/16 v17, 0x1

    :goto_4
    const-wide/16 v26, -0x2

    cmp-long v26, p3, v26

    if-nez v26, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/CallOptionHelper;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v26

    const-string v27, "enable_internet_call_value"

    const/16 v28, 0x0

    invoke-static/range {v26 .. v28}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_9

    const/16 v17, 0x1

    :cond_7
    :goto_5
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "makeVoiceCall, number = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " type = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " originalSim = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " associateSims = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/CallOptionHelper;->log(Ljava/lang/String;)V

    const-wide/16 v26, -0x1

    cmp-long v26, v9, v26

    if-nez v26, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/CallOptionHelper;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v26

    const-string v27, "enable_internet_call_value"

    const/16 v28, 0x0

    invoke-static/range {v26 .. v28}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v12

    invoke-virtual/range {v19 .. v19}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getInsertedSimCount()I

    move-result v8

    if-nez v12, :cond_a

    if-nez v8, :cond_a

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    goto/16 :goto_3

    :cond_8
    const/16 v17, 0x0

    goto/16 :goto_4

    :catch_1
    move-exception v11

    invoke-virtual {v11}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/CallOptionHelper;->log(Ljava/lang/String;)V

    const/16 v17, 0x0

    goto/16 :goto_4

    :cond_9
    const/16 v17, 0x0

    goto/16 :goto_5

    :cond_a
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "always, associateSimInserts = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " originalSim = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/CallOptionHelper;->log(Ljava/lang/String;)V

    const/16 v26, 0x1

    move/from16 v0, v26

    if-le v5, v0, :cond_c

    const-wide/16 v21, -0x5

    :cond_b
    :goto_6
    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    const/16 v26, 0x5

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    const-string v26, "-CallOptionHelper.makeVoiceCall"

    invoke-static/range {v26 .. v26}, Lcom/mediatek/contacts/Profiler;->trace(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_c
    const/16 v26, 0x1

    move/from16 v0, v26

    if-ne v5, v0, :cond_d

    move-wide/from16 v21, v3

    goto :goto_6

    :cond_d
    if-eqz v17, :cond_b

    move-wide/from16 v21, p3

    goto :goto_6

    :cond_e
    if-nez v13, :cond_10

    const-wide/16 v26, -0x5

    cmp-long v26, p3, v26

    if-eqz v26, :cond_10

    const-wide/16 v26, -0x2

    cmp-long v26, v9, v26

    if-nez v26, :cond_10

    cmp-long v26, p3, v9

    if-eqz v26, :cond_10

    if-eqz v17, :cond_f

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    :goto_7
    const/16 v26, 0x5

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    goto/16 :goto_3

    :cond_f
    const-wide/16 v26, -0x5

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    goto :goto_7

    :cond_10
    const-wide/16 v26, -0x2

    cmp-long v26, v9, v26

    if-nez v26, :cond_16

    if-nez v13, :cond_11

    const-wide/16 v26, -0x5

    cmp-long v26, p3, v26

    if-eqz v26, :cond_15

    const-wide/16 v26, -0x2

    cmp-long v26, p3, v26

    if-eqz v26, :cond_15

    :cond_11
    const/16 v26, 0x1

    move/from16 v0, v26

    if-le v5, v0, :cond_13

    const-wide/16 v21, -0x5

    :cond_12
    :goto_8
    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    const/16 v26, 0x5

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    goto/16 :goto_3

    :cond_13
    const/16 v26, 0x1

    move/from16 v0, v26

    if-ne v5, v0, :cond_14

    move-wide/from16 v21, v3

    goto :goto_8

    :cond_14
    if-eqz v17, :cond_12

    move-wide/from16 v21, p3

    goto :goto_8

    :cond_15
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->type:I

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    goto/16 :goto_3

    :cond_16
    const-wide/16 v26, -0x5

    cmp-long v26, p3, v26

    if-nez v26, :cond_17

    if-nez v13, :cond_17

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "deaultSim = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/CallOptionHelper;->log(Ljava/lang/String;)V

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    move-object/from16 v0, p5

    iput-wide v9, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->id:J

    const-string v26, "-CallOptionHelper.makeVoiceCall"

    invoke-static/range {v26 .. v26}, Lcom/mediatek/contacts/Profiler;->trace(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_17
    const-wide/16 v26, -0x5

    cmp-long v26, p3, v26

    if-eqz v26, :cond_1b

    if-nez v13, :cond_1b

    cmp-long v26, v9, p3

    if-eqz v26, :cond_18

    if-nez v17, :cond_19

    :cond_18
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    move-object/from16 v0, p5

    iput-wide v9, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->id:J

    goto/16 :goto_3

    :cond_19
    const-wide/16 v21, -0x5

    if-eqz v17, :cond_1a

    move-wide/from16 v21, p3

    :cond_1a
    const/16 v26, 0x5

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    goto/16 :goto_3

    :cond_1b
    const-wide/16 v26, -0x5

    cmp-long v26, p3, v26

    if-nez v26, :cond_21

    if-eqz v13, :cond_21

    const/16 v26, 0x2

    move/from16 v0, v26

    if-lt v5, v0, :cond_1c

    const-wide/16 v26, -0x5

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    const/16 v26, 0x5

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    goto/16 :goto_3

    :cond_1c
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_1e

    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    move/from16 v0, v26

    int-to-long v3, v0

    :cond_1d
    :goto_9
    const/16 v26, 0x1

    move/from16 v0, v26

    if-ne v5, v0, :cond_21

    cmp-long v26, v9, v3

    if-nez v26, :cond_20

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    move-object/from16 v0, p5

    iput-wide v9, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->id:J

    goto/16 :goto_3

    :cond_1e
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v26

    const/16 v27, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-lt v0, v1, :cond_1d

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_1f
    :goto_a
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_1d

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Integer;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v23

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v20

    if-ltz v20, :cond_1f

    :try_start_2
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result v26

    if-eqz v26, :cond_1f

    move/from16 v0, v23

    int-to-long v3, v0

    goto :goto_9

    :catch_2
    move-exception v11

    invoke-virtual {v11}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/CallOptionHelper;->log(Ljava/lang/String;)V

    goto :goto_a

    :cond_20
    const/16 v26, 0x5

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    goto/16 :goto_3

    :cond_21
    cmp-long v26, v9, p3

    if-nez v26, :cond_22

    cmp-long v26, v9, v3

    if-nez v26, :cond_22

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    move-object/from16 v0, p5

    iput-wide v9, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->id:J

    goto/16 :goto_3

    :cond_22
    cmp-long v26, v9, p3

    if-nez v26, :cond_23

    if-eqz v13, :cond_23

    if-nez v5, :cond_23

    const/16 v26, 0x5

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    goto/16 :goto_3

    :cond_23
    const-wide/16 v26, -0x5

    cmp-long v26, p3, v26

    if-eqz v26, :cond_24

    if-eqz v13, :cond_24

    if-nez v5, :cond_24

    move-wide/from16 v0, p3

    long-to-int v0, v0

    move/from16 v26, v0

    move-object/from16 v0, v19

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v20

    if-ltz v20, :cond_24

    cmp-long v26, p3, v9

    if-eqz v26, :cond_24

    :try_start_3
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z

    move-result v26

    if-eqz v26, :cond_24

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    const/16 v26, 0x5

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_3

    :catch_3
    move-exception v11

    const-string v26, "catch the remote exception!"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/CallOptionHelper;->log(Ljava/lang/String;)V

    :cond_24
    const/16 v26, 0x2

    move/from16 v0, v26

    if-lt v5, v0, :cond_25

    const/16 v26, 0x5

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    const-wide/16 v26, -0x5

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    goto/16 :goto_3

    :cond_25
    const/16 v26, 0x1

    move/from16 v0, v26

    if-ne v5, v0, :cond_26

    const/16 v26, 0x5

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    goto/16 :goto_3

    :cond_26
    const-wide/16 v26, -0x5

    cmp-long v26, v3, v26

    if-nez v26, :cond_27

    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    move/from16 v0, v26

    int-to-long v3, v0

    :cond_27
    move-object/from16 v0, p5

    iput-wide v3, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->id:J

    const/16 v26, 0x6

    move/from16 v0, v26

    move-object/from16 v1, p5

    iput v0, v1, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->reason:I

    new-instance v6, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;-><init>(Lcom/mediatek/contacts/CallOptionHelper;)V

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getInsertedSimCount()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-gt v0, v1, :cond_2b

    const/16 v26, 0x0

    move/from16 v0, v26

    iput v0, v6, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->type:I

    if-eqz v17, :cond_29

    move-wide/from16 v24, p3

    :goto_b
    const-wide/16 v26, -0x2

    cmp-long v26, v9, v26

    if-nez v26, :cond_2a

    const-wide/16 v26, -0x2

    move-wide/from16 v0, v26

    iput-wide v0, v6, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->suggested:J

    :cond_28
    :goto_c
    move-object/from16 v0, p5

    iput-object v6, v0, Lcom/mediatek/contacts/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    goto/16 :goto_3

    :cond_29
    move-wide/from16 v24, v9

    goto :goto_b

    :cond_2a
    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v26, v0

    move-object/from16 v0, v19

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimInfoById(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v26

    move-object/from16 v0, v26

    iput-object v0, v6, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->viaSimInfo:Landroid/provider/Telephony$SIMInfo;

    goto :goto_c

    :cond_2b
    const/16 v26, 0x1

    move/from16 v0, v26

    iput v0, v6, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->type:I

    if-eqz v17, :cond_2c

    :goto_d
    move-wide/from16 v0, p3

    iput-wide v0, v6, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->suggested:J

    const-wide/16 v26, -0x1

    cmp-long v26, v9, v26

    if-eqz v26, :cond_28

    const-wide/16 v26, -0x2

    cmp-long v26, v9, v26

    if-eqz v26, :cond_28

    long-to-int v0, v9

    move/from16 v26, v0

    move-object/from16 v0, v19

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimInfoById(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v26

    move-object/from16 v0, v26

    iput-object v0, v6, Lcom/mediatek/contacts/CallOptionHelper$AssociateSimMissingArgs;->viaSimInfo:Landroid/provider/Telephony$SIMInfo;

    goto :goto_c

    :cond_2c
    move-wide/from16 p3, v9

    goto :goto_d
.end method

.method public setCallback(Lcom/mediatek/contacts/CallOptionHelper$Callback;)V
    .locals 0
    .param p1    # Lcom/mediatek/contacts/CallOptionHelper$Callback;

    iput-object p1, p0, Lcom/mediatek/contacts/CallOptionHelper;->mCallback:Lcom/mediatek/contacts/CallOptionHelper$Callback;

    return-void
.end method
