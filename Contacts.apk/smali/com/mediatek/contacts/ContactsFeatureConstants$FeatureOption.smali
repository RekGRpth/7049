.class public interface abstract Lcom/mediatek/contacts/ContactsFeatureConstants$FeatureOption;
.super Ljava/lang/Object;
.source "ContactsFeatureConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/ContactsFeatureConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FeatureOption"
.end annotation


# static fields
.field public static final MTK_BT_PROFILE_BPP:Z = true

.field public static final MTK_DIALER_SEARCH_SUPPORT:Z = true

.field public static final MTK_DRM_APP:Z = true

.field public static final MTK_GEMINI_3G_SWITCH:Z = false

.field public static final MTK_GEMINI_SUPPORT:Z = true

.field public static final MTK_PHONE_NUMBER_GEODESCRIPTION:Z = true

.field public static final MTK_SEARCH_DB_SUPPORT:Z = true

.field public static final MTK_THEMEMANAGER_APP:Z = true

.field public static final MTK_VT3G324M_SUPPORT:Z = true
