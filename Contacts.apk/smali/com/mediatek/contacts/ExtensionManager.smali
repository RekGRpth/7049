.class public Lcom/mediatek/contacts/ExtensionManager;
.super Ljava/lang/Object;
.source "ExtensionManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ExtensionManager"

.field private static mContext:Landroid/content/Context;

.field private static mInstance:Lcom/mediatek/contacts/ExtensionManager;

.field private static mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;


# instance fields
.field private mCallDetailExtension:Lcom/android/contacts/ext/CallDetailExtension;

.field private mCallListExtension:Lcom/android/contacts/ext/CallListExtension;

.field private mContactAccountExtension:Lcom/android/contacts/ext/ContactAccountExtension;

.field private mContactDetailExtension:Lcom/android/contacts/ext/ContactDetailExtension;

.field private mContactListExtension:Lcom/android/contacts/ext/ContactListExtension;

.field private mDialPadExtension:Lcom/android/contacts/ext/DialPadExtension;

.field private mDialtactsExtension:Lcom/android/contacts/ext/DialtactsExtension;

.field private mHasPlugin:Z

.field private mQuickContactExtension:Lcom/android/contacts/ext/QuickContactExtension;

.field private mSimPickExtension:Lcom/android/contacts/ext/SimPickExtension;

.field private mSpeedDialExtension:Lcom/android/contacts/ext/SpeedDialExtension;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/contacts/ExtensionManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mHasPlugin:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mHasPlugin:Z

    invoke-direct {p0}, Lcom/mediatek/contacts/ExtensionManager;->getPlugin()V

    return-void
.end method

.method public static getInstance()Lcom/mediatek/contacts/ExtensionManager;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mInstance:Lcom/mediatek/contacts/ExtensionManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/contacts/ExtensionManager;

    invoke-direct {v0}, Lcom/mediatek/contacts/ExtensionManager;-><init>()V

    sput-object v0, Lcom/mediatek/contacts/ExtensionManager;->mInstance:Lcom/mediatek/contacts/ExtensionManager;

    :cond_0
    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mInstance:Lcom/mediatek/contacts/ExtensionManager;

    return-object v0
.end method

.method private getPlugin()V
    .locals 9

    const/4 v8, 0x0

    const/4 v0, 0x0

    sget-object v5, Lcom/mediatek/contacts/ExtensionManager;->mContext:Landroid/content/Context;

    if-eqz v5, :cond_0

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mContext:Landroid/content/Context;

    :goto_0
    const-string v5, "ExtensionManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getPlugin applicationContext : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-class v5, Lcom/android/contacts/ext/IOPContactPlugin;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    new-array v6, v8, [Landroid/content/pm/Signature;

    invoke-static {v0, v5, v6}, Lcom/mediatek/pluginmanager/PluginManager;->create(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Lcom/mediatek/pluginmanager/PluginManager;

    move-result-object v4

    const-string v5, "ExtensionManager"

    const-string v6, "get pluginManager"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4}, Lcom/mediatek/pluginmanager/PluginManager;->getPluginCount()I

    move-result v3

    if-nez v3, :cond_1

    const-string v5, "ExtensionManager"

    const-string v6, "no plugin apk"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Lcom/android/contacts/ext/ContactPluginDefault;

    invoke-direct {v5}, Lcom/android/contacts/ext/ContactPluginDefault;-><init>()V

    sput-object v5, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    :goto_1
    return-void

    :cond_0
    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v4, v8}, Lcom/mediatek/pluginmanager/PluginManager;->getPlugin(I)Lcom/mediatek/pluginmanager/Plugin;

    move-result-object v1

    if-eqz v1, :cond_2

    :try_start_0
    const-string v5, "ExtensionManager"

    const-string v6, "create plug in"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/mediatek/pluginmanager/Plugin;->createObject()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/contacts/ext/IOPContactPlugin;

    sput-object v5, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/mediatek/contacts/ExtensionManager;->mHasPlugin:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    const/4 v5, 0x0

    sput-object v5, Lcom/mediatek/contacts/ExtensionManager;->mContext:Landroid/content/Context;

    goto :goto_1

    :cond_2
    :try_start_1
    const-string v5, "ExtensionManager"

    const-string v6, "contactPlugin is null "

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Lcom/android/contacts/ext/ContactPluginDefault;

    invoke-direct {v5}, Lcom/android/contacts/ext/ContactPluginDefault;-><init>()V

    sput-object v5, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v2

    const-string v5, "ExtensionManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getPlugin is error "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Lcom/android/contacts/ext/ContactPluginDefault;

    invoke-direct {v5}, Lcom/android/contacts/ext/ContactPluginDefault;-><init>()V

    sput-object v5, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    goto :goto_2
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    const-string v0, "ExtensionManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "context : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sput-object p0, Lcom/mediatek/contacts/ExtensionManager;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public HasPlugin()Z
    .locals 3

    const-string v0, "ExtensionManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "return mHasPlugin : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/contacts/ExtensionManager;->mHasPlugin:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mHasPlugin:Z

    return v0
.end method

.method public getCallDetailExtension()Lcom/android/contacts/ext/CallDetailExtension;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mCallDetailExtension:Lcom/android/contacts/ext/CallDetailExtension;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    invoke-interface {v0}, Lcom/android/contacts/ext/IOPContactPlugin;->createCallDetailExtension()Lcom/android/contacts/ext/CallDetailExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mCallDetailExtension:Lcom/android/contacts/ext/CallDetailExtension;

    const-string v0, "ExtensionManager"

    const-string v1, "return CallDetailExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mCallDetailExtension:Lcom/android/contacts/ext/CallDetailExtension;

    return-object v0
.end method

.method public getCallListExtension()Lcom/android/contacts/ext/CallListExtension;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mCallListExtension:Lcom/android/contacts/ext/CallListExtension;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    invoke-interface {v0}, Lcom/android/contacts/ext/IOPContactPlugin;->createCallListExtension()Lcom/android/contacts/ext/CallListExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mCallListExtension:Lcom/android/contacts/ext/CallListExtension;

    const-string v0, "ExtensionManager"

    const-string v1, "return CallListExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mCallListExtension:Lcom/android/contacts/ext/CallListExtension;

    return-object v0
.end method

.method public getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactAccountExtension:Lcom/android/contacts/ext/ContactAccountExtension;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    invoke-interface {v0}, Lcom/android/contacts/ext/IOPContactPlugin;->createContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactAccountExtension:Lcom/android/contacts/ext/ContactAccountExtension;

    const-string v0, "ExtensionManager"

    const-string v1, "return ContactAccountExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactAccountExtension:Lcom/android/contacts/ext/ContactAccountExtension;

    return-object v0
.end method

.method public getContactDetailExtension()Lcom/android/contacts/ext/ContactDetailExtension;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactDetailExtension:Lcom/android/contacts/ext/ContactDetailExtension;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    invoke-interface {v0}, Lcom/android/contacts/ext/IOPContactPlugin;->createContactDetailExtension()Lcom/android/contacts/ext/ContactDetailExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactDetailExtension:Lcom/android/contacts/ext/ContactDetailExtension;

    const-string v0, "ExtensionManager"

    const-string v1, "return ContactDetailExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactDetailExtension:Lcom/android/contacts/ext/ContactDetailExtension;

    return-object v0
.end method

.method public getContactListExtension()Lcom/android/contacts/ext/ContactListExtension;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactListExtension:Lcom/android/contacts/ext/ContactListExtension;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    invoke-interface {v0}, Lcom/android/contacts/ext/IOPContactPlugin;->createContactListExtension()Lcom/android/contacts/ext/ContactListExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactListExtension:Lcom/android/contacts/ext/ContactListExtension;

    const-string v0, "ExtensionManager"

    const-string v1, "return ContactListExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactListExtension:Lcom/android/contacts/ext/ContactListExtension;

    return-object v0
.end method

.method public getDialPadExtension()Lcom/android/contacts/ext/DialPadExtension;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mDialPadExtension:Lcom/android/contacts/ext/DialPadExtension;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    invoke-interface {v0}, Lcom/android/contacts/ext/IOPContactPlugin;->createDialPadExtension()Lcom/android/contacts/ext/DialPadExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mDialPadExtension:Lcom/android/contacts/ext/DialPadExtension;

    const-string v0, "ExtensionManager"

    const-string v1, "return DialPadExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mDialPadExtension:Lcom/android/contacts/ext/DialPadExtension;

    return-object v0
.end method

.method public getDialtactsExtension()Lcom/android/contacts/ext/DialtactsExtension;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mDialtactsExtension:Lcom/android/contacts/ext/DialtactsExtension;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    invoke-interface {v0}, Lcom/android/contacts/ext/IOPContactPlugin;->createDialtactsExtension()Lcom/android/contacts/ext/DialtactsExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mDialtactsExtension:Lcom/android/contacts/ext/DialtactsExtension;

    const-string v0, "ExtensionManager"

    const-string v1, "return DialtactsExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mDialtactsExtension:Lcom/android/contacts/ext/DialtactsExtension;

    return-object v0
.end method

.method public getQuickContactExtension()Lcom/android/contacts/ext/QuickContactExtension;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mQuickContactExtension:Lcom/android/contacts/ext/QuickContactExtension;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    invoke-interface {v0}, Lcom/android/contacts/ext/IOPContactPlugin;->createQuickContactExtension()Lcom/android/contacts/ext/QuickContactExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mQuickContactExtension:Lcom/android/contacts/ext/QuickContactExtension;

    const-string v0, "ExtensionManager"

    const-string v1, "return QuickContactExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mQuickContactExtension:Lcom/android/contacts/ext/QuickContactExtension;

    return-object v0
.end method

.method public getSimPickExtension()Lcom/android/contacts/ext/SimPickExtension;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mSimPickExtension:Lcom/android/contacts/ext/SimPickExtension;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    invoke-interface {v0}, Lcom/android/contacts/ext/IOPContactPlugin;->createSimPickExtension()Lcom/android/contacts/ext/SimPickExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mSimPickExtension:Lcom/android/contacts/ext/SimPickExtension;

    const-string v0, "ExtensionManager"

    const-string v1, "return SimPickExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mSimPickExtension:Lcom/android/contacts/ext/SimPickExtension;

    return-object v0
.end method

.method public getSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mSpeedDialExtension:Lcom/android/contacts/ext/SpeedDialExtension;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->mPlugin:Lcom/android/contacts/ext/IOPContactPlugin;

    invoke-interface {v0}, Lcom/android/contacts/ext/IOPContactPlugin;->createSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mSpeedDialExtension:Lcom/android/contacts/ext/SpeedDialExtension;

    const-string v0, "ExtensionManager"

    const-string v1, "return SpeedDialExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mSpeedDialExtension:Lcom/android/contacts/ext/SpeedDialExtension;

    return-object v0
.end method
