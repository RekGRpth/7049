.class Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread;
.super Ljava/lang/Thread;
.source "ShareContactViaSDCard.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/ShareContactViaSDCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchContactThread"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread$CanceledException;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/ShareContactViaSDCard;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-static {v1}, Lcom/mediatek/contacts/ShareContactViaSDCard;->access$100(Lcom/mediatek/contacts/ShareContactViaSDCard;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    iget-object v1, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-static {v1}, Lcom/mediatek/contacts/ShareContactViaSDCard;->access$100(Lcom/mediatek/contacts/ShareContactViaSDCard;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-static {v2, v1}, Lcom/mediatek/contacts/ShareContactViaSDCard;->access$202(Lcom/mediatek/contacts/ShareContactViaSDCard;Landroid/net/Uri;)Landroid/net/Uri;

    const-string v1, "ShareContactViaSDCard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDataUri is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-static {v3}, Lcom/mediatek/contacts/ShareContactViaSDCard;->access$200(Lcom/mediatek/contacts/ShareContactViaSDCard;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "ShareContactViaSDCard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "type is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-static {v1}, Lcom/mediatek/contacts/ShareContactViaSDCard;->access$200(Lcom/mediatek/contacts/ShareContactViaSDCard;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    iget-object v2, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$SearchContactThread;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-static {v2}, Lcom/mediatek/contacts/ShareContactViaSDCard;->access$300(Lcom/mediatek/contacts/ShareContactViaSDCard;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/mediatek/contacts/ShareContactViaSDCard;->shareViaSDCard(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
