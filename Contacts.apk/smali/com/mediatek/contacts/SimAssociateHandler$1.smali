.class Lcom/mediatek/contacts/SimAssociateHandler$1;
.super Landroid/content/BroadcastReceiver;
.source "SimAssociateHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/SimAssociateHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/SimAssociateHandler;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/SimAssociateHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/SimAssociateHandler$1;->this$0:Lcom/mediatek/contacts/SimAssociateHandler;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.contacts.associate_changed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/SimAssociateHandler$1;->this$0:Lcom/mediatek/contacts/SimAssociateHandler;

    const-string v2, "RCV associated sim changed!"

    invoke-virtual {v1, v2}, Lcom/mediatek/contacts/SimAssociateHandler;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/contacts/SimAssociateHandler$1;->this$0:Lcom/mediatek/contacts/SimAssociateHandler;

    iget-object v1, v1, Lcom/mediatek/contacts/SimAssociateHandler;->mMyHandler:Lcom/mediatek/contacts/SimAssociateHandler$MyHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method
