.class public Lcom/mediatek/contacts/SimAssociateHandler;
.super Landroid/os/HandlerThread;
.source "SimAssociateHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/SimAssociateHandler$MyHandler;,
        Lcom/mediatek/contacts/SimAssociateHandler$ContactContentObserver;
    }
.end annotation


# static fields
.field public static final ASSOCIATE_CHANGED:Ljava/lang/String; = "com.android.contacts.associate_changed"

.field private static final MIN_MATCH:I = 0x7

.field private static final MSG_LOAD:I = 0x0

.field private static final MSG_QUIT:I = 0x1

.field private static final MSG_REFRESH:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SimAssociateHandler"

.field private static sMe:Lcom/mediatek/contacts/SimAssociateHandler;


# instance fields
.field private mCacheDirty:Z

.field mContactContentObserver:Lcom/mediatek/contacts/SimAssociateHandler$ContactContentObserver;

.field protected mContext:Landroid/content/Context;

.field private mLoading:Z

.field protected mMyHandler:Lcom/mediatek/contacts/SimAssociateHandler$MyHandler;

.field protected mSimAssociationMaps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList;",
            ">;"
        }
    .end annotation
.end field

.field private mStarted:Z

.field protected myReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x1

    const/4 v2, 0x0

    const-string v1, "SimAssociateHandler"

    invoke-direct {p0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mLoading:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mStarted:Z

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mSimAssociationMaps:Ljava/util/HashMap;

    new-instance v1, Lcom/mediatek/contacts/SimAssociateHandler$1;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/SimAssociateHandler$1;-><init>(Lcom/mediatek/contacts/SimAssociateHandler;)V

    iput-object v1, p0, Lcom/mediatek/contacts/SimAssociateHandler;->myReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.android.contacts.associate_changed"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/mediatek/contacts/SimAssociateHandler$ContactContentObserver;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/SimAssociateHandler$ContactContentObserver;-><init>(Lcom/mediatek/contacts/SimAssociateHandler;)V

    iput-object v1, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mContactContentObserver:Lcom/mediatek/contacts/SimAssociateHandler$ContactContentObserver;

    iget-object v1, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/contacts/SimAssociateHandler;->myReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mContactContentObserver:Lcom/mediatek/contacts/SimAssociateHandler$ContactContentObserver;

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iput-boolean v4, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mCacheDirty:Z

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/SimAssociateHandler;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/SimAssociateHandler;

    iget-boolean v0, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mCacheDirty:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/contacts/SimAssociateHandler;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/SimAssociateHandler;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mCacheDirty:Z

    return p1
.end method

.method static synthetic access$102(Lcom/mediatek/contacts/SimAssociateHandler;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/SimAssociateHandler;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mLoading:Z

    return p1
.end method

.method public static getInstance()Lcom/mediatek/contacts/SimAssociateHandler;
    .locals 2

    sget-object v0, Lcom/mediatek/contacts/SimAssociateHandler;->sMe:Lcom/mediatek/contacts/SimAssociateHandler;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/contacts/SimAssociateHandler;

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/SimAssociateHandler;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/contacts/SimAssociateHandler;->sMe:Lcom/mediatek/contacts/SimAssociateHandler;

    :cond_0
    sget-object v0, Lcom/mediatek/contacts/SimAssociateHandler;->sMe:Lcom/mediatek/contacts/SimAssociateHandler;

    return-object v0
.end method


# virtual methods
.method public load()V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "load"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/SimAssociateHandler;->log(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mLoading:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mCacheDirty:Z

    if-nez v0, :cond_1

    :cond_0
    monitor-exit p0

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mLoading:Z

    iget-object v0, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mMyHandler:Lcom/mediatek/contacts/SimAssociateHandler$MyHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "duplicate MSG_LOAD"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/SimAssociateHandler;->log(Ljava/lang/String;)V

    :goto_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mMyHandler:Lcom/mediatek/contacts/SimAssociateHandler$MyHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "SimAssociateHandler"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public prepair()V
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Thread;->start()V

    new-instance v0, Lcom/mediatek/contacts/SimAssociateHandler$MyHandler;

    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mediatek/contacts/SimAssociateHandler$MyHandler;-><init>(Lcom/mediatek/contacts/SimAssociateHandler;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mMyHandler:Lcom/mediatek/contacts/SimAssociateHandler$MyHandler;

    return-void
.end method

.method public query(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x7

    if-le v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x7

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mSimAssociationMaps:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    return-object v1
.end method

.method public relase()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mMyHandler:Lcom/mediatek/contacts/SimAssociateHandler$MyHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mMyHandler:Lcom/mediatek/contacts/SimAssociateHandler$MyHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/contacts/SimAssociateHandler;->myReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/contacts/SimAssociateHandler;->mContactContentObserver:Lcom/mediatek/contacts/SimAssociateHandler$ContactContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method
