.class Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;
.super Ljava/lang/Object;
.source "SpecialCharSequenceMgrProxy.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SimContactQueryCookie"
.end annotation


# instance fields
.field public contactNum:I

.field public context:Landroid/content/Context;

.field public doubleQuery:Z

.field public find:[Z

.field private mHandler:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;

.field private mToken:I

.field public progressDialog:Landroid/app/ProgressDialog;

.field public simName:[Ljava/lang/String;

.field public simNumber:[Ljava/lang/String;

.field public text:Ljava/lang/String;

.field private textField:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(ILcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;I)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;
    .param p3    # I

    const/4 v1, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->simNumber:[Ljava/lang/String;

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->simName:[Ljava/lang/String;

    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->find:[Z

    iput p1, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->contactNum:I

    iput-object p2, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->mHandler:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;

    iput p3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->mToken:I

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->textField:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized getQueryHandler()Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->mHandler:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTextField()Landroid/widget/EditText;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->textField:Landroid/widget/EditText;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->textField:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->mHandler:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;

    iget v1, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->mToken:I

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setTextField(Landroid/widget/EditText;)V
    .locals 1
    .param p1    # Landroid/widget/EditText;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->textField:Landroid/widget/EditText;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
