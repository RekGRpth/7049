.class public Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;
.super Ljava/lang/Object;
.source "SpecialCharSequenceMgrProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;,
        Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;
    }
.end annotation


# static fields
.field private static final ADN_INDEX_COLUMN_NAME:Ljava/lang/String; = "index"

.field private static final ADN_NAME_COLUMN_NAME:Ljava/lang/String; = "name"

.field private static final ADN_PHONE_NUMBER_COLUMN_NAME:Ljava/lang/String; = "number"

.field private static final ADN_QUERY_TOKEN_SIM1:I = 0x0

.field private static final ADN_QUERY_TOKEN_SIM2:I = 0x1

.field private static final MMI_IMEI_DISPLAY:Ljava/lang/String; = "*#06#"

.field private static final SIM_CONTACT_URI:Ljava/lang/String; = "content://icc/adn"

.field private static final TAG:Ljava/lang/String; = "SpecialCharSequenceMgrProxy"

.field private static final USIM_CONTACT_URI:Ljava/lang/String; = "content://icc/pbr"

.field private static sCookie:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

.field private static sStopProgress:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->sCookie:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->sStopProgress:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->sStopProgress:Z

    return v0
.end method

.method static buildSIMContactQueryUri(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/contacts/util/TelephonyUtils;->isUSIMInner(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "content://icc/pbr"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    add-int/lit8 v1, p0, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    const-string v1, "content://icc/adn"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static dismissDialog()V
    .locals 3

    const-string v0, "SpecialCharSequenceMgrProxy"

    const-string v1, "dismissProgressDialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->sStopProgress:Z

    const-string v0, "SpecialCharSequenceMgrProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sStopProgress "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->sStopProgress:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->sCookie:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->sCookie:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v0, v0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->sCookie:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v0, v0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->sCookie:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v0, v0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method static fdnRequest(I)Z
    .locals 6
    .param p0    # I

    const/4 v0, 0x0

    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v3, "SpecialCharSequenceMgrProxy"

    const-string v4, "fdnRequest iTel is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    invoke-interface {v2, p0}, Lcom/android/internal/telephony/ITelephony;->isFDNEnabledGemini(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    const-string v3, "SpecialCharSequenceMgrProxy"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fdnRequest fdn enable is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "SpecialCharSequenceMgrProxy"

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method static handleAdnEntry(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;)Z
    .locals 19
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/widget/EditText;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleAdnEntry, input = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    const-string v5, "keyguard"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/app/KeyguardManager;

    invoke-virtual {v13}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return v5

    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v14

    const/4 v5, 0x1

    if-le v14, v5, :cond_e

    const/4 v5, 0x5

    if-ge v14, v5, :cond_e

    const-string v5, "#"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    const/4 v5, 0x0

    sput-boolean v5, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->sStopProgress:Z

    const/4 v12, -0x1

    const/4 v5, 0x0

    add-int/lit8 v6, v14, -0x1

    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v12

    if-gtz v12, :cond_1

    const/4 v5, 0x0

    goto :goto_0

    :catch_0
    move-exception v11

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v1, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;-><init>(Landroid/content/ContentResolver;)V

    new-instance v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    const/4 v5, 0x0

    invoke-direct {v3, v12, v1, v5}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;-><init>(ILcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;I)V

    sput-object v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->sCookie:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iput v12, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->contactNum:I

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->setTextField(Landroid/widget/EditText;)V

    if-eqz p2, :cond_4

    invoke-virtual/range {p2 .. p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->text:Ljava/lang/String;

    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "index = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    iget-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    if-nez v5, :cond_2

    new-instance v5, Landroid/app/ProgressDialog;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    :cond_2
    iget-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    const v6, 0x7f0c0174

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setTitle(I)V

    iget-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    const v6, 0x7f0c0173

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5, v3}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    move-object/from16 v0, p0

    iput-object v0, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->context:Landroid/content/Context;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "voice_call_sim_setting"

    const-wide/16 v7, -0x3

    invoke-static {v5, v6, v7, v8}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v9

    const-wide/16 v5, -0x2

    cmp-long v5, v9, v5

    if-eqz v5, :cond_3

    const-wide/16 v5, -0x5

    cmp-long v5, v9, v5

    if-nez v5, :cond_5

    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x0

    iput-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->text:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v11

    const-string v5, "SpecialCharSequenceMgrProxy"

    invoke-virtual {v11}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    const/4 v5, 0x1

    goto/16 :goto_0

    :cond_5
    :try_start_2
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getInsertedSimCount()I

    move-result v17

    const/4 v2, 0x0

    const-wide/16 v5, -0x1

    cmp-long v5, v9, v5

    if-nez v5, :cond_c

    const/4 v5, 0x0

    invoke-static {v5}, Lcom/mediatek/contacts/util/TelephonyUtils;->isRadioOnInner(I)Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v5, 0x1

    invoke-static {v5}, Lcom/mediatek/contacts/util/TelephonyUtils;->isRadioOnInner(I)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "radio power off, bail out"

    invoke-static {v5}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v5, 0x0

    invoke-static {v5}, Lcom/mediatek/contacts/util/TelephonyUtils;->isSimReadyInner(I)Z

    move-result v15

    const/4 v5, 0x1

    invoke-static {v5}, Lcom/mediatek/contacts/util/TelephonyUtils;->isSimReadyInner(I)Z

    move-result v16

    if-nez v15, :cond_7

    if-nez v16, :cond_7

    const-string v5, "sim not ready, bail out"

    invoke-static {v5}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v5, 0x0

    iput-boolean v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->doubleQuery:Z

    if-nez v15, :cond_b

    if-eqz v16, :cond_b

    const/4 v2, 0x1

    :cond_8
    :goto_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sim1Ready = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sim2Ready = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " doubleQuery = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->doubleQuery:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    :cond_9
    invoke-static {v2}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->buildSIMContactQueryUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "slot = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " uri = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    iget-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v5, :cond_a

    iget-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->isShowing()Z

    move-result v5

    if-nez v5, :cond_a

    const-string v5, "SpecialCharSequenceMgrProxy"

    const-string v6, "handleAdnEntry() sc.progressDialog.show()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    :cond_a
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "number"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "index"

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b
    if-eqz v15, :cond_8

    if-eqz v16, :cond_8

    const/4 v5, 0x1

    iput-boolean v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->doubleQuery:Z

    goto/16 :goto_3

    :cond_c
    long-to-int v5, v9

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v2

    long-to-int v5, v9

    invoke-static {v5}, Lcom/mediatek/contacts/util/TelephonyUtils;->isRadioOn(I)Z

    move-result v5

    if-eqz v5, :cond_d

    long-to-int v5, v9

    invoke-static {v5}, Lcom/mediatek/contacts/util/TelephonyUtils;->isSimReady(I)Z

    move-result v5

    if-nez v5, :cond_9

    :cond_d
    const-string v5, "radio power off or sim not ready, bail out"

    invoke-static {v5}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method static handleChars(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->handleChars(Landroid/content/Context;Ljava/lang/String;ZLandroid/widget/EditText;)Z

    move-result v0

    return v0
.end method

.method public static handleChars(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/widget/EditText;

    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->handleChars(Landroid/content/Context;Ljava/lang/String;ZLandroid/widget/EditText;)Z

    move-result v0

    return v0
.end method

.method static handleChars(Landroid/content/Context;Ljava/lang/String;ZLandroid/widget/EditText;)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Landroid/widget/EditText;

    const-string v1, "SpecialCharSequenceMgrProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleChars() dialString:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->handleIMEIDisplay(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0, v0}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->handlePinEntry(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0, v0, p3}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->handleAdnEntry(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0, v0}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->handleSecretCode(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static handleIMEIDisplay(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const-string v1, "*#06#"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    move-result v0

    invoke-static {p0, p2}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->showIMEIPanel(Landroid/content/Context;Z)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static handlePinEntry(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const-string v10, "phone"

    invoke-static {v10}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v10

    invoke-static {v10}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v6

    const-string v10, "**04"

    invoke-virtual {p1, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "**05"

    invoke-virtual {p1, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    :cond_0
    const-string v10, "#"

    invoke-virtual {p1, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    move-object v0, p1

    :try_start_0
    new-instance v5, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$1;

    invoke-direct {v5, v6, v0}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$1;-><init>(Lcom/android/internal/telephony/ITelephony;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/contacts/util/ContactsSettingsUtils;->getDefaultSIMForVoiceCall()J

    move-result-wide v1

    const-wide/16 v10, -0x5

    cmp-long v10, v1, v10

    if-eqz v10, :cond_1

    const-wide/16 v10, -0x2

    cmp-long v10, v1, v10

    if-nez v10, :cond_2

    :cond_1
    const/4 v10, 0x0

    :goto_0
    return v10

    :cond_2
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getInsertedSimCount()I

    move-result v7

    const-wide/16 v10, -0x1

    cmp-long v10, v1, v10

    if-nez v10, :cond_3

    const/4 v10, 0x2

    if-ne v7, v10, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c0028

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {p0, v10, v11, v5}, Lcom/mediatek/contacts/widget/SimPickerDialog;->create(Landroid/content/Context;Ljava/lang/String;ZLandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    const/4 v10, 0x1

    goto :goto_0

    :cond_3
    const-wide/16 v10, -0x2

    cmp-long v10, v1, v10

    if-nez v10, :cond_4

    const/4 v10, 0x0

    goto :goto_0

    :cond_4
    const-wide/16 v10, -0x1

    cmp-long v10, v1, v10

    if-nez v10, :cond_6

    invoke-virtual {v8}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getInsertedSimInfoList()Ljava/util/List;

    move-result-object v10

    if-eqz v10, :cond_5

    invoke-virtual {v8}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getInsertedSimInfoList()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_5

    invoke-virtual {v8}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getInsertedSimInfoList()Ljava/util/List;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/provider/Telephony$SIMInfo;

    iget v10, v10, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-interface {v6, v0, v10}, Lcom/android/internal/telephony/ITelephony;->handlePinMmiGemini(Ljava/lang/String;I)Z

    move-result v10

    goto :goto_0

    :cond_5
    const/4 v10, 0x0

    goto :goto_0

    :cond_6
    long-to-int v10, v1

    invoke-virtual {v8, v10}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v9

    invoke-interface {v6, v0, v9}, Lcom/android/internal/telephony/ITelephony;->handlePinMmiGemini(Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    goto :goto_0

    :catch_0
    move-exception v4

    const-string v10, "SpecialCharSequenceMgrProxy"

    const-string v11, "Failed to handlePinMmi due to remote exception"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    goto :goto_0

    :cond_7
    const/4 v10, 0x0

    goto :goto_0
.end method

.method static handleSecretCode(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/android/contacts/SpecialCharSequenceMgr;->handleSecretCode(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static log(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "SpecialCharSequenceMgrProxy"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static showIMEIPanel(Landroid/content/Context;Z)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    const/4 v7, 0x0

    const v6, 0x7f0c003a

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Landroid/telephony/TelephonyManager;->getDeviceIdGemini(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v5

    invoke-virtual {v2, v4}, Landroid/telephony/TelephonyManager;->getDeviceIdGemini(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    aget-object v3, v1, v5

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v5

    :cond_0
    aget-object v3, v1, v4

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    :cond_1
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0c016b

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1, v7}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x104000a

    invoke-virtual {v3, v4, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method static showMEIDPanel(Landroid/content/Context;ZLandroid/telephony/TelephonyManager;)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # Landroid/telephony/TelephonyManager;

    invoke-static {p0, p1, p2}, Lcom/android/contacts/SpecialCharSequenceMgr;->showMEIDPanel(Landroid/content/Context;ZLandroid/telephony/TelephonyManager;)V

    return-void
.end method
