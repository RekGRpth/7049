.class public Lcom/mediatek/contacts/activities/CallLogMultipleChoiceActivity;
.super Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;
.source "CallLogMultipleChoiceActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CallLogMultipleChoiceActivity"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;-><init>()V

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "CallLogMultipleChoiceActivity"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/MenuItem;

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v2

    :goto_0
    return v2

    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->mFragment:Lcom/mediatek/contacts/calllog/CallLogMultipleDeleteFragment;

    invoke-virtual {v2}, Lcom/mediatek/contacts/calllog/CallLogMultipleDeleteFragment;->getSelections()Ljava/lang/String;

    move-result-object v0

    const-string v2, "calllogids"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v2, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0701b5
        :pswitch_0
    .end packed-switch
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->onStopForSubClass()V

    return-void
.end method
