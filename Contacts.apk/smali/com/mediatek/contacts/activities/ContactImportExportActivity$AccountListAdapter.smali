.class Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ContactImportExportActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/activities/ContactImportExportActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccountListAdapter"
.end annotation


# instance fields
.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$1100(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/mediatek/contacts/model/AccountWithDataSetEx;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->getItem(I)Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, 0x0

    if-eqz p2, :cond_1

    move-object v1, p2

    check-cast v1, Landroid/widget/CheckedTextView;

    :goto_0
    iget-object v3, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$1100(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;

    iput-object v1, v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mView:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$800(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)I

    move-result v3

    if-ne v3, p1, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    return-object v1

    :cond_1
    iget-object v3, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f0400b5

    invoke-virtual {v3, v4, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    goto :goto_0
.end method
