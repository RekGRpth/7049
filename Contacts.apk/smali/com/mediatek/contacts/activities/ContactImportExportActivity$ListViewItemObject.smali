.class Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;
.super Ljava/lang/Object;
.source "ContactImportExportActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/activities/ContactImportExportActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListViewItemObject"
.end annotation


# instance fields
.field public mAccount:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

.field public mView:Landroid/widget/CheckedTextView;

.field final synthetic this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Lcom/mediatek/contacts/model/AccountWithDataSetEx;)V
    .locals 0
    .param p2    # Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mAccount:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mAccount:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    if-nez v1, :cond_1

    const-string v0, "null"

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mAccount:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mAccount:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/contacts/util/AccountFilterUtil;->getAccountDisplayNameByAccount(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mAccount:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v0, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method
