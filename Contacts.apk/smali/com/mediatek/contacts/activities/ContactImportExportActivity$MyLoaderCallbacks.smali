.class Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;
.super Ljava/lang/Object;
.source "ContactImportExportActivity.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/activities/ContactImportExportActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/mediatek/contacts/model/AccountWithDataSetEx;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;
    .param p2    # Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;-><init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/model/AccountWithDataSetEx;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountsLoader;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountsLoader;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->onLoadFinished(Landroid/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/model/AccountWithDataSetEx;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/model/AccountWithDataSetEx;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x1

    if-nez p2, :cond_1

    invoke-static {}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to load accounts"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$500(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v0, p2}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$502(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$500(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-virtual {v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->getStorageAccounts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$500(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v2, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    new-instance v1, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks$1;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks$1;-><init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_2
    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$600(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v0, v2}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$700(Lcom/mediatek/contacts/activities/ContactImportExportActivity;I)V

    :goto_1
    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$800(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$900(Lcom/mediatek/contacts/activities/ContactImportExportActivity;I)V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$1000(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$600(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$700(Lcom/mediatek/contacts/activities/ContactImportExportActivity;I)V

    goto :goto_1
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/model/AccountWithDataSetEx;",
            ">;>;)V"
        }
    .end annotation

    return-void
.end method
