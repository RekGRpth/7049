.class Lcom/mediatek/contacts/activities/EditSimContactActivity$6;
.super Landroid/telephony/PhoneStateListener;
.source "EditSimContactActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/activities/EditSimContactActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity$6;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 12
    .param p1    # Landroid/telephony/ServiceState;

    const/4 v11, 0x0

    const-string v8, "EditSimContactActivity"

    const-string v9, "IN onServiceStateChanged "

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity$6;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v8}, Landroid/app/Activity;->closeContextMenu()V

    const-string v8, "phone"

    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v6, 0x1

    const/4 v1, 0x1

    const/4 v2, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    invoke-static {v11}, Lcom/mediatek/contacts/SubContactsUtils;->simStateReady(I)Z

    move-result v5

    const-string v8, "EditSimContactActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sim1Ready IS "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "EditSimContactActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "before sim1RadioOn is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "EditSimContactActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "before hasSim1Card is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_0

    const/4 v8, 0x0

    :try_start_0
    invoke-interface {v3, v8}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z

    move-result v8

    if-nez v8, :cond_0

    const/4 v4, 0x0

    :cond_0
    if-eqz v3, :cond_1

    const/4 v8, 0x0

    invoke-interface {v3, v8}, Lcom/android/internal/telephony/ITelephony;->hasIccCardGemini(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-nez v8, :cond_1

    const/4 v1, 0x0

    :cond_1
    :goto_0
    const-string v8, "EditSimContactActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "after sim1RadioOn is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "EditSimContactActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "after hasSim1Card is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "EditSimContactActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "PhoneStateListener.onServiceStateChanged: serviceState="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v4, :cond_2

    const-string v8, "EditSimContactActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "TelephonyManager.getDefault().getSimStateGemini(com.android.internal.telephony.Phone.GEMINI_SIM_1) is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v10

    invoke-virtual {v10, v11}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x1

    if-eqz v1, :cond_2

    iget-object v8, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity$6;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v8}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v8

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity$6;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3902(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    iget-object v8, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity$6;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v8}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity$6;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v8}, Landroid/app/Activity;->finish()V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_0
.end method
