.class Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;
.super Ljava/lang/Thread;
.source "EditSimContactActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/activities/EditSimContactActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InsertSimContactThread"
.end annotation


# instance fields
.field public mCanceled:Z

.field mode:I

.field final synthetic this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;I)V
    .locals 2
    .param p2    # I

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const-string v0, "InsertSimContactThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->mCanceled:Z

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->mode:I

    iput p2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->mode:I

    const-string v0, "EditSimContactActivity"

    const-string v1, "InsertSimContactThread"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 73

    const/16 v33, 0x0

    const/16 v57, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$100()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$002(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$300()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$202(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "before replace - updatephone is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[run] befor replaceall updatephone : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "-"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$202(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$202(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[run] after replaceall updatephone : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "after replace - updatephone is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "tag"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, ""

    :goto_0
    invoke-virtual {v4, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "number"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, ""

    :goto_1
    invoke-virtual {v4, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "USIM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$600()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$502(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$800()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v7}, Lcom/android/contacts/ext/ContactAccountExtension;->updateContentValues(Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "before replace - update_additional_number is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[run] befor replaceall update_additional_number : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "-"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[run] after replaceall update_additional_number : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "after replace - update_additional_number is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "anr"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, ""

    :goto_2
    invoke-virtual {v4, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v3, "emails"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, ""

    :goto_3
    invoke-virtual {v4, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1100()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/android/contacts/ext/ContactAccountExtension;->updateContentValues(Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1100()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1202(Ljava/lang/String;)Ljava/lang/String;

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v3

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isPhoneBookReady(I)Z

    move-result v3

    iput-boolean v3, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhbReady:Z

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "the mPhbReady is = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-boolean v5, v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhbReady:Z

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " the mSlotId is = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->mode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1b

    const-string v2, "huibin"

    const-string v3, "thread mode == MODE_INSERT"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->iTel:Lcom/android/internal/telephony/ITelephony;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->iTel:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z

    move-result v2

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1402(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->iTel:Lcom/android/internal/telephony/ITelephony;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->iTel:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/android/internal/telephony/ITelephony;->isFDNEnabledGemini(I)Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1502(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    :cond_5
    const/4 v2, 0x5

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v3

    if-eq v2, v3, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1602(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "USIM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    :cond_7
    :goto_5
    return-void

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    :catch_0
    move-exception v47

    invoke-virtual/range {v47 .. v47}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_4

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    :cond_e
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    const-string v2, "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1902(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    :cond_f
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Z

    move-result v2

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2102(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    goto/16 :goto_5

    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    const-string v2, "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1802(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    goto :goto_6

    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, -0x1

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto/16 :goto_5

    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    const-string v2, "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1802(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    goto/16 :goto_7

    :cond_13
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v2

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1200()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v5, v6}, Lcom/android/contacts/ext/ContactAccountExtension;->isTextValid(Ljava/lang/String;II)Z

    move-result v2

    if-nez v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    goto/16 :goto_5

    :cond_14
    const-string v2, "EditSimContactActivity"

    const-string v3, "********BEGIN insert to SIM card "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/contacts/simcontact/SimCardUtils$SimUri;->getSimUri(I)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v11, v2, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v33

    const-string v2, "EditSimContactActivity"

    const-string v3, "********END insert to SIM card "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "values is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkUri is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v33

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    move-object/from16 v0, v33

    invoke-static {v2, v0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2300(Lcom/mediatek/contacts/activities/EditSimContactActivity;Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2102(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    goto/16 :goto_5

    :cond_15
    invoke-static/range {v33 .. v33}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v15

    const-string v2, "EditSimContactActivity"

    const-string v3, "insert to db"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v46, -0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "USIM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    const/16 v59, -0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v54

    :goto_8
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v45

    check-cast v45, Ljava/util/Map$Entry;

    invoke-interface/range {v45 .. v45}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v48

    invoke-interface/range {v45 .. v45}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v50

    check-cast v50, Ljava/lang/String;

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    move-object/from16 v0, v50

    invoke-static {v2, v0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->syncUSIMGroupNewIfMissing(ILjava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v59

    :goto_9
    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[USIM group]syncUSIMGroupNewIfMissing ugrpId:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v59

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v59, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    long-to-int v3, v15

    move/from16 v0, v59

    invoke-static {v2, v3, v0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->addUSIMGroupMember(III)Z

    move-result v58

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[USIM group]addUSIMGroupMember suFlag:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v58

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    :catch_1
    move-exception v43

    const/16 v59, -0x1

    goto :goto_9

    :catch_2
    move-exception v43

    invoke-virtual/range {v43 .. v43}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;->getErrorType()I

    move-result v46

    const/16 v59, -0x1

    goto :goto_9

    :cond_16
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->remove()V

    goto :goto_8

    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Landroid/accounts/Account;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2500()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2600()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v12, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->indicate:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/util/ArrayList;

    move-result-object v18

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1200()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v5 .. v19}, Lcom/mediatek/contacts/extention/aassne/SimUtils;->insertToDB(Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;JLjava/lang/String;JLjava/util/Set;Ljava/util/ArrayList;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v55

    if-nez v55, :cond_18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Landroid/accounts/Account;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2500()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2600()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v12, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->indicate:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v17

    invoke-static/range {v6 .. v17}, Lcom/mediatek/contacts/SubContactsUtils;->insertToDB(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;JLjava/lang/String;JLjava/util/Set;)Landroid/net/Uri;

    move-result-object v55

    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x0

    move/from16 v0, v46

    invoke-static {v2, v0, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2700(Lcom/mediatek/contacts/activities/EditSimContactActivity;ILjava/lang/String;)V

    const/4 v2, -0x1

    move/from16 v0, v46

    if-ne v0, v2, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/android/contacts/util/PhoneCapabilityTester;->isUsingTwoPanes(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1a

    new-instance v53, Landroid/content/Intent;

    invoke-direct/range {v53 .. v53}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, v53

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, v53

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, -0x1

    move-object/from16 v0, v53

    invoke-virtual {v2, v3, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    :cond_19
    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto/16 :goto_5

    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->startViewActivity(Landroid/net/Uri;)V

    goto :goto_a

    :cond_1b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->mode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    const-string v2, "EditSimContactActivity"

    const-string v3, "thread mode is MODE_EDIT"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2800(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1c

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$100()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_24

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2800(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_b
    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$102(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_25

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2800(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_c
    invoke-static {v3, v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$002(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$800()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1d

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[run -edit] befor replaceall update_additional_number : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "-"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[run -edit] after replaceall update_additional_number : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1d
    const-string v3, "newTag"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_26

    const-string v2, ""

    :goto_d
    invoke-virtual {v7, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "newNumber"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_27

    const-string v2, ""

    :goto_e
    invoke-virtual {v7, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/util/ArrayList;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    invoke-virtual/range {v5 .. v10}, Lcom/android/contacts/ext/ContactAccountExtension;->updateContentValues(Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/lang/String;I)Z

    const-string v3, "newEmails"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_28

    const-string v2, ""

    :goto_f
    invoke-virtual {v7, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1200()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v7, v3}, Lcom/mediatek/contacts/extention/aassne/SneExt;->buildNicknameValueForInsert(ILandroid/content/ContentValues;Ljava/lang/String;)Z

    const-string v2, "index"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updatevalues IS "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mode IS "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->mode:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v34, 0x0

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "indicate  is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->indicate:J

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->iTel:Lcom/android/internal/telephony/ITelephony;

    if-eqz v2, :cond_1e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->iTel:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z

    move-result v2

    if-nez v2, :cond_1e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1402(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->iTel:Lcom/android/internal/telephony/ITelephony;

    if-eqz v2, :cond_1f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->iTel:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/android/internal/telephony/ITelephony;->isFDNEnabledGemini(I)Z

    move-result v2

    if-eqz v2, :cond_1f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1502(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    :cond_1f
    const/4 v2, 0x5

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v3

    if-eq v2, v3, :cond_20

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1602(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_20
    :goto_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    sget-object v18, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/4 v2, 0x0

    const-string v3, "contact_id"

    aput-object v3, v19, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-virtual/range {v17 .. v22}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v32

    if-eqz v32, :cond_22

    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x0

    move-object/from16 v0, v32

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->contact_id:I

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "contact_id is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget v5, v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->contact_id:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_21
    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->close()V

    :cond_22
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "SIM"

    if-ne v2, v3, :cond_2b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_29

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_29

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/contacts/SubContactsUtils;->getUri(I)Landroid/net/Uri;

    move-result-object v52

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v68

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "where "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v68

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "iccUri ******** "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v52

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v52

    move-object/from16 v1, v68

    invoke-virtual {v2, v0, v1, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v38

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteDone is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v38

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    move/from16 v0, v38

    if-ne v0, v2, :cond_23

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget v3, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->contact_id:I

    int-to-long v5, v3

    invoke-static {v2, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v42

    invoke-virtual {v2, v0, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v37

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteDB is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v37

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto/16 :goto_5

    :cond_24
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$100()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2800(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_b

    :cond_25
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2800(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_c

    :cond_26
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_d

    :cond_27
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_e

    :cond_28
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_f

    :catch_3
    move-exception v47

    invoke-virtual/range {v47 .. v47}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_10

    :cond_29
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2a

    const-string v2, "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1802(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    :cond_2a
    :goto_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Z

    move-result v2

    if-eqz v2, :cond_32

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2102(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    goto/16 :goto_5

    :cond_2b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "USIM"

    if-ne v2, v3, :cond_2a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2d

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/contacts/SubContactsUtils;->getUri(I)Landroid/net/Uri;

    move-result-object v52

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v68

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "where "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v68

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "iccUri ******** "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v52

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v52

    move-object/from16 v1, v68

    invoke-virtual {v2, v0, v1, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v38

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteDone is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v38

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    move/from16 v0, v38

    if-ne v0, v2, :cond_2c

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget v3, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->contact_id:I

    int-to-long v5, v3

    invoke-static {v2, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v42

    invoke-virtual {v2, v0, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v37

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteDB is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v37

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto/16 :goto_5

    :cond_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_31

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_31

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2e

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_31

    :cond_2e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    :cond_2f
    :goto_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_30

    const-string v2, "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_30

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1902(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    :cond_30
    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mFixNumberInvalid is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_11

    :cond_31
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2f

    const-string v2, "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1802(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    goto :goto_12

    :cond_32
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v2

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1200()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v5, v6}, Lcom/android/contacts/ext/ContactAccountExtension;->isTextValid(Ljava/lang/String;II)Z

    move-result v2

    if-nez v2, :cond_33

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    goto/16 :goto_5

    :cond_33
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3000()[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v5, v6}, Lcom/android/contacts/ext/ContactAccountExtension;->getProjection(I[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/contacts/SubContactsUtils;->getUri(I)Landroid/net/Uri;

    move-result-object v18

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v17, v11

    invoke-virtual/range {v17 .. v22}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v34

    if-eqz v34, :cond_35

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/contacts/SubContactsUtils;->getUri(I)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v11, v2, v7, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v57

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updatevalues IS "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result IS "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v57

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    move/from16 v0, v57

    invoke-static {v2, v0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3100(Lcom/mediatek/contacts/activities/EditSimContactActivity;I)Z

    move-result v2

    if-eqz v2, :cond_34

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2102(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z

    goto/16 :goto_5

    :cond_34
    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->close()V

    :cond_35
    const-string v2, "EditSimContactActivity"

    const-string v3, "update to db"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v56, Landroid/content/ContentValues;

    invoke-direct/range {v56 .. v56}, Landroid/content/ContentValues;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "raw_contact_id = \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mimetype"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "vnd.android.cursor.item/name"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v71

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "wherename is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v71

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " updatename:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "   "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3b

    const-string v2, "data2"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v56

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v56

    move-object/from16 v1, v71

    invoke-virtual {v11, v2, v0, v1, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v66

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upname is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v66

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_36
    :goto_13
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "raw_contact_id = \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mimetype"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "is_additional_number"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v72

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " wherephone is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v72

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mOldPhone:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "|updatephone:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2500()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3d

    const-string v2, "data1"

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2500()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v72

    invoke-virtual {v11, v2, v0, v1, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v67

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upnumber is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v67

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_37
    :goto_14
    const/16 v46, -0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "USIM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_45

    new-instance v44, Landroid/content/ContentValues;

    invoke-direct/range {v44 .. v44}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "data2"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "raw_contact_id = \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mimetype"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v70

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "wheremail is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v70

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3f

    const-string v2, "data1"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v44

    move-object/from16 v1, v70

    invoke-virtual {v11, v2, v0, v1, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v65

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upemail is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v65

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_38
    :goto_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v24

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1200()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v0, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    move-wide/from16 v28, v0

    move-object/from16 v25, v11

    invoke-static/range {v23 .. v29}, Lcom/mediatek/contacts/extention/aassne/SneExt;->updateDataToDb(ILjava/lang/String;Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;J)V

    new-instance v31, Landroid/content/ContentValues;

    invoke-direct/range {v31 .. v31}, Landroid/content/ContentValues;-><init>()V

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3600(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/util/ArrayList;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v0, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    move-wide/from16 v28, v0

    const/16 v30, 0x1

    move-object/from16 v25, v11

    invoke-virtual/range {v23 .. v30}, Lcom/android/contacts/ext/ContactAccountExtension;->updateDataToDb(Ljava/lang/String;Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/util/ArrayList;JI)Z

    move-result v2

    if-nez v2, :cond_39

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "raw_contact_id = \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mimetype"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "is_additional_number"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " =1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v69

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "whereadditional is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v69

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_41

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_41

    const-string v2, "data1"

    invoke-static {}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2600()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, v69

    invoke-virtual {v11, v2, v0, v1, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v64

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upadditional is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v64

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_39
    :goto_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_43

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v51

    :goto_17
    invoke-interface/range {v51 .. v51}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_43

    invoke-interface/range {v51 .. v51}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v45

    check-cast v45, Ljava/util/Map$Entry;

    invoke-interface/range {v45 .. v45}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v48

    invoke-interface/range {v45 .. v45}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v50

    check-cast v50, Ljava/lang/String;

    const/16 v59, -0x1

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    move-object/from16 v0, v50

    invoke-static {v2, v0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->hasExistGroup(ILjava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_4

    move-result v59

    :goto_18
    if-lez v59, :cond_3a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)J

    move-result-wide v5

    long-to-int v3, v5

    move/from16 v0, v59

    invoke-static {v2, v3, v0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->deleteUSIMGroupMember(III)Z

    :cond_3a
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mimetype=\'vnd.android.cursor.item/group_membership\' AND raw_contact_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "data1"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v48

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v11, v2, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v35

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[USIM group]DB deleteCount:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v35

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_17

    :cond_3b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3c

    const-string v2, "raw_contact_id"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v56

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/name"

    move-object/from16 v0, v56

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "data2"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v56

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v56

    invoke-virtual {v11, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v62

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upNameUri is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v62

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_13

    :cond_3c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_36

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v71

    invoke-virtual {v11, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v40

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteName is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v40

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_13

    :cond_3d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3e

    const-string v2, "raw_contact_id"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "data1"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "is_additional_number"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "data2"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x3

    invoke-virtual/range {v20 .. v25}, Lcom/android/contacts/ext/ContactAccountExtension;->updateContentValues(Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/lang/String;I)Z

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v22

    invoke-virtual {v11, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v63

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upNumberUri is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v63

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_14

    :cond_3e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_37

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v72

    invoke-virtual {v11, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v41

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deletePhone is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v41

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_14

    :cond_3f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_40

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_40

    const-string v2, "raw_contact_id"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "data1"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v44

    invoke-virtual {v11, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v61

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upEmailUri is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v61

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_15

    :cond_40
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_38

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v70

    invoke-virtual {v11, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v39

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteEmail is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v39

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_15

    :cond_41
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_42

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$3700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_42

    const-string v2, "raw_contact_id"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "data1"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "is_additional_number"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "data2"

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v31

    invoke-virtual {v11, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v60

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upAdditionalUri is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v60

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_16

    :cond_42
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_39

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v69

    invoke-virtual {v11, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v36

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteAdditional is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v36

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_16

    :catch_4
    move-exception v43

    const/16 v59, -0x1

    goto/16 :goto_18

    :cond_43
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_45

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v2, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v54

    :cond_44
    :goto_19
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_45

    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v45

    check-cast v45, Ljava/util/Map$Entry;

    invoke-interface/range {v45 .. v45}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v48

    invoke-interface/range {v45 .. v45}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v50

    check-cast v50, Ljava/lang/String;

    const/16 v59, -0x1

    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    move-object/from16 v0, v50

    invoke-static {v2, v0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->syncUSIMGroupNewIfMissing(ILjava/lang/String;)I
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException; {:try_start_4 .. :try_end_4} :catch_6

    move-result v59

    :goto_1a
    if-lez v59, :cond_44

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)J

    move-result-wide v5

    long-to-int v3, v5

    move/from16 v0, v59

    invoke-static {v2, v3, v0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->addUSIMGroupMember(III)Z

    invoke-virtual/range {v31 .. v31}, Landroid/content/ContentValues;->clear()V

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/group_membership"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "data1"

    invoke-static/range {v48 .. v49}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "raw_contact_id"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v5, v3, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v31

    invoke-virtual {v11, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_19

    :catch_5
    move-exception v43

    const/16 v59, -0x1

    goto :goto_1a

    :catch_6
    move-exception v43

    invoke-virtual/range {v43 .. v43}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;->getErrorType()I

    move-result v46

    const/16 v59, -0x1

    goto :goto_1a

    :cond_45
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, 0x0

    move/from16 v0, v46

    invoke-static {v2, v0, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->access$2700(Lcom/mediatek/contacts/activities/EditSimContactActivity;ILjava/lang/String;)V

    const/4 v2, -0x1

    move/from16 v0, v46

    if-ne v0, v2, :cond_46

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    const/4 v3, -0x1

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    :cond_46
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;->this$0:Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto/16 :goto_5
.end method
