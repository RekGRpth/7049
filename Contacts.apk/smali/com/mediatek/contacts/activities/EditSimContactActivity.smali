.class public Lcom/mediatek/contacts/activities/EditSimContactActivity;
.super Landroid/app/Activity;
.source "EditSimContactActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;
    }
.end annotation


# static fields
.field private static final ADDRESS_BOOK_COLUMN_NAMES:[Ljava/lang/String;

.field public static final EDIT_SIM_CONTACT:Ljava/lang/String; = "com.android.contacts.action.EDIT_SIM_CONTACT"

.field private static final LISTEN_PHONE_NONE_STATES:I = 0x2

.field private static final LISTEN_PHONE_STATES:I = 0x1

.field static final MODE_DEFAULT:I = 0x0

.field static final MODE_EDIT:I = 0x2

.field static final MODE_INSERT:I = 0x1

.field private static final SIM_DATA:Ljava/lang/String; = "simData"

.field private static final SIM_NUM_PATTERN:Ljava/lang/String; = "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

.field private static final SIM_OLD_DATA:Ljava/lang/String; = "simOldData"

.field private static final TAG:Ljava/lang/String; = "EditSimContactActivity"

.field private static final USIM_EMAIL_PATTERN:Ljava/lang/String; = "[[0-9][a-z][A-Z][_]][[0-9][a-z][A-Z][-_.]]*@[[0-9][a-z][A-Z][-_.]]+"

.field private static mAfterOtherPhone:Ljava/lang/String;

.field private static mAfterPhone:Ljava/lang/String;

.field private static mEmail:Ljava/lang/String;

.field private static mName:Ljava/lang/String;

.field private static mNickname:Ljava/lang/String;

.field private static mOtherPhone:Ljava/lang/String;

.field private static mPhone:Ljava/lang/String;

.field private static mUpdateNickname:Ljava/lang/String;


# instance fields
.field contact_id:I

.field groupNum:I

.field final iTel:Lcom/android/internal/telephony/ITelephony;

.field indicate:J

.field private mAccount:Landroid/accounts/Account;

.field private mAccountName:Ljava/lang/String;

.field private mAccountType:Ljava/lang/String;

.field private mAirPlaneModeOn:Z

.field private mAirPlaneModeOnNotEdit:Z

.field private mAnrsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/contacts/ext/Anr;",
            ">;"
        }
    .end annotation
.end field

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mDoublePhoneNumber:Z

.field private mEmail2GInvalid:Z

.field private mEmailInvalid:Z

.field private mFDNEnabled:Z

.field private mFixNumberInvalid:Z

.field private mFixNumberLong:Z

.field private mGeneralFailure:Z

.field mGroupAddList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mIndexInSim:J

.field private mLookupUri:Landroid/net/Uri;

.field mMode:I

.field private mModemSwitchListener:Landroid/content/BroadcastReceiver;

.field private mNameLong:Z

.field private mNumberInvalid:Z

.field private mNumberIsNull:Z

.field private mNumberLong:Z

.field private mOldAnrsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/contacts/ext/Anr;",
            ">;"
        }
    .end annotation
.end field

.field private mOldEmail:Ljava/lang/String;

.field mOldGroupAddList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOldName:Ljava/lang/String;

.field private mOldNickname:Ljava/lang/String;

.field private mOldOtherPhone:Ljava/lang/String;

.field private mOldPhone:Ljava/lang/String;

.field private mOnBackGoing:Z

.field mPhbReady:Z

.field mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

.field private mPhoneTypeSuffix:Ljava/lang/String;

.field private mQuitEdit:Z

.field private mSIMInvalid:Z

.field private mSaveDialog:Landroid/app/ProgressDialog;

.field private mSaveFailToastStrId:I

.field private mSaveMode:I

.field private mSimType:Ljava/lang/String;

.field private mSlotId:I

.field private mStorageFull:Z

.field raw_contactId:J

.field private saveContactHandler:Landroid/os/Handler;

.field private simData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/contacts/model/EntityDelta;",
            ">;"
        }
    .end annotation
.end field

.field private simOldData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/contacts/model/EntityDelta;",
            ">;"
        }
    .end annotation
.end field

.field private updateName:Ljava/lang/String;

.field private update_additional_number:Ljava/lang/String;

.field private updatemail:Ljava/lang/String;

.field private updatephone:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterPhone:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterOtherPhone:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mName:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail:Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "number"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "emails"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "additionalNumber"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "groupIds"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->ADDRESS_BOOK_COLUMN_NAMES:[Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNickname:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdateNickname:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    const-wide/16 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->updateName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->updatephone:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->updatemail:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->update_additional_number:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    const-string v0, "SIM"

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimType:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    iput-wide v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndexInSim:J

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOn:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOnNotEdit:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFDNEnabled:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSIMInvalid:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberIsNull:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberInvalid:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberLong:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNameLong:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberLong:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mStorageFull:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v3, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOnBackGoing:Z

    iput-object v4, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhoneTypeSuffix:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmailInvalid:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail2GInvalid:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mDoublePhoneNumber:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveMode:I

    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->iTel:Lcom/android/internal/telephony/ITelephony;

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhbReady:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->indicate:J

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mMode:I

    iput-wide v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->contact_id:I

    iput v3, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->groupNum:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    iput-object v4, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->saveContactHandler:Landroid/os/Handler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAnrsList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldAnrsList:Ljava/util/ArrayList;

    new-instance v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$6;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$6;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$7;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$7;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$8;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$8;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$9;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$9;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mModemSwitchListener:Landroid/content/BroadcastReceiver;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldNickname:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->updateName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->updateName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAnrsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNickname:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdateNickname:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdateNickname:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    return v0
.end method

.method static synthetic access$1402(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOn:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFDNEnabled:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSIMInvalid:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberIsNull:Z

    return p1
.end method

.method static synthetic access$1802(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberInvalid:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberInvalid:Z

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->updatephone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText()Z

    move-result v0

    return v0
.end method

.method static synthetic access$202(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->updatephone:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2102(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOnBackGoing:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->showSaveFailToast()V

    return-void
.end method

.method static synthetic access$2300(Lcom/mediatek/contacts/activities/EditSimContactActivity;Landroid/net/Uri;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText2(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Landroid/accounts/Account;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$2500()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/mediatek/contacts/activities/EditSimContactActivity;ILjava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->showResultToastText(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhoneTypeSuffix:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)J
    .locals 2
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndexInSim:J

    return-wide v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3000()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->ADDRESS_BOOK_COLUMN_NAMES:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/mediatek/contacts/activities/EditSimContactActivity;I)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->updateFailToastText(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldNickname:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldAnrsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    return v0
.end method

.method static synthetic access$3902(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOnNotEdit:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->listenPhoneStates()V

    return-void
.end method

.method static synthetic access$4100(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->stopListenPhoneStates()V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->updatemail:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->updatemail:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->update_additional_number:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->update_additional_number:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterOtherPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    return-object v0
.end method

.method private doSaveAction(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x2

    const/4 v3, 0x1

    const-string v1, "EditSimContactActivity"

    const-string v2, "In doSaveAction "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-ne p1, v3, :cond_1

    const-string v1, "huibin"

    const-string v2, "doSaveAction mode == MODE_INSERT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "EditSimContactActivity"

    const-string v2, "mode == MODE_INSERT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->getsaveContactHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;

    invoke-direct {v1, p0, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v4, :cond_0

    const-string v1, "huibin"

    const-string v2, "doSaveAction mode == MODE_EDIT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->getsaveContactHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;

    invoke-direct {v1, p0, v4}, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private getsaveContactHandler()Landroid/os/Handler;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->saveContactHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "saveContacts"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->saveContactHandler:Landroid/os/Handler;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->saveContactHandler:Landroid/os/Handler;

    return-object v1
.end method

.method private listenPhoneStates()V
    .locals 4

    const/4 v3, 0x1

    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v3, v3}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    iget-object v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    return-void
.end method

.method private setSaveFailToastText()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSaveFailToastText mPhbReady is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhbReady:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhbReady:Z

    if-nez v2, :cond_1

    const v2, 0x7f0c0060

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    :cond_0
    :goto_0
    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSaveFailToastStrId IS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    if-ltz v2, :cond_b

    new-instance v1, Lcom/mediatek/contacts/activities/EditSimContactActivity$1;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$1;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_1
    return v0

    :cond_1
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOn:Z

    if-eqz v2, :cond_2

    const v2, 0x7f0c0063

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOn:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFDNEnabled:Z

    if-eqz v2, :cond_3

    const v2, 0x7f0c0064

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFDNEnabled:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_3
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSIMInvalid:Z

    if-eqz v2, :cond_4

    const v2, 0x7f0c0061

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSIMInvalid:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberIsNull:Z

    if-eqz v2, :cond_5

    const v2, 0x7f0c0062

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberIsNull:Z

    goto :goto_0

    :cond_5
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    if-eqz v2, :cond_6

    const v2, 0x7f0c0065

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    goto :goto_0

    :cond_6
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmailInvalid:Z

    if-eqz v2, :cond_7

    const v2, 0x7f0c0066

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmailInvalid:Z

    goto :goto_0

    :cond_7
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail2GInvalid:Z

    if-eqz v2, :cond_8

    const v2, 0x7f0c0067

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail2GInvalid:Z

    goto :goto_0

    :cond_8
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberInvalid:Z

    if-eqz v2, :cond_9

    const v2, 0x7f0c0068

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberInvalid:Z

    goto/16 :goto_0

    :cond_9
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOnNotEdit:Z

    if-eqz v2, :cond_a

    const v2, 0x7f0c0069

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOnNotEdit:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto/16 :goto_0

    :cond_a
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mDoublePhoneNumber:Z

    if-eqz v2, :cond_0

    const v2, 0x7f0c005f

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mDoublePhoneNumber:Z

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method private setSaveFailToastText2(Landroid/net/Uri;)Z
    .locals 5
    .param p1    # Landroid/net/Uri;

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-eqz p1, :cond_a

    const-string v2, "error"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, -0x1

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    const-string v2, "-1"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberLong:Z

    const v2, 0x7f0c006e

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberLong:Z

    :cond_0
    :goto_0
    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSaveFailToastText2 mSaveFailToastStrId IS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    if-ltz v2, :cond_9

    new-instance v1, Lcom/mediatek/contacts/activities/EditSimContactActivity$2;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$2;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_1
    :goto_1
    return v0

    :cond_2
    const-string v2, "-2"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNameLong:Z

    const v2, 0x7f0c006c

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNameLong:Z

    goto :goto_0

    :cond_3
    const-string v2, "-3"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mStorageFull:Z

    const v2, 0x7f0c006d

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mStorageFull:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_4
    const-string v2, "-6"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberLong:Z

    const v2, 0x7f0c006b

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberLong:Z

    goto :goto_0

    :cond_5
    const-string v2, "-10"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    const v2, 0x7f0c006f

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto/16 :goto_0

    :cond_6
    const-string v2, "-11"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    const v2, 0x7f0c0060

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto/16 :goto_0

    :cond_7
    const-string v2, "-12"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    const v2, 0x7f0c00d4

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    goto/16 :goto_0

    :cond_8
    const-string v2, "-13"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0c0070

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_1

    :cond_a
    if-eqz p1, :cond_1

    move v0, v1

    goto/16 :goto_1
.end method

.method private showResultToastText(ILjava/lang/String;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v6, -0x1

    const/4 v1, 0x0

    if-ne p1, v6, :cond_0

    const v2, 0x7f0c0149

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[mtk performance result]:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    move-object v0, v1

    if-ne p1, v6, :cond_3

    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->compleDate()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    return-void

    :cond_0
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    const v2, 0x7f0c0089

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    const v2, 0x7f0c008a

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "errorType "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not exist."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    if-ne p1, v6, :cond_4

    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->compleDate()Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v2, Lcom/mediatek/contacts/activities/EditSimContactActivity$4;

    invoke-direct {v2, p0, v0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$4;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_4
    new-instance v2, Lcom/mediatek/contacts/activities/EditSimContactActivity$5;

    invoke-direct {v2, p0, v0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$5;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method private showSaveFailToast()V
    .locals 1

    new-instance v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$10;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$10;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private stopListenPhoneStates()V
    .locals 4

    const/4 v3, 0x0

    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v3, v3}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    iget-object v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    return-void
.end method

.method private trimAnr(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    move-object v0, p1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "EditSimContactActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[run] befor replaceall additional_number : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EditSimContactActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[run] after replaceall additional_number : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0
.end method

.method private updateFailToastText(I)Z
    .locals 2
    .param p1    # I

    const/4 v1, -0x1

    const/4 v0, 0x1

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    if-ne p1, v1, :cond_1

    const v1, 0x7f0c006e

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    :cond_0
    :goto_0
    iget v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    if-ltz v1, :cond_8

    new-instance v1, Lcom/mediatek/contacts/activities/EditSimContactActivity$3;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$3;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_1
    return v0

    :cond_1
    const/4 v1, -0x2

    if-ne p1, v1, :cond_2

    const v1, 0x7f0c006c

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    goto :goto_0

    :cond_2
    const/4 v1, -0x3

    if-ne p1, v1, :cond_3

    const v1, 0x7f0c006d

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_3
    const/4 v1, -0x6

    if-ne p1, v1, :cond_4

    const v1, 0x7f0c006b

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    goto :goto_0

    :cond_4
    const/16 v1, -0xa

    if-ne p1, v1, :cond_5

    const v1, 0x7f0c006f

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_5
    const/16 v1, -0xb

    if-ne p1, v1, :cond_6

    const v1, 0x7f0c0060

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_6
    const/16 v1, -0xc

    if-ne p1, v1, :cond_7

    const v1, 0x7f0c00d4

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    goto :goto_0

    :cond_7
    const/16 v1, -0xd

    if-ne p1, v1, :cond_0

    const v1, 0x7f0c0070

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public backToFragment()V
    .locals 4

    const/4 v3, 0x0

    const-string v1, "EditSimContactActivity"

    const-string v2, "[backToFragment]"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "simData1"

    iget-object v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v1, "mQuitEdit"

    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v3, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iput-boolean v3, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public checkGroupNameStatus(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    :try_start_0
    const-string v3, "GBK"

    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    array-length v2, v3
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget v3, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    invoke-static {v3}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->getUSIMGrpMaxNameLen(I)I

    move-result v3

    if-le v2, v3, :cond_0

    const v3, 0x7f0c0089

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->backToFragment()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_0
.end method

.method public compleDate()Z
    .locals 8

    const/4 v2, 0x0

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mName:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v2, 0x1

    :cond_0
    :goto_0
    const/4 v4, 0x0

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v4, 0x1

    :cond_1
    :goto_1
    const/4 v0, 0x0

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v0, 0x1

    :cond_2
    :goto_2
    const/4 v3, 0x0

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v3, 0x1

    :cond_3
    :goto_3
    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v5, v6}, Ljava/util/AbstractMap;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v1, 0x1

    :cond_4
    :goto_4
    const-string v5, "EditSimContactActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[showResultToastText]compleName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | complePhone : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | compleOther : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | compleEmail: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | compleGroup : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "EditSimContactActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[showResultToastText] mName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | mOldName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | mEmail : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | mOldEmail : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_a

    if-eqz v4, :cond_a

    if-eqz v3, :cond_a

    if-eqz v0, :cond_a

    if-eqz v1, :cond_a

    const/4 v5, 0x1

    :goto_5
    return v5

    :cond_5
    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_6
    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v4, 0x1

    goto/16 :goto_1

    :cond_7
    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_8
    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v3, 0x1

    goto/16 :goto_3

    :cond_9
    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    if-nez v5, :cond_4

    const/4 v1, 0x1

    goto/16 :goto_4

    :cond_a
    const/4 v5, 0x0

    goto :goto_5
.end method

.method public fixIntent()Z
    .locals 28

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "the fixintent resolver = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mLookupUri:Landroid/net/Uri;

    move-object/from16 v23, v0

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "uri is "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/content/Intent;->resolveType(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v17

    const-string v24, "com.android.contacts"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    const-string v24, "vnd.android.cursor.item/contact"

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1

    invoke-static/range {v23 .. v23}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    move-object/from16 v0, v21

    invoke-static {v0, v6, v7}, Lcom/mediatek/contacts/SubContactsUtils;->queryForRawContactId(Landroid/content/ContentResolver;J)J

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    :cond_0
    :goto_0
    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "raw_contactId IS "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    move-wide/from16 v26, v0

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->raw_contactId:J

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x1

    cmp-long v24, v24, v26

    if-gez v24, :cond_2

    const-string v24, "EditSimContactActivity"

    const-string v25, "the raw_contactId is wrong"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v24, 0x7f0c0060

    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/widget/Toast;->show()V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    const/16 v24, 0x0

    :goto_1
    return v24

    :cond_1
    const-string v24, "vnd.android.cursor.item/raw_contact"

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    invoke-static/range {v23 .. v23}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->indicate:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v20

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "[fixIntent] oldcount:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->groupNum:I

    move/from16 v24, v0

    move/from16 v0, v24

    new-array v0, v0, [J

    move-object/from16 v19, v0

    const/16 v16, 0x0

    const/4 v14, 0x0

    :goto_2
    move/from16 v0, v20

    if-ge v14, v0, :cond_b

    const-string v25, "EditSimContactActivity"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "simOldData.get(0).getContentValues().get(i).getAsString(Data.MIMETYPE)   "

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v27, "mimetype"

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v25, "vnd.android.cursor.item/name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v26, "mimetype"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    :cond_3
    :goto_3
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    :cond_4
    const-string v25, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v26, "mimetype"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string v26, "AAS"

    invoke-virtual/range {v24 .. v26}, Lcom/android/contacts/ext/ContactAccountExtension;->isFeatureAccount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/ContentValues;

    invoke-static {v8}, Lcom/mediatek/contacts/extention/aassne/SimUtils;->isAdditionalNumber(Landroid/content/ContentValues;)Z

    move-result v24

    if-eqz v24, :cond_5

    new-instance v3, Lcom/android/contacts/ext/Anr;

    invoke-direct {v3}, Lcom/android/contacts/ext/Anr;-><init>()V

    const-string v24, "data1"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->trimAnr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    iput-object v0, v3, Lcom/android/contacts/ext/Anr;->mAdditionNumber:Ljava/lang/String;

    const-string v24, "data3"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    iput-object v0, v3, Lcom/android/contacts/ext/Anr;->mAasIndex:Ljava/lang/String;

    const-string v24, "_id"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    iput-wide v0, v3, Lcom/android/contacts/ext/Anr;->mId:J

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldAnrsList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_5
    const-string v24, "data1"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    goto/16 :goto_3

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data2"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const-string v25, "7"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    goto/16 :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    goto/16 :goto_3

    :cond_8
    const-string v25, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v26, "mimetype"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    goto/16 :goto_3

    :cond_9
    const-string v25, "vnd.android.cursor.item/group_membership"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v26, "mimetype"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    aput-wide v24, v19, v16

    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "mimetype"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/mediatek/contacts/extention/aassne/SneExt;->isNickname(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldNickname:Ljava/lang/String;

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "mOldNickname="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldNickname:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_b
    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "the mOldName is : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "   mOldOtherPhone : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "  mOldPhone:  "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " mOldEmail : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "[fixIntent] the indicate : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->indicate:J

    move-wide/from16 v26, v0

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " | the mSlotId : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v24, v0

    const-string v25, "USIM Account"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->groupNum:I

    move/from16 v24, v0

    move/from16 v0, v24

    new-array v11, v0, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->groupNum:I

    move/from16 v24, v0

    move/from16 v0, v24

    new-array v10, v0, [J

    move-object/from16 v0, v19

    array-length v5, v0

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "bufferGroupNum : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v24, "groupName"

    move-object/from16 v0, v24

    invoke-virtual {v15, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const-string v24, "groupId"

    move-object/from16 v0, v24

    invoke-virtual {v15, v0}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v10

    const/4 v14, 0x0

    :goto_4
    if-ge v14, v5, :cond_e

    const/4 v9, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->groupNum:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v9, v0, :cond_d

    aget-wide v24, v19, v14

    aget-wide v26, v10, v9

    cmp-long v24, v24, v26

    if-nez v24, :cond_c

    aget-object v22, v11, v9

    aget-wide v12, v19, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    move-object/from16 v24, v0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_c
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    :cond_d
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    :cond_e
    const/16 v24, 0x1

    goto/16 :goto_1
.end method

.method public onBackPressed()V
    .locals 2

    iget-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOnBackGoing:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOnBackGoing:Z

    const-string v0, "EditSimContactActivity"

    const-string v1, "[onBackPressed]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string v0, "EditSimContactActivity"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 33
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v20

    const-string v29, "simData"

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    const-string v29, "simOldData"

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simOldData:Ljava/util/ArrayList;

    const-string v29, "slotId"

    const/16 v30, -0x1

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v29

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    const-string v29, "indicate_phone_or_sim_contact"

    const-wide/16 v30, -0x1

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    move-wide/from16 v2, v30

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v29

    move-wide/from16 v0, v29

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->indicate:J

    const-string v29, "simIndex"

    const/16 v30, -0x1

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v29

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v29, v0

    move-wide/from16 v0, v29

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndexInSim:J

    const-string v29, "simSaveMode"

    const/16 v30, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v29

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveMode:I

    invoke-virtual/range {v20 .. v20}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mLookupUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getValues()Lcom/android/contacts/model/EntityDelta$ValuesDelta;

    move-result-object v29

    const-string v30, "account_type"

    invoke-virtual/range {v29 .. v30}, Lcom/android/contacts/model/EntityDelta$ValuesDelta;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v29, v0

    const-string v30, "USIM Account"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_0

    const-string v29, "groupNum"

    const/16 v30, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v29

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->groupNum:I

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "groupNum : "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->groupNum:I

    move/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getValues()Lcom/android/contacts/model/EntityDelta$ValuesDelta;

    move-result-object v29

    const-string v30, "account_name"

    invoke-virtual/range {v29 .. v30}, Lcom/android/contacts/model/EntityDelta$ValuesDelta;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v29, v0

    if-eqz v29, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountName:Ljava/lang/String;

    move-object/from16 v29, v0

    if-eqz v29, :cond_3

    new-instance v29, Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountName:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-direct/range {v29 .. v31}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccount:Landroid/accounts/Account;

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "the mSlotId is ="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " the indicate is ="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->indicate:J

    move-wide/from16 v31, v0

    invoke-virtual/range {v30 .. v32}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " the mSaveMode = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveMode:I

    move/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " the accounttype is = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " the uri is  = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mLookupUri:Landroid/net/Uri;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " | mIndexInSim : "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndexInSim:J

    move-wide/from16 v31, v0

    invoke-virtual/range {v30 .. v32}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "the resolver is = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v29, v0

    const/16 v30, -0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->indicate:J

    move-wide/from16 v29, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v29

    invoke-static {v0, v1, v2}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoById(Landroid/content/Context;J)Landroid/provider/Telephony$SIMInfo;

    move-result-object v19

    if-eqz v19, :cond_1

    move-object/from16 v0, v19

    iget v0, v0, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    :cond_1
    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "onCreate indicate is "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->indicate:J

    move-wide/from16 v31, v0

    invoke-virtual/range {v30 .. v32}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "onCreate mSlotId is "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v29, v0

    invoke-static/range {v29 .. v29}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isPhoneBookReady(I)Z

    move-result v29

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhbReady:Z

    const/16 v29, 0x2

    move/from16 v0, v29

    new-array v5, v0, [Ljava/lang/String;

    const/16 v29, 0x2

    move/from16 v0, v29

    new-array v8, v0, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->groupNum:I

    move/from16 v29, v0

    move/from16 v0, v29

    new-array v6, v0, [J

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v9

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "onCreate count:"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v18, 0x0

    :goto_0
    move/from16 v0, v18

    if-ge v0, v9, :cond_d

    const-string v30, "vnd.android.cursor.item/name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v31, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v31, "mimetype"

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v30, "data1"

    invoke-virtual/range {v29 .. v30}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    sput-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mName:Ljava/lang/String;

    :cond_2
    :goto_1
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    :cond_4
    :goto_2
    return-void

    :cond_5
    const-string v30, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v31, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v31, "mimetype"

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_9

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v30, v0

    const-string v31, "AAS"

    invoke-virtual/range {v29 .. v31}, Lcom/android/contacts/ext/ContactAccountExtension;->isFeatureAccount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v29

    if-eqz v29, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/ContentValues;

    invoke-static {v10}, Lcom/mediatek/contacts/extention/aassne/SimUtils;->isAdditionalNumber(Landroid/content/ContentValues;)Z

    move-result v29

    if-eqz v29, :cond_6

    new-instance v4, Lcom/android/contacts/ext/Anr;

    invoke-direct {v4}, Lcom/android/contacts/ext/Anr;-><init>()V

    const-string v29, "data1"

    move-object/from16 v0, v29

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->trimAnr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    iput-object v0, v4, Lcom/android/contacts/ext/Anr;->mAdditionNumber:Ljava/lang/String;

    const-string v29, "data3"

    move-object/from16 v0, v29

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    iput-object v0, v4, Lcom/android/contacts/ext/Anr;->mAasIndex:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAnrsList:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v29, 0x0

    const-string v30, "data1"

    move-object/from16 v0, v30

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    aput-object v30, v5, v29

    goto/16 :goto_1

    :cond_6
    const-string v29, "data1"

    move-object/from16 v0, v29

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    aput-object v29, v8, v24

    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_1

    :cond_7
    const-string v30, "7"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v31, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v31, "data2"

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v30, "data1"

    invoke-virtual/range {v29 .. v30}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    aput-object v29, v5, v22

    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_1

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v30, "data1"

    invoke-virtual/range {v29 .. v30}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    aput-object v29, v8, v24

    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_1

    :cond_9
    const-string v30, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v31, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v31, "mimetype"

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v30, "data1"

    invoke-virtual/range {v29 .. v30}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    sput-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail:Ljava/lang/String;

    goto/16 :goto_1

    :cond_a
    const-string v30, "vnd.android.cursor.item/group_membership"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v31, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v31, "mimetype"

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v30, "data1"

    invoke-virtual/range {v29 .. v30}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Long;->longValue()J

    move-result-wide v29

    aput-wide v29, v6, v23

    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_1

    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v30, "mimetype"

    invoke-virtual/range {v29 .. v30}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Lcom/mediatek/contacts/extention/aassne/SneExt;->isNickname(Ljava/lang/String;)Z

    move-result v29

    if-eqz v29, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->simData:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/contacts/model/EntityDelta;

    invoke-virtual/range {v29 .. v29}, Lcom/android/contacts/model/EntityDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/ContentValues;

    const-string v30, "data1"

    invoke-virtual/range {v29 .. v30}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    sput-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNickname:Ljava/lang/String;

    sget-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNickname:Ljava/lang/String;

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v29

    if-eqz v29, :cond_c

    const-string v29, ""

    :goto_3
    sput-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNickname:Ljava/lang/String;

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "mNickname:"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNickname:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_c
    sget-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNickname:Ljava/lang/String;

    goto :goto_3

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v29, v0

    const-string v30, "USIM Account"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->groupNum:I

    move/from16 v29, v0

    move/from16 v0, v29

    new-array v14, v0, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->groupNum:I

    move/from16 v29, v0

    move/from16 v0, v29

    new-array v13, v0, [J

    array-length v7, v6

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "bufferGroupNum : "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v29, "groupName"

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    const-string v29, "groupId"

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v13

    const/16 v18, 0x0

    :goto_4
    move/from16 v0, v18

    if-ge v0, v7, :cond_10

    const/4 v12, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->groupNum:I

    move/from16 v29, v0

    move/from16 v0, v29

    if-ge v12, v0, :cond_f

    aget-wide v29, v6, v18

    aget-wide v31, v13, v12

    cmp-long v29, v29, v31

    if-nez v29, :cond_e

    aget-object v27, v14, v12

    aget-wide v15, v6, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    move-object/from16 v29, v0

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_e
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    :cond_f
    add-int/lit8 v18, v18, 0x1

    goto :goto_4

    :cond_10
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/mediatek/contacts/ExtensionManager;->getContactDetailExtension()Lcom/android/contacts/ext/ContactDetailExtension;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v5, v8}, Lcom/android/contacts/ext/ContactDetailExtension;->isDoublePhoneNumber([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v29

    if-eqz v29, :cond_13

    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mDoublePhoneNumber:Z

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText()Z

    :goto_6
    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "the mName is = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mName:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " the mPhone is ="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " the buffer[] is "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const/16 v31, 0x0

    aget-object v31, v5, v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " the mOtherPhone is = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "the email is ="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->iTel:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v29, v0

    if-eqz v29, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->iTel:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v30, v0

    invoke-interface/range {v29 .. v30}, Lcom/android/internal/telephony/ITelephony;->getIccCardTypeGemini(I)Ljava/lang/String;

    move-result-object v29

    const-string v30, "USIM"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_11

    const-string v29, "USIM"

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimType:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_11
    :goto_7
    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "initial phone number "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    sput-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterPhone:Ljava/lang/String;

    sget-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v29

    if-nez v29, :cond_16

    sget-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    invoke-static/range {v29 .. v29}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    sput-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterPhone:Ljava/lang/String;

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "*********** after split phone number "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterPhone:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v29, "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

    sget-object v30, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterPhone:Ljava/lang/String;

    invoke-static/range {v29 .. v30}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v29

    if-nez v29, :cond_12

    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    :cond_12
    invoke-direct/range {p0 .. p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText()Z

    move-result v29

    if-eqz v29, :cond_16

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_2

    :cond_13
    const/16 v29, 0x1

    aget-object v29, v5, v29

    if-nez v29, :cond_14

    const/16 v29, 0x1

    aget-object v29, v8, v29

    if-eqz v29, :cond_15

    :cond_14
    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mDoublePhoneNumber:Z

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText()Z

    goto/16 :goto_6

    :cond_15
    const/16 v29, 0x0

    aget-object v29, v5, v29

    sput-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    const/16 v29, 0x0

    aget-object v29, v8, v29

    sput-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    goto/16 :goto_6

    :catch_0
    move-exception v11

    invoke-virtual {v11}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_7

    :cond_16
    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "initial mOtherPhone number "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    sput-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterOtherPhone:Ljava/lang/String;

    sget-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v29

    if-nez v29, :cond_18

    sget-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    invoke-static/range {v29 .. v29}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    sput-object v29, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterOtherPhone:Ljava/lang/String;

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "*********** after split mOtherPhone number "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterOtherPhone:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v29, "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

    sget-object v30, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAfterOtherPhone:Ljava/lang/String;

    invoke-static/range {v29 .. v30}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v29

    if-nez v29, :cond_17

    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText()Z

    move-result v29

    if-eqz v29, :cond_18

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_2

    :cond_18
    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "initial name is  "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mName:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveMode:I

    move/from16 v29, v0

    const/16 v30, 0x2

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_19

    const/16 v29, 0x2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mMode:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mLookupUri:Landroid/net/Uri;

    move-object/from16 v29, v0

    if-eqz v29, :cond_1a

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->fixIntent()Z

    move-result v21

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "isGoing : "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v21, :cond_4

    :cond_19
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v29, v0

    invoke-static/range {v29 .. v29}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->isServiceRunning(I)Z

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v29, v0

    invoke-static/range {v29 .. v29}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->getServiceState(I)I

    move-result v26

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "[onCreate] serviceState : "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " | hasImported : "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v17, :cond_1b

    const v29, 0x7f0c006a

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v28

    const/16 v29, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move/from16 v2, v29

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Landroid/widget/Toast;->show()V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_2

    :cond_1a
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_2

    :cond_1b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveMode:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->doSaveAction(I)V

    const-string v29, "EditSimContactActivity"

    const-string v30, "StructuredName.CONTENT_ITEM_TYPE = vnd.android.cursor.item/name"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v29, "EditSimContactActivity"

    const-string v30, "Phone.CONTENT_ITEM_TYPE = vnd.android.cursor.item/phone_v2"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v29, "EditSimContactActivity"

    const-string v30, "Email.CONTENT_ITEM_TYPE = vnd.android.cursor.item/email_v2"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v29, "EditSimContactActivity"

    const-string v30, "GroupMembership.CONTENT_ITEM_TYPE = vnd.android.cursor.item/group_membership"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v29, "EditSimContactActivity"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "the mName is = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mName:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " the mPhone is ="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhone:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " the buffer[] is "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const/16 v31, 0x0

    aget-object v31, v5, v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " the mOtherPhone is = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOtherPhone:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "the email is ="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public startViewActivity(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    return-void
.end method
