.class Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1;
.super Ljava/lang/Object;
.source "SpeedDialManageActivity.java"

# interfaces
.implements Landroid/widget/SimpleCursorAdapter$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/activities/SpeedDialManageActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1;->this$0:Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setViewValue(Landroid/view/View;Landroid/database/Cursor;I)Z
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/database/Cursor;
    .param p3    # I

    const/16 v4, 0x8

    const v10, 0x7f070197

    const/4 v9, 0x5

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v7, "SpeedDialManageActivity"

    const-string v8, "setViewValue() begin"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const/4 v7, 0x3

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const v7, 0x7f070198

    if-eq v3, v7, :cond_0

    const v7, 0x7f070196

    if-eq v3, v7, :cond_0

    if-ne v3, v10, :cond_7

    :cond_0
    if-eqz v0, :cond_3

    :goto_0
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    if-ne v3, v10, :cond_1

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1;->this$0:Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    invoke-virtual {p1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez v0, :cond_4

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    new-instance v4, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1$1;

    invoke-direct {v4, p0, p1, v1}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1$1;-><init>(Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_1
    :goto_1
    if-ne v3, v10, :cond_5

    :cond_2
    :goto_2
    return v6

    :cond_3
    move v4, v5

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    new-instance v4, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1$2;

    invoke-direct {v4, p0, v1}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1$2;-><init>(Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1;Landroid/view/View;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_5
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/contacts/ExtensionManager;->getSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;

    move-result-object v4

    iget-object v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1;->this$0:Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    invoke-static {v6}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->access$000(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)[Z

    move-result-object v6

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aget-boolean v6, v6, v7

    const v7, 0x7f070198

    invoke-virtual {v4, p1, v3, v6, v7}, Lcom/android/contacts/ext/SpeedDialExtension;->setView(Landroid/view/View;IZI)V

    :cond_6
    :goto_3
    move v6, v5

    goto :goto_2

    :cond_7
    const v7, 0x7f070199

    if-ne v3, v7, :cond_a

    if-eqz v0, :cond_8

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v7, "1"

    invoke-static {v4, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_8
    move v4, v6

    :goto_4
    invoke-virtual {p1, v4}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_3

    :cond_9
    move v4, v5

    goto :goto_4

    :cond_a
    const v7, 0x7f070195

    if-ne v3, v7, :cond_6

    invoke-virtual {p1, v5}, Landroid/view/View;->setClickable(Z)V

    if-eqz v0, :cond_b

    :goto_5
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    if-nez v0, :cond_2

    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "phone/sim indicator = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "int value = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {p2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1;->this$0:Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    invoke-static {v4}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->access$100(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)Lcom/android/contacts/ContactPhotoManager;

    move-result-object v4

    check-cast p1, Landroid/widget/ImageView;

    const/4 v5, 0x4

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v4, p1, v7, v8, v6}, Lcom/android/contacts/ContactPhotoManager;->loadThumbnail(Landroid/widget/ImageView;JZ)V

    goto/16 :goto_2

    :cond_b
    move v4, v5

    goto :goto_5
.end method
