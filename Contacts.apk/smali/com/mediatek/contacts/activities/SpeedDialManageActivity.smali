.class public Lcom/mediatek/contacts/activities/SpeedDialManageActivity;
.super Landroid/app/ListActivity;
.source "SpeedDialManageActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnShowListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/activities/SpeedDialManageActivity$QueryHandler;
    }
.end annotation


# static fields
.field private static final ADAPTER_TO:[I

.field static final BIND_DISPLAY_NAME_INDEX:I = 0x1

.field static final BIND_ID_INDEX:I = 0x0

.field static final BIND_INDICATE_PHONE_SIM_INDEX:I = 0x5

.field static final BIND_LABEL_INDEX:I = 0x2

.field static final BIND_NUMBER_INDEX:I = 0x3

.field static final BIND_PHOTO_ID_INDEX:I = 0x4

.field static final BIND_PROJECTION:[Ljava/lang/String;

.field private static final LIST_CAPACITY:I = 0x9

.field private static final MENU_REMOVE:I = 0x1

.field public static final PREF_NAME:Ljava/lang/String; = "speed_dial"

.field static final QUERY_CUSTOM_LABEL_INDEX:I = 0x6

.field static final QUERY_DISPLAY_NAME_INDEX:I = 0x1

.field static final QUERY_ID_INDEX:I = 0x0

.field static final QUERY_INDICATE_PHONE_SIM_INDEX:I = 0x4

.field static final QUERY_LABEL_INDEX:I = 0x2

.field static final QUERY_NUMBER_INDEX:I = 0x3

.field static final QUERY_PHOTO_ID_INDEX:I = 0x5

.field static final QUERY_PROJECTION:[Ljava/lang/String;

.field private static final QUERY_TOKEN:I = 0x2f

.field private static final REQUEST_CODE_PICK_CONTACT:I = 0x1

.field static final SPEED_DIAL_MAX:I = 0x9

.field static final SPEED_DIAL_MIN:I = 0x2

.field private static final SPEED_DIAL_NUMBER:I = 0x64

.field private static final TAG:Ljava/lang/String; = "SpeedDialManageActivity"

.field private static final VIEW_EDGE:I = 0x1e

.field private static final WAIT_CURSOR_DELAY_TIME:J = 0x1f4L

.field private static final WAIT_CURSOR_START:I = 0x3e8

.field private static final WAIT_SYMBOL_AS_STRING:Ljava/lang/String;

.field private static sIsQueryContact:Z


# instance fields
.field private mAdapter:Landroid/widget/SimpleCursorAdapter;

.field private mAddPosition:I

.field private mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

.field private mHandler:Landroid/os/Handler;

.field private mHasGotPref:Z

.field private mHasNumberByKey:Z

.field private mITel:Lcom/android/internal/telephony/ITelephony;

.field private mITelephony:Lcom/android/internal/telephony/ITelephony;

.field private mInputDialog:Landroid/app/Dialog;

.field private mIsWaitingActivityResult:Z

.field private mListView:Landroid/widget/ListView;

.field private mMatrixCursor:Landroid/database/MatrixCursor;

.field private mNeedRemovePosition:Z

.field private mPref:Landroid/content/SharedPreferences;

.field private mPrefMarkState:[I

.field private mPrefNumContactState:[Z

.field private mPrefNumState:[Ljava/lang/String;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mQueryHandler:Lcom/mediatek/contacts/activities/SpeedDialManageActivity$QueryHandler;

.field private mQueryTimes:I

.field private mRemoveConfirmDialog:Landroid/app/AlertDialog;

.field private mRemovePosition:I

.field private mSimInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

.field private mSlot:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x6

    const/4 v3, 0x0

    const/16 v0, 0x3b

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->WAIT_SYMBOL_AS_STRING:Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v5

    const-string v1, "type"

    aput-object v1, v0, v6

    const-string v1, "number"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "indicate_phone_or_sim_contact"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    const-string v1, "label"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->QUERY_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v5

    const-string v1, "type"

    aput-object v1, v0, v6

    const-string v1, "number"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "indicate_phone_or_sim_contact"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->BIND_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->ADAPTER_TO:[I

    sput-boolean v3, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->sIsQueryContact:Z

    return-void

    :array_0
    .array-data 4
        0x7f070194
        0x7f070199
        0x7f070196
        0x7f070198
        0x7f070195
        0x7f070197
    .end array-data
.end method

.method public constructor <init>()V
    .locals 7

    const/4 v6, 0x0

    const/16 v5, 0xa

    const/4 v4, -0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    iput v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    iput v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    iput-object v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemoveConfirmDialog:Landroid/app/AlertDialog;

    iput-boolean v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mNeedRemovePosition:Z

    iput-boolean v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mIsWaitingActivityResult:Z

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ""

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    new-array v0, v5, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumContactState:[Z

    new-array v0, v5, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    iput-boolean v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHasNumberByKey:Z

    iput-object v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iput v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mSlot:I

    new-instance v0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$5;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$5;-><init>(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHandler:Landroid/os/Handler;

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
    .end array-data
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)[Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumContactState:[Z

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)Lcom/android/contacts/ContactPhotoManager;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->updatePreferences()V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->showToastIfNecessary()V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)Landroid/widget/SimpleCursorAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAdapter:Landroid/widget/SimpleCursorAdapter;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->showProgressIndication()V

    return-void
.end method

.method static synthetic access$202(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    return p1
.end method

.method static synthetic access$302(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemoveConfirmDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$400(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    iget v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    return v0
.end method

.method static synthetic access$404(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    iget v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;ILandroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;
    .param p1    # I
    .param p2    # Landroid/database/Cursor;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->populateMatrixCursorRow(ILandroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)Landroid/database/MatrixCursor;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->goOnQuery()V

    return-void
.end method

.method static synthetic access$800()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->sIsQueryContact:Z

    return v0
.end method

.method static synthetic access$802(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->sIsQueryContact:Z

    return p0
.end method

.method static synthetic access$900(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/SpeedDialManageActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->dismissProgressIndication()V

    return-void
.end method

.method private dismissProgressIndication()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "SpeedDialManageActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dismiss exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private findKeyByNumber(Ljava/lang/String;)I
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v1, -0x1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    :goto_1
    const/16 v2, 0xa

    if-ge v0, v2, :cond_2

    const-string v2, "vnd.android.cursor.item/phone_v2"

    const-string v3, "vnd.android.cursor.item/phone_v2"

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-static {p0, v2, p1, v3, v4}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->shouldCollapse(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private getPrefStatus()V
    .locals 5

    const-string v1, "SpeedDialManageActivity"

    const-string v2, "getPrefStatus()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "speed_dial"

    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPref:Landroid/content/SharedPreferences;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHasGotPref:Z

    const/4 v0, 0x2

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    iget-object v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->offset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private goOnQuery()V
    .locals 9

    const/4 v2, 0x0

    iget v8, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    :goto_0
    const/16 v0, 0xa

    if-ge v8, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SpeedDialManageActivity"

    const-string v1, "log for empry block!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    iget v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v4, v8, -0x1

    invoke-static {p0, v0, v1, v4}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->populateMatrixCursorEmpty(Landroid/content/Context;Landroid/database/MatrixCursor;II)V

    const-string v0, "SpeedDialManageActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "goOnQuery(), mQueryTimes = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", end = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x9

    if-le v8, v0, :cond_1

    const-string v0, "SpeedDialManageActivity"

    const-string v1, "goOnQuery(), queryComplete in goOnQuery()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->sIsQueryContact:Z

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->dismissProgressIndication()V

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->updatePreferences()V

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->showToastIfNecessary()V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAdapter:Landroid/widget/SimpleCursorAdapter;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    :goto_1
    return-void

    :cond_1
    iput v8, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    const-string v0, "SpeedDialManageActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "goOnQuery(), startQuery at mQueryTimes = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SpeedDialManageActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "goOnQuery(), number = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    aget-object v4, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    aget-object v1, v1, v4

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v0, "SpeedDialManageActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "goOnQuery(), uri = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryHandler:Lcom/mediatek/contacts/activities/SpeedDialManageActivity$QueryHandler;

    const/16 v1, 0x2f

    sget-object v4, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->QUERY_PROJECTION:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private goOnQuery2()V
    .locals 9

    const/4 v3, 0x0

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    :goto_0
    iget v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    const/16 v2, 0xa

    if-ge v0, v2, :cond_4

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    aget-object v0, v0, v2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    add-int/lit8 v2, v2, -0x1

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    invoke-static {p0, v0, v2, v4}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->populateMatrixCursorEmpty(Landroid/content/Context;Landroid/database/MatrixCursor;II)V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPref:Landroid/content/SharedPreferences;

    iget v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v8, ""

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v2

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPref:Landroid/content/SharedPreferences;

    iget v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    invoke-static {v5}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->offset(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const/4 v8, -0x1

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    aput v4, v0, v2

    :cond_0
    :goto_1
    iget v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    add-int/lit8 v2, v2, -0x1

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    add-int/lit8 v4, v4, -0x1

    invoke-static {p0, v0, v2, v4}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->populateMatrixCursorEmpty(Landroid/content/Context;Landroid/database/MatrixCursor;II)V

    const-string v0, "SpeedDialManageActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "goOnQuery2(), startQuery at mQueryTimes = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SpeedDialManageActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "goOnQuery2(), number = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    aget-object v2, v2, v4

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v0, "SpeedDialManageActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "goOnQuery2(), uri = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->QUERY_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    iget v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0, v6}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->populateMatrixCursorRow(ILandroid/database/Cursor;)V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumContactState:[Z

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    const/4 v4, 0x1

    aput-boolean v4, v0, v2

    :goto_2
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :cond_2
    const-string v0, "SpeedDialManageActivity"

    const-string v2, "goOnQuery2(), query, get nothing after query "

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/contacts/ExtensionManager;->getSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/contacts/ext/SpeedDialExtension;->needClearPreState()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    add-int/lit8 v2, v2, -0x1

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    invoke-static {p0, v0, v2, v4}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->populateMatrixCursorEmpty(Landroid/content/Context;Landroid/database/MatrixCursor;II)V

    iget v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->clearPrefStateIfNecessary(I)V

    :goto_3
    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumContactState:[Z

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    const/4 v4, 0x0

    aput-boolean v4, v0, v2

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    invoke-virtual {p0, p0, v0, v2}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->populateMatrixCursorEmpty(Landroid/content/Context;Landroid/database/MatrixCursor;I)V

    goto :goto_3

    :cond_4
    const-string v0, "SpeedDialManageActivity"

    const-string v2, "goOnQuery2(), updatePreferences before "

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->updatePreferences()V

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->showToastIfNecessary()V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAdapter:Landroid/widget/SimpleCursorAdapter;

    iget-object v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    invoke-virtual {v0, v2}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method private initMatrixCursor()V
    .locals 6

    const/4 v5, 0x2

    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->BIND_PROJECTION:[Ljava/lang/String;

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c016d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, ""

    aput-object v2, v1, v5

    const/4 v2, 0x3

    const-string v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    iput v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryTimes:I

    return-void
.end method

.method private isSimReady(I)Z
    .locals 8
    .param p1    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    const-string v5, "SpeedDialManageActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isSimReady(), simId=  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    int-to-long v5, p1

    :try_start_0
    invoke-static {p0, v5, v6}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v2

    const-string v5, "SpeedDialManageActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isSimReady(), slotId=  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, -0x1

    if-eq v5, v2, :cond_0

    const/4 v5, 0x5

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v6

    if-ne v5, v6, :cond_3

    move v1, v4

    :goto_1
    const-string v5, "SpeedDialManageActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isSimReady(), mITel.isSimInsert(slotId)=  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v7, v2}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "SpeedDialManageActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isSimReady(), mITel.isRadioOnGemini(slotId)=  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v7, v2}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "SpeedDialManageActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isSimReady(), mITel.isFDNEnabledGemini(slotId)=  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v7, v2}, Lcom/android/internal/telephony/ITelephony;->isFDNEnabledGemini(I)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "SpeedDialManageActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isSimReady(), meLock=  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "SpeedDialManageActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isSimReady(), ContactsUtils.isServiceRunning[slotId]=  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/contacts/ContactsUtils;->isServiceRunning:[Z

    aget-boolean v7, v7, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v5, v2}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v5, v2}, Lcom/android/internal/telephony/ITelephony;->hasIccCardGemini(I)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v5, v2}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v5, v2}, Lcom/android/internal/telephony/ITelephony;->isFDNEnabledGemini(I)Z

    move-result v5

    if-nez v5, :cond_0

    if-eqz v1, :cond_0

    sget-object v5, Lcom/android/contacts/ContactsUtils;->isServiceRunning:[Z

    aget-boolean v5, v5, v2

    if-nez v5, :cond_0

    :cond_2
    move v3, v4

    goto/16 :goto_0

    :cond_3
    move v1, v3

    goto/16 :goto_1

    :cond_4
    iget-object v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v5, v2}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v5, v2}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v5, v2}, Lcom/android/internal/telephony/ITelephony;->isFDNEnabledGemini(I)Z

    move-result v5

    if-nez v5, :cond_0

    if-eqz v1, :cond_0

    sget-object v5, Lcom/android/contacts/ContactsUtils;->isServiceRunning:[Z

    aget-boolean v5, v5, v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v5, :cond_0

    :cond_5
    move v3, v4

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v4, "SpeedDialManageActivity"

    const-string v5, "RemoteException!"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static offset(I)I
    .locals 1
    .param p0    # I

    add-int/lit8 v0, p0, 0x64

    return v0
.end method

.method static populateMatrixCursorEmpty(Landroid/content/Context;Landroid/database/MatrixCursor;II)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/MatrixCursor;
    .param p2    # I
    .param p3    # I

    move v0, p2

    :goto_0
    if-ge v0, p3, :cond_0

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    add-int/lit8 v3, v0, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0032

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "-1"

    aput-object v3, v1, v2

    invoke-virtual {p1, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private populateMatrixCursorRow(ILandroid/database/Cursor;)V
    .locals 11
    .param p1    # I
    .param p2    # Landroid/database/Cursor;

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v7, 0x1

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x2

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const-string v0, ""

    if-nez v6, :cond_2

    const/4 v7, 0x6

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v7, 0x3

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v7, 0x5

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v5, -0x1

    const/4 v7, 0x4

    invoke-interface {p2, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-nez v7, :cond_0

    const/4 v7, 0x4

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    :cond_0
    const-string v7, "SpeedDialManageActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "populateMatrixCursorRow(), name = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", label = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", number = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " photoId:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "simId: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v5, :cond_1

    invoke-virtual {p0, v5}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->getSimType(I)J

    move-result-wide v3

    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    add-int/lit8 v8, p1, 0x1

    invoke-static {p0, v7, p1, v8}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->populateMatrixCursorEmpty(Landroid/content/Context;Landroid/database/MatrixCursor;II)V

    iget-object v7, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget-object v8, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, p1

    iget-object v7, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    iget-object v8, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->offset(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, -0x1

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    aput v8, v7, p1

    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v6, v8}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    iget-object v7, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    add-int/lit8 v10, p1, 0x1

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    aput-object v2, v8, v9

    const/4 v9, 0x4

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v7, v8}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static final shouldCollapse(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Ljava/lang/CharSequence;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-static {v5, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-static {v5, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    if-ne p2, p4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    if-eqz p2, :cond_2

    if-nez p4, :cond_3

    :cond_2
    move v3, v4

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->WAIT_SYMBOL_AS_STRING:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->WAIT_SYMBOL_AS_STRING:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v5, v0

    array-length v6, v1

    if-eq v5, v6, :cond_4

    move v3, v4

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    :goto_1
    array-length v5, v0

    if-ge v2, v5, :cond_0

    aget-object v5, v0, v2

    aget-object v6, v1, v2

    invoke-static {p0, v5, v6}, Landroid/telephony/PhoneNumberUtils;->compare(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    move v3, v4

    goto :goto_0

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    if-ne p1, p3, :cond_7

    if-eq p2, p4, :cond_0

    :cond_7
    invoke-static {p1, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-static {p2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_8
    move v3, v4

    goto :goto_0
.end method

.method private showProgressIndication()V
    .locals 3

    const-string v0, "SpeedDialManageActivity"

    const-string v1, "loading contacts... "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->dismissProgressIndication()V

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0284

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private showToastIfNecessary()V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, -0x1

    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "showToastIfNecessary(),  mAddPosition= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "showToastIfNecessary(),  mHasNumberByKey= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHasNumberByKey:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "showToastIfNecessary(),  mIsWaitingActivityResult= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mIsWaitingActivityResult:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mIsWaitingActivityResult:Z

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/contacts/ExtensionManager;->getSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    iget-boolean v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mNeedRemovePosition:Z

    invoke-virtual {v4, v5, v6}, Lcom/android/contacts/ext/SpeedDialExtension;->setAddPosition(IZ)I

    move-result v4

    iput v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    if-eq v4, v7, :cond_0

    iget-boolean v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHasNumberByKey:Z

    if-eqz v4, :cond_2

    iput v7, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    invoke-virtual {v4, v7}, Landroid/database/AbstractCursor;->moveToPosition(I)Z

    const v4, 0x7f0c004a

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "showToastIfNecessary(), mMatrixCursor\'s present position: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    invoke-virtual {v6}, Landroid/database/AbstractCursor;->getPosition()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mMatrixCursor\'s count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    invoke-virtual {v6}, Landroid/database/MatrixCursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mPosition + 1: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    iget v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    invoke-virtual {v4, v5}, Landroid/database/AbstractCursor;->moveToPosition(I)Z

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    invoke-virtual {v4, v8}, Landroid/database/MatrixCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    invoke-virtual {v4, v9}, Landroid/database/MatrixCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    add-int/lit8 v1, v4, 0x1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const v4, 0x7f0c0034

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v3, v5, v10

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    iput v7, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    invoke-virtual {v4, v7}, Landroid/database/AbstractCursor;->moveToPosition(I)Z

    goto/16 :goto_0

    :cond_3
    const v4, 0x7f0c0033

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v10

    aput-object v2, v5, v8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private startQuery()V
    .locals 4

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->initMatrixCursor()V

    const-string v0, "SpeedDialManageActivity"

    const-string v1, "startQuery(), query init"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->sIsQueryContact:Z

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->goOnQuery()V

    return-void
.end method

.method private updatePreferences()V
    .locals 7

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v1, 0x2

    :goto_0
    const/16 v4, 0xa

    if-ge v1, v4, :cond_3

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    aget v2, v4, v1

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->isSimReady(I)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    const/4 v3, 0x1

    :goto_1
    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updatePreferences(), isSimReady("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v1}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->offset(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    aget v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method


# virtual methods
.method actuallyRemove()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    add-int/lit8 v2, v2, 0x1

    const-string v3, ""

    aput-object v3, v1, v2

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    add-int/lit8 v2, v2, 0x1

    const/4 v3, -0x1

    aput v3, v1, v2

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->offset(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    iget v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    add-int/lit8 v3, v3, 0x1

    aget v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->startQuery()V

    return-void
.end method

.method clearPrefStateIfNecessary(I)V
    .locals 5
    .param p1    # I

    const/4 v4, -0x1

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    aget v0, v1, p1

    const-string v1, "SpeedDialManageActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clearPrefStateIfNecessary(), simId=  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; queryTimes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq v0, v4, :cond_0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->isSimReady(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string v1, "SpeedDialManageActivity"

    const-string v2, "clearPrefStateIfNecessary(), isSImReady"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    aput v4, v1, p1

    iget-object v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, p1

    :cond_1
    return-void
.end method

.method confirmRemovePosition(I)V
    .locals 11
    .param p1    # I

    const v10, 0x7f0c0035

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "confirmRemovePosition(), position= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAdapter:Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {v4, p1}, Landroid/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "confirmRemovePosition(), name= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const v4, 0x7f0c0037

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v3, v5, v9

    add-int/lit8 v6, p1, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "confirmRemovePosition(), message= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemoveConfirmDialog:Landroid/app/AlertDialog;

    if-nez v4, :cond_1

    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$4;

    invoke-direct {v5, p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$4;-><init>(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1080027

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$3;

    invoke-direct {v5, p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$3;-><init>(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)V

    invoke-virtual {v4, v10, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x1040000

    new-instance v6, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$2;

    invoke-direct {v6, p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$2;-><init>(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemoveConfirmDialog:Landroid/app/AlertDialog;

    :cond_1
    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemoveConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :cond_2
    const v4, 0x7f0c0036

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v9

    aput-object v1, v5, v7

    add-int/lit8 v6, p1, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public getSimType(I)J
    .locals 7
    .param p1    # I

    const-wide/16 v1, 0x0

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mSimInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    if-nez v4, :cond_0

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mSimInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    :cond_0
    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mSimInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-virtual {v4, p1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v4

    iput v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mSlot:I

    const/4 v0, -0x1

    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[getSimType] mSlot = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mSlot:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mSimInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mSlot:I

    invoke-virtual {v4, v5}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimInfoBySlot(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    iget v0, v3, Landroid/provider/Telephony$SIMInfo;->mColor:I

    :cond_1
    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[getSimType] i = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    const-string v4, "SpeedDialManageActivity"

    const-string v5, "no match color"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v1, -0x1

    :goto_0
    const-string v4, "SpeedDialManageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[getSimType] photoId : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-wide v1

    :pswitch_0
    const-wide/16 v1, -0xa

    goto :goto_0

    :pswitch_1
    const-wide/16 v1, -0xb

    goto :goto_0

    :pswitch_2
    const-wide/16 v1, -0xc

    goto :goto_0

    :pswitch_3
    const-wide/16 v1, -0xd

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 13
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const-string v0, "SpeedDialManageActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onActivityResult]mAddPosition:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mIsWaitingActivityResult:Z

    const/4 v0, 0x1

    if-ne v0, p1, :cond_0

    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    if-nez p3, :cond_2

    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    const-string v9, ""

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "data1"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Data._ID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {v9}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v0, "SpeedDialManageActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult(), uri = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "indicate_phone_or_sim_contact"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    const/4 v10, -0x1

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    if-eqz v11, :cond_3

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    invoke-interface {v11, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    :cond_3
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->getPrefStatus()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHasGotPref:Z

    invoke-direct {p0, v9}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->findKeyByNumber(Ljava/lang/String;)I

    move-result v12

    const-string v0, "SpeedDialManageActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult(),  after findKeyByNumber(), tempKey="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    if-le v0, v12, :cond_6

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    add-int/lit8 v2, v2, 0x1

    aput-object v9, v0, v2

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    iget v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    add-int/lit8 v2, v2, 0x1

    aput v10, v0, v2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHasNumberByKey:Z

    :goto_1
    const-string v0, "SpeedDialManageActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult: number = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", simId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    :goto_2
    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    array-length v0, v0

    if-ge v8, v0, :cond_1

    const-string v0, "SpeedDialManageActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mPrefNumState["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mPrefMarkState["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefMarkState:[I

    aget v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_4
    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHasNumberByKey:Z

    goto :goto_1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v4, -0x1

    const/4 v5, 0x0

    if-ne p2, v4, :cond_0

    check-cast p1, Landroid/app/AlertDialog;

    const v3, 0x7f07008b

    invoke-virtual {p1, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mNeedRemovePosition:Z

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->findKeyByNumber(Ljava/lang/String;)I

    move-result v3

    if-ne v4, v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    add-int/lit8 v4, v4, 0x1

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->offset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iput-boolean v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHasNumberByKey:Z

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->startQuery()V

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHasNumberByKey:Z

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->showToastIfNecessary()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f070197

    if-ne v3, v4, :cond_3

    const/4 v2, -0x1

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/AdapterView;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-ne v3, v4, :cond_2

    move v2, v0

    :cond_0
    const-string v3, "SpeedDialManageActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onClick(),  before confirmRemovePosition(), position= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v3

    add-int/2addr v3, v2

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->confirmRemovePosition(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f070193

    if-ne v3, v4, :cond_1

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mNeedRemovePosition:Z

    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.GET_CONTENT"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    const-string v3, "SpeedDialManageActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[startActivityForResult] mAddPosition:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v1

    check-cast v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    move-object v0, v1

    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iget v1, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->confirmRemovePosition(I)V

    const/4 v1, 0x1

    return v1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "SpeedDialManageActivity"

    const-string v1, "onCreate() , begin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    invoke-static {p0}, Lcom/android/contacts/ContactPhotoManager;->getInstance(Landroid/content/Context;)Lcom/android/contacts/ContactPhotoManager;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    new-instance v0, Landroid/widget/SimpleCursorAdapter;

    const v2, 0x7f0400b8

    const/4 v3, 0x0

    sget-object v4, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->BIND_PROJECTION:[Ljava/lang/String;

    sget-object v5, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->ADAPTER_TO:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAdapter:Landroid/widget/SimpleCursorAdapter;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAdapter:Landroid/widget/SimpleCursorAdapter;

    new-instance v1, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$1;-><init>(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/SimpleCursorAdapter;->setViewBinder(Landroid/widget/SimpleCursorAdapter$ViewBinder;)V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAdapter:Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {p0, v0}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$QueryHandler;

    invoke-direct {v0, p0, p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity$QueryHandler;-><init>(Lcom/mediatek/contacts/activities/SpeedDialManageActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mQueryHandler:Lcom/mediatek/contacts/activities/SpeedDialManageActivity$QueryHandler;

    const-string v0, "SpeedDialManageActivity"

    const-string v1, "onCreate() , end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 11
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    const/4 v10, 0x1

    const/4 v9, 0x0

    const-string v6, "SpeedDialManageActivity"

    const-string v7, "context menu created"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    move-object v0, p3

    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_1

    const-string v6, "SpeedDialManageActivity"

    const-string v7, "bad menuInfo"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v2

    const-string v6, "SpeedDialManageActivity"

    const-string v7, "bad menuInfo"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    const-string v6, "SpeedDialManageActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onCreateContextMenu(), info.position="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v6

    iget v7, v3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-interface {v6, v7}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    if-eqz v1, :cond_0

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x3

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "SpeedDialManageActivity"

    const-string v7, "What about really creating?"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v6, 0x7f0c0035

    invoke-interface {p1, v9, v10, v9, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_0

    :cond_2
    invoke-interface {p1, v5}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    const-string v0, "SpeedDialManageActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemoveConfirmDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemoveConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemoveConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemoveConfirmDialog:Landroid/app/AlertDialog;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mMatrixCursor:Landroid/database/MatrixCursor;

    invoke-virtual {v0}, Landroid/database/AbstractCursor;->close()V

    :cond_1
    return-void
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 7
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    const/4 v6, 0x1

    const-string v3, "SpeedDialManageActivity"

    const-string v4, "onListItemClick"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/contacts/ExtensionManager;->getSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/contacts/ext/SpeedDialExtension;->showSpeedInputDialog()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v4, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mInputDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mInputDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v3, 0x7f0c0045

    invoke-virtual {v0, v3, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v3, 0x1040000

    invoke-virtual {v0, v3, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v3, 0x7f0400b7

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mInputDialog:Landroid/app/Dialog;

    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mInputDialog:Landroid/app/Dialog;

    invoke-virtual {v3, p0}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    iget-object v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mInputDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    iput-boolean v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mNeedRemovePosition:Z

    goto :goto_0

    :cond_3
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->isProcessing(I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "SpeedDialManageActivity"

    const-string v4, "delete or copy is processing "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v3, 0x7f0c0060

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.GET_CONTENT"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    iput-boolean v6, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mIsWaitingActivityResult:Z

    const-string v3, "SpeedDialManageActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[onListItemClick]mIsWaitingActivityResult:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mIsWaitingActivityResult:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v1, v6}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    const-string v3, "SpeedDialManageActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[startActivityForResult]mAddPosition:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mInputDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mInputDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mInputDialog:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "add_position"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    const-string v0, "mNeedRemovePosition"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mNeedRemovePosition:Z

    const-string v0, "mRemovePosition"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    const-string v0, "mIsWaitingActivityResult"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mIsWaitingActivityResult:Z

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "SpeedDialManageActivity"

    const-string v1, "onResume begin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    iget-boolean v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHasGotPref:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->getPrefStatus()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mHasGotPref:Z

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->startQuery()V

    iget v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->confirmRemovePosition(I)V

    :cond_1
    const-string v0, "SpeedDialManageActivity"

    const-string v1, "onResume end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, -0x1

    iget v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    if-eq v0, v2, :cond_0

    const-string v0, "add_position"

    iget v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "mNeedRemovePosition"

    iget-boolean v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mNeedRemovePosition:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    iget v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    if-eq v0, v2, :cond_1

    const-string v0, "mRemovePosition"

    iget v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mRemovePosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mIsWaitingActivityResult:Z

    if-eqz v0, :cond_2

    const-string v0, "mIsWaitingActivityResult"

    iget-boolean v1, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mIsWaitingActivityResult:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onShow(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;

    move-object v2, p1

    check-cast v2, Landroid/app/AlertDialog;

    const v3, 0x7f07008b

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    iget v3, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mAddPosition:I

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    check-cast p1, Landroid/app/AlertDialog;

    const v2, 0x7f070193

    invoke-virtual {p1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onStop()V
    .locals 2

    const-string v0, "SpeedDialManageActivity"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    sget-boolean v0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->sIsQueryContact:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->dismissProgressIndication()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->sIsQueryContact:Z

    :cond_0
    return-void
.end method

.method populateMatrixCursorEmpty(Landroid/content/Context;Landroid/database/MatrixCursor;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/MatrixCursor;
    .param p3    # I

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    aget-object v2, v2, p3

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/mediatek/contacts/activities/SpeedDialManageActivity;->mPrefNumState:[Ljava/lang/String;

    aget-object v2, v2, p3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "0"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "-1"

    aput-object v2, v0, v1

    invoke-virtual {p2, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    return-void
.end method
