.class public Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;
.super Ljava/lang/Object;
.source "CallLogSimInfoHelper.java"


# static fields
.field static final SLOT_ID_FIRST:I = 0x0

.field static final SLOT_ID_SECOND:I = 0x1

.field private static final TAG:Ljava/lang/String; = "CallLogSimInfoHelper"


# instance fields
.field private mDrawableSimColor1:Landroid/graphics/drawable/Drawable;

.field private mDrawableSimColor2:Landroid/graphics/drawable/Drawable;

.field private mDrawableSimLockedColor:Landroid/graphics/drawable/Drawable;

.field private mDrawableSimSipColor:Landroid/graphics/drawable/Drawable;

.field private mInsertSimColor1:I

.field private mInsertSimColor2:I

.field private mResources:Landroid/content/res/Resources;

.field private mSipCallDisplayName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .param p1    # Landroid/content/res/Resources;

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSipCallDisplayName:Ljava/lang/String;

    iput v1, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mInsertSimColor1:I

    iput v1, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mInsertSimColor2:I

    iput-object p1, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    return-void
.end method

.method public static getSimIdBySlotID(I)I
    .locals 4
    .param p0    # I

    const/4 v3, -0x1

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getInsertedSimInfoList()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/provider/Telephony$SIMInfo;

    iget v2, v2, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-ne p0, v2, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/provider/Telephony$SIMInfo;

    iget-wide v2, v2, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    long-to-int v2, v2

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "CallLogSimInfoHelper"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public getSimColorDrawableById(I)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1    # I

    const/4 v4, 0x0

    const/4 v7, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getSimColorDrawableById() simId == ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->log(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f090061

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v2, v5

    const/4 v5, -0x2

    if-ne v5, p1, :cond_2

    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimSipColor:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    const v5, 0x20200d3

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, v2, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    invoke-direct {v4, v5, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimSipColor:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimSipColor:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    :cond_1
    :goto_0
    return-object v4

    :cond_2
    if-eqz p1, :cond_1

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getInsertedSimColorById(I)I

    move-result v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getSimColorDrawableById() color == ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->log(Ljava/lang/String;)V

    const/4 v5, -0x1

    if-eq v5, v1, :cond_8

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSlotIdBySimId(I)I

    move-result v5

    if-nez v5, :cond_5

    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimColor1:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mInsertSimColor1:I

    if-eq v4, v1, :cond_4

    :cond_3
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimBackgroundResByColorId(I)I

    move-result v3

    iput v1, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mInsertSimColor1:I

    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, v2, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    invoke-direct {v4, v5, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimColor1:Landroid/graphics/drawable/Drawable;

    :cond_4
    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimColor1:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_5
    const/4 v5, 0x1

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSlotIdBySimId(I)I

    move-result v6

    if-ne v5, v6, :cond_1

    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimColor2:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_6

    iget v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mInsertSimColor2:I

    if-eq v4, v1, :cond_7

    :cond_6
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimBackgroundResByColorId(I)I

    move-result v3

    iput v1, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mInsertSimColor2:I

    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, v2, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    invoke-direct {v4, v5, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimColor2:Landroid/graphics/drawable/Drawable;

    :cond_7
    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimColor2:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimLockedColor:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_9

    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    const v5, 0x20200d0

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, v2, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    invoke-direct {v4, v5, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimLockedColor:Landroid/graphics/drawable/Drawable;

    :cond_9
    iget-object v4, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimLockedColor:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public getSimDisplayNameById(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    const/4 v0, -0x2

    if-ne v0, p1, :cond_1

    const-string v0, ""

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSipCallDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c0027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSipCallDisplayName:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSipCallDisplayName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    if-nez p1, :cond_2

    const-string v0, ""

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimDisplayNameById(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
