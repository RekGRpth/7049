.class public Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;
.super Landroid/app/Fragment;
.source "CallLogUnavailableFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "CallLogUnavailableFragment"

.field public static mDestroyed:Z


# instance fields
.field private mListener:Lcom/android/contacts/list/OnContactsUnavailableActionListener;

.field private mMessageView:Landroid/widget/TextView;

.field private mProgress:Landroid/widget/ProgressBar;

.field private mProviderStatusWatcher:Lcom/android/contacts/list/ProviderStatusWatcher;

.field private mSecondaryMessageView:Landroid/widget/TextView;

.field private mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mDestroyed:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const-string v0, "CallLogUnavailableFragment"

    const-string v1, "************onCreateView"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f040017

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mView:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mView:Landroid/view/View;

    const v1, 0x7f070097

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mMessageView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mView:Landroid/view/View;

    const v1, 0x7f070098

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mSecondaryMessageView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mView:Landroid/view/View;

    const v1, 0x7f070099

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->update()V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "CallLogUnavailableFragment"

    const-string v1, "CallLogUnavailableFrament destory"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mDestroyed:Z

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method

.method public setOnCallLogUnavailableActionListener(Lcom/android/contacts/list/OnContactsUnavailableActionListener;)V
    .locals 0
    .param p1    # Lcom/android/contacts/list/OnContactsUnavailableActionListener;

    iput-object p1, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mListener:Lcom/android/contacts/list/OnContactsUnavailableActionListener;

    return-void
.end method

.method public setProviderStatusWatcher(Lcom/android/contacts/list/ProviderStatusWatcher;)V
    .locals 0
    .param p1    # Lcom/android/contacts/list/ProviderStatusWatcher;

    iput-object p1, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mProviderStatusWatcher:Lcom/android/contacts/list/ProviderStatusWatcher;

    return-void
.end method

.method public update()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mProviderStatusWatcher:Lcom/android/contacts/list/ProviderStatusWatcher;

    invoke-virtual {v1}, Lcom/android/contacts/list/ProviderStatusWatcher;->getProviderStatus()Lcom/android/contacts/list/ProviderStatusWatcher$Status;

    move-result-object v0

    const-string v1, "CallLogUnavailableFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallLogUnavailableFragment providerStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v1, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mDestroyed:Z

    if-nez v1, :cond_0

    iget v1, v0, Lcom/android/contacts/list/ProviderStatusWatcher$Status;->status:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    :cond_0
    const-string v1, "CallLogUnavailableFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDestoryed is true callLogUnavailableFragment & providerStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    iget v1, v0, Lcom/android/contacts/list/ProviderStatusWatcher$Status;->status:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mMessageView:Landroid/widget/TextView;

    const v2, 0x7f0c0260

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mMessageView:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogUnavailableFragment;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
