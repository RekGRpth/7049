.class public final Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;
.super Ljava/lang/Object;
.source "AssociationSimActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/detail/AssociationSimActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactDetailInfo"
.end annotation


# instance fields
.field public mContactData:Lcom/android/contacts/ContactLoader$Result;

.field public mDisplaySubtitle:Ljava/lang/String;

.field public mDisplayTitle:Ljava/lang/String;

.field public mLookupUri:Landroid/net/Uri;

.field public mNumberInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;"
        }
    .end annotation
.end field

.field private final mPhotoSetter:Lcom/android/contacts/detail/ContactDetailPhotoSetter;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/util/List",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mDisplayTitle:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mDisplaySubtitle:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mLookupUri:Landroid/net/Uri;

    iput-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mContactData:Lcom/android/contacts/ContactLoader$Result;

    iput-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mNumberInfoList:Ljava/util/List;

    new-instance v0, Lcom/android/contacts/detail/ContactDetailPhotoSetter;

    invoke-direct {v0}, Lcom/android/contacts/detail/ContactDetailPhotoSetter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mPhotoSetter:Lcom/android/contacts/detail/ContactDetailPhotoSetter;

    iput-object p1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mDisplayTitle:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mDisplaySubtitle:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mLookupUri:Landroid/net/Uri;

    iput-object p4, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mNumberInfoList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public setPhoto(Landroid/content/Context;Landroid/widget/ImageView;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mContactData:Lcom/android/contacts/ContactLoader$Result;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mContactData:Lcom/android/contacts/ContactLoader$Result;

    invoke-virtual {v2}, Lcom/android/contacts/ContactLoader$Result;->getPhotoUri()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mPhotoSetter:Lcom/android/contacts/detail/ContactDetailPhotoSetter;

    iget-object v3, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mContactData:Lcom/android/contacts/ContactLoader$Result;

    invoke-virtual {v2, p1, v3, p2, v0}, Lcom/android/contacts/detail/ContactDetailPhotoSetter;->setupContactPhotoForClick(Landroid/content/Context;Lcom/android/contacts/ContactLoader$Result;Landroid/widget/ImageView;Z)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
