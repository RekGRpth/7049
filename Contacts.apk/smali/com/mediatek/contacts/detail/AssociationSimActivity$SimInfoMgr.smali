.class public final Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;
.super Ljava/lang/Object;
.source "AssociationSimActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/detail/AssociationSimActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "SimInfoMgr"
.end annotation


# instance fields
.field private mShowingIndex:I

.field public mSimInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/detail/AssociationSimActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mShowingIndex:I

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mShowingIndex:I

    return-void
.end method

.method public getShowingSimId()I
    .locals 2

    const/4 v0, -0x1

    iget v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mShowingIndex:I

    if-le v1, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    iget v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mShowingIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/Telephony$SIMInfo;

    iget-wide v0, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    long-to-int v0, v0

    :cond_0
    return v0
.end method

.method public getShowingSimName()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mShowingIndex:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    iget v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mShowingIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/Telephony$SIMInfo;

    iget-object v0, v0, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public initSimInfo(Landroid/content/Context;)Z
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->clear()V

    iget-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-static {v1, v0}, Lcom/android/contacts/activities/ContactDetailActivity;->getInsertedSimCardInfoList(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    iget-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public setShowingIndex(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mShowingIndex:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mShowingIndex:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setShowingIndexBySimId(I)Z
    .locals 6
    .param p1    # I

    if-gez p1, :cond_0

    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->setShowingIndex(I)Z

    move-result v2

    :goto_0
    return v2

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget-wide v2, v1, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    int-to-long v4, p1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    iput v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mShowingIndex:I

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setShowingSlot(I)Z
    .locals 3
    .param p1    # I

    if-gez p1, :cond_0

    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->setShowingIndex(I)Z

    move-result v2

    :goto_0
    return v2

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget v2, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-ne v2, p1, :cond_1

    iput v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mShowingIndex:I

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
