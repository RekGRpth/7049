.class public Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;
.super Landroid/widget/CursorAdapter;
.source "DialerSearchAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;,
        Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;,
        Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;
    }
.end annotation


# static fields
.field public static final CALL_GEOCODED_LOCATION_INDEX:I = 0x5

.field public static final CALL_LOG_DATE_INDEX:I = 0x2

.field public static final CALL_LOG_ID_INDEX:I = 0x3

.field public static final CALL_TYPE_INDEX:I = 0x4

.field public static final CONTACT_ID_INDEX:I = 0x1

.field public static final CONTACT_NAME_LOOKUP_INDEX:I = 0xe

.field public static final CONTACT_STARRED_INDEX:I = 0x9

.field public static final DS_MATCHED_DATA_DIVIDER:I = 0x3

.field public static final DS_MATCHED_DATA_OFFSETS:I = 0x10

.field public static final DS_MATCHED_NAME_OFFSETS:I = 0x11

.field public static final INDICATE_PHONE_SIM_INDEX:I = 0x8

.field public static final IS_SDN_CONTACT:I = 0xf

.field public static final NAME_INDEX:I = 0xc

.field public static final NAME_LOOKUP_ID_INDEX:I = 0x0

.field public static final NUMBER_TYPE_EMERGENCY:I = 0x5

.field public static final NUMBER_TYPE_NORMAL:I = 0x0

.field public static final NUMBER_TYPE_PAYPHONE:I = 0x4

.field public static final NUMBER_TYPE_PRIVATE:I = 0x3

.field public static final NUMBER_TYPE_UNKNOWN:I = 0x1

.field public static final NUMBER_TYPE_VOICEMAIL:I = 0x2

.field public static final PHOTO_ID_INDEX:I = 0xa

.field public static final SEARCH_PHONE_NUMBER_INDEX:I = 0xd

.field public static final SEARCH_PHONE_TYPE_INDEX:I = 0xb

.field public static final SIM_ID_INDEX:I = 0x6

.field private static final TAG:Ljava/lang/String; = "DialerSearchAdapter"

.field public static final VIEW_TYPE_CALL_LOG:I = 0x1

.field public static final VIEW_TYPE_CONTACT:I = 0x0

.field public static final VIEW_TYPE_CONTACT_CALL_LOG:I = 0x2

.field public static final VIEW_TYPE_COUNT:I = 0x1

.field public static final VIEW_TYPE_UNKNOWN:I = -0x1

.field public static final VTCALL:I = 0x7


# instance fields
.field protected mCallLogSimInfoHelper:Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

.field mCallOptionHandler:Lcom/mediatek/contacts/CallOptionHandler;

.field protected mCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

.field protected mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

.field protected mContext:Landroid/content/Context;

.field protected mDialerSearchCursor:Landroid/database/Cursor;

.field protected mDisplayMetrics:Landroid/util/DisplayMetrics;

.field protected mEmergency:Ljava/lang/String;

.field mHyphonManager:Lcom/mediatek/contacts/HyphonManager;

.field private mListener:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;

.field protected mNumberTypeMaps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mOperatorHorizontalPadding:I

.field protected mOperatorVerticalPadding:I

.field protected mPayphoneNumber:Ljava/lang/String;

.field protected mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

.field protected mPrivateNumber:Ljava/lang/String;

.field protected mSIMInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

.field protected mSpanColorBg:I

.field protected mSpanColorFg:I

.field protected mSpecialNumberMaps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mUnknownNumber:Ljava/lang/String;

.field protected mVideoCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

.field protected mVoiceMail:Ljava/lang/String;

.field protected mVoiceMailNumber:Ljava/lang/String;

.field protected mVoiceMailNumber2:Ljava/lang/String;

.field public needClearDigits:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, v2, v3}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    const/4 v2, 0x5

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x5

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVideoCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mSpecialNumberMaps:Ljava/util/HashMap;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mNumberTypeMaps:Ljava/util/HashMap;

    iput-boolean v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->needClearDigits:Z

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mListener:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mEmergency:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c016d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVoiceMail:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c016f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mPrivateNumber:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0170

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mPayphoneNumber:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c016e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mUnknownNumber:Ljava/lang/String;

    const/4 v2, -0x1

    iput v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mSpanColorFg:I

    const-string v2, "#39caff"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mSpanColorBg:I

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v1

    if-eqz v1, :cond_0

    iput v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mSpanColorBg:I

    :cond_0
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mSIMInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020062

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020061

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v2, v7

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02005f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v2, v8

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVideoCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200e0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVideoCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200e3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVideoCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200e2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v2, v7

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVideoCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200df

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v2, v8

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mSpecialNumberMaps:Ljava/util/HashMap;

    const-string v3, "-1"

    const-string v4, "-1"

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mSpecialNumberMaps:Ljava/util/HashMap;

    const-string v3, "-2"

    const-string v4, "-2"

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mSpecialNumberMaps:Ljava/util/HashMap;

    const-string v3, "-3"

    const-string v4, "-3"

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/contacts/ContactPhotoManager;->getInstance(Landroid/content/Context;)Lcom/android/contacts/ContactPhotoManager;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    new-instance v2, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;-><init>(Landroid/content/res/Resources;)V

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallLogSimInfoHelper:Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09005f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mOperatorVerticalPadding:I

    const v2, 0x7f090060

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mOperatorHorizontalPadding:I

    invoke-static {}, Lcom/mediatek/contacts/HyphonManager;->getInstance()Lcom/mediatek/contacts/HyphonManager;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mHyphonManager:Lcom/mediatek/contacts/HyphonManager;

    new-instance v2, Lcom/android/contacts/calllog/PhoneNumberHelper;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/contacts/calllog/PhoneNumberHelper;-><init>(Landroid/content/res/Resources;)V

    iput-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;Lcom/mediatek/contacts/CallOptionHandler;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;
    .param p3    # Lcom/mediatek/contacts/CallOptionHandler;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;-><init>(Landroid/content/Context;Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;)V

    iput-object p3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallOptionHandler:Lcom/mediatek/contacts/CallOptionHandler;

    return-void
.end method


# virtual methods
.method public bindCallLogView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 32
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->type:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->date:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/view/View;->setVisibility(I)V

    const/16 v27, 0xd

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v27, 0x4

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    const/16 v27, 0x7

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    const/16 v27, 0x6

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->getNumberType(Ljava/lang/String;I)I

    move-result v19

    const/16 v27, 0x2

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/16 v27, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    const/16 v27, 0x5

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/16 v27, 0x3

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->callId:I

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "type = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " videoCall = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " simId = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "number = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "geocode = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mHyphonManager:Lcom/mediatek/contacts/HyphonManager;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/HyphonManager;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "formatNumber = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->log(Ljava/lang/String;)V

    const/16 v27, 0x1

    move/from16 v0, v19

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    move-object/from16 v27, v0

    const/16 v28, 0x8

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->divider:Landroid/view/View;

    move-object/from16 v27, v0

    const/16 v28, 0x8

    invoke-virtual/range {v27 .. v28}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    const/16 v27, 0xa

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    if-lez v16, :cond_0

    const/16 v27, 0xf

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    invoke-static/range {v16 .. v17}, Lcom/mediatek/contacts/dialpad/DialerSearchUtils;->getSimType(II)J

    move-result-wide v20

    :cond_0
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->photo:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->photo:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/contacts/calllog/PhoneNumberHelper;->isSipNumber(Ljava/lang/CharSequence;)Z

    move-result v28

    move-object/from16 v0, v27

    move-object/from16 v1, v18

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;->assignPhoneNumber(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->photo:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-object/from16 v28, v0

    const/16 v29, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-wide/from16 v2, v20

    move/from16 v4, v29

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/contacts/ContactPhotoManager;->loadThumbnail(Landroid/widget/ImageView;JZ)V

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f0c02cc

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    :cond_1
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->isSpecialNumber(I)Z

    move-result v27

    if-eqz v27, :cond_9

    const/16 v27, 0x2

    move/from16 v0, v19

    move/from16 v1, v27

    if-eq v0, v1, :cond_2

    const/16 v27, 0x5

    move/from16 v0, v19

    move/from16 v1, v27

    if-ne v0, v1, :cond_7

    :cond_2
    const/16 v27, 0x2

    move/from16 v0, v19

    move/from16 v1, v27

    if-ne v0, v1, :cond_5

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVoiceMail:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->getNumberHighlight(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_6

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v15, v13, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->highlightHyphon(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v23

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->date:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v27, 0x1

    move/from16 v0, v25

    move/from16 v1, v27

    if-ne v0, v1, :cond_b

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVideoCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    :goto_3
    const/16 v27, 0x5

    move/from16 v0, v27

    move/from16 v1, v24

    if-ne v0, v1, :cond_c

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->type:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    add-int/lit8 v28, v24, -0x1

    aget-object v28, v6, v28

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallLogSimInfoHelper:Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->getSimDisplayNameById(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_d

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallLogSimInfoHelper:Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->getSimColorDrawableById(I)Landroid/graphics/drawable/Drawable;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallLogSimInfoHelper:Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->getSimColorDrawableById(I)Landroid/graphics/drawable/Drawable;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mOperatorHorizontalPadding:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mOperatorVerticalPadding:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mOperatorHorizontalPadding:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mOperatorVerticalPadding:I

    move/from16 v31, v0

    invoke-virtual/range {v27 .. v31}, Landroid/widget/TextView;->setPadding(IIII)V

    :goto_5
    new-instance v5, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;)V

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v27, v0

    move-wide/from16 v0, v27

    iput-wide v0, v5, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;->id:J

    move-object/from16 v0, v18

    iput-object v0, v5, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;->number:Ljava/lang/String;

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_4
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->divider:Landroid/view/View;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mEmergency:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->specialNumberToString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_8

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_9
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->getNumberHighlight(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_a

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v15, v13, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->highlightHyphon(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v23

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_a
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_b
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    goto/16 :goto_3

    :cond_c
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->type:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    aget-object v28, v6, v24

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    :cond_d
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/16 v28, 0x8

    invoke-virtual/range {v27 .. v28}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5
.end method

.method public bindContactCallLogView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 18
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;

    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual/range {p0 .. p3}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->bindContactView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->date:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->type:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v13, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v13, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/4 v13, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/16 v13, 0xd

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v13, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v13, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    iput v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->callId:I

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "bindContactCallLogView type = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " videoCall = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " simId = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-static {v13}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v5, v13}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->date:Landroid/widget/TextView;

    invoke-virtual {v13, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v13, 0x1

    if-ne v11, v13, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVideoCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    :goto_0
    const/4 v13, 0x5

    if-ne v13, v10, :cond_1

    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->type:Landroid/widget/ImageView;

    add-int/lit8 v14, v10, -0x1

    aget-object v14, v2, v14

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallLogSimInfoHelper:Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

    invoke-virtual {v13, v9}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->getSimDisplayNameById(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_2

    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    invoke-virtual {v13, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallLogSimInfoHelper:Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

    invoke-virtual {v14, v9}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->getSimColorDrawableById(I)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mOperatorHorizontalPadding:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mOperatorVerticalPadding:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mOperatorHorizontalPadding:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mOperatorVerticalPadding:I

    move/from16 v17, v0

    invoke-virtual/range {v13 .. v17}, Landroid/widget/TextView;->setPadding(IIII)V

    :goto_2
    new-instance v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;)V

    int-to-long v13, v9

    iput-wide v13, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;->id:J

    iput-object v8, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;->number:Ljava/lang/String;

    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    invoke-virtual {v13, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallTypeDrawables:[Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_1
    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->type:Landroid/widget/ImageView;

    aget-object v14, v2, v10

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_2
    iget-object v13, v12, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public bindContactView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 25
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->type:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->date:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    const/16 v22, 0xd

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/16 v22, 0xb

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const/16 v22, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/16 v22, 0x6

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v14, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->getNumberType(Ljava/lang/String;I)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mHyphonManager:Lcom/mediatek/contacts/HyphonManager;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Lcom/mediatek/contacts/HyphonManager;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/16 v22, 0xc

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v12, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v19

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v22

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    move/from16 v3, v19

    invoke-virtual {v0, v1, v12, v2, v3}, Lcom/android/contacts/ext/ContactAccountExtension;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v11

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "name = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " number = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " label = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->getContactUri(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->contactUri:Landroid/net/Uri;

    const/16 v22, 0xa

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    if-lez v9, :cond_0

    const/16 v22, 0xf

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/contacts/dialpad/DialerSearchUtils;->getSimType(II)J

    move-result-wide v16

    :cond_0
    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v15, v0, :cond_4

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->divider:Landroid/view/View;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    const/16 v22, 0x2

    move/from16 v0, v22

    if-eq v15, v0, :cond_1

    const/16 v22, 0x5

    move/from16 v0, v22

    if-ne v15, v0, :cond_5

    :cond_1
    const-wide/16 v16, 0x0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->photo:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;->assignPhoneNumber(Ljava/lang/String;Z)V

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->photo:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->photo:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-wide/from16 v2, v16

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/contacts/ContactPhotoManager;->loadThumbnail(Landroid/widget/ImageView;JZ)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->isSpecialNumber(I)Z

    move-result v22

    if-eqz v22, :cond_a

    const/16 v22, 0x2

    move/from16 v0, v22

    if-eq v15, v0, :cond_2

    const/16 v22, 0x5

    move/from16 v0, v22

    if-ne v15, v0, :cond_8

    :cond_2
    const/16 v22, 0x2

    move/from16 v0, v22

    if-ne v15, v0, :cond_6

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVoiceMail:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->getNumberHighlight(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_7

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v7, v14}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->highlightHyphon(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v20

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_3
    new-instance v5, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;)V

    const-wide/16 v22, -0x5

    move-wide/from16 v0, v22

    iput-wide v0, v5, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;->id:J

    iput-object v14, v5, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;->number:Ljava/lang/String;

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_4
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->divider:Landroid/view/View;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->photo:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;->assignPhoneNumber(Ljava/lang/String;Z)V

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->photo:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->contactUri:Landroid/net/Uri;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mEmergency:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->specialNumberToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_9

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_9
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_a
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_d

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->getNameHighlight(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_b

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v13}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->highlightString(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v20

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->getNumberHighlight(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_c

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v7, v14}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->highlightHyphon(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v20

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v11, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->setLabelAndNumber(Landroid/widget/TextView;Ljava/lang/CharSequence;Landroid/text/SpannableStringBuilder;)V

    goto/16 :goto_3

    :cond_b
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_c
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v11, v7}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->setLabelAndNumber(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_d
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_f

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->getNumberHighlight(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_e

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v7, v14}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->highlightHyphon(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v20

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_e
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_f
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const-string v2, "+bindView"

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->log(Ljava/lang/String;)V

    invoke-virtual {p0, p3}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->getViewType(Landroid/database/Cursor;)I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v2, "-bindView"

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->log(Ljava/lang/String;)V

    return-void

    :pswitch_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->bindContactView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    const/4 v2, 0x0

    iput v2, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->viewType:I

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->bindCallLogView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    const/4 v2, 0x1

    iput v2, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->viewType:I

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->bindContactCallLogView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    const/4 v2, 0x2

    iput v2, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->viewType:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected createViewHolder()Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;
    .locals 1

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;)V

    return-object v0
.end method

.method protected getContactUri(Landroid/database/Cursor;)Landroid/net/Uri;
    .locals 4
    .param p1    # Landroid/database/Cursor;

    const/16 v2, 0xe

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v2, v0

    invoke-static {v2, v3, v1}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method protected getNameHighlight(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/database/Cursor;

    const-string v1, "matched_name_offsets"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getNumberHighlight(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/database/Cursor;

    const-string v1, "matched_data_offsets"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getNumberType(Ljava/lang/String;I)I
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v2, 0x2

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const-string v3, "-1"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_2
    const-string v3, "-2"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v1, 0x3

    goto :goto_1

    :cond_3
    const-string v3, "-3"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v1, 0x4

    goto :goto_1

    :cond_4
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v1, 0x5

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mSIMInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-virtual {v3, p2}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v0

    if-nez v0, :cond_7

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVoiceMailNumber:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVoiceMailNumber:Ljava/lang/String;

    invoke-static {v3, p1}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    :cond_7
    const/4 v3, 0x1

    if-ne v0, v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVoiceMailNumber2:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVoiceMailNumber2:Ljava/lang/String;

    invoke-static {v3, p1}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getView position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " convertView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " parent:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mDialerSearchCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "couldn\'t move cursor to position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-nez p2, :cond_1

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mDialerSearchCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v1, v2, p3}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mDialerSearchCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0, v1, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    return-object v0

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method public getViewType(Landroid/database/Cursor;)I
    .locals 4
    .param p1    # Landroid/database/Cursor;

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v1, :cond_1

    if-lez v0, :cond_1

    const/4 v2, 0x2

    :cond_0
    :goto_0
    return v2

    :cond_1
    if-lez v1, :cond_2

    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    if-lez v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected highlightHyphon(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {p2, p1, p3}, Lcom/mediatek/contacts/dialpad/DialerSearchUtils;->adjustHighlitePositionForHyphen(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    iget v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mSpanColorBg:I

    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    const/16 v5, 0x21

    invoke-virtual {v1, v3, v4, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v1
.end method

.method protected highlightString(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    if-gt v3, v4, :cond_0

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    if-le v3, v4, :cond_1

    :cond_0
    return-object v2

    :cond_1
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    iget v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mSpanColorBg:I

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    add-int/lit8 v5, v5, 0x1

    const/16 v6, 0x21

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v0, v0, 0x3

    goto :goto_0
.end method

.method protected isSpecialNumber(I)Z
    .locals 1
    .param p1    # I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "DialerSearchAdapter"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected newItemClickIntent(Landroid/view/View;)Landroid/content/Intent;
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;

    iget v3, v2, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->viewType:I

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    const-class v4, Lcom/android/contacts/CallDetailActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v3, Landroid/provider/CallLog$Calls;->CONTENT_URI_WITH_VOICEMAIL:Landroid/net/Uri;

    iget v4, v2, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->callId:I

    int-to-long v4, v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_1
    iget-object v1, v2, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->contactUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const-string v2, "+newView"

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->log(Ljava/lang/String;)V

    const v2, 0x7f04004a

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->createViewHolder()Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;

    move-result-object v1

    const v2, 0x7f07008a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->name:Landroid/widget/TextView;

    const v2, 0x7f070105

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->labelAndNumber:Landroid/widget/TextView;

    const v2, 0x7f070106

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->type:Landroid/widget/ImageView;

    const v2, 0x7f070107

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->operator:Landroid/widget/TextView;

    const v2, 0x7f070057

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->date:Landroid/widget/TextView;

    const v2, 0x7f070108

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    const v2, 0x7f070089

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    iput-object v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->photo:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    const v2, 0x7f070090

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->divider:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const-string v2, "-newView"

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->log(Ljava/lang/String;)V

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;

    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.CALL_PRIVILEGED"

    const-string v4, "tel"

    iget-object v5, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;->number:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v3, "com.android.phone.extra.original"

    iget-wide v4, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;->id:J

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallOptionHandler:Lcom/mediatek/contacts/CallOptionHandler;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mCallOptionHandler:Lcom/mediatek/contacts/CallOptionHandler;

    invoke-virtual {v3, v1}, Lcom/mediatek/contacts/CallOptionHandler;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_1
    const-string v3, "onClick(), view id = dialer_search_item_view"

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/contacts/ExtensionManager;->getDialtactsExtension()Lcom/android/contacts/ext/DialtactsExtension;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/contacts/ext/DialtactsExtension;->startActivity()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->needClearDigits:Z

    invoke-virtual {p0, p1}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->newItemClickIntent(Landroid/view/View;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;

    iget-object v3, v2, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$ViewHolder;->call:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mListener:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "callinfo number = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;->number:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mListener:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;

    iget-object v4, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$CallInfo;->number:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;->onListViewItemClicked(Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f070104 -> :sswitch_1
        0x7f070108 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getVoiceMailNumberGemini(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVoiceMailNumber:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getVoiceMailNumberGemini(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mVoiceMailNumber2:Ljava/lang/String;

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    invoke-virtual {v0}, Lcom/android/contacts/ContactPhotoManager;->pause()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    invoke-virtual {v0}, Lcom/android/contacts/ContactPhotoManager;->resume()V

    goto :goto_0
.end method

.method protected setLabelAndNumber(Landroid/widget/TextView;Ljava/lang/CharSequence;Landroid/text/SpannableStringBuilder;)V
    .locals 3
    .param p1    # Landroid/widget/TextView;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Landroid/text/SpannableStringBuilder;

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/contacts/activities/DialtactsActivity;->isUr:Z

    if-eqz v0, :cond_0

    const-string v0, "\u200e"

    invoke-virtual {p3, v2, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    invoke-virtual {p3}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isUriNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v2, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected setLabelAndNumber(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/widget/TextView;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/contacts/activities/DialtactsActivity;->isUr:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u200e"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_0
    invoke-static {p3}, Landroid/telephony/PhoneNumberUtils;->isUriNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setResultCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mDialerSearchCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mDialerSearchCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mDialerSearchCursor:Landroid/database/Cursor;

    return-void
.end method

.method specialNumberToString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mUnknownNumber:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mPrivateNumber:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->mPayphoneNumber:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
