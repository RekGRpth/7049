.class Lcom/mediatek/contacts/dialpad/DialerSearchController$1;
.super Ljava/lang/Object;
.source "DialerSearchController.java"

# interfaces
.implements Lcom/android/contacts/preference/ContactsPreferences$ChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/contacts/dialpad/DialerSearchController;-><init>(Landroid/content/Context;Landroid/widget/ListView;Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;Lcom/mediatek/contacts/CallOptionHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const-string v1, "contacts display or sort order changed"

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$000(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$102(Lcom/mediatek/contacts/dialpad/DialerSearchController;Z)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->startQuery(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$102(Lcom/mediatek/contacts/dialpad/DialerSearchController;Z)Z

    goto :goto_0
.end method
