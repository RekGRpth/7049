.class Lcom/mediatek/contacts/dialpad/DialerSearchController$2;
.super Landroid/os/Handler;
.source "DialerSearchController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/dialpad/DialerSearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$2;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const/4 v2, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$300()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$2;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$2;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$2;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const-string v1, "mPhoneStateListener startQuery"

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$2;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->startQuery(Ljava/lang/String;I)V

    :cond_1
    invoke-static {v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$302(I)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method
