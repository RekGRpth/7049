.class Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;
.super Landroid/database/ContentObserver;
.source "DialerSearchController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/dialpad/DialerSearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const-string v1, "ContactContentObserver: "

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$000(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$102(Lcom/mediatek/contacts/dialpad/DialerSearchController;Z)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$102(Lcom/mediatek/contacts/dialpad/DialerSearchController;Z)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->startQuery(Ljava/lang/String;I)V

    goto :goto_0
.end method
