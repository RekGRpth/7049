.class Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;
.super Lcom/android/contacts/util/PhoneNumberFormatter$PhoneNumberFormattingTextWatcherEx;
.source "DialerSearchController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/dialpad/DialerSearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DsTextWatcher"
.end annotation


# static fields
.field private static final DBG_INT:Z = true


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-direct {p0}, Lcom/android/contacts/util/PhoneNumberFormatter$PhoneNumberFormattingTextWatcherEx;-><init>()V

    return-void
.end method

.method private logd(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "DialerSearchController"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1    # Landroid/text/Editable;

    const/4 v3, 0x0

    sget-boolean v1, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[afterTextChanged]mSelfChanged:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[afterTextChanged]text:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$202(Lcom/mediatek/contacts/dialpad/DialerSearchController;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-boolean v1, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFormatting:Z

    if-nez v1, :cond_1

    const-string v1, "formatting"

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFormatting:Z

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v2, v2, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitString:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v2, v2, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitString:Ljava/lang/String;

    invoke-static {v2}, Lcom/mediatek/contacts/dialpad/DialerSearchUtils;->tripNonDigit(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitString:Ljava/lang/String;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget v2, v2, Lcom/mediatek/contacts/dialpad/DialerSearchController;->searchMode:I

    invoke-virtual {v1, v0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->startQuery(Ljava/lang/String;I)V

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iput-boolean v3, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFormatting:Z

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iput-boolean v3, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumberOnly:Z

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$100(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->startQuery(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v1, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$102(Lcom/mediatek/contacts/dialpad/DialerSearchController;Z)Z

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const-string v2, "NULL_INPUT"

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->startQuery(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sget-boolean v1, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[beforeTextChanged]mSelfChanged:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[beforeTextChanged]s:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|start:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|after:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mChangeInMiddle:Z

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v5, 0x0

    const/4 v4, 0x1

    sget-boolean v2, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onTextChanged]mSelfChanged:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onTextChanged]s:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|start:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|count:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|before:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-boolean v2, v2, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFormatting:Z

    if-nez v2, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-boolean v2, v2, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumberOnly:Z

    if-nez v2, :cond_2

    if-gt p4, v4, :cond_2

    if-gt p3, v4, :cond_2

    if-ne p4, p3, :cond_1

    add-int/lit8 v2, v1, -0x1

    if-eq p2, v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-boolean v2, v2, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mChangeInMiddle:Z

    if-eqz v2, :cond_4

    :cond_2
    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-virtual {v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->setSearchNumberOnly()V

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iput v4, v2, Lcom/mediatek/contacts/dialpad/DialerSearchController;->searchMode:I

    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iput-boolean v5, v2, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mChangeInMiddle:Z

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iput v5, v2, Lcom/mediatek/contacts/dialpad/DialerSearchController;->searchMode:I

    goto :goto_1
.end method
