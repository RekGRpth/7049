.class public Lcom/mediatek/contacts/dialpad/DialerSearchController;
.super Landroid/content/AsyncQueryHandler;
.source "DialerSearchController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;,
        Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;,
        Lcom/mediatek/contacts/dialpad/DialerSearchController$CallLogContentObserver;,
        Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;,
        Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field public static final DIALER_SEARCH_MODE_ALL:I = 0x0

.field public static final DIALER_SEARCH_MODE_NUMBER:I = 0x1

.field public static final DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

.field private static final DS_MSG_CONTACTS_DELETE_CHANGED:I = 0x3e8

.field private static final DS_MSG_DELAY_TIME:I = 0x3e8

.field private static final EMPTY_NUMBER:Ljava/lang/String; = ""

.field private static final QUERY_TOKEN_INCREMENT:I = 0x32

.field private static final QUERY_TOKEN_INIT:I = 0x1e

.field private static final QUERY_TOKEN_NULL:I = 0x28

.field private static final QUERY_TOKEN_SIMPLE:I = 0x3c

.field private static final TAG:Ljava/lang/String; = "DialerSearchController"

.field private static delCount:I


# instance fields
.field protected mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

.field mCallLogContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$CallLogContentObserver;

.field protected mChangeInMiddle:Z

.field protected mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

.field protected mContext:Landroid/content/Context;

.field private final mDBHandlerForDelContacts:Landroid/os/Handler;

.field private mDataChanged:Z

.field protected mDialerSearchCursorCount:I

.field protected mDigitString:Ljava/lang/String;

.field protected mDigits:Landroid/widget/EditText;

.field private mDigitsFilled:Z

.field protected mDisplayOrder:I

.field private mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

.field mFilterContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;

.field protected mFormatting:Z

.field private mIsForeground:Z

.field protected mListView:Landroid/widget/ListView;

.field protected mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

.field protected mPrevQueryDigCnt:I

.field private mPreviousText:Ljava/lang/String;

.field protected mQueryComplete:Z

.field protected mSearchNumCntQ:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mSearchNumberOnly:Z

.field protected mSelectedContactUri:Landroid/net/Uri;

.field protected mSortOrder:I

.field protected noMoreResult:Z

.field protected noResultDigCnt:I

.field protected searchMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "vds_contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "vds_call_date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "vds_call_log_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "vds_call_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "vds_sim_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "vds_indicate_phone_sim"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "vds_starred"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "vds_photo_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "vds_phone_type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "vds_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "vds_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "vds_lookup"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "matched_data_offsets"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "matched_name_offsets"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "vds_is_sdn_contact"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

    sput v3, Lcom/mediatek/contacts/dialpad/DialerSearchController;->delCount:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/ListView;Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/ListView;
    .param p3    # Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;-><init>(Landroid/content/Context;Landroid/widget/ListView;Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;Lcom/mediatek/contacts/CallOptionHandler;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/ListView;Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;Lcom/mediatek/contacts/CallOptionHandler;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/ListView;
    .param p3    # Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;
    .param p4    # Lcom/mediatek/contacts/CallOptionHandler;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    iput-boolean v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsForeground:Z

    iput-boolean v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    iput-boolean v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitsFilled:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchController$2;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController$2;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDBHandlerForDelContacts:Landroid/os/Handler;

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mListView:Landroid/widget/ListView;

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    invoke-direct {v0, p1, p3, p4}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;-><init>(Landroid/content/Context;Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;Lcom/mediatek/contacts/CallOptionHandler;)V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchController$CallLogContentObserver;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController$CallLogContentObserver;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mCallLogContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$CallLogContentObserver;

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFilterContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;

    new-instance v0, Lcom/android/contacts/preference/ContactsPreferences;

    invoke-direct {v0, p1}, Lcom/android/contacts/preference/ContactsPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    new-instance v1, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V

    invoke-virtual {v0, v1}, Lcom/android/contacts/preference/ContactsPreferences;->registerChangeListener(Lcom/android/contacts/preference/ContactsPreferences$ChangeListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsForeground:Z

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/contacts/dialpad/DialerSearchController;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/dialpad/DialerSearchController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    return p1
.end method

.method static synthetic access$202(Lcom/mediatek/contacts/dialpad/DialerSearchController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/dialpad/DialerSearchController;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mPreviousText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300()I
    .locals 1

    sget v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->delCount:I

    return v0
.end method

.method static synthetic access$302(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->delCount:I

    return p0
.end method


# virtual methods
.method public configureFromIntent(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitsFilled:Z

    return-void
.end method

.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "DialerSearchController"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method obtainDialerSearchResult(I)Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;
    .locals 1
    .param p1    # I

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V

    iput p1, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;->count:I

    return-object v0
.end method

.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    const/4 v3, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "result is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    if-gez p3, :cond_1

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContext:Landroid/content/Context;

    const v2, 0x7f0c0038

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSelectedContactUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    const-string v1, "Before delete db"

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSelectedContactUri:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    const-string v1, "onDeleteComplete startQuery"

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    if-lez v0, :cond_0

    sput v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->delCount:I

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDBHandlerForDelContacts:Landroid/os/Handler;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mCallLogContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$CallLogContentObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mCallLogContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$CallLogContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFilterContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;

    if-eqz v0, :cond_1

    const-string v0, "DialerSearchController onDestroy : unregister the filter observer."

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFilterContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    :cond_3
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    invoke-virtual {v0}, Lcom/android/contacts/preference/ContactsPreferences;->unregisterChangeListener()V

    :cond_4
    return-void
.end method

.method public onPause()V
    .locals 1

    const-string v0, "onPause"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsForeground:Z

    return-void
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mPrevQueryDigCnt:I

    :cond_0
    const-string v3, "+onQueryComplete"

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    if-eqz p3, :cond_2

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDialerSearchCursorCount:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cursor count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDialerSearchCursorCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    iget v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDialerSearchCursorCount:I

    if-lez v3, :cond_4

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mQueryComplete:Z

    const/4 v3, 0x0

    iput v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->noResultDigCnt:I

    invoke-static {v2}, Lcom/mediatek/contacts/dialpad/DialerSearchUtils;->tripHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mPrevQueryDigCnt:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    iget v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDialerSearchCursorCount:I

    invoke-virtual {p0, v4}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->obtainDialerSearchResult(I)Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;->onDialerSearchResult(Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;)V

    :cond_1
    invoke-virtual {v1, p3}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->setResultCursor(Landroid/database/Cursor;)V

    invoke-virtual {v1, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    :cond_2
    :goto_0
    const-string v3, "-onQueryComplete"

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    return-void

    :cond_3
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    iget v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDialerSearchCursorCount:I

    invoke-virtual {p0, v4}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->obtainDialerSearchResult(I)Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;->onDialerSearchResult(Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;)V

    :cond_5
    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->noResultDigCnt:I

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->setResultCursor(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v0, "onResume"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    invoke-virtual {v0}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->onResume()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "DialerSearchController onResume startQuery"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->startQuery(Ljava/lang/String;I)V

    :cond_1
    :goto_0
    iput-boolean v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsForeground:Z

    return-void

    :cond_2
    const-string v0, "DialerSearchController onResume with digits"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitsFilled:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DialerSearchController mDigitsFilled"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitsFilled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitsFilled:Z

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    if-eqz v0, :cond_4

    const-string v0, "DialerSearchController onResume with digits, refresh the data"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    iget-boolean v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->needClearDigits:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    iput-boolean v2, v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->needClearDigits:Z

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    const-string v0, "onStop"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "DialerSearchController onStop"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setDialerSearchTextWatcher(Landroid/widget/EditText;)V
    .locals 4
    .param p1    # Landroid/widget/EditText;

    const/4 v3, 0x1

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.android.contacts.dialer_search/callLog/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mCallLogContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$CallLogContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.android.contacts/dialer_search/filter/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFilterContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$ContactContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public setOnDialerSearchResult(Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;)V
    .locals 0
    .param p1    # Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    return-void
.end method

.method public setSearchNumberOnly()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumberOnly:Z

    return-void
.end method

.method public startQuery(Ljava/lang/String;I)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startQuery searchContent: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " mode: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mediatek/contacts/dialpad/DialerSearchUtils;->tripHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->noResultDigCnt:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->noResultDigCnt:I

    if-le v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->noMoreResult:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "noResultDigCnt: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->noResultDigCnt:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " || mDigits.getText(): "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iput-boolean v8, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mQueryComplete:Z

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    invoke-virtual {v0}, Lcom/android/contacts/preference/ContactsPreferences;->getDisplayOrder()I

    move-result v0

    iput v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDisplayOrder:I

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    invoke-virtual {v0}, Lcom/android/contacts/preference/ContactsPreferences;->getSortOrder()I

    move-result v0

    iput v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSortOrder:I

    const/16 v1, 0x1e

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://com.android.contacts/dialer_search/filter/init#"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDisplayOrder:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "#"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSortOrder:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/dialpad/DialerSearchController;->DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v8

    goto :goto_0

    :cond_2
    const-string v0, "NULL_INPUT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v1, 0x28

    const-string v0, "content://com.android.contacts/dialer_search/filter/null_input"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/dialpad/DialerSearchController;->DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    if-nez p2, :cond_4

    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->noMoreResult:Z

    if-nez v0, :cond_0

    const/16 v1, 0x32

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://com.android.contacts/dialer_search/filter/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/dialpad/DialerSearchController;->DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    if-ne p2, v1, :cond_0

    const/16 v1, 0x3c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://com.android.contacts/dialer_search_number/filter/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/dialpad/DialerSearchController;->DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public updateDialerSearch()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->startQuery(Ljava/lang/String;I)V

    goto :goto_0
.end method
