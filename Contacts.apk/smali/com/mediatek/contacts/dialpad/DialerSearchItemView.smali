.class public Lcom/mediatek/contacts/dialpad/DialerSearchItemView;
.super Landroid/widget/FrameLayout;
.source "DialerSearchItemView.java"


# static fields
.field private static final DBG:Z = true

.field private static DialerSearchItemViewHeight:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DialerSearchListItem"

.field private static mListItemQuickContactPaddingBottom:I

.field private static mListItemQuickContactPaddingTop:I


# instance fields
.field protected mCall:Landroid/widget/ImageButton;

.field protected mCallType:Landroid/widget/ImageView;

.field protected mDate:Landroid/widget/TextView;

.field protected mDivider:Landroid/view/View;

.field protected mLabelAndNumber:Landroid/widget/TextView;

.field protected mName:Landroid/widget/TextView;

.field protected mOperator:Landroid/widget/TextView;

.field protected mQuickContactBadge:Landroid/widget/QuickContactBadge;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->DialerSearchItemViewHeight:I

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mListItemQuickContactPaddingTop:I

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mListItemQuickContactPaddingBottom:I

    return-void
.end method


# virtual methods
.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "DialerSearchListItem"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    const v0, 0x7f070089

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mQuickContactBadge:Landroid/widget/QuickContactBadge;

    const v0, 0x7f070105

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    const v0, 0x7f07008a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mName:Landroid/widget/TextView;

    const v0, 0x7f070106

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    const v0, 0x7f070107

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mOperator:Landroid/widget/TextView;

    const v0, 0x7f070057

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDate:Landroid/widget/TextView;

    const v0, 0x7f070090

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDivider:Landroid/view/View;

    const v0, 0x7f070108

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCall:Landroid/widget/ImageButton;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 25
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "changed = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " left = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " top = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " right = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " bottom = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    move/from16 v17, v0

    sub-int v22, p4, p2

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingRight:I

    move/from16 v23, v0

    sub-int v18, v22, v23

    sget v19, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mListItemQuickContactPaddingTop:I

    sub-int v22, p5, p3

    sget v23, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mListItemQuickContactPaddingBottom:I

    sub-int v16, v22, v23

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "mPaddingTop = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingTop:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " mPaddingBottom = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingBottom:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " mPaddingLeft = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " mPaddingRight = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingRight:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->log(Ljava/lang/String;)V

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "parentTop = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " parentBottom = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->log(Ljava/lang/String;)V

    move/from16 v9, p2

    move/from16 v10, p3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mQuickContactBadge:Landroid/widget/QuickContactBadge;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mQuickContactBadge:Landroid/widget/QuickContactBadge;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    sub-int v22, v16, v19

    sub-int v22, v22, v11

    div-int/lit8 v22, v22, 0x2

    add-int v10, v19, v22

    move/from16 v9, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mQuickContactBadge:Landroid/widget/QuickContactBadge;

    move-object/from16 v22, v0

    add-int v23, v9, v21

    add-int v24, v10, v11

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v9, v10, v1, v2}, Landroid/view/View;->layout(IIII)V

    add-int v20, v9, v21

    sget v22, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mListItemQuickContactPaddingTop:I

    add-int v22, v22, v11

    sget v23, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mListItemQuickContactPaddingBottom:I

    add-int v22, v22, v23

    sput v22, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->DialerSearchItemViewHeight:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCall:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingRight:I

    move/from16 v23, v0

    add-int v21, v22, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCall:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    sget v22, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->DialerSearchItemViewHeight:I

    sub-int v22, v22, v11

    div-int/lit8 v10, v22, 0x2

    sub-int v9, v18, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCall:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    add-int v23, v9, v21

    add-int v24, v10, v11

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v9, v10, v1, v2}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDivider:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDivider:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    sub-int v22, v16, v19

    sub-int v22, v22, v11

    div-int/lit8 v22, v22, 0x2

    add-int v10, v19, v22

    sub-int v9, v9, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDivider:Landroid/view/View;

    move-object/from16 v22, v0

    add-int v23, v9, v21

    add-int v24, v10, v11

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v9, v10, v1, v2}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mName:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mName:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mName:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/widget/FrameLayout$LayoutParams;

    move/from16 v10, v19

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v22, v0

    add-int v9, v20, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mName:Landroid/widget/TextView;

    move-object/from16 v22, v0

    add-int v23, v9, v21

    add-int v24, v10, v11

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v9, v10, v1, v2}, Landroid/view/View;->layout(IIII)V

    add-int v14, v10, v11

    move v3, v11

    add-int/2addr v6, v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getVisibility()I

    move-result v22

    if-nez v22, :cond_2

    const/4 v8, 0x1

    :goto_0
    if-eqz v8, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/widget/FrameLayout$LayoutParams;

    sub-int v10, v16, v11

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v22, v0

    add-int v9, v20, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    add-int v23, v9, v21

    add-int v24, v10, v11

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v9, v10, v1, v2}, Landroid/view/View;->layout(IIII)V

    add-int v7, v9, v21

    invoke-static {v5, v11}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mOperator:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mOperator:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mOperator:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/widget/FrameLayout$LayoutParams;

    sub-int v10, v16, v11

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v22, v0

    add-int v9, v7, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mOperator:Landroid/widget/TextView;

    move-object/from16 v22, v0

    add-int v23, v9, v21

    add-int v24, v10, v11

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v9, v10, v1, v2}, Landroid/view/View;->layout(IIII)V

    add-int v15, v9, v21

    invoke-static {v5, v11}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDate:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDate:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDate:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/widget/FrameLayout$LayoutParams;

    sub-int v10, v16, v11

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v22, v0

    add-int v9, v15, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDate:Landroid/widget/TextView;

    move-object/from16 v22, v0

    add-int v23, v9, v21

    add-int v24, v10, v11

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v9, v10, v1, v2}, Landroid/view/View;->layout(IIII)V

    invoke-static {v5, v11}, Ljava/lang/Math;->max(II)I

    move-result v5

    add-int/2addr v6, v5

    :cond_0
    move/from16 v12, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getVisibility()I

    move-result v22

    if-nez v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v8, :cond_3

    move v10, v14

    :goto_1
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v22, v0

    add-int v9, v20, v22

    add-int v12, v9, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    move-object/from16 v22, v0

    add-int v23, v9, v21

    add-int v24, v10, v11

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v9, v10, v1, v2}, Landroid/view/View;->layout(IIII)V

    invoke-static {v4, v11}, Ljava/lang/Math;->max(II)I

    move-result v4

    :cond_1
    add-int/2addr v6, v4

    sget v22, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->DialerSearchItemViewHeight:I

    move/from16 v0, v22

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v22

    sput v22, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->DialerSearchItemViewHeight:I

    return-void

    :cond_2
    const/4 v8, 0x0

    goto/16 :goto_0

    :cond_3
    sub-int v10, v16, v11

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Landroid/view/View;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->resolveSizeAndState(III)I

    move-result v1

    sget v2, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->DialerSearchItemViewHeight:I

    invoke-virtual {p0, v1, v2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method
