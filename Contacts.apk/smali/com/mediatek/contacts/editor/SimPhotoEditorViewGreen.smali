.class public Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;
.super Lcom/android/contacts/editor/PhotoEditorView;
.source "SimPhotoEditorViewGreen.java"


# static fields
.field private static final PHOTO_QUALTY:I = 0x64

.field private static final TAG:Ljava/lang/String; = "SimPhotoEditorViewGreen"


# instance fields
.field private mEntry:Lcom/android/contacts/model/EntityDelta$ValuesDelta;

.field private mFrameView:Landroid/view/View;

.field private mHasSetPhoto:Z

.field private mListener:Lcom/android/contacts/editor/Editor$EditorListener;

.field private mPhotoImageView:Landroid/widget/ImageView;

.field private mReadOnly:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/contacts/editor/PhotoEditorView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mHasSetPhoto:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/contacts/editor/PhotoEditorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mHasSetPhoto:Z

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;)Lcom/android/contacts/editor/Editor$EditorListener;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;

    iget-object v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mListener:Lcom/android/contacts/editor/Editor$EditorListener;

    return-object v0
.end method


# virtual methods
.method public clearAllFields()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->resetDefault()V

    return-void
.end method

.method public deleteEditor()V
    .locals 0

    return-void
.end method

.method public hasSetPhoto()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mHasSetPhoto:Z

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mHasSetPhoto:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFieldChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Photos don\'t support direct field changes"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Lcom/android/contacts/editor/PhotoEditorView;->onFinishInflate()V

    const v0, 0x7f07014c

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mPhotoImageView:Landroid/widget/ImageView;

    const v0, 0x7f070147

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mFrameView:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mFrameView:Landroid/view/View;

    new-instance v1, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen$1;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen$1;-><init>(Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected resetDefault()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mPhotoImageView:Landroid/widget/ImageView;

    const v3, 0x7f020086

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v3, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mFrameView:Landroid/view/View;

    iget-boolean v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mReadOnly:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    iput-boolean v2, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mHasSetPhoto:Z

    iget-object v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mEntry:Lcom/android/contacts/model/EntityDelta$ValuesDelta;

    invoke-virtual {v0, v1}, Lcom/android/contacts/model/EntityDelta$ValuesDelta;->setFromTemplate(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public setDeletable(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setEditorListener(Lcom/android/contacts/editor/Editor$EditorListener;)V
    .locals 0
    .param p1    # Lcom/android/contacts/editor/Editor$EditorListener;

    iput-object p1, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mListener:Lcom/android/contacts/editor/Editor$EditorListener;

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/contacts/editor/PhotoEditorView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mFrameView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public setPhotoBitmap(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;

    if-nez p1, :cond_0

    iget-object v4, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mEntry:Lcom/android/contacts/model/EntityDelta$ValuesDelta;

    const-string v5, "data15"

    const/4 v3, 0x0

    check-cast v3, [B

    invoke-virtual {v4, v5, v3}, Lcom/android/contacts/model/EntityDelta$ValuesDelta;->put(Ljava/lang/String;[B)V

    invoke-virtual {p0}, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->resetDefault()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    mul-int/lit8 v2, v3, 0x4

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    :try_start_0
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {p1, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    iget-object v3, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mEntry:Lcom/android/contacts/model/EntityDelta$ValuesDelta;

    const-string v4, "data15"

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/android/contacts/model/EntityDelta$ValuesDelta;->put(Ljava/lang/String;[B)V

    const-string v3, "SimPhotoEditorViewGreen"

    const-string v4, "the setPhotoBitmap"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mPhotoImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v3, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mFrameView:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setEnabled(Z)V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mHasSetPhoto:Z

    iget-object v3, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mEntry:Lcom/android/contacts/model/EntityDelta$ValuesDelta;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/contacts/model/EntityDelta$ValuesDelta;->setFromTemplate(Z)V

    iget-object v3, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mEntry:Lcom/android/contacts/model/EntityDelta$ValuesDelta;

    const-string v4, "is_super_primary"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/android/contacts/model/EntityDelta$ValuesDelta;->put(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "SimPhotoEditorViewGreen"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to serialize photo: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setSuperPrimary(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mEntry:Lcom/android/contacts/model/EntityDelta$ValuesDelta;

    const-string v2, "is_super_primary"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/android/contacts/model/EntityDelta$ValuesDelta;->put(Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setValues(Lcom/android/contacts/model/DataKind;Lcom/android/contacts/model/EntityDelta$ValuesDelta;Lcom/android/contacts/model/EntityDelta;ZLcom/android/contacts/editor/ViewIdGenerator;)V
    .locals 6
    .param p1    # Lcom/android/contacts/model/DataKind;
    .param p2    # Lcom/android/contacts/model/EntityDelta$ValuesDelta;
    .param p3    # Lcom/android/contacts/model/EntityDelta;
    .param p4    # Z
    .param p5    # Lcom/android/contacts/editor/ViewIdGenerator;

    const/4 v5, 0x0

    iput-object p2, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mEntry:Lcom/android/contacts/model/EntityDelta$ValuesDelta;

    iput-boolean p4, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mReadOnly:Z

    const-string v2, "SimPhotoEditorViewGreen"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "the values is = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p5, p3, p1, p2, v5}, Lcom/android/contacts/editor/ViewIdGenerator;->getId(Lcom/android/contacts/model/EntityDelta;Lcom/android/contacts/model/DataKind;Lcom/android/contacts/model/EntityDelta$ValuesDelta;I)I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/view/View;->setId(I)V

    if-eqz p2, :cond_1

    const-string v2, "data15"

    invoke-virtual {p2, v2}, Lcom/android/contacts/model/EntityDelta$ValuesDelta;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    invoke-static {v1, v5, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    const-string v2, "SimPhotoEditorViewGreen"

    const-string v3, "the setValues"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mPhotoImageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mFrameView:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mHasSetPhoto:Z

    iget-object v2, p0, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->mEntry:Lcom/android/contacts/model/EntityDelta$ValuesDelta;

    invoke-virtual {v2, v5}, Lcom/android/contacts/model/EntityDelta$ValuesDelta;->setFromTemplate(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->resetDefault()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/contacts/editor/SimPhotoEditorViewGreen;->resetDefault()V

    goto :goto_0
.end method
