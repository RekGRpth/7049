.class public Lcom/mediatek/contacts/extention/CallLogExtention;
.super Ljava/lang/Object;
.source "CallLogExtention.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public disableCallButton(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    const/16 v1, 0x8

    const v0, 0x7f07004c

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f070059

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f07005b

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f070065

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f07006a

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public getCallDetailHistoryAdapter(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/android/contacts/calllog/CallTypeHelper;[Lcom/android/contacts/PhoneCallDetails;ZZLandroid/view/View;Ljava/lang/String;)Lcom/android/contacts/calllog/CallDetailHistoryAdapter;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Lcom/android/contacts/calllog/CallTypeHelper;
    .param p4    # [Lcom/android/contacts/PhoneCallDetails;
    .param p5    # Z
    .param p6    # Z
    .param p7    # Landroid/view/View;
    .param p8    # Ljava/lang/String;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getLayoutResID()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public layoutRCSIcon(IIIIILandroid/widget/ImageView;)I
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Landroid/widget/ImageView;

    return p4
.end method

.method public measureExtention(Landroid/widget/ImageView;)V
    .locals 0
    .param p1    # Landroid/widget/ImageView;

    return-void
.end method

.method public setEXtenstionItem(Landroid/app/Activity;Landroid/net/Uri;Lcom/android/contacts/PhoneCallDetails;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/net/Uri;
    .param p3    # Lcom/android/contacts/PhoneCallDetails;

    return-void
.end method

.method public setExtentionIcon(Lcom/mediatek/contacts/calllog/CallLogListItemView;Lcom/android/contacts/PhoneCallDetails;)V
    .locals 0
    .param p1    # Lcom/mediatek/contacts/calllog/CallLogListItemView;
    .param p2    # Lcom/android/contacts/PhoneCallDetails;

    return-void
.end method
