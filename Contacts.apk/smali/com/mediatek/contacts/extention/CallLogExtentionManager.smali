.class public Lcom/mediatek/contacts/extention/CallLogExtentionManager;
.super Ljava/lang/Object;
.source "CallLogExtentionManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CallLogExtentionManager"

.field private static sInstance:Lcom/mediatek/contacts/extention/CallLogExtentionManager;


# instance fields
.field private mCallLogExtention:Lcom/mediatek/contacts/extention/CallLogExtention;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/mediatek/contacts/extention/CallLogExtentionManager;->createCallLogExtention()Lcom/mediatek/contacts/extention/CallLogExtention;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/extention/CallLogExtentionManager;->mCallLogExtention:Lcom/mediatek/contacts/extention/CallLogExtention;

    return-void
.end method

.method private createCallLogExtention()Lcom/mediatek/contacts/extention/CallLogExtention;
    .locals 2

    invoke-static {}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->isSupport()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CallLogExtentionManager"

    const-string v1, "get CallLogExtentionForRCS"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;

    invoke-direct {v0}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "CallLogExtentionManager"

    const-string v1, "get CallLogExtention"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/contacts/extention/CallLogExtention;

    invoke-direct {v0}, Lcom/mediatek/contacts/extention/CallLogExtention;-><init>()V

    goto :goto_0
.end method

.method public static getInstance()Lcom/mediatek/contacts/extention/CallLogExtentionManager;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/extention/CallLogExtentionManager;->sInstance:Lcom/mediatek/contacts/extention/CallLogExtentionManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/contacts/extention/CallLogExtentionManager;

    invoke-direct {v0}, Lcom/mediatek/contacts/extention/CallLogExtentionManager;-><init>()V

    sput-object v0, Lcom/mediatek/contacts/extention/CallLogExtentionManager;->sInstance:Lcom/mediatek/contacts/extention/CallLogExtentionManager;

    :cond_0
    sget-object v0, Lcom/mediatek/contacts/extention/CallLogExtentionManager;->sInstance:Lcom/mediatek/contacts/extention/CallLogExtentionManager;

    return-object v0
.end method


# virtual methods
.method public getCallLogExtention()Lcom/mediatek/contacts/extention/CallLogExtention;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/extention/CallLogExtentionManager;->mCallLogExtention:Lcom/mediatek/contacts/extention/CallLogExtention;

    return-object v0
.end method
