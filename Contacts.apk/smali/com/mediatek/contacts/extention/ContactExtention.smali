.class public Lcom/mediatek/contacts/extention/ContactExtention;
.super Ljava/lang/Object;
.source "ContactExtention.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bindExtentionIcon(Lcom/android/contacts/list/ContactListItemView;ILandroid/database/Cursor;)V
    .locals 0
    .param p1    # Lcom/android/contacts/list/ContactListItemView;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;

    return-void
.end method

.method public getExtentionIntent(II)Landroid/content/Intent;
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getExtentionKind(Lcom/android/contacts/model/DataKind;Ljava/lang/String;Landroid/content/ContentValues;)Lcom/android/contacts/model/DataKind;
    .locals 0
    .param p1    # Lcom/android/contacts/model/DataKind;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/ContentValues;

    return-object p1
.end method

.method public getExtentionMimeType()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getExtentionTitles(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-object p2
.end method

.method public getLayoutResID()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getRCSIcon(J)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public layoutExtentionIcon(IIIIILandroid/widget/ImageView;)I
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Landroid/widget/ImageView;

    return p4
.end method

.method public measureExtentionIcon(Landroid/widget/ImageView;)V
    .locals 0
    .param p1    # Landroid/widget/ImageView;

    return-void
.end method

.method public onContactDetialOpen(Landroid/net/Uri;)V
    .locals 0
    .param p1    # Landroid/net/Uri;

    return-void
.end method

.method public setDetailKindView(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public setExtentionButton(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/app/Activity;

    return-void
.end method

.method public setExtentionIcon(Lcom/mediatek/contacts/extention/ContactExtention;Lcom/android/contacts/list/ContactTileAdapter$ContactEntry;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # Lcom/mediatek/contacts/extention/ContactExtention;
    .param p2    # Lcom/android/contacts/list/ContactTileAdapter$ContactEntry;

    const/4 v0, 0x0

    return-object v0
.end method

.method public setExtentionSubTitle(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public setScondBuotton(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/app/Activity;

    return-void
.end method
