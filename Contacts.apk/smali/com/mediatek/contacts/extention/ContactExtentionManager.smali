.class public Lcom/mediatek/contacts/extention/ContactExtentionManager;
.super Ljava/lang/Object;
.source "ContactExtentionManager.java"


# static fields
.field public static final RCS_CONTACT_PRESENCE_CHANGED:Ljava/lang/String; = "android.intent.action.RCS_CONTACT_PRESENCE_CHANGED"

.field private static final TAG:Ljava/lang/String; = "ContactExtentionManager"

.field private static sInstance:Lcom/mediatek/contacts/extention/ContactExtentionManager;


# instance fields
.field private mContactExtention:Lcom/mediatek/contacts/extention/ContactExtention;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/mediatek/contacts/extention/ContactExtentionManager;->createContactExtention()Lcom/mediatek/contacts/extention/ContactExtention;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/extention/ContactExtentionManager;->mContactExtention:Lcom/mediatek/contacts/extention/ContactExtention;

    return-void
.end method

.method private createContactExtention()Lcom/mediatek/contacts/extention/ContactExtention;
    .locals 2

    invoke-static {}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->isSupport()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ContactExtentionManager"

    const-string v1, "get ContactExtentionForRCS"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;

    invoke-direct {v0}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "ContactExtentionManager"

    const-string v1, "get ContactExtention"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/contacts/extention/ContactExtention;

    invoke-direct {v0}, Lcom/mediatek/contacts/extention/ContactExtention;-><init>()V

    goto :goto_0
.end method

.method public static getInstance()Lcom/mediatek/contacts/extention/ContactExtentionManager;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/extention/ContactExtentionManager;->sInstance:Lcom/mediatek/contacts/extention/ContactExtentionManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/contacts/extention/ContactExtentionManager;

    invoke-direct {v0}, Lcom/mediatek/contacts/extention/ContactExtentionManager;-><init>()V

    sput-object v0, Lcom/mediatek/contacts/extention/ContactExtentionManager;->sInstance:Lcom/mediatek/contacts/extention/ContactExtentionManager;

    :cond_0
    sget-object v0, Lcom/mediatek/contacts/extention/ContactExtentionManager;->sInstance:Lcom/mediatek/contacts/extention/ContactExtentionManager;

    return-object v0
.end method


# virtual methods
.method public getContactExtention()Lcom/mediatek/contacts/extention/ContactExtention;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/extention/ContactExtentionManager;->mContactExtention:Lcom/mediatek/contacts/extention/ContactExtention;

    return-object v0
.end method
