.class public interface abstract Lcom/mediatek/contacts/extention/ICallLogExtention;
.super Ljava/lang/Object;
.source "ICallLogExtention.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/extention/ICallLogExtention$Action;,
        Lcom/mediatek/contacts/extention/ICallLogExtention$OnPresenceChangedListener;
    }
.end annotation


# virtual methods
.method public abstract addOnPresenceChangedListener(Lcom/mediatek/contacts/extention/ICallLogExtention$OnPresenceChangedListener;Ljava/lang/String;)V
.end method

.method public abstract getAppIcon()Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getChatString()Ljava/lang/String;
.end method

.method public abstract getContactActions(Ljava/lang/String;)[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;
.end method

.method public abstract getContactPresence(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
.end method

.method public abstract isEnabled()Z
.end method
