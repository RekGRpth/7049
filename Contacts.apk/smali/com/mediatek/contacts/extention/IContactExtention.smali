.class public interface abstract Lcom/mediatek/contacts/extention/IContactExtention;
.super Ljava/lang/Object;
.source "IContactExtention.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/extention/IContactExtention$Action;,
        Lcom/mediatek/contacts/extention/IContactExtention$OnPresenceChangedListener;
    }
.end annotation


# virtual methods
.method public abstract addOnPresenceChangedListener(Lcom/mediatek/contacts/extention/IContactExtention$OnPresenceChangedListener;J)V
.end method

.method public abstract getAppIcon()Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getAppTitle()Ljava/lang/String;
.end method

.method public abstract getContactActions()[Lcom/mediatek/contacts/extention/IContactExtention$Action;
.end method

.method public abstract getContactPresence(J)Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getMimeType()Ljava/lang/String;
.end method

.method public abstract getRCSIcon(J)Landroid/graphics/drawable/Drawable;
.end method

.method public abstract isEnabled()Z
.end method

.method public abstract onContactDetailOpen(Landroid/net/Uri;)V
.end method
