.class public Lcom/mediatek/contacts/extention/aassne/SneExt;
.super Ljava/lang/Object;
.source "SneExt.java"


# static fields
.field private static final ERROR:I = -0x1

.field private static MAX_USIM_SNE_MAX_LENGTH:[I = null

.field private static final MTK_GEMINI_SUPPORT:Z = true

.field private static final SIMPHONEBOOK2_SERVICE:Ljava/lang/String; = "simphonebook2"

.field private static final SIMPHONEBOOK_SERVICE:Ljava/lang/String; = "simphonebook"

.field private static final SLOT_ID1:I = 0x0

.field private static final SLOT_ID2:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SnesExt"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/contacts/extention/aassne/SneExt;->MAX_USIM_SNE_MAX_LENGTH:[I

    return-void

    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildNicknameValueForInsert(ILandroid/content/ContentValues;Ljava/lang/String;)Z
    .locals 2
    .param p0    # I
    .param p1    # Landroid/content/ContentValues;
    .param p2    # Ljava/lang/String;

    invoke-static {p0}, Lcom/mediatek/contacts/extention/aassne/SneExt;->hasSne(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "sne"

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p2, ""

    :cond_0
    invoke-virtual {p1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ensureNicknameKindForEditorExt(Lcom/android/contacts/model/AccountType;ILcom/android/contacts/model/EntityDelta;)V
    .locals 4
    .param p0    # Lcom/android/contacts/model/AccountType;
    .param p1    # I
    .param p2    # Lcom/android/contacts/model/EntityDelta;

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v1

    iget-object v2, p0, Lcom/android/contacts/model/AccountType;->accountType:Ljava/lang/String;

    const-string v3, "SNE"

    invoke-virtual {v1, v2, v3}, Lcom/android/contacts/ext/ContactAccountExtension;->isFeatureAccount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "vnd.android.cursor.item/nickname"

    invoke-virtual {p0, v1}, Lcom/android/contacts/model/AccountType;->getKindForMimetype(Ljava/lang/String;)Lcom/android/contacts/model/DataKind;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/mediatek/contacts/extention/aassne/SneExt;->hasSne(I)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/contacts/model/UsimAccountType;->updateNickname(Lcom/android/contacts/model/DataKind;Z)V

    :cond_0
    const-string v1, "vnd.android.cursor.item/nickname"

    invoke-static {p2, p0, v1}, Lcom/android/contacts/model/EntityModifier;->ensureKindExists(Lcom/android/contacts/model/EntityDelta;Lcom/android/contacts/model/AccountType;Ljava/lang/String;)Lcom/android/contacts/model/EntityDelta$ValuesDelta;

    :cond_1
    return-void
.end method

.method public static getSneRecordMaxLen(I)I
    .locals 6
    .param p0    # I

    const/4 v2, -0x1

    const-string v3, "SnesExt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[getSneRecordMaxLen]end slot:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ltz p0, :cond_0

    const/4 v3, 0x1

    if-le p0, v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    sget-object v3, Lcom/mediatek/contacts/extention/aassne/SneExt;->MAX_USIM_SNE_MAX_LENGTH:[I

    aget v3, v3, p0

    if-gez v3, :cond_2

    :try_start_0
    invoke-static {p0}, Lcom/mediatek/contacts/extention/aassne/SimUtils;->getIIccPhoneBook(I)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v3, Lcom/mediatek/contacts/extention/aassne/SneExt;->MAX_USIM_SNE_MAX_LENGTH:[I

    invoke-interface {v1}, Lcom/android/internal/telephony/IIccPhoneBook;->getSneRecordLen()I

    move-result v4

    aput v4, v3, p0

    const-string v3, "SnesExt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSneRecordMaxLen, len="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/mediatek/contacts/extention/aassne/SneExt;->MAX_USIM_SNE_MAX_LENGTH:[I

    aget v5, v5, p0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    const-string v2, "SnesExt"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[getSneRecordMaxLen]maxNameLen:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/extention/aassne/SneExt;->MAX_USIM_SNE_MAX_LENGTH:[I

    aget v4, v4, p0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/mediatek/contacts/extention/aassne/SneExt;->MAX_USIM_SNE_MAX_LENGTH:[I

    aget v2, v2, p0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "SnesExt"

    const-string v4, "catched exception."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/mediatek/contacts/extention/aassne/SneExt;->MAX_USIM_SNE_MAX_LENGTH:[I

    aput v2, v3, p0

    goto :goto_1
.end method

.method public static hasSne(I)Z
    .locals 7
    .param p0    # I

    const-string v4, "SnesExt"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[hasSne]slot:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    if-ltz p0, :cond_0

    const/4 v4, 0x1

    if-le p0, v4, :cond_1

    :cond_0
    move v2, v1

    :goto_0
    return v2

    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/mediatek/contacts/extention/aassne/SimUtils;->getIIccPhoneBook(I)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v3}, Lcom/android/internal/telephony/IIccPhoneBook;->hasSne()Z

    move-result v1

    const-string v4, "SnesExt"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "hasSne, hasSne="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    const-string v4, "SnesExt"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[hasSne] hasSne:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "SnesExt"

    const-string v5, "[hasSne] exception."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static isNickname(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    invoke-static {}, Lcom/mediatek/contacts/extention/aassne/SneExt;->isSneEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "vnd.android.cursor.item/nickname"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSneEnable()Z
    .locals 2

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v0

    const-string v1, "SNE"

    invoke-virtual {v0, v1}, Lcom/android/contacts/ext/ContactAccountExtension;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static updateDataToDb(ILjava/lang/String;Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 8
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # J

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v0

    const-string v1, "SNE"

    invoke-virtual {v0, p1, v1}, Lcom/android/contacts/ext/ContactAccountExtension;->isFeatureAccount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/mediatek/contacts/extention/aassne/SneExt;->hasSne(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    move-wide v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/android/contacts/ext/ContactAccountExtension;->updateDataToDb(Ljava/lang/String;Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/util/ArrayList;JI)Z

    :cond_0
    return-void
.end method
