.class public Lcom/mediatek/contacts/extention/rcs/CallDetailHistoryAdapterForRCS;
.super Lcom/android/contacts/calllog/CallDetailHistoryAdapter;
.source "CallDetailHistoryAdapterForRCS.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CallDetailHistoryAdapterForRCS"


# instance fields
.field private mNumber:Ljava/lang/String;

.field private mShowRCS:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/android/contacts/calllog/CallTypeHelper;[Lcom/android/contacts/PhoneCallDetails;ZZLandroid/view/View;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Lcom/android/contacts/calllog/CallTypeHelper;
    .param p4    # [Lcom/android/contacts/PhoneCallDetails;
    .param p5    # Z
    .param p6    # Z
    .param p7    # Landroid/view/View;
    .param p8    # Ljava/lang/String;

    invoke-direct/range {p0 .. p7}, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/android/contacts/calllog/CallTypeHelper;[Lcom/android/contacts/PhoneCallDetails;ZZLandroid/view/View;)V

    iput-object p8, p0, Lcom/mediatek/contacts/extention/rcs/CallDetailHistoryAdapterForRCS;->mNumber:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/16 v7, 0x8

    const/4 v6, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    if-nez p1, :cond_0

    const/4 v3, 0x0

    invoke-static {}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->getCallLogPlugin()Lcom/mediatek/contacts/extention/ICallLogExtention;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-static {}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->getCallLogPlugin()Lcom/mediatek/contacts/extention/ICallLogExtention;

    move-result-object v5

    invoke-interface {v5}, Lcom/mediatek/contacts/extention/ICallLogExtention;->isEnabled()Z

    move-result v3

    invoke-static {}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->getCallLogPlugin()Lcom/mediatek/contacts/extention/ICallLogExtention;

    move-result-object v5

    iget-object v8, p0, Lcom/mediatek/contacts/extention/rcs/CallDetailHistoryAdapterForRCS;->mNumber:Ljava/lang/String;

    invoke-interface {v5, v8}, Lcom/mediatek/contacts/extention/ICallLogExtention;->getContactPresence(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v5, 0x1

    :goto_0
    iput-boolean v5, p0, Lcom/mediatek/contacts/extention/rcs/CallDetailHistoryAdapterForRCS;->mShowRCS:Z

    const-string v5, "CallDetailHistoryAdapterForRCS"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isEnabledRCS : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " | mShowRCS : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/mediatek/contacts/extention/rcs/CallDetailHistoryAdapterForRCS;->mShowRCS:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const v5, 0x7f07005e

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v5, p0, Lcom/mediatek/contacts/extention/rcs/CallDetailHistoryAdapterForRCS;->mShowRCS:Z

    if-eqz v5, :cond_3

    if-eqz v3, :cond_3

    move v5, v6

    :goto_2
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    const v5, 0x7f07005d

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-boolean v5, p0, Lcom/mediatek/contacts/extention/rcs/CallDetailHistoryAdapterForRCS;->mShowRCS:Z

    if-eqz v5, :cond_4

    if-eqz v3, :cond_4

    :goto_3
    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-object v2

    :cond_1
    move v5, v6

    goto :goto_0

    :cond_2
    const-string v5, "CallDetailHistoryAdapterForRCS"

    const-string v8, "CallLogPlugin is null"

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    move v5, v7

    goto :goto_2

    :cond_4
    move v6, v7

    goto :goto_3
.end method
