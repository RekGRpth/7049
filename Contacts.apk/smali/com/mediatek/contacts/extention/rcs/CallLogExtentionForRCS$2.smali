.class Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS$2;
.super Ljava/lang/Object;
.source "CallLogExtentionForRCS.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS$2;->this$0:Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/contacts/PhoneCallDetails;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS$2;->this$0:Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;

    invoke-static {v4}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->access$000(Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;)[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

    move-result-object v4

    aget-object v4, v4, v5

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS$2;->this$0:Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;

    invoke-static {v4}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->access$000(Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;)[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

    move-result-object v4

    aget-object v4, v4, v5

    iget-object v1, v4, Lcom/mediatek/contacts/extention/ICallLogExtention$Action;->intentAction:Landroid/content/Intent;

    const-string v4, "CallLogExtentionForRCS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[mRCSTransforActionListener] intent : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    if-eqz v0, :cond_3

    iget-object v4, v0, Lcom/android/contacts/PhoneCallDetails;->name:Ljava/lang/CharSequence;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/android/contacts/PhoneCallDetails;->name:Ljava/lang/CharSequence;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_0
    iget-object v4, v0, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CallLogExtentionForRCS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[mRCSTransforActionListener] name : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " | number : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v2, v3

    :cond_1
    const-string v4, "rcs_display_name"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "rcs_phone_number"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    iget-object v4, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS$2;->this$0:Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;

    invoke-static {v4}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->access$100(Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_2
    const-string v4, "CallLogExtentionForRCS"

    const-string v5, "[mRCSTransforActionListener] mRCSActions[1] is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v4, "CallLogExtentionForRCS"

    const-string v5, "[mRCSTransforActionListener] details is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
