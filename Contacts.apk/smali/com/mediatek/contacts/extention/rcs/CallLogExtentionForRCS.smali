.class public Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;
.super Lcom/mediatek/contacts/extention/CallLogExtention;
.source "CallLogExtentionForRCS.java"


# static fields
.field public static final RCS_DISPLAY_NAME:Ljava/lang/String; = "rcs_display_name"

.field public static final RCS_PHONE_NUMBER:Ljava/lang/String; = "rcs_phone_number"

.field private static final TAG:Ljava/lang/String; = "CallLogExtentionForRCS"

.field private static sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mRCSActions:[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

.field private mRCSIcon:Landroid/widget/ImageView;

.field private mRCSIconViewHeight:I

.field private mRCSIconViewWidth:I

.field private mRCSIconViewWidthAndHeightAreReady:Z

.field private final mRCSTextActionListener:Landroid/view/View$OnClickListener;

.field private final mRCSTransforActionListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/contacts/extention/CallLogExtention;-><init>()V

    new-instance v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS$1;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS$1;-><init>(Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;)V

    iput-object v0, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSTextActionListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS$2;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS$2;-><init>(Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;)V

    iput-object v0, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSTransforActionListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;)[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;

    iget-object v0, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSActions:[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;

    iget-object v0, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public static getCallLogPlugin()Lcom/mediatek/contacts/extention/ICallLogExtention;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    return-object v0
.end method

.method public static isSupport()Z
    .locals 8

    const/4 v5, 0x0

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v4

    const-class v6, Lcom/mediatek/contacts/extention/ICallLogExtention;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    new-array v7, v5, [Landroid/content/pm/Signature;

    invoke-static {v4, v6, v7}, Lcom/mediatek/pluginmanager/PluginManager;->create(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Lcom/mediatek/pluginmanager/PluginManager;

    move-result-object v3

    const/4 v0, 0x0

    invoke-virtual {v3}, Lcom/mediatek/pluginmanager/PluginManager;->getPluginCount()I

    move-result v2

    if-nez v2, :cond_0

    const-string v4, "CallLogExtentionForRCS"

    const-string v6, "no plugin apk"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    :goto_0
    return v4

    :cond_0
    invoke-virtual {v3, v5}, Lcom/mediatek/pluginmanager/PluginManager;->getPlugin(I)Lcom/mediatek/pluginmanager/Plugin;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {v0}, Lcom/mediatek/pluginmanager/Plugin;->createObject()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/contacts/extention/ICallLogExtention;

    sput-object v4, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-object v4, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "CallLogExtentionForRCS"

    const-string v6, "error get object"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    const-string v4, "CallLogExtentionForRCS"

    const-string v6, "callLogPlugin is null"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    const-string v4, "CallLogExtentionForRCS"

    const-string v6, "sCallLogPlugin is null"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    goto :goto_0
.end method


# virtual methods
.method public disableCallButton(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    const/16 v1, 0x8

    const v0, 0x7f07004c

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f070059

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f07005b

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f070065

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f07006a

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f07006e

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f07005d

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public getCallDetailHistoryAdapter(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/android/contacts/calllog/CallTypeHelper;[Lcom/android/contacts/PhoneCallDetails;ZZLandroid/view/View;Ljava/lang/String;)Lcom/android/contacts/calllog/CallDetailHistoryAdapter;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Lcom/android/contacts/calllog/CallTypeHelper;
    .param p4    # [Lcom/android/contacts/PhoneCallDetails;
    .param p5    # Z
    .param p6    # Z
    .param p7    # Landroid/view/View;
    .param p8    # Ljava/lang/String;

    new-instance v0, Lcom/mediatek/contacts/extention/rcs/CallDetailHistoryAdapterForRCS;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/mediatek/contacts/extention/rcs/CallDetailHistoryAdapterForRCS;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/android/contacts/calllog/CallTypeHelper;[Lcom/android/contacts/PhoneCallDetails;ZZLandroid/view/View;Ljava/lang/String;)V

    return-object v0
.end method

.method public getLayoutResID()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method protected isVisible(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public layoutRCSIcon(IIIIILandroid/widget/ImageView;)I
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Landroid/widget/ImageView;

    invoke-virtual {p0, p6}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->isVisible(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p6, :cond_0

    sub-int v1, p3, p2

    iget v2, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewHeight:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v0, p2, v1

    iget v1, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewWidth:I

    sub-int v1, p4, v1

    iget v2, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewHeight:I

    add-int/2addr v2, v0

    invoke-virtual {p6, v1, v0, p4, v2}, Landroid/view/View;->layout(IIII)V

    iget v1, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewWidth:I

    add-int/2addr v1, p5

    sub-int/2addr p4, v1

    :cond_0
    return p4
.end method

.method public measureExtention(Landroid/widget/ImageView;)V
    .locals 4
    .param p1    # Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->isVisible(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewWidthAndHeightAreReady:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    invoke-interface {v1}, Lcom/mediatek/contacts/extention/ICallLogExtention;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewWidth:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewHeight:I

    :goto_0
    const-string v1, "CallLogExtentionForRCS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "measureExtention mRCSIconViewWidth : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | mRCSIconViewHeight : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewWidthAndHeightAreReady:Z

    :cond_0
    return-void

    :cond_1
    iput v2, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewWidth:I

    iput v2, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewHeight:I

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewWidth:I

    iput v2, p0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSIconViewHeight:I

    goto :goto_0
.end method

.method public setEXtenstionItem(Landroid/app/Activity;Landroid/net/Uri;Lcom/android/contacts/PhoneCallDetails;)V
    .locals 22
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/net/Uri;
    .param p3    # Lcom/android/contacts/PhoneCallDetails;

    const/4 v14, 0x0

    const/4 v7, 0x0

    const/4 v4, 0x0

    const/4 v12, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mActivity:Landroid/app/Activity;

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v13, 0x0

    if-eqz p3, :cond_5

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/contacts/PhoneCallDetails;->name:Ljava/lang/CharSequence;

    move-object/from16 v19, v0

    if-eqz v19, :cond_4

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/contacts/PhoneCallDetails;->name:Ljava/lang/CharSequence;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    :goto_0
    const-string v19, "CallLogExtentionForRCS"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "[setEXtenstionItem] Number = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " | contactUri : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " | firstDetails : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " | displayName : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v19, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    if-eqz v19, :cond_8

    sget-object v19, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    invoke-interface/range {v19 .. v19}, Lcom/mediatek/contacts/extention/ICallLogExtention;->getChatString()Ljava/lang/String;

    move-result-object v4

    sget-object v19, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    move-object/from16 v0, v19

    invoke-interface {v0, v14}, Lcom/mediatek/contacts/extention/ICallLogExtention;->getContactPresence(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    sget-object v19, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    invoke-interface/range {v19 .. v19}, Lcom/mediatek/contacts/extention/ICallLogExtention;->isEnabled()Z

    move-result v13

    sget-object v19, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    move-object/from16 v0, v19

    invoke-interface {v0, v14}, Lcom/mediatek/contacts/extention/ICallLogExtention;->getContactActions(Ljava/lang/String;)[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSActions:[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSActions:[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aget-object v19, v19, v20

    if-eqz v19, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSActions:[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    aget-object v19, v19, v20

    if-eqz v19, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSActions:[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aget-object v19, v19, v20

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/mediatek/contacts/extention/ICallLogExtention$Action;->intentAction:Landroid/content/Intent;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    const/4 v10, 0x1

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSActions:[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    aget-object v19, v19, v20

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/mediatek/contacts/extention/ICallLogExtention$Action;->intentAction:Landroid/content/Intent;

    move-object/from16 v19, v0

    if-eqz v19, :cond_1

    const/4 v9, 0x1

    :cond_1
    :goto_1
    const-string v20, "CallLogExtentionForRCS"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "[setEXtenstionItem] RCSicon : "

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    if-eqz v12, :cond_7

    const/16 v19, 0x1

    :goto_2
    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v21, " | isEnable : "

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v21, " | hasIM , hasFT : "

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v21, " , "

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSActions:[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    aget-object v19, v19, v20

    if-eqz v19, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSActions:[Lcom/mediatek/contacts/extention/ICallLogExtention$Action;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    aget-object v19, v19, v20

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/mediatek/contacts/extention/ICallLogExtention$Action;->icon:Landroid/graphics/drawable/Drawable;

    :goto_4
    if-eqz v12, :cond_a

    if-eqz v13, :cond_a

    const/16 v16, 0x1

    :goto_5
    const v19, 0x7f07006d

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v19, 0x7f07005d

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    const v19, 0x7f07006e

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const v19, 0x7f07006f

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "CallLogExtentionForRCS"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "[setEXtenstionItem] chat = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " | textValueForRCS : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v10, :cond_2

    move-object/from16 v18, v14

    :cond_2
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const v19, 0x7f070070

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v19, 0x7f070072

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSTransforActionListener:Landroid/view/View$OnClickListener;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const v19, 0x7f070071

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->mRCSTextActionListener:Landroid/view/View$OnClickListener;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v11, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    if-eqz v16, :cond_b

    const/16 v19, 0x0

    :goto_6
    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz v16, :cond_c

    const/16 v19, 0x0

    :goto_7
    move/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v16, :cond_d

    const/16 v19, 0x0

    :goto_8
    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz v16, :cond_e

    const/16 v19, 0x0

    :goto_9
    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    if-eqz v16, :cond_f

    const/16 v19, 0x0

    :goto_a
    move/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz v10, :cond_10

    if-nez v9, :cond_10

    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_b
    return-void

    :cond_4
    const-string v19, "CallLogExtentionForRCS"

    const-string v20, "[setEXtenstionItem] name is null"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    const-string v19, "CallLogExtentionForRCS"

    const-string v20, "[setEXtenstionItem]firstDetails is null"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    const-string v19, "CallLogExtentionForRCS"

    const-string v20, "[setEXtenstionItem] mRCSActions is null"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_7
    const/16 v19, 0x0

    goto/16 :goto_2

    :cond_8
    const-string v19, "CallLogExtentionForRCS"

    const-string v20, "[setEXtenstionItem]sCallLogPlugin is null"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_9
    const-string v19, "CallLogExtentionForRCS"

    const-string v20, "[setEXtenstionItem] mRCSActions[1] is null"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_a
    const/16 v16, 0x0

    goto/16 :goto_5

    :cond_b
    const/16 v19, 0x8

    goto :goto_6

    :cond_c
    const/16 v19, 0x8

    goto :goto_7

    :cond_d
    const/16 v19, 0x8

    goto :goto_8

    :cond_e
    const/16 v19, 0x8

    goto :goto_9

    :cond_f
    const/16 v19, 0x8

    goto :goto_a

    :cond_10
    if-nez v10, :cond_11

    if-eqz v9, :cond_11

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/view/View;->setClickable(Z)V

    goto :goto_b

    :cond_11
    if-nez v10, :cond_3

    if-nez v9, :cond_3

    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    const/16 v19, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_b
.end method

.method public setExtentionIcon(Lcom/mediatek/contacts/calllog/CallLogListItemView;Lcom/android/contacts/PhoneCallDetails;)V
    .locals 5
    .param p1    # Lcom/mediatek/contacts/calllog/CallLogListItemView;
    .param p2    # Lcom/android/contacts/PhoneCallDetails;

    invoke-virtual {p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->removeExtentionIconView()V

    sget-object v2, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    sget-object v2, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    iget-object v3, p2, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/mediatek/contacts/extention/ICallLogExtention;->getContactPresence(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v2, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    invoke-interface {v2}, Lcom/mediatek/contacts/extention/ICallLogExtention;->isEnabled()Z

    move-result v1

    const-string v2, "CallLogExtentionForRCS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[setExtentionIcon] isEnabled : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->setExtentionIcon(Z)V

    :goto_0
    return-void

    :cond_0
    const-string v2, "CallLogExtentionForRCS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[setExtentionIcon] details.contactUri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/android/contacts/PhoneCallDetails;->contactUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | a : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " |isEnabled : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->setExtentionIcon(Z)V

    goto :goto_0

    :cond_1
    const-string v2, "CallLogExtentionForRCS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[setExtentionIcon] sCallLogPlugin : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/extention/rcs/CallLogExtentionForRCS;->sCallLogPlugin:Lcom/mediatek/contacts/extention/ICallLogExtention;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | details : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | itemView : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
