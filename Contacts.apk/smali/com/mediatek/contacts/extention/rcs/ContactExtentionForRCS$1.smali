.class Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$1;
.super Ljava/lang/Object;
.source "ContactExtentionForRCS.java"

# interfaces
.implements Lcom/mediatek/contacts/extention/IContactExtention$OnPresenceChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->bindExtentionIcon(Lcom/android/contacts/list/ContactListItemView;ILandroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$1;->this$0:Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPresenceChanged(JI)V
    .locals 4
    .param p1    # J
    .param p3    # I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.RCS_CONTACT_PRESENCE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "ContactExtentionForRCS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[bindExtentionIcon] contactId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | presence : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
