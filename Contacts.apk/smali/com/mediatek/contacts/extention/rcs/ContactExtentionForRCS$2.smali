.class Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$2;
.super Ljava/lang/Object;
.source "ContactExtentionForRCS.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$2;->this$0:Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string v1, "ContactExtentionForRCS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[mRCSSecondonClickListener] name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$2;->this$0:Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;

    invoke-static {v3}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->access$000(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | number : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$2;->this$0:Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;

    invoke-static {v3}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->access$100(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "rcs_display_name"

    iget-object v2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$2;->this$0:Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;

    invoke-static {v2}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->access$000(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "rcs_phone_number"

    iget-object v2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$2;->this$0:Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;

    invoke-static {v2}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->access$100(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$2;->this$0:Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;

    invoke-static {v1}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->access$200(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "ContactExtentionForRCS"

    const-string v2, "[mRCSSecondonClickListener] intent is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
