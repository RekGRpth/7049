.class public Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;
.super Lcom/mediatek/contacts/extention/ContactExtention;
.source "ContactExtentionForRCS.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<mRCSSecondonClick",
        "Listener:Ljava/lang/Object;",
        ">",
        "Lcom/mediatek/contacts/extention/ContactExtention;"
    }
.end annotation


# static fields
.field private static final DATA_KIND_WEIGHT:I = 0xa

.field public static final RCS_DISPLAY_NAME:Ljava/lang/String; = "rcs_display_name"

.field public static final RCS_PHONE_NUMBER:Ljava/lang/String; = "rcs_phone_number"

.field private static final TAG:Ljava/lang/String; = "ContactExtentionForRCS"

.field private static sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mDetailView:Landroid/view/View;

.field private mFTValue:I

.field private mIMValue:I

.field private mName:Ljava/lang/String;

.field private mNumber:Ljava/lang/String;

.field private mRCSIconViewHeight:I

.field private mRCSIconViewWidth:I

.field private mRCSIconViewWidthAndHeightAreReady:Z

.field private final mRCSSecondonClickListener:Landroid/view/View$OnClickListener;

.field private mSetScondBuottononClickListner:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/contacts/extention/ContactExtention;-><init>()V

    new-instance v0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$2;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$2;-><init>(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)V

    iput-object v0, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSSecondonClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$3;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$3;-><init>(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)V

    iput-object v0, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mSetScondBuottononClickListner:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;

    iget-object v0, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;

    iget-object v0, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;

    iget-object v0, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public static getContactPlugin()Lcom/mediatek/contacts/extention/IContactExtention;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    return-object v0
.end method

.method public static isSupport()Z
    .locals 8

    const/4 v5, 0x0

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v4

    const-class v6, Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    new-array v7, v5, [Landroid/content/pm/Signature;

    invoke-static {v4, v6, v7}, Lcom/mediatek/pluginmanager/PluginManager;->create(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Lcom/mediatek/pluginmanager/PluginManager;

    move-result-object v3

    const/4 v0, 0x0

    invoke-virtual {v3}, Lcom/mediatek/pluginmanager/PluginManager;->getPluginCount()I

    move-result v2

    if-nez v2, :cond_0

    const-string v4, "ContactExtentionForRCS"

    const-string v6, "no plugin apk"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    :goto_0
    return v4

    :cond_0
    invoke-virtual {v3, v5}, Lcom/mediatek/pluginmanager/PluginManager;->getPlugin(I)Lcom/mediatek/pluginmanager/Plugin;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {v0}, Lcom/mediatek/pluginmanager/Plugin;->createObject()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/contacts/extention/IContactExtention;

    sput-object v4, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-object v4, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "ContactExtentionForRCS"

    const-string v6, "error in get object"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    const-string v4, "ContactExtentionForRCS"

    const-string v6, "contactPlugin is null"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    const-string v4, "ContactExtentionForRCS"

    const-string v6, "sContactPlugin is null"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    goto :goto_0
.end method


# virtual methods
.method public bindExtentionIcon(Lcom/android/contacts/list/ContactListItemView;ILandroid/database/Cursor;)V
    .locals 8
    .param p1    # Lcom/android/contacts/list/ContactListItemView;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/android/contacts/list/ContactListItemView;->removeExtentionIconView()V

    const-string v4, "ContactExtentionForRCS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[bindExtentionIcon] view : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v4, :cond_1

    sget-object v4, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v4}, Lcom/mediatek/contacts/extention/IContactExtention;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "_id"

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const-string v4, "ContactExtentionForRCS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[bindExtentionIcon] contactId : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v4, v1, v2}, Lcom/mediatek/contacts/extention/IContactExtention;->getContactPresence(J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v4, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    new-instance v5, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$1;

    invoke-direct {v5, p0}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS$1;-><init>(Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;)V

    invoke-interface {v4, v5, v1, v2}, Lcom/mediatek/contacts/extention/IContactExtention;->addOnPresenceChangedListener(Lcom/mediatek/contacts/extention/IContactExtention$OnPresenceChangedListener;J)V

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lcom/android/contacts/list/ContactListItemView;->setExtentionIcon(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, v7}, Lcom/android/contacts/list/ContactListItemView;->setExtentionIcon(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v7}, Lcom/android/contacts/list/ContactListItemView;->setExtentionIcon(Z)V

    goto :goto_0
.end method

.method public getExtentionIntent(II)Landroid/content/Intent;
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x1

    const/4 v1, 0x0

    iput p1, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mIMValue:I

    iput p2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mFTValue:I

    sget-object v2, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v2, :cond_2

    sget-object v2, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v2}, Lcom/mediatek/contacts/extention/IContactExtention;->getContactActions()[Lcom/mediatek/contacts/extention/IContactExtention$Action;

    move-result-object v0

    iget v2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mIMValue:I

    if-ne v2, v3, :cond_1

    const/4 v2, 0x0

    aget-object v2, v0, v2

    iget-object v1, v2, Lcom/mediatek/contacts/extention/IContactExtention$Action;->intentAction:Landroid/content/Intent;

    :cond_0
    :goto_0
    const-string v2, "ContactExtentionForRCS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[getExtentionIntent] intent : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | im : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | ft : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :cond_1
    iget v2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mFTValue:I

    if-ne v2, v3, :cond_0

    aget-object v2, v0, v3

    iget-object v1, v2, Lcom/mediatek/contacts/extention/IContactExtention$Action;->intentAction:Landroid/content/Intent;

    goto :goto_0

    :cond_2
    const-string v2, "ContactExtentionForRCS"

    const-string v3, "[getExtentionIntent] sContactPlugin is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getExtentionKind(Lcom/android/contacts/model/DataKind;Ljava/lang/String;Landroid/content/ContentValues;)Lcom/android/contacts/model/DataKind;
    .locals 6
    .param p1    # Lcom/android/contacts/model/DataKind;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/ContentValues;

    const/4 v2, 0x0

    sget-object v3, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v3, :cond_2

    sget-object v3, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v3}, Lcom/mediatek/contacts/extention/IContactExtention;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v3}, Lcom/mediatek/contacts/extention/IContactExtention;->getMimeType()Ljava/lang/String;

    move-result-object v1

    const-string v3, "ContactExtentionForRCS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[getExtentionKind] newMimeType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz p3, :cond_0

    const-string v3, "display_name"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mName:Ljava/lang/String;

    :cond_0
    new-instance v0, Lcom/android/contacts/model/DataKind;

    const/16 v3, 0xa

    const v5, 0x7f0400c0

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/contacts/model/DataKind;-><init>(Ljava/lang/String;IIZI)V

    const-string v3, "ContactExtentionForRCS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[getExtentionKind] newkind : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " |Title : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " | mName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/android/contacts/model/BaseAccountType$SimpleInflater;

    const-string v4, "data1"

    invoke-direct {v3, v4}, Lcom/android/contacts/model/BaseAccountType$SimpleInflater;-><init>(Ljava/lang/String;)V

    iput-object v3, v0, Lcom/android/contacts/model/DataKind;->actionBody:Lcom/android/contacts/model/AccountType$StringInflater;

    iput v2, v0, Lcom/android/contacts/model/DataKind;->titleRes:I

    move-object p1, v0

    :goto_0
    return-object p1

    :cond_1
    const-string v2, "ContactExtentionForRCS"

    const-string v3, "[getExtentionKind] retrun kind "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v2, :cond_3

    if-eqz p2, :cond_3

    sget-object v2, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v2}, Lcom/mediatek/contacts/extention/IContactExtention;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "ContactExtentionForRCS"

    const-string v3, "[getExtentionKind] rcs is turn off so return null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    goto :goto_0

    :cond_3
    const-string v2, "ContactExtentionForRCS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[getExtentionKind] sContactPlugin is null or not enabled sContactPlugin : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | mimeType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getExtentionMimeType()Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    sget-object v2, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v2}, Lcom/mediatek/contacts/extention/IContactExtention;->getMimeType()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ContactExtentionForRCS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getExtentionMimeType mimeType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    :goto_0
    return-object v1

    :cond_0
    const-string v2, "ContactExtentionForRCS"

    const-string v3, "getExtentionMimeType sContactPlugin is null "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    goto :goto_0
.end method

.method public getExtentionTitles(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    sget-object v1, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v1}, Lcom/mediatek/contacts/extention/IContactExtention;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v1}, Lcom/mediatek/contacts/extention/IContactExtention;->getAppTitle()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ContactExtentionForRCS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[getExtentionTitles] title : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "ContactExtentionForRCS"

    const-string v2, "getExtentionTitles return null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    goto :goto_0
.end method

.method public getLayoutResID()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getRCSIcon(J)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1    # J

    sget-object v0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v0, p1, p2}, Lcom/mediatek/contacts/extention/IContactExtention;->getRCSIcon(J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "ContactExtentionForRCS"

    const-string v1, "[getRCSIcon] mContactPlutin is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "ContactExtentionForRCS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[getRCSIcon] id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 4

    sget-object v1, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v1}, Lcom/mediatek/contacts/extention/IContactExtention;->isEnabled()Z

    move-result v0

    const-string v1, "ContactExtentionForRCS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[isEnabled] result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    const-string v1, "ContactExtentionForRCS"

    const-string v2, "isEnabled]sContactPlugin is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isVisible(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public layoutExtentionIcon(IIIIILandroid/widget/ImageView;)I
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Landroid/widget/ImageView;

    invoke-virtual {p0, p6}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->isVisible(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    sub-int v1, p3, p2

    iget v2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewHeight:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v0, p2, v1

    iget v1, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewWidth:I

    sub-int v1, p4, v1

    iget v2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewHeight:I

    add-int/2addr v2, v0

    invoke-virtual {p6, v1, v0, p4, v2}, Landroid/view/View;->layout(IIII)V

    iget v1, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewWidth:I

    add-int/2addr v1, p5

    sub-int/2addr p4, v1

    :cond_0
    return p4
.end method

.method public measureExtentionIcon(Landroid/widget/ImageView;)V
    .locals 4
    .param p1    # Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->isVisible(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewWidthAndHeightAreReady:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v1}, Lcom/mediatek/contacts/extention/IContactExtention;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewWidth:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewHeight:I

    :goto_0
    const-string v1, "ContactExtentionForRCS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "measureExtention mRCSIconViewWidth : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | mRCSIconViewHeight : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewWidthAndHeightAreReady:Z

    :cond_0
    return-void

    :cond_1
    iput v2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewWidth:I

    iput v2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewHeight:I

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewWidth:I

    iput v2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSIconViewHeight:I

    goto :goto_0
.end method

.method public onContactDetialOpen(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;

    sget-object v0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v0, p1}, Lcom/mediatek/contacts/extention/IContactExtention;->onContactDetailOpen(Landroid/net/Uri;)V

    :goto_0
    const-string v0, "ContactExtentionForRCS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onContactDetialOpen] contactLookupUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const-string v0, "ContactExtentionForRCS"

    const-string v1, "[onContactDetialOpen] mContactPlutin is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDetailKindView(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mDetailView:Landroid/view/View;

    const-string v0, "ContactExtentionForRCS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[setDetailKindView] mDetailView : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mDetailView:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setExtentionButton(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/app/Activity;

    iput-object p4, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mActivity:Landroid/app/Activity;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mNumber:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x0

    sget-object v8, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v8, :cond_7

    sget-object v8, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v8}, Lcom/mediatek/contacts/extention/IContactExtention;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_7

    sget-object v8, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v8}, Lcom/mediatek/contacts/extention/IContactExtention;->getMimeType()Ljava/lang/String;

    move-result-object v5

    sget-object v8, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v8}, Lcom/mediatek/contacts/extention/IContactExtention;->getContactActions()[Lcom/mediatek/contacts/extention/IContactExtention$Action;

    move-result-object v4

    const/4 v8, 0x0

    aget-object v8, v4, v8

    if-eqz v8, :cond_0

    const/4 v8, 0x1

    aget-object v8, v4, v8

    if-eqz v8, :cond_0

    const/4 v8, 0x0

    aget-object v8, v4, v8

    iget-object v0, v8, Lcom/mediatek/contacts/extention/IContactExtention$Action;->icon:Landroid/graphics/drawable/Drawable;

    const/4 v8, 0x1

    aget-object v8, v4, v8

    iget-object v1, v8, Lcom/mediatek/contacts/extention/IContactExtention$Action;->icon:Landroid/graphics/drawable/Drawable;

    :goto_0
    if-eqz p3, :cond_1

    if-eqz v5, :cond_1

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    :goto_1
    return-void

    :cond_0
    const-string v8, "ContactExtentionForRCS"

    const-string v9, "setExtentionButton action is null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const v8, 0x7f0700c1

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const v8, 0x7f0700c3

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v8, 0x7f0700c4

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0700c5

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    if-eqz v6, :cond_2

    if-eqz v7, :cond_2

    const-string v8, "ContactExtentionForRCS"

    const-string v9, "[setExtentionButton] 1"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    const-string v8, "ContactExtentionForRCS"

    const-string v9, "[setExtentionButton] 2"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v8, 0x8

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v8, 0x8

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    if-eqz v2, :cond_4

    iget v8, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mIMValue:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_4

    iget v8, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mFTValue:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_4

    const-string v8, "ContactExtentionForRCS"

    const-string v9, "[setExtentionButton] 3"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    iget v8, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mIMValue:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    iget v8, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mFTValue:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    const-string v8, "ContactExtentionForRCS"

    const-string v9, "[setExtentionButton] 4"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/view/View;->setClickable(Z)V

    const/4 v8, 0x1

    aget-object v8, v4, v8

    iget-object v8, v8, Lcom/mediatek/contacts/extention/IContactExtention$Action;->intentAction:Landroid/content/Intent;

    invoke-virtual {v3, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v8, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mRCSSecondonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    if-eqz v2, :cond_6

    if-eqz v3, :cond_6

    iget v8, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mIMValue:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    iget v8, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mFTValue:I

    const/4 v9, 0x1

    if-eq v8, v9, :cond_6

    const-string v8, "ContactExtentionForRCS"

    const-string v9, "[setExtentionButton] 5"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/view/View;->setClickable(Z)V

    :cond_6
    if-eqz v2, :cond_7

    if-eqz v3, :cond_7

    iget v8, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mIMValue:I

    const/4 v9, 0x1

    if-eq v8, v9, :cond_7

    iget v8, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mFTValue:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_7

    const-string v8, "ContactExtentionForRCS"

    const-string v9, "[setExtentionButton] 5"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/view/View;->setClickable(Z)V

    :cond_7
    const-string v8, "ContactExtentionForRCS"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[setExtentionButton] mimeType : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " | mimeTypeForRCS : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " | mRCSAction : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public setExtentionIcon(Lcom/mediatek/contacts/extention/ContactExtention;Lcom/android/contacts/list/ContactTileAdapter$ContactEntry;)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1    # Lcom/mediatek/contacts/extention/ContactExtention;
    .param p2    # Lcom/android/contacts/list/ContactTileAdapter$ContactEntry;

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    const/4 v2, 0x0

    sget-object v4, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v4}, Lcom/mediatek/contacts/extention/IContactExtention;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-wide v0, p2, Lcom/android/contacts/list/ContactTileAdapter$ContactEntry;->contact_id:J

    sget-object v3, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v3, v0, v1}, Lcom/mediatek/contacts/extention/IContactExtention;->getContactPresence(J)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const-string v3, "ContactExtentionForRCS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[setExtentionIcon] contactId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v2

    :cond_0
    const-string v4, "ContactExtentionForRCS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setExtentionIcon sContactPlugin : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_0

    :cond_1
    const-string v4, "ContactExtentionForRCS"

    const-string v5, "setExtentionIcon mExtentionIcon is null : "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_0
.end method

.method public setExtentionSubTitle(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    sget-object v1, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    sget-object v1, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v1}, Lcom/mediatek/contacts/extention/IContactExtention;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    invoke-virtual {p3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_0
    const-string v1, "ContactExtentionForRCS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[setExtentionSubTitle] subTitle : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "| data : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_1
    const-string v1, "ContactExtentionForRCS"

    const-string v2, "setExtentionSubTitle return null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setScondBuotton(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 14
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/app/Activity;

    sget-object v11, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    if-eqz v11, :cond_6

    sget-object v11, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v11}, Lcom/mediatek/contacts/extention/IContactExtention;->isEnabled()Z

    move-result v11

    if-eqz v11, :cond_6

    if-eqz p1, :cond_5

    sget-object v11, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v11}, Lcom/mediatek/contacts/extention/IContactExtention;->getMimeType()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mDetailView:Landroid/view/View;

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mDetailView:Landroid/view/View;

    const v12, 0x7f0700c2

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mDetailView:Landroid/view/View;

    const v12, 0x7f0700c1

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iget-object v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mDetailView:Landroid/view/View;

    const v12, 0x7f0700c3

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iget-object v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mDetailView:Landroid/view/View;

    const v12, 0x7f0700b9

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iget-object v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mDetailView:Landroid/view/View;

    const v12, 0x7f0700c5

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iget-object v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mDetailView:Landroid/view/View;

    const v12, 0x7f0700c4

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    sget-object v11, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-interface {v11}, Lcom/mediatek/contacts/extention/IContactExtention;->getContactActions()[Lcom/mediatek/contacts/extention/IContactExtention$Action;

    move-result-object v5

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mName:Ljava/lang/String;

    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mNumber:Ljava/lang/String;

    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mActivity:Landroid/app/Activity;

    if-eqz v9, :cond_4

    if-eqz v7, :cond_4

    if-eqz v3, :cond_4

    if-eqz v6, :cond_4

    if-eqz v8, :cond_4

    if-eqz v10, :cond_4

    if-eqz v5, :cond_3

    const/4 v11, 0x0

    aget-object v11, v5, v11

    iget-object v1, v11, Lcom/mediatek/contacts/extention/IContactExtention$Action;->icon:Landroid/graphics/drawable/Drawable;

    const/4 v11, 0x1

    aget-object v11, v5, v11

    iget-object v2, v11, Lcom/mediatek/contacts/extention/IContactExtention$Action;->icon:Landroid/graphics/drawable/Drawable;

    const/4 v11, 0x1

    aget-object v11, v5, v11

    iget-object v4, v11, Lcom/mediatek/contacts/extention/IContactExtention$Action;->intentAction:Landroid/content/Intent;

    :goto_0
    iget v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mIMValue:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_0

    iget v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mFTValue:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_0

    const-string v11, "ContactExtentionForRCS"

    const-string v12, "setScondBuotton 1"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    const/4 v11, 0x0

    invoke-virtual {v7, v11}, Landroid/view/View;->setVisibility(I)V

    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {v6, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mSetScondBuottononClickListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mIMValue:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_1

    iget v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mFTValue:I

    const/4 v12, 0x1

    if-eq v11, v12, :cond_1

    const-string v11, "ContactExtentionForRCS"

    const-string v12, "setScondBuotton 2"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    const/16 v11, 0x8

    invoke-virtual {v7, v11}, Landroid/view/View;->setVisibility(I)V

    const/16 v11, 0x8

    invoke-virtual {v3, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v6, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Landroid/view/View;->setClickable(Z)V

    :cond_1
    iget v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mIMValue:I

    const/4 v12, 0x1

    if-eq v11, v12, :cond_2

    iget v11, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mFTValue:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    const-string v11, "ContactExtentionForRCS"

    const-string v12, "setScondBuotton 3"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    const/16 v11, 0x8

    invoke-virtual {v7, v11}, Landroid/view/View;->setVisibility(I)V

    const/16 v11, 0x8

    invoke-virtual {v3, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Landroid/view/View;->setClickable(Z)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const-string v11, "ContactExtentionForRCS"

    const-string v12, "[setScondBuotton] is null"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    const-string v11, "ContactExtentionForRCS"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[setScondBuotton] vewVtCallDivider : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " | secondaryActionDivider : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " | btnVtCallAction : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " | secondaryActionButton : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " | secondaryActionViewContainer : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " | vtcallActionViewContainer : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    const-string v11, "ContactExtentionForRCS"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[setScondBuotton] mDetailView or mimetype is not equals mimetype : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " | mDetailView : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->mDetailView:Landroid/view/View;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_6
    const-string v11, "ContactExtentionForRCS"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[setScondBuotton] sContactPlugin is null or not enabled | sContactPlugin : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/mediatek/contacts/extention/rcs/ContactExtentionForRCS;->sContactPlugin:Lcom/mediatek/contacts/extention/IContactExtention;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
