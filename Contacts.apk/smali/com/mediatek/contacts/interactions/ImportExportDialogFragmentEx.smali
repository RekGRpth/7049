.class public Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx;
.super Landroid/app/DialogFragment;
.source "ImportExportDialogFragmentEx.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx$Listener;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "ImportExportDialogFragmentEx"

.field private static mDisplayTextList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static mListener:Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx$Listener;

.field private static mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public static show(Landroid/app/FragmentManager;Ljava/util/List;Ljava/lang/String;Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx$Listener;)V
    .locals 2
    .param p0    # Landroid/app/FragmentManager;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/FragmentManager;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx$Listener;",
            ")V"
        }
    .end annotation

    sput-object p1, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx;->mDisplayTextList:Ljava/util/List;

    sput-object p2, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx;->mTitle:Ljava/lang/String;

    sput-object p3, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx;->mListener:Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx$Listener;

    new-instance v0, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx;

    invoke-direct {v0}, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx;-><init>()V

    const-string v1, "ImportExportDialogFragmentEx"

    invoke-virtual {v0, p0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    new-instance v0, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx$1;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0400b0

    sget-object v4, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx;->mDisplayTextList:Ljava/util/List;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx$1;-><init>(Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx;Landroid/content/Context;ILjava/util/List;Landroid/view/LayoutInflater;)V

    new-instance v6, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx$2;

    invoke-direct {v6, p0, v0}, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx$2;-><init>(Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx;Landroid/widget/ArrayAdapter;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/mediatek/contacts/interactions/ImportExportDialogFragmentEx;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2, v6}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
