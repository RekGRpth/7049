.class public Lcom/mediatek/contacts/list/ContactGroupListFragment;
.super Lcom/android/contacts/group/GroupBrowseListFragment;
.source "ContactGroupListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/list/ContactGroupListFragment$GroupQueryTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/mediatek/contacts/list/ContactGroupListFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/contacts/list/ContactGroupListFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/contacts/group/GroupBrowseListFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/list/ContactGroupListFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/list/ContactGroupListFragment;

    invoke-virtual {p0}, Lcom/android/contacts/group/GroupBrowseListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/list/ContactGroupListFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/contacts/list/ContactGroupListFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/list/ContactGroupListFragment;

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactGroupListFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected configAdapter()Lcom/android/contacts/group/GroupBrowseListAdapter;
    .locals 2

    new-instance v0, Lcom/mediatek/contacts/list/ContactGroupListAdapter;

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactGroupListFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/ContactGroupListAdapter;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected configOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    new-instance v0, Lcom/mediatek/contacts/list/ContactGroupListFragment$1;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/list/ContactGroupListFragment$1;-><init>(Lcom/mediatek/contacts/list/ContactGroupListFragment;)V

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/contacts/group/GroupBrowseListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/android/contacts/group/GroupBrowseListFragment;->onAttach(Landroid/app/Activity;)V

    iput-object p1, p0, Lcom/mediatek/contacts/list/ContactGroupListFragment;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onDetach()V
    .locals 1

    invoke-super {p0}, Lcom/android/contacts/group/GroupBrowseListFragment;->onDetach()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/list/ContactGroupListFragment;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onOkClick()V
    .locals 10

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/android/contacts/group/GroupBrowseListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v9, v8}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->finish()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/contacts/group/GroupBrowseListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v7

    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/contacts/group/GroupBrowseListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/contacts/group/GroupBrowseListAdapter;

    invoke-virtual {p0}, Lcom/android/contacts/group/GroupBrowseListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/AdapterView;->getCount()I

    move-result v5

    const/4 v2, 0x0

    const/4 v6, 0x0

    move v3, v2

    :goto_0
    if-ge v6, v5, :cond_1

    invoke-virtual {p0}, Lcom/android/contacts/group/GroupBrowseListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v0, v6}, Lcom/android/contacts/group/GroupBrowseListAdapter;->getItem(I)Lcom/android/contacts/group/GroupListItem;

    move-result-object v4

    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v4}, Lcom/android/contacts/group/GroupListItem;->getGroupId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v3

    :goto_1
    add-int/lit8 v6, v6, 0x1

    move v3, v2

    goto :goto_0

    :cond_1
    new-instance v7, Lcom/mediatek/contacts/list/ContactGroupListFragment$GroupQueryTask;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v7, p0, v8}, Lcom/mediatek/contacts/list/ContactGroupListFragment$GroupQueryTask;-><init>(Lcom/mediatek/contacts/list/ContactGroupListFragment;Landroid/app/Activity;)V

    const/4 v8, 0x1

    new-array v8, v8, [[Ljava/lang/String;

    aput-object v1, v8, v9

    invoke-virtual {v7, v8}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    :cond_2
    move v2, v3

    goto :goto_1
.end method
