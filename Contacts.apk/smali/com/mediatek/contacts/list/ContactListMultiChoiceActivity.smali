.class public Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;
.super Lcom/android/contacts/ContactsActivity;
.source "ContactListMultiChoiceActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/SearchView$OnCloseListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$3;,
        Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;
    }
.end annotation


# static fields
.field public static final CONTACTGROUPLISTACTIVITY_RESULT_CODE:I = 0x1

.field private static final DEFAULT_DIRECTORY_RESULT_LIMIT:I = 0x14

.field private static final FOCUS_DELAY:I = 0xc8

.field private static final KEY_ACTION_CODE:Ljava/lang/String; = "actionCode"

.field public static final RESTRICT_LIST:Ljava/lang/String; = "restrictlist"

.field private static final SUBACTIVITY_ADD_TO_EXISTING_CONTACT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ContactsMultiChoiceActivity"


# instance fields
.field private mActionCode:I

.field private mIntentResolverEx:Lcom/mediatek/contacts/list/ContactsIntentResolverEx;

.field protected mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/contacts/list/ContactEntryListFragment",
            "<*>;"
        }
    .end annotation
.end field

.field private mRequest:Lcom/android/contacts/list/ContactsRequest;

.field private mSearchView:Landroid/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/contacts/ContactsActivity;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    new-instance v0, Lcom/mediatek/contacts/list/ContactsIntentResolverEx;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/list/ContactsIntentResolverEx;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIntentResolverEx:Lcom/mediatek/contacts/list/ContactsIntentResolverEx;

    return-void
.end method

.method private showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V
    .locals 12
    .param p1    # Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    const/4 v11, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    sget-object v7, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$3;->$SwitchMap$com$mediatek$contacts$list$ContactListMultiChoiceActivity$SelectionMode:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {v0}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f040043

    invoke-virtual {v7, v8, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v7, 0x7f0700e5

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/SearchView;

    iput-object v7, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v7, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v7, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v7, v10}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    iget-object v7, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    const v8, 0x7f0c028b

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v7, v9}, Landroid/widget/SearchView;->setIconified(Z)V

    iget-object v7, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v7, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    iget-object v7, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v7, p0}, Landroid/widget/SearchView;->setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V

    iget-object v7, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v7, p0}, Landroid/widget/SearchView;->setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v7, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    const/4 v8, 0x6

    invoke-virtual {v7, v8}, Landroid/widget/SearchView;->setImeOptions(I)V

    new-instance v7, Landroid/app/ActionBar$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, -0x2

    invoke-direct {v7, v8, v9}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v6, v7}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    invoke-virtual {v0, v10}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    invoke-virtual {v0, v10}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    invoke-virtual {v0, v10}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto :goto_0

    :pswitch_2
    const-string v7, "layout_inflater"

    invoke-virtual {p0, v7}, Lcom/android/contacts/ContactsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    const v7, 0x7f04008e

    invoke-virtual {v4, v7, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v7, 0x7f070094

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    new-instance v7, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$1;

    invoke-direct {v7, p0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$1;-><init>(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;)V

    invoke-virtual {v2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v7, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v7}, Lcom/android/contacts/list/ContactsRequest;->getActionCode()I

    move-result v7

    const/16 v8, 0x5c

    if-ne v7, v8, :cond_0

    const v7, 0x7f070154

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v7, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$2;

    invoke-direct {v7, p0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$2;-><init>(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v7, 0x7f070153

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v7, 0x10

    const/16 v8, 0x1a

    invoke-virtual {v0, v7, v8}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    iput-object v11, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private showInputMethod(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/android/contacts/ContactsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ContactsMultiChoiceActivity"

    const-string v2, "Failed to show soft input method."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public configureListFragment()V
    .locals 4

    iget v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v2}, Lcom/android/contacts/list/ContactsRequest;->getActionCode()I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v1}, Lcom/android/contacts/list/ContactsRequest;->getActionCode()I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    const-string v1, "ContactsMultiChoiceActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "configureListFragment action code is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    sparse-switch v1, :sswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid action code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    new-instance v1, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    :goto_1
    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v2}, Lcom/android/contacts/list/ContactsRequest;->isLegacyCompatibilityMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/contacts/list/ContactEntryListFragment;->setLegacyCompatibilityMode(Z)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v2}, Lcom/android/contacts/list/ContactsRequest;->getQueryString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/contacts/list/ContactEntryListFragment;->setQueryString(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lcom/android/contacts/list/ContactEntryListFragment;->setDirectoryResultLimit(I)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/contacts/list/ContactEntryListFragment;->setVisibleScrollbarEnabled(Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0700e6

    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_0

    :sswitch_1
    new-instance v1, Lcom/mediatek/contacts/list/ContactsVCardPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/ContactsVCardPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    goto :goto_1

    :sswitch_2
    new-instance v1, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "intent"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_1

    :sswitch_3
    new-instance v1, Lcom/mediatek/contacts/list/MultiEmailsPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiEmailsPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    goto :goto_1

    :sswitch_4
    new-instance v1, Lcom/mediatek/contacts/list/MultiPhoneNumbersPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiPhoneNumbersPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    goto :goto_1

    :sswitch_5
    new-instance v1, Lcom/mediatek/contacts/list/MultiDataItemsPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiDataItemsPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "intent"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto/16 :goto_1

    :sswitch_6
    new-instance v1, Lcom/mediatek/contacts/list/ContactsMultiDeletionFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/ContactsMultiDeletionFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    goto/16 :goto_1

    :sswitch_7
    new-instance v1, Lcom/mediatek/contacts/list/ContactsGroupMultiPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/ContactsGroupMultiPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "intent"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto/16 :goto_1

    :sswitch_8
    new-instance v1, Lcom/mediatek/contacts/list/MultiPhoneAndEmailsPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiPhoneAndEmailsPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    goto/16 :goto_1

    :sswitch_9
    new-instance v1, Lcom/mediatek/contacts/list/MultiContactsShareFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiContactsShareFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x3d -> :sswitch_0
        0x3e -> :sswitch_6
        0x3f -> :sswitch_7
        0x40 -> :sswitch_9
        0x5b -> :sswitch_4
        0x5c -> :sswitch_8
        0x5d -> :sswitch_5
        0x6a -> :sswitch_3
        0x100003d -> :sswitch_1
        0x200003d -> :sswitch_2
    .end sparse-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    if-nez p1, :cond_1

    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    if-eqz p3, :cond_0

    invoke-virtual {p0, p3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    const v2, 0x1b208

    if-ne p2, v2, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_2
    const/4 v2, 0x1

    if-ne p2, v2, :cond_3

    const-string v2, "checkedids"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    instance-of v2, v2, Lcom/mediatek/contacts/list/MultiPhoneAndEmailsPickerFragment;

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    check-cast v0, Lcom/mediatek/contacts/list/MultiPhoneAndEmailsPickerFragment;

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;->markItemsAsSelectedForCheckedGroups([J)V

    :cond_3
    return-void
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/app/Fragment;

    instance-of v0, p1, Lcom/android/contacts/list/ContactEntryListFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/contacts/list/ContactEntryListFragment;

    iput-object p1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/view/View;->isFocused()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    :cond_0
    sget-object v1, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;->ListMode:Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    instance-of v1, v1, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    check-cast v0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;

    invoke-virtual {v0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateSelectedItemsView()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    instance-of v1, v1, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    check-cast v0, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;

    invoke-virtual {v0}, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;->updateSelectedItemsView()V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f070153

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;->SearchMode:Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V

    :cond_0
    return-void
.end method

.method public onClose()Z
    .locals 4

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    if-nez v2, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    :cond_2
    sget-object v2, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;->ListMode:Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    instance-of v2, v2, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    check-cast v0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;

    invoke-virtual {v0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateSelectedItemsView()V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    instance-of v2, v2, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    check-cast v0, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;

    invoke-virtual {v0}, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;->updateSelectedItemsView()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    const-string v0, "ContactsMultiChoiceActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onConfigurationChanged]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    invoke-virtual {v1}, Lcom/android/contacts/list/ContactEntryListFragment;->getContextMenuAdapter()Lcom/android/contacts/widget/ContextMenuAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/android/contacts/widget/ContextMenuAdapter;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/16 v5, 0x8

    invoke-super {p0, p1}, Lcom/android/contacts/ContactsActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v3, "actionCode"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    :cond_0
    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIntentResolverEx:Lcom/mediatek/contacts/list/ContactsIntentResolverEx;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mediatek/contacts/list/ContactsIntentResolverEx;->resolveIntent(Landroid/content/Intent;)Lcom/android/contacts/list/ContactsRequest;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v3}, Lcom/android/contacts/list/ContactsRequest;->isValid()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "ContactsMultiChoiceActivity"

    const-string v4, "Request is invalid!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v3}, Lcom/android/contacts/list/ContactsRequest;->getRedirectIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    const v3, 0x7f040037

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->configureListFragment()V

    const v3, 0x7f0700e5

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SearchView;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    const v3, 0x7f0700e7

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    sget-object v3, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;->ListMode:Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    invoke-direct {p0, v3}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v3, 0x7f10000c

    invoke-virtual {v1, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v3, 0x7f0701d2

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v3}, Lcom/android/contacts/list/ContactsRequest;->getActionCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string v3, "ContactsMultiChoiceActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Defualt handle for this request: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 v3, 0x1

    return v3

    :sswitch_0
    const v3, 0x7f0200ab

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const v3, 0x1040001

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    :sswitch_1
    const v3, 0x7f0200aa

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const v3, 0x7f0c0114

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    :sswitch_2
    const v3, 0x7f0200b6

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const v3, 0x7f0c01e8

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    :sswitch_3
    const v3, 0x7f0200af

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const v3, 0x7f0c00cd

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3e -> :sswitch_1
        0x3f -> :sswitch_3
        0x40 -> :sswitch_2
        0x200003d -> :sswitch_0
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/android/contacts/ContactsActivity;->onDestroy()V

    const-string v0, "ContactsMultiChoiceActivity"

    const-string v1, "[onDestroy]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0700e5

    if-ne v0, v1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->findFocus()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showInputMethod(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/MenuItem;

    const/4 v2, 0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    instance-of v3, v3, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    check-cast v0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v2

    :cond_1
    :goto_1
    return v2

    :sswitch_0
    invoke-virtual {v0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->onSelectAll()V

    goto :goto_1

    :sswitch_1
    invoke-virtual {v0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->onClearSelect()V

    goto :goto_1

    :sswitch_2
    invoke-virtual {v0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->onOptionAction()V

    instance-of v3, v0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    if-eqz v3, :cond_1

    const-string v3, "ContactsMultiChoiceActivity"

    const-string v4, "Send result for copy action"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v3, 0x1b208

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setResult(I)V

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    instance-of v3, v3, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    check-cast v0, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;

    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_3
    invoke-virtual {v0}, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;->onSelectAll()V

    goto :goto_1

    :sswitch_4
    invoke-virtual {v0}, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;->onClearSelect()V

    goto :goto_1

    :sswitch_5
    invoke-virtual {v0}, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;->onOptionAction()V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x7f0701b3 -> :sswitch_0
        0x7f0701d1 -> :sswitch_1
        0x7f0701d2 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0701b3 -> :sswitch_3
        0x7f0701d1 -> :sswitch_4
        0x7f0701d2 -> :sswitch_5
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->onBackPressed()V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    instance-of v1, v1, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    check-cast v0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;

    invoke-virtual {v0, p1}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->startSearch(Ljava/lang/String;)V

    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    :cond_1
    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    instance-of v1, v1, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/android/contacts/list/ContactEntryListFragment;

    check-cast v0, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;

    invoke-virtual {v0, p1}, Lcom/mediatek/contacts/list/DataKindPickerBaseFragment;->startSearch(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/contacts/activities/TransactionSafeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "actionCode"

    iget v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public returnPickerResult(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public returnPickerResult(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->returnPickerResult(Landroid/content/Intent;)V

    return-void
.end method

.method public startActivityAndForwardResult(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const/high16 v1, 0x2000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method
