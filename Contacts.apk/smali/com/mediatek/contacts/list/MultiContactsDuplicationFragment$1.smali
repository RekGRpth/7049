.class Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$1;
.super Ljava/lang/Object;
.source "MultiContactsDuplicationFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$1;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const-string v3, "CopyMultiContacts"

    const-string v4, "serviceComplete run"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$1;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v3}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$700(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)Lcom/mediatek/CellConnService/CellConnMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/CellConnService/CellConnMgr;->getResult()I

    move-result v0

    const-string v3, "CopyMultiContacts"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "serviceComplete result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Lcom/mediatek/CellConnService/CellConnMgr;->resultToString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$1;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v3}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$700(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)Lcom/mediatek/CellConnService/CellConnMgr;

    const/4 v3, 0x2

    if-ne v3, v0, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$1;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v3}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$808(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)I

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$1;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v3}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$100(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)Landroid/accounts/Account;

    move-result-object v3

    check-cast v3, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v3}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getSlotId()I

    move-result v2

    sget-object v3, Lcom/android/contacts/ContactsUtils;->isServiceRunning:[Z

    aget-boolean v1, v3, v2

    const-string v3, "CopyMultiContacts"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AbstractService state is running? "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_1

    const-string v3, "CopyMultiContacts"

    const-string v4, "service is running, we would wait the service finished."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$1;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v3}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$900(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)V

    goto :goto_0

    :cond_1
    const-string v3, "CopyMultiContacts"

    const-string v4, "service is finished."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$1;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v3}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$1000(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$SendRequestHandler;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$1;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v4}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$1000(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$SendRequestHandler;

    move-result-object v4

    const/16 v5, 0x64

    iget-object v6, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$1;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v6}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$500(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
