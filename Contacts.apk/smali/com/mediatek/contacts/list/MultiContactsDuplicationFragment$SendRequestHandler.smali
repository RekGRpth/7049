.class Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$SendRequestHandler;
.super Landroid/os/Handler;
.source "MultiContactsDuplicationFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SendRequestHandler"
.end annotation


# static fields
.field public static final MSG_END:I = 0x12c

.field public static final MSG_PBH_LOAD_FINISH:I = 0xc8

.field public static final MSG_REQUEST:I = 0x64


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$SendRequestHandler;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/16 v3, 0x64

    const/16 v2, 0x12c

    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$SendRequestHandler;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v0}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$200(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$CopyRequestConnection;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$CopyRequestConnection;->sendCopyRequest(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$SendRequestHandler;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v0}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$310(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p1, Landroid/os/Message;->what:I

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$SendRequestHandler;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-virtual {v0}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->destroyMyself()V

    goto :goto_0

    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$SendRequestHandler;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v0}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$400(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)V

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment$SendRequestHandler;->this$0:Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-static {v0}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;->access$500(Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_4
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
