.class public Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;
.super Lcom/android/contacts/list/ContactEntryListFragment;
.source "MultiContactsPickerBaseFragment.java"

# interfaces
.implements Lcom/mediatek/contacts/list/ContactListMultiChoiceListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment$1;,
        Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment$FilterHeaderClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/contacts/list/ContactEntryListFragment",
        "<",
        "Lcom/android/contacts/list/ContactListAdapter;",
        ">;",
        "Lcom/mediatek/contacts/list/ContactListMultiChoiceListener;"
    }
.end annotation


# static fields
.field private static final ALLOWED_ITEMS_MAX:I = 0xdac

.field public static final FRAGMENT_ARGS:Ljava/lang/String; = "intent"

.field private static final KEY_CHECKEDIDS:Ljava/lang/String; = "checkedids"

.field private static final KEY_CHECKEDSTATES:Ljava/lang/String; = "checkedstates"

.field private static final KEY_FILTER:Ljava/lang/String; = "filter"

.field private static final REQUEST_CODE_ACCOUNT_FILTER:I = 0x1

.field protected static final RESULTINTENTEXTRANAME:Ljava/lang/String; = "com.mediatek.contacts.list.pickcontactsresult"

.field private static final TAG:Ljava/lang/String; = "MultiContactsPickerBaseFragment"


# instance fields
.field private mAccountFilterHeader:Landroid/view/View;

.field private mCheckedItemsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mEmptyView:Landroid/widget/TextView;

.field private mFilter:Lcom/android/contacts/list/ContactListFilter;

.field private mFilterHeaderClickListener:Landroid/view/View$OnClickListener;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mSearchString:Ljava/lang/String;

.field private mShowFilterHeader:Z

.field private mSlectedItemsFormater:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/contacts/list/ContactEntryListFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mShowFilterHeader:Z

    new-instance v0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment$FilterHeaderClickListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment$FilterHeaderClickListener;-><init>(Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment$1;)V

    iput-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilterHeaderClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;)Lcom/android/contacts/list/ContactListFilter;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    return-object v0
.end method

.method private checkHeaderViewVisibility()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateFilterHeaderView()V

    return-void
.end method

.method private restoreFilter()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/android/contacts/list/ContactListFilter;->restoreDefaultPreferences(Landroid/content/SharedPreferences;)Lcom/android/contacts/list/ContactListFilter;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    return-void
.end method

.method private saveFilter()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    invoke-static {v0, v1}, Lcom/android/contacts/list/ContactListFilter;->storeToPreferences(Landroid/content/SharedPreferences;Lcom/android/contacts/list/ContactListFilter;)V

    return-void
.end method

.method private setFilter(Lcom/android/contacts/list/ContactListFilter;)V
    .locals 3
    .param p1    # Lcom/android/contacts/list/ContactListFilter;

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    if-nez v0, :cond_1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    invoke-virtual {v0, p1}, Lcom/android/contacts/list/ContactListFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    const-string v0, "MultiContactsPickerBaseFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "New filter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    invoke-direct {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->saveFilter()V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->reloadData()V

    goto :goto_0
.end method

.method private updateFilterHeaderView()V
    .locals 5

    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mShowFilterHeader:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mAccountFilterHeader:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mAccountFilterHeader:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mAccountFilterHeader:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->isSearchMode()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mAccountFilterHeader:Landroid/view/View;

    const/4 v4, 0x1

    invoke-static {v3, v0, v4}, Lcom/android/contacts/util/AccountFilterUtil;->updateAccountFilterTitleForPeople(Landroid/view/View;Lcom/android/contacts/list/ContactListFilter;Z)Z

    move-result v1

    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mAccountFilterHeader:Landroid/view/View;

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    :cond_2
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mAccountFilterHeader:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateListCheckBoxeState(Z)V
    .locals 8
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    invoke-interface {v5}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    const-wide/16 v1, -0x1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v5

    const/16 v6, 0xdac

    if-lt v5, v6, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v6, 0x7f0c00b8

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateSelectedItemsView(I)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5, v4, p1}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    invoke-virtual {v0, v4}, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;->getContactID(I)I

    move-result v5

    int-to-long v1, v5

    iget-object v5, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5, v4, p1}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    invoke-virtual {v0, v4}, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;->getContactID(I)I

    move-result v5

    int-to-long v1, v5

    iget-object v5, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private updateSelectedItemsView(I)V
    .locals 5
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v1

    check-cast v1, Lcom/android/contacts/list/ContactListAdapter;

    invoke-virtual {v1}, Lcom/android/contacts/list/ContactEntryListAdapter;->isSearchMode()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f070095

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_1

    const-string v1, "MultiContactsPickerBaseFragment"

    const-string v2, "Load view resource error!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mSlectedItemsFormater:Ljava/lang/String;

    if-nez v1, :cond_2

    const-string v1, "MultiContactsPickerBaseFragment"

    const-string v2, "Load string resource error!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mSlectedItemsFormater:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected configureAdapter()V
    .locals 6

    const/4 v5, 0x1

    invoke-super {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->configureAdapter()V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/android/contacts/list/ContactEntryListAdapter;->setEmptyListEnabled(Z)V

    invoke-virtual {v0, v5}, Lcom/android/contacts/widget/IndexerListAdapter;->setSectionHeaderDisplayEnabled(Z)V

    invoke-virtual {v0, v5}, Lcom/android/contacts/list/ContactEntryListAdapter;->setDisplayPhotos(Z)V

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/android/contacts/list/ContactEntryListAdapter;->setQuickContactEnabled(Z)V

    invoke-virtual {v0, v5}, Lcom/android/contacts/list/ContactEntryListAdapter;->setEmptyListEnabled(Z)V

    invoke-super {p0, v5}, Lcom/android/contacts/list/ContactEntryListFragment;->setPhotoLoaderEnabled(Z)V

    invoke-super {p0, v5}, Lcom/android/contacts/list/ContactEntryListFragment;->setSectionHeaderDisplayEnabled(Z)V

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    invoke-virtual {v0, v4}, Lcom/android/contacts/list/ContactEntryListAdapter;->setFilter(Lcom/android/contacts/list/ContactListFilter;)V

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mSearchString:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/android/contacts/list/ContactEntryListAdapter;->setQueryString(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mAccountFilterHeader:Landroid/view/View;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mAccountFilterHeader:Landroid/view/View;

    const v5, 0x7f070030

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    if-eqz v1, :cond_1

    const v4, 0x7f0c0284

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    return-void
.end method

.method protected bridge synthetic createListAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->createListAdapter()Lcom/android/contacts/list/ContactListAdapter;

    move-result-object v0

    return-object v0
.end method

.method protected createListAdapter()Lcom/android/contacts/list/ContactListAdapter;
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;-><init>(Landroid/content/Context;Landroid/widget/ListView;)V

    const/4 v1, -0x2

    invoke-static {v1}, Lcom/android/contacts/list/ContactListFilter;->createFilterWithType(I)Lcom/android/contacts/list/ContactListFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/contacts/list/ContactEntryListAdapter;->setFilter(Lcom/android/contacts/list/ContactListFilter;)V

    invoke-virtual {v0, v3}, Lcom/android/contacts/widget/IndexerListAdapter;->setSectionHeaderDisplayEnabled(Z)V

    invoke-virtual {v0, v3}, Lcom/android/contacts/list/ContactEntryListAdapter;->setDisplayPhotos(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/contacts/list/ContactEntryListAdapter;->setQuickContactEnabled(Z)V

    invoke-virtual {v0, v3}, Lcom/android/contacts/list/ContactEntryListAdapter;->setEmptyListEnabled(Z)V

    return-object v0
.end method

.method protected inflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;

    const v0, 0x7f04008d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public isAccountFilterEnable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0c005a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mSlectedItemsFormater:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateSelectedItemsView()V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/contacts/list/ContactListFilterController;->getInstance(Landroid/content/Context;)Lcom/android/contacts/list/ContactListFilterController;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/android/contacts/util/AccountFilterUtil;->handleAccountFilterResult(Lcom/android/contacts/list/ContactListFilterController;ILandroid/content/Intent;)V

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/contacts/list/ContactListFilterController;->getInstance(Landroid/content/Context;)Lcom/android/contacts/list/ContactListFilterController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/contacts/list/ContactListFilterController;->getFilter()Lcom/android/contacts/list/ContactListFilter;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->setFilter(Lcom/android/contacts/list/ContactListFilter;)V

    invoke-direct {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateFilterHeaderView()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "MultiContactsPickerBaseFragment"

    const-string v1, "getActivity() returns null during Fragment#onActivityResult()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/android/contacts/list/ContactEntryListFragment;->onAttach(Landroid/app/Activity;)V

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->isAccountFilterEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->restoreFilter()V

    :cond_0
    return-void
.end method

.method public onClearSelect()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateListCheckBoxeState(Z)V

    return-void
.end method

.method protected onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2}, Lcom/android/contacts/list/ContactEntryListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07002f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mAccountFilterHeader:Landroid/view/View;

    invoke-virtual {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->isAccountFilterEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mAccountFilterHeader:Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilterHeaderClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->checkHeaderViewVisibility()V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f070152

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mEmptyView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mEmptyView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mEmptyView:Landroid/widget/TextView;

    const v1, 0x7f0c0140

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mAccountFilterHeader:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0
.end method

.method protected onItemClick(IJ)V
    .locals 2
    .param p1    # I
    .param p2    # J

    const-string v0, "MultiContactsPickerBaseFragment"

    const-string v1, "onItemClick"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v3, 0x0

    const-string v1, "MultiContactsPickerBaseFragment"

    const-string v2, "onItemClick with adapterView"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v1

    const/16 v2, 0xdac

    if-le v1, v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0c00b8

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, p3, v3}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    :goto_0
    return-void

    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/android/contacts/list/ContactEntryListFragment;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, p3, v0}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateSelectedItemsView(I)V

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 8
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mEmptyView:Landroid/widget/TextView;

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v4

    check-cast v4, Lcom/android/contacts/list/ContactListAdapter;

    invoke-virtual {v4}, Lcom/android/contacts/list/ContactEntryListAdapter;->isSearchMode()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mEmptyView:Landroid/widget/TextView;

    const v5, 0x7f0c0156

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    if-eqz p2, :cond_5

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_5

    :cond_1
    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mEmptyView:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/AbsListView;->clearChoices()V

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x0

    if-eqz p2, :cond_7

    const/4 v4, -0x1

    invoke-interface {p2, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_2
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v2, -0x1

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    int-to-long v5, v2

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    int-to-long v5, v2

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v3, v0}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    if-eqz v0, :cond_3

    add-int/lit8 v1, v1, 0x1

    :cond_3
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mEmptyView:Landroid/widget/TextView;

    const v5, 0x7f0c0140

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mEmptyView:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mEmptyView:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v3, v7}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    int-to-long v5, v2

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_7
    invoke-direct {p0, v1}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateSelectedItemsView(I)V

    invoke-direct {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateFilterHeaderView()V

    invoke-super {p0, p1, p2}, Lcom/android/contacts/list/ContactEntryListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onOptionAction()V
    .locals 12

    const/4 v11, 0x0

    const/4 v10, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v0, v10, v11}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    if-nez v7, :cond_1

    invoke-virtual {v0, v10, v11}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    new-array v4, v8, [J

    if-nez v4, :cond_2

    invoke-virtual {v0, v10, v11}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v1

    check-cast v1, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/AdapterView;->getCount()I

    move-result v5

    const/4 v6, 0x0

    move v3, v2

    :goto_1
    if-ge v6, v5, :cond_5

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    move-result v9

    if-eqz v9, :cond_3

    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v1, v6}, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;->getContactID(I)I

    move-result v9

    int-to-long v9, v9

    aput-wide v9, v4, v3

    if-le v2, v8, :cond_4

    :goto_2
    const-string v9, "com.mediatek.contacts.list.pickcontactsresult"

    invoke-virtual {v7, v9, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    const/4 v9, -0x1

    invoke-virtual {v0, v9, v7}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_3
    move v2, v3

    :cond_4
    add-int/lit8 v6, v6, 0x1

    move v3, v2

    goto :goto_1

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/contacts/list/ContactEntryListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v10, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v1

    new-array v0, v1, [J

    const/4 v6, 0x0

    iget-object v10, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    add-int/lit8 v7, v6, 0x1

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    aput-wide v10, v0, v6

    move v6, v7

    goto :goto_0

    :cond_0
    const-string v10, "checkedids"

    invoke-virtual {p1, v10, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    new-array v2, v1, [Z

    iget-object v10, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v9

    const/4 v6, 0x0

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    add-int/lit8 v7, v6, 0x1

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    aput-boolean v10, v2, v6

    move v6, v7

    goto :goto_1

    :cond_1
    const-string v10, "checkedstates"

    invoke-virtual {p1, v10, v2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    const-string v10, "filter"

    iget-object v11, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    invoke-virtual {p1, v10, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public onSelectAll()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateListCheckBoxeState(Z)V

    return-void
.end method

.method public restoreSavedState(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/contacts/list/ContactEntryListFragment;->restoreSavedState(Landroid/os/Bundle;)V

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    const-string v4, "filter"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/android/contacts/list/ContactListFilter;

    iput-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    const-string v4, "checkedids"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    const-string v4, "checkedstates"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    if-nez v4, :cond_2

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    :cond_2
    array-length v4, v1

    array-length v5, v3

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    array-length v0, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    iget-object v4, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    aget-wide v5, v1, v2

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aget-boolean v6, v3, v2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected setDataSetChangedNotifyEnable(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;->setDataSetChangedNotifyEnable(Z)V

    :cond_0
    return-void
.end method

.method protected setListFilter(Lcom/android/contacts/list/ContactListFilter;)V
    .locals 2
    .param p1    # Lcom/android/contacts/list/ContactListFilter;

    invoke-virtual {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->isAccountFilterEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The #setListFilter could not be called if #isAccountFilterEnable is true"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/contacts/list/ContactListAdapter;

    iget-object v1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mFilter:Lcom/android/contacts/list/ContactListFilter;

    invoke-virtual {v0, v1}, Lcom/android/contacts/list/ContactEntryListAdapter;->setFilter(Lcom/android/contacts/list/ContactListFilter;)V

    invoke-direct {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateFilterHeaderView()V

    return-void
.end method

.method public showFilterHeader(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mShowFilterHeader:Z

    return-void
.end method

.method public startSearch(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;

    if-nez p1, :cond_2

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mSearchString:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/android/contacts/list/ContactEntryListAdapter;->setQueryString(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/contacts/list/ContactEntryListAdapter;->setSearchMode(Z)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->reloadData()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mSearchString:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-object p1, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mSearchString:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/android/contacts/list/ContactEntryListAdapter;->setQueryString(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/contacts/list/ContactEntryListAdapter;->setSearchMode(Z)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->reloadData()V

    goto :goto_0
.end method

.method public updateSelectedItemsView()V
    .locals 8

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;

    const/4 v1, 0x0

    const-wide/16 v2, -0x1

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v6

    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_1

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v5}, Lcom/mediatek/contacts/list/MultiContactsBasePickerAdapter;->getItemId(I)J

    move-result-wide v2

    iget-object v6, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->mCheckedItemsMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_0

    add-int/lit8 v1, v1, 0x1

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;->updateSelectedItemsView(I)V

    return-void
.end method
