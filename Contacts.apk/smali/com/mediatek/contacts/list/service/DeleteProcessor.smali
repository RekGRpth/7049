.class public Lcom/mediatek/contacts/list/service/DeleteProcessor;
.super Lcom/android/contacts/vcard/ProcessorBase;
.source "DeleteProcessor.java"


# static fields
.field private static final DEBUG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "ContactsMultiDeletion"

.field private static final MAX_COUNT:I = 0x60f

.field private static final MAX_COUNT_IN_ONE_BATCH:I = 0x32

.field private static final MAX_OP_COUNT_IN_ONE_BATCH:I = 0x64


# instance fields
.field private volatile mCanceled:Z

.field private volatile mDone:Z

.field private volatile mIsRunning:Z

.field private final mJobId:I

.field private final mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

.field private mModemSwitchListener:Landroid/content/BroadcastReceiver;

.field private final mRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/list/service/MultiChoiceRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final mResolver:Landroid/content/ContentResolver;

.field private mReveiced3GSwitch:Ljava/lang/Boolean;

.field private final mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/list/service/MultiChoiceService;Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;Ljava/util/List;I)V
    .locals 3
    .param p1    # Lcom/mediatek/contacts/list/service/MultiChoiceService;
    .param p2    # Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/contacts/list/service/MultiChoiceService;",
            "Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/list/service/MultiChoiceRequest;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/contacts/vcard/ProcessorBase;-><init>()V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mReveiced3GSwitch:Ljava/lang/Boolean;

    new-instance v1, Lcom/mediatek/contacts/list/service/DeleteProcessor$1;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/list/service/DeleteProcessor$1;-><init>(Lcom/mediatek/contacts/list/service/DeleteProcessor;)V

    iput-object v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mModemSwitchListener:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mResolver:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    iput-object p3, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mRequests:Ljava/util/List;

    iput p4, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mJobId:I

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const v1, 0x20000006

    const-string v2, "ContactsMultiDeletion"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/contacts/list/service/DeleteProcessor;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/list/service/DeleteProcessor;
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mReveiced3GSwitch:Ljava/lang/Boolean;

    return-object p1
.end method

.method private actualBatchDelete(Ljava/util/ArrayList;)I
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    const/4 v2, 0x0

    const-string v7, "ContactsMultiDeletion"

    const-string v8, "actualBatchDelete"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v4, v7, [Ljava/lang/String;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const-string v7, "?"

    invoke-static {v4, v7}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v7, "_id IN ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-static {v8, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mResolver:Landroid/content/ContentResolver;

    sget-object v7, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    const-string v9, "batch"

    const-string v10, "true"

    invoke-virtual {v7, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-array v7, v2, [Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    invoke-virtual {v8, v9, v10, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const-string v7, "ContactsMultiDeletion"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "actualBatchDelete "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Contacts"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private static addCallerIsSyncAdapterParameter(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p0    # Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private isReadyForDelete(I)Z
    .locals 6
    .param p1    # I

    const/4 v2, 0x0

    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    :try_start_0
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/ITelephony;->hasIccCardGemini(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, p1}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, p1}, Lcom/android/internal/telephony/ITelephony;->isFDNEnabledGemini(I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p1}, Lcom/mediatek/contacts/SubContactsUtils;->checkPhbReady(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x5

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "ContactsMultiDeletion"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isReadyForDelete: RemoteException -> "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private needBreakProcess(I)Z
    .locals 6
    .param p1    # I

    const/4 v2, 0x0

    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    :try_start_0
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "ContactsMultiDeletion"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "needBreakProcess: RemoteException "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerReceiver()V
    .locals 0

    return-void
.end method

.method private runInternal()V
    .locals 27

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/contacts/list/service/DeleteProcessor;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "ContactsMultiDeletion"

    const-string v3, "Canceled before actually handling"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/16 v25, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    const/4 v11, 0x0

    const/4 v5, 0x0

    const/16 v18, 0x64

    const/16 v2, 0x60f

    if-le v6, v2, :cond_1

    const/16 v18, 0x32

    const-string v2, "ContactsMultiDeletion"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "iBatchDel = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v23

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_2
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mCanceled:Z

    if-eqz v2, :cond_5

    const-string v2, "ContactsMultiDeletion"

    const-string v3, "runInternal run: mCanceled = true, break looper"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    const-string v2, "ContactsMultiDeletion"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "iBatchDel : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | endtime : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v7, v15, v23

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/mediatek/contacts/list/service/DeleteProcessor;->actualBatchDelete(Ljava/util/ArrayList;)I

    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mCanceled:Z

    if-eqz v2, :cond_a

    const-string v2, "ContactsMultiDeletion"

    const-string v3, "runInternal run: mCanceled = true, return"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mJobId:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget v9, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mJobId:I

    sub-int v12, v6, v11

    move v10, v6

    invoke-virtual/range {v7 .. v12}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onCanceled(IIIII)V

    goto/16 :goto_0

    :cond_5
    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mJobId:I

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mContactName:Ljava/lang/String;

    invoke-virtual/range {v2 .. v7}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onProcessed(IIIILjava/lang/String;)V

    const-string v2, "ContactsMultiDeletion"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "runInternal run: request.mIndicator = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    iget-wide v7, v0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mIndicator:J

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, v21

    iget-wide v2, v0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mIndicator:J

    const-wide/16 v7, -0x1

    cmp-long v2, v2, v7

    if-lez v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v21

    iget-wide v3, v0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mIndicator:J

    invoke-static {v2, v3, v4}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mReveiced3GSwitch:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/DeleteProcessor;->isReadyForDelete(I)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_6
    const-string v2, "ContactsMultiDeletion"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "runInternal run: isReadyForDelete("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") = false"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v25, 0x0

    goto/16 :goto_1

    :cond_7
    invoke-static/range {v22 .. v22}, Lcom/mediatek/contacts/SubContactsUtils;->getUri(I)Landroid/net/Uri;

    move-result-object v14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    iget v3, v0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mSimIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v2, v14, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_8

    const-string v2, "ContactsMultiDeletion"

    const-string v3, "runInternal run: delete the sim contact failed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    move-object/from16 v0, v21

    iget v2, v0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mContactId:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-lt v2, v0, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/mediatek/contacts/list/service/DeleteProcessor;->actualBatchDelete(Ljava/util/ArrayList;)I

    const-string v2, "ContactsMultiDeletion"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "the "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v20, v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " times iBatchDel = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    sub-int v2, v6, v5

    const/16 v3, 0x60f

    if-gt v2, v3, :cond_c

    const/16 v18, 0x64

    move/from16 v19, v20

    goto/16 :goto_1

    :cond_8
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_9
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mJobId:I

    move/from16 v0, v25

    invoke-virtual {v2, v3, v0}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    if-eqz v25, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mJobId:I

    invoke-virtual {v2, v3, v4, v6}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onFinished(III)V

    goto/16 :goto_0

    :cond_b
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget v9, v0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mJobId:I

    sub-int v12, v6, v11

    move v10, v6

    invoke-virtual/range {v7 .. v12}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onFailed(IIIII)V

    goto/16 :goto_0

    :cond_c
    move/from16 v19, v20

    goto/16 :goto_1
.end method

.method private unregisterReceiver()V
    .locals 0

    return-void
.end method


# virtual methods
.method public declared-synchronized cancel(Z)Z
    .locals 7
    .param p1    # Z

    const/4 v6, 0x1

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    const-string v1, "ContactsMultiDeletion"

    const-string v2, "DeleteProcessor received cancel request"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mDone:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mCanceled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    const-string v1, "ContactsMultiDeletion"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[cancel]!mIsRunning : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mIsRunning:Z

    if-nez v3, :cond_2

    move v0, v6

    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mCanceled:Z

    iget-boolean v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mIsRunning:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    iget v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mJobId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v1, 0x2

    iget v2, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mJobId:I

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onCanceled(IIIII)V

    :goto_1
    move v0, v6

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    iget v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mJobId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v1, 0x2

    iget v2, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mJobId:I

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onCanceling(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public declared-synchronized isCancelled()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mCanceled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isDone()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 2

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mIsRunning:Z

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const/16 v0, 0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-direct {p0}, Lcom/mediatek/contacts/list/service/DeleteProcessor;->registerReceiver()V

    invoke-direct {p0}, Lcom/mediatek/contacts/list/service/DeleteProcessor;->runInternal()V

    invoke-direct {p0}, Lcom/mediatek/contacts/list/service/DeleteProcessor;->unregisterReceiver()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mDone:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    monitor-enter p0

    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mDone:Z

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/DeleteProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_1
    throw v0

    :catchall_2
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0
.end method
