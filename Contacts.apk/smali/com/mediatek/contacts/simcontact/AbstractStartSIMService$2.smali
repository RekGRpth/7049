.class Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;
.super Ljava/lang/Thread;
.source "AbstractStartSIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->query(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

.field final synthetic val$iccUri:Landroid/net/Uri;

.field final synthetic val$simType:I

.field final synthetic val$slotId:I

.field final synthetic val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Landroid/net/Uri;IILcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iput-object p2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$iccUri:Landroid/net/Uri;

    iput p3, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$slotId:I

    iput p4, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$simType:I

    iput-object p5, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    :try_start_0
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$iccUri:Landroid/net/Uri;

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$200()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    :goto_0
    const-string v0, "AbstractStartSIMService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[query]slotId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$slotId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|cursor:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v7

    const-string v0, "AbstractStartSIMService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[query]slotId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$slotId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|cursor count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$simType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {v6, v0, v1}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->syncUSIMGroupContactsGroup(Landroid/content/Context;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;Ljava/util/HashMap;)V

    :goto_1
    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iput-object v8, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sendImportSimContactsMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    return-void

    :catch_0
    move-exception v9

    const-string v0, "AbstractStartSIMService"

    const-string v1, "catched exception. cursor is null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;->val$slotId:I

    invoke-static {v6, v0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->deleteUSIMGroupOnPhone(Landroid/content/Context;I)V

    goto :goto_1
.end method
