.class Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;
.super Ljava/util/Timer;
.source "AbstractStartSIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RefreshTimer"
.end annotation


# instance fields
.field private mSlotId:I

.field private mTimerTask:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;

.field final synthetic this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-direct {p0}, Ljava/util/Timer;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->mTimerTask:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->mSlotId:I

    iput p2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->mSlotId:I

    return-void
.end method


# virtual methods
.method protected clear()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->mTimerTask:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;

    return-void
.end method

.method protected declared-synchronized schedule(JLcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 3
    .param p1    # J
    .param p3    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[schedule]slotId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->mSlotId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[schedule]timerTask:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->mTimerTask:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[schedule]delay:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->mTimerTask:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->mTimerTask:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;

    invoke-virtual {v0, p3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;->updateTaskWork(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v0, p3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$600(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->mSlotId:I

    invoke-direct {v0, v1, v2, p3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;ILcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->mTimerTask:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->mTimerTask:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;

    invoke-virtual {p0, v0, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
