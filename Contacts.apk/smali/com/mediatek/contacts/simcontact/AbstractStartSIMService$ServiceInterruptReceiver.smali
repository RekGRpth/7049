.class Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceInterruptReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AbstractStartSIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ServiceInterruptReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceInterruptReceiver;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceInterruptReceiver;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onReceive]intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.action.LAUNCH_CONTACTS_LIST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    sput-boolean v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sNeedToDelay:Z

    :cond_0
    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceInterruptReceiver;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onReceive]sNeedToDelay: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sNeedToDelay:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    return-void
.end method
