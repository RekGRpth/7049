.class public Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;
.super Ljava/lang/Object;
.source "AbstractStartSIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ServiceWorkData"
.end annotation


# instance fields
.field public mInServiceState:I

.field public mSimCursor:Landroid/database/Cursor;

.field public mSimId:I

.field public mSimType:I

.field public mSlotId:I

.field public mWorkType:I


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimId:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimCursor:Landroid/database/Cursor;

    iput v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mInServiceState:I

    return-void
.end method

.method constructor <init>(IIIILandroid/database/Cursor;I)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/database/Cursor;
    .param p6    # I

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimId:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimCursor:Landroid/database/Cursor;

    iput v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mInServiceState:I

    iput p1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    iput p2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iput p3, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimId:I

    iput p4, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    iput-object p5, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimCursor:Landroid/database/Cursor;

    iput p6, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mInServiceState:I

    return-void
.end method

.method constructor <init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 2
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimId:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimCursor:Landroid/database/Cursor;

    iput v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mInServiceState:I

    iget v0, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    iget v0, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iget v0, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimId:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimId:I

    iget v0, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    iget-object v0, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimCursor:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimCursor:Landroid/database/Cursor;

    iget v0, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mInServiceState:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mInServiceState:I

    return-void
.end method
