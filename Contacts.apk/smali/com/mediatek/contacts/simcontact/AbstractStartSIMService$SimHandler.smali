.class Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;
.super Landroid/os/Handler;
.source "AbstractStartSIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 16
    .param p1    # Landroid/os/Message;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage] msg.what = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget v5, v0, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->what:I

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]process MSG SLOT1|| sServiceState:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$700()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$800(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto :goto_0

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]process MSG SLOT2|| sServiceState2:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$900()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$800(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto :goto_0

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]LOAD SLOT1|| sServiceState:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$700()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$700()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_0

    const/4 v3, 0x2

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$702(I)I

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v15, v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkSimState(I)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkPhoneBookState(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v14, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]LOAD SLOT1|| simStateReady:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    if-eqz v14, :cond_2

    const/4 v3, 0x1

    if-ne v15, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-static {v4, v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto/16 :goto_0

    :cond_1
    const/4 v14, 0x0

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    move-result-object v3

    const/16 v4, 0x1f4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v10

    invoke-virtual {v10}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :sswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]LOAD SLOT2|| sServiceState2:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$900()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$900()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_0

    const/4 v3, 0x2

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$902(I)I

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v15, v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkSimState(I)Z

    move-result v3

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkPhoneBookState(I)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v14, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]LOAD SLOT2|| simStateReady:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    if-eqz v14, :cond_4

    const/4 v3, 0x1

    if-ne v15, v3, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-static {v4, v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto/16 :goto_0

    :cond_3
    const/4 v14, 0x0

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    move-result-object v3

    const/16 v4, 0x1f5

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v10

    invoke-virtual {v10}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :sswitch_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]REMOVE SLOT1|| sServiceState:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$700()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$700()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    const/4 v3, 0x1

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$702(I)I

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-virtual {v3, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->deleteSimContact(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto/16 :goto_0

    :sswitch_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]REMOVE SLOT2|| sServiceState2:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$900()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$900()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    const/4 v3, 0x1

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$902(I)I

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-virtual {v3, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->deleteSimContact(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto/16 :goto_0

    :sswitch_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]import SLOT1|| sServiceState:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$700()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$700()I

    move-result v3

    const/4 v4, 0x3

    if-ge v3, v4, :cond_0

    const/4 v3, 0x3

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$702(I)I

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-direct {v4, v5, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1102(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1100(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :sswitch_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]import SLOT2|| sServiceState2:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$900()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$900()I

    move-result v3

    const/4 v4, 0x3

    if-ge v3, v4, :cond_0

    const/4 v3, 0x3

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$902(I)I

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-direct {v4, v5, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1202(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1200(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :sswitch_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]finish SLOT1|| sServiceState:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$700()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$702(I)I

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "AbstractStartSIMService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SLOT1 mSimWorkQueue: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/AbstractCollection;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v8, -0x1

    :goto_3
    const-string v3, "AbstractStartSIMService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SLOT1 DoneWorkType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x1

    if-ne v8, v3, :cond_5

    sget-object v3, Lcom/android/contacts/ContactsUtils;->isServiceRunning:[Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1400(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)I

    move-result v4

    const/4 v5, 0x0

    aput-boolean v5, v3, v4

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    const-string v3, "AbstractStartSIMService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SLOT1 LastWorkType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq v8, v2, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;-><init>(IIIILandroid/database/Cursor;I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    move-result-object v3

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v13

    iput v2, v13, Landroid/os/Message;->arg1:I

    iput-object v1, v13, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v13}, Landroid/os/Message;->sendToTarget()V

    const/4 v11, 0x0

    :cond_6
    const-string v3, "AbstractStartSIMService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SLOT1 reallyFinished1: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v11, :cond_7

    new-instance v9, Landroid/content/Intent;

    const-string v3, "com.android.contacts.ACTION_PHB_LOAD_FINISHED"

    invoke-direct {v9, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "simId"

    const/4 v4, 0x0

    invoke-virtual {v9, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "slotId"

    const/4 v4, 0x0

    invoke-virtual {v9, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-virtual {v3, v9}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    const-string v4, "After stopSelf SLOT1"

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto/16 :goto_3

    :sswitch_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[handleMessage]finish SLOT2|| sServiceState2:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$900()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$902(I)I

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "AbstractStartSIMService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SLOT2 mSimWorkQueue2: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/AbstractCollection;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_c

    const/4 v8, -0x1

    :goto_4
    const-string v3, "AbstractStartSIMService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SLOT2 DoneWorkType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x1

    if-ne v8, v3, :cond_9

    sget-object v3, Lcom/android/contacts/ContactsUtils;->isServiceRunning:[Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1400(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)I

    move-result v4

    const/4 v5, 0x0

    aput-boolean v5, v3, v4

    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    const-string v3, "AbstractStartSIMService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SLOT2 LastWorkType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq v8, v2, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    const/4 v3, 0x1

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;-><init>(IIIILandroid/database/Cursor;I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    move-result-object v3

    const/16 v4, 0x65

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v13

    iput v2, v13, Landroid/os/Message;->arg1:I

    iput-object v1, v13, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v13}, Landroid/os/Message;->sendToTarget()V

    const/4 v12, 0x0

    :cond_a
    const-string v3, "AbstractStartSIMService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SLOT2 reallyFinished2: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v12, :cond_b

    new-instance v9, Landroid/content/Intent;

    const-string v3, "com.android.contacts.ACTION_PHB_LOAD_FINISHED"

    invoke-direct {v9, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "simId"

    const/4 v4, 0x1

    invoke-virtual {v9, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "slotId"

    const/4 v4, 0x1

    invoke-virtual {v9, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-virtual {v3, v9}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    const-string v4, "After stopSelf SLOT2"

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto/16 :goto_4

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_3
        0x12c -> :sswitch_4
        0x12d -> :sswitch_5
        0x190 -> :sswitch_6
        0x191 -> :sswitch_7
        0x1f4 -> :sswitch_8
        0x1f5 -> :sswitch_9
    .end sparse-switch
.end method
