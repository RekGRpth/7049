.class public abstract Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
.super Landroid/app/Service;
.source "AbstractStartSIMService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$NamePhoneTypePair;,
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;,
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;,
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;,
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;,
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;,
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceInterruptReceiver;
    }
.end annotation


# static fields
.field public static final ACTION_PHB_LOAD_FINISHED:Ljava/lang/String; = "com.android.contacts.ACTION_PHB_LOAD_FINISHED"

.field protected static final ADDITIONAL_NUMBER_COLUMN:I = 0x4

.field private static final COLUMN_NAMES:[Ljava/lang/String;

.field private static final DBG:Z = true

.field private static final DELAY_TIME:I = 0x9c4

.field protected static final EMAIL_COLUMN:I = 0x3

.field public static final FINISH_IMPORTING:I = 0x1f4

.field protected static final GROUP_COLUMN:I = 0x5

.field public static final IMPORT_NEW:I = 0x190

.field protected static final INDEX_COLUMN:I = 0x0

.field public static final LOAD_SIM_CONTACTS:I = 0xc8

.field private static final MAX_OP_COUNT_IN_ONE_BATCH:I = 0x5a

.field public static final MSG_DELAY_PROCESSING:I = 0x64

.field public static final MSG_SLOT1_FINISH_IMPORTING:I = 0x1f4

.field public static final MSG_SLOT1_IMPORT:I = 0x190

.field public static final MSG_SLOT1_LOAD:I = 0xc8

.field public static final MSG_SLOT1_MSG_DELAY_PROCESSING:I = 0x64

.field public static final MSG_SLOT1_REMOVE_OLD:I = 0x12c

.field public static final MSG_SLOT2_FINISH_IMPORTING:I = 0x1f5

.field public static final MSG_SLOT2_IMPORT:I = 0x191

.field public static final MSG_SLOT2_LOAD:I = 0xc9

.field public static final MSG_SLOT2_MSG_DELAY_PROCESSING:I = 0x65

.field public static final MSG_SLOT2_REMOVE_OLD:I = 0x12d

.field protected static final NAME_COLUMN:I = 0x1

.field protected static final NUMBER_COLUMN:I = 0x2

.field private static final REFRESH_INTERVAL_MS:J = 0x3e8L

.field private static final REFRESH_INTERVAL_MS_2:J = 0x514L

.field public static final REMOVE_OLD:I = 0x12c

.field public static final SERVICE_DELETE_CONTACTS:I = 0x1

.field public static final SERVICE_IDLE:I = 0x0

.field public static final SERVICE_IMPORT_CONTACTS:I = 0x3

.field public static final SERVICE_QUERY_SIM:I = 0x2

.field public static final SERVICE_SLOT_KEY:Ljava/lang/String; = "which_slot"

.field public static final SERVICE_WORK_IMPORT:I = 0x1

.field public static final SERVICE_WORK_NONE:I = 0x0

.field public static final SERVICE_WORK_REMOVE:I = 0x2

.field public static final SERVICE_WORK_TYPE:Ljava/lang/String; = "work_type"

.field public static final SERVICE_WORK_UNKNOWN:I = 0x3

.field public static final SIM_TYPE_SIM:I = 0x0

.field public static final SIM_TYPE_UIM:I = 0x2

.field public static final SIM_TYPE_UNKNOWN:I = -0x1

.field public static final SIM_TYPE_USIM:I = 0x1

.field public static final SLOT1:I = 0x0

.field public static final SLOT2:I = 0x1

.field private static final TAG:Ljava/lang/String; = "AbstractStartSIMService"

.field protected static sNeedToDelay:Z

.field private static sServiceState:I

.field private static sServiceState2:I


# instance fields
.field private mCurrentSimId:I

.field private mGrpIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

.field protected mReceiver:Landroid/content/BroadcastReceiver;

.field private final mRefreshTimer:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

.field private final mRefreshTimer2:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

.field private mSimWorkQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSimWorkQueue2:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mThread1:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

.field private mThread2:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    sput v3, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceState:I

    sput v3, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceState2:I

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "index"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "number"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "emails"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "additionalNumber"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "groupIds"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->COLUMN_NAMES:[Ljava/lang/String;

    sput-boolean v3, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sNeedToDelay:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mSimWorkQueue:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mSimWorkQueue2:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mGrpIdMap:Ljava/util/HashMap;

    new-instance v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;I)V

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mRefreshTimer:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

    new-instance v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;I)V

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mRefreshTimer2:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sendLoadSimMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->query(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mThread1:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mThread1:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mThread2:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mThread2:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mSimWorkQueue:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mCurrentSimId:I

    return v0
.end method

.method static synthetic access$1500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mSimWorkQueue2:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->COLUMN_NAMES:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mGrpIdMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Landroid/database/Cursor;Landroid/content/ContentResolver;IJILjava/util/HashSet;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Landroid/database/Cursor;
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # I
    .param p4    # J
    .param p6    # I
    .param p7    # Ljava/util/HashSet;
    .param p8    # Z

    invoke-direct/range {p0 .. p8}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->actuallyImportOneSimContact(Landroid/database/Cursor;Landroid/content/ContentResolver;IJILjava/util/HashSet;Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sendRemoveSimContactsMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    return-void
.end method

.method static synthetic access$700()I
    .locals 1

    sget v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceState:I

    return v0
.end method

.method static synthetic access$702(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceState:I

    return p0
.end method

.method static synthetic access$800(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->processDelayMessage(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    return-void
.end method

.method static synthetic access$900()I
    .locals 1

    sget v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceState2:I

    return v0
.end method

.method static synthetic access$902(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceState2:I

    return p0
.end method

.method private actuallyImportOneSimContact(Landroid/database/Cursor;Landroid/content/ContentResolver;IJILjava/util/HashSet;Z)V
    .locals 49
    .param p1    # Landroid/database/Cursor;
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # I
    .param p4    # J
    .param p6    # I
    .param p8    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Landroid/content/ContentResolver;",
            "IJI",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;Z)V"
        }
    .end annotation

    invoke-static/range {p0 .. p0}, Lcom/android/contacts/model/AccountTypeManager;->getInstance(Landroid/content/Context;)Lcom/android/contacts/model/AccountTypeManager;

    move-result-object v12

    const/16 v44, 0x1

    move/from16 v0, v44

    invoke-virtual {v12, v0}, Lcom/android/contacts/model/AccountTypeManager;->getAccounts(Z)Ljava/util/List;

    move-result-object v29

    const/16 v44, 0x1

    move/from16 v0, p6

    move/from16 v1, v44

    if-ne v0, v1, :cond_8

    const/16 v27, 0x1

    :goto_0
    const/4 v8, -0x1

    const/4 v4, 0x0

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v44

    if-eqz v44, :cond_2

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/contacts/model/AccountWithDataSet;

    instance-of v0, v5, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    move/from16 v44, v0

    if-eqz v44, :cond_0

    move-object v6, v5

    check-cast v6, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v6}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getSlotId()I

    move-result v8

    move/from16 v0, p3

    if-ne v8, v0, :cond_0

    iget-object v0, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v44, v0

    const-string v45, "USIM Account"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_9

    const/4 v7, 0x1

    :goto_1
    iget-object v0, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v44, v0

    const-string v45, "UIM Account"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1

    const/4 v7, 0x2

    :cond_1
    move/from16 v0, p6

    if-ne v7, v0, :cond_2

    move-object v4, v6

    :cond_2
    if-nez v4, :cond_3

    :cond_3
    new-instance v35, Ljava/util/ArrayList;

    invoke-direct/range {v35 .. v35}, Ljava/util/ArrayList;-><init>()V

    const/16 v21, 0x0

    const/4 v10, 0x0

    if-eqz p1, :cond_1d

    const/16 v44, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-static/range {p0 .. p0}, Lcom/android/contacts/ContactsUtils;->getCurrentCountryIso(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    const-string v44, "AbstractStartSIMService"

    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "[actuallyImportOneSimContact] countryCode : "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_2
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v44

    if-eqz v44, :cond_1a

    const/16 v44, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v26, v0

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "SLOT"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string v45, "||indexInSim:"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string v45, "||isInserted:"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    if-nez p7, :cond_a

    const/16 v44, 0x0

    :goto_3
    move-object/from16 v0, v45

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    if-eqz p7, :cond_5

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v44

    move-object/from16 v0, p7

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v44

    if-nez v44, :cond_4

    :cond_5
    new-instance v33, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$NamePhoneTypePair;

    const/16 v44, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v33

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$NamePhoneTypePair;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v33

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$NamePhoneTypePair;->name:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v33

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$NamePhoneTypePair;->phoneType:I

    move/from16 v37, v0

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "phoneType is "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    move-object/from16 v0, v33

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$NamePhoneTypePair;->phoneTypeSuffix:Ljava/lang/String;

    move-object/from16 v38, v0

    const/16 v44, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v36

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "indexInSim = "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string v45, ", intIndexInSim = "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string v45, ", name = "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string v45, ", number = "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    const/16 v28, 0x0

    sget-object v44, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v44 .. v44}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    new-instance v43, Landroid/content/ContentValues;

    invoke-direct/range {v43 .. v43}, Landroid/content/ContentValues;-><init>()V

    const/4 v9, 0x0

    if-eqz v4, :cond_6

    iget-object v9, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v44, "account_name"

    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v45, v0

    invoke-virtual/range {v43 .. v45}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v44, "account_type"

    iget-object v0, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v45, v0

    invoke-virtual/range {v43 .. v45}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-string v44, "indicate_phone_or_sim_contact"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v45

    invoke-virtual/range {v43 .. v45}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v44, "aggregation_mode"

    const/16 v45, 0x3

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    invoke-virtual/range {v43 .. v45}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v44, "index_in_sim"

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v45

    invoke-virtual/range {v43 .. v45}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz p8, :cond_7

    const-string v44, "is_sdn_contact"

    const/16 v45, 0x1

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    invoke-virtual/range {v43 .. v45}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_7
    move-object/from16 v0, v43

    invoke-virtual {v13, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v13}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v44

    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v28, v28, 0x1

    invoke-static/range {v36 .. v36}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v44

    if-nez v44, :cond_d

    const-string v44, "AbstractStartSIMService"

    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "[actuallyImportOneSimContact] phoneNumber before : "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v15}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getAsYouTypeFormatter(Ljava/lang/String;)Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    move-result-object v31

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->toCharArray()[C

    move-result-object v14

    array-length v0, v14

    move/from16 v23, v0

    const/16 v34, 0x0

    :goto_4
    move/from16 v0, v34

    move/from16 v1, v23

    if-ge v0, v1, :cond_b

    aget-char v44, v14, v34

    move-object/from16 v0, v31

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;->inputDigit(C)Ljava/lang/String;

    move-result-object v36

    add-int/lit8 v34, v34, 0x1

    goto :goto_4

    :cond_8
    const/16 v27, 0x0

    goto/16 :goto_0

    :cond_9
    const/4 v7, 0x0

    goto/16 :goto_1

    :cond_a
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v44

    move-object/from16 v0, p7

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v44

    goto/16 :goto_3

    :cond_b
    const-string v44, "AbstractStartSIMService"

    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "[actuallyImportOneSimContact] phoneNumber after : "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v44, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v44 .. v44}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v44, "raw_contact_id"

    move-object/from16 v0, v44

    move/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v44, "mimetype"

    const-string v45, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v44

    const/16 v45, 0x2

    move-object/from16 v0, v44

    move-object/from16 v1, p1

    move/from16 v2, v45

    invoke-virtual {v0, v9, v13, v1, v2}, Lcom/android/contacts/ext/ContactAccountExtension;->checkOperationBuilder(Ljava/lang/String;Landroid/content/ContentProviderOperation$Builder;Landroid/database/Cursor;I)Z

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Lcom/mediatek/contacts/ExtensionManager;->getContactListExtension()Lcom/android/contacts/ext/ContactListExtension;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/android/contacts/ext/ContactListExtension;->getReplaceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    const-string v44, "data1"

    move-object/from16 v0, v44

    move-object/from16 v1, v36

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-static/range {v38 .. v38}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v44

    if-nez v44, :cond_c

    const-string v44, "data15"

    move-object/from16 v0, v44

    move-object/from16 v1, v38

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :cond_c
    invoke-virtual {v13}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v44

    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v28, v28, 0x1

    :cond_d
    invoke-static/range {v32 .. v32}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v44

    if-nez v44, :cond_e

    sget-object v44, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v44 .. v44}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v44, "raw_contact_id"

    move-object/from16 v0, v44

    move/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v44, "mimetype"

    const-string v45, "vnd.android.cursor.item/name"

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v44, "data2"

    move-object/from16 v0, v44

    move-object/from16 v1, v32

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v13}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v44

    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v28, v28, 0x1

    :cond_e
    if-eqz v27, :cond_19

    const-string v44, "[actuallyImportOneSimContact]import a USIM contact."

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    const/16 v44, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "[actuallyImportOneSimContact]emailAddresses:"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v44

    if-nez v44, :cond_10

    const-string v44, ","

    move-object/from16 v0, v19

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    move-object/from16 v11, v18

    array-length v0, v11

    move/from16 v30, v0

    const/16 v22, 0x0

    :goto_5
    move/from16 v0, v22

    move/from16 v1, v30

    if-ge v0, v1, :cond_10

    aget-object v17, v11, v22

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "emailAddress IS "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v44

    if-nez v44, :cond_f

    const-string v44, "null"

    move-object/from16 v0, v17

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-nez v44, :cond_f

    sget-object v44, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v44 .. v44}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v44, "raw_contact_id"

    move-object/from16 v0, v44

    move/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v44, "mimetype"

    const-string v45, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v44, "data2"

    const/16 v45, 0x4

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v44, "data1"

    move-object/from16 v0, v44

    move-object/from16 v1, v17

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v13}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v44

    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v28, v28, 0x1

    :cond_f
    add-int/lit8 v22, v22, 0x1

    goto :goto_5

    :cond_10
    const/16 v44, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "[actuallyImportOneSimContact]additionalNumber:"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v44

    if-nez v44, :cond_13

    const-string v44, "AbstractStartSIMService"

    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "[actuallyImportOneSimContact] additionalNumber before : "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v15}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getAsYouTypeFormatter(Ljava/lang/String;)Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    move-result-object v31

    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v14

    array-length v0, v14

    move/from16 v23, v0

    const/16 v34, 0x0

    :goto_6
    move/from16 v0, v34

    move/from16 v1, v23

    if-ge v0, v1, :cond_11

    aget-char v44, v14, v34

    move-object/from16 v0, v31

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;->inputDigit(C)Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v34, v34, 0x1

    goto :goto_6

    :cond_11
    const-string v44, "AbstractStartSIMService"

    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "[actuallyImportOneSimContact] additionalNumber after : "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "additionalNumber is "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    sget-object v44, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v44 .. v44}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v44, "raw_contact_id"

    move-object/from16 v0, v44

    move/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v44, "mimetype"

    const-string v45, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v44

    const/16 v45, 0x0

    move-object/from16 v0, v44

    move-object/from16 v1, p1

    move/from16 v2, v45

    invoke-virtual {v0, v9, v13, v1, v2}, Lcom/android/contacts/ext/ContactAccountExtension;->checkOperationBuilder(Ljava/lang/String;Landroid/content/ContentProviderOperation$Builder;Landroid/database/Cursor;I)Z

    move-result v44

    if-nez v44, :cond_12

    const-string v44, "data2"

    const/16 v45, 0x7

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :cond_12
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Lcom/mediatek/contacts/ExtensionManager;->getContactListExtension()Lcom/android/contacts/ext/ContactListExtension;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v10}, Lcom/android/contacts/ext/ContactListExtension;->getReplaceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v44, "data1"

    move-object/from16 v0, v44

    invoke-virtual {v13, v0, v10}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v44, "is_additional_number"

    const/16 v45, 0x1

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v13}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v44

    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v28, v28, 0x1

    :cond_13
    invoke-static {v8}, Lcom/mediatek/contacts/extention/aassne/SneExt;->hasSne(I)Z

    move-result v44

    if-eqz v44, :cond_14

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v35

    move-object/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v9, v1, v2, v3}, Lcom/android/contacts/ext/ContactAccountExtension;->buildOperationFromCursor(Ljava/lang/String;Ljava/util/ArrayList;Landroid/database/Cursor;I)Z

    move-result v44

    if-eqz v44, :cond_14

    add-int/lit8 v28, v28, 0x1

    :cond_14
    const/16 v44, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "[actuallyImportOneSimContact]sim group id string: "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-static/range {v42 .. v42}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v44

    if-nez v44, :cond_19

    const/16 v40, 0x0

    invoke-static/range {v42 .. v42}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v44

    if-nez v44, :cond_15

    const-string v44, ","

    move-object/from16 v0, v42

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v40

    :cond_15
    move-object/from16 v11, v40

    array-length v0, v11

    move/from16 v30, v0

    const/16 v22, 0x0

    :goto_7
    move/from16 v0, v22

    move/from16 v1, v30

    if-ge v0, v1, :cond_19

    aget-object v41, v11, v22

    const/16 v39, -0x1

    :try_start_0
    invoke-static/range {v41 .. v41}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v44

    if-nez v44, :cond_16

    invoke-static/range {v41 .. v41}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v39

    :cond_16
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "[USIM Group] sim group id ugrpId: "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    if-lez v39, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mGrpIdMap:Ljava/util/HashMap;

    move-object/from16 v44, v0

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "[USIM Group]simgroup mapping group grpId: "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    if-nez v20, :cond_18

    const-string v44, "AbstractStartSIMService"

    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "[USIM Group] Error. Catch unhandled SIM group error. ugrp: "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_17
    :goto_8
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_7

    :catch_0
    move-exception v16

    const-string v44, "[USIM Group] catched exception"

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_8

    :cond_18
    sget-object v44, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v44 .. v44}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v44, "mimetype"

    const-string v45, "vnd.android.cursor.item/group_membership"

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v44, "data1"

    move-object/from16 v0, v44

    move-object/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v44, "raw_contact_id"

    move-object/from16 v0, v44

    move/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v13}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v44

    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v28, v28, 0x1

    goto :goto_8

    :cond_19
    add-int v21, v21, v28

    const/16 v44, 0x5a

    move/from16 v0, v21

    move/from16 v1, v44

    if-le v0, v1, :cond_4

    :try_start_1
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkSimState(I)Z

    move-result v44

    if-nez v44, :cond_1e

    const-string v44, "check sim State: false"

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_1a
    :try_start_2
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "Before applyBatch. sNeedToDelay:"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    sget-boolean v45, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sNeedToDelay:Z

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    sget-boolean v44, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sNeedToDelay:Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_6

    if-eqz v44, :cond_1b

    const-wide/16 v44, 0x9c4

    :try_start_3
    invoke-static/range {v44 .. v45}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Landroid/content/OperationApplicationException; {:try_start_3 .. :try_end_3} :catch_6

    :goto_9
    const/16 v44, 0x0

    :try_start_4
    sput-boolean v44, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sNeedToDelay:Z

    :cond_1b
    const-string v44, "Before applyBatch "

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkSimState(I)Z

    move-result v44

    if-eqz v44, :cond_1c

    const-string v44, "check sim State: true"

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v44

    if-nez v44, :cond_1c

    const-string v44, "com.android.contacts"

    move-object/from16 v0, p2

    move-object/from16 v1, v44

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    :cond_1c
    const-string v44, "After applyBatch "

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Landroid/content/OperationApplicationException; {:try_start_4 .. :try_end_4} :catch_6

    :cond_1d
    :goto_a
    return-void

    :cond_1e
    :try_start_5
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "Before applyBatch. sNeedToDelay:"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    sget-boolean v45, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sNeedToDelay:Z

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    sget-boolean v44, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sNeedToDelay:Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_5 .. :try_end_5} :catch_3

    if-eqz v44, :cond_1f

    const-wide/16 v44, 0x9c4

    :try_start_6
    invoke-static/range {v44 .. v45}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_6 .. :try_end_6} :catch_3

    :goto_b
    const/16 v44, 0x0

    :try_start_7
    sput-boolean v44, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sNeedToDelay:Z

    :cond_1f
    const-string v44, "Before applyBatch. "

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    const-string v44, "com.android.contacts"

    move-object/from16 v0, p2

    move-object/from16 v1, v44

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    const-string v44, "After applyBatch "

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_7 .. :try_end_7} :catch_3

    :goto_c
    const/16 v21, 0x0

    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_2

    :catch_1
    move-exception v16

    const/16 v44, 0x0

    :try_start_8
    sput-boolean v44, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sNeedToDelay:Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_b

    :catch_2
    move-exception v16

    const-string v44, "AbstractStartSIMService"

    const-string v45, "%s: %s"

    const/16 v46, 0x2

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v48

    aput-object v48, v46, v47

    const/16 v47, 0x1

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v48

    aput-object v48, v46, v47

    invoke-static/range {v45 .. v46}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    :catch_3
    move-exception v16

    const-string v44, "AbstractStartSIMService"

    const-string v45, "%s: %s"

    const/16 v46, 0x2

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v48

    aput-object v48, v46, v47

    const/16 v47, 0x1

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v48

    aput-object v48, v46, v47

    invoke-static/range {v45 .. v46}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    :catch_4
    move-exception v16

    const/16 v44, 0x0

    :try_start_9
    sput-boolean v44, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sNeedToDelay:Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Landroid/content/OperationApplicationException; {:try_start_9 .. :try_end_9} :catch_6

    goto/16 :goto_9

    :catch_5
    move-exception v16

    const-string v44, "AbstractStartSIMService"

    const-string v45, "%s: %s"

    const/16 v46, 0x2

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v48

    aput-object v48, v46, v47

    const/16 v47, 0x1

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v48

    aput-object v48, v46, v47

    invoke-static/range {v45 .. v46}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    :catch_6
    move-exception v16

    const-string v44, "AbstractStartSIMService"

    const-string v45, "%s: %s"

    const/16 v46, 0x2

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v48

    aput-object v48, v46, v47

    const/16 v47, 0x1

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v48

    aput-object v48, v46, v47

    invoke-static/range {v45 .. v46}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a
.end method

.method public static getServiceState(I)I
    .locals 1
    .param p0    # I

    if-nez p0, :cond_0

    sget v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceState:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceState2:I

    goto :goto_0
.end method

.method public static isServiceRunning(I)Z
    .locals 3
    .param p0    # I

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_2

    sget v2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceState:I

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    sget v2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceState2:I

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "AbstractStartSIMService"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private processDelayMessage(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 4
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v0, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mRefreshTimer:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->schedule(JLcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mRefreshTimer2:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

    const-wide/16 v2, 0x514

    invoke-virtual {v1, v2, v3, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->schedule(JLcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto :goto_0
.end method

.method private query(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 6
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    const/4 v0, 0x1

    iget v3, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iget v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    const/4 v5, -0x1

    if-ne v1, v5, :cond_0

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->getSimTypeBySlot(I)I

    move-result v1

    iput v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    :cond_0
    iget v4, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[query]slotId: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " ||isUSIM:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-ne v4, v0, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SimCardUtils$SimUri;->getSimUri(I)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[query]uri: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Landroid/net/Uri;IILcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendLoadSimMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 4
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[sendLoadSimMsg]slotId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    add-int/lit16 v3, v1, 0xc8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget v2, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    iput v2, v0, Landroid/os/Message;->arg1:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method private sendRemoveSimContactsMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 4
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[sendRemoveSimContactsMsg]slotId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    add-int/lit16 v3, v1, 0x12c

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mRefreshTimer:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

    invoke-virtual {v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->clear()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mRefreshTimer2:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

    invoke-virtual {v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->clear()V

    goto :goto_0
.end method


# virtual methods
.method checkPhoneBookState(I)Z
    .locals 1
    .param p1    # I

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isPhoneBookReady(I)Z

    move-result v0

    return v0
.end method

.method checkSimState(I)Z
    .locals 9
    .param p1    # I

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimPukRequest(I)Z

    move-result v5

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimPinRequest(I)Z

    move-result v4

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimInserted(I)Z

    move-result v3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSetRadioOn(Landroid/content/ContentResolver;I)Z

    move-result v1

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isFdnEnabed(I)Z

    move-result v0

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimInfoReady()Z

    move-result v2

    const-string v6, "AbstractStartSIMService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[checkSimState]slotId:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||simPUKReq: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||simPINReq: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||isRadioOn: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||isFdnEnabled: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||simInserted: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||isSimInfoReady:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v5, :cond_0

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    if-nez v4, :cond_0

    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    :cond_0
    const/4 v6, 0x0

    :goto_0
    return v6

    :cond_1
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public deleteSimContact(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 3
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v0, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;ILcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const-string v0, "onCreate()"

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "onDestroy()"

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mReceiver:Landroid/content/BroadcastReceiver;

    :cond_0
    sget-object v0, Lcom/android/contacts/ContactsUtils;->isServiceRunning:[Z

    iget v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mCurrentSimId:I

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 9
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    const/4 v6, 0x0

    const/4 v3, -0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[onStart]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", startId "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mReceiver:Landroid/content/BroadcastReceiver;

    if-nez v4, :cond_1

    new-instance v4, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceInterruptReceiver;

    invoke-direct {v4, p0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceInterruptReceiver;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)V

    iput-object v4, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v7, Landroid/content/IntentFilter;

    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "com.android.action.LAUNCH_CONTACTS_LIST"

    invoke-virtual {v7, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v7}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_1
    new-instance v4, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    invoke-direct {v4, p0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)V

    iput-object v4, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    const-string v4, "which_slot"

    invoke-virtual {p1, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v4, "work_type"

    invoke-virtual {p1, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-nez v2, :cond_2

    iget-object v4, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mSimWorkQueue:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    const-string v4, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[onStart]slotId: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "|workType:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mCurrentSimId:I

    sget-object v4, Lcom/android/contacts/ContactsUtils;->isServiceRunning:[Z

    iget v5, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mCurrentSimId:I

    const/4 v8, 0x1

    aput-boolean v8, v4, v5

    new-instance v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    const/4 v5, 0x0

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;-><init>(IIIILandroid/database/Cursor;I)V

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sendTaskPoolMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mSimWorkQueue2:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public sendImportSimContactsMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 4
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[sendImportSimContactsMsg]slotId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    add-int/lit16 v3, v1, 0x190

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public sendTaskPoolMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 4
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    add-int/lit8 v3, v1, 0x64

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget v2, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    iput v2, v0, Landroid/os/Message;->arg1:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
