.class public Lcom/mediatek/contacts/simcontact/BootCmpReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BootCmpReceiver.java"


# static fields
.field private static INTENT_SIM_FILE_CHANGED:Ljava/lang/String; = null

.field private static INTENT_SIM_FILE_CHANGED_2:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "BootCmpReceiver"

.field private static mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->mContext:Landroid/content/Context;

    const-string v0, "android.intent.action.sim.SIM_FILES_CHANGED"

    sput-object v0, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->INTENT_SIM_FILE_CHANGED:Ljava/lang/String;

    const-string v0, "android.intent.action.sim.SIM_FILES_CHANGED_2"

    sput-object v0, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->INTENT_SIM_FILE_CHANGED_2:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    sput-object p1, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->mContext:Landroid/content/Context;

    const-string v1, "BootCmpReceiver"

    const-string v2, "In onReceive "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BootCmpReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "android.intent.action.PHB_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, p2}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->processPhoneBookChanged(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, p2}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->processAirplaneModeChanged(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v1, "android.intent.action.DUAL_SIM_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, p2}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->processDualSimModeChanged(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    const-string v1, "android.intent.action.SIM_STATE_CHANGED_EXTEND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0, p2}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->processSimStateChanged(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->INTENT_SIM_FILE_CHANGED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->processSimFilesChanged(I)V

    goto :goto_0

    :cond_5
    sget-object v1, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->INTENT_SIM_FILE_CHANGED_2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->processSimFilesChanged(I)V

    goto :goto_0

    :cond_6
    const-string v1, "android.intent.action.SIM_SETTING_INFO_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0, p2}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->processSimInfoUpdateForSettingChanged(Landroid/content/Intent;)V

    goto :goto_0

    :cond_7
    const-string v1, "android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p0}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->processIpoShutDown()V

    goto :goto_0

    :cond_8
    const-string v1, "android.intent.action.ACTION_PHONE_RESTART"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p2}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->processPhoneReset(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method processAirplaneModeChanged(Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Intent;

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v1, "BootCmpReceiver"

    const-string v2, "processAirplaneModeChanged"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "state"

    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "BootCmpReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[processAirplaneModeChanged]isAirplaneModeOn:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    invoke-virtual {p0, v5, v6}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    invoke-virtual {p0, v4, v6}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v5, v4}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    invoke-virtual {p0, v4, v4}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0
.end method

.method processDualSimModeChanged(Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Intent;

    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const-string v4, "BootCmpReceiver"

    const-string v5, "processDualSimModeChanged"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "mode"

    const/4 v5, -0x1

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    sget-object v4, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->mContext:Landroid/content/Context;

    const-string v5, "sim_setting_preference"

    invoke-virtual {v4, v5, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "dual_sim_mode"

    invoke-interface {v1, v4, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string v4, "BootCmpReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[processDualSimModeChanged]type:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|prevType:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v3, :pswitch_data_0

    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "dual_sim_mode"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :pswitch_0
    if-ne v2, v7, :cond_0

    invoke-virtual {p0, v9, v8}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :cond_0
    if-ne v2, v8, :cond_1

    invoke-virtual {p0, v7, v8}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v9, v8}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    invoke-virtual {p0, v7, v8}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :pswitch_1
    if-nez v2, :cond_2

    invoke-virtual {p0, v9, v7}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :cond_2
    if-ne v2, v10, :cond_3

    invoke-virtual {p0, v7, v8}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v9, v7}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    invoke-virtual {p0, v7, v8}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :pswitch_2
    if-nez v2, :cond_4

    invoke-virtual {p0, v7, v7}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :cond_4
    if-ne v2, v10, :cond_5

    invoke-virtual {p0, v9, v8}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v7, v7}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    invoke-virtual {p0, v9, v8}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :pswitch_3
    if-ne v2, v7, :cond_6

    invoke-virtual {p0, v7, v7}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :cond_6
    if-ne v2, v8, :cond_7

    invoke-virtual {p0, v9, v7}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :cond_7
    invoke-virtual {p0, v9, v7}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    invoke-virtual {p0, v7, v7}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method processIpoShutDown()V
    .locals 2

    const/4 v1, 0x2

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    return-void
.end method

.method processPhoneBookChanged(Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Intent;

    const-string v3, "BootCmpReceiver"

    const-string v4, "processPhoneBookChanged"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "ready"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v3, "simId"

    const/16 v4, -0xa

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "BootCmpReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[processPhoneBookChanged]phbReady:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|slotId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    if-ltz v2, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimWrapperInstanceUnCheck()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->updateSimInfoCache()V

    :cond_0
    return-void
.end method

.method processPhoneReset(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/4 v4, -0x1

    const-string v2, "BootCmpReceiver"

    const-string v3, "processPhoneReset"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimWrapperInstanceUnCheck()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->updateSimInfoCache()V

    :cond_0
    const-string v2, "SimId"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v4, :cond_1

    const-string v2, "BootCmpReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processPhoneReset"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    :cond_1
    return-void
.end method

.method processSimFilesChanged(I)V
    .locals 3
    .param p1    # I

    const-string v0, "BootCmpReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "processSimStateChanged:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isPhoneBookReady(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    :cond_0
    return-void
.end method

.method processSimInfoUpdateForSettingChanged(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const-string v1, "BootCmpReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processSimInfoUpdateForSettingChanged:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimWrapperInstanceUnCheck()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->updateSimInfoCache()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    goto :goto_0
.end method

.method processSimStateChanged(Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Intent;

    const-string v4, "BootCmpReceiver"

    const-string v5, "processSimStateChanged"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "phoneName"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "ss"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "simId"

    const/4 v5, -0x1

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "BootCmpReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mPhoneName:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|mIccStae:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|mySlotId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "ABSENT"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimWrapperInstanceUnCheck()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->updateSimInfoCache()V

    :cond_0
    const-string v4, "ABSENT"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "LOCKED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "NETWORK"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v4, 0x2

    invoke-virtual {p0, v3, v4}, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->startSimService(II)V

    :cond_2
    return-void
.end method

.method public startSimService(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->mContext:Landroid/content/Context;

    const-class v2, Lcom/mediatek/contacts/simcontact/StartSIMService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :goto_0
    const-string v1, "which_slot"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "work_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "BootCmpReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[startSimService]slotId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|workType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/mediatek/contacts/simcontact/BootCmpReceiver;->mContext:Landroid/content/Context;

    const-class v2, Lcom/mediatek/contacts/simcontact/StartSIMService2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method
