.class Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;
.super Ljava/lang/Thread;
.source "SIMInfoWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->updateSimInfoCache()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, -0x1

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$000(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v4, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$100(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/provider/Telephony$SIMInfo;->getAllSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$002(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/List;)Ljava/util/List;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$000(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v4, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$000(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$202(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;I)I

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$302(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/HashMap;)Ljava/util/HashMap;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$402(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/HashMap;)Ljava/util/HashMap;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$000(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$500(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Landroid/provider/Telephony$SIMInfo;)I

    move-result v2

    if-eq v2, v6, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$300(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$400(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v5, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    const-string v4, "[updateSimInfo] update mAllSimInfoList"

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$600(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/lang/String;)V

    :cond_2
    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$700(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v4, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$100(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$702(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/List;)Ljava/util/List;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$700(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v4, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$700(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$802(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;I)I

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$902(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/HashMap;)Ljava/util/HashMap;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$1002(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/HashMap;)Ljava/util/HashMap;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$700(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$500(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Landroid/provider/Telephony$SIMInfo;)I

    move-result v2

    if-eq v2, v6, :cond_3

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$900(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$1000(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/HashMap;

    move-result-object v3

    iget v4, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    const-string v4, "[updateSimInfo] updated mAllSimInfoList is null"

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$600(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_5
    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    const-string v4, "[updateSimInfo] update mInsertedSimInfoList"

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$600(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/lang/String;)V

    :cond_6
    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$1100(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Landroid/os/RegistrantList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RegistrantList;->notifyRegistrants()V

    goto :goto_2

    :cond_7
    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;->this$0:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    const-string v4, "[updateSimInfo] updated mInsertedSimInfoList is null"

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->access$600(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/lang/String;)V

    goto :goto_2
.end method
