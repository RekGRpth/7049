.class public Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
.super Ljava/lang/Object;
.source "SIMInfoWrapper.java"


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "SIMInfoWrapper"

.field private static mIsNullResult:Z

.field private static sSIMInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;


# instance fields
.field private mAllSimCount:I

.field private mAllSimInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAllSimInfoMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mInsertSim:Z

.field private mInsertedSimCount:I

.field private mInsertedSimInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mInsertedSimInfoMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSimIdSlotIdPairs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSimInfoUpdateRegistrantList:Landroid/os/RegistrantList;

.field private mSlotIdSimIdPairs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mIsNullResult:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v6, -0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoList:Ljava/util/List;

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoList:Ljava/util/List;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoMap:Ljava/util/HashMap;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoMap:Ljava/util/HashMap;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSlotIdSimIdPairs:Ljava/util/HashMap;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSimIdSlotIdPairs:Ljava/util/HashMap;

    iput-boolean v4, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertSim:Z

    iput v4, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimCount:I

    iput v4, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimCount:I

    new-instance v3, Landroid/os/RegistrantList;

    invoke-direct {v3}, Landroid/os/RegistrantList;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSimInfoUpdateRegistrantList:Landroid/os/RegistrantList;

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/provider/Telephony$SIMInfo;->getAllSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoList:Ljava/util/List;

    invoke-static {p1}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoList:Ljava/util/List;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoList:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoList:Ljava/util/List;

    if-nez v3, :cond_2

    :cond_0
    const-string v3, "[SIMInfoWrapper] mSimInfoList OR mInsertedSimInfoList is nulll"

    invoke-direct {p0, v3}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->log(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimCount:I

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimCount:I

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoMap:Ljava/util/HashMap;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoMap:Ljava/util/HashMap;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSlotIdSimIdPairs:Ljava/util/HashMap;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSimIdSlotIdPairs:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getCheckedSimId(Landroid/provider/Telephony$SIMInfo;)I

    move-result v2

    if-eq v2, v6, :cond_3

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoMap:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSimIdSlotIdPairs:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v5, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getCheckedSimId(Landroid/provider/Telephony$SIMInfo;)I

    move-result v2

    if-eq v2, v6, :cond_5

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoMap:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSlotIdSimIdPairs:Ljava/util/HashMap;

    iget v4, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSlotIdSimIdPairs:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .param p1    # Ljava/util/HashMap;

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSlotIdSimIdPairs:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Landroid/os/RegistrantList;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSimInfoUpdateRegistrantList:Landroid/os/RegistrantList;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;I)I
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimCount:I

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .param p1    # Ljava/util/HashMap;

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoMap:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic access$400(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSimIdSlotIdPairs:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .param p1    # Ljava/util/HashMap;

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSimIdSlotIdPairs:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic access$500(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Landroid/provider/Telephony$SIMInfo;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .param p1    # Landroid/provider/Telephony$SIMInfo;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getCheckedSimId(Landroid/provider/Telephony$SIMInfo;)I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$802(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;I)I
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimCount:I

    return p1
.end method

.method static synthetic access$900(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$902(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .param p1    # Ljava/util/HashMap;

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoMap:Ljava/util/HashMap;

    return-object p1
.end method

.method private getCheckedSimId(Landroid/provider/Telephony$SIMInfo;)I
    .locals 4
    .param p1    # Landroid/provider/Telephony$SIMInfo;

    if-eqz p1, :cond_0

    iget-wide v0, p1, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p1, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[getCheckedSimId]Wrong simId is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_1

    const-wide/16 v0, -0x1

    :goto_1
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->log(Ljava/lang/String;)V

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    iget-wide v0, p1, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    goto :goto_1
.end method

.method public static getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .locals 2

    sget-boolean v0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mIsNullResult:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->sSIMInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    if-nez v0, :cond_1

    new-instance v0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->sSIMInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    :cond_1
    sget-object v0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->sSIMInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    goto :goto_0
.end method

.method static getSimWrapperInstanceUnCheck()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;
    .locals 1

    sget-boolean v0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mIsNullResult:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->sSIMInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "SIMInfoWrapper"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static setNull(Z)V
    .locals 1
    .param p0    # Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->sSIMInfoWrapper:Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    sput-boolean p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mIsNullResult:Z

    return-void
.end method


# virtual methods
.method public getAllSimCount()I
    .locals 1

    iget v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimCount:I

    return v0
.end method

.method public getAllSimInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoList:Ljava/util/List;

    return-object v0
.end method

.method public getInsertedSimColorById(I)I
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/Telephony$SIMInfo;

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Landroid/provider/Telephony$SIMInfo;->mColor:I

    goto :goto_0
.end method

.method public getInsertedSimCount()I
    .locals 1

    iget v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimCount:I

    return v0
.end method

.method public getInsertedSimDisplayNameById(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/Telephony$SIMInfo;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getInsertedSimInfoById(I)Landroid/provider/Telephony$SIMInfo;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/Telephony$SIMInfo;

    return-object v0
.end method

.method public getInsertedSimInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoList:Ljava/util/List;

    return-object v0
.end method

.method public getInsertedSimInfoMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public getInsertedSimSlotById(I)I
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/Telephony$SIMInfo;

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    goto :goto_0
.end method

.method public getSimBackgroundResByColorId(I)I
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getSimBackgroundResByColorId() colorId = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->log(Ljava/lang/String;)V

    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    sget-object v0, Landroid/provider/Telephony;->SIMBackgroundRes:[I

    aget v0, v0, p1

    return v0
.end method

.method public getSimColorById(I)I
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/Telephony$SIMInfo;

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Landroid/provider/Telephony$SIMInfo;->mColor:I

    goto :goto_0
.end method

.method public getSimDisplayNameById(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/Telephony$SIMInfo;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSimDisplayNameBySlotId(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimIdBySlotId(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimDisplayNameById(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getSimIdBySlotId(I)I
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSlotIdSimIdPairs:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getSimInfoById(I)Landroid/provider/Telephony$SIMInfo;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/Telephony$SIMInfo;

    return-object v0
.end method

.method public getSimInfoBySlot(I)Landroid/provider/Telephony$SIMInfo;
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimCount:I

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget v3, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-ne v3, p1, :cond_0

    move-object v2, v1

    :goto_1
    return-object v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v2, v1

    goto :goto_1
.end method

.method public getSimInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertSim:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mInsertedSimInfoList:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoList:Ljava/util/List;

    goto :goto_0
.end method

.method public getSimInfoMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public getSimSlotById(I)I
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mAllSimInfoMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/Telephony$SIMInfo;

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    goto :goto_0
.end method

.method public getSlotIdBySimId(I)I
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSimIdSlotIdPairs:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public registerForSimInfoUpdate(Landroid/os/Handler;ILjava/lang/Object;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSimInfoUpdateRegistrantList:Landroid/os/RegistrantList;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    return-void
.end method

.method public unregisterForSimInfoUpdate(Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/os/Handler;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->mSimInfoUpdateRegistrantList:Landroid/os/RegistrantList;

    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    return-void
.end method

.method public updateSimInfoCache()V
    .locals 1

    new-instance v0, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper$1;-><init>(Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
