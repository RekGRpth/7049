.class public Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;
.super Ljava/lang/Exception;
.source "ContactsGroupUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/util/ContactsGroupUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "USIMGroupException"
.end annotation


# static fields
.field public static final ERROR_STR_GRP_COUNT_OUTOFBOUND:Ljava/lang/String; = "Group count out of bound"

.field public static final ERROR_STR_GRP_NAME_OUTOFBOUND:Ljava/lang/String; = "Group name out of bound"

.field public static final GROUP_NAME_OUT_OF_BOUND:I = 0x1

.field public static final GROUP_NUMBER_OUT_OF_BOUND:I = 0x2

.field public static final USIM_ERROR_GROUP_COUNT:I = -0x14

.field public static final USIM_ERROR_NAME_LEN:I = -0xa

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field mErrorType:I

.field mSlotId:I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput p2, p0, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;->mErrorType:I

    iput p3, p0, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;->mSlotId:I

    return-void
.end method


# virtual methods
.method public getErrorSlotId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;->mSlotId:I

    return v0
.end method

.method public getErrorType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;->mErrorType:I

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Details message: errorType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;->mErrorType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
