.class public final Lcom/mediatek/contacts/util/ErrorCause;
.super Ljava/lang/Object;
.source "ErrorCause.java"


# static fields
.field public static final ERROR_UNKNOWN:I = 0x1

.field public static final ERROR_USIM_EMAIL_LOST:I = 0x6

.field public static final NO_ERROR:I = 0x0

.field public static final SIM_ADN_LIST_NOT_EXIT:I = -0xb

.field public static final SIM_ANR_TOO_LONG:I = -0x6

.field public static final SIM_GENERIC_FAILURE:I = -0xa

.field public static final SIM_ICC_NOT_READY:I = -0x4

.field public static final SIM_NAME_TOO_LONG:I = -0x2

.field public static final SIM_NOT_READY:I = 0x3

.field public static final SIM_NUMBER_TOO_LONG:I = -0x1

.field public static final SIM_PASSWORD_ERROR:I = -0x5

.field public static final SIM_STORAGE_FULL:I = -0x3

.field public static final USER_CANCEL:I = 0x2

.field public static final USIM_GROUP_NAME_OUT_OF_BOUND:I = 0x4

.field public static final USIM_GROUP_NUMBER_OUT_OF_BOUND:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
