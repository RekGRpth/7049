.class public Lcom/mediatek/contacts/util/TelephonyUtils;
.super Ljava/lang/Object;
.source "TelephonyUtils.java"


# static fields
.field private static final DEFAULT_SIM:I = -0x3

.field private static final USIM:Ljava/lang/String; = "USIM"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get3GCapabilitySIM()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static isRadioOn()Z
    .locals 1

    const/4 v0, -0x3

    invoke-static {v0}, Lcom/mediatek/contacts/util/TelephonyUtils;->isRadioOnInner(I)Z

    move-result v0

    return v0
.end method

.method public static isRadioOn(I)Z
    .locals 2
    .param p0    # I

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v0

    invoke-static {v0}, Lcom/mediatek/contacts/util/TelephonyUtils;->isRadioOnInner(I)Z

    move-result v1

    return v1
.end method

.method public static isRadioOnInner()Z
    .locals 1

    const/4 v0, -0x3

    invoke-static {v0}, Lcom/mediatek/contacts/util/TelephonyUtils;->isRadioOnInner(I)Z

    move-result v0

    return v0
.end method

.method public static isRadioOnInner(I)Z
    .locals 5
    .param p0    # I

    const/4 v1, 0x0

    const-string v4, "phone"

    invoke-static {v4}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v3

    if-nez v3, :cond_0

    move v2, v1

    :goto_0
    return v2

    :cond_0
    const/4 v4, -0x3

    if-ne p0, v4, :cond_1

    :try_start_0
    invoke-interface {v3}, Lcom/android/internal/telephony/ITelephony;->isRadioOn()Z

    move-result v1

    :cond_1
    if-ltz p0, :cond_2

    invoke-interface {v3, p0}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_2
    :goto_1
    move v2, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static isSimInsert()Z
    .locals 1

    const/4 v0, -0x3

    invoke-static {v0}, Lcom/mediatek/contacts/util/TelephonyUtils;->isSimInsertInner(I)Z

    move-result v0

    return v0
.end method

.method public static isSimInsert(I)Z
    .locals 2
    .param p0    # I

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v0

    invoke-static {v0}, Lcom/mediatek/contacts/util/TelephonyUtils;->isSimInsertInner(I)Z

    move-result v1

    return v1
.end method

.method static isSimInsertInner(I)Z
    .locals 1
    .param p0    # I

    const/4 v0, 0x0

    return v0
.end method

.method public static isSimReady()Z
    .locals 1

    const/4 v0, -0x3

    invoke-static {v0}, Lcom/mediatek/contacts/util/TelephonyUtils;->isSimReadyInner(I)Z

    move-result v0

    return v0
.end method

.method public static isSimReady(I)Z
    .locals 2
    .param p0    # I

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getDefault()Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/mediatek/contacts/simcontact/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v0

    invoke-static {v0}, Lcom/mediatek/contacts/util/TelephonyUtils;->isSimReadyInner(I)Z

    move-result v1

    return v1
.end method

.method public static isSimReadyInner(I)Z
    .locals 7
    .param p0    # I

    const/4 v6, 0x5

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    if-nez v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v5, -0x1

    if-ne p0, v5, :cond_1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v5, -0x3

    if-ne p0, v5, :cond_3

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v5

    if-ne v5, v6, :cond_2

    move v0, v3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1

    :cond_3
    invoke-virtual {v2, p0}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v5

    if-ne v5, v6, :cond_4

    move v0, v3

    :goto_2
    goto :goto_1

    :cond_4
    move v0, v4

    goto :goto_2
.end method

.method public static isUSIMInner(I)Z
    .locals 3
    .param p0    # I

    const/4 v0, 0x0

    const-string v2, "phone"

    invoke-static {v2}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-nez v1, :cond_0

    :cond_0
    return v0
.end method
