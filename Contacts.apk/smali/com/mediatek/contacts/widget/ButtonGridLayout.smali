.class public Lcom/mediatek/contacts/widget/ButtonGridLayout;
.super Landroid/widget/FrameLayout;
.source "ButtonGridLayout.java"


# instance fields
.field private final COLUMNS:I

.field private final ROWS:I

.field private mButtonHeight:I

.field private mButtonWidth:I

.field private mHeight:I

.field private mHeightInc:I

.field private mLayouted:Z

.field private mWidthInc:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->COLUMNS:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->ROWS:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mLayouted:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->COLUMNS:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->ROWS:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mLayouted:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->COLUMNS:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->ROWS:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mLayouted:Z

    return-void
.end method


# virtual methods
.method protected createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/graphics/drawable/Drawable;

    new-instance v0, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setId(I)V

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, p3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mButtonWidth:I

    iget v3, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mButtonHeight:I

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09006b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mButtonWidth:I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09006c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mButtonHeight:I

    iget v3, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mButtonWidth:I

    iput v3, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mWidthInc:I

    iget v3, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mButtonHeight:I

    iput v3, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mHeightInc:I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [I

    const v5, 0x101030e

    aput v5, v4, v6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v3, 0x7f07001e

    const v4, 0x7f02003e

    invoke-virtual {p0, v3, v4, v1}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v3, 0x7f07001f

    const v4, 0x7f02003f

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v3, 0x7f070020

    const v4, 0x7f020040

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v3, 0x7f070021

    const v4, 0x7f020041

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v3, 0x7f070022

    const v4, 0x7f020042

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v3, 0x7f070023

    const v4, 0x7f020043

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v3, 0x7f070024

    const v4, 0x7f020044

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v3, 0x7f070025

    const v4, 0x7f020045

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v3, 0x7f070026

    const v4, 0x7f020046

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v3, 0x7f070028

    const v4, 0x7f020048

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v3, 0x7f070027

    const v4, 0x7f02003d

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v3, 0x7f070029

    const v4, 0x7f020047

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/mediatek/contacts/widget/ButtonGridLayout;->createGridButton(IILandroid/graphics/drawable/Drawable;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-boolean v6, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mLayouted:Z

    if-eqz v6, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mLayouted:Z

    const/4 v2, 0x0

    iget v5, p0, Landroid/view/View;->mPaddingTop:I

    const/4 v3, 0x0

    :goto_0
    const/4 v6, 0x4

    if-ge v3, v6, :cond_0

    iget v4, p0, Landroid/view/View;->mPaddingLeft:I

    const/4 v1, 0x0

    :goto_1
    const/4 v6, 0x3

    if-ge v1, v6, :cond_2

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v6, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mButtonWidth:I

    add-int/2addr v6, v4

    iget v7, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mButtonHeight:I

    add-int/2addr v7, v5

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    iget v6, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mWidthInc:I

    add-int/2addr v4, v6

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget v6, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mHeightInc:I

    add-int/2addr v5, v6

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, p0, Lcom/mediatek/contacts/widget/ButtonGridLayout;->mButtonHeight:I

    mul-int/lit8 v1, v4, 0x4

    invoke-virtual {p0, v2, v1}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method
