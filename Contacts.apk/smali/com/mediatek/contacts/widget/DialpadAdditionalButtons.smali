.class public Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;
.super Landroid/widget/FrameLayout;
.source "DialpadAdditionalButtons.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DialpadAdditionalButtons"


# instance fields
.field private mButtonHeight:I

.field private mButtonWidth:I

.field private mDividerHeight:I

.field private mDividerVertical:Landroid/graphics/drawable/Drawable;

.field private mDividerWidth:I

.field private mLayouted:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mLayouted:Z

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09006d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    const v1, 0x7f090085

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonHeight:I

    const v1, 0x7f090073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerHeight:I

    const v1, 0x7f090074

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerWidth:I

    return-void
.end method


# virtual methods
.method protected init()V
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    new-array v7, v10, [I

    const v8, 0x101030e

    aput v8, v7, v9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    new-array v7, v10, [I

    const v8, 0x101030a

    aput v8, v7, v9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerVertical:Landroid/graphics/drawable/Drawable;

    new-instance v0, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    const v6, 0x7f020098

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v6, 0x7f07002a

    invoke-virtual {v0, v6}, Landroid/view/View;->setId(I)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerVertical:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v6}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    const v6, 0x7f020091

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    const v6, 0x7f020012

    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundResource(I)V

    const v6, 0x7f07002b

    invoke-virtual {v0, v6}, Landroid/view/View;->setId(I)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerVertical:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v6}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v2, 0x7f07002c

    const v4, 0x7f0200b2

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v6

    if-eqz v6, :cond_0

    const v2, 0x7f07002d

    const v4, 0x7f020094

    :cond_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->init()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-boolean v2, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mLayouted:Z

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mLayouted:Z

    invoke-virtual {p0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v2, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    iget v3, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonHeight:I

    invoke-virtual {v0, v5, v5, v2, v3}, Landroid/view/View;->layout(IIII)V

    iget v2, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonHeight:I

    iget v3, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerHeight:I

    sub-int/2addr v2, v3

    shr-int/lit8 v1, v2, 0x1

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v2, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    iget v3, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    iget v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerWidth:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerHeight:I

    add-int/2addr v4, v1

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/view/View;->layout(IIII)V

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v2, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    iget v3, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    shl-int/lit8 v3, v3, 0x1

    iget v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonHeight:I

    invoke-virtual {v0, v2, v5, v3, v4}, Landroid/view/View;->layout(IIII)V

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v2, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    shl-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    shl-int/lit8 v3, v3, 0x1

    iget v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerWidth:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerHeight:I

    add-int/2addr v4, v1

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/view/View;->layout(IIII)V

    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v2, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    shl-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    shl-int/lit8 v3, v3, 0x1

    iget v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonHeight:I

    invoke-virtual {v0, v2, v5, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method
