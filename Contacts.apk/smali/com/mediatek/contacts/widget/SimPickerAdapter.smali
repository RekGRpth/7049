.class public Lcom/mediatek/contacts/widget/SimPickerAdapter;
.super Landroid/widget/BaseAdapter;
.source "SimPickerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/widget/SimPickerAdapter$1;,
        Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;,
        Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;
    }
.end annotation


# static fields
.field public static final ITEM_TYPE_ACCOUNT:I = 0x3

.field public static final ITEM_TYPE_INTERNET:I = 0x1

.field public static final ITEM_TYPE_SIM:I = 0x0

.field public static final ITEM_TYPE_TEXT:I = 0x2

.field public static final ITEM_TYPE_UNKNOWN:I = -0x1


# instance fields
.field mContext:Landroid/content/Context;

.field mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;",
            ">;"
        }
    .end annotation
.end field

.field mSingleChoice:Z

.field mSingleChoiceIndex:I

.field mSuggestedSimId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;J)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;",
            ">;J)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mContext:Landroid/content/Context;

    iput-wide p3, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mSuggestedSimId:J

    iput-object p2, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mItems:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mSingleChoice:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mSingleChoiceIndex:I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mItems:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;

    iget v1, v0, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;->type:I

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;->data:Ljava/lang/Object;

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget v1, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget v1, v0, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;->type:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, -0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget v1, v0, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;->type:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    iget v1, v0, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;->type:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    :cond_2
    iget-object v1, v0, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;->data:Ljava/lang/Object;

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mItems:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;

    iget v1, v0, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;->type:I

    return v1
.end method

.method protected getSimStatusIcon(I)I
    .locals 3
    .param p1    # I

    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimIndicatorStateGemini(I)I

    move-result v1

    const/4 v0, 0x0

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const v0, 0x20200e4

    goto :goto_0

    :pswitch_2
    const v0, 0x20200f7

    goto :goto_0

    :pswitch_3
    const v0, 0x20200fc

    goto :goto_0

    :pswitch_4
    const v0, 0x20200fe

    goto :goto_0

    :pswitch_5
    const v0, 0x20200e1

    goto :goto_0

    :pswitch_6
    const v0, 0x20200dc

    goto :goto_0

    :pswitch_7
    const v0, 0x20200fd

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public getSingleChoice()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mSingleChoice:Z

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    move-object v7, p2

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/mediatek/contacts/widget/SimPickerAdapter;->getItemViewType(I)I

    move-result v8

    if-nez v7, :cond_1

    iget-object v9, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    new-instance v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;

    const/4 v9, 0x0

    invoke-direct {v2, p0, v9}, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;-><init>(Lcom/mediatek/contacts/widget/SimPickerAdapter;Lcom/mediatek/contacts/widget/SimPickerAdapter$1;)V

    if-nez v8, :cond_4

    const v9, 0x7f0400b2

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v9, 0x7f070187

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mSimSignal:Landroid/widget/TextView;

    const v9, 0x7f070186

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mSimStatus:Landroid/widget/ImageView;

    const v9, 0x7f070188

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mShortPhoneNumber:Landroid/widget/TextView;

    const v9, 0x7f07018a

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mDisplayName:Landroid/widget/TextView;

    const v9, 0x7f07018b

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mPhoneNumber:Landroid/widget/TextView;

    const v9, 0x7f070185

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mSimIcon:Landroid/view/View;

    const v9, 0x7f070189

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mSuggested:Landroid/widget/TextView;

    const v9, 0x7f07018c

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    :cond_0
    :goto_0
    invoke-virtual {v7, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {v7}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;

    iget-boolean v9, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mSingleChoice:Z

    if-eqz v9, :cond_7

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    if-eqz v9, :cond_7

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    if-nez v8, :cond_d

    iget-object v9, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mItems:Ljava/util/List;

    invoke-interface {v9, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;

    iget-object v5, v9, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;->data:Ljava/lang/Object;

    check-cast v5, Landroid/provider/Telephony$SIMInfo;

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mDisplayName:Landroid/widget/TextView;

    iget-object v10, v5, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mSimIcon:Landroid/view/View;

    sget-object v10, Landroid/provider/Telephony;->SIMBackgroundRes:[I

    iget v11, v5, Landroid/provider/Telephony$SIMInfo;->mColor:I

    aget v10, v10, v11

    invoke-virtual {v9, v10}, Landroid/view/View;->setBackgroundResource(I)V

    iget-wide v9, v5, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    iget-wide v11, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mSuggestedSimId:J

    cmp-long v9, v9, v11

    if-nez v9, :cond_8

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mSuggested:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    :try_start_0
    const-string v4, ""

    iget-object v9, v5, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_b

    iget v9, v5, Landroid/provider/Telephony$SIMInfo;->mDispalyNumberFormat:I

    packed-switch v9, :pswitch_data_0

    :goto_3
    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mPhoneNumber:Landroid/widget/TextView;

    iget-object v10, v5, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mPhoneNumber:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mShortPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mSimSignal:Landroid/widget/TextView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    if-eqz v9, :cond_2

    iget v9, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mSingleChoiceIndex:I

    int-to-long v9, v9

    iget-wide v11, v5, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    cmp-long v9, v9, v11

    if-nez v9, :cond_c

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/CompoundButton;->setChecked(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_5
    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mSimStatus:Landroid/widget/ImageView;

    iget v10, v5, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-virtual {p0, v10}, Lcom/mediatek/contacts/widget/SimPickerAdapter;->getSimStatusIcon(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_3
    :goto_6
    return-object v7

    :cond_4
    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    const v9, 0x7f0400b3

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v9, 0x7f07018d

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mInternetIcon:Landroid/widget/ImageView;

    const v9, 0x7f07018c

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    goto/16 :goto_0

    :cond_5
    const/4 v9, 0x2

    if-eq v8, v9, :cond_6

    const/4 v9, 0x3

    if-ne v8, v9, :cond_0

    :cond_6
    const v9, 0x7f0400b4

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v9, 0x7f0700d1

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mText:Landroid/widget/TextView;

    const v9, 0x7f07018c

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    iput-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    goto/16 :goto_0

    :cond_7
    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_8
    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mSuggested:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :pswitch_0
    :try_start_1
    iget-object v9, v5, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x4

    if-gt v9, v10, :cond_9

    iget-object v4, v5, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    goto/16 :goto_3

    :cond_9
    iget-object v9, v5, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x4

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    :pswitch_1
    iget-object v9, v5, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x4

    if-gt v9, v10, :cond_a

    iget-object v4, v5, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    goto/16 :goto_3

    :cond_a
    iget-object v9, v5, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    iget-object v10, v5, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x4

    iget-object v11, v5, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    :pswitch_2
    const-string v4, ""

    goto/16 :goto_3

    :cond_b
    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mPhoneNumber:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    :catch_0
    move-exception v1

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mShortPhoneNumber:Landroid/widget/TextView;

    const-string v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_c
    :try_start_2
    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/CompoundButton;->setChecked(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_5

    :cond_d
    const/4 v9, 0x1

    if-ne v8, v9, :cond_f

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mInternetIcon:Landroid/widget/ImageView;

    const v10, 0x20200d3

    invoke-virtual {v9, v10}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    if-eqz v9, :cond_3

    iget v9, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mSingleChoiceIndex:I

    const/16 v10, 0x4e24

    if-ne v9, v10, :cond_e

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto/16 :goto_6

    :cond_e
    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto/16 :goto_6

    :cond_f
    const/4 v9, 0x2

    if-ne v8, v9, :cond_11

    iget-object v9, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mItems:Ljava/util/List;

    invoke-interface {v9, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;

    iget-object v6, v9, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;->data:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mText:Landroid/widget/TextView;

    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    if-eqz v9, :cond_3

    iget v9, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mSingleChoiceIndex:I

    const/16 v10, 0x4e25

    if-ne v9, v10, :cond_10

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto/16 :goto_6

    :cond_10
    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto/16 :goto_6

    :cond_11
    const/4 v9, 0x3

    if-ne v8, v9, :cond_3

    iget-object v9, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mItems:Ljava/util/List;

    invoke-interface {v9, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;

    iget-object v0, v9, Lcom/mediatek/contacts/widget/SimPickerAdapter$ItemHolder;->data:Ljava/lang/Object;

    check-cast v0, Landroid/accounts/Account;

    iget-object v9, v2, Lcom/mediatek/contacts/widget/SimPickerAdapter$ViewHolder;->mText:Landroid/widget/TextView;

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public setSingleChoice(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mSingleChoice:Z

    return-void
.end method

.method public setSingleChoiceIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/contacts/widget/SimPickerAdapter;->mSingleChoiceIndex:I

    return-void
.end method
