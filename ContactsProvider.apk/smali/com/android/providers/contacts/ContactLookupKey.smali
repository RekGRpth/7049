.class public Lcom/android/providers/contacts/ContactLookupKey;
.super Ljava/lang/Object;
.source "ContactLookupKey.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;
    }
.end annotation


# static fields
.field public static final LOOKUP_TYPE_DISPLAY_NAME:I = 0x1

.field public static final LOOKUP_TYPE_PROFILE:I = 0x3

.field public static final LOOKUP_TYPE_RAW_CONTACT_ID:I = 0x2

.field public static final LOOKUP_TYPE_SOURCE_ID:I = 0x0

.field public static final PROFILE_LOOKUP_KEY:Ljava/lang/String; = "profile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static appendEscapedSourceId(Ljava/lang/StringBuilder;Ljava/lang/String;)Z
    .locals 4
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x2e

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p0, p1, v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    const-string v3, ".."

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v1, 0x1

    goto :goto_0
.end method

.method public static appendToLookupKey(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    if-nez p6, :cond_0

    const-string p6, ""

    :cond_0
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-static {p1, p2}, Lcom/android/providers/contacts/ContactLookupKey;->getAccountHashCode(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    if-nez p5, :cond_3

    const/16 v1, 0x72

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p6}, Lcom/android/providers/contacts/NameNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/16 v1, 0x69

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p0, p5}, Lcom/android/providers/contacts/ContactLookupKey;->appendEscapedSourceId(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_0
.end method

.method public static getAccountHashCode(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    and-int/lit16 v0, v0, 0xfff

    goto :goto_0
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 22
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;",
            ">;"
        }
    .end annotation

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const-string v19, "profile"

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    new-instance v13, Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;

    invoke-direct {v13}, Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;-><init>()V

    const/16 v19, 0x3

    move/from16 v0, v19

    iput v0, v13, Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;->lookupType:I

    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v9

    :cond_1
    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const/4 v11, 0x0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v6, 0x0

    const/4 v10, -0x1

    const/4 v5, 0x0

    const/4 v14, 0x0

    :goto_0
    if-ge v11, v8, :cond_0

    const/4 v3, 0x0

    const/4 v6, 0x0

    move v12, v11

    :goto_1
    if-ge v12, v8, :cond_18

    add-int/lit8 v11, v12, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v19, 0x30

    move/from16 v0, v19

    if-lt v3, v0, :cond_2

    const/16 v19, 0x39

    move/from16 v0, v19

    if-le v3, v0, :cond_3

    :cond_2
    :goto_2
    const/16 v19, 0x69

    move/from16 v0, v19

    if-ne v3, v0, :cond_4

    const/4 v10, 0x0

    const/4 v5, 0x0

    :goto_3
    packed-switch v10, :pswitch_data_0

    new-instance v19, Ljava/lang/IllegalStateException;

    invoke-direct/range {v19 .. v19}, Ljava/lang/IllegalStateException;-><init>()V

    throw v19

    :cond_3
    mul-int/lit8 v19, v6, 0xa

    add-int/lit8 v20, v3, -0x30

    add-int v6, v19, v20

    move v12, v11

    goto :goto_1

    :cond_4
    const/16 v19, 0x65

    move/from16 v0, v19

    if-ne v3, v0, :cond_5

    const/4 v10, 0x0

    const/4 v5, 0x1

    goto :goto_3

    :cond_5
    const/16 v19, 0x6e

    move/from16 v0, v19

    if-ne v3, v0, :cond_6

    const/4 v10, 0x1

    goto :goto_3

    :cond_6
    const/16 v19, 0x72

    move/from16 v0, v19

    if-ne v3, v0, :cond_7

    const/4 v10, 0x2

    goto :goto_3

    :cond_7
    new-instance v19, Ljava/lang/IllegalArgumentException;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Invalid lookup id: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v19

    :pswitch_0
    if-eqz v5, :cond_c

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    move v12, v11

    :goto_4
    if-ge v12, v8, :cond_a

    add-int/lit8 v11, v12, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v19, 0x2e

    move/from16 v0, v19

    if-ne v3, v0, :cond_9

    if-ne v11, v8, :cond_8

    new-instance v19, Ljava/lang/IllegalArgumentException;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Invalid lookup id: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v19

    :cond_8
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v19, 0x2e

    move/from16 v0, v19

    if-ne v3, v0, :cond_b

    const/16 v19, 0x2e

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v11, v11, 0x1

    move v12, v11

    goto :goto_4

    :cond_9
    invoke-virtual {v15, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v12, v11

    goto :goto_4

    :cond_a
    move v11, v12

    :cond_b
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    :goto_5
    new-instance v16, Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;

    invoke-direct/range {v16 .. v16}, Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;-><init>()V

    move-object/from16 v0, v16

    iput v6, v0, Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;->accountHashCode:I

    move-object/from16 v0, v16

    iput v10, v0, Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;->lookupType:I

    move-object/from16 v0, v16

    iput-object v14, v0, Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;->rawContactId:Ljava/lang/String;

    move-object/from16 v0, v16

    iput-object v7, v0, Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;->key:Ljava/lang/String;

    const-wide/16 v19, -0x1

    move-wide/from16 v0, v19

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/android/providers/contacts/ContactLookupKey$LookupKeySegment;->contactId:J

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_c
    move/from16 v17, v11

    move v12, v11

    :goto_6
    if-ge v12, v8, :cond_17

    add-int/lit8 v11, v12, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v19, 0x2e

    move/from16 v0, v19

    if-ne v3, v0, :cond_16

    :goto_7
    if-ne v11, v8, :cond_d

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    :cond_d
    add-int/lit8 v19, v11, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    :pswitch_1
    move/from16 v17, v11

    move v12, v11

    :goto_8
    if-ge v12, v8, :cond_15

    add-int/lit8 v11, v12, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v19, 0x2e

    move/from16 v0, v19

    if-ne v3, v0, :cond_14

    :goto_9
    if-ne v11, v8, :cond_e

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    :cond_e
    add-int/lit8 v19, v11, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    :pswitch_2
    const/4 v4, -0x1

    move/from16 v17, v11

    :cond_f
    if-ge v11, v8, :cond_11

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v19, 0x2d

    move/from16 v0, v19

    if-ne v3, v0, :cond_10

    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v4, v0, :cond_10

    move v4, v11

    :cond_10
    add-int/lit8 v11, v11, 0x1

    const/16 v19, 0x2e

    move/from16 v0, v19

    if-ne v3, v0, :cond_f

    :cond_11
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v4, v0, :cond_12

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    add-int/lit8 v17, v4, 0x1

    :cond_12
    if-ne v11, v8, :cond_13

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_5

    :cond_13
    add-int/lit8 v19, v11, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_5

    :cond_14
    move v12, v11

    goto :goto_8

    :cond_15
    move v11, v12

    goto :goto_9

    :cond_16
    move v12, v11

    goto/16 :goto_6

    :cond_17
    move v11, v12

    goto/16 :goto_7

    :cond_18
    move v11, v12

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
