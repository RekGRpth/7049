.class public interface abstract Lcom/android/providers/contacts/ContactsDatabaseHelper$ContactsColumns;
.super Ljava/lang/Object;
.source "ContactsDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContactsColumns"
.end annotation


# static fields
.field public static final CONCRETE_CUSTOM_RINGTONE:Ljava/lang/String; = "contacts.custom_ringtone"

.field public static final CONCRETE_FILTER:Ljava/lang/String; = "contacts.filter"

.field public static final CONCRETE_ID:Ljava/lang/String; = "contacts._id"

.field public static final CONCRETE_INDEX_IN_SIM:Ljava/lang/String; = "contacts.index_in_sim"

.field public static final CONCRETE_INDICATE_PHONE_SIM:Ljava/lang/String; = "contacts.indicate_phone_or_sim_contact"

.field public static final CONCRETE_IS_SDN_CONTACT:Ljava/lang/String; = "contacts.is_sdn_contact"

.field public static final CONCRETE_LAST_TIME_CONTACTED:Ljava/lang/String; = "contacts.last_time_contacted"

.field public static final CONCRETE_LOOKUP_KEY:Ljava/lang/String; = "contacts.lookup"

.field public static final CONCRETE_PHOTO_FILE_ID:Ljava/lang/String; = "contacts.photo_file_id"

.field public static final CONCRETE_SEND_TO_VOICEMAIL:Ljava/lang/String; = "contacts.send_to_voicemail"

.field public static final CONCRETE_SEND_TO_VOICEMAIL_SIP:Ljava/lang/String; = "contacts.send_to_voicemail_sip"

.field public static final CONCRETE_SEND_TO_VOICEMAIL_VT:Ljava/lang/String; = "contacts.send_to_voicemail_vt"

.field public static final CONCRETE_STARRED:Ljava/lang/String; = "contacts.starred"

.field public static final CONCRETE_TIMES_CONTACTED:Ljava/lang/String; = "contacts.times_contacted"

.field public static final LAST_STATUS_UPDATE_ID:Ljava/lang/String; = "status_update_id"
