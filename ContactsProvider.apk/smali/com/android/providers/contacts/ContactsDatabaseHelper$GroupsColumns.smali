.class public interface abstract Lcom/android/providers/contacts/ContactsDatabaseHelper$GroupsColumns;
.super Ljava/lang/Object;
.source "ContactsDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GroupsColumns"
.end annotation


# static fields
.field public static final ACCOUNT_ID:Ljava/lang/String; = "account_id"

.field public static final CONCRETE_ACCOUNT_ID:Ljava/lang/String; = "groups.account_id"

.field public static final CONCRETE_ID:Ljava/lang/String; = "groups._id"

.field public static final CONCRETE_PACKAGE_ID:Ljava/lang/String; = "groups.package_id"

.field public static final CONCRETE_SOURCE_ID:Ljava/lang/String; = "groups.sourceid"

.field public static final PACKAGE_ID:Ljava/lang/String; = "package_id"
