.class public interface abstract Lcom/android/providers/contacts/ContactsDatabaseHelper$ViewGroupsColumns;
.super Ljava/lang/Object;
.source "ContactsDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ViewGroupsColumns"
.end annotation


# static fields
.field public static final CONCRETE_ACCOUNT_NAME:Ljava/lang/String; = "view_groups.account_name"

.field public static final CONCRETE_ACCOUNT_TYPE:Ljava/lang/String; = "view_groups.account_type"

.field public static final CONCRETE_DATA_SET:Ljava/lang/String; = "view_groups.data_set"
