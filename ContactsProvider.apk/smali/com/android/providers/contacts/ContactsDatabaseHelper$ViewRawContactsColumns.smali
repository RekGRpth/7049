.class public interface abstract Lcom/android/providers/contacts/ContactsDatabaseHelper$ViewRawContactsColumns;
.super Ljava/lang/Object;
.source "ContactsDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ViewRawContactsColumns"
.end annotation


# static fields
.field public static final CONCRETE_ACCOUNT_NAME:Ljava/lang/String; = "view_raw_contacts.account_name"

.field public static final CONCRETE_ACCOUNT_TYPE:Ljava/lang/String; = "view_raw_contacts.account_type"

.field public static final CONCRETE_DATA_SET:Ljava/lang/String; = "view_raw_contacts.data_set"
