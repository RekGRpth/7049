.class interface abstract Lcom/android/providers/contacts/ContactsProvider2$DsTempTableColumns;
.super Ljava/lang/Object;
.source "ContactsProvider2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsProvider2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "DsTempTableColumns"
.end annotation


# static fields
.field public static final MATCHED_DATA_OFFSETS:Ljava/lang/String; = "matched_data_offsets"

.field public static final MATCHED_OFFSETS_NAME_ALIAS:Ljava/lang/String; = "matched_name_offsets"

.field public static final MATCHED_OFFSETS_NUMBER_ALIAS:Ljava/lang/String; = "matched_number_offsets"

.field public static final NAME_TYPE:Ljava/lang/String; = "name_type"

.field public static final NORMALIZED_NAME:Ljava/lang/String; = "normalized_name"

.field public static final RAW_CONTACT_ID:Ljava/lang/String; = "temp_raw_contact_id"

.field public static final SEARCH_DATA_OFFSETS:Ljava/lang/String; = "search_data_offsets"

.field public static final _ID:Ljava/lang/String; = "temp_id"
