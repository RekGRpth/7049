.class public Lcom/android/providers/contacts/DataRowHandlerForStructuredPostal;
.super Lcom/android/providers/contacts/DataRowHandler;
.source "DataRowHandlerForStructuredPostal.java"


# instance fields
.field private final STRUCTURED_FIELDS:[Ljava/lang/String;

.field private final mSplitter:Lcom/android/providers/contacts/PostalSplitter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/providers/contacts/ContactsDatabaseHelper;Lcom/android/providers/contacts/aggregation/ContactAggregator;Lcom/android/providers/contacts/PostalSplitter;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/providers/contacts/ContactsDatabaseHelper;
    .param p3    # Lcom/android/providers/contacts/aggregation/ContactAggregator;
    .param p4    # Lcom/android/providers/contacts/PostalSplitter;

    const-string v0, "vnd.android.cursor.item/postal-address_v2"

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/providers/contacts/DataRowHandler;-><init>(Landroid/content/Context;Lcom/android/providers/contacts/ContactsDatabaseHelper;Lcom/android/providers/contacts/aggregation/ContactAggregator;Ljava/lang/String;)V

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "data4"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "data5"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "data6"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "data7"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "data8"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "data9"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data10"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/providers/contacts/DataRowHandlerForStructuredPostal;->STRUCTURED_FIELDS:[Ljava/lang/String;

    iput-object p4, p0, Lcom/android/providers/contacts/DataRowHandlerForStructuredPostal;->mSplitter:Lcom/android/providers/contacts/PostalSplitter;

    return-void
.end method

.method private fixStructuredPostalComponents(Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    .locals 8
    .param p1    # Landroid/content/ContentValues;
    .param p2    # Landroid/content/ContentValues;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-string v7, "data1"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    move v3, v5

    :goto_0
    iget-object v7, p0, Lcom/android/providers/contacts/DataRowHandlerForStructuredPostal;->STRUCTURED_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p2, v7}, Lcom/android/providers/contacts/DataRowHandler;->areAllEmpty(Landroid/content/ContentValues;[Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    move v2, v5

    :goto_1
    new-instance v1, Lcom/android/providers/contacts/PostalSplitter$Postal;

    invoke-direct {v1}, Lcom/android/providers/contacts/PostalSplitter$Postal;-><init>()V

    if-eqz v3, :cond_3

    if-nez v2, :cond_3

    iget-object v5, p0, Lcom/android/providers/contacts/DataRowHandlerForStructuredPostal;->mSplitter:Lcom/android/providers/contacts/PostalSplitter;

    invoke-virtual {v5, v1, v4}, Lcom/android/providers/contacts/PostalSplitter;->split(Lcom/android/providers/contacts/PostalSplitter$Postal;Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Lcom/android/providers/contacts/PostalSplitter$Postal;->toValues(Landroid/content/ContentValues;)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v3, v6

    goto :goto_0

    :cond_2
    move v2, v6

    goto :goto_1

    :cond_3
    if-nez v3, :cond_0

    if-nez v2, :cond_4

    iget-object v5, p0, Lcom/android/providers/contacts/DataRowHandlerForStructuredPostal;->STRUCTURED_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p2, v5}, Lcom/android/providers/contacts/DataRowHandler;->areAnySpecified(Landroid/content/ContentValues;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_4
    invoke-virtual {v1, p1}, Lcom/android/providers/contacts/PostalSplitter$Postal;->fromValues(Landroid/content/ContentValues;)V

    iget-object v5, p0, Lcom/android/providers/contacts/DataRowHandlerForStructuredPostal;->mSplitter:Lcom/android/providers/contacts/PostalSplitter;

    invoke-virtual {v5, v1}, Lcom/android/providers/contacts/PostalSplitter;->join(Lcom/android/providers/contacts/PostalSplitter$Postal;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "data1"

    invoke-virtual {p2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public appendSearchableData(Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;)V
    .locals 1
    .param p1    # Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;

    const-string v0, "data1"

    invoke-virtual {p1, v0}, Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;->appendContentFromColumn(Ljava/lang/String;)V

    return-void
.end method

.method public containsSearchableColumns(Landroid/content/ContentValues;)Z
    .locals 1
    .param p1    # Landroid/content/ContentValues;

    const-string v0, "data1"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasSearchableData()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public insert(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;JLandroid/content/ContentValues;)J
    .locals 2
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Lcom/android/providers/contacts/TransactionContext;
    .param p3    # J
    .param p5    # Landroid/content/ContentValues;

    invoke-direct {p0, p5, p5}, Lcom/android/providers/contacts/DataRowHandlerForStructuredPostal;->fixStructuredPostalComponents(Landroid/content/ContentValues;Landroid/content/ContentValues;)V

    invoke-super/range {p0 .. p5}, Lcom/android/providers/contacts/DataRowHandler;->insert(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;JLandroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public update(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;Landroid/content/ContentValues;Landroid/database/Cursor;Z)Z
    .locals 4
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Lcom/android/providers/contacts/TransactionContext;
    .param p3    # Landroid/content/ContentValues;
    .param p4    # Landroid/database/Cursor;
    .param p5    # Z

    const/4 v3, 0x0

    invoke-interface {p4, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {p0, p1, v1, v2, p3}, Lcom/android/providers/contacts/DataRowHandler;->getAugmentedValues(Landroid/database/sqlite/SQLiteDatabase;JLandroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return v3

    :cond_0
    invoke-direct {p0, v0, p3}, Lcom/android/providers/contacts/DataRowHandlerForStructuredPostal;->fixStructuredPostalComponents(Landroid/content/ContentValues;Landroid/content/ContentValues;)V

    invoke-super/range {p0 .. p5}, Lcom/android/providers/contacts/DataRowHandler;->update(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;Landroid/content/ContentValues;Landroid/database/Cursor;Z)Z

    const/4 v3, 0x1

    goto :goto_0
.end method
