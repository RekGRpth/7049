.class interface abstract Lcom/android/providers/contacts/PhotoProcessor$PhotoSizes;
.super Ljava/lang/Object;
.source "PhotoProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/PhotoProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "PhotoSizes"
.end annotation


# static fields
.field public static final DEFAULT_DISPLAY_PHOTO_LARGE_MEMORY:I = 0x2d0

.field public static final DEFAULT_DISPLAY_PHOTO_MEMORY_CONSTRAINED:I = 0x1e0

.field public static final DEFAULT_THUMBNAIL:I = 0x60

.field public static final LARGE_RAM_THRESHOLD:I = 0x28000000

.field public static final SYS_PROPERTY_DISPLAY_PHOTO_SIZE:Ljava/lang/String; = "contacts.display_photo_size"

.field public static final SYS_PROPERTY_THUMBNAIL_SIZE:Ljava/lang/String; = "contacts.thumbnail_size"
