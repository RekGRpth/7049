.class public Lcom/android/providers/contacts/ReorderingCursorWrapper;
.super Landroid/database/AbstractCursor;
.source "ReorderingCursorWrapper.java"


# instance fields
.field private final mCursor:Landroid/database/Cursor;

.field private final mPositionMap:[I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;[I)V
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .param p2    # [I

    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    array-length v1, p2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cursor and position map have different sizes."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    iput-object p2, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mPositionMap:[I

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloat(I)F
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLong(I)J
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getShort(I)S
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public onMove(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/providers/contacts/ReorderingCursorWrapper;->mPositionMap:[I

    aget v1, v1, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method
