.class public Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;
.super Ljava/lang/Object;
.source "CommonNicknameCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache$NicknameLookupQuery;,
        Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache$NicknameLookupPreloadQuery;
    }
.end annotation


# static fields
.field private static final NICKNAME_BLOOM_FILTER_SIZE:I = 0x1fff


# instance fields
.field private final mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mNicknameBloomFilter:Ljava/util/BitSet;

.field private mNicknameClusterCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/SoftReference",
            "<[",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mNicknameClusterCache:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method

.method private preloadNicknameBloomFilter()V
    .locals 13

    const/4 v3, 0x0

    new-instance v0, Ljava/util/BitSet;

    const/16 v1, 0x2000

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mNicknameBloomFilter:Ljava/util/BitSet;

    iget-object v0, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "nickname_lookup"

    sget-object v2, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache$NicknameLookupPreloadQuery;->COLUMNS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v8

    const/4 v11, 0x0

    :goto_0
    if-ge v11, v8, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v10

    iget-object v0, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mNicknameBloomFilter:Ljava/util/BitSet;

    and-int/lit16 v1, v10, 0x1fff

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    return-void

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public getCommonNicknameClusters(Ljava/lang/String;)[Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mNicknameBloomFilter:Ljava/util/BitSet;

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->preloadNicknameBloomFilter()V

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    iget-object v4, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mNicknameBloomFilter:Ljava/util/BitSet;

    and-int/lit16 v6, v2, 0x1fff

    invoke-virtual {v4, v6}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_1

    :goto_0
    return-object v5

    :cond_1
    const/4 v1, 0x0

    iget-object v6, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mNicknameClusterCache:Ljava/util/HashMap;

    monitor-enter v6

    :try_start_0
    iget-object v4, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mNicknameClusterCache:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mNicknameClusterCache:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/SoftReference;

    if-nez v3, :cond_2

    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_2
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, [Ljava/lang/String;

    move-object v1, v0

    :cond_3
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_4

    invoke-virtual {p0, p1}, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->loadNicknameClusters(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    move-object v3, v5

    :goto_1
    iget-object v5, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mNicknameClusterCache:Ljava/util/HashMap;

    monitor-enter v5

    :try_start_2
    iget-object v4, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mNicknameClusterCache:Ljava/util/HashMap;

    invoke-virtual {v4, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_4
    move-object v5, v1

    goto :goto_0

    :cond_5
    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v4
.end method

.method protected loadNicknameClusters(Ljava/lang/String;)[Ljava/lang/String;
    .locals 12
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "nickname_lookup"

    sget-object v2, Lcom/android/providers/contacts/aggregation/util/CommonNicknameCache$NicknameLookupQuery;->COLUMNS:[Ljava/lang/String;

    const-string v3, "name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-lez v9, :cond_0

    new-array v8, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    :goto_0
    if-ge v11, v9, :cond_0

    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    return-object v8

    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0
.end method
