.class public Lcom/android/providers/contacts/aggregation/util/NameDistance;
.super Ljava/lang/Object;
.source "NameDistance.java"


# static fields
.field private static final MIN_EXACT_PREFIX_LENGTH:I = 0x3

.field private static final WINKLER_BONUS_THRESHOLD:F = 0.7f


# instance fields
.field private final mMatchFlags1:[Z

.field private final mMatchFlags2:[Z

.field private final mMaxLength:I

.field private final mPrefixOnly:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mPrefixOnly:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMaxLength:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMatchFlags2:[Z

    iput-object v0, p0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMatchFlags1:[Z

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMaxLength:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mPrefixOnly:Z

    new-array v0, p1, [Z

    iput-object v0, p0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMatchFlags1:[Z

    new-array v0, p1, [Z

    iput-object v0, p0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMatchFlags2:[Z

    return-void
.end method


# virtual methods
.method public getDistance([B[B)F
    .locals 21
    .param p1    # [B
    .param p2    # [B

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_2

    move-object/from16 v4, p1

    move-object/from16 v3, p2

    :goto_0
    array-length v10, v3

    const/16 v18, 0x3

    move/from16 v0, v18

    if-lt v10, v0, :cond_4

    const/4 v14, 0x1

    const/4 v7, 0x0

    :goto_1
    array-length v0, v3

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v7, v0, :cond_0

    aget-byte v18, v3, v7

    aget-byte v19, v4, v7

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_3

    const/4 v14, 0x0

    :cond_0
    if-eqz v14, :cond_4

    const/high16 v9, 0x3f800000

    :cond_1
    :goto_2
    return v9

    :cond_2
    move-object/from16 v4, p2

    move-object/from16 v3, p1

    goto :goto_0

    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mPrefixOnly:Z

    move/from16 v18, v0

    if-eqz v18, :cond_5

    const/4 v9, 0x0

    goto :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMaxLength:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v10, v0, :cond_6

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMaxLength:I

    :cond_6
    array-length v11, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMaxLength:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v11, v0, :cond_7

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMaxLength:I

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMatchFlags1:[Z

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v10, v2}, Ljava/util/Arrays;->fill([ZIIZ)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMatchFlags2:[Z

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v11, v2}, Ljava/util/Arrays;->fill([ZIIZ)V

    div-int/lit8 v18, v11, 0x2

    add-int/lit8 v15, v18, -0x1

    if-gez v15, :cond_8

    const/4 v15, 0x0

    :cond_8
    const/4 v13, 0x0

    const/4 v7, 0x0

    :goto_3
    if-ge v7, v10, :cond_d

    aget-byte v5, v3, v7

    sub-int v6, v7, v15

    if-gez v6, :cond_9

    const/4 v6, 0x0

    :cond_9
    add-int v18, v7, v15

    add-int/lit8 v16, v18, 0x1

    move/from16 v0, v16

    if-le v0, v11, :cond_a

    move/from16 v16, v11

    :cond_a
    move v8, v6

    :goto_4
    move/from16 v0, v16

    if-ge v8, v0, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMatchFlags2:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, v8

    if-nez v18, :cond_c

    aget-byte v18, v4, v8

    move/from16 v0, v18

    if-ne v5, v0, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMatchFlags1:[Z

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMatchFlags2:[Z

    move-object/from16 v19, v0

    const/16 v20, 0x1

    aput-boolean v20, v19, v8

    aput-boolean v20, v18, v7

    add-int/lit8 v13, v13, 0x1

    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_c
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_d
    if-nez v13, :cond_e

    const/4 v9, 0x0

    goto/16 :goto_2

    :cond_e
    const/16 v17, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    :goto_5
    if-ge v7, v10, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMatchFlags1:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, v7

    if-eqz v18, :cond_11

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/aggregation/util/NameDistance;->mMatchFlags2:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, v8

    if-nez v18, :cond_f

    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    :cond_f
    aget-byte v18, v3, v7

    aget-byte v19, v4, v8

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_10

    add-int/lit8 v17, v17, 0x1

    :cond_10
    add-int/lit8 v8, v8, 0x1

    :cond_11
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    :cond_12
    int-to-float v12, v13

    int-to-float v0, v10

    move/from16 v18, v0

    div-float v18, v12, v18

    int-to-float v0, v11

    move/from16 v19, v0

    div-float v19, v12, v19

    add-float v18, v18, v19

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v19, v0

    const/high16 v20, 0x40000000

    div-float v19, v19, v20

    sub-float v19, v12, v19

    div-float v19, v19, v12

    add-float v18, v18, v19

    const/high16 v19, 0x40400000

    div-float v9, v18, v19

    const v18, 0x3f333333

    cmpg-float v18, v9, v18

    if-ltz v18, :cond_1

    const/4 v14, 0x0

    const/4 v7, 0x0

    :goto_7
    if-ge v7, v10, :cond_13

    aget-byte v18, p1, v7

    aget-byte v19, p2, v7

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_14

    :cond_13
    const v18, 0x3dcccccd

    const/high16 v19, 0x3f800000

    int-to-float v0, v11

    move/from16 v20, v0

    div-float v19, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(FF)F

    move-result v18

    int-to-float v0, v14

    move/from16 v19, v0

    mul-float v18, v18, v19

    const/high16 v19, 0x3f800000

    sub-float v19, v19, v9

    mul-float v18, v18, v19

    add-float v9, v9, v18

    goto/16 :goto_2

    :cond_14
    add-int/lit8 v14, v14, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_7
.end method
