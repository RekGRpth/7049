.class public interface abstract Lcom/mediatek/providers/contacts/ContactsFeatureConstants$FeatureOption;
.super Ljava/lang/Object;
.source "ContactsFeatureConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/providers/contacts/ContactsFeatureConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FeatureOption"
.end annotation


# static fields
.field public static final MTK_DIALER_SEARCH_SUPPORT:Z = true

.field public static final MTK_GEMINI_SUPPORT:Z = true

.field public static final MTK_PHONE_NUMBER_GEODESCRIPTION:Z = true

.field public static final MTK_SEARCH_DB_SUPPORT:Z = true
