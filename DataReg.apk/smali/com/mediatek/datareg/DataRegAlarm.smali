.class public Lcom/mediatek/datareg/DataRegAlarm;
.super Ljava/lang/Object;
.source "DataRegAlarm.java"


# static fields
.field public static final ALARM_REGISTER:I = 0x1

.field public static final ALARM_REQUEST:I = 0x2

.field public static final ALARM_WIFI_TIMEOUT:I

.field private static sInstance:Lcom/mediatek/datareg/DataRegAlarm;

.field public static sRegTime:I

.field public static sReqMaxTime:I

.field public static sReqMinTime:I

.field public static sWifiTimeout:I


# instance fields
.field private mAlarmMngr:Landroid/app/AlarmManager;

.field private mContext:Landroid/content/Context;

.field private mPi:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x1e

    sput v0, Lcom/mediatek/datareg/DataRegAlarm;->sWifiTimeout:I

    const/16 v0, 0xa

    sput v0, Lcom/mediatek/datareg/DataRegAlarm;->sReqMaxTime:I

    const/4 v0, 0x5

    sput v0, Lcom/mediatek/datareg/DataRegAlarm;->sReqMinTime:I

    const/16 v0, 0x168

    sput v0, Lcom/mediatek/datareg/DataRegAlarm;->sRegTime:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/datareg/DataRegAlarm;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegAlarm;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/mediatek/datareg/DataRegAlarm;->mAlarmMngr:Landroid/app/AlarmManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mediatek/datareg/store/DataRegStore;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/datareg/store/DataRegStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/datareg/DataRegAlarm;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegAlarm;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/mediatek/datareg/DataRegAlarm;->mAlarmMngr:Landroid/app/AlarmManager;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/mediatek/datareg/DataRegAlarm;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/mediatek/datareg/DataRegAlarm;->sInstance:Lcom/mediatek/datareg/DataRegAlarm;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/datareg/DataRegAlarm;

    invoke-direct {v0, p0}, Lcom/mediatek/datareg/DataRegAlarm;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/datareg/DataRegAlarm;->sInstance:Lcom/mediatek/datareg/DataRegAlarm;

    :cond_0
    sget-object v0, Lcom/mediatek/datareg/DataRegAlarm;->sInstance:Lcom/mediatek/datareg/DataRegAlarm;

    return-object v0
.end method

.method private getRequestTime()J
    .locals 4

    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    sget v1, Lcom/mediatek/datareg/DataRegAlarm;->sReqMinTime:I

    sget v2, Lcom/mediatek/datareg/DataRegAlarm;->sReqMaxTime:I

    sget v3, Lcom/mediatek/datareg/DataRegAlarm;->sReqMinTime:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/2addr v1, v2

    int-to-long v1, v1

    return-wide v1
.end method

.method private setAlarm(Ljava/lang/Class;Ljava/lang/String;IJ)V
    .locals 9
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "IJ)V"
        }
    .end annotation

    const-string v1, "DataReg/ALARM"

    const-string v2, "setAlarm directly.."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "DataReg/ALARM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "DataReg/ALARM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "DataReg/ALARM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reqCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "DataReg/ALARM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " minutes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/mediatek/datareg/DataRegAlarm;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/datareg/DataRegAlarm;->mPi:Landroid/app/PendingIntent;

    if-eqz v1, :cond_0

    const-string v1, "DataReg/ALARM"

    const-string v2, "Cancel previous alarm"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/datareg/DataRegAlarm;->cancelAlarm()V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datareg/DataRegAlarm;->mContext:Landroid/content/Context;

    const/high16 v2, 0x40000000

    invoke-static {v1, p3, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/datareg/DataRegAlarm;->mPi:Landroid/app/PendingIntent;

    iget-object v1, p0, Lcom/mediatek/datareg/DataRegAlarm;->mPi:Landroid/app/PendingIntent;

    if-nez v1, :cond_1

    const-string v1, "DataReg/ALARM"

    const-string v2, "PendingIntent is null!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/datareg/DataRegAlarm;->mAlarmMngr:Landroid/app/AlarmManager;

    const/4 v2, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/16 v5, 0x3c

    mul-long/2addr v5, p4

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    add-long/2addr v3, v5

    iget-object v5, p0, Lcom/mediatek/datareg/DataRegAlarm;->mPi:Landroid/app/PendingIntent;

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method


# virtual methods
.method public cancelAlarm()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegAlarm;->mPi:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegAlarm;->mAlarmMngr:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/mediatek/datareg/DataRegAlarm;->mPi:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datareg/DataRegAlarm;->mPi:Landroid/app/PendingIntent;

    :goto_0
    return-void

    :cond_0
    const-string v0, "DataReg/ALARM"

    const-string v1, "mPi is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setAlarm(I)V
    .locals 6
    .param p1    # I

    const-string v0, "DataReg/ALARM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAlarm() alarm type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    const-string v0, "DataReg/ALARM"

    const-string v1, "do not set any alarm.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "DataReg/ALARM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/DataRegAlarm;->sWifiTimeout:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " minutes alarm to listen wifi state.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-class v1, Lcom/mediatek/datareg/DataRegReceiver;

    const-string v2, "com.mediatek.datareg.WIFI_TIMEOUT"

    sget v0, Lcom/mediatek/datareg/DataRegAlarm;->sWifiTimeout:I

    int-to-long v4, v0

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/datareg/DataRegAlarm;->setAlarm(Ljava/lang/Class;Ljava/lang/String;IJ)V

    goto :goto_0

    :pswitch_1
    const-string v0, "DataReg/ALARM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/DataRegAlarm;->sRegTime:I

    div-int/lit8 v2, v2, 0x3c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hours alarm for next register.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-class v1, Lcom/mediatek/datareg/DataRegReceiver;

    const-string v2, "com.mediatek.datareg.TRY_REG"

    sget v0, Lcom/mediatek/datareg/DataRegAlarm;->sRegTime:I

    int-to-long v4, v0

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/datareg/DataRegAlarm;->setAlarm(Ljava/lang/Class;Ljava/lang/String;IJ)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegAlarm;->getRequestTime()J

    move-result-wide v4

    const-string v0, "DataReg/ALARM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " minutes alarm for next request.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-class v1, Lcom/mediatek/datareg/DataRegReceiver;

    const-string v2, "com.mediatek.datareg.TRY_REQ"

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/datareg/DataRegAlarm;->setAlarm(Ljava/lang/Class;Ljava/lang/String;IJ)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
