.class public final Lcom/mediatek/datareg/DataRegConst$ACTION;
.super Ljava/lang/Object;
.source "DataRegConst.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datareg/DataRegConst;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ACTION"
.end annotation


# static fields
.field public static final CONFIG_PROPERTY:Ljava/lang/String; = "com.mediatek.datareg.config_properties"

.field public static final MOBILE_CONNECTED:Ljava/lang/String; = "com.mediatek.datareg.MOBILE_CONNECTED"

.field public static final RES_TIMEOUT:Ljava/lang/String; = "com.mediatek.datareg.RES_TIMEOUT"

.field public static final SIM_STATE_CHANGED:Ljava/lang/String; = "com.mediatek.datareg.SIM_STATE_CHANGED"

.field public static final TRY_MOBILE:Ljava/lang/String; = "com.mediatek.datareg.TRY_MOBILE"

.field public static final TRY_REG:Ljava/lang/String; = "com.mediatek.datareg.TRY_REG"

.field public static final TRY_REQ:Ljava/lang/String; = "com.mediatek.datareg.TRY_REQ"

.field public static final WIFI_CONNECTED:Ljava/lang/String; = "com.mediatek.datareg.WIFI_CONNECTED"

.field public static final WIFI_TIMEOUT:Ljava/lang/String; = "com.mediatek.datareg.WIFI_TIMEOUT"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
