.class public final Lcom/mediatek/datareg/DataRegConst$TAG;
.super Ljava/lang/Object;
.source "DataRegConst.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datareg/DataRegConst;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TAG"
.end annotation


# static fields
.field public static final ALARM:Ljava/lang/String; = "DataReg/ALARM"

.field public static final CONNECTION:Ljava/lang/String; = "DataReg/CONNECTION"

.field public static final CONTENT:Ljava/lang/String; = "DataReg/ContentHelper"

.field public static final DEVICE:Ljava/lang/String; = "DataReg/DEVICE"

.field public static final ENTITY:Ljava/lang/String; = "DataReg/ENTITY"

.field public static final HTTPS:Ljava/lang/String; = "DataReg/HTTPS"

.field public static final JSON:Ljava/lang/String; = "DataReg/JsonHelper"

.field public static final LISTENER:Ljava/lang/String; = "DataReg/LISTENER"

.field public static final RECEIVER:Ljava/lang/String; = "DataReg/RECEIVER"

.field public static final SCHEDULER:Ljava/lang/String; = "DataReg/SCHEDULER"

.field public static final SERVICE:Ljava/lang/String; = "DataReg/SERVICE"

.field public static final STORE:Ljava/lang/String; = "DataReg/STORE"

.field public static final TASK:Ljava/lang/String; = "DataReg/TASK"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
