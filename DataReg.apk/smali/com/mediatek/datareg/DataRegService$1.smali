.class Lcom/mediatek/datareg/DataRegService$1;
.super Landroid/os/Handler;
.source "DataRegService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datareg/DataRegService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datareg/DataRegService;


# direct methods
.method constructor <init>(Lcom/mediatek/datareg/DataRegService;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const-string v0, "DataReg/SERVICE"

    const-string v1, "handle message.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v0, "DataReg/SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WIFI_CONNECTED_WITHIN_TIMEOUT: Wifi is connected within "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/DataRegAlarm;->sWifiTimeout:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " minutes, start to register.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$100(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/DataRegAlarm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/DataRegAlarm;->cancelAlarm()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$200(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/conn/DataRegConnManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->stopListenWifi()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$300(Lcom/mediatek/datareg/DataRegService;)V

    goto :goto_0

    :pswitch_2
    const-string v0, "DataReg/SERVICE"

    const-string v1, "WIFI_CONNECTED: Wifi is connected, start to register.."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$200(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/conn/DataRegConnManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->stopListenNetwork()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$400(Lcom/mediatek/datareg/DataRegService;)V

    goto :goto_0

    :pswitch_3
    const-string v0, "DataReg/SERVICE"

    const-string v1, "MESSAGE.MOBILE_ENABLE: Mobile is connected, start to register.."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$200(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/conn/DataRegConnManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->stopListenNetwork()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$400(Lcom/mediatek/datareg/DataRegService;)V

    goto :goto_0

    :pswitch_4
    const-string v0, "DataReg/SERVICE"

    const-string v1, "MESSAGE.NO_CONN: there is no connection, start to listen network state.."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$200(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/conn/DataRegConnManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->startListenNetwork()V

    goto :goto_0

    :pswitch_5
    const-string v0, "DataReg/SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MESSAGE.REQ_FAIL: request fail, one more request after "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/DataRegAlarm;->sReqMinTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ~ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/DataRegAlarm;->sReqMaxTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "minutes.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$100(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/DataRegAlarm;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/DataRegAlarm;->setAlarm(I)V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$500(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/store/DataRegStore;->store()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$600(Lcom/mediatek/datareg/DataRegService;)V

    goto/16 :goto_0

    :pswitch_6
    const-string v0, "DataReg/SERVICE"

    const-string v1, "MESSAGE.REG_SUCCESS: request success, store register info and stop service.."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$700(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/device/DataRegDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/device/DataRegDeviceManager;->isSimCardAbsent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$500(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v0

    const-string v1, "reg_flag"

    const-string v2, "no_sim"

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$500(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/store/DataRegStore;->store()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$600(Lcom/mediatek/datareg/DataRegService;)V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    goto/16 :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$500(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v0

    const-string v1, "reg_flag"

    const-string v2, "yes"

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_7
    const-string v0, "DataReg/SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MESSAGE.REG_FAIL: register fail, one more register after "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/DataRegAlarm;->sRegTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " minutes, stop service.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$100(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/DataRegAlarm;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/DataRegAlarm;->setAlarm(I)V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$500(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/store/DataRegStore;->store()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$600(Lcom/mediatek/datareg/DataRegService;)V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    goto/16 :goto_0

    :pswitch_8
    const-string v0, "DataReg/SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MESSAGE.REG_STOP: register "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sRegMax:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " times this boot up, stop service and register next boot up.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$500(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/store/DataRegStore;->store()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$600(Lcom/mediatek/datareg/DataRegService;)V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    goto/16 :goto_0

    :pswitch_9
    const-string v0, "DataReg/SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MESSAGE.TERMINATE: register "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sRegTotal:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " times, never register any more on this phone.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$500(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v0

    const-string v1, "terminate"

    const-string v2, "yes"

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$500(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/store/DataRegStore;->store()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->access$600(Lcom/mediatek/datareg/DataRegService;)V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService$1;->this$0:Lcom/mediatek/datareg/DataRegService;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x320
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method
