.class public Lcom/mediatek/datareg/DataRegService;
.super Landroid/app/Service;
.source "DataRegService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datareg/DataRegService$SimStateReceiver;
    }
.end annotation


# static fields
.field private static final mMinAvailableMemorySize:I = 0x2000

.field private static sInstance:Lcom/mediatek/datareg/DataRegService;

.field public static sTime:Ljava/lang/String;


# instance fields
.field private mAlarm:Lcom/mediatek/datareg/DataRegAlarm;

.field private mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

.field private mDeviceMngr:Lcom/mediatek/datareg/device/DataRegDeviceManager;

.field public mHandler:Landroid/os/Handler;

.field private mSimStateReceiver:Lcom/mediatek/datareg/DataRegService$SimStateReceiver;

.field private mStorer:Lcom/mediatek/datareg/store/DataRegStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ""

    sput-object v0, Lcom/mediatek/datareg/DataRegService;->sTime:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mSimStateReceiver:Lcom/mediatek/datareg/DataRegService$SimStateReceiver;

    new-instance v0, Lcom/mediatek/datareg/DataRegService$1;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mediatek/datareg/DataRegService$1;-><init>(Lcom/mediatek/datareg/DataRegService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/DataRegAlarm;
    .locals 1
    .param p0    # Lcom/mediatek/datareg/DataRegService;

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mAlarm:Lcom/mediatek/datareg/DataRegAlarm;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/conn/DataRegConnManager;
    .locals 1
    .param p0    # Lcom/mediatek/datareg/DataRegService;

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/datareg/DataRegService;)V
    .locals 0
    .param p0    # Lcom/mediatek/datareg/DataRegService;

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->startRegister()V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/datareg/DataRegService;)V
    .locals 0
    .param p0    # Lcom/mediatek/datareg/DataRegService;

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->register()V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/store/DataRegStore;
    .locals 1
    .param p0    # Lcom/mediatek/datareg/DataRegService;

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/datareg/DataRegService;)V
    .locals 0
    .param p0    # Lcom/mediatek/datareg/DataRegService;

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->registerStopRun()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/datareg/DataRegService;)Lcom/mediatek/datareg/device/DataRegDeviceManager;
    .locals 1
    .param p0    # Lcom/mediatek/datareg/DataRegService;

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mDeviceMngr:Lcom/mediatek/datareg/device/DataRegDeviceManager;

    return-object v0
.end method

.method private checkTotalInternalMemorySize()Z
    .locals 12

    const-string v8, "DataReg/SERVICE"

    const-string v9, "check system available memory size.."

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v6

    new-instance v7, Landroid/os/StatFs;

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v4, v8

    invoke-virtual {v7}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v8

    int-to-long v0, v8

    mul-long v8, v0, v4

    const-wide/32 v10, 0xf4240

    sub-long v2, v8, v10

    const-string v8, "DataReg/SERVICE"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "the system available size is: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v8, 0x2000

    cmp-long v8, v2, v8

    if-gez v8, :cond_0

    const-string v8, "DataReg/SERVICE"

    const-string v9, "there is no enough memory for DataReg, we will exit!"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    :goto_0
    return v8

    :cond_0
    const-string v8, "DataReg/SERVICE"

    const-string v9, "there is enough memory for DataReg, we will run!"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x1

    goto :goto_0
.end method

.method private init()V
    .locals 2

    const-string v0, "DataReg/SERVICE"

    const-string v1, "init args.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    invoke-virtual {v0}, Lcom/mediatek/datareg/store/DataRegStore;->initBootCompleted()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "wifi_timeout"

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/datareg/DataRegAlarm;->sWifiTimeout:I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "reg_time"

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/datareg/DataRegAlarm;->sRegTime:I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "req_time_min"

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/datareg/DataRegAlarm;->sReqMinTime:I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "req_time_max"

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/datareg/DataRegAlarm;->sReqMaxTime:I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "reg_total"

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegTotal:I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "reg_max"

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegMax:I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "req_max"

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sReqMax:I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "reg_num"

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "req_num"

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "reg_round"

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegRound:I

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->rememberTime()V

    return-void
.end method

.method private isRegisterAlreadyRun()Z
    .locals 2

    sget v0, Lcom/mediatek/datareg/register/DataRegTask;->sRunningFlag:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private register()V
    .locals 3

    const-string v1, "DataReg/SERVICE"

    const-string v2, "start to register.."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    invoke-virtual {v1}, Lcom/mediatek/datareg/conn/DataRegConnManager;->checkWifi()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->startRegister()V

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    invoke-virtual {v1}, Lcom/mediatek/datareg/conn/DataRegConnManager;->checkMobile()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    invoke-virtual {v1}, Lcom/mediatek/datareg/conn/DataRegConnManager;->isInterRoaming()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/datareg/DataRegService;->mAlarm:Lcom/mediatek/datareg/DataRegAlarm;

    invoke-virtual {v1}, Lcom/mediatek/datareg/DataRegAlarm;->cancelAlarm()V

    iget-object v1, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    invoke-virtual {v1}, Lcom/mediatek/datareg/conn/DataRegConnManager;->startListenNetwork()V

    goto :goto_1
.end method

.method private registerIfNeeded()V
    .locals 3

    const-string v0, "DataReg/SERVICE"

    const-string v1, "registerIfNeeded.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mDeviceMngr:Lcom/mediatek/datareg/device/DataRegDeviceManager;

    invoke-virtual {v0}, Lcom/mediatek/datareg/device/DataRegDeviceManager;->needRegister()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "DataReg/SERVICE"

    const-string v1, "need register.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    invoke-virtual {v0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->checkWifi()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DataReg/SERVICE"

    const-string v1, "Wifi is connected, use Wifi to register.."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->register()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "DataReg/SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wifi is not connected!!! try to detect Wifi "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/DataRegAlarm;->sWifiTimeout:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " minutes.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    invoke-virtual {v0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->startListenToWifi()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mAlarm:Lcom/mediatek/datareg/DataRegAlarm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/datareg/DataRegAlarm;->setAlarm(I)V

    goto :goto_0

    :cond_1
    const-string v0, "DataReg/SERVICE"

    const-string v1, "need not register.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    goto :goto_0
.end method

.method private registerStopRun()V
    .locals 2

    const-string v0, "DataReg/SERVICE"

    const-string v1, "set flag to mark register thread stop.."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/mediatek/datareg/register/DataRegTask;->sRunningFlag:I

    shl-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRunningFlag:I

    return-void
.end method

.method private rememberTime()V
    .locals 3

    const-string v1, "DataReg/SERVICE"

    const-string v2, "remember the first boot time or sim changed time.."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/mediatek/datareg/DataRegService;->sTime:Ljava/lang/String;

    return-void
.end method

.method public static sendMessage(I)V
    .locals 4
    .param p0    # I

    const-string v1, "DataReg/SERVICE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "send message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to service.."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mediatek/datareg/DataRegService;->sInstance:Lcom/mediatek/datareg/DataRegService;

    if-nez v1, :cond_0

    const-string v1, "DataReg/SERVICE"

    const-string v2, "sInstance is null !"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "DataReg/SERVICE"

    sget-object v2, Lcom/mediatek/datareg/DataRegService;->sInstance:Lcom/mediatek/datareg/DataRegService;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mediatek/datareg/DataRegService;->sInstance:Lcom/mediatek/datareg/DataRegService;

    iget-object v1, v1, Lcom/mediatek/datareg/DataRegService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    sget-object v1, Lcom/mediatek/datareg/DataRegService;->sInstance:Lcom/mediatek/datareg/DataRegService;

    iget-object v1, v1, Lcom/mediatek/datareg/DataRegService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private startRegister()V
    .locals 2

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->isRegisterAlreadyRun()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRunningFlag:I

    invoke-static {p0}, Lcom/mediatek/datareg/register/DataRegScheduler;->register(Landroid/content/Context;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "DataReg/SERVICE"

    const-string v1, "There is already an register in progress. Do nothing."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    const-string v0, "DataReg/SERVICE"

    const-string v1, "create data reg service.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sput-object p0, Lcom/mediatek/datareg/DataRegService;->sInstance:Lcom/mediatek/datareg/DataRegService;

    invoke-static {p0}, Lcom/mediatek/datareg/store/DataRegStore;->getStorer(Landroid/content/Context;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    invoke-static {p0}, Lcom/mediatek/datareg/device/DataRegDeviceManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datareg/device/DataRegDeviceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mDeviceMngr:Lcom/mediatek/datareg/device/DataRegDeviceManager;

    invoke-static {p0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datareg/conn/DataRegConnManager;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    new-instance v0, Lcom/mediatek/datareg/DataRegAlarm;

    iget-object v1, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    invoke-direct {v0, p0, v1}, Lcom/mediatek/datareg/DataRegAlarm;-><init>(Landroid/content/Context;Lcom/mediatek/datareg/store/DataRegStore;)V

    iput-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mAlarm:Lcom/mediatek/datareg/DataRegAlarm;

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const-string v0, "DataReg/SERVICE"

    const-string v1, "destroy data reg service.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "reg_round"

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sRegRound:I

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "reg_num"

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v1, "req_num"

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    invoke-virtual {v0}, Lcom/mediatek/datareg/store/DataRegStore;->store()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    invoke-virtual {v0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->stopListenWifi()V

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    invoke-virtual {v0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->stopListenNetwork()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mSimStateReceiver:Lcom/mediatek/datareg/DataRegService$SimStateReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datareg/DataRegService;->mSimStateReceiver:Lcom/mediatek/datareg/DataRegService$SimStateReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/datareg/DataRegService;->sInstance:Lcom/mediatek/datareg/DataRegService;

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v7, 0x2

    const-string v8, "DataReg/SERVICE"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onStartCommand..intent: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "yes"

    iget-object v9, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v10, "terminate"

    invoke-virtual {v9, v10}, Lcom/mediatek/datareg/store/DataRegStore;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const-string v8, "DataReg/RECEIVER"

    const-string v9, "has attemp to register 10 times, will never register again on this phone.."

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v7

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->checkTotalInternalMemorySize()Z

    move-result v8

    if-eqz v8, :cond_0

    if-nez p1, :cond_2

    const-string v8, "DataReg/SERVICE"

    const-string v9, "intent is null!!"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v8, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v7, "DataReg/SERVICE"

    const-string v8, "phone boot complete, start service.."

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->init()V

    new-instance v7, Lcom/mediatek/datareg/DataRegService$SimStateReceiver;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Lcom/mediatek/datareg/DataRegService$SimStateReceiver;-><init>(Lcom/mediatek/datareg/DataRegService$1;)V

    iput-object v7, p0, Lcom/mediatek/datareg/DataRegService;->mSimStateReceiver:Lcom/mediatek/datareg/DataRegService$SimStateReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v7, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/datareg/DataRegService;->mSimStateReceiver:Lcom/mediatek/datareg/DataRegService$SimStateReceiver;

    invoke-virtual {p0, v7, v2}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_3
    :goto_1
    const/4 v7, 0x1

    goto :goto_0

    :cond_4
    const-string v8, "com.mediatek.datareg.WIFI_TIMEOUT"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    const-string v8, "DataReg/SERVICE"

    const-string v9, "Wifi prefer timeout, use any available connection to register.."

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/mediatek/datareg/DataRegService;->mConnMngr:Lcom/mediatek/datareg/conn/DataRegConnManager;

    invoke-virtual {v8}, Lcom/mediatek/datareg/conn/DataRegConnManager;->stopListenWifi()V

    iget-object v8, p0, Lcom/mediatek/datareg/DataRegService;->mDeviceMngr:Lcom/mediatek/datareg/device/DataRegDeviceManager;

    invoke-virtual {v8}, Lcom/mediatek/datareg/device/DataRegDeviceManager;->needRegister()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->register()V

    goto :goto_1

    :cond_5
    const-string v8, "DataReg/SERVICE"

    const-string v9, "Already registered, do nothing"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_6
    const-string v8, "com.mediatek.datareg.WIFI_CONNECTED"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    const-string v7, "DataReg/SERVICE"

    const-string v8, "Wifi connected, use wifi to register.."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->startRegister()V

    goto :goto_1

    :cond_7
    const-string v8, "com.mediatek.datareg.TRY_REG"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    const-string v8, "DataReg/SERVICE"

    const-string v9, "try register one more time.."

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/mediatek/datareg/DataRegService;->mDeviceMngr:Lcom/mediatek/datareg/device/DataRegDeviceManager;

    invoke-virtual {v8}, Lcom/mediatek/datareg/device/DataRegDeviceManager;->needRegister()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->register()V

    goto :goto_1

    :cond_8
    const-string v8, "DataReg/SERVICE"

    const-string v9, "Already registered, do nothing"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_9
    const-string v8, "com.mediatek.datareg.TRY_REQ"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    const-string v8, "DataReg/SERVICE"

    const-string v9, "try a new request.."

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/mediatek/datareg/DataRegService;->mDeviceMngr:Lcom/mediatek/datareg/device/DataRegDeviceManager;

    invoke-virtual {v8}, Lcom/mediatek/datareg/device/DataRegDeviceManager;->needRegister()Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->register()V

    goto :goto_1

    :cond_a
    const-string v8, "DataReg/SERVICE"

    const-string v9, "Already registered, do nothing"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_b
    const-string v7, "com.mediatek.datareg.SIM_STATE_CHANGED"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    const-string v7, "ss"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "simId"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v7, "DataReg/SERVICE"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sim "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " state changed to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/mediatek/datareg/DataRegService;->mDeviceMngr:Lcom/mediatek/datareg/device/DataRegDeviceManager;

    invoke-virtual {v7}, Lcom/mediatek/datareg/device/DataRegDeviceManager;->isSimStateStable()Z

    move-result v7

    if-eqz v7, :cond_c

    const-string v7, "DataReg/SERVICE"

    const-string v8, "SIM state is stable, register if needed"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datareg/DataRegService;->registerIfNeeded()V

    goto/16 :goto_1

    :cond_c
    const-string v7, "DataReg/SERVICE"

    const-string v8, "SIM state is unstable, wait until it\'s stable"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_d
    const-string v7, "com.mediatek.datareg.config_properties"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const-string v7, "Keys"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const-string v7, "Values"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v4, :cond_12

    if-eqz v0, :cond_12

    array-length v7, v4

    array-length v8, v0

    if-ne v7, v8, :cond_12

    const/4 v3, 0x0

    :goto_2
    array-length v7, v4

    if-ge v3, v7, :cond_11

    const-string v7, "DataReg/SERVICE"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ">>>>config property, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v4, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v0, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "addr"

    aget-object v8, v4, v3

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    aget-object v7, v0, v3

    const-string v8, "http://"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_10

    aget-object v7, v0, v3

    :goto_3
    aput-object v7, v0, v3

    :cond_e
    const-string v7, ""

    aget-object v8, v0, v3

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_f

    iget-object v7, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    aget-object v8, v4, v3

    aget-object v9, v0, v3

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_10
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "http://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    :cond_11
    iget-object v7, p0, Lcom/mediatek/datareg/DataRegService;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    invoke-virtual {v7}, Lcom/mediatek/datareg/store/DataRegStore;->store()V

    goto/16 :goto_1

    :cond_12
    const-string v7, "DataReg/SERVICE"

    const-string v8, "keys.length != Vals.length, will not update property"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
