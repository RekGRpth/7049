.class public Lcom/mediatek/datareg/conn/DataRegConnManager;
.super Ljava/lang/Object;
.source "DataRegConnManager.java"


# static fields
.field private static sInstance:Lcom/mediatek/datareg/conn/DataRegConnManager;

.field private static sIsOnlyListeningToWifi:Z


# instance fields
.field private mConnMgr:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mIsListeningToNetwork:Z

.field private mIsListeningToWifi:Z

.field private mNetReceiver:Lcom/mediatek/datareg/conn/DataRegNetworkReceiver;

.field private mTelMngr:Landroid/telephony/TelephonyManager;

.field private mWifiReceiver:Lcom/mediatek/datareg/conn/DataRegWifiReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/datareg/conn/DataRegConnManager;->sIsOnlyListeningToWifi:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/datareg/conn/DataRegConnManager;->sInstance:Lcom/mediatek/datareg/conn/DataRegConnManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mConnMgr:Landroid/net/ConnectivityManager;

    iget-object v0, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mTelMngr:Landroid/telephony/TelephonyManager;

    new-instance v0, Lcom/mediatek/datareg/conn/DataRegNetworkReceiver;

    invoke-direct {v0}, Lcom/mediatek/datareg/conn/DataRegNetworkReceiver;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mNetReceiver:Lcom/mediatek/datareg/conn/DataRegNetworkReceiver;

    new-instance v0, Lcom/mediatek/datareg/conn/DataRegWifiReceiver;

    invoke-direct {v0}, Lcom/mediatek/datareg/conn/DataRegWifiReceiver;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mWifiReceiver:Lcom/mediatek/datareg/conn/DataRegWifiReceiver;

    iput-boolean v2, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mIsListeningToWifi:Z

    iput-boolean v2, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mIsListeningToNetwork:Z

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/mediatek/datareg/conn/DataRegConnManager;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/mediatek/datareg/conn/DataRegConnManager;->sInstance:Lcom/mediatek/datareg/conn/DataRegConnManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/datareg/conn/DataRegConnManager;

    invoke-direct {v0, p0}, Lcom/mediatek/datareg/conn/DataRegConnManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/datareg/conn/DataRegConnManager;->sInstance:Lcom/mediatek/datareg/conn/DataRegConnManager;

    :cond_0
    sget-object v0, Lcom/mediatek/datareg/conn/DataRegConnManager;->sInstance:Lcom/mediatek/datareg/conn/DataRegConnManager;

    return-object v0
.end method

.method public static isOnlyListeningToWifi()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/datareg/conn/DataRegConnManager;->sIsOnlyListeningToWifi:Z

    return v0
.end method

.method private lookupHost(Ljava/lang/String;)I
    .locals 6
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    const/4 v4, 0x3

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    const/4 v5, 0x2

    aget-byte v5, v1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    const/4 v5, 0x1

    aget-byte v5, v1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    const/4 v5, 0x0

    aget-byte v5, v1, v5

    and-int/lit16 v5, v5, 0xff

    or-int v0, v4, v5

    :goto_0
    return v0

    :catch_0
    move-exception v2

    const-string v4, "DataReg/CONNECTION"

    const-string v5, "UnknownHostException!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public checkMobile()Z
    .locals 5

    const-string v3, "DataReg/CONNECTION"

    const-string v4, "checkMobile.."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mConnMgr:Landroid/net/ConnectivityManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->isInterRoaming()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v0, "Mobile is inter roaming, will wait wifi/not roaming connection.."

    :goto_0
    const-string v3, "DataReg/CONNECTION"

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_0
    const-string v0, "Mobile is connected.."

    goto :goto_0

    :cond_1
    const-string v0, "Mobile is not connected.."

    goto :goto_0
.end method

.method public checkWifi()Z
    .locals 5

    const-string v3, "DataReg/CONNECTION"

    const-string v4, "checkWifi.."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mConnMgr:Landroid/net/ConnectivityManager;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "Wifi is connected.."

    :goto_0
    const-string v3, "DataReg/CONNECTION"

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_0
    const-string v0, "Wifi is not connected.."

    goto :goto_0
.end method

.method public ensureRouteToHost(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/datareg/conn/DataRegConnManager;->lookupHost(Ljava/lang/String;)I

    move-result v0

    const-string v1, "DataReg/CONNECTION"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "host:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", address: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->checkWifi()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mConnMgr:Landroid/net/ConnectivityManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mConnMgr:Landroid/net/ConnectivityManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v1

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mConnMgr:Landroid/net/ConnectivityManager;

    invoke-virtual {v4, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    iget-object v4, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mConnMgr:Landroid/net/ConnectivityManager;

    invoke-virtual {v4, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v3, "DataReg/CONNECTION"

    const-string v4, "Wifi is connected.."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v2

    :cond_0
    const-string v4, "DataReg/CONNECTION"

    const-string v5, "Wifi is not connected.."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->isInterRoaming()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v2, "DataReg/CONNECTION"

    const-string v4, "Mobile is inter roaming, will wait wifi/not roaming connection.."

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->startListenNetwork()V

    move v2, v3

    goto :goto_0

    :cond_1
    const-string v3, "DataReg/CONNECTION"

    const-string v4, "Mobile is connected.."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v2, "DataReg/CONNECTION"

    const-string v4, "Mobile is not connected.."

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    goto :goto_0
.end method

.method public isInterRoaming()Z
    .locals 3

    const/4 v0, 0x0

    const-string v1, "460"

    iget-object v2, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mConnMgr:Landroid/net/ConnectivityManager;

    invoke-virtual {v1, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    goto :goto_0
.end method

.method public startListenNetwork()V
    .locals 3

    const-string v1, "DataReg/CONNECTION"

    const-string v2, "start listening mobile inter roaming state.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mIsListeningToNetwork:Z

    if-eqz v1, :cond_0

    const-string v1, "DataReg/CONNECTION"

    const-string v2, "Already listening to mobile, do nothing"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/mediatek/datareg/conn/DataRegConnManager;->sIsOnlyListeningToWifi:Z

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mNetReceiver:Lcom/mediatek/datareg/conn/DataRegNetworkReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mIsListeningToNetwork:Z

    goto :goto_0
.end method

.method public startListenToWifi()V
    .locals 4

    const/4 v3, 0x1

    const-string v1, "DataReg/CONNECTION"

    const-string v2, "start only listen wifi mAction state.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mIsListeningToWifi:Z

    if-eqz v1, :cond_0

    const-string v1, "DataReg/CONNECTION"

    const-string v2, "Already listening to wifi, do nothing"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sput-boolean v3, Lcom/mediatek/datareg/conn/DataRegConnManager;->sIsOnlyListeningToWifi:Z

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mWifiReceiver:Lcom/mediatek/datareg/conn/DataRegWifiReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v3, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mIsListeningToWifi:Z

    goto :goto_0
.end method

.method public stopListenNetwork()V
    .locals 2

    const-string v0, "DataReg/CONNECTION"

    const-string v1, "stop listening mobile inter roaming state.."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mIsListeningToNetwork:Z

    if-nez v0, :cond_0

    const-string v0, "DataReg/CONNECTION"

    const-string v1, "Not listening to mobile, do nothing"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mNetReceiver:Lcom/mediatek/datareg/conn/DataRegNetworkReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mIsListeningToNetwork:Z

    goto :goto_0
.end method

.method public stopListenWifi()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "DataReg/CONNECTION"

    const-string v1, "stop only listening wifi mAction state.."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mIsListeningToWifi:Z

    if-nez v0, :cond_0

    const-string v0, "DataReg/CONNECTION"

    const-string v1, "Not listening to wifi, do nothing"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sput-boolean v2, Lcom/mediatek/datareg/conn/DataRegConnManager;->sIsOnlyListeningToWifi:Z

    iget-object v0, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mWifiReceiver:Lcom/mediatek/datareg/conn/DataRegWifiReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-boolean v2, p0, Lcom/mediatek/datareg/conn/DataRegConnManager;->mIsListeningToWifi:Z

    goto :goto_0
.end method
