.class public Lcom/mediatek/datareg/content/DataRegContentGemini;
.super Lcom/mediatek/datareg/content/DataRegContentHelper;
.source "DataRegContentGemini.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/datareg/content/DataRegContentHelper;-><init>(Landroid/content/Context;)V

    const-string v0, "DataReg/ContentHelper"

    const-string v1, "this is a Gemini content helper.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private getCellIDValue(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    const-string v2, "DataReg/ContentHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCellID1Value..sim id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "null"

    iget-object v2, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2, p1}, Landroid/telephony/TelephonyManager;->getCellLocationGemini(I)Landroid/telephony/CellLocation;

    move-result-object v1

    check-cast v1, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string v2, "DataReg/ContentHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cell ID of SIM1 is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method private getICCIDValue(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, p1}, Landroid/telephony/TelephonyManager;->getSimSerialNumberGemini(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    return-object v0
.end method

.method private getIMEIValue(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->getDeviceIdGemini(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getIMSIValue(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, p1}, Landroid/telephony/TelephonyManager;->getSubscriberIdGemini(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    return-object v0
.end method

.method private getLACValue(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    const-string v2, "DataReg/ContentHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getLAC1Value..sim id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "null"

    iget-object v2, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2, p1}, Landroid/telephony/TelephonyManager;->getCellLocationGemini(I)Landroid/telephony/CellLocation;

    move-result-object v0

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    const-string v2, "DataReg/ContentHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LAC of SIM1 is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1
.end method

.method private getMNCValue(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, p1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorGemini(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    return-object v0
.end method

.method private getMSISDNValue(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, p1}, Landroid/telephony/TelephonyManager;->getLine1NumberGemini(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    return-object v0
.end method

.method private getNCLSValue(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    const-string v1, "null"

    iget-object v2, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2, p1}, Landroid/telephony/TelephonyManager;->getNetworkTypeGemini(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentHelper;->getNetworkClass(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getCellID1Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mMasterSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getCellIDValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCellID2Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mSlaveSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getCellIDValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getICCID1Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mMasterSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getICCIDValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getICCID2Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mSlaveSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getICCIDValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIMEI1Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mMasterSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getIMEIValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIMEI2Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mSlaveSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getIMEIValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIMSI1Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mMasterSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getIMSIValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIMSI2Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mSlaveSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getIMSIValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLAC1Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mMasterSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getLACValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLAC2Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mSlaveSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getLACValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMNC1Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mMasterSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getMNCValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMNC2Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mSlaveSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getMNCValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMSISDN1Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mMasterSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getMSISDNValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMSISDN2Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mSlaveSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getMSISDNValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNCLS1Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mMasterSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getNCLSValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNCLS2Value()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mSlaveSim:I

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentGemini;->getNCLSValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
