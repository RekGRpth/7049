.class public Lcom/mediatek/datareg/content/DataRegContentHelper;
.super Ljava/lang/Object;
.source "DataRegContentHelper.java"


# instance fields
.field protected mConnMngr:Landroid/net/ConnectivityManager;

.field protected mContext:Landroid/content/Context;

.field protected final mDummy:Ljava/lang/String;

.field protected mMasterSim:I

.field protected mSlaveSim:I

.field protected mStorer:Lcom/mediatek/datareg/store/DataRegStore;

.field protected mTelMngr:Landroid/telephony/TelephonyManager;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mMasterSim:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mSlaveSim:I

    const-string v0, "null"

    iput-object v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mDummy:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mConnMngr:Landroid/net/ConnectivityManager;

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/mediatek/datareg/store/DataRegStore;->getStorer(Landroid/content/Context;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    return-void
.end method

.method public static getContentHelper(Landroid/content/Context;)Lcom/mediatek/datareg/content/DataRegContentHelper;
    .locals 1
    .param p0    # Landroid/content/Context;

    new-instance v0, Lcom/mediatek/datareg/content/DataRegContentGemini;

    invoke-direct {v0, p0}, Lcom/mediatek/datareg/content/DataRegContentGemini;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public getAccTypeValue()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mConnMngr:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v1, "WiFi"

    :goto_0
    return-object v1

    :cond_0
    if-nez v0, :cond_1

    const-string v1, "Mobile"

    goto :goto_0

    :cond_1
    const-string v1, "Unknown"

    goto :goto_0
.end method

.method public getCellID1Value()Ljava/lang/String;
    .locals 5

    const-string v2, "DataReg/ContentHelper"

    const-string v3, "getCellID1Value.."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "null"

    iget-object v2, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v1

    check-cast v1, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string v2, "DataReg/ContentHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cell ID of SIM1 is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method public getCellID2Value()Ljava/lang/String;
    .locals 1

    const-string v0, "null"

    return-object v0
.end method

.method public getHWVersionValue()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v2, "HWVersion"

    invoke-virtual {v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public getICCID1Value()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    return-object v0
.end method

.method public getICCID2Value()Ljava/lang/String;
    .locals 1

    const-string v0, "null"

    return-object v0
.end method

.method public getIMEI1Value()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    return-object v0
.end method

.method public getIMEI2Value()Ljava/lang/String;
    .locals 1

    const-string v0, "null"

    return-object v0
.end method

.method public getIMSI1Value()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    return-object v0
.end method

.method public getIMSI2Value()Ljava/lang/String;
    .locals 1

    const-string v0, "null"

    return-object v0
.end method

.method public getLAC1Value()Ljava/lang/String;
    .locals 5

    const-string v2, "DataReg/ContentHelper"

    const-string v3, "getLAC1Value.."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "null"

    iget-object v2, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    const-string v2, "DataReg/ContentHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LAC of SIM1 is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1
.end method

.method public getLAC2Value()Ljava/lang/String;
    .locals 1

    const-string v0, "null"

    return-object v0
.end method

.method public getMNC1Value()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    return-object v0
.end method

.method public getMNC2Value()Ljava/lang/String;
    .locals 1

    const-string v0, "null"

    return-object v0
.end method

.method public getMSISDN1Value()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    return-object v0
.end method

.method public getMSISDN2Value()Ljava/lang/String;
    .locals 1

    const-string v0, "null"

    return-object v0
.end method

.method public getManufValue()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v2, "Manuf"

    invoke-virtual {v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public getModelValue()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v2, "Model"

    invoke-virtual {v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public getNCLS1Value()Ljava/lang/String;
    .locals 3

    const-string v1, "null"

    iget-object v2, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mediatek/datareg/content/DataRegContentHelper;->getNetworkClass(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getNCLS2Value()Ljava/lang/String;
    .locals 1

    const-string v0, "null"

    return-object v0
.end method

.method protected getNetworkClass(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const-string v0, "null"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "GSM"

    goto :goto_0

    :pswitch_1
    const-string v0, "WCDMA"

    goto :goto_0

    :pswitch_2
    const-string v0, "null"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public getOSValue()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v2, "OS"

    invoke-virtual {v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "Android"

    :cond_0
    return-object v0
.end method

.method public getOSVersionValue()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mStorer:Lcom/mediatek/datareg/store/DataRegStore;

    const-string v2, "OSVersion"

    invoke-virtual {v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public getSWVersionValue()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/mediatek/datareg/content/DataRegContentHelper;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceSoftwareVersion()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    return-object v0
.end method

.method public getTimeValue()Ljava/lang/String;
    .locals 3

    const-string v0, "DataReg/ContentHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "********this register time is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/datareg/DataRegService;->sTime:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "*********"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mediatek/datareg/DataRegService;->sTime:Ljava/lang/String;

    return-object v0
.end method
