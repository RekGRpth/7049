.class public Lcom/mediatek/datareg/content/DataRegEntity;
.super Ljava/lang/Object;
.source "DataRegEntity.java"


# instance fields
.field public AccType:Ljava/lang/String;

.field public CellID1:Ljava/lang/String;

.field public CellID2:Ljava/lang/String;

.field public HWVersion:Ljava/lang/String;

.field public ICCID1:Ljava/lang/String;

.field public ICCID2:Ljava/lang/String;

.field public IMEI1:Ljava/lang/String;

.field public IMEI2:Ljava/lang/String;

.field public IMSI1:Ljava/lang/String;

.field public IMSI2:Ljava/lang/String;

.field public LAC1:Ljava/lang/String;

.field public LAC2:Ljava/lang/String;

.field public MNC1:Ljava/lang/String;

.field public MNC2:Ljava/lang/String;

.field public MSISDN1:Ljava/lang/String;

.field public MSISDN2:Ljava/lang/String;

.field public Manuf:Ljava/lang/String;

.field public Model:Ljava/lang/String;

.field public NCLS1:Ljava/lang/String;

.field public NCLS2:Ljava/lang/String;

.field public OS:Ljava/lang/String;

.field public OSVersion:Ljava/lang/String;

.field public SWVersion:Ljava/lang/String;

.field public Time:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->AccType:Ljava/lang/String;

    return-object v0
.end method

.method public getCellID1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->CellID1:Ljava/lang/String;

    return-object v0
.end method

.method public getCellID2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->CellID2:Ljava/lang/String;

    return-object v0
.end method

.method public getHWVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->HWVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getICCID1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->ICCID1:Ljava/lang/String;

    return-object v0
.end method

.method public getICCID2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->ICCID2:Ljava/lang/String;

    return-object v0
.end method

.method public getIMEI1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->IMEI1:Ljava/lang/String;

    return-object v0
.end method

.method public getIMEI2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->IMEI2:Ljava/lang/String;

    return-object v0
.end method

.method public getIMSI1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->IMSI1:Ljava/lang/String;

    return-object v0
.end method

.method public getIMSI2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->IMSI2:Ljava/lang/String;

    return-object v0
.end method

.method public getLAC1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->LAC1:Ljava/lang/String;

    return-object v0
.end method

.method public getLAC2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->LAC2:Ljava/lang/String;

    return-object v0
.end method

.method public getMNC1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->MNC1:Ljava/lang/String;

    return-object v0
.end method

.method public getMNC2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->MNC2:Ljava/lang/String;

    return-object v0
.end method

.method public getMSISDN1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->MSISDN1:Ljava/lang/String;

    return-object v0
.end method

.method public getMSISDN2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->MSISDN2:Ljava/lang/String;

    return-object v0
.end method

.method public getManuf()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->Manuf:Ljava/lang/String;

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->Model:Ljava/lang/String;

    return-object v0
.end method

.method public getNCLS1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->NCLS1:Ljava/lang/String;

    return-object v0
.end method

.method public getNCLS2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->NCLS2:Ljava/lang/String;

    return-object v0
.end method

.method public getOS()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->OS:Ljava/lang/String;

    return-object v0
.end method

.method public getOSVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->OSVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getSWVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->SWVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/content/DataRegEntity;->Time:Ljava/lang/String;

    return-object v0
.end method

.method public setAccType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->AccType:Ljava/lang/String;

    return-void
.end method

.method public setCellID1(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->CellID1:Ljava/lang/String;

    return-void
.end method

.method public setCellID2(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->CellID2:Ljava/lang/String;

    return-void
.end method

.method public setHWVersion(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->HWVersion:Ljava/lang/String;

    return-void
.end method

.method public setICCID1(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->ICCID1:Ljava/lang/String;

    return-void
.end method

.method public setICCID2(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->ICCID2:Ljava/lang/String;

    return-void
.end method

.method public setIMEI1(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->IMEI1:Ljava/lang/String;

    return-void
.end method

.method public setIMEI2(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->IMEI2:Ljava/lang/String;

    return-void
.end method

.method public setIMSI1(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->IMSI1:Ljava/lang/String;

    return-void
.end method

.method public setIMSI2(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->IMSI2:Ljava/lang/String;

    return-void
.end method

.method public setLAC1(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->LAC1:Ljava/lang/String;

    return-void
.end method

.method public setLAC2(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->LAC2:Ljava/lang/String;

    return-void
.end method

.method public setMNC1(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->MNC1:Ljava/lang/String;

    return-void
.end method

.method public setMNC2(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->MNC2:Ljava/lang/String;

    return-void
.end method

.method public setMSISDN1(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->MSISDN1:Ljava/lang/String;

    return-void
.end method

.method public setMSISDN2(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->MSISDN2:Ljava/lang/String;

    return-void
.end method

.method public setManuf(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->Manuf:Ljava/lang/String;

    return-void
.end method

.method public setModel(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->Model:Ljava/lang/String;

    return-void
.end method

.method public setNCLS1(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->NCLS1:Ljava/lang/String;

    return-void
.end method

.method public setNCLS2(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->NCLS2:Ljava/lang/String;

    return-void
.end method

.method public setOS(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->OS:Ljava/lang/String;

    return-void
.end method

.method public setOSVersion(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->OSVersion:Ljava/lang/String;

    return-void
.end method

.method public setSWVersion(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->SWVersion:Ljava/lang/String;

    return-void
.end method

.method public setTime(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datareg/content/DataRegEntity;->Time:Ljava/lang/String;

    return-void
.end method
