.class public abstract Lcom/mediatek/datareg/device/DataRegDeviceManager;
.super Ljava/lang/Object;
.source "DataRegDeviceManager.java"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mStore:Lcom/mediatek/datareg/store/DataRegStore;

.field protected mTelMngr:Landroid/telephony/TelephonyManager;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/datareg/device/DataRegDeviceManager;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/mediatek/datareg/store/DataRegStore;->getStorer(Landroid/content/Context;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datareg/device/DataRegDeviceManager;->mStore:Lcom/mediatek/datareg/store/DataRegStore;

    iget-object v0, p0, Lcom/mediatek/datareg/device/DataRegDeviceManager;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/mediatek/datareg/device/DataRegDeviceManager;->mTelMngr:Landroid/telephony/TelephonyManager;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/mediatek/datareg/device/DataRegDeviceManager;
    .locals 1
    .param p0    # Landroid/content/Context;

    new-instance v0, Lcom/mediatek/datareg/device/DataRegDeviceGemini;

    invoke-direct {v0, p0}, Lcom/mediatek/datareg/device/DataRegDeviceGemini;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public abstract isSimCardAbsent()Z
.end method

.method public abstract isSimStateStable()Z
.end method

.method public abstract needRegister()Z
.end method
