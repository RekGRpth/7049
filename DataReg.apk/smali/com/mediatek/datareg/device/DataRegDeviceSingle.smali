.class public Lcom/mediatek/datareg/device/DataRegDeviceSingle;
.super Lcom/mediatek/datareg/device/DataRegDeviceManager;
.source "DataRegDeviceSingle.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/datareg/device/DataRegDeviceManager;-><init>(Landroid/content/Context;)V

    const-string v0, "DataReg/DEVICE"

    const-string v1, "this is a Single device manager.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private isSimAbsent()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/datareg/device/DataRegDeviceManager;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isSimCardAbsent()Z
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/datareg/device/DataRegDeviceSingle;->isSimAbsent()Z

    move-result v0

    return v0
.end method

.method public isSimStateStable()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/device/DataRegDeviceManager;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needRegister()Z
    .locals 7

    const/4 v3, 0x1

    const-string v4, "DataReg/DEVICE"

    const-string v5, "needRegister Single.."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/datareg/device/DataRegDeviceManager;->mStore:Lcom/mediatek/datareg/store/DataRegStore;

    invoke-virtual {v4}, Lcom/mediatek/datareg/store/DataRegStore;->getRegProperty()Ljava/util/Properties;

    move-result-object v1

    invoke-direct {p0}, Lcom/mediatek/datareg/device/DataRegDeviceSingle;->isSimAbsent()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x0

    :goto_0
    const-string v4, "DataReg/DEVICE"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "currentImsi0: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "reg_flag"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "no"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "DataReg/DEVICE"

    const-string v5, "------haven\'t registered, need register.."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return v3

    :cond_0
    iget-object v4, p0, Lcom/mediatek/datareg/device/DataRegDeviceManager;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v4, "no_sim"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "DataReg/DEVICE"

    const-string v5, "------have registered without sim card.."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/datareg/device/DataRegDeviceSingle;->isSimStateStable()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0}, Lcom/mediatek/datareg/device/DataRegDeviceSingle;->isSimAbsent()Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "DataReg/DEVICE"

    const-string v5, "------here is sim card, need register with sim card.."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    const-string v3, "DataReg/DEVICE"

    const-string v4, "------still no sim card, do not need register.."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    const-string v3, "DataReg/DEVICE"

    const-string v4, "-----have registered with sim card already, do not need register.."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
