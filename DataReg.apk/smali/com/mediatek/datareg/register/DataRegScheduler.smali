.class public Lcom/mediatek/datareg/register/DataRegScheduler;
.super Ljava/lang/Object;
.source "DataRegScheduler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static register(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datareg/conn/DataRegConnManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datareg/conn/DataRegConnManager;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DataReg/SCHEDULER"

    const-string v1, "there is connection now, start register thread.. "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mediatek/datareg/register/DataRegTask;

    invoke-direct {v1, p0}, Lcom/mediatek/datareg/register/DataRegTask;-><init>(Landroid/content/Context;)V

    const-string v2, "DataReg_Task"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "DataReg/SCHEDULER"

    const-string v1, "there is no connection now, wait for connection.."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x322

    invoke-static {v0}, Lcom/mediatek/datareg/DataRegService;->sendMessage(I)V

    goto :goto_0
.end method
