.class public Lcom/mediatek/datareg/register/DataRegTask;
.super Ljava/lang/Object;
.source "DataRegTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final PRI_KEY_FILE:Ljava/lang/String; = "/system/etc/DataReg/DataRegSecrets"

.field public static sRegMax:I

.field public static sRegNum:I

.field public static sRegRound:I

.field public static sRegTotal:I

.field public static sReqMax:I

.field public static sReqNum:I

.field public static sRunningFlag:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mRequester:Lcom/mediatek/datareg/register/DataRegRequester;

.field private mRetCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x3

    const/4 v1, 0x0

    const/4 v0, 0x4

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRunningFlag:I

    const/16 v0, 0xa

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegTotal:I

    sput v1, Lcom/mediatek/datareg/register/DataRegTask;->sRegRound:I

    sput v2, Lcom/mediatek/datareg/register/DataRegTask;->sRegMax:I

    sput v2, Lcom/mediatek/datareg/register/DataRegTask;->sReqMax:I

    const/4 v0, 0x1

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    sput v1, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRetCode:I

    iput-object p1, p0, Lcom/mediatek/datareg/register/DataRegTask;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/mediatek/datareg/register/DataRegRequester;

    iget-object v1, p0, Lcom/mediatek/datareg/register/DataRegTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/mediatek/datareg/register/DataRegRequester;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRequester:Lcom/mediatek/datareg/register/DataRegRequester;

    return-void
.end method

.method private encryptContent([B[B)[B
    .locals 6
    .param p1    # [B
    .param p2    # [B

    const/4 v2, 0x0

    :try_start_0
    const-string v3, "AES/ECB/PKCS5Padding"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    const/4 v3, 0x1

    new-instance v4, Ljavax/crypto/spec/SecretKeySpec;

    const-string v5, "AES"

    invoke-direct {v4, p2, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v1

    const-string v3, "DataReg/TASK"

    const-string v4, "NoSuchMethodException : AES/ECB/PKCS5Padding!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "DataReg/TASK"

    const-string v4, "NoSuchMethodException : AES/ECB/PKCS5Padding!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v3, "DataReg/TASK"

    const-string v4, "InvalidKeyException!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v1

    const-string v3, "DataReg/TASK"

    const-string v4, "IllegalBlockSizeException!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception v1

    const-string v3, "DataReg/TASK"

    const-string v4, "BadPaddingException!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private encryptSign([B)[B
    .locals 6
    .param p1    # [B

    const-string v4, "DataReg/TASK"

    const-string v5, "sign and encrypt.."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datareg/register/DataRegTask;->getEncryptPrivateKey()Ljava/security/PrivateKey;

    move-result-object v2

    const/4 v3, 0x0

    :try_start_0
    const-string v4, "RSA/ECB/PKCS1Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v4, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v3

    :goto_0
    return-object v3

    :catch_0
    move-exception v1

    const-string v4, "DataReg/TASK"

    const-string v5, "InvalidKeyException!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v4, "DataReg/TASK"

    const-string v5, "NoSuchAlgorithmException!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v4, "DataReg/TASK"

    const-string v5, "NoSuchPaddingException!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v1

    const-string v4, "DataReg/TASK"

    const-string v5, "IllegalBlockSizeException!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception v1

    const-string v4, "DataReg/TASK"

    const-string v5, "BadPaddingException!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private getAddress()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lcom/mediatek/datareg/register/DataRegTask;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/mediatek/datareg/store/DataRegStore;->getStorer(Landroid/content/Context;)Lcom/mediatek/datareg/store/DataRegStore;

    move-result-object v1

    const-string v2, "addr"

    invoke-virtual {v1, v2}, Lcom/mediatek/datareg/store/DataRegStore;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "port"

    invoke-virtual {v1, v3}, Lcom/mediatek/datareg/store/DataRegStore;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getContentEntity()Lcom/mediatek/datareg/content/DataRegEntity;
    .locals 19

    const-string v15, "DataReg/TASK"

    const-string v16, "getContentEntity.."

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Lcom/mediatek/datareg/content/DataRegEntity;

    invoke-direct {v4}, Lcom/mediatek/datareg/content/DataRegEntity;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/datareg/register/DataRegTask;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/mediatek/datareg/content/DataRegContentHelper;->getContentHelper(Landroid/content/Context;)Lcom/mediatek/datareg/content/DataRegContentHelper;

    move-result-object v2

    const-class v15, Lcom/mediatek/datareg/content/DataRegEntity;

    invoke-virtual {v15}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v5

    move-object v1, v5

    :try_start_0
    array-length v12, v1

    const/4 v11, 0x0

    :goto_0
    if-ge v11, v12, :cond_0

    aget-object v7, v1, v11

    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v8

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "set"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const-class v15, Lcom/mediatek/datareg/content/DataRegEntity;

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-class v18, Ljava/lang/String;

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v15, v13, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "get"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "Value"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-class v15, Lcom/mediatek/datareg/content/DataRegContentHelper;

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v15, v9, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-virtual {v10, v2, v15}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "DataReg/TASK"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "field name:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", value:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v14, v15, v16

    invoke-virtual {v6, v4, v15}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v3

    const-string v15, "DataReg/TASK"

    const-string v16, "NoSuchMethodException!!!"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_1
    return-object v4

    :catch_1
    move-exception v3

    const-string v15, "DataReg/TASK"

    const-string v16, "IllegalArgumentException!!!"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v3

    const-string v15, "DataReg/TASK"

    const-string v16, "IllegalAccessException!!!"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v3

    const-string v15, "DataReg/TASK"

    const-string v16, "InvocationTargetException!!!"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method private getEncryptPrivateKey()Ljava/security/PrivateKey;
    .locals 9

    const/4 v5, 0x0

    :try_start_0
    new-instance v3, Ljava/io/File;

    const-string v7, "/system/etc/DataReg/DataRegSecrets"

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v7

    long-to-int v7, v7

    new-array v2, v7, [B

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I

    new-instance v6, Ljava/security/spec/PKCS8EncodedKeySpec;

    invoke-direct {v6, v2}, Ljava/security/spec/PKCS8EncodedKeySpec;-><init>([B)V

    const-string v7, "RSA"

    invoke-static {v7}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    return-object v5

    :catch_0
    move-exception v0

    const-string v7, "DataReg/TASK"

    const-string v8, "FileNotFoundException!!! File:/system/etc/DataReg/DataRegSecrets"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v7, "DataReg/TASK"

    const-string v8, "RSA NoSuchAlgorithmException!!!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v7, "DataReg/TASK"

    const-string v8, "InvalidKeySpecException!!!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v0

    const-string v7, "DataReg/TASK"

    const-string v8, "IOException!!!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private getMD5Sign([B)[B
    .locals 5
    .param p1    # [B

    const/4 v2, 0x0

    :try_start_0
    const-string v3, "MD5"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const-string v3, "DataReg/TASK"

    const-string v4, "MD5 NoSuchAlgorithmException!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private getServerErrorString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const-string v0, "Unknown"

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "response time exceed 10s"

    goto :goto_0

    :sswitch_1
    const-string v0, "Success"

    goto :goto_0

    :sswitch_2
    const-string v0, "Invalid model or sign in post url"

    goto :goto_0

    :sswitch_3
    const-string v0, "Model does not exist in database"

    goto :goto_0

    :sswitch_4
    const-string v0, "Failed to decode sign"

    goto :goto_0

    :sswitch_5
    const-string v0, "Failed to decode AES key"

    goto :goto_0

    :sswitch_6
    const-string v0, "Invalid request content"

    goto :goto_0

    :sswitch_7
    const-string v0, "Failed to decrypt AES key with RSA key"

    goto :goto_0

    :sswitch_8
    const-string v0, "Digital digest does not match key"

    goto :goto_0

    :sswitch_9
    const-string v0, "Some mandatory info are absent"

    goto :goto_0

    :sswitch_a
    const-string v0, "Failed to get public key"

    goto :goto_0

    :sswitch_b
    const-string v0, "Failed to decrypt with AES key"

    goto :goto_0

    :sswitch_c
    const-string v0, "Server runtime error"

    goto :goto_0

    :sswitch_d
    const-string v0, "Server faked error response"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0xc8 -> :sswitch_1
        0x190 -> :sswitch_2
        0x19a -> :sswitch_3
        0x19b -> :sswitch_4
        0x19c -> :sswitch_5
        0x19d -> :sswitch_6
        0x19e -> :sswitch_7
        0x19f -> :sswitch_8
        0x1a0 -> :sswitch_9
        0x1fe -> :sswitch_a
        0x1ff -> :sswitch_b
        0x200 -> :sswitch_c
        0x257 -> :sswitch_d
    .end sparse-switch
.end method

.method private handleRequestError()V
    .locals 3

    const-string v0, "DataReg/TASK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleRequestError: error code is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRetCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRetCode:I

    if-lez v0, :cond_0

    const-string v0, "DataReg/TASK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error string: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRetCode:I

    invoke-direct {p0, v2}, Lcom/mediatek/datareg/register/DataRegTask;->getServerErrorString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget v0, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    sget v1, Lcom/mediatek/datareg/register/DataRegTask;->sReqMax:I

    if-lt v0, v1, :cond_3

    const-string v0, "DataReg/TASK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "==========>Regist Num:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Request Num:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " register fail this boot up"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    sget v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegRound:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegRound:I

    const-string v0, "DataReg/TASK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "==========>total register round on this phone:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sRegRound:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegRound:I

    sget v1, Lcom/mediatek/datareg/register/DataRegTask;->sRegTotal:I

    if-lt v0, v1, :cond_1

    const-string v0, "DataReg/TASK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "==========>has reached total register times: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sRegTotal:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " on this phone, CU Self Reg terminate.. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x32a

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/register/DataRegTask;->notifyService(I)V

    :goto_0
    return-void

    :cond_1
    sget v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    sget v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegMax:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    sget v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    sget v1, Lcom/mediatek/datareg/register/DataRegTask;->sRegMax:I

    if-le v0, v1, :cond_2

    const-string v0, "DataReg/TASK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "==========>has reached max register times: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sRegMax:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", stop CU Self Reg.. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput v0, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    const/16 v0, 0x324

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/register/DataRegTask;->notifyService(I)V

    goto :goto_0

    :cond_2
    const-string v0, "DataReg/TASK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "try another register "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/DataRegAlarm;->sRegTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " minutes later.. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x325

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/register/DataRegTask;->notifyService(I)V

    goto :goto_0

    :cond_3
    const-string v0, "DataReg/TASK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "==========>Regist Num:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Request Num:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fail, send request later.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x326

    invoke-direct {p0, v0}, Lcom/mediatek/datareg/register/DataRegTask;->notifyService(I)V

    goto/16 :goto_0
.end method

.method private static hexdump([B)V
    .locals 20
    .param p0    # [B

    const/16 v10, 0x10

    const/4 v12, 0x3

    const/4 v11, 0x7

    const/16 v13, 0xb

    const/16 v18, 0x12

    move/from16 v0, v18

    new-array v15, v0, [B

    const/16 v18, 0x4

    move/from16 v0, v18

    new-array v2, v0, [C

    const/16 v18, 0x8

    move/from16 v0, v18

    new-array v6, v0, [C

    const/16 v18, 0x14

    move/from16 v0, v18

    new-array v3, v0, [C

    const/16 v18, 0x0

    const/16 v19, 0x30

    aput-char v19, v3, v18

    const/16 v18, 0x1

    const/16 v19, 0x31

    aput-char v19, v3, v18

    const/16 v18, 0x2

    const/16 v19, 0x32

    aput-char v19, v3, v18

    const/16 v18, 0x3

    const/16 v19, 0x33

    aput-char v19, v3, v18

    const/16 v18, 0x4

    const/16 v19, 0x34

    aput-char v19, v3, v18

    const/16 v18, 0x5

    const/16 v19, 0x35

    aput-char v19, v3, v18

    const/16 v18, 0x6

    const/16 v19, 0x36

    aput-char v19, v3, v18

    const/16 v18, 0x7

    const/16 v19, 0x37

    aput-char v19, v3, v18

    const/16 v18, 0x8

    const/16 v19, 0x38

    aput-char v19, v3, v18

    const/16 v18, 0x9

    const/16 v19, 0x39

    aput-char v19, v3, v18

    const/16 v18, 0xa

    const/16 v19, 0x41

    aput-char v19, v3, v18

    const/16 v18, 0xb

    const/16 v19, 0x42

    aput-char v19, v3, v18

    const/16 v18, 0xc

    const/16 v19, 0x43

    aput-char v19, v3, v18

    const/16 v18, 0xd

    const/16 v19, 0x44

    aput-char v19, v3, v18

    const/16 v18, 0xe

    const/16 v19, 0x45

    aput-char v19, v3, v18

    const/16 v18, 0xf

    const/16 v19, 0x46

    aput-char v19, v3, v18

    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v18, v0

    shr-int/lit8 v14, v18, 0x4

    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v18, v0

    and-int/lit8 v9, v18, 0xf

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v14, :cond_5

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    mul-int/lit8 v4, v5, 0x10

    const/16 v18, 0x0

    shr-int/lit8 v19, v4, 0xc

    and-int/lit8 v19, v19, 0xf

    aget-char v19, v3, v19

    aput-char v19, v6, v18

    const/16 v18, 0x1

    shr-int/lit8 v19, v4, 0x8

    and-int/lit8 v19, v19, 0xf

    aget-char v19, v3, v19

    aput-char v19, v6, v18

    const/16 v18, 0x2

    shr-int/lit8 v19, v4, 0x4

    and-int/lit8 v19, v19, 0xf

    aget-char v19, v3, v19

    aput-char v19, v6, v18

    const/16 v18, 0x3

    and-int/lit8 v19, v4, 0xf

    aget-char v19, v3, v19

    aput-char v19, v6, v18

    new-instance v7, Ljava/lang/String;

    const/16 v18, 0x0

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v7, v6, v0, v1}, Ljava/lang/String;-><init>([CII)V

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ": "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    :goto_1
    const/16 v18, 0x10

    move/from16 v0, v18

    if-ge v8, v0, :cond_4

    mul-int/lit8 v18, v5, 0x10

    add-int v18, v18, v8

    aget-byte v18, p0, v18

    aput-byte v18, v15, v8

    const/16 v18, 0x0

    aget-byte v19, v15, v8

    shr-int/lit8 v19, v19, 0x4

    and-int/lit8 v19, v19, 0xf

    aget-char v19, v3, v19

    aput-char v19, v2, v18

    const/16 v18, 0x1

    aget-byte v19, v15, v8

    and-int/lit8 v19, v19, 0xf

    aget-char v19, v3, v19

    aput-char v19, v2, v18

    const/16 v18, 0x0

    aget-char v18, v2, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v18, 0x1

    aget-char v18, v2, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v18, 0x20

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v18, 0x3

    move/from16 v0, v18

    if-eq v8, v0, :cond_0

    const/16 v18, 0x7

    move/from16 v0, v18

    if-eq v8, v0, :cond_0

    const/16 v18, 0xb

    move/from16 v0, v18

    if-ne v8, v0, :cond_1

    :cond_0
    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    aget-byte v18, v15, v8

    const/16 v19, 0x20

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_2

    aget-byte v18, v15, v8

    const/16 v19, 0x7e

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_3

    :cond_2
    const/16 v18, 0x2e

    aput-byte v18, v15, v8

    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_4
    new-instance v16, Ljava/lang/String;

    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-direct {v0, v15, v1, v8}, Ljava/lang/String;-><init>([BII)V

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " | "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " |"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v18, "DataReg/TASK"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :cond_5
    if-lez v9, :cond_e

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    mul-int/lit8 v4, v5, 0x10

    const/16 v18, 0x0

    shr-int/lit8 v19, v4, 0xc

    and-int/lit8 v19, v19, 0xf

    aget-char v19, v3, v19

    aput-char v19, v6, v18

    const/16 v18, 0x1

    shr-int/lit8 v19, v4, 0x8

    and-int/lit8 v19, v19, 0xf

    aget-char v19, v3, v19

    aput-char v19, v6, v18

    const/16 v18, 0x2

    shr-int/lit8 v19, v4, 0x4

    and-int/lit8 v19, v19, 0xf

    aget-char v19, v3, v19

    aput-char v19, v6, v18

    const/16 v18, 0x3

    and-int/lit8 v19, v4, 0xf

    aget-char v19, v3, v19

    aput-char v19, v6, v18

    new-instance v7, Ljava/lang/String;

    const/16 v18, 0x0

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v7, v6, v0, v1}, Ljava/lang/String;-><init>([CII)V

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ": "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v9, :cond_a

    mul-int/lit8 v18, v5, 0x10

    add-int v18, v18, v8

    aget-byte v18, p0, v18

    aput-byte v18, v15, v8

    const/16 v18, 0x0

    aget-byte v19, v15, v8

    shr-int/lit8 v19, v19, 0x4

    and-int/lit8 v19, v19, 0xf

    aget-char v19, v3, v19

    aput-char v19, v2, v18

    const/16 v18, 0x1

    aget-byte v19, v15, v8

    and-int/lit8 v19, v19, 0xf

    aget-char v19, v3, v19

    aput-char v19, v2, v18

    const/16 v18, 0x0

    aget-char v18, v2, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v18, 0x1

    aget-char v18, v2, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v18, 0x20

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v18, 0x3

    move/from16 v0, v18

    if-eq v8, v0, :cond_6

    const/16 v18, 0x7

    move/from16 v0, v18

    if-eq v8, v0, :cond_6

    const/16 v18, 0xb

    move/from16 v0, v18

    if-ne v8, v0, :cond_7

    :cond_6
    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    aget-byte v18, v15, v8

    const/16 v19, 0x20

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_8

    aget-byte v18, v15, v8

    const/16 v19, 0x7e

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_9

    :cond_8
    const/16 v18, 0x2e

    aput-byte v18, v15, v8

    :cond_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_a
    :goto_3
    const/16 v18, 0x10

    move/from16 v0, v18

    if-ge v8, v0, :cond_d

    const/16 v18, 0x20

    aput-byte v18, v15, v8

    const-string v18, "   "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v18, 0x3

    move/from16 v0, v18

    if-eq v8, v0, :cond_b

    const/16 v18, 0x7

    move/from16 v0, v18

    if-eq v8, v0, :cond_b

    const/16 v18, 0xb

    move/from16 v0, v18

    if-ne v8, v0, :cond_c

    :cond_b
    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    :cond_d
    new-instance v16, Ljava/lang/String;

    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-direct {v0, v15, v1, v8}, Ljava/lang/String;-><init>([BII)V

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " | "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " |"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v18, "DataReg/TASK"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    return-void
.end method

.method private notifyService(I)V
    .locals 0
    .param p1    # I

    invoke-static {p1}, Lcom/mediatek/datareg/DataRegService;->sendMessage(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    const/4 v11, 0x0

    const/4 v8, 0x1

    sput v8, Lcom/mediatek/datareg/register/DataRegTask;->sRunningFlag:I

    const-string v8, "DataReg/TASK"

    const-string v9, "request thread start running.."

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget v8, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    add-int/lit8 v8, v8, 0x1

    sput v8, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    const-string v8, "DataReg/TASK"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "==========>total register round on this phone:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/mediatek/datareg/register/DataRegTask;->sRegRound:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "DataReg/TASK"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "==========>this boot up register time:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", request time:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datareg/register/DataRegTask;->getContentEntity()Lcom/mediatek/datareg/content/DataRegEntity;

    move-result-object v7

    new-instance v8, Lcom/mediatek/datareg/content/DataRegJSONHelper;

    invoke-direct {v8}, Lcom/mediatek/datareg/content/DataRegJSONHelper;-><init>()V

    invoke-virtual {v8, v7}, Lcom/mediatek/datareg/content/DataRegJSONHelper;->wrapFromBean(Lcom/mediatek/datareg/content/DataRegEntity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const-string v8, "DataReg/TASK"

    const-string v9, "Content: json(register_info)"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v2}, Lcom/mediatek/datareg/register/DataRegTask;->hexdump([B)V

    invoke-direct {p0, v2}, Lcom/mediatek/datareg/register/DataRegTask;->getMD5Sign([B)[B

    move-result-object v1

    const-string v8, "DataReg/TASK"

    const-string v9, "MD5 sign used as key: MD5(json(register_info))"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Lcom/mediatek/datareg/register/DataRegTask;->hexdump([B)V

    invoke-direct {p0, v1}, Lcom/mediatek/datareg/register/DataRegTask;->encryptSign([B)[B

    move-result-object v6

    const-string v8, "DataReg/TASK"

    const-string v9, "Encrypted sign: RSA(MD5(json(register_info)), pri_rsa_key)"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v6}, Lcom/mediatek/datareg/register/DataRegTask;->hexdump([B)V

    invoke-static {v6, v11}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v5

    const-string v8, "DataReg/TASK"

    const-string v9, "Encrypted sign in Base64: Base64(RSA(MD5(json(register_info)), pri_rsa_key))"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "DataReg/TASK"

    invoke-static {v8, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2, v1}, Lcom/mediatek/datareg/register/DataRegTask;->encryptContent([B[B)[B

    move-result-object v4

    const-string v8, "DataReg/TASK"

    const-string v9, "Encrypted content: AES(json(register_info), MD5(json(register_info)))"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v4}, Lcom/mediatek/datareg/register/DataRegTask;->hexdump([B)V

    invoke-static {v4, v11}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    const-string v8, "DataReg/TASK"

    const-string v9, "Encrypted content in Base64: Base64(AES(json(register_info), MD5(json(register_info))))"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "DataReg/TASK"

    invoke-static {v8, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRequester:Lcom/mediatek/datareg/register/DataRegRequester;

    invoke-direct {p0}, Lcom/mediatek/datareg/register/DataRegTask;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/mediatek/datareg/register/DataRegRequester;->setAddress(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRequester:Lcom/mediatek/datareg/register/DataRegRequester;

    invoke-virtual {v7}, Lcom/mediatek/datareg/content/DataRegEntity;->getModel()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/mediatek/datareg/register/DataRegRequester;->setModel(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRequester:Lcom/mediatek/datareg/register/DataRegRequester;

    invoke-virtual {v8, v5}, Lcom/mediatek/datareg/register/DataRegRequester;->setSign(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRequester:Lcom/mediatek/datareg/register/DataRegRequester;

    invoke-virtual {v8, v4}, Lcom/mediatek/datareg/register/DataRegRequester;->setContent([B)V

    iget-object v8, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRequester:Lcom/mediatek/datareg/register/DataRegRequester;

    invoke-virtual {v8}, Lcom/mediatek/datareg/register/DataRegRequester;->request()I

    move-result v8

    iput v8, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRetCode:I

    const-string v8, "DataReg/TASK"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "HTTP response code from server is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRetCode:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v8, p0, Lcom/mediatek/datareg/register/DataRegTask;->mRetCode:I

    const/16 v9, 0xc8

    if-ne v8, v9, :cond_0

    const-string v8, "DataReg/TASK"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "==========>Regist Num:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Request Num:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " success, ask service to store the register.."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v8, 0x323

    invoke-direct {p0, v8}, Lcom/mediatek/datareg/register/DataRegTask;->notifyService(I)V

    :goto_0
    sget v8, Lcom/mediatek/datareg/register/DataRegTask;->sRunningFlag:I

    shl-int/lit8 v8, v8, 0x1

    sput v8, Lcom/mediatek/datareg/register/DataRegTask;->sRunningFlag:I

    const-string v8, "DataReg/TASK"

    const-string v9, "request thread stop running.."

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/datareg/register/DataRegTask;->handleRequestError()V

    goto :goto_0
.end method
