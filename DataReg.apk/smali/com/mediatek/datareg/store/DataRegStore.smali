.class public abstract Lcom/mediatek/datareg/store/DataRegStore;
.super Ljava/lang/Object;
.source "DataRegStore.java"


# static fields
.field public static final ADDR:Ljava/lang/String; = "addr"

.field public static final HWVERSION:Ljava/lang/String; = "HWVersion"

.field public static final IMSI0:Ljava/lang/String; = "imsi0"

.field public static final IMSI1:Ljava/lang/String; = "imsi1"

.field public static final MANUF:Ljava/lang/String; = "Manuf"

.field public static final MODEL:Ljava/lang/String; = "Model"

.field public static final OS:Ljava/lang/String; = "OS"

.field public static final OSVERSION:Ljava/lang/String; = "OSVersion"

.field public static final PORT:Ljava/lang/String; = "port"

.field public static final REG_FLAG:Ljava/lang/String; = "reg_flag"

.field public static final REG_MAX:Ljava/lang/String; = "reg_max"

.field public static final REG_NUM:Ljava/lang/String; = "reg_num"

.field public static final REG_ROUND:Ljava/lang/String; = "reg_round"

.field public static final REG_TIME:Ljava/lang/String; = "reg_time"

.field public static final REG_TOTAL:Ljava/lang/String; = "reg_total"

.field public static final REQ_MAX:Ljava/lang/String; = "req_max"

.field public static final REQ_NUM:Ljava/lang/String; = "req_num"

.field public static final REQ_TIME_MAX:Ljava/lang/String; = "req_time_max"

.field public static final REQ_TIME_MIN:Ljava/lang/String; = "req_time_min"

.field protected static final STORE_FILE:Ljava/lang/String; = "DataReg.properties"

.field public static final SWVERSION:Ljava/lang/String; = "SWVersion"

.field public static final TERMINATE:Ljava/lang/String; = "terminate"

.field public static final WIFI_TIMEOUT:Ljava/lang/String; = "wifi_timeout"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mDefaultIntPropertyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mDefaultStringPropertyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mRegProperty:Ljava/util/Properties;

.field protected mTelMngr:Landroid/telephony/TelephonyManager;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/mediatek/datareg/store/DataRegStore;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-direct {p0}, Lcom/mediatek/datareg/store/DataRegStore;->fetchDefaultProperty()V

    invoke-direct {p0}, Lcom/mediatek/datareg/store/DataRegStore;->loadProperty()V

    return-void
.end method

.method private createProperty()V
    .locals 2

    const-string v0, "DataReg/STORE"

    const-string v1, "createProperty.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datareg/store/DataRegStore;->initProperty()V

    invoke-direct {p0}, Lcom/mediatek/datareg/store/DataRegStore;->saveProperty()V

    return-void
.end method

.method private fetchDefaultProperty()V
    .locals 6

    const/16 v5, 0xa

    const/4 v4, 0x3

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "terminate"

    const-string v2, "no"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "reg_flag"

    const-string v2, "no"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "imsi0"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "imsi1"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "addr"

    const-string v2, "http://114.247.0.45"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "port"

    const-string v2, "6002"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "Manuf"

    const-string v2, "Mediatek"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "Model"

    const-string v2, "MediatekTestModel01"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "HWVersion"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "SWVersion"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "OS"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    const-string v1, "OSVersion"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "wifi_timeout"

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "reg_time"

    const/16 v2, 0x168

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "req_time_min"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "req_time_max"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "reg_total"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "reg_round"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "reg_max"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "req_max"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "reg_num"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "req_num"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static getStorer(Landroid/content/Context;)Lcom/mediatek/datareg/store/DataRegStore;
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/mediatek/datareg/store/DataRegStoreGemini;->getInstance(Landroid/content/Context;)Lcom/mediatek/datareg/store/DataRegStoreGemini;

    move-result-object v0

    return-object v0
.end method

.method private initProperty()V
    .locals 6

    const-string v4, "DataReg/STORE"

    const-string v5, "initProperty.."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v4, "/system/etc/DataReg/DataRegDefault.properties"

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v3}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mRegProperty:Ljava/util/Properties;

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    const-string v4, "DataReg/STORE"

    const-string v5, "Default properties loaded."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    const-string v4, "DataReg/STORE"

    const-string v5, "Failed to load default properties."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "DataReg/STORE"

    const-string v5, "Use hardcoded values"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "terminate"

    const-string v5, "no"

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "reg_flag"

    const-string v5, "no"

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "imsi0"

    const-string v5, ""

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "imsi1"

    const-string v5, ""

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "addr"

    sget-object v5, Lcom/mediatek/datareg/register/DataRegRequester;->sAddr:Ljava/lang/String;

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "port"

    sget-object v5, Lcom/mediatek/datareg/register/DataRegRequester;->sPort:Ljava/lang/String;

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "wifi_timeout"

    sget v5, Lcom/mediatek/datareg/DataRegAlarm;->sWifiTimeout:I

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v4, "req_time_min"

    sget v5, Lcom/mediatek/datareg/DataRegAlarm;->sReqMinTime:I

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v4, "req_time_max"

    sget v5, Lcom/mediatek/datareg/DataRegAlarm;->sReqMaxTime:I

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v4, "reg_time"

    sget v5, Lcom/mediatek/datareg/DataRegAlarm;->sRegTime:I

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v4, "reg_total"

    sget v5, Lcom/mediatek/datareg/register/DataRegTask;->sRegTotal:I

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v4, "reg_round"

    sget v5, Lcom/mediatek/datareg/register/DataRegTask;->sRegRound:I

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v4, "req_max"

    sget v5, Lcom/mediatek/datareg/register/DataRegTask;->sReqMax:I

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v4, "reg_max"

    sget v5, Lcom/mediatek/datareg/register/DataRegTask;->sRegMax:I

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v4, "req_num"

    sget v5, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v4, "reg_num"

    sget v5, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v4, "Manuf"

    const-string v5, ""

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "Model"

    const-string v5, ""

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "HWVersion"

    const-string v5, ""

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "SWVersion"

    const-string v5, ""

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "OS"

    const-string v5, ""

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "OSVersion"

    const-string v5, ""

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private loadProperty()V
    .locals 4

    const-string v2, "DataReg/STORE"

    const-string v3, "loadProperty.."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    iput-object v2, p0, Lcom/mediatek/datareg/store/DataRegStore;->mRegProperty:Ljava/util/Properties;

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "DataReg/STORE"

    const-string v3, "load properties from file:DataReg.properties"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/datareg/store/DataRegStore;->mContext:Landroid/content/Context;

    const-string v3, "DataReg.properties"

    invoke-virtual {v2, v3}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/datareg/store/DataRegStore;->mRegProperty:Ljava/util/Properties;

    invoke-virtual {v2, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "DataReg/STORE"

    const-string v3, "property file not exist, create a new one.."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datareg/store/DataRegStore;->createProperty()V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "DataReg/STORE"

    const-string v3, "IOException when load property file!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private saveProperty()V
    .locals 5

    const-string v2, "DataReg/STORE"

    const-string v3, "saveProperty.."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/datareg/store/DataRegStore;->mContext:Landroid/content/Context;

    const-string v3, "DataReg.properties"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/datareg/store/DataRegStore;->mRegProperty:Ljava/util/Properties;

    const-string v3, "DataReg"

    invoke-virtual {v2, v1, v3}, Ljava/util/Properties;->store(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "DataReg/STORE"

    const-string v3, "FileNotFoundException when save property file!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "DataReg/STORE"

    const-string v3, "IOException when save property file!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getIntProperty(Ljava/lang/String;)I
    .locals 6
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/datareg/store/DataRegStore;->mRegProperty:Ljava/util/Properties;

    const-string v4, ""

    invoke-virtual {v3, p1, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DataReg/STORE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'s value is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v3, "DataReg/STORE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "can not get Int value of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from DataReg.properties"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultIntPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getRegProperty()Ljava/util/Properties;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mRegProperty:Ljava/util/Properties;

    return-object v0
.end method

.method public getStringProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/datareg/store/DataRegStore;->mRegProperty:Ljava/util/Properties;

    const-string v2, ""

    invoke-virtual {v1, p1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DataReg/STORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'s value is : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string v1, "DataReg/STORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can not get String value of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from DataReg.properties"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/datareg/store/DataRegStore;->mDefaultStringPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_1
    return-object v0
.end method

.method public initBootCompleted()V
    .locals 5

    const-string v3, "DataReg/STORE"

    const-string v4, "initBootComplete.."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "req_num"

    invoke-virtual {p0, v3}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    const-string v3, "DataReg/STORE"

    const-string v4, "do not finish 3 requests in last register process, but still consider the register process complete.."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "reg_round"

    invoke-virtual {p0, v3}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v1

    const-string v3, "reg_round"

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p0, v3, v4}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v3, "reg_total"

    invoke-virtual {p0, v3}, Lcom/mediatek/datareg/store/DataRegStore;->getIntProperty(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0xa

    :cond_0
    add-int/lit8 v3, v1, 0x1

    if-ne v3, v2, :cond_1

    const-string v3, "terminate"

    const-string v4, "yes"

    invoke-virtual {p0, v3, v4}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v3, "req_num"

    const-string v4, "0"

    invoke-virtual {p0, v3, v4}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "reg_num"

    const-string v4, "1"

    invoke-virtual {p0, v3, v4}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/datareg/store/DataRegStore;->saveProperty()V

    return-void
.end method

.method public declared-synchronized setProperty(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mRegProperty:Ljava/util/Properties;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/datareg/store/DataRegStore;->mRegProperty:Ljava/util/Properties;

    invoke-virtual {v0, p1, p2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setRegProperty(Ljava/util/Properties;)V
    .locals 0
    .param p1    # Ljava/util/Properties;

    iput-object p1, p0, Lcom/mediatek/datareg/store/DataRegStore;->mRegProperty:Ljava/util/Properties;

    return-void
.end method

.method public declared-synchronized store()V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "DataReg/STORE"

    const-string v1, "store.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "reg_round"

    sget v1, Lcom/mediatek/datareg/register/DataRegTask;->sRegRound:I

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v0, "req_num"

    sget v1, Lcom/mediatek/datareg/register/DataRegTask;->sReqNum:I

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    const-string v0, "reg_num"

    sget v1, Lcom/mediatek/datareg/register/DataRegTask;->sRegNum:I

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/datareg/store/DataRegStore;->setProperty(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/mediatek/datareg/store/DataRegStore;->storeProperty()V

    invoke-direct {p0}, Lcom/mediatek/datareg/store/DataRegStore;->saveProperty()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract storeProperty()V
.end method
