.class public Lcom/mediatek/datareg/store/DataRegStoreSingle;
.super Lcom/mediatek/datareg/store/DataRegStore;
.source "DataRegStoreSingle.java"


# static fields
.field private static mInstance:Lcom/mediatek/datareg/store/DataRegStoreSingle;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/datareg/store/DataRegStore;-><init>(Landroid/content/Context;)V

    const-string v0, "DataReg/STORE"

    const-string v1, "this is DataRegStoreSingle.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/mediatek/datareg/store/DataRegStoreSingle;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/mediatek/datareg/store/DataRegStoreSingle;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/datareg/store/DataRegStoreSingle;->mInstance:Lcom/mediatek/datareg/store/DataRegStoreSingle;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/datareg/store/DataRegStoreSingle;

    invoke-direct {v0, p0}, Lcom/mediatek/datareg/store/DataRegStoreSingle;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/datareg/store/DataRegStoreSingle;->mInstance:Lcom/mediatek/datareg/store/DataRegStoreSingle;

    :cond_0
    sget-object v0, Lcom/mediatek/datareg/store/DataRegStoreSingle;->mInstance:Lcom/mediatek/datareg/store/DataRegStoreSingle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected storeProperty()V
    .locals 5

    iget-object v2, p0, Lcom/mediatek/datareg/store/DataRegStore;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const-string v2, "DataReg/STORE"

    const-string v3, "SIM is absent, do not store its IMSI"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/datareg/store/DataRegStore;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, ""

    :cond_1
    iget-object v2, p0, Lcom/mediatek/datareg/store/DataRegStore;->mTelMngr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :cond_2
    const-string v2, "DataReg/STORE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIM NUM:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", IMSI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/datareg/store/DataRegStore;->mRegProperty:Ljava/util/Properties;

    const-string v3, "imsi0"

    invoke-virtual {v2, v3, v0}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_0
.end method
