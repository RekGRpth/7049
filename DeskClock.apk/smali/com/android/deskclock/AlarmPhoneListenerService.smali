.class public Lcom/android/deskclock/AlarmPhoneListenerService;
.super Landroid/app/Service;
.source "AlarmPhoneListenerService.java"


# static fields
.field private static final DELAY_START_ALARM:I = 0x384

.field private static final GIMINI_SIM_1:I = 0x0

.field private static final GIMINI_SIM_2:I = 0x1


# instance fields
.field private mAlarm:Lcom/android/deskclock/Alarm;

.field private mCurrentCallState:I

.field private mCurrentCallState1:I

.field private mCurrentCallState2:I

.field private mHandler:Landroid/os/Handler;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPhoneStateListener1:Landroid/telephony/PhoneStateListener;

.field private mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mTelephonyService:Lcom/android/internal/telephony/ITelephony;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/deskclock/AlarmPhoneListenerService$1;

    invoke-direct {v0, p0}, Lcom/android/deskclock/AlarmPhoneListenerService$1;-><init>(Lcom/android/deskclock/AlarmPhoneListenerService;)V

    iput-object v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/android/deskclock/AlarmPhoneListenerService$2;

    invoke-direct {v0, p0}, Lcom/android/deskclock/AlarmPhoneListenerService$2;-><init>(Lcom/android/deskclock/AlarmPhoneListenerService;)V

    iput-object v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mPhoneStateListener1:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/android/deskclock/AlarmPhoneListenerService$3;

    invoke-direct {v0, p0}, Lcom/android/deskclock/AlarmPhoneListenerService$3;-><init>(Lcom/android/deskclock/AlarmPhoneListenerService;)V

    iput-object v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/deskclock/AlarmPhoneListenerService;)I
    .locals 1
    .param p0    # Lcom/android/deskclock/AlarmPhoneListenerService;

    iget v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mCurrentCallState:I

    return v0
.end method

.method static synthetic access$100(Lcom/android/deskclock/AlarmPhoneListenerService;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/AlarmPhoneListenerService;

    invoke-direct {p0}, Lcom/android/deskclock/AlarmPhoneListenerService;->sendStartAlarmBroadcast()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/deskclock/AlarmPhoneListenerService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/deskclock/AlarmPhoneListenerService;

    iget-object v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/deskclock/AlarmPhoneListenerService;)I
    .locals 1
    .param p0    # Lcom/android/deskclock/AlarmPhoneListenerService;

    iget v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mCurrentCallState1:I

    return v0
.end method

.method static synthetic access$400(Lcom/android/deskclock/AlarmPhoneListenerService;)I
    .locals 1
    .param p0    # Lcom/android/deskclock/AlarmPhoneListenerService;

    iget v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mCurrentCallState2:I

    return v0
.end method

.method private sendStartAlarmBroadcast()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.deskclock.ALARM_ALERT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "setNextAlert"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mAlarm:Lcom/android/deskclock/Alarm;

    iput-boolean v2, v1, Lcom/android/deskclock/Alarm;->vibrate:Z

    iget-object v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mAlarm:Lcom/android/deskclock/Alarm;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/android/deskclock/Alarm;->silent:Z

    const-string v1, "intent.extra.alarm"

    iget-object v2, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mAlarm:Lcom/android/deskclock/Alarm;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "AlarmPhoneListenerService sendStartAlarmBroadcast"

    invoke-static {v1}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    const/16 v3, 0x21

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    iget-object v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mPhoneStateListener1:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    iget-object v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    return-void
.end method

.method public onDestroy()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mPhoneStateListener1:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v3, v3}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    iget-object v0, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v1, "intent.extra.alarm"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/android/deskclock/Alarm;

    iput-object v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mAlarm:Lcom/android/deskclock/Alarm;

    iget-object v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mAlarm:Lcom/android/deskclock/Alarm;

    if-nez v1, :cond_0

    const-string v1, "AlarmKlaxon failed to parse the alarm from the intent"

    invoke-static {v1}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    const/4 v1, 0x2

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getPreciseCallState()I

    move-result v1

    iput v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mCurrentCallState:I

    iget-object v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/telephony/TelephonyManager;->getCallStateGemini(I)I

    move-result v1

    iput v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mCurrentCallState1:I

    iget-object v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/telephony/TelephonyManager;->getCallStateGemini(I)I

    move-result v1

    iput v1, p0, Lcom/android/deskclock/AlarmPhoneListenerService;->mCurrentCallState2:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v1, 0x3

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Catch exception when getPreciseCallState: ex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    goto :goto_1
.end method
