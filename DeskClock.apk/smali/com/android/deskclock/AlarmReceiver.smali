.class public Lcom/android/deskclock/AlarmReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AlarmReceiver.java"


# static fields
.field static final ALARM_PHONE_LISTENER:Ljava/lang/String; = "com.android.deskclock.ALARM_PHONE_LISTENER"

.field private static final GIMINI_SIM_1:I = 0x0

.field private static final GIMINI_SIM_2:I = 0x1

.field private static final STALE_WINDOW:I = 0x1b7740

.field private static final VIBRATE_LENGTH:I = 0x3e8


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentCallState:I

.field private mTelephonyService:Lcom/android/internal/telephony/ITelephony;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/deskclock/AlarmReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/AlarmReceiver;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-direct {p0, p1, p2}, Lcom/android/deskclock/AlarmReceiver;->handleIntent(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private getNotificationManager(Landroid/content/Context;)Landroid/app/NotificationManager;
    .locals 1
    .param p1    # Landroid/content/Context;

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    return-object v0
.end method

.method private handleIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 27
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v23, "alarm_killed"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1

    const-string v23, "snoozed"

    const/16 v24, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_0

    const-string v23, "intent.extra.alarm"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/android/deskclock/Alarm;

    const-string v24, "alarm_killed_timeout"

    const/16 v25, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v24

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    move/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lcom/android/deskclock/AlarmReceiver;->updateNotification(Landroid/content/Context;Lcom/android/deskclock/Alarm;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v23, "cancel_snooze"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    const/4 v4, 0x0

    const-string v23, "intent.extra.alarm"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_2

    const-string v23, "intent.extra.alarm"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/android/deskclock/Alarm;

    :cond_2
    if-eqz v4, :cond_3

    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-static {v0, v1}, Lcom/android/deskclock/Alarms;->disableSnoozeAlert(Landroid/content/Context;I)V

    invoke-static/range {p1 .. p1}, Lcom/android/deskclock/Alarms;->setNextAlert(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    const-string v23, "Unable to parse Alarm from intent."

    invoke-static/range {v23 .. v23}, Lcom/android/deskclock/Log;->wtf(Ljava/lang/String;)V

    const/16 v23, -0x1

    const-wide/16 v24, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v23

    move-wide/from16 v2, v24

    invoke-static {v0, v1, v2, v3}, Lcom/android/deskclock/Alarms;->saveSnoozeAlert(Landroid/content/Context;IJ)V

    goto :goto_0

    :cond_4
    const-string v23, "com.android.deskclock.ALARM_ALERT"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_0

    const/4 v4, 0x0

    const-string v23, "setNextAlert"

    const/16 v24, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v23

    if-eqz v23, :cond_a

    const-string v23, "intent.extra.alarm_raw"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v11

    const/16 v23, 0x0

    array-length v0, v8

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v11, v8, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    sget-object v23, Lcom/android/deskclock/Alarm;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, v23

    invoke-interface {v0, v11}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/deskclock/Alarm;

    :cond_5
    if-nez v4, :cond_6

    const-string v23, "Failed to parse the alarm from the intent"

    invoke-static/range {v23 .. v23}, Lcom/android/deskclock/Log;->wtf(Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Lcom/android/deskclock/Alarms;->setNextAlert(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_6
    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-static {v0, v1}, Lcom/android/deskclock/Alarms;->disableSnoozeAlert(Landroid/content/Context;I)V

    iget-object v0, v4, Lcom/android/deskclock/Alarm;->daysOfWeek:Lcom/android/deskclock/Alarm$DaysOfWeek;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/android/deskclock/Alarm$DaysOfWeek;->isRepeatSet()Z

    move-result v23

    if-nez v23, :cond_9

    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/android/deskclock/Alarms;->enableAlarm(Landroid/content/Context;IZ)V

    :cond_7
    :goto_1
    :try_start_0
    const-string v23, "phone"

    invoke-static/range {v23 .. v23}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/deskclock/AlarmReceiver;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/AlarmReceiver;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/ITelephony;->getPreciseCallState()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/deskclock/AlarmReceiver;->mCurrentCallState:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/AlarmReceiver;->mCurrentCallState:I

    move/from16 v23, v0

    if-eqz v23, :cond_8

    new-instance v20, Landroid/content/Intent;

    const-string v23, "com.android.deskclock.ALARM_PHONE_LISTENER"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v23, "intent.extra.alarm"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const-string v23, "vibrator"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/os/Vibrator;

    if-eqz v22, :cond_8

    const-string v23, "vibrator starts,and vibrates:1000 ms"

    invoke-static/range {v23 .. v23}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    const-wide/16 v23, 0x3e8

    invoke-virtual/range {v22 .. v24}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_8
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Recevied alarm set for "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    iget-wide v0, v4, Lcom/android/deskclock/Alarm;->time:J

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Lcom/android/deskclock/Log;->formatTime(J)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    iget-wide v0, v4, Lcom/android/deskclock/Alarm;->time:J

    move-wide/from16 v23, v0

    const-wide/32 v25, 0x1b7740

    add-long v23, v23, v25

    cmp-long v23, v17, v23

    if-lez v23, :cond_b

    const-string v23, "Ignoring stale alarm"

    invoke-static/range {v23 .. v23}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    invoke-static/range {p1 .. p1}, Lcom/android/deskclock/Alarms;->setNextAlert(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_a
    const-string v23, "intent.extra.alarm"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_7

    const-string v23, "intent.extra.alarm"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/android/deskclock/Alarm;

    goto/16 :goto_1

    :catch_0
    move-exception v9

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Catch exception when getPreciseCallState: ex = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    goto :goto_2

    :cond_b
    invoke-static/range {p1 .. p1}, Lcom/android/deskclock/AlarmAlertWakeLock;->acquireCpuWakeLock(Landroid/content/Context;)V

    new-instance v7, Landroid/content/Intent;

    const-string v23, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    move-object/from16 v0, v23

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-class v6, Lcom/android/deskclock/AlarmAlert;

    const-string v23, "keyguard"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/app/KeyguardManager;

    invoke-virtual {v12}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v23

    if-eqz v23, :cond_c

    const-class v6, Lcom/android/deskclock/AlarmAlertFullScreen;

    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/AlarmReceiver;->mCurrentCallState:I

    move/from16 v23, v0

    if-nez v23, :cond_d

    new-instance v21, Landroid/content/Intent;

    const-string v23, "com.android.deskclock.ALARM_ALERT"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v23, "intent.extra.alarm"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_d
    new-instance v16, Landroid/content/Intent;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v23, "intent.extra.alarm"

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move-object/from16 v2, v16

    move/from16 v3, v24

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v19

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/android/deskclock/Alarm;->getLabelOrDefault(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Landroid/app/Notification;

    const v23, 0x7f02003c

    iget-wide v0, v4, Lcom/android/deskclock/Alarm;->time:J

    move-wide/from16 v24, v0

    move/from16 v0, v23

    move-wide/from16 v1, v24

    invoke-direct {v14, v0, v13, v1, v2}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const v23, 0x7f0b0035

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v19

    invoke-virtual {v14, v0, v13, v1, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget v0, v14, Landroid/app/Notification;->flags:I

    move/from16 v23, v0

    or-int/lit8 v23, v23, 0x3

    move/from16 v0, v23

    iput v0, v14, Landroid/app/Notification;->flags:I

    iget v0, v14, Landroid/app/Notification;->defaults:I

    move/from16 v23, v0

    or-int/lit8 v23, v23, 0x4

    move/from16 v0, v23

    iput v0, v14, Landroid/app/Notification;->defaults:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/AlarmReceiver;->mCurrentCallState:I

    move/from16 v23, v0

    if-eqz v23, :cond_e

    const/16 v23, 0x64

    move/from16 v0, v23

    iput v0, v14, Landroid/app/Notification;->ledOffMS:I

    const/16 v23, 0xc8

    move/from16 v0, v23

    iput v0, v14, Landroid/app/Notification;->ledOnMS:I

    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/AlarmReceiver;->mCurrentCallState:I

    move/from16 v23, v0

    if-nez v23, :cond_f

    new-instance v5, Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v23, "intent.extra.alarm"

    move-object/from16 v0, v23

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v23, 0x10040000

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v23, v0

    const/high16 v24, 0x8000000

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-static {v0, v1, v5, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v14, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    :cond_f
    invoke-direct/range {p0 .. p1}, Lcom/android/deskclock/AlarmReceiver;->getNotificationManager(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v15

    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/app/NotificationManager;->cancel(I)V

    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v15, v0, v14}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method

.method private updateNotification(Landroid/content/Context;Lcom/android/deskclock/Alarm;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/deskclock/Alarm;
    .param p3    # I

    const/4 v8, 0x0

    invoke-direct {p0, p1}, Lcom/android/deskclock/AlarmReceiver;->getNotificationManager(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v3

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/android/deskclock/SetAlarm;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "intent.extra.alarm"

    invoke-virtual {v4, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget v5, p2, Lcom/android/deskclock/Alarm;->id:I

    invoke-static {p1, v5, v4, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p2, p1}, Lcom/android/deskclock/Alarm;->getLabelOrDefault(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/app/Notification;

    const v5, 0x7f02003c

    iget-wide v6, p2, Lcom/android/deskclock/Alarm;->time:J

    invoke-direct {v2, v5, v1, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const v5, 0x7f0b001a

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, p1, v1, v5, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget v5, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v5, v5, 0x10

    iput v5, v2, Landroid/app/Notification;->flags:I

    iget v5, p2, Lcom/android/deskclock/Alarm;->id:I

    invoke-virtual {v3, v5}, Landroid/app/NotificationManager;->cancel(I)V

    iget v5, p2, Lcom/android/deskclock/Alarm;->id:I

    invoke-virtual {v3, v5, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iput-object p1, p0, Lcom/android/deskclock/AlarmReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->goAsync()Landroid/content/BroadcastReceiver$PendingResult;

    move-result-object v4

    invoke-static {p1}, Lcom/android/deskclock/AlarmAlertWakeLock;->createPartialWakeLock(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    new-instance v0, Lcom/android/deskclock/AlarmReceiver$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/deskclock/AlarmReceiver$1;-><init>(Lcom/android/deskclock/AlarmReceiver;Landroid/content/Context;Landroid/content/Intent;Landroid/content/BroadcastReceiver$PendingResult;Landroid/os/PowerManager$WakeLock;)V

    invoke-static {v0}, Lcom/android/deskclock/AsyncHandler;->post(Ljava/lang/Runnable;)V

    return-void
.end method
