.class public final Lcom/android/deskclock/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final activated_background_holo_dark:I = 0x7f020000

.field public static final add_alarm:I = 0x7f020001

.field public static final alarm_alert_fullscreen_bg:I = 0x7f020002

.field public static final appwidget_clock_dial:I = 0x7f020003

.field public static final appwidget_clock_hour:I = 0x7f020004

.field public static final appwidget_clock_minute:I = 0x7f020005

.field public static final background_protector:I = 0x7f020006

.field public static final btn_in_call_round_disable:I = 0x7f020007

.field public static final btn_in_call_round_disable_focused:I = 0x7f020008

.field public static final btn_in_call_round_normal:I = 0x7f020009

.field public static final btn_in_call_round_pressed:I = 0x7f02000a

.field public static final btn_in_call_round_selected:I = 0x7f02000b

.field public static final btn_strip_trans_left:I = 0x7f02000c

.field public static final btn_strip_trans_left_normal:I = 0x7f02000d

.field public static final btn_strip_trans_left_pressed:I = 0x7f02000e

.field public static final btn_strip_trans_left_selected:I = 0x7f02000f

.field public static final btn_strip_trans_middle:I = 0x7f020010

.field public static final btn_strip_trans_middle_normal:I = 0x7f020011

.field public static final btn_strip_trans_middle_pressed:I = 0x7f020012

.field public static final btn_strip_trans_middle_selected:I = 0x7f020013

.field public static final btn_strip_trans_right:I = 0x7f020014

.field public static final btn_strip_trans_right_normal:I = 0x7f020015

.field public static final btn_strip_trans_right_pressed:I = 0x7f020016

.field public static final btn_strip_trans_right_selected:I = 0x7f020017

.field public static final clock_selector:I = 0x7f020018

.field public static final dialog:I = 0x7f020019

.field public static final dialog_divider_horizontal_light:I = 0x7f02001a

.field public static final divider_vertical_dark:I = 0x7f02001b

.field public static final ic_clock_add_alarm:I = 0x7f02001c

.field public static final ic_clock_add_alarm_selected:I = 0x7f02001d

.field public static final ic_clock_alarm_off:I = 0x7f02001e

.field public static final ic_clock_alarm_on:I = 0x7f02001f

.field public static final ic_clock_alarm_selected:I = 0x7f020020

.field public static final ic_clock_brightness:I = 0x7f020021

.field public static final ic_clock_strip_alarm:I = 0x7f020022

.field public static final ic_clock_strip_desk_clock:I = 0x7f020023

.field public static final ic_clock_strip_gallery:I = 0x7f020024

.field public static final ic_clock_strip_home:I = 0x7f020025

.field public static final ic_clock_strip_music:I = 0x7f020026

.field public static final ic_dialog_time:I = 0x7f020027

.field public static final ic_discard_holo_dark:I = 0x7f020028

.field public static final ic_indicator_off:I = 0x7f020029

.field public static final ic_indicator_on:I = 0x7f02002a

.field public static final ic_lock_idle_alarm:I = 0x7f02002b

.field public static final ic_lock_idle_alarm_saver:I = 0x7f02002c

.field public static final ic_lock_idle_alarm_saver_dim:I = 0x7f02002d

.field public static final ic_lock_idle_stopwatch:I = 0x7f02002e

.field public static final ic_menu_add:I = 0x7f02002f

.field public static final ic_menu_alarms:I = 0x7f020030

.field public static final ic_menu_clock_face:I = 0x7f020031

.field public static final ic_menu_desk_clock:I = 0x7f020032

.field public static final ic_menu_done_holo_dark:I = 0x7f020033

.field public static final ic_round_brightness:I = 0x7f020034

.field public static final ic_setalarm_holo_dark:I = 0x7f020035

.field public static final incall_round_button:I = 0x7f020036

.field public static final indicator_bar_onoff:I = 0x7f020037

.field public static final indicator_clock_onoff:I = 0x7f020038

.field public static final list_activated_holo:I = 0x7f020039

.field public static final list_selector_background_pressed:I = 0x7f02003a

.field public static final preview:I = 0x7f02003b

.field public static final stat_notify_alarm:I = 0x7f02003c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
