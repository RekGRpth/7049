.class public final Lcom/android/deskclock/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final add_alarm:I = 0x7f0f000e

.field public static final alarm_button:I = 0x7f0f001e

.field public static final alarm_delete:I = 0x7f0f0032

.field public static final alarm_revert:I = 0x7f0f0031

.field public static final alarm_save:I = 0x7f0f0033

.field public static final alarms_list:I = 0x7f0f000c

.field public static final alertTitle:I = 0x7f0f0002

.field public static final am_pm:I = 0x7f0f0006

.field public static final analog_appwidget:I = 0x7f0f0016

.field public static final base_layout:I = 0x7f0f000d

.field public static final battery:I = 0x7f0f001d

.field public static final clock_onoff:I = 0x7f0f0013

.field public static final date:I = 0x7f0f0024

.field public static final daysOfWeek:I = 0x7f0f0015

.field public static final delete_alarm:I = 0x7f0f003a

.field public static final desk_clock:I = 0x7f0f001a

.field public static final digitalClock:I = 0x7f0f0004

.field public static final dismiss:I = 0x7f0f0009

.field public static final divider:I = 0x7f0f0008

.field public static final done:I = 0x7f0f0010

.field public static final edit_alarm:I = 0x7f0f0039

.field public static final enable_alarm:I = 0x7f0f0038

.field public static final everyday:I = 0x7f0f002e

.field public static final gallery_button:I = 0x7f0f001f

.field public static final header_label:I = 0x7f0f0018

.field public static final header_time:I = 0x7f0f0017

.field public static final home_button:I = 0x7f0f0021

.field public static final indicator:I = 0x7f0f0012

.field public static final label:I = 0x7f0f0011

.field public static final menu_delete:I = 0x7f0f003c

.field public static final menu_item_add_alarm:I = 0x7f0f0035

.field public static final menu_item_desk_clock:I = 0x7f0f0037

.field public static final menu_item_dock_settings:I = 0x7f0f003b

.field public static final menu_item_settings:I = 0x7f0f0036

.field public static final music_button:I = 0x7f0f0020

.field public static final nextAlarm:I = 0x7f0f001b

.field public static final noRepeats:I = 0x7f0f002f

.field public static final power_off:I = 0x7f0f000b

.field public static final power_on:I = 0x7f0f000a

.field public static final repeatList:I = 0x7f0f0030

.field public static final save_menu_item:I = 0x7f0f0034

.field public static final saver_view:I = 0x7f0f0022

.field public static final settings:I = 0x7f0f000f

.field public static final snooze:I = 0x7f0f0007

.field public static final time:I = 0x7f0f0023

.field public static final timeDisplay:I = 0x7f0f0005

.field public static final time_date:I = 0x7f0f0025

.field public static final time_wrapper:I = 0x7f0f0014

.field public static final titleDivider:I = 0x7f0f0003

.field public static final title_template:I = 0x7f0f0001

.field public static final topPanel:I = 0x7f0f0000

.field public static final weather:I = 0x7f0f0026

.field public static final weather_high_temperature:I = 0x7f0f002b

.field public static final weather_icon:I = 0x7f0f0028

.field public static final weather_location:I = 0x7f0f002c

.field public static final weather_low_temperature:I = 0x7f0f002a

.field public static final weather_temp_icon_cluster:I = 0x7f0f0027

.field public static final weather_temperature:I = 0x7f0f0029

.field public static final weekdays:I = 0x7f0f002d

.field public static final window_tint:I = 0x7f0f001c

.field public static final window_touch:I = 0x7f0f0019


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
