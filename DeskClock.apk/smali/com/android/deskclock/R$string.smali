.class public final Lcom/android/deskclock/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add_alarm:I = 0x7f0b0009

.field public static final alarm_alert_alert_silenced:I = 0x7f0b001a

.field public static final alarm_alert_dismiss_text:I = 0x7f0b0019

.field public static final alarm_alert_snooze_set:I = 0x7f0b001c

.field public static final alarm_alert_snooze_text:I = 0x7f0b001b

.field public static final alarm_button_description:I = 0x7f0b003c

.field public static final alarm_in_silent_mode_summary:I = 0x7f0b002a

.field public static final alarm_in_silent_mode_title:I = 0x7f0b0029

.field public static final alarm_klaxon_service_desc:I = 0x7f0b0045

.field public static final alarm_list_title:I = 0x7f0b0008

.field public static final alarm_notify_snooze_label:I = 0x7f0b0036

.field public static final alarm_notify_snooze_text:I = 0x7f0b0037

.field public static final alarm_notify_text:I = 0x7f0b0035

.field public static final alarm_repeat:I = 0x7f0b0016

.field public static final alarm_vibrate:I = 0x7f0b0015

.field public static final alarm_volume_summary:I = 0x7f0b0033

.field public static final alarm_volume_title:I = 0x7f0b0032

.field public static final alert:I = 0x7f0b0017

.field public static final analog_gadget:I = 0x7f0b0027

.field public static final app_label:I = 0x7f0b0007

.field public static final auto_silence_never:I = 0x7f0b002e

.field public static final auto_silence_summary:I = 0x7f0b002d

.field public static final auto_silence_title:I = 0x7f0b002c

.field public static final battery_charging_level:I = 0x7f0b0042

.field public static final clock_instructions:I = 0x7f0b0026

.field public static final control_set_alarm:I = 0x7f0b0047

.field public static final control_set_alarm_with_existing:I = 0x7f0b0048

.field public static final day:I = 0x7f0b001d

.field public static final day_concat:I = 0x7f0b0025

.field public static final days:I = 0x7f0b001e

.field public static final default_label:I = 0x7f0b0013

.field public static final default_ringtone_setting_title:I = 0x7f0b003b

.field public static final delete:I = 0x7f0b0031

.field public static final delete_alarm:I = 0x7f0b000c

.field public static final delete_alarm_confirm:I = 0x7f0b000f

.field public static final desk_clock_button_description:I = 0x7f0b0041

.field public static final disable_alarm:I = 0x7f0b000e

.field public static final done:I = 0x7f0b002f

.field public static final enable_alarm:I = 0x7f0b000d

.field public static final every_day:I = 0x7f0b0023

.field public static final full_wday_month_day_no_year:I = 0x7f0b0000

.field public static final gallery_button_description:I = 0x7f0b003d

.field public static final hide_clock:I = 0x7f0b0011

.field public static final home_button_description:I = 0x7f0b0040

.field public static final hour:I = 0x7f0b001f

.field public static final hours:I = 0x7f0b0020

.field public static final label:I = 0x7f0b0012

.field public static final loading_ringtone:I = 0x7f0b0046

.field public static final menu_desk_clock:I = 0x7f0b000a

.field public static final menu_edit_alarm:I = 0x7f0b000b

.field public static final menu_item_dock_settings:I = 0x7f0b0044

.field public static final minute:I = 0x7f0b0021

.field public static final minutes:I = 0x7f0b0022

.field public static final music_button_description:I = 0x7f0b003e

.field public static final never:I = 0x7f0b0024

.field public static final nightmode_button_description:I = 0x7f0b003f

.field public static final power_off:I = 0x7f0b0002

.field public static final power_on:I = 0x7f0b0001

.field public static final repeat_everyday:I = 0x7f0b0005

.field public static final repeat_none:I = 0x7f0b0006

.field public static final repeat_weekdays:I = 0x7f0b0003

.field public static final revert:I = 0x7f0b0030

.field public static final set_alarm:I = 0x7f0b0014

.field public static final settings:I = 0x7f0b0028

.field public static final show_clock:I = 0x7f0b0010

.field public static final silent_alarm_summary:I = 0x7f0b0034

.field public static final snooze_duration_title:I = 0x7f0b002b

.field public static final stop_watch:I = 0x7f0b0004

.field public static final time:I = 0x7f0b0018

.field public static final volume_button_dialog_title:I = 0x7f0b0039

.field public static final volume_button_setting_summary:I = 0x7f0b003a

.field public static final volume_button_setting_title:I = 0x7f0b0038

.field public static final weather_fetch_failure:I = 0x7f0b0043


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
