.class public final Lcom/android/deskclock/ToastMaster;
.super Ljava/lang/Object;
.source "ToastMaster.java"


# static fields
.field private static sToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/deskclock/ToastMaster;->sToast:Landroid/widget/Toast;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cancelToast()V
    .locals 1

    sget-object v0, Lcom/android/deskclock/ToastMaster;->sToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/deskclock/ToastMaster;->sToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/android/deskclock/ToastMaster;->sToast:Landroid/widget/Toast;

    return-void
.end method

.method public static setToast(Landroid/widget/Toast;)V
    .locals 1
    .param p0    # Landroid/widget/Toast;

    sget-object v0, Lcom/android/deskclock/ToastMaster;->sToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/deskclock/ToastMaster;->sToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_0
    sput-object p0, Lcom/android/deskclock/ToastMaster;->sToast:Landroid/widget/Toast;

    return-void
.end method
