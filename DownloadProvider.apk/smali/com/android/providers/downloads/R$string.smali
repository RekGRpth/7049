.class public final Lcom/android/providers/downloads/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/downloads/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_label:I = 0x7f020005

.field public static final button_cancel_download:I = 0x7f020020

.field public static final button_queue_for_wifi:I = 0x7f02001f

.field public static final button_start_now:I = 0x7f020021

.field public static final cancel:I = 0x7f020001

.field public static final download_file_already_exist:I = 0x7f020003

.field public static final download_percent:I = 0x7f020022

.field public static final download_title:I = 0x7f020000

.field public static final download_unknown_title:I = 0x7f020014

.field public static final notification_download_complete:I = 0x7f020017

.field public static final notification_download_complete_op01:I = 0x7f020004

.field public static final notification_download_failed:I = 0x7f020018

.field public static final notification_filename_extras:I = 0x7f020016

.field public static final notification_filename_separator:I = 0x7f020015

.field public static final notification_need_wifi_for_size:I = 0x7f020019

.field public static final notification_paused_in_background:I = 0x7f02001a

.field public static final ok:I = 0x7f020002

.field public static final permdesc_accessAllDownloads:I = 0x7f020013

.field public static final permdesc_downloadCacheNonPurgeable:I = 0x7f02000f

.field public static final permdesc_downloadCompletedIntent:I = 0x7f02000b

.field public static final permdesc_downloadManager:I = 0x7f020007

.field public static final permdesc_downloadManagerAdvanced:I = 0x7f020009

.field public static final permdesc_downloadWithoutNotification:I = 0x7f020011

.field public static final permdesc_seeAllExternal:I = 0x7f02000d

.field public static final permlab_accessAllDownloads:I = 0x7f020012

.field public static final permlab_downloadCacheNonPurgeable:I = 0x7f02000e

.field public static final permlab_downloadCompletedIntent:I = 0x7f02000a

.field public static final permlab_downloadManager:I = 0x7f020006

.field public static final permlab_downloadManagerAdvanced:I = 0x7f020008

.field public static final permlab_downloadWithoutNotification:I = 0x7f020010

.field public static final permlab_seeAllExternal:I = 0x7f02000c

.field public static final wifi_recommended_body:I = 0x7f02001e

.field public static final wifi_recommended_title:I = 0x7f02001d

.field public static final wifi_required_body:I = 0x7f02001c

.field public static final wifi_required_title:I = 0x7f02001b


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
