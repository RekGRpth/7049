.class Lcom/android/providers/downloads/ui/DownloadList$1;
.super Ljava/lang/Object;
.source "DownloadList.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/providers/downloads/ui/DownloadList;->getOmaDownloadPositiveClickHandler(Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;IILandroid/view/View;)Landroid/content/DialogInterface$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/downloads/ui/DownloadList;

.field final synthetic val$downloadID:I

.field final synthetic val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

.field final synthetic val$showReason:I

.field final synthetic val$v:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/providers/downloads/ui/DownloadList;ILcom/android/providers/downloads/ui/DownloadList$DownloadInfo;ILandroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    iput p2, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$showReason:I

    iput-object p3, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iput p4, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadID:I

    iput-object p5, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$v:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const-string v9, "DownloadManager/OMA"

    const-string v10, "DownloadList: getOmaDownloadPositiveClickHandler"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$showReason:I

    const/16 v10, 0xc9

    if-ne v9, v10, :cond_3

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "uri"

    iget-object v10, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v10, v10, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mObjectUrl:Ljava/lang/String;

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "DownloadManager/OMA"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "DownloadList:getOmaDownloadClickHandler(): onClick(): object url is"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v11, v11, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mObjectUrl:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "mime Type is: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v11, v11, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mType:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v9, "notificationpackage"

    iget-object v10, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    invoke-virtual {v10}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "notificationclass"

    const-class v10, Lcom/android/providers/downloads/ui/OMADLOpenDownloadReceiver;

    invoke-virtual {v10}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "visibility"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "mimetype"

    iget-object v10, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v10, v10, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mType:Ljava/lang/String;

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "OMA_Download"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "destination"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v9, v9, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mNextUrl:Ljava/lang/String;

    if-eqz v9, :cond_0

    const-string v9, "OMA_Download_Next_Url"

    iget-object v10, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v10, v10, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mNextUrl:Ljava/lang/String;

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "DownloadManager/OMA"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "DownloadList:getOmaDownloadClickHandler(): onClick(): next url is"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v11, v11, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mNextUrl:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v9, v9, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mInstallNotifyUrl:Ljava/lang/String;

    if-eqz v9, :cond_1

    const-string v9, "OMA_Download_Install_Notify_Url"

    iget-object v10, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v10, v10, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mInstallNotifyUrl:Ljava/lang/String;

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "DownloadManager/OMA"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "DownloadList:getOmaDownloadClickHandler(): onClick(): install Notify url is"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v11, v11, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mInstallNotifyUrl:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    invoke-static {v9}, Lcom/mediatek/downloadmanager/ext/Extensions;->getDefault(Landroid/content/Context;)Lcom/mediatek/downloadmanager/ext/IDownloadProviderFeatureEx;

    move-result-object v9

    invoke-static {v9}, Lcom/android/providers/downloads/ui/DownloadList;->access$202(Lcom/mediatek/downloadmanager/ext/IDownloadProviderFeatureEx;)Lcom/mediatek/downloadmanager/ext/IDownloadProviderFeatureEx;

    invoke-static {}, Lcom/android/providers/downloads/ui/DownloadList;->access$200()Lcom/mediatek/downloadmanager/ext/IDownloadProviderFeatureEx;

    move-result-object v9

    iget v10, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadID:I

    invoke-interface {v9, v7, v10}, Lcom/mediatek/downloadmanager/ext/IDownloadProviderFeatureEx;->getDownloadPathSelectFileMager(Landroid/content/ContentValues;I)V

    :try_start_0
    new-instance v8, Landroid/net/WebAddress;

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v9, v9, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mObjectUrl:Ljava/lang/String;

    invoke-direct {v8, v9}, Landroid/net/WebAddress;-><init>(Ljava/lang/String;)V

    const-string v9, "description"

    invoke-virtual {v8}, Landroid/net/WebAddress;->getHost()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    invoke-virtual {v9}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Landroid/provider/Downloads$Impl;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9, v10, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/net/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "OMA_Download_Status"

    const/16 v10, 0x1eb

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    invoke-virtual {v9}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    iget v11, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadID:I

    int-to-long v11, v11

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v9, v10, v0, v11, v12}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    iget v10, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadID:I

    int-to-long v10, v10

    invoke-static {v9, v10, v11}, Lcom/android/providers/downloads/ui/DownloadList;->access$500(Lcom/android/providers/downloads/ui/DownloadList;J)V

    :cond_2
    :goto_1
    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/providers/downloads/ui/DownloadList;->access$302(Lcom/android/providers/downloads/ui/DownloadList;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    invoke-static {v9}, Lcom/android/providers/downloads/ui/DownloadList;->access$400(Lcom/android/providers/downloads/ui/DownloadList;)V

    return-void

    :catch_0
    move-exception v1

    const-string v9, "DownloadManager/OMA"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception trying to parse url:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v11, v11, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mObjectUrl:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    invoke-virtual {v9}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Landroid/provider/Downloads$Impl;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9, v10, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/providers/downloads/ui/DownloadList;->access$302(Lcom/android/providers/downloads/ui/DownloadList;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    invoke-static {v9}, Lcom/android/providers/downloads/ui/DownloadList;->access$400(Lcom/android/providers/downloads/ui/DownloadList;)V

    goto :goto_0

    :cond_3
    iget v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$showReason:I

    const/16 v10, 0xcb

    if-ne v9, v10, :cond_4

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v9, v9, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mNextUrl:Ljava/lang/String;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadInfo:Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;

    iget-object v9, v9, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mNextUrl:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    new-instance v2, Landroid/content/Intent;

    const-string v9, "android.intent.action.VIEW"

    invoke-direct {v2, v9, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v9, 0x80000

    invoke-virtual {v2, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    invoke-virtual {v9, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_4
    iget v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$showReason:I

    const/16 v10, 0x191

    if-ne v9, v10, :cond_2

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$v:Landroid/view/View;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$v:Landroid/view/View;

    const v10, 0x7f09000e

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$v:Landroid/view/View;

    const v10, 0x7f090010

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v9, "DownloadManager/Enhance"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "DownloadList:getOmaDownloadClickHandler:onClick():Autenticate UserName is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Password is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "username"

    invoke-virtual {v7, v9, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "password"

    invoke-virtual {v7, v9, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "status"

    const/16 v10, 0xbe

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v9, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    invoke-virtual {v9}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    iget v11, p0, Lcom/android/providers/downloads/ui/DownloadList$1;->val$downloadID:I

    int-to-long v11, v11

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v9, v10, v7, v11, v12}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1
.end method
