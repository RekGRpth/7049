.class Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;
.super Ljava/lang/Object;
.source "DownloadList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/downloads/ui/DownloadList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadInfo"
.end annotation


# instance fields
.field public mDescription:Ljava/lang/String;

.field public mInstallNotifyUrl:Ljava/lang/String;

.field public mName:Ljava/lang/String;

.field public mNextUrl:Ljava/lang/String;

.field public mObjectUrl:Ljava/lang/String;

.field public mSize:I

.field public mSupportByDevice:Z

.field public mType:Ljava/lang/String;

.field public mVendor:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/providers/downloads/ui/DownloadList;


# direct methods
.method constructor <init>(Lcom/android/providers/downloads/ui/DownloadList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # I
    .param p10    # Z

    iput-object p1, p0, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->this$0:Lcom/android/providers/downloads/ui/DownloadList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mName:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mVendor:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mType:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mObjectUrl:Ljava/lang/String;

    iput-object p6, p0, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mNextUrl:Ljava/lang/String;

    iput-object p7, p0, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mInstallNotifyUrl:Ljava/lang/String;

    iput-object p8, p0, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mDescription:Ljava/lang/String;

    iput p9, p0, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mSize:I

    iput-boolean p10, p0, Lcom/android/providers/downloads/ui/DownloadList$DownloadInfo;->mSupportByDevice:Z

    return-void
.end method
