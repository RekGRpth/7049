.class Lcom/android/providers/drm/BootCompletedReceiver$LoadDeviceIdListener;
.super Ljava/lang/Object;
.source "BootCompletedReceiver.java"

# interfaces
.implements Landroid/drm/DrmManagerClient$OnEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/drm/BootCompletedReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadDeviceIdListener"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/android/providers/drm/BootCompletedReceiver;


# direct methods
.method public constructor <init>(Lcom/android/providers/drm/BootCompletedReceiver;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/providers/drm/BootCompletedReceiver$LoadDeviceIdListener;->this$0:Lcom/android/providers/drm/BootCompletedReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/drm/BootCompletedReceiver$LoadDeviceIdListener;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/providers/drm/BootCompletedReceiver$LoadDeviceIdListener;->mContext:Landroid/content/Context;

    return-void
.end method

.method private getIMEI(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Lcom/android/providers/drm/BootCompletedReceiver;->deviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onEvent(Landroid/drm/DrmManagerClient;Landroid/drm/DrmEvent;)V
    .locals 11
    .param p1    # Landroid/drm/DrmManagerClient;
    .param p2    # Landroid/drm/DrmEvent;

    const/16 v10, 0x7d4

    const/16 v9, 0x7d2

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v4, "DRM/BootCompletedReceiver"

    const-string v5, "LoadDeviceIdListener: Answer for event LOAD_DEVICE_ID."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v4, "DRM/BootCompletedReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LoadDeviceIdListener: Device id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "000000000000000"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const-string v4, "DRM/BootCompletedReceiver"

    const-string v5, "LoadDeviceIdListener: try to obtain device id"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/providers/drm/BootCompletedReceiver$LoadDeviceIdListener;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4}, Lcom/android/providers/drm/BootCompletedReceiver$LoadDeviceIdListener;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "DRM/BootCompletedReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LoadDeviceIdListener: device id obtained: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, v8, [B

    aput-byte v7, v0, v7

    new-instance v2, Landroid/drm/DrmInfo;

    const/16 v4, 0x7d8

    const-string v5, "application/vnd.oma.drm.content"

    invoke-direct {v2, v4, v0, v5}, Landroid/drm/DrmInfo;-><init>(I[BLjava/lang/String;)V

    const-string v4, "saveDeviceId"

    invoke-virtual {v2, v4, v1}, Landroid/drm/DrmInfo;->put(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p1, v2}, Landroid/drm/DrmManagerClient;->processExtraDrmInfo(Landroid/drm/DrmInfo;)I

    goto :goto_0

    :cond_1
    new-array v0, v8, [B

    aput-byte v7, v0, v7

    new-instance v2, Landroid/drm/DrmInfo;

    const-string v4, "application/vnd.oma.drm.content"

    invoke-direct {v2, v10, v0, v4}, Landroid/drm/DrmInfo;-><init>(I[BLjava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/drm/DrmManagerClient;->processExtraDrmInfo(Landroid/drm/DrmInfo;)I

    move-result v3

    new-instance v2, Landroid/drm/DrmInfo;

    const-string v4, "application/vnd.oma.drm.content"

    invoke-direct {v2, v9, v0, v4}, Landroid/drm/DrmInfo;-><init>(I[BLjava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/drm/DrmManagerClient;->processExtraDrmInfo(Landroid/drm/DrmInfo;)I

    goto/16 :goto_0

    :pswitch_1
    const-string v4, "DRM/BootCompletedReceiver"

    const-string v5, "LoadDeviceIdListener: Answer for event SAVE_DEVICE_ID."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, v8, [B

    aput-byte v7, v0, v7

    new-instance v2, Landroid/drm/DrmInfo;

    const-string v4, "application/vnd.oma.drm.content"

    invoke-direct {v2, v10, v0, v4}, Landroid/drm/DrmInfo;-><init>(I[BLjava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/drm/DrmManagerClient;->processExtraDrmInfo(Landroid/drm/DrmInfo;)I

    move-result v3

    new-instance v2, Landroid/drm/DrmInfo;

    const-string v4, "application/vnd.oma.drm.content"

    invoke-direct {v2, v9, v0, v4}, Landroid/drm/DrmInfo;-><init>(I[BLjava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/drm/DrmManagerClient;->processExtraDrmInfo(Landroid/drm/DrmInfo;)I

    move-result v3

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7d7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
