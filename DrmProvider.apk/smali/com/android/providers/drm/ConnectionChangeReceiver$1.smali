.class Lcom/android/providers/drm/ConnectionChangeReceiver$1;
.super Ljava/lang/Object;
.source "ConnectionChangeReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/providers/drm/ConnectionChangeReceiver;->launchSimpleThread(Landroid/content/Context;Landroid/drm/DrmManagerClient;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/drm/ConnectionChangeReceiver;

.field final synthetic val$client:Landroid/drm/DrmManagerClient;

.field final synthetic val$conManager:Landroid/net/ConnectivityManager;

.field final synthetic val$type:I


# direct methods
.method constructor <init>(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/net/ConnectivityManager;ILandroid/drm/DrmManagerClient;)V
    .locals 0

    iput-object p1, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$1;->this$0:Lcom/android/providers/drm/ConnectionChangeReceiver;

    iput-object p2, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$1;->val$conManager:Landroid/net/ConnectivityManager;

    iput p3, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$1;->val$type:I

    iput-object p4, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$1;->val$client:Landroid/drm/DrmManagerClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v7, 0x0

    const-string v4, "DRM/ConnectionChangeReceiver"

    const-string v5, "SNTP : the thread launched."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$1;->this$0:Lcom/android/providers/drm/ConnectionChangeReceiver;

    iget-object v5, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$1;->val$conManager:Landroid/net/ConnectivityManager;

    iget v6, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$1;->val$type:I

    invoke-static {v4, v5, v6}, Lcom/android/providers/drm/ConnectionChangeReceiver;->access$200(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/net/ConnectivityManager;I)I

    move-result v3

    const/4 v4, -0x1

    if-eq v4, v3, :cond_0

    invoke-static {}, Lcom/android/providers/drm/ConnectionChangeReceiver;->access$300()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v3

    invoke-static {v4}, Lcom/android/providers/drm/Ntp;->sync(Ljava/lang/String;)I

    move-result v2

    const-string v4, "DRM/ConnectionChangeReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SNTP: synchronization result, utc time offset: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x1

    new-array v0, v4, [B

    aput-byte v7, v0, v7

    new-instance v1, Landroid/drm/DrmInfo;

    const/16 v4, 0x7d1

    const-string v5, "application/vnd.oma.drm.content"

    invoke-direct {v1, v4, v0, v5}, Landroid/drm/DrmInfo;-><init>(I[BLjava/lang/String;)V

    const-string v4, "updateClock"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/drm/DrmInfo;->put(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$1;->val$client:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, v1}, Landroid/drm/DrmManagerClient;->processExtraDrmInfo(Landroid/drm/DrmInfo;)I

    :cond_0
    return-void
.end method
