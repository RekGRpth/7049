.class Lcom/android/providers/drm/ConnectionChangeReceiver$CheckClockListener;
.super Ljava/lang/Object;
.source "ConnectionChangeReceiver.java"

# interfaces
.implements Landroid/drm/DrmManagerClient$OnEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/drm/ConnectionChangeReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckClockListener"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/android/providers/drm/ConnectionChangeReceiver;


# direct methods
.method public constructor <init>(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$CheckClockListener;->this$0:Lcom/android/providers/drm/ConnectionChangeReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$CheckClockListener;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$CheckClockListener;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public onEvent(Landroid/drm/DrmManagerClient;Landroid/drm/DrmEvent;)V
    .locals 5
    .param p1    # Landroid/drm/DrmManagerClient;
    .param p2    # Landroid/drm/DrmEvent;

    const/16 v2, 0x7d6

    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getType()I

    move-result v3

    if-ne v2, v3, :cond_0

    const-string v2, "DRM/ConnectionChangeReceiver"

    const-string v3, "CheckClockListener: Answer for event CHECK_CLOCK."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DRM/ConnectionChangeReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CheckClockListener: Clock status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "valid"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "DRM/ConnectionChangeReceiver"

    const-string v3, "CheckClockListener: Secure timer is already valid"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$CheckClockListener;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$CheckClockListener;->this$0:Lcom/android/providers/drm/ConnectionChangeReceiver;

    iget-object v3, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$CheckClockListener;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/android/providers/drm/ConnectionChangeReceiver;->access$000(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/content/Context;)V

    goto :goto_0
.end method
