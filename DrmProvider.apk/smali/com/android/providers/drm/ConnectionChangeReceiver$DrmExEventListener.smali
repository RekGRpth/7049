.class Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;
.super Ljava/lang/Object;
.source "ConnectionChangeReceiver.java"

# interfaces
.implements Landroid/drm/DrmManagerClient$OnEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/drm/ConnectionChangeReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrmExEventListener"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/android/providers/drm/ConnectionChangeReceiver;


# direct methods
.method public constructor <init>(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;->this$0:Lcom/android/providers/drm/ConnectionChangeReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public onEvent(Landroid/drm/DrmManagerClient;Landroid/drm/DrmEvent;)V
    .locals 9
    .param p1    # Landroid/drm/DrmManagerClient;
    .param p2    # Landroid/drm/DrmEvent;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getType()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v6, "DRM/ConnectionChangeReceiver"

    const-string v7, "DrmExEventListener: Answer for event LOAD_DEVICE_ID."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string v6, "DRM/ConnectionChangeReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DrmExEventListener: Device id: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "000000000000000"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    move v2, v4

    :goto_1
    if-nez v2, :cond_2

    iget-object v6, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/android/providers/drm/BootCompletedReceiver;->deviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    new-array v0, v4, [B

    aput-byte v5, v0, v5

    new-instance v1, Landroid/drm/DrmInfo;

    const/16 v4, 0x7d8

    const-string v5, "application/vnd.oma.drm.content"

    invoke-direct {v1, v4, v0, v5}, Landroid/drm/DrmInfo;-><init>(I[BLjava/lang/String;)V

    const-string v4, "saveDeviceId"

    invoke-virtual {v1, v4, v3}, Landroid/drm/DrmInfo;->put(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p1, v1}, Landroid/drm/DrmManagerClient;->processExtraDrmInfo(Landroid/drm/DrmInfo;)I

    goto :goto_0

    :cond_1
    move v2, v5

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;->this$0:Lcom/android/providers/drm/ConnectionChangeReceiver;

    iget-object v5, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;->mContext:Landroid/content/Context;

    invoke-static {v4, v5, p1}, Lcom/android/providers/drm/ConnectionChangeReceiver;->access$100(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/content/Context;Landroid/drm/DrmManagerClient;)V

    goto :goto_0

    :pswitch_1
    const-string v4, "DRM/ConnectionChangeReceiver"

    const-string v5, "DrmExEventListener: Answer for event SAVE_DEVICE_ID."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;->this$0:Lcom/android/providers/drm/ConnectionChangeReceiver;

    iget-object v5, p0, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;->mContext:Landroid/content/Context;

    invoke-static {v4, v5, p1}, Lcom/android/providers/drm/ConnectionChangeReceiver;->access$100(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/content/Context;Landroid/drm/DrmManagerClient;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
