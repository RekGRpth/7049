.class public Lcom/android/providers/drm/ConnectionChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ConnectionChangeReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;,
        Lcom/android/providers/drm/ConnectionChangeReceiver$CheckClockListener;
    }
.end annotation


# static fields
.field private static final NETWORK_TYPE_MOBILE_NET:Ljava/lang/String; = "enableNET"

.field private static final TAG:Ljava/lang/String; = "DRM/ConnectionChangeReceiver"

.field private static sHostList:[Ljava/lang/String;

.field private static sThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    sput-object v0, Lcom/android/providers/drm/ConnectionChangeReceiver;->sThread:Ljava/lang/Thread;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "hshh.org"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "t1.hshh.org"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "t2.hshh.org"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "t3.hshh.org"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "clock.via.net"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/drm/ConnectionChangeReceiver;->sHostList:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/content/Context;)V
    .locals 0
    .param p0    # Lcom/android/providers/drm/ConnectionChangeReceiver;
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/providers/drm/ConnectionChangeReceiver;->launchSNTP(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/content/Context;Landroid/drm/DrmManagerClient;)V
    .locals 0
    .param p0    # Lcom/android/providers/drm/ConnectionChangeReceiver;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/drm/DrmManagerClient;

    invoke-direct {p0, p1, p2}, Lcom/android/providers/drm/ConnectionChangeReceiver;->launchSimpleThread(Landroid/content/Context;Landroid/drm/DrmManagerClient;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/net/ConnectivityManager;I)I
    .locals 1
    .param p0    # Lcom/android/providers/drm/ConnectionChangeReceiver;
    .param p1    # Landroid/net/ConnectivityManager;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/providers/drm/ConnectionChangeReceiver;->checkRouteToHost(Landroid/net/ConnectivityManager;I)I

    move-result v0

    return v0
.end method

.method static synthetic access$300()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/providers/drm/ConnectionChangeReceiver;->sHostList:[Ljava/lang/String;

    return-object v0
.end method

.method private checkRouteToHost(Landroid/net/ConnectivityManager;I)I
    .locals 9
    .param p1    # Landroid/net/ConnectivityManager;
    .param p2    # I

    const-string v6, "DRM/ConnectionChangeReceiver"

    const-string v7, "==== check if there\'s available route to SNTP servers ===="

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, -0x1

    if-eqz p1, :cond_0

    sget-object v6, Lcom/android/providers/drm/ConnectionChangeReceiver;->sHostList:[Ljava/lang/String;

    array-length v5, v6

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_0

    const/4 v1, 0x0

    :try_start_0
    const-string v6, "DRM/ConnectionChangeReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get host address by name: ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/providers/drm/ConnectionChangeReceiver;->sHostList:[Ljava/lang/String;

    aget-object v8, v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, Lcom/android/providers/drm/ConnectionChangeReceiver;->sHostList:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-static {v6}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/providers/drm/ConnectionChangeReceiver;->ipToInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const-string v6, "DRM/ConnectionChangeReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "request route for host: ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/providers/drm/ConnectionChangeReceiver;->sHostList:[Ljava/lang/String;

    aget-object v8, v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, p2, v1}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "DRM/ConnectionChangeReceiver"

    const-string v7, "request route for host success."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    :cond_0
    return v4

    :catch_0
    move-exception v2

    const-string v6, "DRM/ConnectionChangeReceiver"

    const-string v7, "caught UnknownHostException"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const-string v6, "DRM/ConnectionChangeReceiver"

    const-string v7, "request route for host failed."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private ipToInt(Ljava/lang/String;)I
    .locals 12
    .param p1    # Ljava/lang/String;

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v5, -0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    const-string v6, "\\."

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v4, v0

    const/4 v6, 0x4

    if-ne v4, v6, :cond_0

    new-array v1, v4, [I

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_2

    :try_start_0
    aget-object v6, v0, v3

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v1, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_2
    const-string v5, "DRM/ConnectionChangeReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ipToInt: a[0] = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v1, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", a[1] = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v1, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", a[2] = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v1, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", a[3] = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v1, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    aget v5, v1, v11

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x18

    aget v6, v1, v10

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x10

    or-int/2addr v5, v6

    aget v6, v1, v9

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    aget v6, v1, v8

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v5, v6

    goto :goto_0
.end method

.method private launchSNTP(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v5, 0x0

    sget-object v3, Lcom/android/providers/drm/ConnectionChangeReceiver;->sThread:Ljava/lang/Thread;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/android/providers/drm/ConnectionChangeReceiver;->sThread:Ljava/lang/Thread;

    if-eqz v3, :cond_1

    sget-object v3, Lcom/android/providers/drm/ConnectionChangeReceiver;->sThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->isAlive()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "DRM/ConnectionChangeReceiver"

    const-string v4, "SNTP : the thread is not running."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/drm/DrmManagerClient;

    invoke-direct {v0, p1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;

    invoke-direct {v3, p0, p1}, Lcom/android/providers/drm/ConnectionChangeReceiver$DrmExEventListener;-><init>(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/drm/DrmManagerClient;->setOnEventListener(Landroid/drm/DrmManagerClient$OnEventListener;)V

    const/4 v3, 0x1

    new-array v1, v3, [B

    aput-byte v5, v1, v5

    new-instance v2, Landroid/drm/DrmInfo;

    const/16 v3, 0x7d7

    const-string v4, "application/vnd.oma.drm.content"

    invoke-direct {v2, v3, v1, v4}, Landroid/drm/DrmInfo;-><init>(I[BLjava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/drm/DrmManagerClient;->processExtraDrmInfo(Landroid/drm/DrmInfo;)I

    :goto_0
    return-void

    :cond_1
    const-string v3, "DRM/ConnectionChangeReceiver"

    const-string v4, "SNTP : the thread is already running."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private launchSimpleThread(Landroid/content/Context;Landroid/drm/DrmManagerClient;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/drm/DrmManagerClient;

    const-string v3, "DRM/ConnectionChangeReceiver"

    const-string v4, "SNTP : launch the thread."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "connectivity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    const-string v3, "DRM/ConnectionChangeReceiver"

    const-string v4, "SNTP : invalid connectivity manager."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v3, "DRM/ConnectionChangeReceiver"

    const-string v4, "SNTP : invalid active network info."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "DRM/ConnectionChangeReceiver"

    const-string v4, "SNTP : unavailable active network."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    const-string v3, "DRM/ConnectionChangeReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SNTP : active network type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/android/providers/drm/ConnectionChangeReceiver$1;

    invoke-direct {v4, p0, v0, v2, p2}, Lcom/android/providers/drm/ConnectionChangeReceiver$1;-><init>(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/net/ConnectivityManager;ILandroid/drm/DrmManagerClient;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v3, Lcom/android/providers/drm/ConnectionChangeReceiver;->sThread:Ljava/lang/Thread;

    sget-object v3, Lcom/android/providers/drm/ConnectionChangeReceiver;->sThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v5, 0x0

    const-string v3, "DRM/ConnectionChangeReceiver"

    const-string v4, "onReceive : CONNECTIVITY_CHANGE received."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/drm/DrmManagerClient;

    invoke-direct {v0, p1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/android/providers/drm/ConnectionChangeReceiver$CheckClockListener;

    invoke-direct {v3, p0, p1}, Lcom/android/providers/drm/ConnectionChangeReceiver$CheckClockListener;-><init>(Lcom/android/providers/drm/ConnectionChangeReceiver;Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/drm/DrmManagerClient;->setOnEventListener(Landroid/drm/DrmManagerClient$OnEventListener;)V

    const/4 v3, 0x1

    new-array v1, v3, [B

    aput-byte v5, v1, v5

    new-instance v2, Landroid/drm/DrmInfo;

    const/16 v3, 0x7d6

    const-string v4, "application/vnd.oma.drm.content"

    invoke-direct {v2, v3, v1, v4}, Landroid/drm/DrmInfo;-><init>(I[BLjava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/drm/DrmManagerClient;->processExtraDrmInfo(Landroid/drm/DrmInfo;)I

    return-void
.end method
