.class Lcom/android/providers/drm/DrmPushReceiver$1;
.super Ljava/lang/Thread;
.source "DrmPushReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/providers/drm/DrmPushReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/drm/DrmPushReceiver;

.field final synthetic val$contextRef:Landroid/content/Context;

.field final synthetic val$rightData:[B

.field final synthetic val$rightMimeType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/providers/drm/DrmPushReceiver;Landroid/content/Context;[BLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/providers/drm/DrmPushReceiver$1;->this$0:Lcom/android/providers/drm/DrmPushReceiver;

    iput-object p2, p0, Lcom/android/providers/drm/DrmPushReceiver$1;->val$contextRef:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/providers/drm/DrmPushReceiver$1;->val$rightData:[B

    iput-object p4, p0, Lcom/android/providers/drm/DrmPushReceiver$1;->val$rightMimeType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    :try_start_0
    const-string v6, "DrmPushReceiver"

    const-string v7, "onReceive : received drm rights object via WAP PUSH."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/drm/DrmManagerClient;

    iget-object v6, p0, Lcom/android/providers/drm/DrmPushReceiver$1;->val$contextRef:Landroid/content/Context;

    invoke-direct {v1, v6}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/drm/DrmRights;

    iget-object v6, p0, Lcom/android/providers/drm/DrmPushReceiver$1;->val$rightData:[B

    iget-object v7, p0, Lcom/android/providers/drm/DrmPushReceiver$1;->val$rightMimeType:Ljava/lang/String;

    invoke-direct {v2, v6, v7}, Landroid/drm/DrmRights;-><init>([BLjava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/drm/DrmManagerClient;->getContentIdFromRights(Landroid/drm/DrmRights;)Ljava/lang/String;

    move-result-object v0

    const-string v6, "DrmPushReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceive: content id of rights object: ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v1, v2, v6, v7}, Landroid/drm/DrmManagerClient;->saveRights(Landroid/drm/DrmRights;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    const-string v6, "DrmPushReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceive: result of saving drm rights object: ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    new-instance v6, Landroid/content/ComponentName;

    const-string v7, "com.android.providers.drm"

    const-string v8, "com.android.providers.drm.DrmService"

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v6, "cid"

    invoke-virtual {v4, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/providers/drm/DrmPushReceiver$1;->val$contextRef:Landroid/content/Context;

    invoke-virtual {v6, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v3

    const-string v6, "DrmPushReceiver"

    const-string v7, "onReceive: IO error occurs when saving rights objects."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
