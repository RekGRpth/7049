.class public Lcom/android/providers/drm/TimeChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TimeChangedReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DRM/TimeChangedReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v5, 0x0

    const-string v3, "DRM/TimeChangedReceiver"

    const-string v4, "onReceive : TIME_SET received."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/drm/DrmManagerClient;

    invoke-direct {v0, p1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    new-array v1, v3, [B

    aput-byte v5, v1, v5

    new-instance v2, Landroid/drm/DrmInfo;

    const/16 v3, 0x7d3

    const-string v4, "application/vnd.oma.drm.content"

    invoke-direct {v2, v3, v1, v4}, Landroid/drm/DrmInfo;-><init>(I[BLjava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/drm/DrmManagerClient;->processExtraDrmInfo(Landroid/drm/DrmInfo;)I

    return-void
.end method
