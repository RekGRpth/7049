.class public Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;
.super Landroid/app/Activity;
.source "AmoiSystemInfo.java"


# instance fields
.field private hardwareVersion:Ljava/lang/String;

.field private mBuildType:Landroid/widget/TextView;

.field private mHardWareVer:Landroid/widget/TextView;

.field private mSN:Landroid/widget/TextView;

.field private mSystemVerTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getRevisioninfo()V
    .locals 7

    const-string v3, "/proc/hwversion"

    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    :goto_0
    invoke-virtual {v4}, Ljava/io/FileInputStream;->read()I

    move-result v1

    const/4 v5, -0x1

    if-eq v1, v5, :cond_1

    const/16 v5, 0xa

    if-eq v1, v5, :cond_0

    int-to-char v5, v1

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/amoi/AmoiEngineerMode/DeviceInfo;->getHardwareVersion()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->hardwareVersion:Ljava/lang/String;

    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "V"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->hardwareVersion:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f03002c

    invoke-virtual {p0, v3}, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->setContentView(I)V

    const-string v3, "phone"

    invoke-virtual {p0, v3}, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->getRevisioninfo()V

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSN()Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f070064

    invoke-virtual {p0, v3}, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->mSystemVerTextView:Landroid/widget/TextView;

    const-string v3, "ro.build.display.id"

    const-string v4, "NULL"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->mSystemVerTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f070066

    invoke-virtual {p0, v3}, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->mSN:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->mSN:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f070068

    invoke-virtual {p0, v3}, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->mBuildType:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->mBuildType:Landroid/widget/TextView;

    const-string v4, "ro.build.type"

    const-string v5, "NULL"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f07006a

    invoke-virtual {p0, v3}, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->mHardWareVer:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->mHardWareVer:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/AmoiSystemInfo;->hardwareVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
