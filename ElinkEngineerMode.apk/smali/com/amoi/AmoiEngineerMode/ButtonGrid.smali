.class public Lcom/amoi/AmoiEngineerMode/ButtonGrid;
.super Landroid/view/ViewGroup;
.source "ButtonGrid.java"


# static fields
.field private static final COLUMNS:I = 0x5

.field private static final NUM_CHILDREN:I = 0x19

.field private static final ROWS:I = 0x5


# instance fields
.field private mButtonHeight:I

.field private mButtonWidth:I

.field private mButtons:[Landroid/view/View;

.field private mHeight:I

.field private mHeightInc:I

.field private mWidth:I

.field private mWidthInc:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x19

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mButtons:[Landroid/view/View;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0x19

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mButtons:[Landroid/view/View;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v0, 0x19

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mButtons:[Landroid/view/View;

    return-void
.end method


# virtual methods
.method protected amoilayout(II)Z
    .locals 12
    .param p1    # I
    .param p2    # I

    const/4 v11, 0x5

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mButtons:[Landroid/view/View;

    const/4 v8, 0x4

    new-array v0, v8, [I

    fill-array-data v0, :array_0

    const/4 v8, 0x3

    new-array v1, v8, [I

    fill-array-data v1, :array_1

    const/4 v4, 0x0

    sub-int v8, p2, p1

    iget v9, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mHeight:I

    sub-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->getPaddingTop()I

    move-result v9

    add-int v7, v8, v9

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v11, :cond_1

    const/4 v6, 0x0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v11, :cond_0

    aget-object v8, v2, v4

    aget v9, v1, v3

    add-int/2addr v9, v6

    aget v10, v0, v5

    add-int/2addr v10, v7

    invoke-virtual {v8, v6, v7, v9, v10}, Landroid/view/View;->layout(IIII)V

    aget v8, v1, v3

    add-int/2addr v6, v8

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    aget v8, v0, v5

    add-int/2addr v7, v8

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v8, 0x1

    return v8

    nop

    :array_0
    .array-data 4
        0x36
        0x36
        0x35
        0x45
    .end array-data

    :array_1
    .array-data 4
        0x6e
        0x64
        0x6e
    .end array-data
.end method

.method protected onFinishInflate()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mButtons:[Landroid/view/View;

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x19

    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    aget-object v3, v0, v2

    invoke-virtual {v3, v4, v4}, Landroid/view/View;->measure(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    aget-object v1, v0, v4

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iput v3, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mButtonWidth:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iput v3, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mButtonHeight:I

    iget v3, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mButtonWidth:I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->getPaddingLeft()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mWidthInc:I

    iget v3, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mButtonHeight:I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mHeightInc:I

    iget v3, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mWidthInc:I

    mul-int/lit8 v3, v3, 0x5

    iput v3, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mWidth:I

    iget v3, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mHeightInc:I

    mul-int/lit8 v3, v3, 0x5

    iput v3, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mHeight:I

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 12
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mButtons:[Landroid/view/View;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->getPaddingLeft()I

    move-result v3

    sub-int v9, p4, p2

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->getPaddingLeft()I

    move-result v10

    sub-int/2addr v9, v10

    div-int/lit8 v5, v9, 0x5

    sub-int v9, p5, p3

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->getPaddingTop()I

    move-result v10

    sub-int/2addr v9, v10

    div-int/lit8 v4, v9, 0x5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->getPaddingTop()I

    move-result v8

    const/4 v6, 0x0

    :goto_0
    const/4 v9, 0x5

    if-ge v6, v9, :cond_1

    move v7, v3

    const/4 v1, 0x0

    :goto_1
    const/4 v9, 0x5

    if-ge v1, v9, :cond_0

    aget-object v9, v0, v2

    add-int v10, v7, v5

    add-int v11, v8, v4

    invoke-virtual {v9, v7, v8, v10, v11}, Landroid/view/View;->layout(IIII)V

    add-int/2addr v7, v5

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    add-int/2addr v8, v4

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mWidth:I

    invoke-static {v2, p1}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->resolveSize(II)I

    move-result v1

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->mHeight:I

    invoke-static {v2, p2}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/amoi/AmoiEngineerMode/ButtonGrid;->setMeasuredDimension(II)V

    return-void
.end method

.method public setChildrenBackgroundResource(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x19

    if-ge v0, v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
