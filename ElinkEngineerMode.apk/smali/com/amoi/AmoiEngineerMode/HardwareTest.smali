.class public Lcom/amoi/AmoiEngineerMode/HardwareTest;
.super Landroid/app/Activity;
.source "HardwareTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amoi/AmoiEngineerMode/HardwareTest$1;,
        Lcom/amoi/AmoiEngineerMode/HardwareTest$TestButtonListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HardwareTests"

.field private static final buttonToActivityMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x12

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f070040

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f070037

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/CameraTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f070032

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/AudioTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f070038

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/SubCameraTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f070036

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f070031

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/LCDTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f07003d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/IndicatorTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f07002f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/BluetoothTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f07003a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/KeystrokeTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f070039

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f070030

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/WifiTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f070034

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/MotorTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f070035

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/SensorsTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f07002e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/GPSTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f070033

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/lightTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f07003c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f07003e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/FlashLightTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f07003b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/SimStatusTest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    const v1, 0x7f07003f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lcom/amoi/AmoiEngineerMode/HardwareTest;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/HardwareTest;
    .param p1    # Ljava/lang/Class;

    invoke-direct {p0, p1}, Lcom/amoi/AmoiEngineerMode/HardwareTest;->getIntentByActivity(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private getIntentByActivity(Ljava/lang/Class;)Landroid/content/Intent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v4, 0x7f030014

    invoke-virtual {p0, v4}, Lcom/amoi/AmoiEngineerMode/HardwareTest;->setContentView(I)V

    new-instance v3, Lcom/amoi/AmoiEngineerMode/HardwareTest$TestButtonListener;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/amoi/AmoiEngineerMode/HardwareTest$TestButtonListener;-><init>(Lcom/amoi/AmoiEngineerMode/HardwareTest;Lcom/amoi/AmoiEngineerMode/HardwareTest$1;)V

    sget-object v4, Lcom/amoi/AmoiEngineerMode/HardwareTest;->buttonToActivityMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/HardwareTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f07003d

    if-ne v1, v4, :cond_1

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    :cond_1
    const v4, 0x7f07003e

    if-ne v1, v4, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_2
    return-void
.end method
