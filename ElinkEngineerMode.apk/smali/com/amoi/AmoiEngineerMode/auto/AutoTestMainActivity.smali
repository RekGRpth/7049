.class public Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;
.super Landroid/app/Activity;
.source "AutoTestMainActivity.java"


# static fields
.field public static final RESULT_EXIT:I = 0x3

.field public static final RESULT_NEXT:I = 0x2

.field public static final RESULT_PREVIOUSE:I = 0x1

.field public static final TAG:Ljava/lang/String; = "AutoTestMainActivity"

.field private static final TEST_CLASSES:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/PhoneInfoAutoTest;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/SimCardAutoTest;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/BatteryAutoTest;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/FlashLightAutoTest;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/TouchPanelEdgeAutoTest;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/ReceiverAutoTest;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/KeystrokeAutoTest;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/ReceiverCurcuitAutoTet;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/LCDAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/SpeakerAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/VibratorAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/MainMicAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/GravitySensorAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/RefMicAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/LightSensorAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/MagneticSensorAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/IndicatorAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/BluetoothAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/WifiAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/GPSAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/SDCardAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/BacklightAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/SDCard2AutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/CameraAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/ButtonLightAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/SubCameraAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/HeadsetAutoTest;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/FMAtuoTest;

    aput-object v2, v0, v1

    sput-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private getIntentAtCurrent(I)Landroid/content/Intent;
    .locals 5
    .param p1    # I

    sget-object v2, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.mediatek.FMRadio"

    const-string v4, "com.mediatek.FMRadio.FMRadioActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v2, "AmoiEngineeMode"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object v1, v0

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sget-object v2, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v2, v2, p1

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-object v1, v0

    goto :goto_0
.end method

.method private toNextActivity(I)V
    .locals 3
    .param p1    # I

    add-int/lit8 p1, p1, 0x1

    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    array-length v0, v0

    if-lt p1, v0, :cond_0

    const-string v0, "AutoTestMainActivity"

    const-string v1, "toNextActivity:indexOfActivityName > arrays\'length"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    array-length v0, v0

    add-int/lit8 p1, v0, -0x1

    :cond_0
    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/IndicatorAutoTest;

    if-ne v0, v1, :cond_1

    add-int/lit8 p1, p1, 0x1

    :cond_1
    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/ButtonLightAutoTest;

    if-ne v0, v1, :cond_2

    :cond_2
    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/FlashLightAutoTest;

    if-ne v0, v1, :cond_3

    :cond_3
    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/RefMicAutoTest;

    if-eq v0, v1, :cond_4

    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/MainMicAutoTest;

    if-ne v0, v1, :cond_4

    :cond_4
    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/MagneticSensorAutoTest;

    if-ne v0, v1, :cond_5

    add-int/lit8 p1, p1, 0x1

    :cond_5
    const-string v0, "AutoTestMainActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mainActivity:toNextActivity,nextActivity="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->getIntentAtCurrent(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private toPreActivity(I)V
    .locals 3
    .param p1    # I

    add-int/lit8 p1, p1, -0x1

    if-gez p1, :cond_0

    const-string v0, "AutoTestMainActivity"

    const-string v1, "toPreActivity:indexOfActivity < 0"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    :cond_0
    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/IndicatorAutoTest;

    if-ne v0, v1, :cond_1

    add-int/lit8 p1, p1, -0x1

    :cond_1
    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/ButtonLightAutoTest;

    if-ne v0, v1, :cond_2

    :cond_2
    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/FlashLightAutoTest;

    if-ne v0, v1, :cond_3

    :cond_3
    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/RefMicAutoTest;

    if-eq v0, v1, :cond_4

    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/MainMicAutoTest;

    if-ne v0, v1, :cond_4

    :cond_4
    sget-object v0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->TEST_CLASSES:[Ljava/lang/Class;

    aget-object v0, v0, p1

    const-class v1, Lcom/amoi/AmoiEngineerMode/auto/MagneticSensorAutoTest;

    if-ne v0, v1, :cond_5

    add-int/lit8 p1, p1, -0x1

    :cond_5
    const-string v0, "AutoTestMainActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mainActivity:toPreActivity,PreActivity="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->getIntentAtCurrent(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p2, :pswitch_data_0

    const-string v0, "AutoTestMainActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mainActivity:resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " result error!!!!!!!!!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->finish()V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->toNextActivity(I)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->toPreActivity(I)V

    goto :goto_0

    :pswitch_2
    const-string v0, "AutoTestMainActivity"

    const-string v1, "finish"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v2, "AutoTestMainActivity"

    const-string v3, "onCreate"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    const/16 v2, 0x1a

    const-string v3, "AutoTest"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    if-nez p1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/amoi/AmoiEngineerMode/auto/PhoneInfoAutoTest;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "AutoTestMainActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/auto/AutoTestMainActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method
