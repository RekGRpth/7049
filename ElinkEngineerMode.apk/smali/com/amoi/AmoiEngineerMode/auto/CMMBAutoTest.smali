.class public Lcom/amoi/AmoiEngineerMode/auto/CMMBAutoTest;
.super Lcom/amoi/AmoiEngineerMode/base/CMMBBase;
.source "CMMBAutoTest.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/CMMBBase;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/auto/FinishHandler;->exit(Landroid/app/Activity;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const v1, 0x7f03001f

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/auto/CMMBAutoTest;->setContentView(I)V

    const v1, 0x7f060060

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/auto/CMMBAutoTest;->setTitle(I)V

    const v1, 0x7f070002

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/auto/CMMBAutoTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0600b6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/util/ButtonHelper;->initAutoButtons(Landroid/app/Activity;)V

    invoke-super {p0, p1}, Lcom/amoi/AmoiEngineerMode/base/CMMBBase;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method
