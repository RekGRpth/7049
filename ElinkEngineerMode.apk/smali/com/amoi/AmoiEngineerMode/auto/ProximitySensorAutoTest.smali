.class public Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;
.super Lcom/amoi/AmoiEngineerMode/base/SensorBase;
.source "ProximitySensorAutoTest.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ProximitySensor"


# instance fields
.field private layout:Landroid/widget/RelativeLayout;

.field private mOriginalAccelerometerState:I

.field private msgTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/SensorBase;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->mOriginalAccelerometerState:I

    return-void
.end method

.method private changeBackground(F)V
    .locals 2
    .param p1    # F

    const v0, 0x3dcccccd

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->layout:Landroid/widget/RelativeLayout;

    const v1, -0xbbbbbc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->layout:Landroid/widget/RelativeLayout;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method private closeAccelerometer()V
    .locals 5

    :try_start_0
    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->mOriginalAccelerometerState:I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget v2, p0, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->mOriginalAccelerometerState:I

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v2, "ProximitySensor"

    const-string v3, "error to close Accelerometer"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "ProximitySensor"

    const-string v3, "error to get Accelerometer state"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private openAccelerometer()V
    .locals 4

    iget v1, p0, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->mOriginalAccelerometerState:I

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "ProximitySensor"

    const-string v2, "error to open Accelerometer"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method protected initSensor()V
    .locals 2

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->sm:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->sensor:Landroid/hardware/Sensor;

    return-void
.end method

.method protected initView()V
    .locals 3

    const v1, 0x7f060065

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->setTitle(Ljava/lang/CharSequence;)V

    const v1, 0x7f070051

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->msgTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->msgTextView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->msgTextView:Landroid/widget/TextView;

    const v2, 0x7f060086

    invoke-virtual {p0, v2}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f07004e

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->layout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->layout:Landroid/widget/RelativeLayout;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0600a0

    invoke-virtual {p0, v2}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "--"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->resultTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->closeAccelerometer()V

    invoke-super {p0, p1}, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->openAccelerometer()V

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->onDestroy()V

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->onResume()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->sm:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->sensor:Landroid/hardware/Sensor;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5
    .param p1    # Landroid/hardware/SensorEvent;

    const/4 v4, 0x0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0600a0

    invoke-virtual {p0, v3}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, v1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aget v2, v1, v4

    invoke-direct {p0, v2}, Lcom/amoi/AmoiEngineerMode/auto/ProximitySensorAutoTest;->changeBackground(F)V

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->resultTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
