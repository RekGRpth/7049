.class public Lcom/amoi/AmoiEngineerMode/auto/SubCameraAutoTest;
.super Lcom/amoi/AmoiEngineerMode/base/SubCameraBase;
.source "SubCameraAutoTest.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/SubCameraBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected initButton()V
    .locals 0

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/util/ButtonHelper;->initAutoButtons(Landroid/app/Activity;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/auto/FinishHandler;->exit(Landroid/app/Activity;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/amoi/AmoiEngineerMode/base/SubCameraBase;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "EngineerMode"

    const-string v1, "SubCameraAutoTest onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
