.class public abstract Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;
.super Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;
.source "AudioAgingServiceBase.java"


# static fields
.field protected static final PLAY_TIME:I = 0x82

.field private static final TAG:Ljava/lang/String; = "AudioAgingServiceBase"


# instance fields
.field protected audioManager:Landroid/media/AudioManager;

.field protected beginTime:Ljava/util/Date;

.field protected notificationManager:Landroid/app/NotificationManager;

.field protected playCount:I

.field protected wakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->playCount:I

    return-void
.end method


# virtual methods
.method protected acquireWakeLock()V
    .locals 3

    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->wakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    return-void
.end method

.method protected getContentIntent()Landroid/app/PendingIntent;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {p0, v1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getFailString()Ljava/lang/String;
.end method

.method protected abstract getSuccessString()Ljava/lang/String;
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 9
    .param p1    # Landroid/media/MediaPlayer;

    const v4, 0x7f020008

    iget v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->playCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->playCount:I

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->beginTime:Ljava/util/Date;

    invoke-static {v0, v8}, Lcom/amoi/AmoiEngineerMode/util/TimeHelper;->compareHours(Ljava/util/Date;Ljava/util/Date;)J

    move-result-wide v6

    const-string v0, "EngineerMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AudioAgingServiceBase begin="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->beginTime:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->toLocaleString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; now="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Ljava/util/Date;->toLocaleString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; hours="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; playCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->playCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v2, 0x82

    cmp-long v0, v6, v2

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->getSuccessString()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->stopSelf()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->onCompletion(Landroid/media/MediaPlayer;)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->onCreate()V

    const-string v0, "EngineerMode"

    const-string v1, "AudioAgingServiceBase onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->acquireWakeLock()V

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->notificationManager:Landroid/app/NotificationManager;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->onDestroy()V

    const-string v0, "EngineerMode"

    const-string v1, "AudioAgingServiceBase onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->releaseWakeLock()V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 6
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const v4, 0x7f020001

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->getFailString()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-super {p0, p1, p2, p3}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v0

    return v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->initAudio()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->beginTime:Ljava/util/Date;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->beginTime:Ljava/util/Date;

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method protected releaseWakeLock()V
    .locals 1

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->wakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->wakeLock:Landroid/os/PowerManager$WakeLock;

    :cond_0
    return-void
.end method

.method protected showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I

    new-instance v1, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, p5, p1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->getContentIntent()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, p0, p2, p3, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/AudioAgingServiceBase;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v2, p4, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method
