.class public abstract Lcom/amoi/AmoiEngineerMode/base/BatteryBase;
.super Landroid/app/Activity;
.source "BatteryBase.java"


# static fields
.field static final EXIST_A:I = 0x2

.field static final EXIST_AB:I = 0x0

.field static final EXIST_B:I = 0x1

.field static final EXIST_NONE:I = 0x3

.field private static final INTENT_ACTION:Ljava/lang/String; = "android.intent.action.ACTION_DUAL_BATTERY_CHANGED"

.field private static final TAG:Ljava/lang/String; = "BatteryInfo"

.field private static final UPDATE_TEXTVIEW_BATTERY:I = 0x1

.field private static final UPDATE_TEXTVIEW_BATTERY_STATUS:I = 0x2


# instance fields
.field private battery:Ljava/lang/String;

.field protected exitButton:Landroid/widget/Button;

.field private mBatteryInfoReceiver:Landroid/content/BroadcastReceiver;

.field private mDoul_battery:Z

.field private mHandler:Landroid/os/Handler;

.field private mOtherInfo:Landroid/widget/TextView;

.field private mOtherInfoString:Ljava/lang/String;

.field private mOtherInfoTV:Landroid/widget/TextView;

.field protected nextButton:Landroid/widget/Button;

.field protected prevButton:Landroid/widget/Button;

.field private statusString:Ljava/lang/String;

.field private tv_battery:Landroid/widget/TextView;

.field private tv_batteryStatus:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mDoul_battery:Z

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase$2;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase$2;-><init>(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mBatteryInfoReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;)Z
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;

    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mDoul_battery:Z

    return v0
.end method

.method static synthetic access$100(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mOtherInfoString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mOtherInfoString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$184(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;
    .param p1    # Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mOtherInfoString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mOtherInfoString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mOtherInfoTV:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mOtherInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->battery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->battery:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$484(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;
    .param p1    # Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->battery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->battery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->tv_battery:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->statusString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->statusString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$684(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;
    .param p1    # Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->statusString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->statusString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->tv_batteryStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/BatteryBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected abstract initButton()V
.end method

.method protected initView()V
    .locals 2

    const v1, 0x7f030009

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->setContentView(I)V

    const v1, 0x7f070004

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->prevButton:Landroid/widget/Button;

    const v1, 0x7f070005

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->nextButton:Landroid/widget/Button;

    const v1, 0x7f070006

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->exitButton:Landroid/widget/Button;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mDoul_battery:Z

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iget-boolean v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mDoul_battery:Z

    if-eqz v1, :cond_0

    const-string v1, "android.intent.action.ACTION_DUAL_BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :cond_0
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mBatteryInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const v1, 0x7f070012

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->tv_battery:Landroid/widget/TextView;

    const v1, 0x7f070014

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->tv_batteryStatus:Landroid/widget/TextView;

    const v1, 0x7f070016

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mOtherInfoTV:Landroid/widget/TextView;

    const v1, 0x7f070015

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mOtherInfo:Landroid/widget/TextView;

    new-instance v1, Lcom/amoi/AmoiEngineerMode/base/BatteryBase$1;

    invoke-direct {v1, p0}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase$1;-><init>(Lcom/amoi/AmoiEngineerMode/base/BatteryBase;)V

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->initButton()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->mBatteryInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/BatteryBase;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected abstract showButton(I)V
.end method
