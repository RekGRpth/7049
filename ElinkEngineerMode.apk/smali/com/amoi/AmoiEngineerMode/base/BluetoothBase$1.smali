.class Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;


# direct methods
.method constructor <init>(Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;)V
    .locals 0

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const v6, 0x7f060053

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v4, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "android.bluetooth.adapter.extra.STATE"

    const/4 v5, -0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->testingView:Landroid/widget/TextView;

    const v5, 0x7f060040

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->testingView:Landroid/widget/TextView;

    const v5, 0x7f060041

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    goto :goto_0

    :cond_1
    const-string v4, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->testingView:Landroid/widget/TextView;

    const v5, 0x7f0600ad

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_2
    const-string v4, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-boolean v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->mSuccess:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget v5, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->mDiscoveryTimes:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->mDiscoveryTimes:I

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->mDiscoveryTimes:I

    const/4 v5, 0x5

    if-lt v4, v5, :cond_3

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->testingView:Landroid/widget/TextView;

    const/high16 v5, -0x10000

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->testingView:Landroid/widget/TextView;

    const v5, 0x7f0600af

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    invoke-virtual {v4, v6}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->infoView:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    invoke-virtual {v4}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->showButton()V

    goto/16 :goto_0

    :cond_3
    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    goto/16 :goto_0

    :cond_4
    const-string v4, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->mSuccess:Z

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->testingView:Landroid/widget/TextView;

    const v5, -0xff0100

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->testingView:Landroid/widget/TextView;

    const v5, 0x7f0600ae

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    const-string v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    invoke-virtual {v4, v6}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    const v5, 0x7f0600ac

    invoke-virtual {v4, v5}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->infoView:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    iget-object v4, v4, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;

    invoke-virtual {v4}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->showButton()V

    goto/16 :goto_0

    :cond_5
    const-string v4, "EngineerMode"

    const-string v5, "Bluetooth BluetoothDevice.ACTION_FOUND not has a device"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
