.class public abstract Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;
.super Lcom/amoi/AmoiEngineerMode/base/TestBase;
.source "BluetoothBase.java"


# static fields
.field protected static final MAX_DISCOVERY_TIME:I = 0x5

.field protected static final TAG:Ljava/lang/String; = "Bluetooth"


# instance fields
.field protected bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field protected infoView:Landroid/widget/TextView;

.field protected mDiscoveryTimes:I

.field protected final mReceiver:Landroid/content/BroadcastReceiver;

.field protected mSuccess:Z

.field protected originalState:Z

.field protected testingView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;-><init>()V

    iput-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->mSuccess:Z

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->mDiscoveryTimes:I

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase$1;-><init>(Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method protected closeBluetooth()V
    .locals 2

    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->originalState:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    const-string v0, "EngineerMode"

    const-string v1, "Bluetooth close bluetooth..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected initBluetooth()V
    .locals 2

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->originalState:Z

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->openBluetooth()V

    return-void
.end method

.method protected abstract initButton()V
.end method

.method protected initView()V
    .locals 2

    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->setContentView(I)V

    const v0, 0x7f06000c

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f070002

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->testingView:Landroid/widget/TextView;

    const v0, 0x7f070003

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->infoView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->infoView:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->infoView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->initButton()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->initBluetooth()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onDestroy()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->closeBluetooth()V

    return-void
.end method

.method protected openBluetooth()V
    .locals 2

    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->originalState:Z

    if-nez v0, :cond_0

    const-string v0, "EngineerMode"

    const-string v1, "Bluetooth start bluetooth..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/BluetoothBase;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    goto :goto_0
.end method

.method protected abstract showButton()V
.end method
