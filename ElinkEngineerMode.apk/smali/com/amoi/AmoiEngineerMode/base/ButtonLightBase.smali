.class public abstract Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;
.super Landroid/app/Activity;
.source "ButtonLightBase.java"


# static fields
.field protected static final MSG_CHANGE_LIGHT:I = 0x1


# instance fields
.field protected handler:Landroid/os/Handler;

.field protected lightController:Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;

.field protected originalLight:F

.field protected switcher:Z

.field protected timer:Ljava/util/Timer;

.field protected timerTask:Ljava/util/TimerTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->switcher:Z

    return-void
.end method


# virtual methods
.method protected changeLight()V
    .locals 3

    const-string v0, "EngineerMode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ButtonLightBase turn on button light: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->switcher:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->switcher:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;

    invoke-virtual {v0}, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;->turnOn()V

    :goto_0
    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->switcher:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->switcher:Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;

    invoke-virtual {v0}, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;->turnOff()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected abstract initButton()V
.end method

.method protected initHandler()V
    .locals 1

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase$1;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase$1;-><init>(Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->handler:Landroid/os/Handler;

    return-void
.end method

.method protected initTimerAndTimerTask()V
    .locals 6

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->timer:Ljava/util/Timer;

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase$2;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase$2;-><init>(Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->timerTask:Ljava/util/TimerTask;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->timerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method protected initView()V
    .locals 2

    const v1, 0x7f03001f

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->setContentView(I)V

    const v1, 0x7f06000b

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->setTitle(Ljava/lang/CharSequence;)V

    const v1, 0x7f070002

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f06005a

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f070003

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f06005b

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "EngineerMode"

    const-string v1, "ButtonLightBase onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->initButton()V

    new-instance v0, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;-><init>(Landroid/view/Window;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;

    invoke-virtual {v0}, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;->getLightState()F

    move-result v0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->originalLight:F

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->initHandler()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->initTimerAndTimerTask()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "EngineerMode"

    const-string v1, "ButtonLightBase onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->stopTimerAndTimerTask()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;

    iget v1, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->originalLight:F

    invoke-virtual {v0, v1}, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;->setLightState(F)V

    return-void
.end method

.method protected stopTimerAndTimerTask()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->timer:Ljava/util/Timer;

    :cond_0
    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/ButtonLightBase;->timerTask:Ljava/util/TimerTask;

    return-void
.end method
