.class public abstract Lcom/amoi/AmoiEngineerMode/base/CameraBase;
.super Landroid/app/Activity;
.source "CameraBase.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amoi/AmoiEngineerMode/base/CameraBase$AutoFocusCallback;
    }
.end annotation


# static fields
.field protected static final MSG_RETAKE_PICTURE:I = 0x0

.field protected static final TAG:Ljava/lang/String; = "CameraBase"

.field protected static final fileName:Ljava/lang/String; = "/mnt/sdcard/CameraBase_picture_test.jpg"

.field private static mContext:Landroid/content/Context;


# instance fields
.field private handler:Landroid/os/Handler;

.field private jpegCallback:Landroid/hardware/Camera$PictureCallback;

.field private mAutoFocusCallBack:Lcom/amoi/AmoiEngineerMode/base/CameraBase$AutoFocusCallback;

.field private mCamera:Landroid/hardware/Camera;

.field private mCameraId:I

.field private mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceView:Landroid/view/SurfaceView;

.field private rawPictureCallback:Landroid/hardware/Camera$PictureCallback;

.field private retakeButton:Landroid/widget/Button;

.field private takeButton:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/CameraBase$AutoFocusCallback;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase$AutoFocusCallback;-><init>(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mAutoFocusCallBack:Lcom/amoi/AmoiEngineerMode/base/CameraBase$AutoFocusCallback;

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/CameraBase$4;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase$4;-><init>(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/CameraBase$5;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase$5;-><init>(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->rawPictureCallback:Landroid/hardware/Camera$PictureCallback;

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/CameraBase$6;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase$6;-><init>(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->jpegCallback:Landroid/hardware/Camera$PictureCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/CameraBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->takeButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/CameraBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->retakeButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)Landroid/hardware/Camera$ShutterCallback;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/CameraBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    return-object v0
.end method

.method static synthetic access$300(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)Landroid/hardware/Camera$PictureCallback;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/CameraBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->rawPictureCallback:Landroid/hardware/Camera$PictureCallback;

    return-object v0
.end method

.method static synthetic access$400(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)Landroid/hardware/Camera$PictureCallback;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/CameraBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->jpegCallback:Landroid/hardware/Camera$PictureCallback;

    return-object v0
.end method

.method static synthetic access$500(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)Landroid/hardware/Camera;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/CameraBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    return-object v0
.end method

.method private buildView()V
    .locals 3

    const/16 v2, 0x8

    const v0, 0x7f07001d

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->takeButton:Landroid/widget/Button;

    const v0, 0x7f07001e

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->retakeButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->takeButton:Landroid/widget/Button;

    new-instance v1, Lcom/amoi/AmoiEngineerMode/base/CameraBase$2;

    invoke-direct {v1, p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase$2;-><init>(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->retakeButton:Landroid/widget/Button;

    new-instance v1, Lcom/amoi/AmoiEngineerMode/base/CameraBase$3;

    invoke-direct {v1, p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase$3;-><init>(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->takeButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->retakeButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->takeButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->retakeButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f0600d7

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->showToast(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getDisplayOrientation(II)I
    .locals 4
    .param p0    # I
    .param p1    # I

    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {p1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v2, p0

    rem-int/lit16 v1, v2, 0x168

    rsub-int v2, v1, 0x168

    rem-int/lit16 v1, v2, 0x168

    :goto_0
    return v1

    :cond_0
    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v2, p0

    add-int/lit16 v2, v2, 0x168

    rem-int/lit16 v1, v2, 0x168

    goto :goto_0
.end method

.method public static getDisplayRotation(Landroid/app/Activity;)I
    .locals 3
    .param p0    # Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v1

    :pswitch_1
    const/16 v1, 0x5a

    goto :goto_0

    :pswitch_2
    const/16 v1, 0xb4

    goto :goto_0

    :pswitch_3
    const/16 v1, 0x10e

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getOptimalPreviewSize(Landroid/app/Activity;Ljava/util/List;D)Landroid/hardware/Camera$Size;
    .locals 15
    .param p0    # Landroid/app/Activity;
    .param p2    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;D)",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    const-wide v0, 0x3f50624dd2f1a9fcL

    if-nez p1, :cond_1

    const/4 v6, 0x0

    :cond_0
    return-object v6

    :cond_1
    const/4 v6, 0x0

    const-wide v4, 0x7fefffffffffffffL

    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v11

    invoke-interface {v11}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v11

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v12

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v10

    if-gtz v10, :cond_2

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v10

    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/hardware/Camera$Size;

    iget v11, v9, Landroid/hardware/Camera$Size;->width:I

    int-to-double v11, v11

    iget v13, v9, Landroid/hardware/Camera$Size;->height:I

    int-to-double v13, v13

    div-double v7, v11, v13

    sub-double v11, v7, p2

    invoke-static {v11, v12}, Ljava/lang/Math;->abs(D)D

    move-result-wide v11

    const-wide v13, 0x3f50624dd2f1a9fcL

    cmpl-double v11, v11, v13

    if-gtz v11, :cond_3

    iget v11, v9, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v11, v11

    cmpg-double v11, v11, v4

    if-gez v11, :cond_3

    move-object v6, v9

    iget v11, v9, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v4, v11

    goto :goto_0

    :cond_4
    if-nez v6, :cond_0

    const-string v11, "CameraBase"

    const-string v12, "No preview size match the aspect ratio"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-wide v4, 0x7fefffffffffffffL

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/hardware/Camera$Size;

    iget v11, v9, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v11, v11

    cmpg-double v11, v11, v4

    if-gez v11, :cond_5

    move-object v6, v9

    iget v11, v9, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v4, v11

    goto :goto_1
.end method

.method private static isSupported(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setDisplayOrientation()V
    .locals 3

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v1

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCameraId:I

    invoke-static {v1, v2}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->getDisplayOrientation(II)I

    move-result v0

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2, v0}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    return-void
.end method

.method private setPreviewSize(Landroid/hardware/Camera$Parameters;)V
    .locals 16
    .param p1    # Landroid/hardware/Camera$Parameters;

    invoke-static {}, Lcom/amoi/AmoiEngineerMode/base/CameraHolder;->instance()Lcom/amoi/AmoiEngineerMode/base/CameraHolder;

    move-result-object v12

    invoke-virtual {v12}, Lcom/amoi/AmoiEngineerMode/base/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCameraId:I

    aget-object v12, v12, v13

    iget v3, v12, Landroid/hardware/Camera$CameraInfo;->orientation:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v10

    if-eqz v3, :cond_0

    const/16 v12, 0xb4

    if-ne v3, v12, :cond_2

    :cond_0
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/hardware/Camera$Size;

    iget v12, v8, Landroid/hardware/Camera$Size;->width:I

    iget v13, v7, Landroid/hardware/Camera$Size;->width:I

    if-ne v12, v13, :cond_1

    iget v12, v8, Landroid/hardware/Camera$Size;->height:I

    iget v13, v7, Landroid/hardware/Camera$Size;->height:I

    if-ne v12, v13, :cond_1

    iget v12, v7, Landroid/hardware/Camera$Size;->height:I

    iget v13, v7, Landroid/hardware/Camera$Size;->width:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    const/4 v11, 0x0

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/hardware/Camera$Size;

    iget v11, v8, Landroid/hardware/Camera$Size;->width:I

    iget v4, v8, Landroid/hardware/Camera$Size;->height:I

    goto :goto_1

    :cond_3
    const-wide/16 v1, 0x0

    if-eqz v3, :cond_4

    const/16 v12, 0xb4

    if-ne v3, v12, :cond_6

    :cond_4
    if-le v4, v11, :cond_6

    int-to-double v12, v4

    int-to-double v14, v11

    div-double v1, v12, v14

    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-static {v0, v9, v1, v2}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->getOptimalPreviewSize(Landroid/app/Activity;Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object v6

    if-eqz v3, :cond_5

    const/16 v12, 0xb4

    if-ne v3, v12, :cond_7

    :cond_5
    iget v12, v6, Landroid/hardware/Camera$Size;->height:I

    iget v13, v6, Landroid/hardware/Camera$Size;->width:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    :goto_3
    return-void

    :cond_6
    int-to-double v12, v11

    int-to-double v14, v4

    div-double v1, v12, v14

    goto :goto_2

    :cond_7
    iget v12, v6, Landroid/hardware/Camera$Size;->width:I

    iget v13, v6, Landroid/hardware/Camera$Size;->height:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    goto :goto_3
.end method


# virtual methods
.method protected deleteFile()V
    .locals 2

    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/sdcard/CameraBase_picture_test.jpg"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    return-void
.end method

.method protected deleteSurfaceView()V
    .locals 2

    const v1, 0x7f07001b

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public finish()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->takeButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->retakeButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->deleteSurfaceView()V

    invoke-super {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method protected getCameraType()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract initButton()V
.end method

.method protected initCamera()V
    .locals 2

    const v0, 0x7f07001c

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mSurfaceView:Landroid/view/SurfaceView;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->getCameraType()I

    move-result v0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCameraId:I

    return-void
.end method

.method protected initHandler()V
    .locals 1

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/CameraBase$1;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase$1;-><init>(Lcom/amoi/AmoiEngineerMode/base/CameraBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->handler:Landroid/os/Handler;

    return-void
.end method

.method protected initView()V
    .locals 2

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->requestWindowFeature(I)Z

    const v0, 0x7f03000c

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->setContentView(I)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->initButton()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->initCamera()V

    sput-object p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->buildView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->initHandler()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->deleteFile()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected retakePicture()V
    .locals 5

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->deleteFile()V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    const-string v1, "auto"

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->cancelAutoFocus()V

    :cond_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->startPreview()V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->retakeButton:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->handler:Landroid/os/Handler;

    const/4 v2, 0x0

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method protected showToast(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 5
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v2, "EngineerMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CameraBase surfaceChanged w="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " h="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->setPreviewSize(Landroid/hardware/Camera$Parameters;)V

    const-string v2, "auto"

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "auto"

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->setDisplayOrientation()V

    :try_start_0
    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2, p1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->startPreview()V

    return-void

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->release()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 5
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->release()V

    iput-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    :cond_0
    const-string v2, "CameraBase"

    const-string v3, "chenpx --- open camera"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCameraId:I

    invoke-static {v2}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v2

    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    const-string v2, "CameraBase"

    const-string v3, "chenpx --- open camera success!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->setDisplayOrientation()V

    :try_start_0
    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2, p1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "EngineerMode"

    const-string v3, "CameraBase error to create camera"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->release()V

    iput-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "EngineerMode"

    const-string v1, "CameraBase surfaceDestroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    return-void
.end method

.method protected takePicture()V
    .locals 6

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->takeButton:Landroid/widget/Button;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    const-string v2, "auto"

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mAutoFocusCallBack:Lcom/amoi/AmoiEngineerMode/base/CameraBase$AutoFocusCallback;

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mCamera:Landroid/hardware/Camera;

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->rawPictureCallback:Landroid/hardware/Camera$PictureCallback;

    iget-object v5, p0, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->jpegCallback:Landroid/hardware/Camera$PictureCallback;

    invoke-virtual {v2, v3, v4, v5}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->finish()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/CameraBase;->finish()V

    goto :goto_0
.end method
