.class public abstract Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;
.super Landroid/app/Activity;
.source "FlashLightBase.java"

# interfaces
.implements Lcom/amoi/AmoiEngineerMode/base/Retestable;


# static fields
.field protected static final MSG_CHANGE_LIGHT:I = 0x2

.field protected static final MSG_END_FLASH:I = 0x3

.field protected static final MSG_TURN_OFF_FLASH:I = 0x0

.field protected static final MSG_TURN_ON_FLASH:I = 0x1


# instance fields
.field protected exitButton:Landroid/widget/Button;

.field private handler:Landroid/os/Handler;

.field private mCamera:Landroid/hardware/Camera;

.field private mCount:I

.field protected nextButton:Landroid/widget/Button;

.field protected prevButton:Landroid/widget/Button;

.field private retestButton:Landroid/widget/Button;

.field private timer:Ljava/util/Timer;

.field private timerTask:Ljava/util/TimerTask;

.field private tipsView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCamera:Landroid/hardware/Camera;

    const/4 v0, 0x0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCount:I

    return-void
.end method

.method static synthetic access$000(Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;)V
    .locals 0
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->openFlashLight()V

    return-void
.end method

.method static synthetic access$100(Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;)V
    .locals 0
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->closeFlashLight()V

    return-void
.end method

.method static synthetic access$200(Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;)V
    .locals 0
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->changeFlashLight()V

    return-void
.end method

.method static synthetic access$300(Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->tipsView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->retestButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$500(Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private changeFlashLight()V
    .locals 3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->isAgingTest()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCount:I

    const/16 v1, 0xb

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCount:I

    rem-int/lit8 v0, v0, 0x2

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    iget v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCount:I

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->openFlashLight()V

    goto :goto_1

    :pswitch_1
    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->closeFlashLight()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private closeFlashLight()V
    .locals 1

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCamera:Landroid/hardware/Camera;

    :cond_0
    return-void
.end method

.method private initView()V
    .locals 2

    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->setContentView(I)V

    const v0, 0x7f0600dd

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f070002

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->tipsView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->tipsView:Landroid/widget/TextView;

    const v1, 0x7f0600dc

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/util/ButtonHelper;->hideAutoButtons(Landroid/app/Activity;)V

    const v0, 0x7f070047

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->retestButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->retestButton:Landroid/widget/Button;

    new-instance v1, Lcom/amoi/AmoiEngineerMode/base/RetestButtonListener;

    invoke-direct {v1, p0}, Lcom/amoi/AmoiEngineerMode/base/RetestButtonListener;-><init>(Lcom/amoi/AmoiEngineerMode/base/Retestable;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private openFlashLight()V
    .locals 2

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCamera:Landroid/hardware/Camera;

    if-nez v1, :cond_0

    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v1

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCamera:Landroid/hardware/Camera;

    :cond_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    const-string v1, "torch"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->startPreview()V

    return-void
.end method


# virtual methods
.method protected abstract initButton()V
.end method

.method protected initTimerAndTimerTask()V
    .locals 6

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->timer:Ljava/util/Timer;

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase$2;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase$2;-><init>(Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->timerTask:Ljava/util/TimerTask;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->timerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x12c

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method protected inithandler()V
    .locals 1

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase$1;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase$1;-><init>(Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->handler:Landroid/os/Handler;

    return-void
.end method

.method public isAgingTest()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->initButton()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->inithandler()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->handler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->closeFlashLight()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->stopTimerAndTimerTask()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->retest()V

    return-void
.end method

.method public retest()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->mCount:I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->tipsView:Landroid/widget/TextView;

    const v1, 0x7f0600dc

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->retestButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->initTimerAndTimerTask()V

    return-void
.end method

.method protected abstract showButton()V
.end method

.method protected stopTimerAndTimerTask()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->timer:Ljava/util/Timer;

    :cond_0
    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/FlashLightBase;->timerTask:Ljava/util/TimerTask;

    return-void
.end method
