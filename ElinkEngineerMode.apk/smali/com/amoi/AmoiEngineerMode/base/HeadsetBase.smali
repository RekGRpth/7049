.class public abstract Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;
.super Landroid/app/Activity;
.source "HeadsetBase.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$HeadsetTestThread;,
        Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$MediaButtonBroadcastReceiver;
    }
.end annotation


# static fields
.field protected static final MSG_UPDATE_TEXT_HEADSETIN:I = 0x2

.field protected static final MSG_UPDATE_TEXT_TESTING:I = 0x1


# instance fields
.field protected audioManager:Landroid/media/AudioManager;

.field protected handler:Landroid/os/Handler;

.field protected isTesting:Z

.field protected mTestThread:Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$HeadsetTestThread;

.field protected receiver:Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$MediaButtonBroadcastReceiver;

.field protected startButton:Landroid/widget/Button;

.field protected textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->isTesting:Z

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$HeadsetTestThread;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$HeadsetTestThread;-><init>(Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->mTestThread:Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$HeadsetTestThread;

    return-void
.end method


# virtual methods
.method protected abstract initButton()V
.end method

.method protected initHandler()V
    .locals 1

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$1;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$1;-><init>(Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->handler:Landroid/os/Handler;

    return-void
.end method

.method protected initHeadset()V
    .locals 2

    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->audioManager:Landroid/media/AudioManager;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$MediaButtonBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$MediaButtonBroadcastReceiver;-><init>(Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;)V

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->receiver:Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$MediaButtonBroadcastReceiver;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->receiver:Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$MediaButtonBroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->startTest()V

    return-void
.end method

.method protected initView()V
    .locals 1

    const v0, 0x7f030020

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->setContentView(I)V

    const v0, 0x7f06003d

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->setTitle(I)V

    const v0, 0x7f070002

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->textView:Landroid/widget/TextView;

    const v0, 0x7f07004a

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->startButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->startButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->startTest()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f07004a
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "EngineerMode"

    const-string v1, "HeadsetBase onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->initButton()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->initHandler()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->initHeadset()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "EngineerMode"

    const-string v1, "HeadsetBase onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->receiver:Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$MediaButtonBroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->stopTest()V

    return-void
.end method

.method protected startTest()V
    .locals 2

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->isTesting:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->mTestThread:Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$HeadsetTestThread;

    invoke-virtual {v0}, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase$HeadsetTestThread;->start()V

    :cond_1
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method protected stopTest()V
    .locals 1

    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->isTesting:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/HeadsetBase;->isTesting:Z

    :cond_0
    return-void
.end method
