.class Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$1;
.super Landroid/os/Handler;
.source "IndicatorBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->initHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;


# direct methods
.method constructor <init>(Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;)V
    .locals 0

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/LightController;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;

    iget v1, v1, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->count:I

    rem-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->showLED(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;

    iget v1, v0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->count:I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/LightController;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;

    iget v1, v1, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->count:I

    rem-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->isShowLED(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;

    iget v1, v0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->count:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
