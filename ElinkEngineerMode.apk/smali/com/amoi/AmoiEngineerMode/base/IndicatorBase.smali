.class public abstract Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;
.super Landroid/app/Activity;
.source "IndicatorBase.java"


# static fields
.field protected static final MSG_CHANGE_LED_COLOR:I = 0x1

.field protected static final MSG_CHANGE_LED_ON_OFF:I = 0x2

.field private static final TAG:Ljava/lang/String; = "IndicatorBase"


# instance fields
.field protected count:I

.field protected handler:Landroid/os/Handler;

.field protected lightController:Lcom/amoi/AmoiEngineerMode/util/LightController;

.field protected originalGreen:Ljava/lang/String;

.field protected originalRed:Ljava/lang/String;

.field protected timer:Ljava/util/Timer;

.field protected timerTask:Ljava/util/TimerTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "0"

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->originalRed:Ljava/lang/String;

    const-string v0, "0"

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->originalGreen:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected abstract initButton()V
.end method

.method protected initHandler()V
    .locals 1

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$1;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$1;-><init>(Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->handler:Landroid/os/Handler;

    return-void
.end method

.method protected initIndicator()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/amoi/AmoiEngineerMode/util/LightController;

    invoke-direct {v0}, Lcom/amoi/AmoiEngineerMode/util/LightController;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/LightController;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/LightController;

    invoke-virtual {v0, v2}, Lcom/amoi/AmoiEngineerMode/util/LightController;->getLightState(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->originalRed:Ljava/lang/String;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/LightController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->getLightState(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->originalGreen:Ljava/lang/String;

    iput v2, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->count:I

    return-void
.end method

.method protected initTimerAndTimerTask()V
    .locals 6

    const-wide/16 v2, 0x320

    const-string v0, "EngineerMode"

    const-string v1, "IndicatorBase initTimerAndTimerTask"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->timer:Ljava/util/Timer;

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$2;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase$2;-><init>(Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->timerTask:Ljava/util/TimerTask;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->timerTask:Ljava/util/TimerTask;

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method protected initView()V
    .locals 2

    const v1, 0x7f03001f

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->setContentView(I)V

    const v1, 0x7f06000e

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->setTitle(Ljava/lang/CharSequence;)V

    const v1, 0x7f070002

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f06005c

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f070003

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f06005e

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "EngineerMode"

    const-string v1, "IndicatorBase onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->initButton()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->initIndicator()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->initHandler()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "EngineerMode"

    const-string v1, "IndicatorBase onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->stoptimerAndTimerTask()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/LightController;

    invoke-virtual {v0}, Lcom/amoi/AmoiEngineerMode/util/LightController;->turnOffLEDs()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->stoptimerAndTimerTask()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->lightController:Lcom/amoi/AmoiEngineerMode/util/LightController;

    invoke-virtual {v0}, Lcom/amoi/AmoiEngineerMode/util/LightController;->turnOffLEDs()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->initTimerAndTimerTask()V

    return-void
.end method

.method protected stoptimerAndTimerTask()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "EngineerMode"

    const-string v1, "IndicatorBase stoptimerAndTimerTask"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->timer:Ljava/util/Timer;

    :cond_0
    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/IndicatorBase;->timerTask:Ljava/util/TimerTask;

    return-void
.end method
