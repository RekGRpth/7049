.class public Lcom/amoi/AmoiEngineerMode/base/InfoItem;
.super Ljava/lang/Object;
.source "InfoItem.java"


# static fields
.field public static final LEVEL_HIGH:I = 0x2

.field public static final LEVEL_NORMAL:I = 0x1


# instance fields
.field private label:Ljava/lang/String;

.field private level:I

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->level:I

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->label:Ljava/lang/String;

    iput-object p2, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->value:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->level:I

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->label:Ljava/lang/String;

    iput-object p2, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->value:Ljava/lang/String;

    iput p3, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->level:I

    return-void
.end method


# virtual methods
.method public getLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getLevel()I
    .locals 1

    iget v0, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->level:I

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->value:Ljava/lang/String;

    return-object v0
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->label:Ljava/lang/String;

    return-void
.end method

.method public setLevel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->level:I

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->value:Ljava/lang/String;

    return-void
.end method
