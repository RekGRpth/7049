.class public Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "InfoListViewAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private inflater:Landroid/view/LayoutInflater;

.field private items:[Lcom/amoi/AmoiEngineerMode/base/InfoItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lcom/amoi/AmoiEngineerMode/base/InfoItem;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # [Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;->items:[Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;->items:[Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;->items:[Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;->items:[Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    aget-object v0, v3, p1

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;->inflater:Landroid/view/LayoutInflater;

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;->context:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    iput-object v3, p0, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;->inflater:Landroid/view/LayoutInflater;

    :cond_0
    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;->inflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030015

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const v3, 0x7f070041

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f070042

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;->getLevel()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    return-object v1
.end method
