.class public abstract Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;
.super Lcom/amoi/AmoiEngineerMode/base/TestBase;
.source "KeystrokeBase.java"


# static fields
.field protected static final KEYCODES:[I

.field private static final TAG:Ljava/lang/String; = "KeystrokeBase"


# instance fields
.field protected keysView:Landroid/widget/GridView;

.field protected pressedKeyCount:I

.field protected tipsView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->KEYCODES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x18
        0x52
        0x3
        0x4
        0x54
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->pressedKeyCount:I

    return-void
.end method


# virtual methods
.method enableHomeKeyDispatched(Z)V
    .locals 5
    .param p1    # Z

    const-string v2, "EngineerMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableHomeKeyDispatched, enable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    if-eqz p1, :cond_0

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, -0x80000000

    or-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void

    :cond_0
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, 0x7fffffff

    and-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_0
.end method

.method protected abstract initButton()V
.end method

.method protected initView()V
    .locals 4

    const v1, 0x7f030017

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->setContentView(I)V

    const v1, 0x7f06000d

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->setTitle(Ljava/lang/CharSequence;)V

    const v1, 0x7f070043

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->tipsView:Landroid/widget/TextView;

    const v1, 0x7f070044

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->keysView:Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->keysView:Landroid/widget/GridView;

    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x1090003

    invoke-direct {v2, p0, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/util/ButtonHelper;->hideAutoButtons(Landroid/app/Activity;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->initButton()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->enableHomeKeyDispatched(Z)V

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const-string v2, "EngineerMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "KeystrokeBase key: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " repeat:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->KEYCODES:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    sget-object v2, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->KEYCODES:[I

    aget v2, v2, v0

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->keysView:Landroid/widget/GridView;

    invoke-virtual {v2, v0}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->pressedKeyCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->pressedKeyCount:I

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->pressedKeyCount:I

    sget-object v3, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->KEYCODES:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->tipsView:Landroid/widget/TextView;

    const v3, 0x7f060024

    invoke-virtual {p0, v3}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->showButton()V

    :cond_0
    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_1
.end method

.method protected onPause()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->enableHomeKeyDispatched(Z)V

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/KeystrokeBase;->enableHomeKeyDispatched(Z)V

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onResume()V

    return-void
.end method

.method protected abstract showButton()V
.end method
