.class public abstract Lcom/amoi/AmoiEngineerMode/base/LcdBase;
.super Landroid/app/Activity;
.source "LcdBase.java"


# static fields
.field protected static final CHANGE_DELAYTIME:I = 0x320

.field protected static final COLORS:[I

.field protected static final MSG_CHANGE_COLOR:I = 0x1


# instance fields
.field protected currentColor:I

.field protected handler:Landroid/os/Handler;

.field protected layout:Landroid/widget/RelativeLayout;

.field protected timer:Ljava/util/Timer;

.field protected timerTask:Ljava/util/TimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->COLORS:[I

    return-void

    nop

    :array_0
    .array-data 4
        -0x10000
        -0xff0100
        -0xffff01
        -0x1000000
        -0x1
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->currentColor:I

    return-void
.end method


# virtual methods
.method protected abstract changeColor()V
.end method

.method protected initHandler()V
    .locals 1

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/LcdBase$2;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/LcdBase$2;-><init>(Lcom/amoi/AmoiEngineerMode/base/LcdBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->handler:Landroid/os/Handler;

    return-void
.end method

.method protected initTimerAndTimerTask()V
    .locals 6

    const-wide/16 v2, 0x320

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->timer:Ljava/util/Timer;

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/LcdBase$3;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/LcdBase$3;-><init>(Lcom/amoi/AmoiEngineerMode/base/LcdBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->timerTask:Ljava/util/TimerTask;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->timerTask:Ljava/util/TimerTask;

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method protected initView()V
    .locals 2

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->requestWindowFeature(I)Z

    const v0, 0x7f030018

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->setContentView(I)V

    const v0, 0x7f070045

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->layout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->layout:Landroid/widget/RelativeLayout;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->layout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/amoi/AmoiEngineerMode/base/LcdBase$1;

    invoke-direct {v1, p0}, Lcom/amoi/AmoiEngineerMode/base/LcdBase$1;-><init>(Lcom/amoi/AmoiEngineerMode/base/LcdBase;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->initHandler()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->initTimerAndTimerTask()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->stoptimerAndTimerTask()V

    return-void
.end method

.method protected stoptimerAndTimerTask()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->timer:Ljava/util/Timer;

    :cond_0
    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/LcdBase;->timerTask:Ljava/util/TimerTask;

    return-void
.end method
