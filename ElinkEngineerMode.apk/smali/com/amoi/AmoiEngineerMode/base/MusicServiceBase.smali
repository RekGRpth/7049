.class public abstract Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;
.super Landroid/app/Service;
.source "MusicServiceBase.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase$PlayerHandler;
    }
.end annotation


# static fields
.field protected static final INTERVAL_TIME:I = 0xbb8

.field protected static final MSG_COMPLETION:I = 0x1

.field public static final MUSIC_RAW_ID:Ljava/lang/String; = "music_raw_id"

.field private static final TAG:Ljava/lang/String; = "MusicServiceBase"


# instance fields
.field protected audioManager:Landroid/media/AudioManager;

.field protected mediaPlayer:Landroid/media/MediaPlayer;

.field protected originalVolume:I

.field private volatile playerHandler:Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase$PlayerHandler;

.field private volatile playerLooper:Landroid/os/Looper;

.field protected streamType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method protected getVolume()I
    .locals 3

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->audioManager:Landroid/media/AudioManager;

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->streamType:I

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    return v0
.end method

.method protected initAudio()V
    .locals 4

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->audioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->streamType:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->originalVolume:I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->audioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->streamType:I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->getVolume()I

    move-result v2

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    iget v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->streamType:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    :cond_0
    return-void
.end method

.method protected abstract initStreamType()V
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->playerHandler:Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase$PlayerHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase$PlayerHandler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onCreate()V
    .locals 3

    const-string v1, "EngineerMode"

    const-string v2, "MusicServiceBase onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->audioManager:Landroid/media/AudioManager;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MusicService"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->playerLooper:Landroid/os/Looper;

    new-instance v1, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase$PlayerHandler;

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->playerLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase$PlayerHandler;-><init>(Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->playerHandler:Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase$PlayerHandler;

    return-void
.end method

.method public onDestroy()V
    .locals 5

    const-string v1, "EngineerMode"

    const-string v2, "MusicServiceBase onDestroy()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->playerLooper:Landroid/os/Looper;

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    :try_start_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->audioManager:Landroid/media/AudioManager;

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->streamType:I

    iget v3, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->originalVolume:I

    const/16 v4, 0x8

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "EngineerMode"

    const-string v2, "MusicServiceBase \u505c\u6b62MediaPlayer\u51fa\u73b0\u5f02\u5e38"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "error occurred in MediaPlayer\uff1a"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sparse-switch p2, :sswitch_data_0

    :goto_0
    const-string v1, " extra:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, "EngineerMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MusicServiceBase"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->stopSelf()V

    const/4 v1, 0x1

    return v1

    :sswitch_0
    const-string v1, "(MEDIA_ERROR_UNKNOWN)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :sswitch_1
    const-string v1, "(MEDIA_ERROR_SERVER_DIED)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->initStreamType()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->initAudio()V

    const-string v1, "music_raw_id"

    const v2, 0x7f040001

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->startPlayer(I)V

    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method protected startPlayer(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->streamType:I

    invoke-static {p0, p1, v0}, Lcom/amoi/AmoiEngineerMode/util/MediaPlayerHelper;->create(Landroid/content/Context;II)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "EngineerMode"

    const-string v1, "MusicServiceBase error to create media player"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/MusicServiceBase;->stopSelf()V

    goto :goto_0
.end method
