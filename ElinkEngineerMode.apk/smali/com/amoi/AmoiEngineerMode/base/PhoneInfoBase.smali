.class public abstract Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;
.super Landroid/app/Activity;
.source "PhoneInfoBase.java"


# static fields
.field protected static final BASEBAND:Ljava/lang/String; = "gsm.version.baseband"

.field protected static final ENGINEER_VERSION:Ljava/lang/String; = "AMOI-20120316-V2.0"

.field public static final FILE_GSM:Ljava/lang/String; = "/local/nvm/GsmCalData.nvm"

.field public static final FILE_MANUFACTORY:Ljava/lang/String; = "/local/nvm/ManufactoryInf.nvm"

.field public static final FILE_TD:Ljava/lang/String; = "/local/nvm/TdCalData.nvm"

.field protected static final GEMINI_SIM_1:I = 0x0

.field protected static final GEMINI_SIM_2:I = 0x1

.field protected static final REVISION:Ljava/lang/String; = "Revision"


# instance fields
.field protected ap_build:Ljava/lang/String;

.field protected baseband:Ljava/lang/String;

.field protected hardwareVersion:Ljava/lang/String;

.field private final iTel:Lcom/android/internal/telephony/ITelephony;

.field protected infoArray:[Ljava/lang/String;

.field protected macAddress:Ljava/lang/String;

.field protected phoneInfoListView:Landroid/widget/ListView;

.field private sim:Ljava/lang/String;

.field protected softwareVersion:Ljava/lang/String;

.field protected textGsm:Ljava/lang/String;

.field protected textTd:Ljava/lang/String;

.field private wifiMgr:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->iTel:Lcom/android/internal/telephony/ITelephony;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->phoneInfoListView:Landroid/widget/ListView;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->softwareVersion:Ljava/lang/String;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->hardwareVersion:Ljava/lang/String;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->baseband:Ljava/lang/String;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->ap_build:Ljava/lang/String;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->macAddress:Ljava/lang/String;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->sim:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected getBaseband()V
    .locals 1

    const-string v0, "gsm.version.baseband"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->baseband:Ljava/lang/String;

    return-void
.end method

.method protected getByte(Ljava/lang/String;I)B
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->openFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "GBK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    aget-byte v0, v2, p2
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method getFileData()[B
    .locals 10

    new-instance v2, Ljava/io/File;

    const-string v8, "/local/nvm/ManufactoryInf.nvm"

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v8, 0x2

    new-array v6, v8, [B

    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    new-instance v5, Ljava/io/BufferedReader;

    const/16 v8, 0x400

    invoke-direct {v5, v3, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    :goto_0
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v4, v0

    const/4 v8, 0x0

    add-int/lit8 v9, v4, -0x2

    aget-byte v9, v0, v9

    aput-byte v9, v6, v8

    const/4 v8, 0x1

    add-int/lit8 v9, v4, -0x1

    aget-byte v9, v0, v9

    aput-byte v9, v6, v8

    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    return-object v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method protected getFormattedKernelVersion()Ljava/lang/String;
    .locals 9

    const/4 v8, 0x4

    :try_start_0
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    const-string v7, "/proc/version"

    invoke-direct {v6, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x100

    invoke-direct {v5, v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    :try_start_2
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    const-string v0, "\\w+\\s+\\w+\\s+([^\\s]+)\\s+\\(([^\\s@]+(?:@[^\\s.]+)?)[^)]*\\)\\s+\\((?:[^(]*\\([^)]*\\))?[^)]*\\)\\s+([^\\s]+)\\s+(?:PREEMPT\\s+)?(.+)"

    const-string v6, "\\w+\\s+\\w+\\s+([^\\s]+)\\s+\\(([^\\s@]+(?:@[^\\s.]+)?)[^)]*\\)\\s+\\((?:[^(]*\\([^)]*\\))?[^)]*\\)\\s+([^\\s]+)\\s+(?:PREEMPT\\s+)?(.+)"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "Unavailable"

    :goto_0
    return-object v6

    :catchall_0
    move-exception v6

    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    throw v6
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v1

    const-string v6, "Unavailable"

    goto :goto_0

    :cond_0
    :try_start_3
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v6

    if-ge v6, v8, :cond_1

    const-string v6, "Unavailable"

    goto :goto_0

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v6

    goto :goto_0
.end method

.method protected getFromSystemProp()V
    .locals 2

    const-string v0, "ro.build.display.id"

    const-string v1, "NULL"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->softwareVersion:Ljava/lang/String;

    const-string v0, "ro.mediatek.version.release"

    const-string v1, "NULL"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->ap_build:Ljava/lang/String;

    return-void
.end method

.method protected getRevisioninfo()V
    .locals 7

    const-string v3, "/proc/hwversion"

    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    :goto_0
    invoke-virtual {v4}, Ljava/io/FileInputStream;->read()I

    move-result v1

    const/4 v5, -0x1

    if-eq v1, v5, :cond_1

    const/16 v5, 0xa

    if-eq v1, v5, :cond_0

    int-to-char v5, v1

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/amoi/AmoiEngineerMode/DeviceInfo;->getHardwareVersion()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->hardwareVersion:Ljava/lang/String;

    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "V"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->hardwareVersion:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method protected initInfo()V
    .locals 13

    const/4 v12, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x2

    const-string v8, "phone"

    invoke-virtual {p0, v8}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    invoke-virtual {v7, v10}, Landroid/telephony/TelephonyManager;->getDeviceIdGemini(I)Ljava/lang/String;

    move-result-object v3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v7, v12}, Landroid/telephony/TelephonyManager;->getDeviceIdGemini(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v10}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSN()Ljava/lang/String;

    move-result-object v6

    const-string v8, "wifi"

    invoke-virtual {p0, v8}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/wifi/WifiManager;

    iput-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->wifiMgr:Landroid/net/wifi/WifiManager;

    iget-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->wifiMgr:Landroid/net/wifi/WifiManager;

    if-nez v8, :cond_1

    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->macAddress:Ljava/lang/String;

    :cond_0
    :try_start_0
    iget-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->iTel:Lcom/android/internal/telephony/ITelephony;

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z

    move-result v8

    if-eqz v8, :cond_2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SIM1: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f0600e5

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->sim:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    :try_start_1
    iget-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->iTel:Lcom/android/internal/telephony/ITelephony;

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z

    move-result v8

    if-eqz v8, :cond_3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->sim:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\nSIM2: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f0600e5

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->sim:Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getFromSystemProp()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->testNvm()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getRevisioninfo()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getBaseband()V

    const/16 v8, 0x9

    new-array v5, v8, [Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    new-instance v8, Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const v9, 0x7f06002f

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->softwareVersion:Ljava/lang/String;

    invoke-direct {v8, v9, v10, v11}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v8, v5, v2

    add-int/lit8 v2, v2, 0x1

    new-instance v8, Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const v9, 0x7f0600d3

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v6, v11}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v8, v5, v2

    add-int/lit8 v2, v2, 0x1

    new-instance v8, Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const v9, 0x7f060034

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v3, v11}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v8, v5, v2

    add-int/lit8 v2, v2, 0x1

    new-instance v8, Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const v9, 0x7f0600df

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->macAddress:Ljava/lang/String;

    invoke-direct {v8, v9, v10, v11}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v8, v5, v2

    add-int/lit8 v2, v2, 0x1

    new-instance v8, Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const v9, 0x7f060035

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->hardwareVersion:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v8, v5, v2

    add-int/lit8 v2, v2, 0x1

    new-instance v8, Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const v9, 0x7f060033

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getFormattedKernelVersion()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v8, v5, v2

    add-int/lit8 v2, v2, 0x1

    new-instance v8, Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const v9, 0x7f0600da

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->ap_build:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v8, v5, v2

    add-int/lit8 v2, v2, 0x1

    new-instance v8, Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const v9, 0x7f060036

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->baseband:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v8, v5, v2

    add-int/lit8 v2, v2, 0x1

    new-instance v8, Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const v9, 0x7f060037

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "AMOI-20120316-V2.0"

    invoke-direct {v8, v9, v10}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v8, v5, v2

    add-int/lit8 v2, v2, 0x1

    iget-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->phoneInfoListView:Landroid/widget/ListView;

    new-instance v9, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;

    invoke-direct {v9, p0, v5}, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;-><init>(Landroid/content/Context;[Lcom/amoi/AmoiEngineerMode/base/InfoItem;)V

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :cond_1
    iget-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->wifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v8}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v4

    goto/16 :goto_0

    :cond_2
    :try_start_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SIM1: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f0600e6

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->sim:Ljava/lang/String;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1

    :cond_3
    :try_start_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->sim:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\nSIM2: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f0600e6

    invoke-virtual {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->sim:Ljava/lang/String;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method protected abstract initView()V
.end method

.method protected isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001e

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->setContentView(I)V

    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f070019

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->phoneInfoListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->initInfo()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->wifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->wifiMgr:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    :cond_0
    return-void
.end method

.method public openFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1    # Ljava/lang/String;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x400

    new-array v0, v7, [B

    const/4 v1, 0x0

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v5, 0x0

    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    const/4 v7, -0x1

    if-eq v5, v7, :cond_0

    const/4 v7, 0x0

    invoke-virtual {v6, v0, v7, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v1}, Ljava/lang/String;-><init>([B)V

    return-object v7

    :cond_0
    :try_start_1
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method protected testNvm()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v1, "/local/nvm/GsmCalData.nvm"

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "/local/nvm/TdCalData.nvm"

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "OK"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->textGsm:Ljava/lang/String;

    :goto_0
    const-string v1, "/local/nvm/ManufactoryInf.nvm"

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "\u6682\u4e0d\u652f\u6301"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->textTd:Ljava/lang/String;

    const-string v1, "SmallTest"

    const-string v2, "file is not found"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_0
    const-string v1, "/local/nvm/GsmCalData.nvm"

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Only GSM RF OK"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->textGsm:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v1, "/local/nvm/TdCalData.nvm"

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "Only TD RF OK"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->textGsm:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v1, "\u6682\u4e0d\u652f\u6301"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->textGsm:Ljava/lang/String;

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->getFileData()[B

    move-result-object v0

    aget-byte v1, v0, v4

    if-ne v1, v3, :cond_4

    aget-byte v1, v0, v3

    if-ne v1, v3, :cond_4

    const-string v1, "PASS"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->textTd:Ljava/lang/String;

    goto :goto_1

    :cond_4
    aget-byte v1, v0, v4

    if-eq v1, v3, :cond_5

    aget-byte v1, v0, v3

    if-eq v1, v3, :cond_5

    const-string v1, "\u6682\u4e0d\u652f\u6301"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->textTd:Ljava/lang/String;

    goto :goto_1

    :cond_5
    const-string v1, "SmallTest"

    const-string v2, "file is found"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    aget-byte v1, v0, v4

    if-ne v1, v3, :cond_6

    const-string v1, "Only GSM RF PASS"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->textTd:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const-string v1, "Only TD RF PASS"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/PhoneInfoBase;->textTd:Ljava/lang/String;

    goto :goto_1
.end method
