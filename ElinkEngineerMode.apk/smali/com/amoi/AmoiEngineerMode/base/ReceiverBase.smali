.class public abstract Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;
.super Landroid/app/Activity;
.source "ReceiverBase.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ReceiverBase"


# instance fields
.field private textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private initView()V
    .locals 2

    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->setContentView(I)V

    const v0, 0x7f060026

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f070002

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->textView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->textView:Landroid/widget/TextView;

    const v1, 0x7f060050

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private startMusic()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/amoi/AmoiEngineerMode/base/ReceiverMusicService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private stopMusic()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/amoi/AmoiEngineerMode/base/ReceiverMusicService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->stopService(Landroid/content/Intent;)Z

    return-void
.end method


# virtual methods
.method protected abstract initButton()V
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "EngineerMode"

    const-string v1, "ReceiverBase......onCreate()......"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->initButton()V

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->startMusic()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "EngineerMode"

    const-string v1, "ReceiverBase......onDestroy()......"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverBase;->stopMusic()V

    return-void
.end method
