.class public Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$HeadsetTestThread;
.super Ljava/lang/Thread;
.source "ReceiverCircuitBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "HeadsetTestThread"
.end annotation


# static fields
.field private static final SAMPLE_RATE:I = 0x1f40


# instance fields
.field final synthetic this$0:Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;


# direct methods
.method protected constructor <init>(Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;)V
    .locals 0

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$HeadsetTestThread;->this$0:Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    const/4 v1, 0x1

    const/4 v7, 0x0

    const/16 v2, 0x1f40

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$HeadsetTestThread;->this$0:Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;

    iput-boolean v1, v4, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->isTesting:Z

    const/16 v4, -0x13

    invoke-static {v4}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {v2, v3, v3}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v5

    add-int/lit16 v5, v5, 0x2800

    new-instance v0, Landroid/media/AudioRecord;

    move v4, v3

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    new-instance v6, Landroid/media/AudioTrack;

    move v8, v2

    move v9, v3

    move v10, v3

    move v11, v5

    move v12, v1

    invoke-direct/range {v6 .. v12}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    invoke-virtual {v6, v2}, Landroid/media/AudioTrack;->setPlaybackRate(I)I

    new-array v13, v5, [B

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    invoke-virtual {v6}, Landroid/media/AudioTrack;->play()V

    :goto_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$HeadsetTestThread;->this$0:Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;

    iget-boolean v1, v1, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->isTesting:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0, v13, v7, v5}, Landroid/media/AudioRecord;->read([BII)I

    array-length v1, v13

    invoke-virtual {v6, v13, v7, v1}, Landroid/media/AudioTrack;->write([BII)I

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    const/4 v0, 0x0

    invoke-virtual {v6}, Landroid/media/AudioTrack;->stop()V

    invoke-virtual {v6}, Landroid/media/AudioTrack;->release()V

    const/4 v6, 0x0

    return-void
.end method
