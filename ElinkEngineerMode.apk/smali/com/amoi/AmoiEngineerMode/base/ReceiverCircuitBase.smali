.class public abstract Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;
.super Landroid/app/Activity;
.source "ReceiverCircuitBase.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$HeadsetTestThread;
    }
.end annotation


# static fields
.field protected static final MSG_START_TEST:I = 0x3

.field protected static final MSG_UPDATE_TEXT_HEADSETIN:I = 0x2

.field protected static final MSG_UPDATE_TEXT_TESTING:I = 0x1


# instance fields
.field protected audioManager:Landroid/media/AudioManager;

.field protected handler:Landroid/os/Handler;

.field protected isTesting:Z

.field protected mTestThread:Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$HeadsetTestThread;

.field protected originalVolume:I

.field protected startButton:Landroid/widget/Button;

.field protected textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->isTesting:Z

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$HeadsetTestThread;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$HeadsetTestThread;-><init>(Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->mTestThread:Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$HeadsetTestThread;

    return-void
.end method

.method static synthetic access$000(Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;)V
    .locals 0
    .param p0    # Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->startTest()V

    return-void
.end method

.method private initView()V
    .locals 1

    const v0, 0x7f03000e

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->setContentView(I)V

    const v0, 0x7f0600d0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->setTitle(I)V

    const v0, 0x7f070021

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->startButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->startButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f070020

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->textView:Landroid/widget/TextView;

    return-void
.end method

.method private startTest()V
    .locals 2

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->textView:Landroid/widget/TextView;

    const v1, 0x7f0600d2

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->isTesting:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->initAudio()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->mTestThread:Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$HeadsetTestThread;

    invoke-virtual {v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$HeadsetTestThread;->start()V

    :cond_1
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method


# virtual methods
.method protected initAudio()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->originalVolume:I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x1

    const/16 v2, 0x8

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    return-void
.end method

.method protected abstract initButton()V
.end method

.method protected initHandler()V
    .locals 1

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$1;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase$1;-><init>(Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->handler:Landroid/os/Handler;

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->startTest()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f070021
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "EngineerMode"

    const-string v1, "HeadsetBase onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->audioManager:Landroid/media/AudioManager;

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->initButton()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->initHandler()V

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->startTest()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "EngineerMode"

    const-string v1, "HeadsetBase onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->stopTest()V

    return-void
.end method

.method protected stopTest()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->isTesting:Z

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->isTesting:Z

    :cond_0
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->audioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/amoi/AmoiEngineerMode/base/ReceiverCircuitBase;->originalVolume:I

    const/16 v2, 0x8

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void
.end method
