.class public abstract Lcom/amoi/AmoiEngineerMode/base/RecordBase;
.super Lcom/amoi/AmoiEngineerMode/base/TestBase;
.source "RecordBase.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amoi/AmoiEngineerMode/base/RecordBase$RecordHandler;,
        Lcom/amoi/AmoiEngineerMode/base/RecordBase$UpdateSecondTask;
    }
.end annotation


# static fields
.field protected static final MSG_UPDATE_TIME:I = 0x1

.field private static final TAG:Ljava/lang/String; = "RecordBase"


# instance fields
.field protected STRING_PLAYBACK:Ljava/lang/String;

.field protected STRING_RECORDING:Ljava/lang/String;

.field protected STRING_SECOND:Ljava/lang/String;

.field protected handler:Landroid/os/Handler;

.field protected hasSDCard:Z

.field protected player:Landroid/media/MediaPlayer;

.field protected recordFileName:Ljava/lang/String;

.field protected recorder:Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;

.field protected seconds:I

.field protected text:Ljava/lang/String;

.field protected textView:Landroid/widget/TextView;

.field protected timer:Ljava/util/Timer;

.field protected timerTask:Ljava/util/TimerTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recordFileName:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->seconds:I

    return-void
.end method


# virtual methods
.method protected destroyTimer()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timerTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timerTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timerTask:Ljava/util/TimerTask;

    :cond_0
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timer:Ljava/util/Timer;

    :cond_1
    return-void
.end method

.method protected abstract handleNoSDCardExtra()V
.end method

.method protected initHandler()V
    .locals 1

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/RecordBase$RecordHandler;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase$RecordHandler;-><init>(Lcom/amoi/AmoiEngineerMode/base/RecordBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->handler:Landroid/os/Handler;

    return-void
.end method

.method protected abstract initView()V
.end method

.method protected isSdcardExist()Z
    .locals 2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->stopTimer()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->startTimer()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "RecordBase"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->isSdcardExist()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->hasSDCard:Z

    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->hasSDCard:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->textView:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->textView:Landroid/widget/TextView;

    const v1, 0x7f060049

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->textView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->handleNoSDCardExtra()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recordFileName:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recordFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Amoi_RecordTest.3pg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recordFileName:Ljava/lang/String;

    const v0, 0x7f060038

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->STRING_RECORDING:Ljava/lang/String;

    const v0, 0x7f06003a

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->STRING_PLAYBACK:Ljava/lang/String;

    const v0, 0x7f060039

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->STRING_SECOND:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->initHandler()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->startRecord()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onDestroy()V

    const-string v0, "RecordBase"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->hasSDCard:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->stopPlay()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->stopRecord()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->destroyTimer()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recordFileName:Ljava/lang/String;

    invoke-static {v0}, Lcom/amoi/AmoiEngineerMode/util/FileHelper;->delete(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected playback()V
    .locals 0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->stopRecord()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->startPlay()V

    return-void
.end method

.method protected startPlay()V
    .locals 3

    const-string v1, "RecordBase"

    const-string v2, "startPlay"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->stopTimer()V

    :try_start_0
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->player:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recordFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->STRING_PLAYBACK:Ljava/lang/String;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->text:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->startTimer()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v1, "RecordBase"

    const-string v2, "playing"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    const-string v1, "RecordBase"

    const-string v2, "error to play record"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected startRecord()V
    .locals 3

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->stopTimer()V

    :try_start_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recordFileName:Ljava/lang/String;

    invoke-static {v1}, Lcom/amoi/AmoiEngineerMode/util/FileHelper;->delete(Ljava/lang/String;)V

    new-instance v1, Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;-><init>(I)V

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recorder:Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recorder:Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recordFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;->setOutputFile(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recorder:Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;

    invoke-virtual {v1}, Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;->prepare()V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recorder:Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;

    invoke-virtual {v1}, Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;->start()V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->STRING_RECORDING:Ljava/lang/String;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->text:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->startTimer()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    const-string v1, "RecordBase"

    const-string v2, "recording"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    const-string v1, "RecordBase"

    const-string v2, "error to start record"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "RecordBase"

    const-string v2, "error to start record"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v1, "\u542f\u52a8\u5f55\u97f3\u673a\u5931\u8d25!"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected startTimer()V
    .locals 6

    const-string v0, "EngineerMode"

    const-string v1, "RecordBase startTimerAndTimerTask"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timer:Ljava/util/Timer;

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->seconds:I

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/RecordBase$UpdateSecondTask;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase$UpdateSecondTask;-><init>(Lcom/amoi/AmoiEngineerMode/base/RecordBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timerTask:Ljava/util/TimerTask;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method protected stopPlay()V
    .locals 3

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->player:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->player:Landroid/media/MediaPlayer;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "RecordBase"

    const-string v2, "error to stop player"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected stopRecord()V
    .locals 3

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recorder:Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recorder:Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;

    invoke-virtual {v1}, Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recorder:Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;

    invoke-virtual {v1}, Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;->release()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->recorder:Lcom/amoi/AmoiEngineerMode/util/AudioRecorder;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "RecordBase"

    const-string v2, "error to stop record"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected stopTimer()V
    .locals 1

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timerTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timerTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    :cond_0
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    :cond_1
    return-void
.end method
