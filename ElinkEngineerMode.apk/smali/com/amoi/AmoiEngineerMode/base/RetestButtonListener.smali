.class public Lcom/amoi/AmoiEngineerMode/base/RetestButtonListener;
.super Ljava/lang/Object;
.source "RetestButtonListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private activity:Lcom/amoi/AmoiEngineerMode/base/Retestable;


# direct methods
.method public constructor <init>(Lcom/amoi/AmoiEngineerMode/base/Retestable;)V
    .locals 0
    .param p1    # Lcom/amoi/AmoiEngineerMode/base/Retestable;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/RetestButtonListener;->activity:Lcom/amoi/AmoiEngineerMode/base/Retestable;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const-string v0, "RetestButtonListener"

    const-string v1, "retest button clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/RetestButtonListener;->activity:Lcom/amoi/AmoiEngineerMode/base/Retestable;

    invoke-interface {v0}, Lcom/amoi/AmoiEngineerMode/base/Retestable;->retest()V

    return-void
.end method
