.class public abstract Lcom/amoi/AmoiEngineerMode/base/SDCardBase;
.super Landroid/app/Activity;
.source "SDCardBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardWriteException;,
        Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardNotFoundException;,
        Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardTestTask;
    }
.end annotation


# static fields
.field protected static final SLEEP_TIME:I = 0x3e8

.field protected static final TAG:Ljava/lang/String; = "SDCardBase"


# instance fields
.field protected DoulSD:Z

.field private audioManager:Landroid/media/AudioManager;

.field protected exitButton:Landroid/widget/Button;

.field protected fileName:Ljava/lang/String;

.field protected hasSDCard:Z

.field protected mContext:Landroid/content/Context;

.field protected nextButton:Landroid/widget/Button;

.field private originalVolume:I

.field protected player:Landroid/media/MediaPlayer;

.field protected prevButton:Landroid/widget/Button;

.field protected textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->DoulSD:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->mContext:Landroid/content/Context;

    return-void
.end method

.method private getVolume()I
    .locals 3

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected getInputStreamFromMp3()Ljava/io/InputStream;
    .locals 3

    const v1, 0x7f040002

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    return-object v2
.end method

.method protected getStorgeState()Ljava/lang/String;
    .locals 1

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getStrogeDirectory()Ljava/io/File;
    .locals 1

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected initAudio()V
    .locals 4

    const/4 v3, 0x3

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->audioManager:Landroid/media/AudioManager;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->originalVolume:I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->audioManager:Landroid/media/AudioManager;

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->getVolume()I

    move-result v1

    const/16 v2, 0x8

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void
.end method

.method protected abstract initButton()V
.end method

.method protected initView()V
    .locals 1

    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->setContentView(I)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->isExternalStroge2()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0600cc

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    const v0, 0x7f070002

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->textView:Landroid/widget/TextView;

    const v0, 0x7f070004

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->prevButton:Landroid/widget/Button;

    const v0, 0x7f070005

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->nextButton:Landroid/widget/Button;

    const v0, 0x7f070006

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->exitButton:Landroid/widget/Button;

    return-void

    :cond_0
    const v0, 0x7f0600cb

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected isExternalStroge2()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    iput-object p0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->initButton()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->initAudio()V

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardTestTask;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardTestTask;-><init>(Lcom/amoi/AmoiEngineerMode/base/SDCardBase;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardTestTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onDestroy()V
    .locals 5

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-boolean v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->hasSDCard:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->player:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    :cond_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->fileName:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->fileName:Ljava/lang/String;

    invoke-static {v1}, Lcom/amoi/AmoiEngineerMode/util/FileHelper;->delete(Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    iget v3, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->originalVolume:I

    const/16 v4, 0x8

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "EngineerMode"

    const-string v2, "SDCardBase error to stop the media player"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected playMusic()V
    .locals 3

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->fileName:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "EngineerMode"

    const-string v2, "SDCardBase fileName is null. No SD card?"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "EngineerMode"

    const-string v2, "SDCardBase play music after write......."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->player:Landroid/media/MediaPlayer;

    if-nez v1, :cond_1

    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->player:Landroid/media/MediaPlayer;

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->player:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->fileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->player:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "EngineerMode"

    const-string v2, "SDCardBase playMusic error:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "EngineerMode"

    const-string v2, "SDCardBase playMusic error:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "EngineerMode"

    const-string v2, "SDCardBase playMusic error:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected abstract showButton(I)V
.end method

.method protected writeSDCard()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardNotFoundException;,
            Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardWriteException;
        }
    .end annotation

    const/16 v7, 0x1000

    new-array v0, v7, [B

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->getStorgeState()Ljava/lang/String;

    move-result-object v6

    const-string v7, "mounted"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->hasSDCard:Z

    iget-boolean v7, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->hasSDCard:Z

    if-eqz v7, :cond_1

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->getInputStreamFromMp3()Ljava/io/InputStream;

    move-result-object v3

    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->getStrogeDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/Amoi_SDCardTest.mp3"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->fileName:Ljava/lang/String;

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->fileName:Ljava/lang/String;

    invoke-static {v7}, Lcom/amoi/AmoiEngineerMode/util/FileHelper;->delete(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->fileName:Ljava/lang/String;

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    const/4 v7, -0x1

    if-eq v1, v7, :cond_0

    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v7, "EngineerMode"

    const-string v8, "SDCardBase .....write file to sd card error........."

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v7, Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardWriteException;

    invoke-direct {v7, p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardWriteException;-><init>(Lcom/amoi/AmoiEngineerMode/base/SDCardBase;)V

    throw v7

    :cond_0
    :try_start_1
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    const-string v7, "EngineerMode"

    const-string v8, "SDCardBase write SD card success"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-wide/16 v7, 0x3e8

    :try_start_2
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    return-void

    :catch_1
    move-exception v2

    const-string v7, "EngineerMode"

    const-string v8, "SDCardBase sleep error"

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_1
    new-instance v7, Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardNotFoundException;

    invoke-direct {v7, p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase$SDCardNotFoundException;-><init>(Lcom/amoi/AmoiEngineerMode/base/SDCardBase;)V

    throw v7
.end method
