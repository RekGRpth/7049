.class public abstract Lcom/amoi/AmoiEngineerMode/base/SensorBase;
.super Landroid/app/Activity;
.source "SensorBase.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field protected resultTextView:Landroid/widget/TextView;

.field protected sensor:Landroid/hardware/Sensor;

.field protected sm:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected initButton()V
    .locals 0

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/util/ButtonHelper;->initAutoButtons(Landroid/app/Activity;)V

    return-void
.end method

.method protected abstract initSensor()V
.end method

.method protected abstract initView()V
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1    # Landroid/hardware/Sensor;
    .param p2    # I

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/auto/FinishHandler;->exit(Landroid/app/Activity;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030026

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->setContentView(I)V

    const v0, 0x7f07004f

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->resultTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->initButton()V

    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->sm:Landroid/hardware/SensorManager;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->initSensor()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->sm:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->sm:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/SensorBase;->sensor:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    return-void
.end method
