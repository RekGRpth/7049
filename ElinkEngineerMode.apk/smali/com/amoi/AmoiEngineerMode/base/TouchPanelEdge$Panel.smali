.class Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;
.super Landroid/view/View;
.source "TouchPanelEdge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Panel"
.end annotation


# instance fields
.field private mPaint:Landroid/graphics/Paint;

.field private mTouchData:[Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;

.field final synthetic this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

.field private traceCounter:I


# direct methods
.method public constructor <init>(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;Landroid/content/Context;)V
    .locals 9
    .param p2    # Landroid/content/Context;

    const/16 v8, 0x1e

    const/16 v7, 0x64

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->getTestPoint()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p1, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mArrayList:Ljava/util/ArrayList;

    new-array v0, v8, [Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mTouchData:[Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;

    const/4 v6, 0x0

    iput v6, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->traceCounter:I

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v7, v7, v7, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v8, :cond_0

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mTouchData:[Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;

    new-instance v5, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;

    invoke-direct {v5, p1}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;-><init>(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;)V

    aput-object v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private getNext(I)I
    .locals 2
    .param p1    # I

    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v13, -0x10000

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    const/4 v10, -0x1

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    const/high16 v10, 0x41a00000

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "W: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    # getter for: Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->w:F
    invoke-static {v10}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->access$000(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;)F

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    # getter for: Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mWidthPix:I
    invoke-static {v9}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->access$100(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;)I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    add-int/lit16 v9, v9, -0x96

    int-to-float v0, v9

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    # getter for: Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mHightPix:I
    invoke-static {v9}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->access$200(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;)I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    add-int/lit8 v9, v9, -0x64

    int-to-float v1, v9

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0, v1, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "H: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    # getter for: Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->h:F
    invoke-static {v10}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->access$300(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;)F

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    # getter for: Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mWidthPix:I
    invoke-static {v9}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->access$100(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;)I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    add-int/lit16 v9, v9, -0x96

    int-to-float v2, v9

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    # getter for: Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mHightPix:I
    invoke-static {v9}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->access$200(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;)I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    add-int/lit8 v9, v9, -0x50

    int-to-float v3, v9

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v2, v3, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v9, v13}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    iget-object v10, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    # getter for: Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mPointRadius:I
    invoke-static {v10}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->access$400(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v4, 0x0

    const/4 v4, 0x0

    :goto_0
    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    iget-object v9, v9, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_0

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    iget-object v9, v9, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$EdgePoint;

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v9, v13}, Landroid/graphics/Paint;->setColor(I)V

    iget v9, v5, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$EdgePoint;->x:I

    int-to-float v9, v9

    iget v10, v5, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$EdgePoint;->y:I

    int-to-float v10, v10

    iget-object v11, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v11}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v11

    iget-object v12, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_1
    const/16 v9, 0x1e

    if-ge v4, v9, :cond_2

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mTouchData:[Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;

    aget-object v6, v9, v4

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    const v10, -0xff0100

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    iget v9, v6, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;->r:F

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-lez v9, :cond_1

    iget v9, v6, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;->x:F

    iget v10, v6, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;->y:F

    const/high16 v11, 0x40000000

    iget-object v12, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->invalidate()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1    # Landroid/view/MotionEvent;

    const/4 v11, 0x1

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v10

    # setter for: Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->w:F
    invoke-static {v9, v10}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->access$002(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;F)F

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v10

    # setter for: Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->h:F
    invoke-static {v9, v10}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->access$302(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;F)F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v8

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    # getter for: Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mPointRadius:I
    invoke-static {v9}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->access$400(Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;)I

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    const/4 v10, 0x2

    if-eq v9, v10, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    if-ne v9, v11, :cond_4

    :cond_0
    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    iget-object v9, v9, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    const/4 v1, 0x0

    :goto_0
    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    iget-object v9, v9, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v1, v9, :cond_3

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    iget-object v9, v9, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$EdgePoint;

    iget v9, v2, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$EdgePoint;->x:I

    int-to-float v4, v9

    iget v9, v2, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$EdgePoint;->y:I

    int-to-float v5, v9

    int-to-float v9, v6

    sub-float v9, v4, v9

    cmpg-float v9, v9, v7

    if-gez v9, :cond_1

    int-to-float v9, v6

    add-float/2addr v9, v4

    cmpl-float v9, v9, v7

    if-lez v9, :cond_1

    int-to-float v9, v6

    sub-float v9, v5, v9

    cmpg-float v9, v9, v8

    if-gez v9, :cond_1

    int-to-float v9, v6

    add-float/2addr v9, v5

    cmpl-float v9, v9, v8

    if-lez v9, :cond_1

    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    iget-object v9, v9, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->this$0:Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge;

    invoke-static {v9}, Lcom/amoi/AmoiEngineerMode/auto/FinishHandler;->next(Landroid/app/Activity;)V

    :cond_3
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->mTouchData:[Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;

    iget v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->traceCounter:I

    aget-object v3, v0, v9

    iput v7, v3, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;->x:F

    iput v8, v3, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;->y:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v9

    const/high16 v10, 0x43fa0000

    mul-float/2addr v9, v10

    iput v9, v3, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$TouchData;->r:F

    iget v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->traceCounter:I

    invoke-direct {p0, v9}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->getNext(I)I

    move-result v9

    iput v9, p0, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->traceCounter:I

    :cond_4
    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/TouchPanelEdge$Panel;->invalidate()V

    return v11
.end method
