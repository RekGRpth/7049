.class public abstract Lcom/amoi/AmoiEngineerMode/base/VibratorBase;
.super Lcom/amoi/AmoiEngineerMode/base/TestBase;
.source "VibratorBase.java"

# interfaces
.implements Lcom/amoi/AmoiEngineerMode/base/Retestable;


# static fields
.field protected static final MSG_END_OF_VIBRATING:I = 0x1

.field protected static final MSG_LOOP_VIBRATING:I = 0x2

.field private static final TAG:Ljava/lang/String; = "VibratorBase"


# instance fields
.field protected handler:Landroid/os/Handler;

.field protected mAningTestTime:I

.field protected mTimer:Ljava/util/Timer;

.field protected retestButton:Landroid/widget/Button;

.field protected tipsView:Landroid/widget/TextView;

.field protected vibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract initButton()V
.end method

.method protected initVibrator()V
    .locals 1

    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->vibrator:Landroid/os/Vibrator;

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase$1;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase$1;-><init>(Lcom/amoi/AmoiEngineerMode/base/VibratorBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->handler:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->startVibrate()V

    return-void
.end method

.method protected initView()V
    .locals 2

    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->setContentView(I)V

    const v0, 0x7f06001a

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f070002

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->tipsView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->tipsView:Landroid/widget/TextView;

    const v1, 0x7f06001b

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->isKeepScreenOn(Z)V

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/util/ButtonHelper;->hideAutoButtons(Landroid/app/Activity;)V

    const v0, 0x7f070047

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->retestButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->retestButton:Landroid/widget/Button;

    new-instance v1, Lcom/amoi/AmoiEngineerMode/base/RetestButtonListener;

    invoke-direct {v1, p0}, Lcom/amoi/AmoiEngineerMode/base/RetestButtonListener;-><init>(Lcom/amoi/AmoiEngineerMode/base/Retestable;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected isAgingTest()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method isKeepScreenOn(Z)V
    .locals 5
    .param p1    # Z

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->isAgingTest()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v2, "EngineerMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableScreenKeepOn, enable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    if-eqz p1, :cond_1

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v2, v2, 0x80

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0

    :cond_1
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v2, v2, -0x81

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->initButton()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->initVibrator()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onDestroy()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->isKeepScreenOn(Z)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->vibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->isAgingTest()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->isKeepScreenOn(Z)V

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->isKeepScreenOn(Z)V

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onResume()V

    return-void
.end method

.method public retest()V
    .locals 2

    const-string v0, "EngineerMode"

    const-string v1, "VibratorBase retest"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->tipsView:Landroid/widget/TextView;

    const v1, 0x7f06001b

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->retestButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->startVibrate()V

    return-void
.end method

.method protected abstract showButton()V
.end method

.method protected startVibrate()V
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->isAgingTest()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->mTimer:Ljava/util/Timer;

    iput v2, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->mAningTestTime:I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/amoi/AmoiEngineerMode/base/VibratorBase$2;

    invoke-direct {v1, p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase$2;-><init>(Lcom/amoi/AmoiEngineerMode/base/VibratorBase;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xfa0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->vibrator:Landroid/os/Vibrator;

    const/4 v1, 0x3

    new-array v1, v1, [J

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x1770

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    nop

    :array_0
    .array-data 8
        0x0
        0x7d0
        0x7d0
    .end array-data
.end method

.method protected stopVibrate()V
    .locals 2

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->vibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->tipsView:Landroid/widget/TextView;

    const v1, 0x7f06001c

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->retestButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/VibratorBase;->showButton()V

    return-void
.end method
