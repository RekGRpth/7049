.class Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;
.super Landroid/content/BroadcastReceiver;
.source "WifiBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amoi/AmoiEngineerMode/base/WifiBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;


# direct methods
.method constructor <init>(Lcom/amoi/AmoiEngineerMode/base/WifiBase;)V
    .locals 0

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v7, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "wifi_state"

    const/4 v8, -0x1

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->testingView:Landroid/widget/TextView;

    const v8, 0x7f0600b0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_1
    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->startScan()Z

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->testingView:Landroid/widget/TextView;

    const v8, 0x7f0600b2

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    const-string v7, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_4

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    const/4 v8, 0x1

    iput-boolean v8, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mSuccess:Z

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v8, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v8, v8, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v7, v8}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    const/4 v8, 0x0

    iput-object v8, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mReceiver:Landroid/content/BroadcastReceiver;

    :cond_2
    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->testingView:Landroid/widget/TextView;

    const v8, -0xff0100

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->testingView:Landroid/widget/TextView;

    const v8, 0x7f0600b3

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->infoView:Landroid/widget/TextView;

    const v8, 0x7f0600b5

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    const v8, 0x7f03002a

    invoke-direct {v1, v7, v8}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/ScanResult;

    iget-object v7, v5, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "  "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v5, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_1

    :cond_3
    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->listView:Landroid/widget/ListView;

    invoke-virtual {v7, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    invoke-virtual {v7}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->showButton()V

    :cond_4
    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-boolean v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mSuccess:Z

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget v8, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mScanTimes:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mScanTimes:I

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mScanTimes:I

    const/16 v8, 0xa

    if-lt v7, v8, :cond_5

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->testingView:Landroid/widget/TextView;

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->testingView:Landroid/widget/TextView;

    const v8, 0x7f0600b4

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    invoke-virtual {v7}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->showButton()V

    goto/16 :goto_0

    :cond_5
    iget-object v7, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;->this$0:Lcom/amoi/AmoiEngineerMode/base/WifiBase;

    iget-object v7, v7, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->startScan()Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
