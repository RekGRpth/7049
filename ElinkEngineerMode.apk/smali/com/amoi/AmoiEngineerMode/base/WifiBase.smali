.class public abstract Lcom/amoi/AmoiEngineerMode/base/WifiBase;
.super Lcom/amoi/AmoiEngineerMode/base/TestBase;
.source "WifiBase.java"


# static fields
.field protected static final MAX_SCAN_TIME:I = 0xa

.field protected static final TAG:Ljava/lang/String; = "Wifi"


# instance fields
.field protected infoView:Landroid/widget/TextView;

.field protected listView:Landroid/widget/ListView;

.field protected mReceiver:Landroid/content/BroadcastReceiver;

.field protected mScanTimes:I

.field protected mSuccess:Z

.field protected mWifiManager:Landroid/net/wifi/WifiManager;

.field protected originalState:Z

.field protected testingView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;-><init>()V

    iput-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mSuccess:Z

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mScanTimes:I

    new-instance v0, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;

    invoke-direct {v0, p0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase$1;-><init>(Lcom/amoi/AmoiEngineerMode/base/WifiBase;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method protected closeWifi()V
    .locals 2

    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->originalState:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    const-string v0, "EngineerMode"

    const-string v1, "Wifi close wifi..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected abstract initButton()V
.end method

.method protected initView()V
    .locals 1

    const v0, 0x7f03001d

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->setContentView(I)V

    const v0, 0x7f06005f

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f070048

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->testingView:Landroid/widget/TextView;

    const v0, 0x7f070049

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->infoView:Landroid/widget/TextView;

    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->listView:Landroid/widget/ListView;

    return-void
.end method

.method protected initWifi()V
    .locals 2

    const-string v1, "wifi"

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->originalState:Z

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->openWifi()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->initView()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->initButton()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->initWifi()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/amoi/AmoiEngineerMode/base/TestBase;->onDestroy()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->closeWifi()V

    return-void
.end method

.method protected openWifi()V
    .locals 2

    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->originalState:Z

    if-nez v0, :cond_0

    const-string v0, "EngineerMode"

    const-string v1, "Wifi start wifi..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/WifiBase;->testingView:Landroid/widget/TextView;

    const v1, 0x7f0600b2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method protected abstract showButton()V
.end method
