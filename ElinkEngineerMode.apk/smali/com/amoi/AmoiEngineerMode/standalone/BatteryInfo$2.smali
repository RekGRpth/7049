.class Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;
.super Landroid/content/BroadcastReceiver;
.source "BatteryInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;


# direct methods
.method constructor <init>(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 22
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v18, "BatteryInfo"

    const-string v19, "receiver init............"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    # getter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->mDoul_battery:Z
    invoke-static/range {v18 .. v18}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$000(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;)Z

    move-result v18

    if-eqz v18, :cond_7

    const-string v18, "android.intent.action.ACTION_DUAL_BATTERY_CHANGED"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    const-string v18, "power_bat"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    const-string v18, "present_bat"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    const-string v18, "batA_level"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v18, "batB_level"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const-string v18, "batA_voltage"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const-string v18, "batB_voltage"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    const-string v18, "pmic_change"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    const-string v18, "internal_change"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    const-string v18, "allow_set"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string v18, "temperature"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    if-nez v9, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600ba

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "%"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->battery:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$402(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600bb

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "%"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$484(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/Object;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600c9

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "mV"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->mOtherInfoString:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$102(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600ca

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "mV"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$184(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/Object;)Ljava/lang/String;

    :cond_0
    :goto_0
    if-nez v16, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600c2

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$184(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/Object;)Ljava/lang/String;

    :cond_1
    const/16 v18, 0x1

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600c3

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$184(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/Object;)Ljava/lang/String;

    :cond_2
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v13, v0, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600c0

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$184(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/Object;)Ljava/lang/String;

    :cond_3
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v13, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600c1

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$184(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/Object;)Ljava/lang/String;

    :cond_4
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v10, v0, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600c5

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$184(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/Object;)Ljava/lang/String;

    :cond_5
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v10, v0, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600c6

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$184(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/Object;)Ljava/lang/String;

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    # getter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->mHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$800(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;)Landroid/os/Handler;

    move-result-object v18

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    # getter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->mHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$800(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;)Landroid/os/Handler;

    move-result-object v18

    const/16 v19, 0x2

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_7
    const-string v18, "android.intent.action.BATTERY_CHANGED"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_a

    const-string v18, "level"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    const-string v18, "scale"

    const/16 v19, 0x64

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    const-string v18, "temperature"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    div-int/lit8 v17, v18, 0xa

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    # getter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->mDoul_battery:Z
    invoke-static/range {v18 .. v18}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$000(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;)Z

    move-result v18

    if-nez v18, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    mul-int/lit8 v20, v11, 0x64

    div-int v20, v20, v14

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "%"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->battery:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$402(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    :cond_8
    const-string v18, "plugged"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    const-string v18, "status"

    const/16 v19, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    const-string v18, "BatteryInfo"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "status:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v15, v0, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v19, v0

    const v20, 0x7f060099

    invoke-virtual/range {v19 .. v20}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->statusString:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$602(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    if-lez v12, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v19, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    # getter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->statusString:Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$600(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v21, v0

    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v12, v0, :cond_d

    const v18, 0x7f06009a

    :goto_1
    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->statusString:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$602(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    :cond_9
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600db

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\u00b0"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$684(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/Object;)Ljava/lang/String;

    const-string v18, "BatteryInfo"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "statusString:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    # getter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->statusString:Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$600(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    # getter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->mHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$800(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;)Landroid/os/Handler;

    move-result-object v18

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    # getter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->mHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$800(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;)Landroid/os/Handler;

    move-result-object v18

    const/16 v19, 0x2

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_a
    return-void

    :cond_b
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v9, v0, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600ba

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "%"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->battery:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$402(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600c9

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "V"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->mOtherInfoString:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$102(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_c
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v9, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600bb

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "%"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->battery:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$402(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v20, v0

    const v21, 0x7f0600ca

    invoke-virtual/range {v20 .. v21}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "V"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->mOtherInfoString:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$102(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_d
    const v18, 0x7f06009b

    goto/16 :goto_1

    :cond_e
    const/16 v18, 0x3

    move/from16 v0, v18

    if-ne v15, v0, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v19, v0

    const v20, 0x7f06009c

    invoke-virtual/range {v19 .. v20}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->statusString:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$602(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_2

    :cond_f
    const/16 v18, 0x4

    move/from16 v0, v18

    if-ne v15, v0, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v19, v0

    const v20, 0x7f06009d

    invoke-virtual/range {v19 .. v20}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->statusString:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$602(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_2

    :cond_10
    const/16 v18, 0x5

    move/from16 v0, v18

    if-ne v15, v0, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v19, v0

    const v20, 0x7f06009e

    invoke-virtual/range {v19 .. v20}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->statusString:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$602(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_2

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo$2;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;

    move-object/from16 v19, v0

    const v20, 0x7f060098

    invoke-virtual/range {v19 .. v20}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->statusString:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;->access$602(Lcom/amoi/AmoiEngineerMode/standalone/BatteryInfo;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_2
.end method
