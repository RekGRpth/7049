.class public Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;
.super Landroid/app/Activity;
.source "DoulSDCardTest.java"


# instance fields
.field private audioListView:Landroid/widget/ListView;

.field private mArrayAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private sdarray:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->audioListView:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->sdarray:[Ljava/lang/String;

    return-void
.end method

.method private initView()V
    .locals 3

    const v0, 0x7f070001

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->audioListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->sdarray:[Ljava/lang/String;

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x1090003

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->sdarray:[Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->audioListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->audioListView:Landroid/widget/ListView;

    new-instance v1, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest$1;

    invoke-direct {v1, p0}, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest$1;-><init>(Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->setContentView(I)V

    const v0, 0x7f0600cd

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->setTitle(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/standalone/DoulSDCardTest;->initView()V

    return-void
.end method
