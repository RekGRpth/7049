.class public Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;
.super Landroid/app/Activity;
.source "OtherInfo.java"


# instance fields
.field private mCameraType:Ljava/lang/String;

.field private mLcdType:Ljava/lang/String;

.field protected phoneInfoListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->phoneInfoListView:Landroid/widget/ListView;

    return-void
.end method

.method private getCamerainfo()V
    .locals 6

    const-string v3, "/proc/main_camera_name"

    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    :goto_0
    invoke-virtual {v4}, Ljava/io/FileInputStream;->read()I

    move-result v1

    const/4 v5, -0x1

    if-eq v1, v5, :cond_1

    const/16 v5, 0xa

    if-eq v1, v5, :cond_0

    int-to-char v5, v1

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    const v5, 0x7f0600ed

    invoke-virtual {p0, v5}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->mCameraType:Ljava/lang/String;

    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->mCameraType:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private initInfo()V
    .locals 6

    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->getLcdinfo()V

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->getCamerainfo()V

    new-array v1, v5, [Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const/4 v0, 0x0

    new-instance v2, Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const v3, 0x7f0600ec

    invoke-virtual {p0, v3}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->mLcdType:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    new-instance v2, Lcom/amoi/AmoiEngineerMode/base/InfoItem;

    const v3, 0x7f0600ee

    invoke-virtual {p0, v3}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->mCameraType:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5}, Lcom/amoi/AmoiEngineerMode/base/InfoItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->phoneInfoListView:Landroid/widget/ListView;

    new-instance v3, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;

    invoke-direct {v3, p0, v1}, Lcom/amoi/AmoiEngineerMode/base/InfoListViewAdapter;-><init>(Landroid/content/Context;[Lcom/amoi/AmoiEngineerMode/base/InfoItem;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method


# virtual methods
.method protected getLcdinfo()V
    .locals 6

    const-string v3, "/proc/lcm_name"

    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    :goto_0
    invoke-virtual {v4}, Ljava/io/FileInputStream;->read()I

    move-result v1

    const/4 v5, -0x1

    if-eq v1, v5, :cond_1

    const/16 v5, 0xa

    if-eq v1, v5, :cond_0

    int-to-char v5, v1

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    const v5, 0x7f0600ed

    invoke-virtual {p0, v5}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->mLcdType:Ljava/lang/String;

    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->mLcdType:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method protected initButton()V
    .locals 0

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/util/ButtonHelper;->initOnlyExitButton(Landroid/app/Activity;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001e

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->setContentView(I)V

    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f070019

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->phoneInfoListView:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->initInfo()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/OtherInfo;->initButton()V

    return-void
.end method
