.class public Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;
.super Landroid/app/Activity;
.source "ProcessTest.java"


# instance fields
.field protected bt_ft_tv:Landroid/widget/TextView;

.field protected telMgr:Landroid/telephony/TelephonyManager;

.field textGsm:Ljava/lang/String;

.field textTd:Ljava/lang/String;

.field textVGsm:Landroid/widget/TextView;

.field textVTd:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method private testNvm()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v1, "/local/nvm/GsmCalData.nvm"

    invoke-direct {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "/local/nvm/TdCalData.nvm"

    invoke-direct {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "OK"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textGsm:Ljava/lang/String;

    :goto_0
    const-string v1, "/local/nvm/ManufactoryInf.nvm"

    invoke-direct {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "NG"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textTd:Ljava/lang/String;

    const-string v1, "SmallTest"

    const-string v2, "file is not found"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_0
    const-string v1, "/local/nvm/GsmCalData.nvm"

    invoke-direct {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Only GSM RF OK"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textGsm:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v1, "/local/nvm/TdCalData.nvm"

    invoke-direct {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->isFileExit(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "Only TD RF OK"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textGsm:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v1, "NG"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textGsm:Ljava/lang/String;

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->getFileData()[B

    move-result-object v0

    aget-byte v1, v0, v4

    if-ne v1, v3, :cond_4

    aget-byte v1, v0, v3

    if-ne v1, v3, :cond_4

    const-string v1, "PASS"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textTd:Ljava/lang/String;

    goto :goto_1

    :cond_4
    aget-byte v1, v0, v4

    if-eq v1, v3, :cond_5

    aget-byte v1, v0, v3

    if-eq v1, v3, :cond_5

    const-string v1, "NG"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textTd:Ljava/lang/String;

    goto :goto_1

    :cond_5
    const-string v1, "SmallTest"

    const-string v2, "file is found"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    aget-byte v1, v0, v4

    if-ne v1, v3, :cond_6

    const-string v1, "Only GSM RF PASS"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textTd:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const-string v1, "Only TD RF PASS"

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textTd:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method getFileData()[B
    .locals 10

    new-instance v2, Ljava/io/File;

    const-string v8, "/local/nvm/ManufactoryInf.nvm"

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v8, 0x2

    new-array v6, v8, [B

    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    new-instance v5, Ljava/io/BufferedReader;

    const/16 v8, 0x400

    invoke-direct {v5, v3, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    :goto_0
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v4, v0

    const/4 v8, 0x0

    add-int/lit8 v9, v4, -0x2

    aget-byte v9, v0, v9

    aput-byte v9, v6, v8

    const/4 v8, 0x1

    add-int/lit8 v9, v4, -0x1

    aget-byte v9, v0, v9

    aput-byte v9, v6, v8

    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    :cond_0
    :goto_1
    return-object v6

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method protected initButton()V
    .locals 0

    invoke-static {p0}, Lcom/amoi/AmoiEngineerMode/util/ButtonHelper;->initOnlyExitButton(Landroid/app/Activity;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030011

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->setContentView(I)V

    const v0, 0x7f06004d

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->initButton()V

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->telMgr:Landroid/telephony/TelephonyManager;

    const v0, 0x7f070023

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textVGsm:Landroid/widget/TextView;

    const v0, 0x7f070024

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textVTd:Landroid/widget/TextView;

    const v0, 0x7f070027

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->bt_ft_tv:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->bt_ft_tv:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->telMgr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSN()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->testNvm()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textVGsm:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textGsm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textVTd:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/ProcessTest;->textTd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
