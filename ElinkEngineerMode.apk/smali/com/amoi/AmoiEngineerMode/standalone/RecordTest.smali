.class public Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;
.super Lcom/amoi/AmoiEngineerMode/base/RecordBase;
.source "RecordTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "RecordTest"


# instance fields
.field protected playRecordButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/RecordBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected handleNoSDCardExtra()V
    .locals 2

    const-string v0, "RecordTest"

    const-string v1, "handleNoSDCard"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->playRecordButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method protected initView()V
    .locals 3

    const-string v1, "RecordTest"

    const-string v2, "initView"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x7f030029

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->setContentView(I)V

    const v1, 0x7f060029

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->setTitle(Ljava/lang/CharSequence;)V

    const v1, 0x7f07005b

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->textView:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f07005d

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/amoi/AmoiEngineerMode/base/ExitButtonListener;

    invoke-direct {v1, p0}, Lcom/amoi/AmoiEngineerMode/base/ExitButtonListener;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f07005c

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->playRecordButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->playRecordButton:Landroid/widget/Button;

    const v2, 0x7f06003b

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->playRecordButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "RecordTest"

    const-string v1, "....onClick.....bt_playback."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/amoi/AmoiEngineerMode/base/RecordBase;->hasSDCard:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->playRecordButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->playback()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/RecordTest;->playRecordButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f07005c
        :pswitch_0
    .end packed-switch
.end method
