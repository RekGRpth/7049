.class public Lcom/amoi/AmoiEngineerMode/standalone/SDCard2Test;
.super Lcom/amoi/AmoiEngineerMode/base/SDCardBase;
.source "SDCard2Test.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getInputStreamFromMp3()Ljava/io/InputStream;
    .locals 3

    const v1, 0x7f040003

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SDCard2Test;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    return-object v2
.end method

.method protected getStorgeState()Ljava/lang/String;
    .locals 5

    const-string v0, "/mnt/sdcard2"

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->mContext:Landroid/content/Context;

    const-string v4, "storage"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/storage/StorageManager;

    invoke-virtual {v2, v0}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "mounted"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "/mnt/sdcard"

    invoke-virtual {v2, v3}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method protected getStrogeDirectory()Ljava/io/File;
    .locals 5

    const-string v0, "/mnt/sdcard2"

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->mContext:Landroid/content/Context;

    const-string v4, "storage"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/storage/StorageManager;

    const-string v3, "/mnt/sdcard2"

    invoke-virtual {v2, v3}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "mounted"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v0, "/mnt/sdcard"

    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v3
.end method

.method protected initButton()V
    .locals 2

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->prevButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->nextButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->exitButton:Landroid/widget/Button;

    new-instance v1, Lcom/amoi/AmoiEngineerMode/base/ExitButtonListener;

    invoke-direct {v1, p0}, Lcom/amoi/AmoiEngineerMode/base/ExitButtonListener;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected isExternalStroge2()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected showButton(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/base/SDCardBase;->exitButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method
