.class public Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;
.super Landroid/app/Activity;
.source "SensorResult.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
.field public static final COMPASS:I = 0x3

.field public static final GSENSOR:I = 0x1

.field public static final LSENSOR:I = 0x2

.field public static final PSENSOR:I = 0x0

.field private static final RADIANS_TO_DEGREES:F = 57.29578f

.field private static final TAG:Ljava/lang/String; = "Sensor"


# instance fields
.field private curSensor:I

.field private gsensor_rotation:Landroid/widget/ImageView;

.field private layout:Landroid/widget/LinearLayout;

.field private mLastOrientation:I

.field private mOriginalAccelerometerState:I

.field mSensor:Landroid/hardware/Sensor;

.field protected originalLight:Landroid/view/WindowManager$LayoutParams;

.field result:Landroid/widget/TextView;

.field sensorName:Landroid/widget/TextView;

.field sm:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const v0, 0xffff

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->mLastOrientation:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->mOriginalAccelerometerState:I

    return-void
.end method

.method private changeBackground(F)V
    .locals 2
    .param p1    # F

    const v0, 0x3dcccccd

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->layout:Landroid/widget/LinearLayout;

    const v1, -0xbbbbbc

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->layout:Landroid/widget/LinearLayout;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method private closeAccelerometer()V
    .locals 5

    :try_start_0
    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->mOriginalAccelerometerState:I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->mOriginalAccelerometerState:I

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v2, "Sensor"

    const-string v3, "error to close Accelerometer"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "Sensor"

    const-string v3, "error to get Accelerometer state"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getSensor(I)Landroid/hardware/Sensor;
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->sm:Landroid/hardware/SensorManager;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    const v1, 0x7f060065

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->setTitle(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->sm:Landroid/hardware/SensorManager;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    const v1, 0x7f060063

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->setTitle(I)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->sm:Landroid/hardware/SensorManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    const v1, 0x7f060064

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->setTitle(I)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->sm:Landroid/hardware/SensorManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    const v1, 0x7f060066

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->setTitle(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private openAccelerometer()V
    .locals 4

    iget v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->mOriginalAccelerometerState:I

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "Sensor"

    const-string v2, "error to open Accelerometer"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method protected changeLight(F)V
    .locals 4
    .param p1    # F

    const/high16 v3, 0x41200000

    mul-float v1, p1, v3

    const/high16 v2, 0x437f0000

    div-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    div-float p1, v1, v3

    const v1, 0x3dcccccd

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    const p1, 0x3dcccccd

    :cond_0
    const/high16 v1, 0x3f800000

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    const/high16 p1, 0x3f800000

    :cond_1
    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1    # Landroid/hardware/Sensor;
    .param p2    # I

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030027

    invoke-virtual {p0, v2}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->setContentView(I)V

    const v2, 0x7f070053

    invoke-virtual {p0, v2}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->sensorName:Landroid/widget/TextView;

    const v2, 0x7f070054

    invoke-virtual {p0, v2}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->result:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->originalLight:Landroid/view/WindowManager$LayoutParams;

    const v2, 0x7f070055

    invoke-virtual {p0, v2}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->gsensor_rotation:Landroid/widget/ImageView;

    const v2, 0x7f070052

    invoke-virtual {p0, v2}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->layout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->layout:Landroid/widget/LinearLayout;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "curSensor"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->curSensor:I

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->curSensor:I

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->closeAccelerometer()V

    :cond_0
    const-string v2, "sensor"

    invoke-virtual {p0, v2}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->sm:Landroid/hardware/SensorManager;

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->curSensor:I

    invoke-direct {p0, v2}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->mSensor:Landroid/hardware/Sensor;

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->sensorName:Landroid/widget/TextView;

    iget v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->curSensor:I

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->openAccelerometer()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->originalLight:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method public onOrientationChanged(Landroid/hardware/SensorEvent;)V
    .locals 10
    .param p1    # Landroid/hardware/SensorEvent;

    const/16 v9, 0xe1

    const/16 v8, 0x87

    const/4 v7, 0x0

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v3, v7

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x1

    aget v2, v3, v4

    neg-float v3, v1

    float-to-double v3, v3

    float-to-double v5, v2

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v3

    neg-double v3, v3

    const-wide v5, 0x404ca5dc20000000L

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v0, v3

    if-gez v0, :cond_0

    add-int/lit16 v0, v0, 0x168

    :cond_0
    const/16 v3, 0x2d

    if-le v0, v3, :cond_2

    if-gt v0, v8, :cond_2

    const/16 v0, 0x5a

    :goto_0
    iget v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->mLastOrientation:I

    if-eq v3, v0, :cond_1

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->mLastOrientation:I

    iget v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->mLastOrientation:I

    sparse-switch v3, :sswitch_data_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    if-le v0, v8, :cond_3

    if-gt v0, v9, :cond_3

    const/16 v0, 0xb4

    goto :goto_0

    :cond_3
    if-le v0, v9, :cond_4

    const/16 v3, 0x13b

    if-gt v0, v3, :cond_4

    const/16 v0, 0x10e

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :sswitch_0
    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->gsensor_rotation:Landroid/widget/ImageView;

    const v4, 0x7f020004

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->gsensor_rotation:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :sswitch_1
    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->gsensor_rotation:Landroid/widget/ImageView;

    const v4, 0x7f020005

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->gsensor_rotation:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :sswitch_2
    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->gsensor_rotation:Landroid/widget/ImageView;

    const v4, 0x7f020007

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->gsensor_rotation:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :sswitch_3
    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->gsensor_rotation:Landroid/widget/ImageView;

    const v4, 0x7f020006

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->gsensor_rotation:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->sm:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->curSensor:I

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->sm:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->mSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x0

    invoke-virtual {v1, p0, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "Sensor"

    const-string v2, "error registerListener"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->sm:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->mSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    invoke-virtual {v1, p0, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 8
    .param p1    # Landroid/hardware/SensorEvent;

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    iget v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->curSensor:I

    packed-switch v3, :pswitch_data_0

    :goto_0
    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->result:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :pswitch_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v0, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v2, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aget v3, v2, v5

    invoke-direct {p0, v3}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->changeBackground(F)V

    goto :goto_0

    :pswitch_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v0, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v2, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "y:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v2, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "z:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v2, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->onOrientationChanged(Landroid/hardware/SensorEvent;)V

    goto :goto_0

    :pswitch_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v0, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v2, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aget v3, v2, v5

    invoke-virtual {p0, v3}, Lcom/amoi/AmoiEngineerMode/standalone/SensorResult;->changeLight(F)V

    goto/16 :goto_0

    :pswitch_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x3

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v2, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "y:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v2, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "z:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v2, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
