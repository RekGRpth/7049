.class public Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;
.super Landroid/view/View;
.source "TPMultitouch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field public mInputIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/Vector",
            "<",
            "Ljava/util/Vector",
            "<",
            "Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private mMinPtrId:I

.field public mPtsStatus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;


# direct methods
.method public constructor <init>(Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;

    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mInputIds:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mMinPtrId:I

    return-void
.end method

.method private calcMinId(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mMinPtrId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iput p1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mMinPtrId:I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mMinPtrId:I

    if-ge v0, p1, :cond_1

    iget p1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mMinPtrId:I

    :cond_1
    iput p1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mMinPtrId:I

    goto :goto_0
.end method


# virtual methods
.method public Clear()V
    .locals 5

    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mInputIds:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->clear()V

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->invalidate()V

    return-void
.end method

.method getPaint(I)Landroid/graphics/Paint;
    .locals 10
    .param p1    # I

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0xff

    const/4 v5, 0x3

    const/4 v3, 0x7

    new-array v0, v3, [[I

    new-array v3, v5, [I

    fill-array-data v3, :array_0

    aput-object v3, v0, v7

    new-array v3, v5, [I

    fill-array-data v3, :array_1

    aput-object v3, v0, v8

    new-array v3, v5, [I

    fill-array-data v3, :array_2

    aput-object v3, v0, v9

    new-array v3, v5, [I

    fill-array-data v3, :array_3

    aput-object v3, v0, v5

    const/4 v3, 0x4

    new-array v4, v5, [I

    fill-array-data v4, :array_4

    aput-object v4, v0, v3

    const/4 v3, 0x5

    new-array v4, v5, [I

    fill-array-data v4, :array_5

    aput-object v4, v0, v3

    const/4 v3, 0x6

    new-array v4, v5, [I

    fill-array-data v4, :array_6

    aput-object v4, v0, v3

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/4 v3, 0x7

    if-ge p1, v3, :cond_0

    aget-object v3, v0, p1

    aget v3, v3, v7

    aget-object v4, v0, p1

    aget v4, v4, v8

    aget-object v5, v0, p1

    aget v5, v5, v9

    invoke-virtual {v1, v6, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    :goto_0
    iget-object v3, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;

    iget v3, v3, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;->mPointSize:I

    int-to-double v3, v3

    const-wide v5, 0x400d0a3d70a3d70aL

    mul-double/2addr v3, v5

    const-wide v5, 0x401d7ae147ae147bL

    add-double/2addr v3, v5

    double-to-int v2, v3

    int-to-float v3, v2

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    return-object v1

    :cond_0
    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_0

    :array_0
    .array-data 4
        0xff
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0xff
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x0
        0xff
    .end array-data

    :array_3
    .array-data 4
        0xff
        0xff
        0x0
    .end array-data

    :array_4
    .array-data 4
        0x0
        0xff
        0xff
    .end array-data

    :array_5
    .array-data 4
        0xff
        0x0
        0xff
    .end array-data

    :array_6
    .array-data 4
        0xff
        0xff
        0xff
    .end array-data
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 24
    .param p1    # Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v10, 0x0

    :goto_0
    if-ge v10, v8, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Vector;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v18

    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v6

    const-string v21, "MTXXP"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "input size: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v6, :cond_7

    invoke-virtual {v11, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/Vector;

    invoke-virtual {v15}, Ljava/util/Vector;->size()I

    move-result v7

    const-string v21, "MTXXP"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Line"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " size "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v21, 0x2

    move/from16 v0, v21

    if-le v7, v0, :cond_4

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;

    move-object/from16 v0, v21

    iget v13, v0, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateX:I

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;

    move-object/from16 v0, v21

    iget v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateY:I

    const/4 v9, 0x1

    :goto_2
    if-ge v9, v7, :cond_1

    invoke-virtual {v15, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;

    move-object/from16 v0, v21

    iget v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateX:I

    move/from16 v19, v0

    invoke-virtual {v15, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;

    move-object/from16 v0, v21

    iget v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateY:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;->mDisplayHistory:Z

    move/from16 v21, v0

    if-eqz v21, :cond_0

    int-to-float v0, v13

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;->mPointSize:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move-object/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_0
    move/from16 v13, v19

    move/from16 v14, v20

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v21, v7, -0x1

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;

    add-int/lit8 v21, v6, -0x1

    move/from16 v0, v21

    if-ne v5, v0, :cond_4

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "pid "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    iget v0, v12, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->pid:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " x="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    iget v0, v12, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateX:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", y="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    iget v0, v12, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateY:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    const/16 v21, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v22

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    move/from16 v2, v21

    move/from16 v3, v22

    move-object/from16 v4, v16

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget v0, v12, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateX:I

    move/from16 v21, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v22

    div-int/lit8 v22, v22, 0x2

    sub-int v19, v21, v22

    iget v0, v12, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateY:I

    move/from16 v21, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v22

    mul-int/lit8 v22, v22, 0x3

    sub-int v20, v21, v22

    if-gez v19, :cond_5

    const/16 v19, 0x0

    :cond_2
    :goto_3
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_6

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v20

    :cond_3
    :goto_4
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v21

    move/from16 v3, v22

    move-object/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v0, v12, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateX:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    iget v0, v12, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateY:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;->mPointSize:I

    move/from16 v23, v0

    mul-int/lit8 v23, v23, 0x3

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move-object/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;->me:Landroid/util/DisplayMetrics;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v21, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v22

    sub-int v21, v21, v22

    move/from16 v0, v19

    move/from16 v1, v21

    if-le v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;->me:Landroid/util/DisplayMetrics;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v21, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v22

    sub-int v19, v21, v22

    goto/16 :goto_3

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;->me:Landroid/util/DisplayMetrics;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch;->me:Landroid/util/DisplayMetrics;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v20, v0

    goto/16 :goto_4

    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    :cond_8
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 16
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v2, v1, 0xff

    shr-int/lit8 v12, v1, 0x8

    const/4 v14, 0x5

    if-eq v2, v14, :cond_0

    if-nez v2, :cond_5

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lt v12, v14, :cond_4

    new-instance v11, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;

    invoke-direct {v11}, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;-><init>()V

    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;->isDown:Z

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v14, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    const/4 v5, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-ge v5, v14, :cond_9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;

    iget-boolean v14, v13, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;->isDown:Z

    if-eqz v14, :cond_8

    iget-boolean v14, v13, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;->isNewLine:Z

    if-nez v14, :cond_3

    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mInputIds:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lt v5, v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mInputIds:Ljava/util/ArrayList;

    new-instance v15, Ljava/util/Vector;

    invoke-direct {v15}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mInputIds:Ljava/util/ArrayList;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Vector;

    invoke-virtual {v14, v7}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;->isNewLine:Z

    :cond_3
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;

    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;->isDown:Z

    goto :goto_0

    :cond_5
    const/4 v14, 0x6

    if-eq v2, v14, :cond_6

    const/4 v14, 0x1

    if-ne v2, v14, :cond_1

    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lt v12, v14, :cond_7

    new-instance v11, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;

    invoke-direct {v11}, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;-><init>()V

    const/4 v14, 0x0

    iput-boolean v14, v11, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;->isDown:Z

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v14, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;

    const/4 v14, 0x0

    iput-boolean v14, v11, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;->isDown:Z

    goto/16 :goto_0

    :cond_8
    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointStatusStruct;->isNewLine:Z

    goto :goto_2

    :cond_9
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v10

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v10, :cond_b

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->calcMinId(I)V

    move-object/from16 v0, p0

    iget v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mMinPtrId:I

    sub-int v9, v8, v14

    new-instance v6, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;

    invoke-direct {v6}, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;-><init>()V

    iput v2, v6, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->action:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v14

    float-to-int v14, v14

    iput v14, v6, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateX:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v14

    float-to-int v14, v14

    iput v14, v6, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->coordinateY:I

    iput v9, v6, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->pid:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v14

    iput v14, v6, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->pressure:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getSize(I)F

    move-result v14

    iput v14, v6, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->fat_size:F

    iget v14, v6, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->fat_size:F

    const v15, 0x3c23d70a

    cmpl-float v14, v14, v15

    if-lez v14, :cond_a

    iget v14, v6, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->fat_size:F

    :goto_4
    iput v14, v6, Lcom/amoi/AmoiEngineerMode/standalone/TouchScreen_PointDataStruct;->fat_size:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mInputIds:Ljava/util/ArrayList;

    invoke-virtual {v14, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Vector;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->mInputIds:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/Vector;

    invoke-virtual {v15}, Ljava/util/Vector;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v14, v15}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Vector;

    invoke-virtual {v3, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_a
    const v14, 0x3c23d70a

    goto :goto_4

    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/amoi/AmoiEngineerMode/standalone/TPMultitouch$MyView;->invalidate()V

    const/4 v14, 0x1

    return v14
.end method
