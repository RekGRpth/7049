.class public Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;
.super Landroid/app/Activity;
.source "TPPanelTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mpressed_time:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->mpressed_time:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    iget v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->mpressed_time:I

    const/16 v3, 0x19

    if-lt v2, v3, :cond_1

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    :cond_1
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->mpressed_time:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->mpressed_time:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f07006c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/16 v2, 0x400

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setFlags(II)V

    const v1, 0x7f03002e

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->setContentView(I)V

    const v1, 0x7f07006c

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f07006d

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f07006e

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f07006f

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070070

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070071

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070072

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070073

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070074

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070075

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070076

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070077

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070078

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070079

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f07007a

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f07007b

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f07007c

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f07007d

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f07007e

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f07007f

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070080

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070081

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070082

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070083

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070084

    invoke-virtual {p0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x0

    iput v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TPPanelTest;->mpressed_time:I

    return-void
.end method
