.class public Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;
.super Landroid/view/View;
.source "TouchPanelTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PaintView"
.end annotation


# static fields
.field private static final TOUCH_TOLERANCE:F = 4.0f


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapPaint:Landroid/graphics/Paint;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mPath:Landroid/graphics/Path;

.field private mX:F

.field private mY:F

.field final synthetic this$0:Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;


# direct methods
.method public constructor <init>(Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;Landroid/content/Context;)V
    .locals 3
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;

    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x1e0

    const/16 v1, 0x320

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mBitmap:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mCanvas:Landroid/graphics/Canvas;

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mBitmapPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mPath:Landroid/graphics/Path;

    return-void
.end method

.method private touch_move(FF)V
    .locals 4
    .param p1    # F
    .param p2    # F

    const/high16 v3, 0x40800000

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mX:F

    sub-float v2, p1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mY:F

    sub-float v2, p2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v2, v0, v3

    if-gez v2, :cond_0

    cmpl-float v2, v1, v3

    if-ltz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Path;->lineTo(FF)V

    iput p1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mX:F

    iput p2, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mY:F

    :cond_1
    return-void
.end method

.method private touch_start(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    iput p1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mX:F

    iput p2, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mY:F

    return-void
.end method

.method private touch_up()V
    .locals 3

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mPath:Landroid/graphics/Path;

    iget v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mX:F

    iget v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mY:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;

    # getter for: Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;->mPaint:Landroid/graphics/Paint;
    invoke-static {v2}, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;->access$000(Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    const/4 v2, 0x0

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mBitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->mPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;

    # getter for: Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;->mPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;->access$000(Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->touch_start(FF)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->invalidate()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->touch_move(FF)V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->invalidate()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->touch_up()V

    invoke-virtual {p0}, Lcom/amoi/AmoiEngineerMode/standalone/TouchPanelTest$PaintView;->invalidate()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
