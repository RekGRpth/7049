.class Lcom/amoi/AmoiEngineerMode/standalone/lightTest$1;
.super Ljava/lang/Object;
.source "lightTest.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/amoi/AmoiEngineerMode/standalone/lightTest;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/amoi/AmoiEngineerMode/standalone/lightTest;


# direct methods
.method constructor <init>(Lcom/amoi/AmoiEngineerMode/standalone/lightTest;)V
    .locals 0

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/standalone/lightTest$1;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/lightTest;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v0, 0x0

    packed-switch p3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/lightTest$1;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/lightTest;

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/ButtonLightTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/lightTest$1;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/lightTest;

    invoke-virtual {v1, v0}, Lcom/amoi/AmoiEngineerMode/standalone/lightTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/lightTest$1;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/lightTest;

    const-class v2, Lcom/amoi/AmoiEngineerMode/standalone/BacklightTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/standalone/lightTest$1;->this$0:Lcom/amoi/AmoiEngineerMode/standalone/lightTest;

    invoke-virtual {v1, v0}, Lcom/amoi/AmoiEngineerMode/standalone/lightTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
