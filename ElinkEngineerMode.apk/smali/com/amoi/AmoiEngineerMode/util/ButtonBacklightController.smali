.class public Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;
.super Ljava/lang/Object;
.source "ButtonBacklightController.java"


# static fields
.field public static final STATE_OFF:F = 0.0f

.field public static final STATE_ON:F = 1.0f


# instance fields
.field private mWindow:Landroid/view/Window;


# direct methods
.method public constructor <init>(Landroid/view/Window;)V
    .locals 0
    .param p1    # Landroid/view/Window;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;->mWindow:Landroid/view/Window;

    return-void
.end method


# virtual methods
.method public getLightState()F
    .locals 2

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;->mWindow:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    return v1
.end method

.method public setLightState(F)V
    .locals 2
    .param p1    # F

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;->mWindow:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    iget-object v1, p0, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;->mWindow:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method public turnOff()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;->setLightState(F)V

    return-void
.end method

.method public turnOn()V
    .locals 1

    const/high16 v0, 0x3f800000

    invoke-virtual {p0, v0}, Lcom/amoi/AmoiEngineerMode/util/ButtonBacklightController;->setLightState(F)V

    return-void
.end method
