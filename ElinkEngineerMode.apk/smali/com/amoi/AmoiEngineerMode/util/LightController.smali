.class public Lcom/amoi/AmoiEngineerMode/util/LightController;
.super Ljava/lang/Object;
.source "LightController.java"


# static fields
.field public static final BUTTON_LED:I = 0x3

.field private static final BUTTON_LED_FILE:Ljava/lang/String; = "/sys/class/leds/button-backlight/brightness"

.field public static final GREEN_LED:I = 0x1

.field private static final GREEN_LED_FILE:Ljava/lang/String; = "/sys/class/leds/green/brightness"

.field public static final RED_LED:I = 0x0

.field private static final RED_LED_FILE:Ljava/lang/String; = "/sys/class/leds/red/brightness"

.field public static final STATE_OFF:Ljava/lang/String; = "0"

.field public static final STATE_ON:Ljava/lang/String; = "255"

.field private static final TAG:Ljava/lang/String; = "LightController"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private applyToDevice(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v3, "LightController"

    const-string v4, "error to change led"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private getDeviceFile(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-object v0

    :pswitch_1
    const-string v0, "/sys/class/leds/red/brightness"

    goto :goto_0

    :pswitch_2
    const-string v0, "/sys/class/leds/green/brightness"

    goto :goto_0

    :pswitch_3
    const-string v0, "/sys/class/leds/button-backlight/brightness"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getLightState(I)Ljava/lang/String;
    .locals 10
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->getDeviceFile(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v8, ""

    :goto_0
    return-object v8

    :cond_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v8, 0x400

    new-array v0, v8, [B

    const/4 v1, 0x0

    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v6, 0x0

    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    :goto_1
    invoke-virtual {v5, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v6

    const/4 v8, -0x1

    if-eq v6, v8, :cond_1

    const/4 v8, 0x0

    invoke-virtual {v7, v0, v8, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    const-string v8, "LightController"

    const-string v9, "error to read led state"

    invoke-static {v8, v9, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v8, ""

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public isShowLED(I)V
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v0, "/sys/class/leds/red/brightness"

    const-string v1, "255"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string v0, "/sys/class/leds/red/brightness"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    const-string v0, "/sys/class/leds/button-backlight/brightness"

    const-string v1, "255"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setLEDStates(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v0, "/sys/class/leds/red/brightness"

    invoke-direct {p0, v0, p1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "/sys/class/leds/green/brightness"

    invoke-direct {p0, v0, p2}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setLightState(ILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->getDeviceFile(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0, p2}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showLED(I)V
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v0, "/sys/class/leds/red/brightness"

    const-string v1, "255"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "/sys/class/leds/green/brightness"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string v0, "/sys/class/leds/red/brightness"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "/sys/class/leds/green/brightness"

    const-string v1, "255"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    const-string v0, "/sys/class/leds/button-backlight/brightness"

    const-string v1, "255"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public turnOffButtonLight()V
    .locals 2

    const-string v0, "/sys/class/leds/button-backlight/brightness"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public turnOffLEDs()V
    .locals 2

    const-string v0, "/sys/class/leds/red/brightness"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "/sys/class/leds/green/brightness"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public turnOnButtonLight()V
    .locals 2

    const-string v0, "/sys/class/leds/button-backlight/brightness"

    const-string v1, "255"

    invoke-direct {p0, v0, v1}, Lcom/amoi/AmoiEngineerMode/util/LightController;->applyToDevice(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
