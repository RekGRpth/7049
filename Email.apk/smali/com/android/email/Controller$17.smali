.class Lcom/android/email/Controller$17;
.super Ljava/lang/Object;
.source "Controller.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/email/Controller;->deleteAccount(JLcom/android/email/activity/setup/AccountSettings$RemoveAccountCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/email/Controller;

.field final synthetic val$accountId:J

.field final synthetic val$callback:Lcom/android/email/activity/setup/AccountSettings$RemoveAccountCallback;


# direct methods
.method constructor <init>(Lcom/android/email/Controller;JLcom/android/email/activity/setup/AccountSettings$RemoveAccountCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/Controller$17;->this$0:Lcom/android/email/Controller;

    iput-wide p2, p0, Lcom/android/email/Controller$17;->val$accountId:J

    iput-object p4, p0, Lcom/android/email/Controller$17;->val$callback:Lcom/android/email/activity/setup/AccountSettings$RemoveAccountCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const-string v0, "Controller#deleteAccount"

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->printStartLog(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/email/Controller$17;->this$0:Lcom/android/email/Controller;

    iget-wide v1, p0, Lcom/android/email/Controller$17;->val$accountId:J

    iget-object v3, p0, Lcom/android/email/Controller$17;->this$0:Lcom/android/email/Controller;

    invoke-static {v3}, Lcom/android/email/Controller;->access$400(Lcom/android/email/Controller;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/email/Controller;->deleteAccountSync(JLandroid/content/Context;)V

    iget-object v0, p0, Lcom/android/email/Controller$17;->val$callback:Lcom/android/email/activity/setup/AccountSettings$RemoveAccountCallback;

    iget-wide v1, p0, Lcom/android/email/Controller$17;->val$accountId:J

    invoke-virtual {v0, v1, v2}, Lcom/android/email/activity/setup/AccountSettings$RemoveAccountCallback;->finishRemoveAccount(J)V

    const-string v0, "Controller#deleteAccount"

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->printStopLog(Ljava/lang/String;)V

    return-void
.end method
