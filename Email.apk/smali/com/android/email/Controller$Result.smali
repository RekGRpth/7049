.class public abstract Lcom/android/email/Controller$Result;
.super Ljava/lang/Object;
.source "Controller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/Controller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Result"
.end annotation


# instance fields
.field private volatile mRegistered:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected final isRegistered()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/email/Controller$Result;->mRegistered:Z

    return v0
.end method

.method public loadAttachmentCallback(Lcom/android/emailcommon/mail/MessagingException;JJJI)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # J
    .param p6    # J
    .param p8    # I

    return-void
.end method

.method public loadMessageForViewCallback(Lcom/android/emailcommon/mail/MessagingException;JJI)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # J
    .param p6    # I

    return-void
.end method

.method public sendMailCallback(Lcom/android/emailcommon/mail/MessagingException;JJI)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # J
    .param p6    # I

    return-void
.end method

.method public serviceCheckMailCallback(Lcom/android/emailcommon/mail/MessagingException;JJIJ)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # J
    .param p6    # I
    .param p7    # J

    return-void
.end method

.method protected setRegistered(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/Controller$Result;->mRegistered:Z

    return-void
.end method

.method public synchronizeEnvelopeFinished(JJ)V
    .locals 0
    .param p1    # J
    .param p3    # J

    return-void
.end method

.method public updateMailboxCallback(Lcom/android/emailcommon/mail/MessagingException;JJIILjava/util/ArrayList;)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # J
    .param p6    # I
    .param p7    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/emailcommon/mail/MessagingException;",
            "JJII",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public updateMailboxListCallback(Lcom/android/emailcommon/mail/MessagingException;JI)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # I

    return-void
.end method
