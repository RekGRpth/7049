.class Lcom/android/email/MessagingController$1;
.super Ljava/lang/Object;
.source "MessagingController.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/MessagingController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/email/MessagingController$Command;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/email/MessagingController;


# direct methods
.method constructor <init>(Lcom/android/email/MessagingController;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/MessagingController$1;->this$0:Lcom/android/email/MessagingController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/email/MessagingController$Command;Lcom/android/email/MessagingController$Command;)I
    .locals 2
    .param p1    # Lcom/android/email/MessagingController$Command;
    .param p2    # Lcom/android/email/MessagingController$Command;

    iget v0, p1, Lcom/android/email/MessagingController$Command;->priorityNumber:I

    iget v1, p2, Lcom/android/email/MessagingController$Command;->priorityNumber:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/android/email/MessagingController$Command;

    check-cast p2, Lcom/android/email/MessagingController$Command;

    invoke-virtual {p0, p1, p2}, Lcom/android/email/MessagingController$1;->compare(Lcom/android/email/MessagingController$Command;Lcom/android/email/MessagingController$Command;)I

    move-result v0

    return v0
.end method
