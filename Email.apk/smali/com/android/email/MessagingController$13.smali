.class Lcom/android/email/MessagingController$13;
.super Lcom/android/email/MessagingController$CancelableRunnable;
.source "MessagingController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/email/MessagingController;->loadAttachment(JJJJLcom/android/email/MessagingListener;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mIsCancelled:Z

.field final synthetic this$0:Lcom/android/email/MessagingController;

.field final synthetic val$accountId:J

.field final synthetic val$attachmentId:J

.field final synthetic val$background:Z

.field final synthetic val$mailboxId:J

.field final synthetic val$messageId:J


# direct methods
.method constructor <init>(Lcom/android/email/MessagingController;JJJZJ)V
    .locals 1

    iput-object p1, p0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iput-wide p2, p0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    iput-wide p4, p0, Lcom/android/email/MessagingController$13;->val$accountId:J

    iput-wide p6, p0, Lcom/android/email/MessagingController$13;->val$messageId:J

    iput-boolean p8, p0, Lcom/android/email/MessagingController$13;->val$background:Z

    iput-wide p9, p0, Lcom/android/email/MessagingController$13;->val$mailboxId:J

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/email/MessagingController$CancelableRunnable;-><init>(Lcom/android/email/MessagingController;Lcom/android/email/MessagingController$1;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/MessagingController$13;->mIsCancelled:Z

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 3

    const-string v1, "cancel loadAttachment"

    invoke-static {v1}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v1}, Lcom/android/email/MessagingController;->access$1300(Lcom/android/email/MessagingController;)Lcom/android/emailcommon/mail/Folder;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v1}, Lcom/android/email/MessagingController;->access$1300(Lcom/android/email/MessagingController;)Lcom/android/emailcommon/mail/Folder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/emailcommon/mail/Folder;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Attachment"

    const-string v2, "Close the connection"

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/email/MessagingController$13;->mIsCancelled:Z

    iget-object v1, p0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v1}, Lcom/android/email/MessagingController;->access$1300(Lcom/android/email/MessagingController;)Lcom/android/emailcommon/mail/Folder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/emailcommon/mail/Folder;->closeConnection()V

    iget-object v1, p0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v1}, Lcom/android/email/MessagingController;->access$1300(Lcom/android/email/MessagingController;)Lcom/android/emailcommon/mail/Folder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/emailcommon/mail/Folder;->close(Z)V

    iget-object v1, p0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "loadAttachment cancel failed"

    invoke-static {v1, v0}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public run()V
    .locals 33

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Attachment"

    const-string v3, "Attachment downloading canceled before really starting"

    invoke-static {v2, v3}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    :goto_0
    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_0
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$100(Lcom/android/email/MessagingController;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v2, v3, v4}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v20

    if-nez v20, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$300(Lcom/android/email/MessagingController;)Lcom/android/email/GroupMessagingListener;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$accountId:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/email/MessagingController$13;->val$messageId:J

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    new-instance v9, Lcom/android/emailcommon/mail/MessagingException;

    const-string v10, "The attachment is null"

    invoke-direct {v9, v10}, Lcom/android/emailcommon/mail/MessagingException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/android/email/MessagingController$13;->val$background:Z

    invoke-virtual/range {v2 .. v10}, Lcom/android/email/GroupMessagingListener;->loadAttachmentFailed(JJJLcom/android/emailcommon/mail/MessagingException;Z)V
    :try_end_2
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto :goto_0

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    :cond_1
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$100(Lcom/android/email/MessagingController;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-static {v2, v0}, Lcom/android/emailcommon/utility/Utility;->attachmentExists(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$300(Lcom/android/email/MessagingController;)Lcom/android/email/GroupMessagingListener;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$accountId:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/email/MessagingController$13;->val$messageId:J

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-virtual/range {v2 .. v8}, Lcom/android/email/GroupMessagingListener;->loadAttachmentFinished(JJJ)V
    :try_end_4
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto/16 :goto_0

    :catchall_2
    move-exception v2

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v2

    :cond_2
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$100(Lcom/android/email/MessagingController;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$accountId:J

    invoke-static {v2, v3, v4}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$100(Lcom/android/email/MessagingController;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$mailboxId:J

    invoke-static {v2, v3, v4}, Lcom/android/emailcommon/provider/Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$100(Lcom/android/email/MessagingController;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$messageId:J

    invoke-static {v2, v3, v4}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v26

    if-eqz v19, :cond_3

    if-eqz v25, :cond_3

    if-nez v26, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$300(Lcom/android/email/MessagingController;)Lcom/android/email/GroupMessagingListener;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$accountId:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/email/MessagingController$13;->val$messageId:J

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    new-instance v9, Lcom/android/emailcommon/mail/MessagingException;

    const-string v10, "Account, mailbox, message or attachment are null"

    invoke-direct {v9, v10}, Lcom/android/emailcommon/mail/MessagingException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/android/email/MessagingController$13;->val$background:Z

    invoke-virtual/range {v2 .. v10}, Lcom/android/email/GroupMessagingListener;->loadAttachmentFailed(JJJLcom/android/emailcommon/mail/MessagingException;Z)V
    :try_end_6
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto/16 :goto_0

    :catchall_3
    move-exception v2

    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v2

    :cond_4
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$100(Lcom/android/email/MessagingController;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-static {v2, v0}, Lcom/android/emailcommon/TrafficFlags;->getAttachmentFlags(Landroid/content/Context;Lcom/android/emailcommon/provider/Account;)I

    move-result v2

    invoke-static {v2}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/android/emailcommon/provider/Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mProtocolSearchInfo:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mProtocolSearchInfo:Ljava/lang/String;

    move-object/from16 v29, v0

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$100(Lcom/android/email/MessagingController;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/android/email/mail/Store;->getInstance(Lcom/android/emailcommon/provider/Account;Landroid/content/Context;)Lcom/android/email/mail/Store;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/android/email/mail/Store;->getFolder(Ljava/lang/String;)Lcom/android/emailcommon/mail/Folder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_8
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_c

    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "Attachment"

    const-string v4, "Attachment downloading canceled before opening remote folder"

    invoke-static {v2, v4}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto/16 :goto_0

    :catchall_4
    move-exception v2

    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v2

    :cond_6
    :try_start_b
    sget-object v2, Lcom/android/emailcommon/mail/Folder$OpenMode;->READ_WRITE:Lcom/android/emailcommon/mail/Folder$OpenMode;

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/android/emailcommon/mail/Folder;->open(Lcom/android/emailcommon/mail/Folder$OpenMode;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    move-object/from16 v0, v28

    invoke-static {v2, v0}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    :try_start_c
    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/android/emailcommon/mail/Folder;->createMessage(Ljava/lang/String;)Lcom/android/emailcommon/mail/Message;

    move-result-object v31

    new-instance v32, Lcom/android/emailcommon/internet/MimeBodyPart;

    invoke-direct/range {v32 .. v32}, Lcom/android/emailcommon/internet/MimeBodyPart;-><init>()V

    move-object/from16 v0, v20

    iget-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    long-to-int v2, v2

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Lcom/android/emailcommon/internet/MimeBodyPart;->setSize(I)V

    const-string v2, "X-Android-Attachment-StoreData"

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    move-object/from16 v0, v32

    invoke-virtual {v0, v2, v3}, Lcom/android/emailcommon/internet/MimeBodyPart;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Content-Type"

    const-string v3, "%s;\n name=\"%s\""

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, v20

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, v20

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v32

    invoke-virtual {v0, v2, v3}, Lcom/android/emailcommon/internet/MimeBodyPart;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadAttachment  attachment : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mEncoding:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v22, "base64"

    :cond_7
    const-string v2, "Content-Transfer-Encoding"

    move-object/from16 v0, v32

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Lcom/android/emailcommon/internet/MimeBodyPart;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v27, Lcom/android/emailcommon/internet/MimeMultipart;

    invoke-direct/range {v27 .. v27}, Lcom/android/emailcommon/internet/MimeMultipart;-><init>()V

    const-string v2, "mixed"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Lcom/android/emailcommon/internet/MimeMultipart;->setSubType(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/Multipart;->addBodyPart(Lcom/android/emailcommon/mail/BodyPart;)V

    const-string v2, "Content-Type"

    const-string v3, "multipart/mixed"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Lcom/android/emailcommon/mail/Message;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/Message;->setBody(Lcom/android/emailcommon/mail/Body;)V

    new-instance v23, Lcom/android/emailcommon/mail/FetchProfile;

    invoke-direct/range {v23 .. v23}, Lcom/android/emailcommon/mail/FetchProfile;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x1

    new-array v8, v2, [Lcom/android/emailcommon/mail/Message;

    const/4 v2, 0x0

    aput-object v31, v8, v2

    new-instance v2, Lcom/android/email/Controller$MessageRetrievalListenerBridge;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v3}, Lcom/android/email/MessagingController;->access$1500(Lcom/android/email/MessagingController;)Lcom/android/email/Controller;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$messageId:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-direct/range {v2 .. v7}, Lcom/android/email/Controller$MessageRetrievalListenerBridge;-><init>(Lcom/android/email/Controller;JJ)V

    move-object/from16 v0, v28

    move-object/from16 v1, v23

    invoke-virtual {v0, v8, v1, v2}, Lcom/android/emailcommon/mail/Folder;->fetch([Lcom/android/emailcommon/mail/Message;Lcom/android/emailcommon/mail/FetchProfile;Lcom/android/emailcommon/mail/Folder$MessageRetrievalListener;)V

    const/4 v2, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/android/emailcommon/mail/Folder;->close(Z)V

    invoke-virtual/range {v32 .. v32}, Lcom/android/emailcommon/internet/MimeBodyPart;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v2

    if-nez v2, :cond_8

    new-instance v2, Lcom/android/emailcommon/mail/MessagingException;

    const-string v3, "Attachment not loaded."

    invoke-direct {v2, v3}, Lcom/android/emailcommon/mail/MessagingException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_c
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_c

    :catch_0
    move-exception v9

    :try_start_d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/email/MessagingController$13;->mIsCancelled:Z
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_c

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto/16 :goto_0

    :catchall_5
    move-exception v2

    monitor-exit v3
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    throw v2

    :catchall_6
    move-exception v2

    :try_start_f
    monitor-exit v3
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    :try_start_10
    throw v2
    :try_end_10
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_c

    :catch_1
    move-exception v24

    :try_start_11
    const-string v2, "Email"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while storing attachment."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/email/MessagingController$13;->mIsCancelled:Z
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_c

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto/16 :goto_0

    :catchall_7
    move-exception v2

    monitor-exit v3
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_7

    throw v2

    :cond_8
    :try_start_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "Attachment"

    const-string v3, "Abandon downloaded attachment data"

    invoke-static {v2, v3}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_13
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_13 .. :try_end_13} :catch_0
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_13 .. :try_end_13} :catch_2
    .catchall {:try_start_13 .. :try_end_13} :catchall_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto/16 :goto_0

    :catchall_8
    move-exception v2

    monitor-exit v3
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_8

    throw v2

    :cond_9
    :try_start_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$100(Lcom/android/email/MessagingController;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$accountId:J

    move-object/from16 v0, v32

    move-object/from16 v1, v20

    invoke-static {v2, v0, v1, v3, v4}, Lcom/android/email/LegacyConversions;->saveAttachmentBody(Landroid/content/Context;Lcom/android/emailcommon/mail/Part;Lcom/android/emailcommon/provider/EmailContent$Attachment;J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$300(Lcom/android/email/MessagingController;)Lcom/android/email/GroupMessagingListener;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$accountId:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/email/MessagingController$13;->val$messageId:J

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-virtual/range {v2 .. v8}, Lcom/android/email/GroupMessagingListener;->loadAttachmentFinished(JJJ)V
    :try_end_15
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_15 .. :try_end_15} :catch_0
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_15 .. :try_end_15} :catch_2
    .catchall {:try_start_15 .. :try_end_15} :catchall_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto/16 :goto_0

    :catchall_9
    move-exception v2

    monitor-exit v3
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_9

    throw v2

    :cond_a
    :try_start_17
    sget-boolean v2, Lcom/android/emailcommon/Logging;->LOGD:Z

    if-eqz v2, :cond_b

    const-string v2, "Email"

    const-string v3, ""

    invoke-static {v2, v3, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$300(Lcom/android/email/MessagingController;)Lcom/android/email/GroupMessagingListener;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/MessagingController$13;->val$accountId:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/email/MessagingController$13;->val$messageId:J

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/android/email/MessagingController$13;->val$background:Z

    invoke-virtual/range {v2 .. v10}, Lcom/android/email/GroupMessagingListener;->loadAttachmentFailed(JJJLcom/android/emailcommon/mail/MessagingException;Z)V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto/16 :goto_0

    :catchall_a
    move-exception v2

    monitor-exit v3
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_a

    throw v2

    :cond_c
    :try_start_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$300(Lcom/android/email/MessagingController;)Lcom/android/email/GroupMessagingListener;

    move-result-object v10

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/android/email/MessagingController$13;->val$accountId:J

    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/android/email/MessagingController$13;->val$messageId:J

    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    new-instance v17, Lcom/android/emailcommon/mail/MessagingException;

    const/4 v2, 0x1

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3}, Lcom/android/emailcommon/mail/MessagingException;-><init>(ILjava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/email/MessagingController$13;->val$background:Z

    move/from16 v18, v0

    invoke-virtual/range {v10 .. v18}, Lcom/android/email/GroupMessagingListener;->loadAttachmentFailed(JJJLcom/android/emailcommon/mail/MessagingException;Z)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto/16 :goto_0

    :catchall_b
    move-exception v2

    monitor-exit v3
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_b

    throw v2

    :catch_2
    move-exception v21

    :try_start_1b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/email/MessagingController$13;->mIsCancelled:Z

    if-nez v2, :cond_d

    throw v21
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_c

    :catchall_c
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v3, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v5, -0x1

    invoke-static {v4, v5, v6}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v4, v4, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_e

    throw v2

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/email/MessagingController;->access$1302(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/Folder;)Lcom/android/emailcommon/mail/Folder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Lcom/android/email/MessagingController;->access$1402(Lcom/android/email/MessagingController;J)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/MessagingController$13;->this$0:Lcom/android/email/MessagingController;

    iget-object v2, v2, Lcom/android/email/MessagingController;->mCancelledAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/MessagingController$13;->val$attachmentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto/16 :goto_0

    :catchall_d
    move-exception v2

    monitor-exit v3
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_d

    throw v2

    :catchall_e
    move-exception v2

    :try_start_1e
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_e

    throw v2
.end method
