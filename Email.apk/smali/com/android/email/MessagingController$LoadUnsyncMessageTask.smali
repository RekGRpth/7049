.class public Lcom/android/email/MessagingController$LoadUnsyncMessageTask;
.super Ljava/lang/Object;
.source "MessagingController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/MessagingController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LoadUnsyncMessageTask"
.end annotation


# instance fields
.field private final HEADER:I

.field private mAccountInner:Lcom/android/emailcommon/provider/Account;

.field private mRemoteStoreInner:Lcom/android/email/mail/Store;

.field private mToMailboxInner:Lcom/android/emailcommon/provider/Mailbox;

.field private mUnsyncedMessageInner:Lcom/android/emailcommon/mail/Message;

.field final synthetic this$0:Lcom/android/email/MessagingController;


# direct methods
.method public constructor <init>(Lcom/android/email/MessagingController;Lcom/android/emailcommon/provider/Account;Lcom/android/email/mail/Store;Lcom/android/emailcommon/provider/Mailbox;)V
    .locals 1
    .param p2    # Lcom/android/emailcommon/provider/Account;
    .param p3    # Lcom/android/email/mail/Store;
    .param p4    # Lcom/android/emailcommon/provider/Mailbox;

    iput-object p1, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->HEADER:I

    iput-object p2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->mAccountInner:Lcom/android/emailcommon/provider/Account;

    iput-object p3, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->mRemoteStoreInner:Lcom/android/email/mail/Store;

    iput-object p4, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->mToMailboxInner:Lcom/android/emailcommon/provider/Mailbox;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    :try_start_0
    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->mRemoteStoreInner:Lcom/android/email/mail/Store;

    iget-object v3, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->mToMailboxInner:Lcom/android/emailcommon/provider/Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/email/mail/Store;->getFolder(Ljava/lang/String;)Lcom/android/emailcommon/mail/Folder;

    move-result-object v1

    sget-object v2, Lcom/android/emailcommon/mail/Folder$OpenMode;->READ_WRITE:Lcom/android/emailcommon/mail/Folder$OpenMode;

    invoke-virtual {v1, v2}, Lcom/android/emailcommon/mail/Folder;->open(Lcom/android/emailcommon/mail/Folder$OpenMode;)V

    :goto_0
    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-virtual {v2}, Lcom/android/email/MessagingController;->haveUnsyncedMessages()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$1600(Lcom/android/email/MessagingController;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$1600(Lcom/android/email/MessagingController;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/mail/Message;

    iput-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->mUnsyncedMessageInner:Lcom/android/emailcommon/mail/Message;

    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$1600(Lcom/android/email/MessagingController;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v4, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->mUnsyncedMessageInner:Lcom/android/emailcommon/mail/Message;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    iget-object v3, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->mAccountInner:Lcom/android/emailcommon/provider/Account;

    iget-object v4, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->mUnsyncedMessageInner:Lcom/android/emailcommon/mail/Message;

    iget-object v5, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->mToMailboxInner:Lcom/android/emailcommon/provider/Mailbox;

    invoke-virtual {v2, v3, v1, v4, v5}, Lcom/android/email/MessagingController;->loadUnsyncedMessage(Lcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/mail/Folder;Lcom/android/emailcommon/mail/Message;Lcom/android/emailcommon/provider/Mailbox;)V
    :try_end_2
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v2, "LoadUnsyncMessageAsyncTask"

    invoke-static {v2, v0}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2, v0}, Lcom/android/email/MessagingController;->access$1702(Lcom/android/email/MessagingController;Lcom/android/emailcommon/mail/MessagingException;)Lcom/android/emailcommon/mail/MessagingException;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$1600(Lcom/android/email/MessagingController;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    :try_start_4
    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$1810(Lcom/android/email/MessagingController;)I

    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$1800(Lcom/android/email/MessagingController;)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$1600(Lcom/android/email/MessagingController;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    :cond_0
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :goto_1
    return-void

    :catchall_0
    move-exception v2

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v2
    :try_end_6
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v2

    iget-object v3, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v3}, Lcom/android/email/MessagingController;->access$1600(Lcom/android/email/MessagingController;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    :try_start_7
    iget-object v4, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v4}, Lcom/android/email/MessagingController;->access$1810(Lcom/android/email/MessagingController;)I

    iget-object v4, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v4}, Lcom/android/email/MessagingController;->access$1800(Lcom/android/email/MessagingController;)I

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v4}, Lcom/android/email/MessagingController;->access$1600(Lcom/android/email/MessagingController;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notify()V

    :cond_1
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    throw v2

    :cond_2
    const/4 v2, 0x0

    :try_start_8
    invoke-virtual {v1, v2}, Lcom/android/emailcommon/mail/Folder;->close(Z)V
    :try_end_8
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$1600(Lcom/android/email/MessagingController;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    :try_start_9
    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$1810(Lcom/android/email/MessagingController;)I

    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$1800(Lcom/android/email/MessagingController;)I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/email/MessagingController$LoadUnsyncMessageTask;->this$0:Lcom/android/email/MessagingController;

    invoke-static {v2}, Lcom/android/email/MessagingController;->access$1600(Lcom/android/email/MessagingController;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    :cond_3
    monitor-exit v3

    goto :goto_1

    :catchall_2
    move-exception v2

    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v2

    :catchall_3
    move-exception v2

    :try_start_a
    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v2

    :catchall_4
    move-exception v2

    :try_start_b
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    throw v2
.end method
