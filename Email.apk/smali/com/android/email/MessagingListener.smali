.class public Lcom/android/email/MessagingListener;
.super Ljava/lang/Object;
.source "MessagingListener.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkMailFinished(Landroid/content/Context;JJJ)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # J
    .param p4    # J
    .param p6    # J

    return-void
.end method

.method public checkMailStarted(Landroid/content/Context;JJ)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # J
    .param p4    # J

    return-void
.end method

.method public controllerCommandCompleted(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public listFoldersFailed(JLjava/lang/String;)V
    .locals 0
    .param p1    # J
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public listFoldersFinished(J)V
    .locals 0
    .param p1    # J

    return-void
.end method

.method public listFoldersStarted(J)V
    .locals 0
    .param p1    # J

    return-void
.end method

.method public loadAttachmentFailed(JJJLcom/android/emailcommon/mail/MessagingException;Z)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # J
    .param p7    # Lcom/android/emailcommon/mail/MessagingException;
    .param p8    # Z

    return-void
.end method

.method public loadAttachmentFinished(JJJ)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # J

    return-void
.end method

.method public loadAttachmentStarted(JJJZ)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # J
    .param p7    # Z

    return-void
.end method

.method public loadMessageForViewFailed(JLjava/lang/String;)V
    .locals 0
    .param p1    # J
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public loadMessageForViewFinished(J)V
    .locals 0
    .param p1    # J

    return-void
.end method

.method public loadMessageForViewStarted(J)V
    .locals 0
    .param p1    # J

    return-void
.end method

.method public messageUidChanged(JJLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    return-void
.end method

.method public sendPendingMessagesCompleted(J)V
    .locals 0
    .param p1    # J

    return-void
.end method

.method public sendPendingMessagesFailed(JJLjava/lang/Exception;)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/Exception;

    return-void
.end method

.method public sendPendingMessagesStarted(JJ)V
    .locals 0
    .param p1    # J
    .param p3    # J

    return-void
.end method

.method public synchronizeEnvelopeFinished(JJ)V
    .locals 0
    .param p1    # J
    .param p3    # J

    return-void
.end method

.method public synchronizeMailboxFailed(JJLjava/lang/Exception;)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/Exception;

    return-void
.end method

.method public synchronizeMailboxFinished(JJIILjava/util/ArrayList;)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # I
    .param p6    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJII",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public synchronizeMailboxStarted(JJ)V
    .locals 0
    .param p1    # J
    .param p3    # J

    return-void
.end method
