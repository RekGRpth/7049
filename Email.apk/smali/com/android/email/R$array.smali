.class public final Lcom/android/email/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final account_settings_check_frequency_entries:I = 0x7f060000

.field public static final account_settings_check_frequency_entries_push:I = 0x7f060002

.field public static final account_settings_check_frequency_values:I = 0x7f060001

.field public static final account_settings_check_frequency_values_push:I = 0x7f060003

.field public static final account_settings_mail_window_entries:I = 0x7f060004

.field public static final account_settings_mail_window_entries_with_default:I = 0x7f060006

.field public static final account_settings_mail_window_values:I = 0x7f060005

.field public static final account_settings_mail_window_values_with_default:I = 0x7f060007

.field public static final combined_view_account_colors:I = 0x7f06000e

.field public static final general_preference_auto_advance_entries:I = 0x7f060008

.field public static final general_preference_auto_advance_values:I = 0x7f060009

.field public static final general_preference_text_zoom_entries:I = 0x7f06000a

.field public static final general_preference_text_zoom_size:I = 0x7f060011

.field public static final general_preference_text_zoom_summary_array:I = 0x7f060010

.field public static final general_preference_text_zoom_values:I = 0x7f06000b

.field public static final mailbox_display_icons:I = 0x7f06000d

.field public static final mailbox_display_names:I = 0x7f06000c

.field public static final subject_lengths:I = 0x7f06000f


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
