.class public final Lcom/android/email/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final account_dropdown_dropdownwidth:I = 0x7f0a0042

.field public static final action_bar_account_name_max_width:I = 0x7f0a0031

.field public static final action_bar_mailbox_name_left_margin:I = 0x7f0a0033

.field public static final action_bar_mailbox_name_max_width:I = 0x7f0a0032

.field public static final actionbar_subject_padding_right:I = 0x7f0a0043

.field public static final button_minWidth:I = 0x7f0a0006

.field public static final checkmark_left_margin_wide:I = 0x7f0a002d

.field public static final checkmark_margin_top:I = 0x7f0a002a

.field public static final chip_height:I = 0x7f0a0046

.field public static final chip_padding:I = 0x7f0a0045

.field public static final chip_text_size:I = 0x7f0a0047

.field public static final compose_area_left_padding:I = 0x7f0a003b

.field public static final compose_area_right_padding:I = 0x7f0a003c

.field public static final compose_scrollview_width:I = 0x7f0a0005

.field public static final date_font_size:I = 0x7f0a0041

.field public static final date_margin_top:I = 0x7f0a002c

.field public static final date_region_width_wide:I = 0x7f0a002e

.field public static final error_message_height:I = 0x7f0a0010

.field public static final line_spacing_extra:I = 0x7f0a0048

.field public static final mailbox_list_count_margin_left:I = 0x7f0a0027

.field public static final mailbox_list_padding_left:I = 0x7f0a0025

.field public static final mailbox_list_padding_right:I = 0x7f0a0026

.field public static final mailbox_list_width:I = 0x7f0a0003

.field public static final message_compose_field_label_width:I = 0x7f0a003a

.field public static final message_header_badge_width:I = 0x7f0a000e

.field public static final message_header_bottom_padding:I = 0x7f0a0034

.field public static final message_header_height:I = 0x7f0a000d

.field public static final message_header_name_margin:I = 0x7f0a000f

.field public static final message_list_badge_margin:I = 0x7f0a000b

.field public static final message_list_drag_count_font_size:I = 0x7f0a0015

.field public static final message_list_drag_count_left_margin:I = 0x7f0a0016

.field public static final message_list_drag_message_font_size:I = 0x7f0a0017

.field public static final message_list_drag_message_right_margin:I = 0x7f0a0018

.field public static final message_list_drag_offset:I = 0x7f0a0014

.field public static final message_list_icon_padding_compensation:I = 0x7f0a000c

.field public static final message_list_item_color_tip_height:I = 0x7f0a000a

.field public static final message_list_item_color_tip_width:I = 0x7f0a0009

.field public static final message_list_item_height_normal:I = 0x7f0a0008

.field public static final message_list_item_height_wide:I = 0x7f0a0007

.field public static final message_list_width:I = 0x7f0a0004

.field public static final minimum_width_wide_mode:I = 0x7f0a0023

.field public static final replystate_margin_left:I = 0x7f0a0028

.field public static final replystate_margin_top:I = 0x7f0a0029

.field public static final search_view_width:I = 0x7f0a0030

.field public static final senders_font_size:I = 0x7f0a003f

.field public static final settings_buttons_padding_bottom:I = 0x7f0a0020

.field public static final settings_buttons_padding_top:I = 0x7f0a001f

.field public static final settings_fragment_padding_left:I = 0x7f0a001a

.field public static final settings_fragment_padding_right:I = 0x7f0a001b

.field public static final settings_fragment_padding_top:I = 0x7f0a0019

.field public static final setup_buttons_padding_bottom:I = 0x7f0a0039

.field public static final setup_buttons_padding_left:I = 0x7f0a0035

.field public static final setup_buttons_padding_right:I = 0x7f0a0036

.field public static final setup_buttons_padding_top:I = 0x7f0a0037

.field public static final setup_buttons_vertical_spacing:I = 0x7f0a0038

.field public static final setup_credentials_input_width:I = 0x7f0a0024

.field public static final setup_fragment_padding_left:I = 0x7f0a001d

.field public static final setup_fragment_padding_right:I = 0x7f0a001e

.field public static final setup_fragment_padding_top:I = 0x7f0a001c

.field public static final setup_item_inset_left:I = 0x7f0a0021

.field public static final setup_item_inset_right:I = 0x7f0a0022

.field public static final setup_padding_left:I = 0x7f0a0001

.field public static final setup_padding_right:I = 0x7f0a0002

.field public static final setup_padding_top:I = 0x7f0a0000

.field public static final spinner_frame_width:I = 0x7f0a0044

.field public static final star_margin_top:I = 0x7f0a002b

.field public static final star_right_margin_wide:I = 0x7f0a002f

.field public static final subject_font_size:I = 0x7f0a0040

.field public static final wide_senders_font_size:I = 0x7f0a003d

.field public static final wide_subject_font_size:I = 0x7f0a003e

.field public static final widget_date_font_size:I = 0x7f0a0013

.field public static final widget_senders_font_size:I = 0x7f0a0011

.field public static final widget_subject_font_size:I = 0x7f0a0012


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
