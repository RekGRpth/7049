.class public final Lcom/android/email/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final account_delete_dlg_instructions_fmt:I = 0x7f080188

.field public static final account_delete_dlg_title:I = 0x7f080187

.field public static final account_duplicate_dlg_message_fmt:I = 0x7f0800ec

.field public static final account_duplicate_dlg_title:I = 0x7f0800eb

.field public static final account_folder_list_summary_drafts:I = 0x7f080083

.field public static final account_folder_list_summary_inbox:I = 0x7f080081

.field public static final account_folder_list_summary_outbox:I = 0x7f080084

.field public static final account_folder_list_summary_starred:I = 0x7f080082

.field public static final account_name_display_all:I = 0x7f080068

.field public static final account_password_spaces_error:I = 0x7f0800ed

.field public static final account_security_dialog_content_fmt:I = 0x7f080152

.field public static final account_security_dialog_title:I = 0x7f080151

.field public static final account_security_policy_explanation_fmt:I = 0x7f080156

.field public static final account_security_title:I = 0x7f080155

.field public static final account_settings_action:I = 0x7f080054

.field public static final account_settings_background_attachments_label:I = 0x7f080165

.field public static final account_settings_background_attachments_summary:I = 0x7f080166

.field public static final account_settings_category_delete_account:I = 0x7f080189

.field public static final account_settings_data_usage:I = 0x7f080176

.field public static final account_settings_default_label:I = 0x7f080163

.field public static final account_settings_default_summary:I = 0x7f080164

.field public static final account_settings_delete_account_label:I = 0x7f08018a

.field public static final account_settings_description_label:I = 0x7f08016f

.field public static final account_settings_edit_quick_responses_label:I = 0x7f080172

.field public static final account_settings_edit_quick_responses_summary:I = 0x7f080173

.field public static final account_settings_exit_server_settings:I = 0x7f080160

.field public static final account_settings_incoming_label:I = 0x7f08016b

.field public static final account_settings_incoming_summary:I = 0x7f08016c

.field public static final account_settings_login_dialog_content_fmt:I = 0x7f080162

.field public static final account_settings_login_dialog_title:I = 0x7f080161

.field public static final account_settings_mail_check_frequency_label:I = 0x7f08016a

.field public static final account_settings_name_label:I = 0x7f080170

.field public static final account_settings_notifications:I = 0x7f080175

.field public static final account_settings_notify_label:I = 0x7f080167

.field public static final account_settings_notify_summary:I = 0x7f080169

.field public static final account_settings_outgoing_label:I = 0x7f08016d

.field public static final account_settings_outgoing_summary:I = 0x7f08016e

.field public static final account_settings_ringtone:I = 0x7f080185

.field public static final account_settings_servers:I = 0x7f080186

.field public static final account_settings_signature_hint:I = 0x7f080174

.field public static final account_settings_signature_label:I = 0x7f080171

.field public static final account_settings_summary:I = 0x7f080168

.field public static final account_settings_sync_calendar_enable:I = 0x7f08017b

.field public static final account_settings_sync_calendar_summary:I = 0x7f08017c

.field public static final account_settings_sync_contacts_enable:I = 0x7f080179

.field public static final account_settings_sync_contacts_summary:I = 0x7f08017a

.field public static final account_settings_sync_email_enable:I = 0x7f08017d

.field public static final account_settings_sync_email_summary:I = 0x7f08017e

.field public static final account_settings_vibrate_when_always:I = 0x7f080181

.field public static final account_settings_vibrate_when_dlg_title:I = 0x7f080184

.field public static final account_settings_vibrate_when_label:I = 0x7f08017f

.field public static final account_settings_vibrate_when_never:I = 0x7f080183

.field public static final account_settings_vibrate_when_silent:I = 0x7f080182

.field public static final account_settings_vibrate_when_summary:I = 0x7f080180

.field public static final account_setup_account_type_exchange_action:I = 0x7f0800fb

.field public static final account_setup_account_type_exchange_action_alternate:I = 0x7f0800fc

.field public static final account_setup_account_type_headline:I = 0x7f0800f7

.field public static final account_setup_account_type_imap_action:I = 0x7f0800fa

.field public static final account_setup_account_type_instructions:I = 0x7f0800f8

.field public static final account_setup_account_type_pop_action:I = 0x7f0800f9

.field public static final account_setup_account_type_title:I = 0x7f0800f6

.field public static final account_setup_basics_default_label:I = 0x7f0800e8

.field public static final account_setup_basics_email_label:I = 0x7f0800e6

.field public static final account_setup_basics_exchange_title:I = 0x7f0800e0

.field public static final account_setup_basics_exchange_title_alternate:I = 0x7f0800e1

.field public static final account_setup_basics_headline:I = 0x7f0800e2

.field public static final account_setup_basics_manual_setup_action:I = 0x7f0800e9

.field public static final account_setup_basics_password_label:I = 0x7f0800e7

.field public static final account_setup_basics_title:I = 0x7f0800df

.field public static final account_setup_check_settings_check_incoming_msg:I = 0x7f0800ef

.field public static final account_setup_check_settings_check_outgoing_msg:I = 0x7f0800f0

.field public static final account_setup_check_settings_retr_info_msg:I = 0x7f0800ee

.field public static final account_setup_exchange_certificate_title:I = 0x7f08011d

.field public static final account_setup_exchange_device_id_label:I = 0x7f080122

.field public static final account_setup_exchange_headline:I = 0x7f080118

.field public static final account_setup_exchange_no_certificate:I = 0x7f080121

.field public static final account_setup_exchange_remove_certificate:I = 0x7f080120

.field public static final account_setup_exchange_select_certificate:I = 0x7f08011e

.field public static final account_setup_exchange_server_label:I = 0x7f080119

.field public static final account_setup_exchange_ssl_label:I = 0x7f08011b

.field public static final account_setup_exchange_title:I = 0x7f080117

.field public static final account_setup_exchange_trust_certificates_label:I = 0x7f08011c

.field public static final account_setup_exchange_use_certificate:I = 0x7f08011f

.field public static final account_setup_exchange_username_label:I = 0x7f08011a

.field public static final account_setup_failed_access_denied:I = 0x7f08014b

.field public static final account_setup_failed_auth_required:I = 0x7f080147

.field public static final account_setup_failed_certificate_inaccessible:I = 0x7f080142

.field public static final account_setup_failed_certificate_required:I = 0x7f080141

.field public static final account_setup_failed_check_credentials_message:I = 0x7f080143

.field public static final account_setup_failed_dlg_auth_message:I = 0x7f08013d

.field public static final account_setup_failed_dlg_auth_message_fmt:I = 0x7f08013e

.field public static final account_setup_failed_dlg_certificate_message:I = 0x7f08013f

.field public static final account_setup_failed_dlg_certificate_message_fmt:I = 0x7f080140

.field public static final account_setup_failed_dlg_edit_details_action:I = 0x7f080157

.field public static final account_setup_failed_dlg_server_message:I = 0x7f080144

.field public static final account_setup_failed_dlg_server_message_fmt:I = 0x7f080145

.field public static final account_setup_failed_dlg_title:I = 0x7f080133

.field public static final account_setup_failed_ioerror:I = 0x7f080149

.field public static final account_setup_failed_protocol_unsupported:I = 0x7f08014a

.field public static final account_setup_failed_security:I = 0x7f080148

.field public static final account_setup_failed_security_policies_unsupported:I = 0x7f08014e

.field public static final account_setup_failed_tls_required:I = 0x7f080146

.field public static final account_setup_incoming_delete_policy_delete_label:I = 0x7f08010c

.field public static final account_setup_incoming_delete_policy_label:I = 0x7f08010a

.field public static final account_setup_incoming_delete_policy_never_label:I = 0x7f08010b

.field public static final account_setup_incoming_headline:I = 0x7f0800fe

.field public static final account_setup_incoming_imap_path_prefix_hint:I = 0x7f08010e

.field public static final account_setup_incoming_imap_path_prefix_label:I = 0x7f08010d

.field public static final account_setup_incoming_imap_server_label:I = 0x7f080102

.field public static final account_setup_incoming_password_label:I = 0x7f080100

.field public static final account_setup_incoming_pop_server_label:I = 0x7f080101

.field public static final account_setup_incoming_port_label:I = 0x7f080103

.field public static final account_setup_incoming_security_label:I = 0x7f080104

.field public static final account_setup_incoming_security_none_label:I = 0x7f080105

.field public static final account_setup_incoming_security_ssl_label:I = 0x7f080107

.field public static final account_setup_incoming_security_ssl_trust_certificates_label:I = 0x7f080106

.field public static final account_setup_incoming_security_tls_label:I = 0x7f080109

.field public static final account_setup_incoming_security_tls_trust_certificates_label:I = 0x7f080108

.field public static final account_setup_incoming_title:I = 0x7f0800fd

.field public static final account_setup_incoming_username_label:I = 0x7f0800ff

.field public static final account_setup_names_account_name_label:I = 0x7f0800f3

.field public static final account_setup_names_headline:I = 0x7f0800f2

.field public static final account_setup_names_title:I = 0x7f0800f1

.field public static final account_setup_names_user_name_empty_error:I = 0x7f0800f5

.field public static final account_setup_names_user_name_label:I = 0x7f0800f4

.field public static final account_setup_options_background_attachments_label:I = 0x7f080132

.field public static final account_setup_options_default_label:I = 0x7f08012d

.field public static final account_setup_options_headline:I = 0x7f080124

.field public static final account_setup_options_mail_check_frequency_10min:I = 0x7f080129

.field public static final account_setup_options_mail_check_frequency_15min:I = 0x7f08012a

.field public static final account_setup_options_mail_check_frequency_1hour:I = 0x7f08012c

.field public static final account_setup_options_mail_check_frequency_30min:I = 0x7f08012b

.field public static final account_setup_options_mail_check_frequency_5min:I = 0x7f080128

.field public static final account_setup_options_mail_check_frequency_label:I = 0x7f080125

.field public static final account_setup_options_mail_check_frequency_never:I = 0x7f080126

.field public static final account_setup_options_mail_check_frequency_push:I = 0x7f080127

.field public static final account_setup_options_mail_window_1day:I = 0x7f080136

.field public static final account_setup_options_mail_window_1month:I = 0x7f08013a

.field public static final account_setup_options_mail_window_1week:I = 0x7f080138

.field public static final account_setup_options_mail_window_2weeks:I = 0x7f080139

.field public static final account_setup_options_mail_window_3days:I = 0x7f080137

.field public static final account_setup_options_mail_window_all:I = 0x7f08013b

.field public static final account_setup_options_mail_window_auto:I = 0x7f080135

.field public static final account_setup_options_mail_window_default:I = 0x7f08013c

.field public static final account_setup_options_mail_window_label:I = 0x7f080134

.field public static final account_setup_options_notify_label:I = 0x7f08012e

.field public static final account_setup_options_sync_calendar_label:I = 0x7f080130

.field public static final account_setup_options_sync_contacts_label:I = 0x7f08012f

.field public static final account_setup_options_sync_email_label:I = 0x7f080131

.field public static final account_setup_options_title:I = 0x7f080123

.field public static final account_setup_outgoing_headline:I = 0x7f080110

.field public static final account_setup_outgoing_password_label:I = 0x7f080116

.field public static final account_setup_outgoing_port_label:I = 0x7f080112

.field public static final account_setup_outgoing_require_login_label:I = 0x7f080114

.field public static final account_setup_outgoing_security_label:I = 0x7f080113

.field public static final account_setup_outgoing_smtp_server_label:I = 0x7f080111

.field public static final account_setup_outgoing_title:I = 0x7f08010f

.field public static final account_setup_outgoing_username_label:I = 0x7f080115

.field public static final account_setup_security_policies_required_fmt:I = 0x7f08014d

.field public static final account_setup_security_required_title:I = 0x7f08014c

.field public static final account_setup_username_password_toast:I = 0x7f0800ea

.field public static final account_setup_username_uneditable_error:I = 0x7f08014f

.field public static final account_shortcut_picker_name:I = 0x7f080190

.field public static final account_shortcut_picker_title:I = 0x7f080191

.field public static final accounts_welcome:I = 0x7f0800e3

.field public static final accounts_welcome_exchange:I = 0x7f0800e4

.field public static final accounts_welcome_exchange_alternate:I = 0x7f0800e5

.field public static final action_bar_mailbox_list_title:I = 0x7f0801c3

.field public static final add_account_action:I = 0x7f080051

.field public static final add_cc_bcc_menu:I = 0x7f08005a

.field public static final add_file_attachment:I = 0x7f08005b

.field public static final always_bcc_myself:I = 0x7f080025

.field public static final always_bcc_myself_summary:I = 0x7f080026

.field public static final app_name:I = 0x7f080038

.field public static final attach_calender:I = 0x7f080009

.field public static final attach_contact:I = 0x7f080007

.field public static final attach_error_occurred:I = 0x7f08000a

.field public static final attach_file:I = 0x7f080008

.field public static final attach_image:I = 0x7f080004

.field public static final attach_sound:I = 0x7f080006

.field public static final attach_video:I = 0x7f080005

.field public static final attachment_file_rename_label:I = 0x7f080033

.field public static final attachment_info_apk_install_disabled:I = 0x7f0800ce

.field public static final attachment_info_application_settings:I = 0x7f0800c7

.field public static final attachment_info_deleted:I = 0x7f080020

.field public static final attachment_info_dialog_default_title:I = 0x7f0800c4

.field public static final attachment_info_dialog_wifi_title:I = 0x7f0800c5

.field public static final attachment_info_malware:I = 0x7f0800c9

.field public static final attachment_info_no_intent:I = 0x7f0800cc

.field public static final attachment_info_policy:I = 0x7f0800ca

.field public static final attachment_info_sideload_disabled:I = 0x7f0800cd

.field public static final attachment_info_unknown:I = 0x7f0800c8

.field public static final attachment_info_wifi_only:I = 0x7f0800cb

.field public static final attachment_info_wifi_settings:I = 0x7f0800c6

.field public static final attachment_not_found:I = 0x7f0800cf

.field public static final attachment_overflow:I = 0x7f080017

.field public static final bcc:I = 0x7f080096

.field public static final body_hint:I = 0x7f080098

.field public static final build_number:I = 0x7f080000

.field public static final cancel_action:I = 0x7f08003d

.field public static final cannot_add_this_attachment:I = 0x7f080018

.field public static final cannot_move_multiple_accounts_toast:I = 0x7f080064

.field public static final cannot_move_protocol_not_supported_toast:I = 0x7f080063

.field public static final cannot_move_special_mailboxes_toast:I = 0x7f080065

.field public static final category_general_preferences:I = 0x7f08019f

.field public static final cc:I = 0x7f080095

.field public static final choose_attachment_dialog_title:I = 0x7f08005e

.field public static final choosertitle_sharevia:I = 0x7f0801c8

.field public static final close_action:I = 0x7f08005c

.field public static final compose_action:I = 0x7f080052

.field public static final compose_title:I = 0x7f080039

.field public static final connection_settings:I = 0x7f080014

.field public static final contextmenu_add_contact:I = 0x7f0801ce

.field public static final contextmenu_copy:I = 0x7f0801ca

.field public static final contextmenu_copylink:I = 0x7f0801cb

.field public static final contextmenu_dial_dot:I = 0x7f0801cc

.field public static final contextmenu_feedback:I = 0x7f0801d3

.field public static final contextmenu_help:I = 0x7f0801d2

.field public static final contextmenu_map:I = 0x7f0801d0

.field public static final contextmenu_openlink:I = 0x7f0801c9

.field public static final contextmenu_send_mail:I = 0x7f0801cf

.field public static final contextmenu_sharelink:I = 0x7f0801d1

.field public static final contextmenu_sms_dot:I = 0x7f0801cd

.field public static final copy_email:I = 0x7f0801d5

.field public static final copy_number:I = 0x7f0801d6

.field public static final create_action:I = 0x7f080046

.field public static final debug_clear_webview_cache:I = 0x7f08007d

.field public static final debug_disable_graphics_acceleration_label:I = 0x7f08007e

.field public static final debug_enable_debug_logging_label:I = 0x7f08007a

.field public static final debug_enable_exchange_file_logging_label:I = 0x7f08007c

.field public static final debug_enable_exchange_logging_label:I = 0x7f08007b

.field public static final debug_enable_strict_mode_label:I = 0x7f080080

.field public static final debug_force_one_minute_refresh_label:I = 0x7f08007f

.field public static final debug_title:I = 0x7f08003a

.field public static final debug_version_fmt:I = 0x7f080079

.field public static final delete_action:I = 0x7f080042

.field public static final delete_message_confirm:I = 0x7f08001f

.field public static final delete_message_dialog_title:I = 0x7f08001e

.field public static final delete_quick_response_action:I = 0x7f080047

.field public static final device_admin_description:I = 0x7f08019b

.field public static final device_admin_label:I = 0x7f08019a

.field public static final disable_admin_warning:I = 0x7f080150

.field public static final discard_action:I = 0x7f080049

.field public static final done:I = 0x7f0801d7

.field public static final done_action:I = 0x7f080045

.field public static final download_remaining:I = 0x7f080010

.field public static final downloading_remaining:I = 0x7f080011

.field public static final edit_compose_quoted_text_label:I = 0x7f080027

.field public static final edit_quick_response_dialog:I = 0x7f080177

.field public static final eml_view_title:I = 0x7f0800d1

.field public static final error_loading_message_body:I = 0x7f080062

.field public static final exchange_name:I = 0x7f080197

.field public static final exchange_name_alternate:I = 0x7f080198

.field public static final favorite_action:I = 0x7f080044

.field public static final forward_action:I = 0x7f080043

.field public static final forward_download_failed_ticker:I = 0x7f0800d8

.field public static final forward_download_failed_title:I = 0x7f0800d9

.field public static final gal_searching_fmt:I = 0x7f08019c

.field public static final general_preference_auto_advance_dialog_title:I = 0x7f0801a2

.field public static final general_preference_auto_advance_label:I = 0x7f0801a0

.field public static final general_preference_auto_advance_message_list:I = 0x7f0801a5

.field public static final general_preference_auto_advance_newer:I = 0x7f0801a3

.field public static final general_preference_auto_advance_older:I = 0x7f0801a4

.field public static final general_preference_auto_advance_summary:I = 0x7f0801a1

.field public static final general_preference_auto_download_remaining_label:I = 0x7f080023

.field public static final general_preference_auto_download_remaining_summary:I = 0x7f080024

.field public static final general_preference_reply_all_label:I = 0x7f0801ad

.field public static final general_preference_reply_all_summary:I = 0x7f0801ae

.field public static final general_preference_text_zoom_dialog_title:I = 0x7f0801a7

.field public static final general_preference_text_zoom_huge:I = 0x7f0801ac

.field public static final general_preference_text_zoom_label:I = 0x7f0801a6

.field public static final general_preference_text_zoom_large:I = 0x7f0801ab

.field public static final general_preference_text_zoom_normal:I = 0x7f0801aa

.field public static final general_preference_text_zoom_small:I = 0x7f0801a9

.field public static final general_preference_text_zoom_tiny:I = 0x7f0801a8

.field public static final general_preferences_clear_trusted_senders_summary:I = 0x7f0801b0

.field public static final general_preferences_clear_trusted_senders_title:I = 0x7f0801af

.field public static final header_label_general_preferences:I = 0x7f08019e

.field public static final invalid_char_prompt:I = 0x7f080032

.field public static final loading_attachment:I = 0x7f080019

.field public static final login_failed_ticker:I = 0x7f0800da

.field public static final login_failed_title:I = 0x7f0800db

.field public static final long_string:I = 0x7f0801c4

.field public static final low_storage_attachment_cannot_download:I = 0x7f08000e

.field public static final low_storage_attachment_cannot_view:I = 0x7f08000f

.field public static final low_storage_hint_delete_mail:I = 0x7f08000c

.field public static final low_storage_service_start:I = 0x7f08000d

.field public static final low_storage_service_stop:I = 0x7f08000b

.field public static final mailbox_list_account_selector_account_header:I = 0x7f080087

.field public static final mailbox_list_account_selector_combined_view:I = 0x7f080085

.field public static final mailbox_list_account_selector_mailbox_header_fmt:I = 0x7f080088

.field public static final mailbox_list_account_selector_show_all_folders:I = 0x7f080086

.field public static final mailbox_list_recent_mailboxes:I = 0x7f08008a

.field public static final mailbox_list_user_mailboxes:I = 0x7f080089

.field public static final mailbox_name_display_drafts:I = 0x7f080071

.field public static final mailbox_name_display_inbox:I = 0x7f08006f

.field public static final mailbox_name_display_junk:I = 0x7f080074

.field public static final mailbox_name_display_outbox:I = 0x7f080070

.field public static final mailbox_name_display_sent:I = 0x7f080073

.field public static final mailbox_name_display_trash:I = 0x7f080072

.field public static final mailbox_name_display_unread:I = 0x7f080075

.field public static final mailbox_name_server_drafts:I = 0x7f08006b

.field public static final mailbox_name_server_inbox:I = 0x7f080069

.field public static final mailbox_name_server_junk:I = 0x7f08006e

.field public static final mailbox_name_server_outbox:I = 0x7f08006a

.field public static final mailbox_name_server_sent:I = 0x7f08006d

.field public static final mailbox_name_server_trash:I = 0x7f08006c

.field public static final mailbox_preferences_header:I = 0x7f08018d

.field public static final mailbox_settings_action:I = 0x7f080056

.field public static final mailbox_settings_activity_title:I = 0x7f08018b

.field public static final mailbox_settings_activity_title_with_mailbox:I = 0x7f08018c

.field public static final mailbox_settings_mailbox_check_frequency_label:I = 0x7f08018e

.field public static final mailbox_settings_mailbox_sync_window_label:I = 0x7f08018f

.field public static final mailbox_shortcut_picker_title:I = 0x7f080192

.field public static final mark_as_unread_action:I = 0x7f080057

.field public static final message_compose_attachment_size:I = 0x7f08009f

.field public static final message_compose_bcc_hint:I = 0x7f080091

.field public static final message_compose_cc_hint:I = 0x7f080090

.field public static final message_compose_display_name:I = 0x7f0800a1

.field public static final message_compose_error_invalid_email:I = 0x7f08009e

.field public static final message_compose_error_no_recipients:I = 0x7f08009d

.field public static final message_compose_foward_profix:I = 0x7f080002

.field public static final message_compose_from_label:I = 0x7f080093

.field public static final message_compose_fwd_header_fmt:I = 0x7f080099

.field public static final message_compose_include_quoted_text_checkbox_label:I = 0x7f08009c

.field public static final message_compose_insert_quick_response_list_title:I = 0x7f0800a0

.field public static final message_compose_quoted_text_label:I = 0x7f08009b

.field public static final message_compose_reply_header_fmt:I = 0x7f08009a

.field public static final message_compose_reply_profix:I = 0x7f080003

.field public static final message_compose_subject_hint:I = 0x7f080092

.field public static final message_compose_to_hint:I = 0x7f08008f

.field public static final message_decode_error:I = 0x7f0800d0

.field public static final message_delete_dialog_title:I = 0x7f0800d2

.field public static final message_discarded_toast:I = 0x7f0800d3

.field public static final message_is_empty_description:I = 0x7f08008c

.field public static final message_list_load_more_messages_action:I = 0x7f08008d

.field public static final message_list_no_messages:I = 0x7f08008e

.field public static final message_list_send_pending_messages_action:I = 0x7f08005d

.field public static final message_list_subject_snippet_divider:I = 0x7f0800de

.field public static final message_saved_toast:I = 0x7f0800d4

.field public static final message_subject_description:I = 0x7f08008b

.field public static final message_view_always_show_pictures_button:I = 0x7f0800b6

.field public static final message_view_always_show_pictures_confirmation:I = 0x7f0800b8

.field public static final message_view_always_show_pictures_prompt:I = 0x7f0800b7

.field public static final message_view_attachment_background_load:I = 0x7f0800b2

.field public static final message_view_attachment_cancel_action:I = 0x7f0800af

.field public static final message_view_attachment_info_action:I = 0x7f0800ac

.field public static final message_view_attachment_install_action:I = 0x7f0800a9

.field public static final message_view_attachment_load_action:I = 0x7f0800ab

.field public static final message_view_attachment_play_action:I = 0x7f0800aa

.field public static final message_view_attachment_save_action:I = 0x7f0800ad

.field public static final message_view_attachment_saved:I = 0x7f0800ae

.field public static final message_view_attachment_view_action:I = 0x7f0800a8

.field public static final message_view_bcc_label:I = 0x7f0800a4

.field public static final message_view_cc_label:I = 0x7f0800a3

.field public static final message_view_date_label:I = 0x7f0800a5

.field public static final message_view_display_attachment_toast:I = 0x7f0800d5

.field public static final message_view_from_label:I = 0x7f0800a6

.field public static final message_view_invite_accept:I = 0x7f0800bc

.field public static final message_view_invite_decline:I = 0x7f0800be

.field public static final message_view_invite_maybe:I = 0x7f0800bd

.field public static final message_view_invite_text:I = 0x7f0800bb

.field public static final message_view_invite_title:I = 0x7f0800ba

.field public static final message_view_invite_toast_maybe:I = 0x7f0800c0

.field public static final message_view_invite_toast_no:I = 0x7f0800c1

.field public static final message_view_invite_toast_yes:I = 0x7f0800bf

.field public static final message_view_invite_view:I = 0x7f0800b9

.field public static final message_view_load_attachment_failed_toast:I = 0x7f0800d6

.field public static final message_view_message_details_dialog_title:I = 0x7f0800c3

.field public static final message_view_move_to_newer:I = 0x7f0800dc

.field public static final message_view_move_to_older:I = 0x7f0800dd

.field public static final message_view_parse_message_toast:I = 0x7f0800d7

.field public static final message_view_show_details:I = 0x7f0800c2

.field public static final message_view_show_invite_action:I = 0x7f0800b4

.field public static final message_view_show_message_action:I = 0x7f0800b3

.field public static final message_view_show_pictures_action:I = 0x7f0800b5

.field public static final message_view_status_attachment_not_saved:I = 0x7f0800b1

.field public static final message_view_status_attachment_saved:I = 0x7f0800b0

.field public static final message_view_subject_label:I = 0x7f0800a7

.field public static final message_view_to_label:I = 0x7f0800a2

.field public static final more_string:I = 0x7f0801d4

.field public static final more_than_999:I = 0x7f0801bd

.field public static final move_action:I = 0x7f080058

.field public static final move_to_folder_dialog_title:I = 0x7f08005f

.field public static final multiple_new_message_notification_item:I = 0x7f0801c6

.field public static final need_connection_prompt:I = 0x7f080013

.field public static final next_action:I = 0x7f08003b

.field public static final not_add_more_attachments:I = 0x7f080016

.field public static final not_add_more_text:I = 0x7f080001

.field public static final notification_multiple_new_messages_fmt:I = 0x7f080067

.field public static final notification_to_account:I = 0x7f080066

.field public static final okay_action:I = 0x7f08003c

.field public static final open_message_parsing:I = 0x7f08001a

.field public static final open_message_update_db:I = 0x7f08001b

.field public static final open_message_update_ui:I = 0x7f08001c

.field public static final password_expire_warning_content_title:I = 0x7f080159

.field public static final password_expire_warning_dialog_content_fmt:I = 0x7f08015d

.field public static final password_expire_warning_dialog_title:I = 0x7f08015c

.field public static final password_expire_warning_ticker_fmt:I = 0x7f080158

.field public static final password_expired_content_title:I = 0x7f08015b

.field public static final password_expired_dialog_content_fmt:I = 0x7f08015f

.field public static final password_expired_dialog_title:I = 0x7f08015e

.field public static final password_expired_ticker:I = 0x7f08015a

.field public static final permission_access_provider_desc:I = 0x7f080037

.field public static final permission_access_provider_label:I = 0x7f080036

.field public static final permission_read_attachment_desc:I = 0x7f080035

.field public static final permission_read_attachment_label:I = 0x7f080034

.field public static final picker_combined_view_fmt:I = 0x7f080078

.field public static final picker_mailbox_name_all_inbox:I = 0x7f080077

.field public static final picker_mailbox_name_all_unread:I = 0x7f080076

.field public static final please_wait:I = 0x7f08001d

.field public static final plus_cc_label:I = 0x7f080059

.field public static final position_of_count:I = 0x7f0801b2

.field public static final previous_action:I = 0x7f08003e

.field public static final provider_note_live:I = 0x7f080195

.field public static final provider_note_t_online:I = 0x7f080196

.field public static final quick_responses_empty_view:I = 0x7f080048

.field public static final read_action:I = 0x7f08004c

.field public static final refresh_action:I = 0x7f080050

.field public static final remote_search_failed:I = 0x7f080028

.field public static final remove_star_action:I = 0x7f08004f

.field public static final rename:I = 0x7f080031

.field public static final reply_action:I = 0x7f080040

.field public static final reply_all_action:I = 0x7f080041

.field public static final require_manual_sync_message:I = 0x7f0801c5

.field public static final save_action:I = 0x7f080178

.field public static final save_draft_action:I = 0x7f08004a

.field public static final search_action:I = 0x7f080053

.field public static final search_field_all:I = 0x7f080030

.field public static final search_field_body:I = 0x7f08002f

.field public static final search_field_receiver:I = 0x7f08002d

.field public static final search_field_sender:I = 0x7f08002e

.field public static final search_field_subject:I = 0x7f08002c

.field public static final search_header_text_fmt:I = 0x7f0801c0

.field public static final search_hint:I = 0x7f0801be

.field public static final search_mailbox_hint:I = 0x7f0801bf

.field public static final search_slow_warning_message:I = 0x7f0801c2

.field public static final search_slow_warning_title:I = 0x7f0801c1

.field public static final searching_all_fields_on_sever:I = 0x7f08002b

.field public static final searching_on_server:I = 0x7f080029

.field public static final searching_on_server_title:I = 0x7f08002a

.field public static final security_notification_content_title:I = 0x7f080154

.field public static final security_notification_ticker_fmt:I = 0x7f080153

.field public static final send_action:I = 0x7f08003f

.field public static final send_failed_ticker:I = 0x7f080021

.field public static final send_failed_title:I = 0x7f080022

.field public static final set_star_action:I = 0x7f08004e

.field public static final settings_action:I = 0x7f080055

.field public static final settings_activity_title:I = 0x7f08019d

.field public static final show_quick_text_list_dialog_action:I = 0x7f08004b

.field public static final single_new_message_notification_big_text:I = 0x7f0801c7

.field public static final status_loading_messages:I = 0x7f080060

.field public static final status_network_error:I = 0x7f080061

.field public static final subject_hint:I = 0x7f080097

.field public static final system_account_create_failed:I = 0x7f080199

.field public static final to:I = 0x7f080094

.field public static final toast_account_not_found:I = 0x7f080193

.field public static final toast_mailbox_not_found:I = 0x7f080194

.field public static final too_many_attachments:I = 0x7f080015

.field public static final trusted_senders_cleared:I = 0x7f0801b1

.field public static final unable_to_connect:I = 0x7f080012

.field public static final unread_action:I = 0x7f08004d

.field public static final waitinf_for_sync_message_1:I = 0x7f0801b3

.field public static final waitinf_for_sync_message_2:I = 0x7f0801b4

.field public static final widget_all_mail:I = 0x7f0801b6

.field public static final widget_loading:I = 0x7f0801b9

.field public static final widget_no_accounts:I = 0x7f0801bb

.field public static final widget_no_mailboxes:I = 0x7f0801bc

.field public static final widget_other_views:I = 0x7f0801b5

.field public static final widget_starred:I = 0x7f0801b8

.field public static final widget_touch_to_configure:I = 0x7f0801ba

.field public static final widget_unread:I = 0x7f0801b7


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
