.class public final Lcom/android/email/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AccountSpinnerAnchorTextPrimary:I = 0x7f0c0042

.field public static final AccountSpinnerAnchorTextSecondary:I = 0x7f0c0043

.field public static final AccountSpinnerDropdownTextPrimary:I = 0x7f0c0040

.field public static final AccountSpinnerDropdownTextSecondary:I = 0x7f0c0041

.field public static final AccountSwitchSpinnerItem:I = 0x7f0c003f

.field public static final Attachment:I = 0x7f0c000e

.field public static final Attachment_Button:I = 0x7f0c000f

.field public static final ComposeBodyStyle:I = 0x7f0c0039

.field public static final ComposeButton:I = 0x7f0c003b

.field public static final ComposeButtonColumn:I = 0x7f0c003a

.field public static final ComposeEditTextView:I = 0x7f0c0035

.field public static final ComposeFieldLayout:I = 0x7f0c0027

.field public static final ComposeHeading:I = 0x7f0c0037

.field public static final EmailComposeEditTextView:I = 0x7f0c002a

.field public static final EmailRecipientEditTextView:I = 0x7f0c0028

.field public static final EmailTheme:I = 0x7f0c0001

.field public static final LargeComposeAreaBody:I = 0x7f0c0029

.field public static final NotificationPrimaryText:I = 0x7f0c0044

.field public static final NotificationSecondaryText:I = 0x7f0c0045

.field public static final PlainEditText:I = 0x7f0c0007

.field public static final QuotedTextHeaderStyle:I = 0x7f0c0022

.field public static final RecipientComposeFieldLayout:I = 0x7f0c0038

.field public static final RecipientComposeFieldSpacer:I = 0x7f0c0036

.field public static final RecipientComposeHeading:I = 0x7f0c0026

.field public static final RecipientEditTextView:I = 0x7f0c0047

.field public static final RecipientEditTextViewStyle:I = 0x7f0c0034

.field public static final SearchBar:I = 0x7f0c0000

.field public static final ThemeNoTitleBar:I = 0x7f0c0002

.field public static final accountSettingsButton:I = 0x7f0c0006

.field public static final accountSettingsHeadline:I = 0x7f0c0003

.field public static final accountSetupButton:I = 0x7f0c0005

.field public static final accountSetupHeadline:I = 0x7f0c0004

.field public static final accountSetupInfoText:I = 0x7f0c002b

.field public static final accountSetupLabelText:I = 0x7f0c002c

.field public static final accountSetupTypePrevious:I = 0x7f0c0032

.field public static final action_bar_account_name:I = 0x7f0c0018

.field public static final action_bar_account_name_secondary:I = 0x7f0c0019

.field public static final action_bar_custom_view:I = 0x7f0c0014

.field public static final action_bar_spinner_primary_text:I = 0x7f0c001a

.field public static final action_bar_spinner_secondary_text:I = 0x7f0c001b

.field public static final action_bar_unread_count:I = 0x7f0c0025

.field public static final attachment_container:I = 0x7f0c0010

.field public static final attachment_name:I = 0x7f0c0012

.field public static final attachment_size:I = 0x7f0c003c

.field public static final attachment_thumbnail:I = 0x7f0c0013

.field public static final message_compose_header_field_container:I = 0x7f0c002f

.field public static final message_compose_header_field_container_no_tray:I = 0x7f0c002e

.field public static final message_compose_header_field_label:I = 0x7f0c0030

.field public static final message_compose_header_field_value:I = 0x7f0c0031

.field public static final message_compose_horizontal_divider:I = 0x7f0c002d

.field public static final message_compose_scroll:I = 0x7f0c0033

.field public static final message_details_label:I = 0x7f0c000c

.field public static final message_details_value:I = 0x7f0c000a

.field public static final message_details_value_selectable:I = 0x7f0c000b

.field public static final message_header_sender_address:I = 0x7f0c0021

.field public static final message_header_sender_name:I = 0x7f0c0020

.field public static final message_list_item_normal:I = 0x7f0c001d

.field public static final message_list_item_wide:I = 0x7f0c001e

.field public static final message_view_action_buttons:I = 0x7f0c001f

.field public static final message_view_horizontal_divider:I = 0x7f0c0016

.field public static final message_view_subject:I = 0x7f0c000d

.field public static final message_view_tab:I = 0x7f0c0017

.field public static final message_view_text:I = 0x7f0c0015

.field public static final mtk_attachment_container:I = 0x7f0c0011

.field public static final notification_secondary_text:I = 0x7f0c0008

.field public static final remote_search_title:I = 0x7f0c0024

.field public static final search_header:I = 0x7f0c001c

.field public static final unreadCount:I = 0x7f0c003d

.field public static final unreadCountActionBar:I = 0x7f0c003e

.field public static final unreadCountActionBarTablet:I = 0x7f0c0046

.field public static final unreadCountSearch:I = 0x7f0c0023

.field public static final widget_list_item:I = 0x7f0c0009


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
