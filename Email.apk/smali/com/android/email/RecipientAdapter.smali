.class public Lcom/android/email/RecipientAdapter;
.super Lcom/android/ex/chips/BaseRecipientAdapter;
.source "RecipientAdapter.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/ex/chips/RecipientEditTextView;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/ex/chips/RecipientEditTextView;

    invoke-direct {p0, p1}, Lcom/android/ex/chips/BaseRecipientAdapter;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected getDefaultPhotoResource()I
    .locals 1

    const v0, 0x7f020023

    return v0
.end method

.method protected getItemLayout()I
    .locals 1

    const v0, 0x7f040022

    return v0
.end method

.method public setAccount(Landroid/accounts/Account;)V
    .locals 3
    .param p1    # Landroid/accounts/Account;

    if-eqz p1, :cond_0

    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v2, "unknown"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, v0}, Lcom/android/ex/chips/BaseRecipientAdapter;->setAccount(Landroid/accounts/Account;)V

    :cond_0
    return-void
.end method
