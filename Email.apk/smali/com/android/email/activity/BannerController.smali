.class public Lcom/android/email/activity/BannerController;
.super Ljava/lang/Object;
.source "BannerController.java"


# static fields
.field private static final ANIMATION_DURATION:I = 0x64

.field private static final INTERPOLATOR:Landroid/animation/TimeInterpolator;

.field private static final PROP_SET_BANNER_Y:Ljava/lang/String; = "bannerYAnim"


# instance fields
.field private final mBannerHeight:I

.field private final mBannerView:Landroid/widget/TextView;

.field private mLastAnimator:Landroid/animation/Animator;

.field private mShown:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc00000

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/android/email/activity/BannerController;->INTERPOLATOR:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/TextView;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/email/activity/BannerController;->mBannerView:Landroid/widget/TextView;

    iput p3, p0, Lcom/android/email/activity/BannerController;->mBannerHeight:I

    iget v0, p0, Lcom/android/email/activity/BannerController;->mBannerHeight:I

    neg-int v0, v0

    invoke-virtual {p0, v0}, Lcom/android/email/activity/BannerController;->setBannerYAnim(I)V

    return-void
.end method

.method private getBannerY()I
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/BannerController;->mBannerView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    return v0
.end method

.method private slideBanner(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/android/email/activity/BannerController;->mLastAnimator:Landroid/animation/Animator;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/email/activity/BannerController;->mLastAnimator:Landroid/animation/Animator;

    invoke-virtual {v2}, Landroid/animation/Animator;->cancel()V

    :cond_0
    new-array v1, v6, [Landroid/animation/PropertyValuesHolder;

    const-string v2, "bannerYAnim"

    const/4 v3, 0x2

    new-array v3, v3, [I

    invoke-direct {p0}, Lcom/android/email/activity/BannerController;->getBannerY()I

    move-result v4

    aput v4, v3, v5

    aput p1, v3, v6

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v3, 0x64

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    sget-object v2, Lcom/android/email/activity/BannerController;->INTERPOLATOR:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iput-object v0, p0, Lcom/android/email/activity/BannerController;->mLastAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/email/activity/BannerController;->mShown:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/BannerController;->mShown:Z

    iget v0, p0, Lcom/android/email/activity/BannerController;->mBannerHeight:I

    neg-int v0, v0

    invoke-direct {p0, v0}, Lcom/android/email/activity/BannerController;->slideBanner(I)V

    goto :goto_0
.end method

.method public setBannerYAnim(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/email/activity/BannerController;->mBannerView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v0, p0, Lcom/android/email/activity/BannerController;->mBannerView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public show(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/android/email/activity/BannerController;->mShown:Z

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iput-boolean v1, p0, Lcom/android/email/activity/BannerController;->mShown:Z

    iget-object v2, p0, Lcom/android/email/activity/BannerController;->mBannerView:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v0}, Lcom/android/email/activity/BannerController;->slideBanner(I)V

    move v0, v1

    goto :goto_0
.end method
