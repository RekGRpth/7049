.class Lcom/android/email/activity/EmailActivity$ControllerResult;
.super Lcom/android/email/Controller$Result;
.source "EmailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/EmailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ControllerResult"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/email/activity/EmailActivity;


# direct methods
.method private constructor <init>(Lcom/android/email/activity/EmailActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/activity/EmailActivity$ControllerResult;->this$0:Lcom/android/email/activity/EmailActivity;

    invoke-direct {p0}, Lcom/android/email/Controller$Result;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/email/activity/EmailActivity;Lcom/android/email/activity/EmailActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/EmailActivity;
    .param p2    # Lcom/android/email/activity/EmailActivity$1;

    invoke-direct {p0, p1}, Lcom/android/email/activity/EmailActivity$ControllerResult;-><init>(Lcom/android/email/activity/EmailActivity;)V

    return-void
.end method

.method private handleError(Lcom/android/emailcommon/mail/MessagingException;JI)V
    .locals 4
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # I

    const-wide/16 v2, -0x1

    cmp-long v2, p2, v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    if-lez p4, :cond_0

    iget-object v2, p0, Lcom/android/email/activity/EmailActivity$ControllerResult;->this$0:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2}, Lcom/android/email/activity/EmailActivity;->access$100(Lcom/android/email/activity/EmailActivity;)J

    move-result-wide v2

    cmp-long v2, v2, p2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/email/activity/EmailActivity$ControllerResult;->this$0:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2}, Lcom/android/email/activity/EmailActivity;->access$200(Lcom/android/email/activity/EmailActivity;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/email/activity/EmailActivity$ControllerResult;->this$0:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2, p2, p3}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/email/activity/EmailActivity$ControllerResult;->this$0:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2, p1}, Lcom/android/email/MessagingExceptionStrings;->getErrorString(Landroid/content/Context;Lcom/android/emailcommon/mail/MessagingException;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/android/emailcommon/provider/Account;->mDisplayName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/android/emailcommon/provider/Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_3
    iget-object v2, p0, Lcom/android/email/activity/EmailActivity$ControllerResult;->this$0:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2}, Lcom/android/email/activity/EmailActivity;->access$300(Lcom/android/email/activity/EmailActivity;)Lcom/android/email/activity/BannerController;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/email/activity/BannerController;->show(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/email/activity/EmailActivity$ControllerResult;->this$0:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2, p2, p3}, Lcom/android/email/activity/EmailActivity;->access$102(Lcom/android/email/activity/EmailActivity;J)J

    goto :goto_0
.end method


# virtual methods
.method public loadAttachmentCallback(Lcom/android/emailcommon/mail/MessagingException;JJJI)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # J
    .param p6    # J
    .param p8    # I

    invoke-direct {p0, p1, p2, p3, p8}, Lcom/android/email/activity/EmailActivity$ControllerResult;->handleError(Lcom/android/emailcommon/mail/MessagingException;JI)V

    return-void
.end method

.method public loadMessageForViewCallback(Lcom/android/emailcommon/mail/MessagingException;JJI)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # J
    .param p6    # I

    invoke-direct {p0, p1, p2, p3, p6}, Lcom/android/email/activity/EmailActivity$ControllerResult;->handleError(Lcom/android/emailcommon/mail/MessagingException;JI)V

    return-void
.end method

.method public sendMailCallback(Lcom/android/emailcommon/mail/MessagingException;JJI)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # J
    .param p6    # I

    invoke-direct {p0, p1, p2, p3, p6}, Lcom/android/email/activity/EmailActivity$ControllerResult;->handleError(Lcom/android/emailcommon/mail/MessagingException;JI)V

    return-void
.end method

.method public serviceCheckMailCallback(Lcom/android/emailcommon/mail/MessagingException;JJIJ)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # J
    .param p6    # I
    .param p7    # J

    invoke-direct {p0, p1, p2, p3, p6}, Lcom/android/email/activity/EmailActivity$ControllerResult;->handleError(Lcom/android/emailcommon/mail/MessagingException;JI)V

    return-void
.end method

.method public updateMailboxCallback(Lcom/android/emailcommon/mail/MessagingException;JJIILjava/util/ArrayList;)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # J
    .param p6    # I
    .param p7    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/emailcommon/mail/MessagingException;",
            "JJII",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p6}, Lcom/android/email/activity/EmailActivity$ControllerResult;->handleError(Lcom/android/emailcommon/mail/MessagingException;JI)V

    return-void
.end method

.method public updateMailboxListCallback(Lcom/android/emailcommon/mail/MessagingException;JI)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/MessagingException;
    .param p2    # J
    .param p4    # I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/email/activity/EmailActivity$ControllerResult;->handleError(Lcom/android/emailcommon/mail/MessagingException;JI)V

    return-void
.end method
