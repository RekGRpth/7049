.class Lcom/android/email/activity/MailboxFragmentAdapter;
.super Landroid/widget/CursorAdapter;
.source "MailboxFragmentAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/MailboxFragmentAdapter$1;,
        Lcom/android/email/activity/MailboxFragmentAdapter$CombinedMailboxLoader;,
        Lcom/android/email/activity/MailboxFragmentAdapter$MailboxFragmentLoader;,
        Lcom/android/email/activity/MailboxFragmentAdapter$CursorWithExtras;,
        Lcom/android/email/activity/MailboxFragmentAdapter$EmptyCallback;,
        Lcom/android/email/activity/MailboxFragmentAdapter$Callback;
    }
.end annotation


# static fields
.field private static final ALL_MAILBOX_SELECTION:Ljava/lang/String; = "accountKey=? AND type<64 AND flagVisible=1"

.field private static final CURMAILBOX_PROJECTION:[Ljava/lang/String;

.field private static final ITEM_VIEW_TYPE_HEADER:I = -0x2

.field private static final ITEM_VIEW_TYPE_NORMAL:I = 0x0

.field private static final MAILBOX_ORDER_BY:Ljava/lang/String; = "CASE type WHEN 0 THEN 0 WHEN 3 THEN 1 WHEN 4 THEN 2 WHEN 5 THEN 3 WHEN 6 THEN 4 WHEN 7 THEN 5 ELSE 10 END ,displayName COLLATE LOCALIZED ASC"

.field private static final MAILBOX_SELECTION:Ljava/lang/String; = "accountKey=? AND _id=?"

.field private static final MATRIX_PROJECTION:[Ljava/lang/String;

.field private static final ORIGINAL_ID:Ljava/lang/String; = "orgMailboxId"

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final ROW_TYPE:Ljava/lang/String; = "rowType"

.field private static final ROW_TYPE_ACCOUNT:I = 0x1

.field private static final ROW_TYPE_CURMAILBOX:I = 0x2

.field private static final ROW_TYPE_HEADER:I = 0x4

.field private static final ROW_TYPE_MAILBOX:I = 0x0

.field private static final ROW_TYPE_SUBMAILBOX:I = 0x3

.field private static final SUBMAILBOX_PROJECTION:[Ljava/lang/String;

.field private static final SYSTEM_MAILBOX_SELECTION:Ljava/lang/String; = "accountKey=? AND type<64 AND flagVisible=1 AND type!=1"

.field private static final USER_MAILBOX_SELECTION_WITH_PARENT:Ljava/lang/String; = "accountKey=? AND type<64 AND flagVisible=1 AND parentKey=? AND type=1"

.field private static sEnableUpdate:Z


# instance fields
.field private final mCallback:Lcom/android/email/activity/MailboxFragmentAdapter$Callback;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mResourceHelper:Lcom/android/email/ResourceHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "_id AS orgMailboxId"

    aput-object v1, v0, v3

    const-string v1, "displayName"

    aput-object v1, v0, v5

    const-string v1, "type"

    aput-object v1, v0, v6

    const-string v1, "unreadCount"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "messageCount"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "0 AS rowType"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "flags"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/email/activity/MailboxFragmentAdapter;->PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "_id AS orgMailboxId"

    aput-object v1, v0, v3

    const-string v1, "displayName"

    aput-object v1, v0, v5

    const-string v1, "type"

    aput-object v1, v0, v6

    const-string v1, "unreadCount"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "messageCount"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "3 AS rowType"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "flags"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/email/activity/MailboxFragmentAdapter;->SUBMAILBOX_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "_id AS orgMailboxId"

    aput-object v1, v0, v3

    const-string v1, "displayName"

    aput-object v1, v0, v5

    const-string v1, "type"

    aput-object v1, v0, v6

    const-string v1, "unreadCount"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "messageCount"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "2 AS rowType"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "flags"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/email/activity/MailboxFragmentAdapter;->CURMAILBOX_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "orgMailboxId"

    aput-object v1, v0, v3

    const-string v1, "displayName"

    aput-object v1, v0, v5

    const-string v1, "type"

    aput-object v1, v0, v6

    const-string v1, "unreadCount"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "messageCount"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "rowType"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "flags"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/email/activity/MailboxFragmentAdapter;->MATRIX_PROJECTION:[Ljava/lang/String;

    sput-boolean v3, Lcom/android/email/activity/MailboxFragmentAdapter;->sEnableUpdate:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/email/activity/MailboxFragmentAdapter$Callback;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/email/activity/MailboxFragmentAdapter$Callback;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/email/activity/MailboxFragmentAdapter;->mInflater:Landroid/view/LayoutInflater;

    if-nez p2, :cond_0

    sget-object p2, Lcom/android/email/activity/MailboxFragmentAdapter$EmptyCallback;->INSTANCE:Lcom/android/email/activity/MailboxFragmentAdapter$Callback;

    :cond_0
    iput-object p2, p0, Lcom/android/email/activity/MailboxFragmentAdapter;->mCallback:Lcom/android/email/activity/MailboxFragmentAdapter$Callback;

    invoke-static {p1}, Lcom/android/email/ResourceHelper;->getInstance(Landroid/content/Context;)Lcom/android/email/ResourceHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MailboxFragmentAdapter;->mResourceHelper:Lcom/android/email/ResourceHelper;

    return-void
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/email/activity/MailboxFragmentAdapter;->SUBMAILBOX_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/email/activity/MailboxFragmentAdapter;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Z
    .locals 1

    sget-boolean v0, Lcom/android/email/activity/MailboxFragmentAdapter;->sEnableUpdate:Z

    return v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/email/activity/MailboxFragmentAdapter;->CURMAILBOX_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/email/activity/MailboxFragmentAdapter;->MATRIX_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Landroid/database/MatrixCursor;JLjava/lang/String;IIIIIJ)V
    .locals 0
    .param p0    # Landroid/database/MatrixCursor;
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # J

    invoke-static/range {p0 .. p10}, Lcom/android/email/activity/MailboxFragmentAdapter;->addMailboxRow(Landroid/database/MatrixCursor;JLjava/lang/String;IIIIIJ)V

    return-void
.end method

.method static synthetic access$700(Landroid/content/Context;Landroid/database/MatrixCursor;JJIZ)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/MatrixCursor;
    .param p2    # J
    .param p4    # J
    .param p6    # I
    .param p7    # Z

    invoke-static/range {p0 .. p7}, Lcom/android/email/activity/MailboxFragmentAdapter;->addAccountMailboxRow(Landroid/content/Context;Landroid/database/MatrixCursor;JJIZ)V

    return-void
.end method

.method static synthetic access$800(Landroid/content/Context;Landroid/database/MatrixCursor;JIZ)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/MatrixCursor;
    .param p2    # J
    .param p4    # I
    .param p5    # Z

    invoke-static/range {p0 .. p5}, Lcom/android/email/activity/MailboxFragmentAdapter;->addCombinedMailboxRow(Landroid/content/Context;Landroid/database/MatrixCursor;JIZ)V

    return-void
.end method

.method private static addAccountMailboxRow(Landroid/content/Context;Landroid/database/MatrixCursor;JJIZ)V
    .locals 15
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/MatrixCursor;
    .param p2    # J
    .param p4    # J
    .param p6    # I
    .param p7    # Z

    const-wide/16 v4, 0x0

    cmp-long v4, p4, v4

    if-ltz v4, :cond_0

    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    :cond_0
    const/4 v9, 0x0

    const-wide/16 v4, -0x1

    cmp-long v4, p2, v4

    if-eqz v4, :cond_3

    const-wide/high16 v4, 0x1000000000000000L

    cmp-long v4, p2, v4

    if-eqz v4, :cond_3

    move-wide/from16 v0, p2

    move-wide/from16 v2, p4

    invoke-static {p0, v0, v1, v2, v3}, Lcom/android/email/FolderProperties;->getMessageCountForCombinedMailbox(Landroid/content/Context;JJ)I

    move-result v9

    :goto_0
    if-nez p7, :cond_1

    if-lez v9, :cond_2

    :cond_1
    const-string v4, "Email"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addAccountMailboxRow mMailboxId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p4

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " combined count:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, ""

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-wide/high16 v13, 0x1000000000000000L

    move-object/from16 v4, p1

    move-wide/from16 v5, p4

    move/from16 v8, p6

    move v10, v9

    invoke-static/range {v4 .. v14}, Lcom/android/email/activity/MailboxFragmentAdapter;->addMailboxRow(Landroid/database/MatrixCursor;JLjava/lang/String;IIIIIJ)V

    :cond_2
    return-void

    :cond_3
    move-wide/from16 v0, p4

    invoke-static {p0, v0, v1}, Lcom/android/email/FolderProperties;->getMessageCountForCombinedMailbox(Landroid/content/Context;J)I

    move-result v9

    goto :goto_0
.end method

.method private static addCombinedMailboxRow(Landroid/content/Context;Landroid/database/MatrixCursor;JIZ)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/MatrixCursor;
    .param p2    # J
    .param p4    # I
    .param p5    # Z

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    invoke-static {p0, p2, p3}, Lcom/android/email/FolderProperties;->getMessageCountForCombinedMailbox(Landroid/content/Context;J)I

    move-result v5

    if-nez p5, :cond_1

    if-lez v5, :cond_2

    :cond_1
    const-string v3, ""

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/high16 v9, 0x1000000000000000L

    move-object v0, p1

    move-wide v1, p2

    move v4, p4

    move v6, v5

    invoke-static/range {v0 .. v10}, Lcom/android/email/activity/MailboxFragmentAdapter;->addMailboxRow(Landroid/database/MatrixCursor;JLjava/lang/String;IIIIIJ)V

    :cond_2
    return-void
.end method

.method private static addMailboxRow(Landroid/database/MatrixCursor;JLjava/lang/String;IIIIIJ)V
    .locals 5
    .param p0    # Landroid/database/MatrixCursor;
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # J

    move-wide v0, p1

    const-wide/16 v3, 0x0

    cmp-long v3, p1, v3

    if-gez v3, :cond_0

    const-wide v3, 0x7fffffffffffffffL

    add-long v0, v3, p1

    :cond_0
    invoke-virtual {p0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-virtual {v2, p3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-static {p9, p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    return-void
.end method

.method private bindListHeader(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const v1, 0x7f0f004d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p2, p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getDisplayName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private bindListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 22
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    invoke-static/range {p3 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->isAccountRow(Landroid/database/Cursor;)Z

    move-result v14

    invoke-static/range {p3 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getType(Landroid/database/Cursor;)I

    move-result v19

    invoke-static/range {p3 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getId(Landroid/database/Cursor;)J

    move-result-wide v12

    invoke-static/range {p3 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getAccountId(Landroid/database/Cursor;)J

    move-result-wide v3

    invoke-static/range {p3 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getFlags(Landroid/database/Cursor;)I

    move-result v8

    invoke-static/range {p3 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getRowType(Landroid/database/Cursor;)I

    move-result v18

    and-int/lit8 v20, v8, 0x1

    if-eqz v20, :cond_0

    and-int/lit8 v20, v8, 0x2

    if-eqz v20, :cond_0

    const/4 v11, 0x1

    :goto_0
    move-object/from16 v15, p1

    check-cast v15, Lcom/android/email/activity/MailboxListItem;

    invoke-static/range {p3 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->isAccountRow(Landroid/database/Cursor;)Z

    move-result v20

    if-eqz v20, :cond_1

    const-wide/16 v20, -0x1

    :goto_1
    move-wide/from16 v0, v20

    iput-wide v0, v15, Lcom/android/email/activity/MailboxListItem;->mMailboxId:J

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v15, Lcom/android/email/activity/MailboxListItem;->mMailboxType:Ljava/lang/Integer;

    iput-wide v3, v15, Lcom/android/email/activity/MailboxListItem;->mAccountId:J

    const-wide/16 v20, 0x0

    cmp-long v20, v12, v20

    if-ltz v20, :cond_2

    sget-object v20, Lcom/android/emailcommon/provider/Mailbox;->INVALID_DROP_TARGETS:[Ljava/lang/Integer;

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/android/emailcommon/utility/Utility;->arrayContains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_2

    and-int/lit8 v20, v8, 0x10

    if-eqz v20, :cond_2

    const/16 v20, 0x1

    :goto_2
    move/from16 v0, v20

    iput-boolean v0, v15, Lcom/android/email/activity/MailboxListItem;->mIsValidDropTarget:Z

    iput-boolean v11, v15, Lcom/android/email/activity/MailboxListItem;->mIsNavigable:Z

    move-object/from16 v0, p0

    iput-object v0, v15, Lcom/android/email/activity/MailboxListItem;->mAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/MailboxFragmentAdapter;->mCallback:Lcom/android/email/activity/MailboxFragmentAdapter$Callback;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v15}, Lcom/android/email/activity/MailboxFragmentAdapter$Callback;->onBind(Lcom/android/email/activity/MailboxListItem;)V

    const v20, 0x7f0f007c

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    invoke-static/range {p2 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getDisplayName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static/range {p3 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->isAccountRow(Landroid/database/Cursor;)Z

    move-result v20

    if-eqz v20, :cond_3

    invoke-static/range {p3 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getUnreadCount(Landroid/database/Cursor;)I

    move-result v6

    :goto_3
    const v20, 0x7f0f007a

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v20, 0x7f0f007b

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    invoke-static/range {p2 .. p2}, Lcom/android/email/FolderProperties;->getInstance(Landroid/content/Context;)Lcom/android/email/FolderProperties;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1, v12, v13, v8}, Lcom/android/email/FolderProperties;->getIcon(IJI)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v20, 0x7f0f007d

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    packed-switch v18, :pswitch_data_0

    if-eqz v11, :cond_5

    const/16 v20, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const v20, 0x7f020033

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_4
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_5
    if-lez v6, :cond_6

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_6
    const v20, 0x7f0f004f

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v14, :cond_7

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/MailboxFragmentAdapter;->mResourceHelper:Lcom/android/email/ResourceHelper;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Lcom/android/email/ResourceHelper;->getAccountColor(J)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :goto_7
    return-void

    :cond_0
    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_1
    move-wide/from16 v20, v12

    goto/16 :goto_1

    :cond_2
    const/16 v20, 0x0

    goto/16 :goto_2

    :cond_3
    invoke-static/range {p2 .. p2}, Lcom/android/email/FolderProperties;->getInstance(Landroid/content/Context;)Lcom/android/email/FolderProperties;

    move-result-object v10

    invoke-static/range {p3 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getUnreadCount(Landroid/database/Cursor;)I

    move-result v20

    invoke-static/range {p3 .. p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getMessageCount(Landroid/database/Cursor;)I

    move-result v21

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v10, v0, v1, v2}, Lcom/android/email/FolderProperties;->getMessageCount(III)I

    move-result v6

    goto/16 :goto_3

    :pswitch_0
    if-eqz v11, :cond_4

    const/16 v20, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const v20, 0x7f020033

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_8
    const/16 v20, 0x4

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    :cond_4
    const/16 v20, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v20, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_8

    :pswitch_1
    const/16 v20, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v20, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    :cond_5
    const/16 v20, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v20, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    :cond_6
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    :cond_7
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static createCombinedViewLoader(Landroid/content/Context;)Landroid/content/Loader;
    .locals 2
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    const-string v1, "MailboxFragmentAdapter#createCombinedViewLoader"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/email/activity/MailboxFragmentAdapter$CombinedMailboxLoader;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/email/activity/MailboxFragmentAdapter$CombinedMailboxLoader;-><init>(Landroid/content/Context;Lcom/android/email/activity/MailboxFragmentAdapter$1;)V

    return-object v0
.end method

.method static createMailboxesLoader(Landroid/content/Context;JJ)Landroid/content/Loader;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ)",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MailboxFragmentAdapter#CursorWithExtras accountId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " parentMailboxId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-wide/high16 v0, 0x1000000000000000L

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Lcom/android/email/activity/MailboxFragmentAdapter$MailboxFragmentLoader;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/email/activity/MailboxFragmentAdapter$MailboxFragmentLoader;-><init>(Landroid/content/Context;JJ)V

    return-object v0
.end method

.method static enableUpdates(Z)V
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/android/email/activity/MailboxFragmentAdapter;->sEnableUpdate:Z

    return-void
.end method

.method static getAccountId(Landroid/database/Cursor;)J
    .locals 2
    .param p0    # Landroid/database/Cursor;

    const-string v0, "accountKey"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method private static getDisplayName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/Cursor;

    const-string v2, "displayName"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/android/email/activity/MailboxFragmentAdapter;->isHeaderRow(Landroid/database/Cursor;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Lcom/android/email/activity/MailboxFragmentAdapter;->isAccountRow(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-static {p0}, Lcom/android/email/FolderProperties;->getInstance(Landroid/content/Context;)Lcom/android/email/FolderProperties;

    move-result-object v0

    invoke-static {p1}, Lcom/android/email/activity/MailboxFragmentAdapter;->getType(Landroid/database/Cursor;)I

    move-result v2

    invoke-static {p1}, Lcom/android/email/activity/MailboxFragmentAdapter;->getId(Landroid/database/Cursor;)J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/android/email/FolderProperties;->getDisplayName(IJLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getFlags(Landroid/database/Cursor;)I
    .locals 1
    .param p0    # Landroid/database/Cursor;

    const-string v0, "flags"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method static getId(Landroid/database/Cursor;)J
    .locals 2
    .param p0    # Landroid/database/Cursor;

    const-string v0, "orgMailboxId"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method static getMessageCount(Landroid/database/Cursor;)I
    .locals 1
    .param p0    # Landroid/database/Cursor;

    const-string v0, "messageCount"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method private static getRowType(Landroid/database/Cursor;)I
    .locals 1
    .param p0    # Landroid/database/Cursor;

    const-string v0, "rowType"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method static getType(Landroid/database/Cursor;)I
    .locals 1
    .param p0    # Landroid/database/Cursor;

    const-string v0, "type"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method static getUnreadCount(Landroid/database/Cursor;)I
    .locals 1
    .param p0    # Landroid/database/Cursor;

    const-string v0, "unreadCount"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method private static isAccountRow(Landroid/database/Cursor;)Z
    .locals 2
    .param p0    # Landroid/database/Cursor;

    const/4 v0, 0x1

    invoke-static {p0}, Lcom/android/email/activity/MailboxFragmentAdapter;->getRowType(Landroid/database/Cursor;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isHeader(I)Z
    .locals 4
    .param p1    # I

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    const-string v3, "rowType"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static isHeaderRow(Landroid/database/Cursor;)Z
    .locals 2
    .param p0    # Landroid/database/Cursor;

    invoke-static {p0}, Lcom/android/email/activity/MailboxFragmentAdapter;->getRowType(Landroid/database/Cursor;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isMailboxRow(Landroid/database/Cursor;)Z
    .locals 1
    .param p0    # Landroid/database/Cursor;

    invoke-static {p0}, Lcom/android/email/activity/MailboxFragmentAdapter;->isAccountRow(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/android/email/activity/MailboxFragmentAdapter;->isHeaderRow(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    instance-of v0, p1, Lcom/android/email/activity/MailboxListItem;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->bindListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->bindListHeader(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method getAccountId(I)J
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-static {v0}, Lcom/android/email/activity/MailboxFragmentAdapter;->getAccountId(Landroid/database/Cursor;)J

    move-result-wide v1

    return-wide v1
.end method

.method getId(I)J
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-static {v0}, Lcom/android/email/activity/MailboxFragmentAdapter;->getId(Landroid/database/Cursor;)J

    move-result-wide v1

    return-wide v1
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/email/activity/MailboxFragmentAdapter;->isHeader(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method isAccountRow(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-static {v0}, Lcom/android/email/activity/MailboxFragmentAdapter;->isAccountRow(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/email/activity/MailboxFragmentAdapter;->isHeader(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isMailboxRow(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-static {v0}, Lcom/android/email/activity/MailboxFragmentAdapter;->isMailboxRow(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, 0x0

    const-string v0, "rowType"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MailboxFragmentAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040031

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MailboxFragmentAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040032

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
