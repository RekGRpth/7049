.class Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;
.super Lcom/android/emailcommon/utility/EmailAsyncTask;
.source "MailboxListFragment.java"


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MailboxListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FindParentMailboxTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask$ResultCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/emailcommon/utility/EmailAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "[",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccountId:J

.field private final mCallback:Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask$ResultCallback;

.field private final mContext:Landroid/content/Context;

.field private final mEnableHighlight:Z

.field private final mHighlightedMailboxId:J

.field private final mParentMailboxId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;JZJJLcom/android/email/activity/MailboxListFragment$FindParentMailboxTask$ResultCallback;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;
    .param p3    # J
    .param p5    # Z
    .param p6    # J
    .param p8    # J
    .param p10    # Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask$ResultCallback;

    invoke-direct {p0, p2}, Lcom/android/emailcommon/utility/EmailAsyncTask;-><init>(Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;)V

    iput-object p1, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mContext:Landroid/content/Context;

    iput-wide p3, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mAccountId:J

    iput-boolean p5, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mEnableHighlight:Z

    iput-wide p6, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mParentMailboxId:J

    iput-wide p8, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mHighlightedMailboxId:J

    iput-object p10, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mCallback:Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask$ResultCallback;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->doInBackground([Ljava/lang/Void;)[Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[Ljava/lang/Long;
    .locals 14
    .param p1    # [Ljava/lang/Void;

    const-wide/16 v7, -0x1

    const/4 v13, 0x0

    iget-object v9, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mContext:Landroid/content/Context;

    iget-wide v10, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mParentMailboxId:J

    invoke-static {v9, v10, v11}, Lcom/android/emailcommon/provider/Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v6

    if-nez v6, :cond_0

    move-wide v2, v7

    :goto_0
    iget-boolean v9, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mEnableHighlight:Z

    if-eqz v9, :cond_3

    iget-wide v9, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mParentMailboxId:J

    iget-wide v11, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mHighlightedMailboxId:J

    cmp-long v9, v9, v11

    if-nez v9, :cond_1

    iget-wide v0, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mParentMailboxId:J

    :goto_1
    move-wide v4, v0

    :goto_2
    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Long;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v13

    const/4 v8, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    return-object v7

    :cond_0
    iget-wide v2, v6, Lcom/android/emailcommon/provider/Mailbox;->mParentKey:J

    goto :goto_0

    :cond_1
    cmp-long v7, v2, v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mContext:Landroid/content/Context;

    iget-wide v8, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mAccountId:J

    invoke-static {v7, v8, v9, v13}, Lcom/android/emailcommon/provider/Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v0

    goto :goto_1

    :cond_2
    move-wide v0, v2

    goto :goto_1

    :cond_3
    const-wide/16 v0, -0x1

    move-wide v4, v2

    goto :goto_2
.end method

.method protected bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->onSuccess([Ljava/lang/Long;)V

    return-void
.end method

.method protected onSuccess([Ljava/lang/Long;)V
    .locals 7
    .param p1    # [Ljava/lang/Long;

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;->mCallback:Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask$ResultCallback;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/4 v3, 0x1

    aget-object v3, p1, v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/4 v5, 0x2

    aget-object v5, p1, v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-interface/range {v0 .. v6}, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask$ResultCallback;->onResult(JJJ)V

    return-void
.end method
