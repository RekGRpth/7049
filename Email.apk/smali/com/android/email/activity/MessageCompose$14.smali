.class final Lcom/android/email/activity/MessageCompose$14;
.super Ljava/lang/Object;
.source "MessageCompose.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/email/activity/MessageCompose;->safeAddAddressesAsync([Lcom/android/emailcommon/mail/Address;Ljava/lang/String;Landroid/widget/MultiAutoCompleteTextView;Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$addrList:Ljava/util/ArrayList;

.field final synthetic val$addrs:[Lcom/android/emailcommon/mail/Address;

.field final synthetic val$ourAddress:Ljava/lang/String;

.field final synthetic val$view:Landroid/widget/MultiAutoCompleteTextView;


# direct methods
.method constructor <init>([Lcom/android/emailcommon/mail/Address;Ljava/lang/String;Ljava/util/ArrayList;Landroid/widget/MultiAutoCompleteTextView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/activity/MessageCompose$14;->val$addrs:[Lcom/android/emailcommon/mail/Address;

    iput-object p2, p0, Lcom/android/email/activity/MessageCompose$14;->val$ourAddress:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/email/activity/MessageCompose$14;->val$addrList:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/android/email/activity/MessageCompose$14;->val$view:Landroid/widget/MultiAutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const/4 v2, 0x0

    const/16 v7, 0xa

    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    invoke-direct {v4, v8}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iget-object v1, p0, Lcom/android/email/activity/MessageCompose$14;->val$addrs:[Lcom/android/emailcommon/mail/Address;

    array-length v6, v1

    const/4 v5, 0x0

    move v3, v2

    :goto_0
    if-ge v5, v6, :cond_3

    aget-object v0, v1, v5

    invoke-virtual {v0}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/android/email/activity/MessageCompose$14;->val$ourAddress:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/android/email/activity/MessageCompose$14;->val$addrList:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/android/email/activity/MessageCompose$14;->val$addrList:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Lcom/android/email/activity/MessageCompose$14$1;

    invoke-direct {v8, p0, v0}, Lcom/android/email/activity/MessageCompose$14$1;-><init>(Lcom/android/email/activity/MessageCompose$14;Lcom/android/emailcommon/mail/Address;)V

    invoke-virtual {v4, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    add-int/lit8 v2, v3, 0x1

    const/16 v8, 0xfa

    if-le v3, v8, :cond_0

    :goto_1
    return-void

    :cond_0
    :try_start_0
    rem-int/lit8 v8, v2, 0xa

    if-nez v8, :cond_1

    const-wide/16 v8, 0x64

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_2
    add-int/lit8 v5, v5, 0x1

    move v3, v2

    goto :goto_0

    :catch_0
    move-exception v8

    goto :goto_2

    :cond_2
    move v2, v3

    goto :goto_2

    :cond_3
    move v2, v3

    goto :goto_1
.end method
